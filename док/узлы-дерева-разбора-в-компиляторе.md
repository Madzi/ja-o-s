# Узлы дерева разбора в компиляторе и принципы их именования

## Шпаргалка

Дерево компилятора довольно сложное, в табличке приведена стихийно сложившаяся терминология. Она, вероятно, потребует
некоторой переделки. 

|Идент|Смысл|
|:--|:--|
|Выражˉтип|Узел, изображающий написанный в тексте программы тип, например отражает встречающиеся в тексте `цел32`, `ряд 5 из цел`, `набор ... кн`, `ДругойМодуль.Тип`|
|типИзРодаТакихТо|Типы имеют сложную иерархию, например, есть несколько "целых" типов, а также "числовые" типы. Чтобы не путаться между собственно типами и группами похожих по cмыслу типов, введено понятие "Род Типов", например, "Род чисел"|
|Выражˉзначение|Узел, изображающий выражение, возвращающее значение (хотя иногда значения может и не быть). Например, `2`, `а + б`, `МояПроц()`, `м[5]`|
|Обращение|Частный случай выражения - значения, который синтаксически может стоять слева от `:=`. По смыслу может оказаться, что не может, но это становится известно уже позже|
|Обращение˛разобранноеКак...|Типы узлов, возникающие на этапе синтаксического разбора, и изображающие обращение|
|Обращение˛осмысленноеКак...|Типы узлов, возникающие на этапе осмысления, и изображающие обращения (они что ли заменяют Обращение˛разобранное - я это не изучал|
|Значение˛известноеВоВремяКомпиляции...|Возникает, когда компилятор способен вычислить значение, во время компиляции, например `2+2` или `"abc"`|
|ОбъявлениеИменованнойСущности...|Отображает фрагмент текста `перем а : цел32`, `проц К()... кн К`, `тип а = цел32`, т.е. когда объявляется сущность, имеющая имя|
|областьВидимостиГдеОбъявлено|Ссылка на ообъект «ВнутренняяОбластьВидимости», в котором определена данная сущность, 
например, для процедуры ообъекта это будет вн. обл. вид., владельцем которой является объявление ообъекта|
|ОбъявлениеТипаˉнаименования|Случай `тип а = набор ... кн`. Здесь для всего объявления будет узел ОбъявлениеТипаˉнаименования, а для части `набор ... кн` - узел ВыражˉтипИзРодаЗаписей|
|Предписание|То, из чего составляется алгоритм, например, присваивание, цикл, ветвление|
|ВеткаПредписания|Для предписаний `если`, `просей` и т.п. - изображает один из вариантов|
|охватывающееПредписание|Предписание, внутри которого находится данное, например, присваивание - внутри тела процедуры|
|ородитель|ооп-родитель, т.е. родитель в смысле ООП|
|ВнутренняяОбластьВидимости|Контейнер для имён, определённых в модуле, в процедуре, в записи и т.п.|
|владелец|Для внутренней области видимости - та сущность, которая порождает эту область видимости, например, для процедуры
область видимости её параметров и локальных переменных в поле владелец будет иметь ссылку на объявление процедуры|

Недостатки данной системы именований:

- не очень чётко различаются владелец, родитель и то, куда вложено, здесь есть нерешённый терминологический конфликт, см. http://вече.программирование-по-русски.рф/viewtopic.php?f=6&t=431
- набор назван записью
- тип байт, взятый из лиспа, мало кому понятен, но кроме того, есть старое название Set. 


## Как принимались решения

В компиляторе существует много сущностей, выражающих сложные смыслы. Часть из этих смыслов такова, что их легко перепутать. Например, "константа", "объявление константы" и "значение константы" - это три разных сущности, смысл которых необходимо различать для понимания текста компилятора. Авторы компилятора Fox создавали полуусловные обозначения. Например, Type означает "узел синтаксического дерева, изображающий выражение-тип, т.е. обозначение какого-то типа", в то время как "Type Declaration" означает "узел синтаксического дерева, изображающий объявление типа", а также "запись в таблице известных компилятору типов". Практика показала, что полуусловные обозначения вводят в заблуждение. Как минимум, для их понятности необходимы исчерпывающие комментарии в коде около объявления этого типа, которых в коде компилятора Fox нет. Мы попытались сначала для каждой сущности называть её словосочетанием, полностью выражающим смысл. Однако это приводит к массовому появлению идентификаторов, таких как "ПосетиЗначениеИзвестноеВоВремяКомпиляцииИмеющееТипИзРодаЦелочисленныхТипов". Естественно, такие идентификаторы, которые едва помещаются на экран, неприемлемы. Чтобы избежать вырабатывать условные названия, мы решили использовать аббревиатуры. При работе с таким кодом нужно иметь под рукой список расшифровок всех аббревиатур.

## Дерево разбора и его узлы

Дерево разбора формируется на этапе синтаксического разбора и после подвергается преобразованиям, в т.ч. 
осмыслению в смыслоуловителе. В т.ч. происходит вычисление константных выражений, вставляются неявные 
и удаляются ненужные преобразования типов. Таким образом, в дереве могут быть узлы, которые отсутствуют в исходном тексте. Кроме того, узлы дерева могут существовать вообще вне дерева разбора, например, таковы встроенные типы цел32, которые содержат сущность "Объявление типа", но нигде в исходном тексте такого объявления нет. В этой ситуации даже само понятие "узел дерева разбора" является условным. 

## Посетители

Обход дерева происходит с помощью посетителей. Методы, осуществляющие обход, называются VisitXXX, или Посети<НазваниеСущностиВРодительномПадеже>. Конкретные виды посетителей: Смыслоуловитель, генератор объектного кода, генератор документации, переводчик, генератор форматированного исходника из дерева.

### Выражениеˉтип

Узел, изображающий вызов типа, например, при объявлении переменной: `перем А : массив 5 из цел32`;
Здесь "массив 5 из цел32" порождает выражениеˉтип. Базовый класс называется Выражениеˉтип, производные - ВыражˉтипИз<ИмяРодаВДательномПадеже>, например, IntegerType = ВыражˉтипИзРодаЧиселЦел 

### Классификация типов по родам, именование сущностей, связанных с типами

Типы целых чисел, например, цел32, цел64, системно_зависимое_целое, образуют род целочисленных типов. Род целочисленных типов и род типов чисел с плавающей точкой вместе образуют род числовых типов. Некоторые из родов чисел отражены как типы узлов дерева разбора, например, "значение, известное во время компиляции, из рода целочисленных типов", а другие существуют только в нашем воображении. При том, в компиляторе почти все сущности, описывающие типы, на самом деле описывают роды. Поэтому мы переводим их как роды типов, за исключением случаев, когда в роде есть (и будет) только один представитель. В более сложных выражениях мы можем опускать слово "тип" и "род" и говорить, например, "род целых" вместо "рода целых чисел".Или `Значение˛известноеВоВремяКомпиляции˛перечисления`, вместо `значение, известное во время компиляции, имеющее тип из рода перечислений`. Притом, сущности под названием, подобным IntegerType в компиляторе - это не собственно типы, а скорее обращения к ним из исходного текста. Поэтому IntegerType мы переводим не как РодЦел, а как ВыражˉтипИзРодаЧиселЦел - "выражение, обозначающее тип из рода целых". Если в роду один тип, то мы ставим его в единственное число. 

## Объявление именованной сущности (Symbol)

Объявление выполняет две функции:

* изображает фрагмент исходного текста, где объявлена именованная сущность, например, `тип ЦЦЦ = цел32` или `конст к = 5`
* находясь в списке известных сущностей (таблице символов или таблице объявлений), говорит процедурам компилятора, что такая сущность существует, а также хранит свойства этой сущности, нужные компилятору. 

Нужно иметь в виду, что фрагмент исходного текста, где объявлена сущность, зависит от того, находится ли эта сущность в ныне компилируемом модуле (тогда это будет фрагмент текста модуля) или она импортирована (тогда это будет фрагмент символьного файла). Для встроенных сущностей объявления не изображают никакого фрагмента исходного текста, а только говорят о существовании объявления - они возникают во время настройки модулей компилятора. 

### Некоторые типы узлов выражений-значений

#### Конструктор байта (Set) - ВыражˉзначениеКонструкторБайта

Возвращает набор чисел от 0 до размерности байта -1, и биты с такими порядковыми номерами будут установлены в результате выполнения этого выражения, например, `{0,1..2}`. В наше время кажется дикостью байт не с 8 битами, но мы взяли это из лиспа. Вероятно, это было ошибкой. Однако называть такой набор битов в слове множеством - это тоже плохо. 

### Внутренняя область видимости

Внутренняя область видимости - это набор имён, находящихся внутри процедуры (параметры, непосредственно вложенные процедуры, переменные). Если назвать просто "область видимости процедуры", то непонятно - это область видимости самой процедуры (где видна процедура) или область того, что внутри процедуры. Поэтому мы называем её внутренней. 

