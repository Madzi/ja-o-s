# Сборка старых версий

О сборке современных см. [сборка.md](сборка.md)

### Сценарий с помощью wsl-build

Сборка была хрупкой, т.к. модифицировала директорию по всей дороге. Поэтому если ломалась посередине процесса, повторно запустить было проблематично.
На всякий случай лучше копировать весь репозиторий целиком перед попыткой сборки.

Работала через wsl, и далее в нём запускаем

```
bash /mnt/c/ob/jaos/WinAos/wsl-build/wsl-build.sh 
``` 

Дальше всё будет написано на экране.

### Собираем яос0 от 2019-12-28

```
>cd /d C:\ob\jaos\winaos  
> move a2.exe a2host.exe  
> del * .Dpi * .Obw SystemTrace * .txt  
>mkdir obgn  
> REM сохраняем куда-нибудь Work\auto.dsk   
>rmdir /Q /S Work  
>mkdir Work  
>mkdir Work /xym  
> a2host.exe  
> A > PET.Open Release.Tool  
> A > Предполагаем, что нужная нам сборка - WinAosNewObjectFile  
> A > Находим её текстом и получаем команду (подправим в ней сразу путь):  
> REM а тут пришлось скопировать Exclude-ы.   
> A > Release.Build --path ="../obgn/" WinAosNewObjectFile ~  
> A > Выскакивает файлик с командами. В нём жмём Ctrl-Enter на слове SystemTools в строке SystemTools.DoCommands  
> A > А потом вот такая незатейливая команда для линковки (путь тоже поменял)  
> A > StaticLinker.Link --fileFormat =PE32 --fileName =A2.exe --extension =GofW --displacement =401000H --path ="../obgn/" Runtime Trace Kernel32 Machine Heaps Modules Objects Kernel KernelLog Streams Commands FIles WinFS Clock Dates Reals Strings Diagnostics BitSets StringPool ObjectFile GenericLinker Reflection  GenericLoader  BootConsole ~  
> A > SystemTools.PowerDown ~  
> move Work\a2.exe a2.exe  
> move Work\a2.log a2.log  
> move obg obg.old  
> move obgn obg  
>rmdir /Q /S xym  
>mkdir xym  
> move Work\xym\* xym  
> a2.exe  
```

Теоретически дальше надо создать перекрёстные ссылки с помощью команды

TFXRef.MakeXRef "C:/ob/jaos/source/" "*Oberon*"~
, но она заведомо должна сломаться. Кроме того, идея отбирать файлы по маске всё же порочна.
Надо, что ли, переделать, чтобы наконец уже бралось на базе конфигурации, и делалось как часть
сборки.

### Собираем версию от 2016-01-05 для начала. Для неё данные из первоисточника не работают.

* Скачиваем (клон репозитория)
* Заходим в терминале директорию WinAos
* del *.obw
* mkdir objt
* rmdir /Q /S xym
* move aos.exe aosHost.exe
* Запускаем aosHost.exe
* Запускаем блокнот в AOS (Главное меню/Edit/Text)
* Копируем из данной вики-страницы, вставляем в блокнот (Ctrl-Alt-V) и выполняем команду:Release.Build --path="objt/" -b WinAos~для выполнения нужно нажать Ctrl-Enter, находясь внутри текста команды.
* То же для команды связывания:PELinker.Link --path=objt/ Win32.Aos.Link~
* Выходим из системы: SystemTools.PowerDown~
* move obj obj.old
* move objt obj
* Может понадобиться повторная пересборка системы из самой себя, например, если механизм

генерации символьных файлов поменялся.

### Собираем a2.exe после 2016-05-15 (новый формат объектных файлов)

```
>cd /d C:\ob\jaos\winaos  
> move a2.exe a2host.exe  
>mkdir obgn  
>mkdir Work  
> a2host.exe  
> A > PET.Open Release.Tool  
> A > Предполагаем, что нужная нам сборка - WinAosNewObjectFile  
> A > Находим её текстом и получаем команду (подправим в ней сразу путь):  
> A > Release.Build --path ="../obgn/" WinAosNewObjectFile ~  
> A > Выскакивает файлик с командами. В нём жмём Ctrl-Enter на слове SystemTools в строке SystemTools.DoCommands  
> A > А потом вот такая незатейливая команда для линковки (путь тоже поменял)  
> A > StaticLinker.Link --fileFormat =PE32 --fileName =A2.exe --extension =GofW --displacement =401000H --path ="../obgn/" Runtime Trace Kernel32 Machine Heaps Modules Objects Kernel KernelLog Streams Commands FIles WinFS Clock Dates Reals Strings Diagnostics BitSets StringPool ObjectFile GenericLinker Reflection  GenericLoader  BootConsole ~  
> A > SystemTools.PowerDown ~  
> move Work\a2.exe a2.exe  
> move obg obg.old  
> move obgn obg  
> a2.exe  
# Создаём перекрёстные ссылки  
TFXRef.MakeXRef "C:/ob/jaos/source/""*Oberon*" ~  
```

### Для версии от 2018-11-16, SHA = 1910c44f5feffc8362de3524f7534145ce411c77

* Скачиваем (клон репозитория)
* Заходим в Win32
* mkdir objt
* Запускаем a2.bat
* move oberon.exe oberonHost.exe
* Запускаем Shell
* PET.Open C:/ob/A2OS/source/Release.Tool
* Там ищем WINAOS и в нём второй набор команд
* Выполняем компиляцию, линковку.
* Выходим из системы
* Скопировать все файлы из objt в bin
