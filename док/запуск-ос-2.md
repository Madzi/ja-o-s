# Запуск ОС

Рассматриваем только для Win32. 

## Порядок загрузки

Похоже, что порядок загрузки определяется командой линковщика, которая указана
в комментариях в файле ЯОС.фок. В частности, для поиска набора модулей
для нашей функции ищи WINAOS в этом файле. Вот их порядок на 2019-09-10 до революции числовых типов:
```
Builtins Trace Kernel32 Machine Heaps Modules Objects Kernel KernelLog Streams
 Commands Files WinFS Clock Dates Reals Strings Diagnostics BitSets StringPool 
 ObjectFile GenericLinker Reflection Loader BootConsole 
```
Моя гипотеза состоит в том, что код инициализации этих модулей (BEGIN..END ИмяМодуля) выполняется в порядке
перечисления этих модулей при линковке. 

### Атрибут INITIAL

Предположительно, обозначает входную точку ОС. Далее рассматриваем модули в порядке загрузки. 

### Trace

При загрузке ничего не делает.

### Kernel

При загрузке ничего не делает, но в нём есть процедура EntryPoint с атрибутом INITIAL - для Dll.

### Machine и ini-файл

В современной (2019, и, видимо, с начала 2016) версии при загрузке ничего не делает. Содержит процедуру Start с атрибутом INITIAL, которая, по видимому, и запускает A2, если она собрана как Exe.

Процедура Start запускает Kernel.Init, а затем определяет и читает файл инициализации. Он (вроде) может называться myaos.ini, или, при его отсутствии, aosg.ini . Выполнение команд из файла инициализации происходит дальше, 
в модуле BootConsole. 

### BootConsole и команды Boot в ini-файле

Мы пропустили кучу модулей. В WINAOS BootConsole - это последний модуль в слинкованном образе, он
выполняет ряд команд и декларативных описаний из ini-файла (или, возможно, из другого источника?). 

Читаются команды Boot, Boot1..Boot9, BootSystem, BootSystem1..BootSystem9, а также описания 
томов BootVol1..BootVol9

Пример набора команд Boot для WinAOS:

```
Boot  = Traps.Install
Boot1 = FileTrapWriter.Install
Boot2 = Display.Install  --fullscreen
Boot3 = WindowManager.Install
Boot4 = Clipboard.Install
Boot5 = FSTools.Mount WORK RelativeFileSystem ./
Boot7 = Autostart.Run
Boot8 = Heaps.SetMetaData
Boot9 = Kernel.GC
```

Они вызываются при загрузке модуля BootConsole. Дальнейшие детали см. в исходнике, BootConsole.Mod.

## Где и как используется информация из ini-файла

* в модуле BootConsole читаются команды Boot, Boot1..Boot9, BootSystem, BootSystem1..BootSystem9, а также описания томов BootVol1..BootVol9
* ищи вызовы Machine.GetConfig

## Файл настройки Configuration.Xml

Сейчас уже некогда писать, но кое-что есть в оф. документации. Там есть секция Autostart, во всяком случае. 

## Аргументы командной строки

Описаны в док-те [сборка и запуск](сборка-и-запуск.md#аргументы-командной-строки).

Также можно попробовать CommandLine.Open, но вряд ли это нужно. 

## Настройки главного окна WinAOS 

Часть этих настроек имеется в файле Windows.Display.Mod, надо искать `options.Add` и на 2019-09-10 это будет 

```
		options.Add("b","bits16",Options.Flag);
		options.Add("f","fullscreen",Options.Flag);
		options.Add("w","width",Options.Integer);
		options.Add("h","height",Options.Integer);
		options.Add("x","xOffset",Options.Integer);
		options.Add("y","yOffset",Options.Integer);
		options.Add("n","noMouseCursor",Options.Flag);
```

Например, чтобы окно было 1600х1000, надо в aosg.ini прописать команду:
```
Boot2 = Display.Install --width=1600 --height=1000 
```

## См. также

* A2Documentation.pdf, глава «Customization of A2»
* [Сборка и запуск](сборка-и-запуск.md) (с точки зрения пользователя)
