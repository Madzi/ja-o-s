Поднимаем яос0 с 2019-09-10 до 2019-09-1т0
=========================================

2019-09-10 - релиз от 2019-09-10 до революции числовых типов
2019-09-1т0 - после (коммит "merged rename branch")

Суть дела
---------

Основное отличие состоит в том, что произведено переименование типов, примерно такое:

```
#!/bin/sh

# rename platform-dependent types
sed -i "s/\bSHORTINT\b/SIGNED8/g" $(grep "\bSHORTINT\b" -Irl)
sed -i "s/\bINTEGER\b/SIGNED16/g" $(grep "\bINTEGER\b" -Irl)
sed -i "s/\bLONGINT\b/SIGNED32/g" $(grep "\bLONGINT\b" -Irl)
sed -i "s/\bHUGEINT\b/SIGNED64/g" $(grep "\bHUGEINT\b" -Irl)
sed -i "s/\bREAL\b/FLOAT32/g" $(grep "\bREAL\b" -Irl)
sed -i "s/\bLONGREAL\b/FLOAT64/g" $(grep "\bLONGREAL\b" -Irl)
sed -i "s/\bCOMPLEX\b/COMPLEX32/g" $(grep "\bCOMPLEX\b" -Irl)
sed -i "s/\bLONGCOMPLEX\b/COMPLEX64/g" $(grep "\bLONGCOMPLEX\b" -Irl)

# rename platform-independent types
sed -i "s/\bWORD\b/INTEGER/g" $(grep "\bWORD\b" -Irl)
sed -i "s/\bLONGWORD\b/LONGINTEGER/g" $(grep "\bLONGWORD\b" -Irl)
sed -i "s/\bWORDSET\b/INTEGERSET/g" $(grep "\bWORDSET\b" -Irl)
```

Попробовал применить этот скрипт к версии до "merge rename branch", но там явно доделывалось
руками. Поэтому действуем так:

- определяем список файлов, которые мы меняли или добавлялись, сравнивая пофайлово яос02 
и состояние мастера, от которого ответвлялись
- смотрим, какие из них менялись в мастере в момент переименования типов
    - если отсутствуют или не менялись и это не модуль, то берём наш
    - если отсутствует и это модуль, то обрабатываем скриптом
    - если присутствует и это модуль, то обрабатываем скриптом наш и сливаем вручную
    - если присутствует и у них не менялся, то так не может быть
- если что-то не работает, то правим

План-журнал действий
---------------

## Отводим промежуточные ветки

яос02-похожая-на-2019-09-1т0 и промежуточная-ветка-2019-09-1т0

яос03 будет расти от промежуточной ветки, т.к. там изменений гораздо больше, 
чем у нас. 

## Список файлов не *.Mod, которые мы меняли

По каждому определить, менялся ли он в революцию

* Tutorial.Text - потом посмотрим (пока берём наш)
* Auto.dsk - берём наш
* Configuration.XML - не менялся, берём наш 
* HotKeys.XML - не менялся, берём наш
* Release.Tool - изменился (HUGEINT->SIGNED64), меняем в нашем и берём его.
* `.gitignore` - определить наличие изменений в мастере - не менялся, берём наш.

Нужные из этих файлов мы корректируем в яос02-похожая-на-2019-09-1т0, фиксируем и копируем
в промежуточную ветку с помощью WinMerge. 

## Список файлов не *.Mod, к-рые мы добавляли

Их просто добавляем в промежуточную ветку с помощью WinMerge.
* WMDebugger.Tool 
* WMDebugger.XML 


## Спиок *.Mod, которые мы добавляли

Их обрабатываем скриптом в ветке яос02-похожая-на-2019-09-1т0, фиксируем, и с помощью WinMerge

```
BtDbgPanel.Mod BtDecoder.Mod BtDTraps.Mod BtMenus.Mod FoxDisassembler1.Mod \
I386Decoder2.Mod PodrobnajaPechatq.Mod PrimerPodrobnajaPechatq.Mod Proba.Mod r.Mod \
RasshirenieOpcijjKompiljatora.Mod TestDbg.Mod TestDbg2.Mod TestDecode.Mod TestDisassembler.Mod \
TestSymbolFile.Mod TryDecode.Mod WMComboBox.Mod WMDebugger.Mod WMEditors3.Mod WMTextView2.Mod \
WMTextView3.Mod WMXmas.Mod Xmas.Mod \
```

## Список *.Mod, которые мы меняли 

Все менялись в революцию. Действие по умолчанию - прогнать скрипт в яос02-похожая-на-2019-09-1т0 , зафиксировать,  сравнить
с состоянием после революции. 

* AMD64.WMRasterScale.Mod (disable ugly font smoothing) 
* BimboScanner.Mod - прогнать скрипт и не сравнивать
* Compiler.Mod
* Decoder.Mod 
* FoxAMDBackend.Mod
* FoxIntermediateBackend.Mod
* FoxSemanticChecker.Mod 
* Heaps.Mod
* I386.WMRasterScale.Mod 
* PET.Mod
* PETTrees.Mod
* TextCompiler.Mod
* TFAOParser.Mod - берём наш после скрипта
* TFCheck.Mod - то же
* TFDocGenerator.Mod - то же
* TFDumpTS.Mod - то же
* TFModuleTrees.Mod - то же
* TFPET.Mod - то же
* TFScopeTools.Mod - то же
* TFStringPool.Mod - то же
* TFTypeSys.Mod - то же
* TFXRef.Mod - то же
* WMTextView.Mod - скрипт и сравнение

```
cp AMD64.WMRasterScale.Mod \
BimboScanner.Mod  \
Compiler.Mod \
Decoder.Mod  \
FoxAMDBackend.Mod \
FoxIntermediateBackend.Mod \
FoxSemanticChecker.Mod  \
Heaps.Mod \
I386.WMRasterScale.Mod  \
PET.Mod \
PETTrees.Mod \
TextCompiler.Mod \
TFAOParser.Mod  \
TFCheck.Mod  \
TFDocGenerator.Mod \
TFDumpTS.Mod \
TFModuleTrees.Mod \
TFPET.Mod \
TFScopeTools.Mod \
TFStringPool.Mod \
TFTypeSys.Mod \
TFXRef.Mod \
WMTextView.Mod \
../2019-09-1т0
```
Выяснились доп. замены: 
system.shortintType -> Global.Signed8
system.longintType -> Global.Signed32
system.hugeintType -> Global.Signed64
system.integerType -> Global.Signed16

Но мы их не будем делать, а просто перенесём наши кусочки куда надо. Они вроде не связаны с этими заменами. 

### Новые файлы

Сливаем метки, новые файлы сохраняют свою историю. 

### Пересборка

Оно не собирается старым компилятором, и в версии "merged rename branch" старые исходники. Поэтому двигаемся чуть вперёд до Rebuilt releases и берём оттуда 
двоичные файлы (плюс один исходный). Делаем это с помощью merge со стратегией recursive/theirs


## Что ещё нужно сделать?

Переименовать всякие там sfTypeInteger (PodrobnajaPechatq.Mod). Но они пока и в Reflection не переделаны, поэтому не будем. 

