set -xeuo pipefail 

SCRIPT_PATH=$(cd $(dirname $0) && pwd);
export JAOS_ROOT=`readlink -f $SCRIPT_PATH`

bash $JAOS_ROOT/Linux32/skripty-sborki/Linux32-iz-Win32/wsl-build.sh --avto 1
bash $JAOS_ROOT/Win32/skripty-sborki/Win32-iz-Win32/wsl-build.sh --avto 1
