;;;; calcu-core.asd

(asdf:defsystem :trob
  :description ""
  :depends-on (:budden-tools)
  :author ""
  :license "(C) Денис Будяк 2020"
  :serial t
  :components ((:file "package")
               (:file "trob-types-vars-macros")
               (:file "procs")))

