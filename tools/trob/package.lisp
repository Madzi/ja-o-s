; -*- Encoding: utf-8; system  :trob -*- 
(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :TROB
  (:use :cl :perga-implementation)
  (:shadowing-import-from 
   :budden-tools budden-tools:str++
   budden-tools:read-file-into-string)
  (:export "TROB:LEXEM
            TROB:MERGE-FILES
            TROB:слей-переводы-файлов
            TROB:с-русского-на-английский-и-обратно
            TROB:индекс-кирилличности
            TROB:допереведи
            TROB:напиши-скрипт-растенения-файлов
            "))

