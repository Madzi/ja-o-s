(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Example6;	(* pjm *)

(*
Readers and Writers scheduler.
Ref: C.A.R. Hoare, "Monitors: An Operating System Structuring Concept", CACM 17(10), 1974
*)

тип
	ReadersWritersScheduler* = окласс
		перем busy: булево; readers, writers: размерМЗ;

		проц StartRead*;
		нач {единолично}
			дождись(~busy и (writers = 0));	(* waiting writers have priority over readers *)
			увел(readers)
		кон StartRead;

		проц EndRead*;
		нач {единолично}
			умень(readers)
		кон EndRead;

		проц StartWrite*;
		нач {единолично}
			увел(writers);
			дождись(~busy и (readers = 0));
			busy := истина
		кон StartWrite;

		проц EndWrite*;
		нач {единолично}
			умень(writers); busy := ложь
		кон EndWrite;

		проц &Init*;
		нач
			busy := ложь; readers := 0; writers := 0
		кон Init;

	кон ReadersWritersScheduler;

кон Example6.
