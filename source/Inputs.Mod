(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Inputs; (** AUTHOR "pjm"; PURPOSE "Abstract input device"; *)

(* Based on SemInput.Mod by Marc Frei *)

использует Machine, Kernel, Plugins;

конст
		(** KeyboardMsg flags. *)
	Release* = 0;	(** a key release event, otherwise a key press or repeat. *)

		(** shift key states. *)
	LeftShift* = 1; RightShift* = 2; LeftCtrl* = 3; RightCtrl* = 4;
	LeftAlt* = 5; RightAlt* = 6; LeftMeta* = 7; RightMeta* = 8;

		(** combined shift key states. *)
	Shift* = {LeftShift, RightShift}; Ctrl* = {LeftCtrl, RightCtrl};
	Alt* = {LeftAlt, RightAlt}; Meta* = {LeftMeta, RightMeta};

		(** flags for KeyState *)
	SHIFT* = 0;  CTRL* = 1;  ALT* = 2;

		(** Значения keysym - логических кодов клавиш ЯОС. Похожи, но не совпадают с X11 keysyms (логическими кодами клавиш X11) *)
	KsNil* = 0FFFFFFH;	(** no key *)

		(** TTY Functions, cleverly chosen to map to ascii *)
	KsBackSpace* = 0FF08H;	(** back space, back char *)
	KsTab* = 0FF09H;
	KsReturn* = 0FF0DH;	(** Return, enter *)
	KsPause* = 0FF13H;	(** Pause, hold *)
	KsScrollLock* = 0FF14H;
	KsSysReq* = 0FF15H;
	KsEscape* = 0FF1BH;
	KsDelete* = 0FFFFH;	(** Delete, rubout *)

		(** Cursor control & motion *)
	KsHome* = 0FF50H;
	KsLeft* = 0FF51H;	(** Move left, left arrow *)
	KsUp* = 0FF52H;	(** Move up, up arrow *)
	KsRight* = 0FF53H;	(** Move right, right arrow *)
	KsDown* = 0FF54H;	(** Move down, down arrow *)
	KsPageUp* = 0FF55H;	(** Prior, previous *)
	KsPageDown* = 0FF56H;	(** Next *)
	KsEnd* = 0FF57H;	(** EOL *)

		(** Misc Functions *)
	KsPrint* = 0FF61H;
	KsInsert* = 0FF63H;	(** Insert, insert here *)
	KsMenu* = 0FF67H;	(** Windows menu *)
	KsBreak* = 0FF6BH;
	KsNumLock* = 0FF7FH;

		(** Keypad functions *)
	KsKPEnter* = 0FF8DH;	(** enter *)
	KsKPMultiply* = 0FFAAH;
	KsKPAdd* = 0FFABH;
	KsKPSubtract* = 0FFADH;
	KsKPDecimal* = 0FFAEH;
	KsKPDivide* = 0FFAFH;

		(** Function keys *)
	KsF1* = 0FFBEH; KsF2* = 0FFBFH; KsF3* = 0FFC0H; KsF4* = 0FFC1H; KsF5* = 0FFC2H; KsF6* = 0FFC3H;
	KsF7* = 0FFC4H; KsF8* = 0FFC5H; KsF9* = 0FFC6H; KsF10* = 0FFC7H; KsF11* = 0FFC8H; KsF12* = 0FFC9H;

		(** Modifiers *)
	KsShiftL* = 0FFE1H;	(** Left shift *)
	KsShiftR* = 0FFE2H;	(** Right shift *)
	KsControlL* = 0FFE3H;	(** Left control *)
	KsControlR* = 0FFE4H;	(** Right control *)
	KsCapsLock* = 0FFE5H;	(** Caps lock *)
	KsMetaL* = 0FFE7H;	(** Left meta, Left Windows *)
	KsMetaR* = 0FFE8H;	(** Right meta, Right Windows *)
	KsAltL* = 0FFE9H;	(** Left alt *)
	KsAltR* = 0FFEAH;	(** Right alt *)

		(** HID Consumer Keys**)
	KsScanPreviousTrack*=	0FF0000H;
	KsScanNextTrack*= 		0FF0001H;
	KsALConsumerControl*= 	0FF0002H;
	KsMute*= 				0FF0003H;
	KsVolumeDecrement*= 	0FF0004H;
	KsVolumeIncrement*= 	0FF0005H;
	KsPlayPause*= 			0FF0006H;
	KsStopOSC*=			0FF0007H;
	KsALEmailReader*=		0FF0008H;
	KsALCalculator*=		0FF0009H;
	KsACSearch*=			0FF000AH;
	KsACHome*=			0FF000BH;
	KsACBack*=				0FF000CH;
	KsACForward*=			0FF000DH;
	KsACBookmarks*=		0FF000EH;
	KsConsumerButtons*=	0FFF000H;
(* логический код клавиши ЯОС = лккя *)	
	лккяFвСочетанииСControl* = 06H; (* при получении такого кода флаги всё равно надо проверять *)
	лккяNвСочетанииСControl* = 0EH;
	лккяPвСочетанииСControl* = 10H;
	

тип
	Message* = запись кон;	(** generic message. *)

	KeyboardMsg* = запись (Message)
		ch*: симв8;	(** extended ASCII key code, or 0X if not relevant *)
		flags*: мнвоНаБитахМЗ;	(** key flags *)
		keysym*: цел32	(** X11-compatible key code *)
	кон;

	MouseMsg* = запись (Message)
		keys*: мнвоНаБитахМЗ;	(** mouse key state. *)
		dx*, dy*, dz*: цел32	(** mouse movement vector. *)
	кон;

	AbsMouseMsg*= запись(Message);
		keys*: мнвоНаБитахМЗ;
		x*,y*,z*,dx*,dy*,dz*: цел32;
	кон;

	PointerMsg* = запись (Message)
		keys*: мнвоНаБитахМЗ;	(** pointer key state. *)
		x*, y*, z*: цел32;	(** pointer position. *)
		mx*, my*, mz*: цел32	(** pointer max values. *)
	кон;

тип
	Sink* = окласс	(** a message receiver. *)
		(** Handle is overriden by a concrete receiver. *)
		проц Handle*(перем msg: Message);
		нач СТОП(301) кон Handle;
	кон Sink;

	Group* = окласс	(** a group of message receivers. *)
		(** Add a receiver to a group. *)
		проц Register*(s: Sink);
		нач СТОП(301) кон Register;

		(** Remove a receiver from a group. *)
		проц Unregister*(s: Sink);
		нач СТОП(301) кон Unregister;

		(** Send a message to all receivers currently in the group. *)
		проц Handle*(перем msg: Message);
		нач СТОП(301) кон Handle;
	кон Group;

тип
	Pointer* = окласс (Sink)	(** convert incremental movements into absolute positions *)
		перем
			cur: PointerMsg;
			threshold, speedup: цел32;
			fixedKeys: мнвоНаБитахМЗ;

		проц Update;
		перем p: PointerMsg;
		нач
			если cur.x < 0 то cur.x := 0
			аесли cur.x > cur.mx то cur.x := cur.mx
			всё;
			если cur.y < 0 то cur.y := 0
			аесли cur.y > cur.my то cur.y := cur.my
			всё;
			если cur.z < 0 то cur.z := 0
			аесли cur.z > cur.mz то cur.z := cur.mz
			всё;
			p := cur; p.keys := p.keys + fixedKeys;
			pointer.Handle(p)
		кон Update;

		проц SetKeys(keys: мнвоНаБитахМЗ);
		нач {единолично}
			fixedKeys := keys; Update
		кон SetKeys;

		проц {перекрыта}Handle*(перем m: Message);
		перем dx, dy: цел32;
		нач {единолично}
			если m суть MouseMsg то
				просейТип m: MouseMsg делай
				dx := m.dx; dy := m.dy;
				если (матМодуль(dx) > threshold) или (матМодуль(dy) > threshold) то
					dx := dx*speedup DIV 10; dy := dy*speedup DIV 10
				всё;
				увел(cur.x, dx); увел(cur.y, dy); увел(cur.z, m.dz);
				cur.keys := m.keys;
				Update;
				всё;
			аесли m суть AbsMouseMsg то
				просейТип m: AbsMouseMsg делай
					cur.x := m.x; cur.y := m.y; cur.z := m.z;
					cur.keys := m.keys;
					Update
				всё;
			всё
		кон Handle;

		проц SetLimits*(mx, my, mz: цел32);
		нач {единолично}
			cur.mx := mx; cur.my := my; cur.mz := mz;
			Update
		кон SetLimits;

		проц &Init*(t, s: цел32);
		нач
			threshold := t; speedup := s;
			cur.x := 0; cur.y := 0; cur.z := 0;
			cur.mx := 1; cur.my := 1; cur.mz := 1;
			cur.keys := {}; fixedKeys := {};
			mouse.Register(сам)
		кон Init;

	кон Pointer;

тип
	List = укль на запись
		next: List;
		s: Sink
	кон;

	Broadcaster = окласс (Group)
		перем sentinel: List;

		проц {перекрыта}Register*(s: Sink);
		перем n: List;
		нач {единолично}
			нов(n); n.s := s; n.next := sentinel.next; sentinel.next := n
		кон Register;

		проц {перекрыта}Unregister*(s: Sink);
		перем n: List;
		нач {единолично}
			n := sentinel;
			нцПока (n.next # НУЛЬ) и (n.next.s # s) делай n := n.next кц;
			если n.next # НУЛЬ то n.next := n.next.next всё
		кон Unregister;

		проц {перекрыта}Handle*(перем msg: Message);
		перем n: List;
		нач {единолично}
			n := sentinel.next;
			нцПока n # НУЛЬ делай n.s.Handle(msg); n := n.next кц
		кон Handle;

	кон Broadcaster;


тип
	OberonInput* = окласс (Plugins.Plugin)
		перем timer-: Kernel.Timer;

		проц Mouse*(перем x, y: цел16; перем keys:мнвоНаБитахМЗ);
		нач
			СТОП(99)	(* abstract *)
		кон Mouse;

		проц Read*(перем ch: симв8; перем break: булево);
		нач
			СТОП(99)	(* abstract *)
		кон Read;

		проц Available*(перем num: цел16; перем break: булево);
		нач
			СТОП(99)	(* abstract *)
		кон Available;

		проц KeyState*(перем k: мнвоНаБитахМЗ);
		нач
			СТОП(99)	(* abstract *)
		кон KeyState;

		проц &Init*;
		нач
			нов(timer)
		кон Init;

	кон OberonInput;

тип
	MouseFixer = окласс (Sink)
		перем ctrl: булево;

		проц {перекрыта}Handle*(перем m: Message);
		перем new: булево;
		нач {единолично}
			просейТип m: KeyboardMsg делай
				new := m.flags * Ctrl # {};
				если new # ctrl то
					ctrl := new;
					если ctrl то main.SetKeys({1}) иначе main.SetKeys({}) всё
				всё
			всё
		кон Handle;

		проц &Init*;
		нач
			ctrl := ложь; keyboard.Register(сам)
		кон Init;

	кон MouseFixer;
	
(* ControlKeyDown проверяет, что нажат модификатор Control, но не другие модификаторы *)	
проц ControlKeyDown*(flags : мнвоНаБитахМЗ) : булево;
нач
	возврат (flags * Ctrl # {}) и (flags - Ctrl = {});
кон ControlKeyDown;

(* EitherShiftOrControlDown проверяет, что нажат модификатор Control и/или Shift, но не другие модификаторы *)	
проц EitherShiftOrControlDown*(flags : мнвоНаБитахМЗ) : булево;
нач
	возврат ((flags * Shift # {}) и (flags * Ctrl = {})) или ((flags * Ctrl # {}) и (flags * Shift = {}));
кон EitherShiftOrControlDown;
	
(* ControlShiftKeyDown проверяет, что нажаты модификаторы Control и Shift, но не другие модификаторы *)	
проц ControlShiftKeyDown*(flags : мнвоНаБитахМЗ) : булево;
нач
	возврат (flags * Ctrl # {}) и (flags * Shift # {}) и (flags - Ctrl - Shift = {});
кон ControlShiftKeyDown;

перем
	keyboard*, mouse*, pointer*: Group;
	main*: Pointer;
	oberonInput*: Plugins.Registry;
	mouseFixer: MouseFixer;

(** Return a default message broadcaster instance. *)
проц NewBroadcaster*(): Group;
перем b: Broadcaster;
нач
	нов(b); нов(b.sentinel); b.sentinel.next := НУЛЬ;
	возврат b
кон NewBroadcaster;

проц Init;
перем s: массив 16 из симв8; i: размерМЗ; threshold, speedup: цел32;
нач
	Machine.GetConfig("Threshold", s);
	i := 0; threshold := Machine.StrToInt(i, s);
	если threshold <= 0 то threshold := 5 всё;
	Machine.GetConfig("Speedup", s);
	i := 0; speedup := Machine.StrToInt(i, s);
	если speedup <= 0 то speedup := 15 всё;
	нов(main, threshold, speedup);
	Machine.GetConfig("MB", s);
	если (s = "2") или (s = "-2") то нов(mouseFixer) всё
кон Init;

нач
	keyboard := NewBroadcaster();
	mouse := NewBroadcaster();
	pointer := NewBroadcaster();
	нов(oberonInput, "Inputs", "Oberon input drivers");
	Init
кон Inputs.
