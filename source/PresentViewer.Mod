модуль PresentViewer; (** AUTHOR "TF"; PURPOSE "Testbed for the component system"; *)

использует
	Modules, Commands, WMComponents, XML, WM := WMWindowManager;

перем w : WMComponents.FormWindow;

проц Open*(context : Commands.Context);
перем
	c : XML.Content; filename : массив 128 из симв8;
	width, height : размерМЗ;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
	c := WMComponents.Load(filename);
	width := 320; height := 240;
	если c # НУЛЬ то
		если c суть WMComponents.VisualComponent то
			width := c(WMComponents.VisualComponent).bounds.GetWidth();
			height := c(WMComponents.VisualComponent).bounds.GetHeight();
			если width <= 0 то width := 10 всё; если height <= 0 то height := 10 всё
		всё;
		нов(w, width, height, истина);
		w.pointerThreshold := 50;
		WM.ExtAddWindow(w, 500, 350, {});
		w.SetTitle(c(XML.Element).GetAttributeValue("caption"));
		w.SetContent(c);
	иначе
		context.error.пСтроку8(filename); context.error.пСтроку8(" not correctly loaded"); context.error.пВК_ПС;
	всё;
кон Open;

(* Cleanup on module free *)
проц Cleanup;
нач
	если w # НУЛЬ то w.Close всё
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон PresentViewer.

System.Free PresentViewer ~
PresentViewer.Open XmasMenu.XML ~

