модуль SkinLanguage; (** AUTHOR "FN"; PURPOSE "Bluebottle Skin Language"; *)

использует
	XML, XMLScanner, XMLParser, XMLObjects, Strings, Files, Потоки, ЛогЯдра,
	WMGraphics;

конст
	Buffersize = 128;
	CR = 0DX; LF = 0AX; TAB = 9X; SP = 20X; EOF = 0X;
	ConfigFileName* = "SkinConfig.XML";

тип
	String = Strings.String;

	ReportError* = проц {делегат} (pos, line, col : цел32; msg : String);
	StringReportError = проц {делегат} (msg : String);

	Property = окласс
	перем name, type : String;
		mandatory, succeeded: булево;
		next : Property;

		проц & Init *(n, t : String);
		нач
			name := n;
			type := t;
			mandatory := истина;
			succeeded := ложь
		кон Init;

	кон Property;

	PropertyList = окласс
	перем first : Property;

		проц Add(e : XML.Element);
		перем p : Property; s : String;
		нач
			нов(p, e.GetAttributeValue("name"), e.GetAttributeValue("type"));
			s := e.GetAttributeValue("mandatory");
			если s # НУЛЬ то
				LowerCase(s); p.mandatory := (s^ # "false")
			всё;
			p.next := first; first := p
		кон Add;

		проц Find(s : String) : Property;
		перем p : Property;
		нач
			p := first;
			нцПока p # НУЛЬ делай
				если p.name^ = s^ то возврат p всё;
				p := p.next
			кц;
			возврат НУЛЬ
		кон Find;

	кон PropertyList;

	Component = окласс
	перем properties : PropertyList;
		name : String;
		next : Component;

		проц & Init*(s : String);
		нач
			name := s;
			нов(properties)
		кон Init;

		проц MandatoryPropertiesSucceeded(re : StringReportError) : булево;
		перем result : булево;
			p : Property;
		нач
			result := истина; p := properties.first;
			нцПока p # НУЛЬ делай
				если p.mandatory и ~p.succeeded то
					re(Append(NewString("Missing mandatory property : "), p.name^));
					result := ложь
				всё;
				p := p.next
			кц;
			возврат result
		кон MandatoryPropertiesSucceeded;

	кон Component;

	ComponentList = окласс
	перем first: Component;

		проц Add(e : XML.Element);
		перем c : Component;
			en : XMLObjects.Enumerator;
			x : XML.Element;
			a : динамическиТипизированныйУкль;
			s : String;
		нач
			нов(c , e.GetAttributeValue("name"));
			en := e.GetContents();
			en.Reset();
			нцПока en.HasMoreElements() делай
				a := en.GetNext();
				если a суть XML.Element то
					x := a(XML.Element);
					s := x.GetName();
					если s^ = "property" то
						c.properties.Add(x)
					всё
				всё
			кц;
			c.next := first;
			first := c
		кон Add;

		проц Find(s : String) : Component;
		перем c : Component;
		нач
			c := first;
			нцПока c # НУЛЬ делай
				если c.name^ = s^ то возврат c всё;
				c := c.next
			кц;
			возврат НУЛЬ
		кон Find;

	кон ComponentList;

	Scanner* = окласс
	перем r: Потоки.Чтец;
		pos-, oldpos-, line-, col-, oldcol- : цел32;

			(** initialize with stream to scan *)
		проц & Init*(r : Потоки.Чтец);
		нач
			сам.r := r;
			line := 1; col := 0; pos := 0; oldpos := 0; oldcol := 0
		кон Init;

			(* return the next character in the stream. update pos, line and col *)
		проц NextCh() : симв8;
		перем ch : симв8;
		нач
			ch := r.чИДайСимв8(); увел(pos);
			если (ch = CR) или (ch = LF) то увел(line); col := 0
			иначе увел(col) всё;
			возврат ch
		кон NextCh;

			(* proceed in the stream until the next non-whitespace occures *)
		проц SkipWhitespace;
		перем ch : симв8;
		нач
			ch := r.ПодглядиСимв8();
			нцПока IsWhitespace(ch) делай
				ch := NextCh(); ch := r.ПодглядиСимв8()
			кц
		кон SkipWhitespace;

			(** return next token. return NIL if end of stream is reached *)
		проц GetString() : String;
		перем ch : симв8;
			buf : массив Buffersize из симв8;
			i : цел32;
			s : String;
		нач
			buf[0] := NextCh(); (* must be '"' *)
			i := 1;
			ch := NextCh();
			нцПока ch # '"' делай
				buf[i] := ch;
				ch := NextCh();
				увел(i)
			кц;
			buf[i] := ch;
			нов(s, i+2);
			копируйСтрокуДо0(buf, s^);
			возврат s
		кон GetString;

		проц GetDelimiter() : String;
		перем s : String;
		нач
			нов(s, 2); s[0] := NextCh(); s[1] := 0X; возврат s
		кон GetDelimiter;

		(* return a string delimited by {, }, :, ;, SP, TAB, EOF, CR or LF *)
		проц GetToken() : String;
		перем ch : симв8; a : массив Buffersize из симв8;
			i : цел32; s : String;
		нач
			i := 0;
			ch := r.ПодглядиСимв8();
			нцПока (ch # '{') и (ch # '}') и (ch # ':') и (ch # ';') и  (ch # CR) и (ch # LF) и (ch # EOF) и (ch # SP) и (ch # TAB ) делай
				a[i] := NextCh();
				ch := r.ПодглядиСимв8();
				увел(i)
			кц;
			a[i] := 0X;
			нов(s, i+1);
			копируйСтрокуДо0(a, s^);
			возврат s
		кон GetToken;

		проц Get*() : String;
		перем ch : симв8;
		нач
			SkipWhitespace();
			oldpos := pos; oldcol := col;
			ch := r.ПодглядиСимв8();
			если ch = '"' то
				возврат GetString()
			аесли (ch = '{') или (ch = '}') или (ch = ':') или (ch = ';') то
				возврат GetDelimiter()
			аесли ch = EOF то
				возврат НУЛЬ (* end of file *)
			иначе
				возврат GetToken()
			всё;
		кон Get;

		проц IsWhitespace(ch : симв8) : булево;
		нач
			возврат (ch = SP) или (ch = TAB) или (ch = CR) или (ch = LF)
		кон IsWhitespace;

	кон Scanner;

	Parser *= окласс
	перем reportError*: ReportError;
		scanner : Scanner;
		components : ComponentList;
		warnings : булево;
		prefix : массив 128 из симв8; (* name of skinfile || "://" *)
		prefixLength : цел32;

			(** initialize with bsl-scanner *)
		проц & Init*(конст filename : массив из симв8; s : Scanner);
		перем i : цел32;
		нач
			scanner := s;
			reportError := DefaultReportError;
			нов(components);
			ReadConfiguration();
			warnings := истина;
			i := 0;
			нцПока filename[i] # 0X делай prefix[i] := filename[i]; увел(i) кц;
			prefix[i] := ':'; увел(i); prefix[i] := '/'; увел(i); prefix[i] := '/'; увел(i);
			prefix[i] := 0X; prefixLength := i
		кон Init;

			(** parse skin. if warnings is FALSE, only errors are shown *)
		проц Parse*(warnings : булево) : XML.Document;
		перем x : XML.Element;
			doc : XML.Document;
		нач
			сам.warnings := warnings;
			если FailToParse("skin") то возврат НУЛЬ всё;
			нов(doc);
			x := ParseSkin();
			если x = НУЛЬ то возврат НУЛЬ всё;
			doc.AddContent(x);
			возврат doc
		кон Parse;

		(* parse skin elements *)
		проц ParseSkin() : XML.Element;
		перем x, y : XML.Element;
		нач
			если FailToParse("{") то возврат НУЛЬ всё;
			нов(x);
			x.SetName("Skin");
				(* meta information *)
			если FailToParse("meta") то возврат НУЛЬ всё;
			если ParseMeta() = НУЛЬ то возврат НУЛЬ всё;
				(* window-style*)
			если FailToParse("window") то возврат НУЛЬ всё;
			y := ParseWindow();
			если y = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(y);
				(* cursors *)
			если FailToParse("cursor") то возврат НУЛЬ всё;
			y := ParseCursor();
			если y = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(y);
				(* component-style *)
			если FailToParse("component") то возврат НУЛЬ всё;
			y := ParseComponentSet();
			если y = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(y);
			если FailToParse("}") то возврат НУЛЬ всё;
			возврат x
		кон ParseSkin;

		(* parse cursor-style *)
		проц ParseCursor() : XML.Element;
		перем x, sub : XML.Element;
			c : Component;
			s : String;
		нач
			если FailToParse("{") то возврат НУЛЬ всё;
			нов(x);
			x.SetName("Cursors");
			s := scanner.Get();
			нцПока (s # НУЛЬ) и (s^ # "}") делай
				c := components.Find(s);
				если c = НУЛЬ то
					ErrorString(Append(NewString("Unknown cursor : "), s^));
					возврат НУЛЬ
				иначе
					sub := ParseComponent(c);
					если sub = НУЛЬ то возврат НУЛЬ всё;
					x.AddContent(sub)
				всё;
				s := scanner.Get()
			кц;
			если s = НУЛЬ то Error("Expected : }"); возврат НУЛЬ всё;
			возврат x
		кон ParseCursor;

		(* parse window-style *)
		проц ParseWindow() : XML.Element;
		перем x, sub : XML.Element;
			c : Component;
			useBitmaps : булево;
		нач
			если FailToParse("{") то возврат НУЛЬ всё;
			нов(x);
			x.SetName("Window");
				(* useBitmaps ? *)
			если FailToParse("useBitmaps") то возврат НУЛЬ всё;
			sub := ParseBooleanProperty(useBitmaps);
			если sub = НУЛЬ то возврат НУЛЬ всё;
			sub.SetName("UseBitmaps");
			x.AddContent(sub);
				(* title*)
			если FailToParse("title") то возврат НУЛЬ всё;
			c := components.Find(NewString("title"));
			утв(c # НУЛЬ);
			sub := ParseComponent(c);
			если sub = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(sub);
			если useBitmaps то
					(* top *)
				если FailToParse("top") то возврат НУЛЬ всё;
				c := components.Find(NewString("top"));
				утв(c # НУЛЬ);
				sub := ParseComponent(c);
				если sub = НУЛЬ то возврат НУЛЬ всё;
				x.AddContent(sub);
					(* bottom *)
				если FailToParse("bottom") то возврат НУЛЬ всё;
				c := components.Find(NewString("bottom"));
				утв(c # НУЛЬ);
				sub := ParseComponent(c);
				если sub = НУЛЬ то возврат НУЛЬ всё;
				x.AddContent(sub);
					(* left *)
				если FailToParse("left") то возврат НУЛЬ всё;
				c := components.Find(NewString("left"));
				утв(c # НУЛЬ);
				sub := ParseComponent(c);
				если sub = НУЛЬ то возврат НУЛЬ всё;
				x.AddContent(sub);
					(* right *)
				если FailToParse("right") то возврат НУЛЬ всё;
				c := components.Find(NewString("right"));
				утв(c # НУЛЬ);
				sub := ParseComponent(c);
				если sub = НУЛЬ то возврат НУЛЬ всё;
				x.AddContent(sub);
			иначе
				(* border *)
				если FailToParse("border") то возврат НУЛЬ всё;
				c := components.Find(NewString("border"));
				утв(c # НУЛЬ);
				sub := ParseComponent(c);
				если sub = НУЛЬ то возврат НУЛЬ всё;
				x.AddContent(sub);
			всё;
				(* desktop *)
			если FailToParse("desktop") то возврат НУЛЬ всё;
			c := components.Find(NewString("desktop"));
			утв(c # НУЛЬ);
			sub := ParseComponent(c);
			если sub = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(sub);
			если FailToParse("}") то возврат НУЛЬ всё;
			возврат x
		кон ParseWindow;

		(* parse meta-data of skin *)
		проц ParseMeta() : XML.Element;
		перем x, y : XML.Element;
		нач
			если FailToParse("{") то возврат НУЛЬ всё;
			нов(x); x.SetName("Meta");
			если FailToParse("name") то возврат НУЛЬ всё;
			y := ParseStringProperty();
			если y = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(y);
			если FailToParse("description") то возврат НУЛЬ всё;
			y := ParseStringProperty();
			если y = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(y);
			если FailToParse("author") то возврат НУЛЬ всё;
			y := ParseStringProperty();
			если y = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(y);
			если FailToParse("date") то возврат	НУЛЬ всё;
			y := ParseStringProperty();
			если y = НУЛЬ то возврат НУЛЬ всё;
			x.AddContent(y);
			если FailToParse("}") то возврат НУЛЬ всё;
			возврат x
		кон ParseMeta;

		(* parse set of components. return NIL in case of error. *)
		проц ParseComponentSet() : XML.Element;
		перем s : String;
			c : Component;
			x, sub : XML.Element;
			error : булево;
		нач
			error := ложь;
			если FailToParse("{") то возврат НУЛЬ всё;
			нов(x);
			x.SetName("Components");
			s := scanner.Get();
			нцПока (s # НУЛЬ) и (s^ # "}") делай
				c := components.Find(s);
				если c = НУЛЬ то
					ErrorString(Append(NewString("Unknown component : "), s^));
					error := истина;
					s := scanner.Get();
					если (s # НУЛЬ) и (s^ = "{") то
						если SkipUntilClosingBracket() = НУЛЬ то возврат НУЛЬ всё
					иначе
						возврат НУЛЬ
					всё
				иначе
					sub := ParseComponent(c);
					если sub = НУЛЬ то
						error := истина;
						если SkipUntilClosingBracket() = НУЛЬ то возврат НУЛЬ всё
					иначе
						x.AddContent(sub)
					всё
				всё;
				s := scanner.Get()
			кц;
			если s = НУЛЬ то Error("Expected : }"); возврат НУЛЬ всё;
			если ~error то возврат x иначе возврат НУЛЬ всё
		кон ParseComponentSet;

		(* parse component and it's respective properties *)
		проц ParseComponent(c : Component) : XML.Element;
		перем s : String;
			p : Property;
			x, sub : XML.Element;
		нач
			если FailToParse("{") то возврат НУЛЬ всё;
			нов(x);
			c.name^[0] := ASCII_вЗаглавную(c.name^[0]);
			x.SetNameAsString(c.name);
				(* parse properties *)
			s := scanner.Get();
			нцПока (s # НУЛЬ) и (s^ # "}") делай
				p := c.properties.Find(s);
				если p = НУЛЬ то
					ErrorString(Append(NewString("Unknown property : "), s^));
					возврат НУЛЬ;
				всё;
				sub := ParseProperty(p);
				если sub = НУЛЬ то возврат НУЛЬ всё;
				x.AddContent(sub);
				s := scanner.Get()
			кц;
			если s = НУЛЬ то Error("Expected '}'"); возврат НУЛЬ всё;
			если ~c.MandatoryPropertiesSucceeded(ErrorString) то возврат НУЛЬ всё;
			возврат x
		кон ParseComponent;

		(* parse property according to type-information given in p. return NIL in case of failure *)
		проц ParseProperty(p : Property) : XML.Element;
		перем x : XML.Element;
			dummy : булево;
		нач
			если p.type^ = "color" то 		x := ParseColorProperty()
			аесли p.type^ = "rectangle" то x := ParseRectangleProperty()
			аесли p.type^ = "int32" то 	x := ParseInt32Property()
			аесли p.type^ = "boolean" то 	x := ParseBooleanProperty(dummy)
			аесли p.type^ = "string" то 	x := ParseStringProperty()
			аесли p.type^ = "resource" то x := ParseResourceProperty(сам.warnings)
			всё;
			если x = НУЛЬ то возврат НУЛЬ всё;
			p.succeeded := истина;
			p.name^[0] := ASCII_вЗаглавную(p.name^[0]);
			x.SetNameAsString(p.name);
			возврат x
		кон ParseProperty;

		(* parse rectangular property. return NIL if fail *)
		проц ParseRectangleProperty() : XML.Element;
		перем prop, sub : XML.Element;
			ac : XML.ArrayChars;
			s : String;
		нач
			если FailToParse("{") то возврат НУЛЬ всё;
				(* height *)
			если FailToParse("height") то возврат НУЛЬ всё;
			если FailToParse(":") то возврат НУЛЬ всё;
			s := scanner.Get();
			если NoNumber(s) то возврат НУЛЬ всё;
			нов(prop);
			нов(sub);
			prop.AddContent(sub);
			sub.SetName("Height");
			нов(ac);
			sub.AddContent(ac);
			ac.SetStr(s^);
			если FailToParse(";") то возврат НУЛЬ всё;
				(* width *)
			если FailToParse("width") то возврат НУЛЬ всё;
			если FailToParse(":") то возврат НУЛЬ всё;
			s := scanner.Get();
			если NoNumber(s) то возврат НУЛЬ всё;
			нов(sub);
			prop.AddContent(sub);
			sub.SetName("Width");
			нов(ac);
			sub.AddContent(ac);
			ac.SetStr(s^);
			если FailToParse(";") то возврат НУЛЬ всё;
			если FailToParse("}") то возврат НУЛЬ всё;
			возврат prop
		кон ParseRectangleProperty;

		(* parse color-property. return NIL if fail *)
		проц ParseColorProperty() : XML.Element;
		нач
			возврат ParseInt32Property()
		кон ParseColorProperty;

		(* parse integer-property. return NIL if fail *)
		проц ParseInt32Property() : XML.Element;
		перем prop : XML.Element;
			ac : XML.ArrayChars;
			s : String;
		нач
			нов(prop);
			если FailToParse(":") то возврат НУЛЬ всё;
			s := scanner.Get();
			если NoNumber(s) то возврат НУЛЬ всё;
			нов(ac);
			ac.SetStr(s^);
			если FailToParse(";") то возврат НУЛЬ всё;
			prop.AddContent(ac);
			возврат prop
		кон ParseInt32Property;

		(* when successful parsed : RETURN xml-Element, bool := parsed value
			when parsing not possible : RETURN NIL, bool undefined *)
		проц ParseBooleanProperty(перем bool : булево) : XML.Element;
		перем prop : XML.Element;
			ac : XML.ArrayChars;
			s : String;
		нач
			нов(prop);
			если FailToParse(":") то возврат НУЛЬ всё;
			s := scanner.Get();
			если NoBoolean(s) то возврат НУЛЬ всё;
			bool := (s^ = "true");
			нов(ac);
			ac.SetStr(s^);
			если FailToParse(";") то возврат НУЛЬ всё;
			prop.AddContent(ac);
			возврат prop
		кон ParseBooleanProperty;

		(* parse string-property *)
		проц ParseStringProperty() : XML.Element;
		перем prop : XML.Element; ac : XML.ArrayChars; s, t : String; i : размерМЗ;
		нач
			нов(prop);
			если FailToParse(":") то возврат НУЛЬ всё;
			s := scanner.Get();
			если NoString(s) то возврат НУЛЬ всё;
			нов(t, длинаМассива(s^)-2);
			нцДля i := 1 до длинаМассива(s^)-3 делай t^[i-1] := s^[i] кц;
			t[длинаМассива(s^)-3] := 0X;
			нов(ac);
			ac.SetStr(t^);
			если FailToParse(";") то возврат НУЛЬ всё;
			prop.AddContent(ac);
			возврат prop
		кон ParseStringProperty;

		(* parse name of a resource. check availability of resource if check is TRUE. return NIL if fail *)
		проц ParseResourceProperty(check : булево) : XML.Element;
		перем prop : XML.Element; ac : XML.ArrayChars; s, t : String; i : размерМЗ;
		нач
			нов(prop);
			если FailToParse(":") то возврат НУЛЬ всё;
			s := scanner.Get();
			если NoString(s) то возврат НУЛЬ всё;
			нов(t, длинаМассива(s^)-2+prefixLength);
			копируйСтрокуДо0(prefix, t^);
			нцДля i := 1 до длинаМассива(s^)-3 делай t^[i-1+prefixLength] := s^[i] кц;
			t[длинаМассива(s^)+prefixLength-3] := 0X;
			нов(ac);
			ac.SetStr(t^);
			если FailToParse(";") то возврат НУЛЬ всё;
			prop.AddContent(ac);
			если check то CheckImage(t^) всё; (* is parsed string name of a loadable image ? *)
			возврат prop
		кон ParseResourceProperty;

		(* read SkinLanguage-configuration from the config-file *)
		проц ReadConfiguration;
		перем scanner : XMLScanner.Scanner; parser : XMLParser.Parser;
			doc : XML.Document; el : XML.Element; en : XMLObjects.Enumerator;
			file : Files.File; r : Files.Reader; p : динамическиТипизированныйУкль; s : String;
		нач
			file := Files.Old(ConfigFileName);
			если file = НУЛЬ то ЛогЯдра.пСтроку8(ConfigFileName); ЛогЯдра.пСтроку8(" not found"); ЛогЯдра.пВК_ПС; возврат всё;
			Files.OpenReader(r, file, 0);
			нов(scanner, r);
			нов(parser, scanner);
			doc := parser.Parse();
			el := doc.GetRoot();
			en := el.GetContents();
			en.Reset();
			нцПока en.HasMoreElements() делай
				p := en.GetNext();
				если p суть XML.Element то
					el := p(XML.Element);
					s := el.GetName();
					если s^ = "component" то components.Add(el) всё
				всё
			кц;
		кон ReadConfiguration;

		проц CheckImage(конст name : массив из симв8);
		нач
			если WMGraphics.LoadImage(name, истина) = НУЛЬ то
				ErrorString(Append(NewString("Warning, no valid image : "), name))
			всё
		кон CheckImage;

		проц FailToParse(конст a : массив из симв8) : булево;
		перем s : String;
		нач
			s := scanner.Get();
			если (s = НУЛЬ) или (s^ # a) то
				ErrorString(Append(Append(NewString("Failed to parse, expected '"), a), "'"));
				возврат истина
			иначе
				возврат ложь
			всё
		кон FailToParse;

		(* return TRUE if s is not a valid number *)
		проц NoNumber(s : String) : булево;
		перем i : цел32;
		нач
			i := кодСимв8(s^[0]);
			если (i >= кодСимв8('0')) и (i <= кодСимв8('9')) то
				возврат ложь
			иначе
				Error("Not a valid number (must begin with digit).");
				возврат истина
			всё
		кон NoNumber;

		(* return FALSE if LOW(s) equals either 'true' or 'false'. s := LOW(s) *)
		проц NoBoolean(s : String) : булево;
		нач
			LowerCase(s);
			если (s^ # "true") и (s^ # "false") то
				Error("Expected 'true' or 'false'.");
				возврат истина
			иначе
				возврат ложь
			всё
		кон NoBoolean;

		(* return TRUE if s is not a valid string that starts and ends with an " *)
		проц NoString(s : String) : булево;
		нач
			если (s = НУЛЬ) или (s^[0] # '"') или (s^[длинаМассива(s^)-2] # '"') то
				Error("Expected a string (encapsulated with '').");
				возврат истина
			иначе
				возврат ложь
			всё
		кон NoString;

		(* return the next '}' that hasn't been preceeded by a corresponding '{'. return NIL at the end of the stream *)
		проц SkipUntilClosingBracket() : String;
		перем s : String;
		нач
			s := scanner.Get();
			нцПока s # НУЛЬ делай
				если s^ = "{" то
					s := SkipUntilClosingBracket();
					если s = НУЛЬ то возврат НУЛЬ всё
				аесли s^ = "}" то
					возврат s
				всё;
				s := scanner.Get()
			кц;
			возврат НУЛЬ
		кон SkipUntilClosingBracket;

		проц Error(конст msg : массив из симв8);
		нач
			reportError(scanner.oldpos, scanner.line, scanner.col, NewString(msg));
		кон Error;

		проц ErrorString(msg :String);
		нач
			reportError(scanner.oldpos, scanner.line, scanner.oldcol, msg);
		кон ErrorString;

	кон Parser;

проц DefaultReportError(pos, line, col: цел32; msg: String);
нач
	ЛогЯдра.пСтроку8("ERROR : "); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("  position : "); ЛогЯдра.пЦел64(pos, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("  line : "); ЛогЯдра.пЦел64(line, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("  column : "); ЛогЯдра.пЦел64(col, 0); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("  message : "); ЛогЯдра.пСтроку8(msg^); ЛогЯдра.пВК_ПС
кон DefaultReportError;

(* ---- helpers ---------------------------------------------------------------*)

проц NewString(конст x : массив из симв8) : String;
перем t : String;
нач
	нов(t, длинаМассива(x)); копируйСтрокуДо0(x, t^); возврат t
кон NewString;

(* result := a || b *)
проц Append(a : String; конст b : массив из симв8) : String;
перем s : String;
	i, al, bl : размерМЗ;
нач
	al := длинаМассива(a^); bl := длинаМассива(b);
	нов(s, al+bl-1);
	нцДля i := 0 до al-1 делай s^[i] := a^[i] кц;
	нцДля i := 0 до bl-1 делай s^[al+i-1] := b[i] кц;
	возврат s
кон Append;

проц LowerCase(s : String);
перем i : размерМЗ;
нач
	нцДля i := 0 до длинаМассива(s^)-1 делай s^[i] := Strings.LOW(s^[i]) кц
кон LowerCase;

кон SkinLanguage.

