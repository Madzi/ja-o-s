модуль HTMLParser;	(** AUTHOR "Simon L. Keel" (heavily based on swalthert's "XMLParser"); PURPOSE "HTML parser"; *)

использует
	Strings, ЛогЯдра, DynamicStrings, Scanner := HTMLScanner, XML;

тип
	String = Strings.String;

	Node = укль на запись
		name : String;
		back : Node;
	кон;


	OpenTagStack= окласс
		перем
			top : Node;

		проц &Init*;
		нач
			top := НУЛЬ;
		кон Init;

		проц Insert(s: String);
		перем
			node : Node;
		нач
			нов(node);
			node.name := s;
			node.back := top;
			top := node;
		кон Insert;

		проц Remove(s: String);
		перем
			node, old : Node;
		нач
			old := НУЛЬ;
			node := top;
			нцПока (node#НУЛЬ) и (s^ # node.name^) делай
				old := node;
				node := node.back;
			кц;
			если node#НУЛЬ то
				если old=НУЛЬ то
					top := node.back;
				иначе
					old.back := node.back;
				всё;
			всё;
		кон Remove;

		проц IsMember(s: String): булево;
		перем
			node : Node;
		нач
			node := top;
			нцПока (node#НУЛЬ) и (s^ # node.name^) делай
				node := node.back;
			кц;
			возврат node#НУЛЬ;
		кон IsMember;

		(** For debugging pupose only. **)
		проц Print;
		перем
			node : Node;
		нач
			node := top;
			нцПока (node#НУЛЬ) делай
				ЛогЯдра.пСтроку8(node.name^); ЛогЯдра.пВК_ПС();
				node := node.back;
			кц;
			ЛогЯдра.пСтроку8("----------"); ЛогЯдра.пВК_ПС();
		кон Print;

	кон OpenTagStack;


	Parser* = окласс
		перем
			scanner: Scanner.Scanner;
			openTagStack : OpenTagStack;
			elemReg*: XML.ElementRegistry;
			reportError*: проц {делегат} (pos, line, row: цел32; msg: массив из симв8);
			closedTag : String;
			newTagName : String;
			closedTagPremature : булево;

		проц &Init*(s: Scanner.Scanner);
		нач
			reportError := DefaultReportError;
			scanner := s;
			нов(openTagStack);
		кон Init;

		проц Error(msg: массив из симв8);
		нач
			reportError(scanner.GetPos(), scanner.line, scanner.col, msg)
		кон Error;

		проц CheckSymbol(expectedSymbols: мнвоНаБитахМЗ; errormsg: массив из симв8): булево;
		нач
			если ~(scanner.sym в expectedSymbols) то
				Error(errormsg); возврат ложь
			иначе
				возврат истина
			всё
		кон CheckSymbol;

		проц Parse*(): XML.Document;
		перем
			doc: XML.Document;
			decl: XML.XMLDecl;
			dtd, newDtd: XML.DocTypeDecl;
			e : XML.Element;
			s: String;
			ds: DynamicStrings.DynamicString;
			msg: массив 21 из симв8;
		нач
			нов(doc);
			dtd := doc.GetDocTypeDecl();

			нцПока истина делай
				scanner.ScanContent();
				просей scanner.sym из
				(* <?xml *)
				| Scanner.TagXMLDeclOpen:
					decl := ParseXMLDecl();
					если decl#НУЛЬ то doc.AddContent(decl) всё;
				(* <!-- ... --> *)
				| Scanner.Comment: doc.AddContent(ParseComment())
				(* <!... *)
				| Scanner.TagDeclOpen:
					s := scanner.GetStr();
					Strings.UpperCase(s^);
					если s^ = 'DOCTYPE' то
						newDtd := ParseDocTypeDecl();
						если dtd=НУЛЬ то
							если newDtd#НУЛЬ то
								dtd := newDtd;
								doc.AddContent(dtd);
							всё;
						всё;
					иначе
						нов(ds);
						msg := "ignoring '<"; ds.Append(msg); ds.Append(s^); ds.Append(msg);
						s := ds.ToArrOfChar();
						Error(s^);
					всё;
				(* < *)
				| Scanner.TagElemStartOpen:
					ParseStartTagName();
					e := ParseElement();
					если e # НУЛЬ то doc.AddContent(e) всё;
				(* char data *)
				| Scanner.CharData: doc.AddContent(ParseCharData())
				(* </ *)
				| Scanner.TagElemEndOpen:
					(* ignore *)
					s := ParseEndTag();
				(* EOF *)
				| Scanner.Eof: возврат doc
				иначе
					Error("unknown content");
				всё;
			кц;
		кон Parse;

		проц ParseXMLDecl(): XML.XMLDecl;
		перем
			decl: XML.XMLDecl;
			s: String;
		нач
			нов(decl);
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "<?xml>: 'version' expected") то возврат НУЛЬ всё;
			s := scanner.GetStr();
			если s^ # "version" то Error("<?xml>: 'version' expected"); возврат НУЛЬ всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Equal}, "<?xml>: '=' expected") то возврат НУЛЬ всё;
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Literal}, "<?xml>: Version Number expected") то возврат НУЛЬ всё;
			s := scanner.GetStr();
			если s=НУЛЬ то s:=Strings.NewString(""); всё;
			decl.SetVersion(s^);
			scanner.ScanMarkup();
			s := scanner.GetStr();
			Strings.LowerCase(s^);
			если (scanner.sym = Scanner.Name) и (s^ = "encoding") то
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Equal}, "<?xml>: encoding: '=' expected") то возврат decl всё;
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Literal}, "<?xml>: Encoding Name expected") то возврат decl всё;
				s := scanner.GetStr();
				если s=НУЛЬ то s:=Strings.NewString(""); всё;
				decl.SetEncoding(s^);
				scanner.ScanMarkup();
				 s := scanner.GetStr();
				 Strings.LowerCase(s^);
			всё;
			если (scanner.sym = Scanner.Name) и (s^ = "standalone") то
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Equal}, "<?xml>: standalone: '=' expected") то возврат decl всё;
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.Literal}, '<?xml>: standalone: "yes" or "no" expected') то возврат decl всё;
				s := scanner.GetStr();
				Strings.LowerCase(s^);
				если s^ = "yes" то decl.SetStandalone(истина)
				аесли s^ = "no" то decl.SetStandalone(ложь)
				иначе Error('<?xml>: standalone: "yes" or "no" expected'); возврат decl
				всё;
				scanner.ScanMarkup()
			всё;
			нцПока (scanner.sym#Scanner.TagPIClose) и (scanner.sym#Scanner.Eof) делай
				scanner.ScanMarkup();
			кц;
			если scanner.sym=Scanner.Eof то Error("<?xml>: '?>' expected") всё;
			возврат decl
		кон ParseXMLDecl;

		проц ParseComment(): XML.Comment;
		перем comment: XML.Comment; s: String;
		нач
			нов(comment);
			s := scanner.GetStr();
			comment.SetStr(s^);
			возврат comment
		кон ParseComment;

		проц ParseDocTypeDecl(): XML.DocTypeDecl;
		перем
			dtd: XML.DocTypeDecl;
			externalSubset: XML.EntityDecl;
			s: String;
		нач
			нов(dtd);
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "<!DOCTYPE: DTD name expected") то возврат НУЛЬ всё;
			s := scanner.GetStr();
			dtd.SetName(s^);
			scanner.ScanMarkup();
			если scanner.sym = Scanner.Name то	(* DTD points to external subset *)
				нов(externalSubset);
				s := scanner.GetStr();
				Strings.UpperCase(s^);
				если s^ = 'SYSTEM' то
					scanner.ScanMarkup();
					s := ParseSystemLiteral();
					если s=НУЛЬ то s:=Strings.NewString(""); всё;
					externalSubset.SetSystemId(s^);
					scanner.ScanMarkup();
				аесли s^ = 'PUBLIC' то
					scanner.ScanMarkup();
					s := ParsePubidLiteral();
					если s=НУЛЬ то s:=Strings.NewString(""); всё;
					externalSubset.SetPublicId(s^);
					scanner.ScanMarkup();
					если scanner.sym=Scanner.Literal то
						s := ParseSystemLiteral();
						если s=НУЛЬ то s:=Strings.NewString(""); всё;
						externalSubset.SetSystemId(s^);
						scanner.ScanMarkup();
					иначе
						s:=Strings.NewString("");
						externalSubset.SetSystemId(s^);
					всё;
				иначе
					Error("<!DOCTYPE>:'SYSTEM' or 'PUBLIC' expected");
					возврат НУЛЬ;
				всё;
				dtd.SetExternalSubset(externalSubset);
			всё;
			нцПока (scanner.sym#Scanner.TagClose) и (scanner.sym#Scanner.Eof) делай
				scanner.ScanMarkup();
			кц;
			если scanner.sym=Scanner.Eof то Error("<!DOCTYPE>: '>' expected") всё;
			возврат dtd;
		кон ParseDocTypeDecl;

		проц ParseSystemLiteral(): String;
		перем systemLiteral: String;
		нач
			если ~CheckSymbol({Scanner.Literal}, "System Literal expected") то возврат НУЛЬ всё;
			systemLiteral := scanner.GetStr();
			возврат systemLiteral
		кон ParseSystemLiteral;

		проц ParsePubidLiteral(): String;
		перем pubidLiteral: String;
		нач
			если ~CheckSymbol({Scanner.Literal}, "PubidLiteral expected") то возврат НУЛЬ всё;
			pubidLiteral := scanner.GetStr();
			если ~IsPubidLiteral(pubidLiteral^) то Error("not a correct Pubid Literal"); возврат НУЛЬ всё;
			возврат pubidLiteral
		кон ParsePubidLiteral;

		проц ParseCharData(): XML.ArrayChars;
		перем
			cd: XML.ArrayChars;
			s: String;
		нач
			нов(cd);
			s := scanner.GetStr();
			cd.SetStr(s^);
			возврат cd
		кон ParseCharData;

		проц ParseElement(): XML.Element;
		перем
			e: XML.Element;
			empty: булево;
			name, s: String;
(*			ds: DynamicStrings.DynamicString;
			msg: ARRAY 21 OF CHAR; *)
		нач
			ParseStartTag(e, empty);
			если e = НУЛЬ то возврат НУЛЬ всё;
			если empty то
				openTagStack.Remove(e.GetName());
				возврат e;
			всё;
			name := e.GetName();
			если name^ = "SCRIPT" то
				scanner.ScanSCRIPT();
				e.AddContent(ParseComment());
				возврат e;
			всё;
			если name^ = "STYLE" то
				scanner.ScanSTYLE();
				e.AddContent(ParseComment());
				возврат e;
			всё;
			нцПока истина делай
				scanner.ScanContent();
				просей scanner.sym из
				| Scanner.CharData: e.AddContent(ParseCharData())
				| Scanner.TagElemStartOpen:
					ParseStartTagName();
					нцДо
						если PrematureTagClosing(name, newTagName) то
							closedTagPremature := истина;
							openTagStack.Remove(name);
						(*	NEW(ds);
							msg := "closing '<"; ds.Append(msg); ds.Append(name^);
							msg := ">' before opening '<"; ds.Append(msg); ds.Append(newTagName^);
							msg := ">'"; ds.Append(msg); s := ds.ToArrOfChar();
							Error(s^);	*)
							возврат e;
						всё;
						closedTagPremature := ложь;
						e.AddContent(ParseElement());
						если closedTag#НУЛЬ то
							если closedTag^=name^ то
								openTagStack.Remove(name);
								closedTag := НУЛЬ;
							всё;
							возврат e;
						всё;
					кцПри ~closedTagPremature;
				| Scanner.Comment: e.AddContent(ParseComment())
				| Scanner.TagPIOpen:
					нцПока (scanner.sym#Scanner.TagClose) и (scanner.sym#Scanner.Eof) делай
						scanner.ScanMarkup();
					кц;
					если scanner.sym=Scanner.Eof то Error("'>' expected") всё;
				| Scanner.TagElemEndOpen:
					s := ParseEndTag();
					если s#НУЛЬ то
						openTagStack.Remove(name);
						если s^=name^ то
							closedTag := НУЛЬ;
						иначе
							closedTag := s;
						всё;
						возврат e;
					всё;
				| Scanner.Eof: Error("element not closed"); возврат e
				иначе
					Error("unknown Element Content");
				всё;
			кц;
		кон ParseElement;

		проц ParseStartTagName;
		нач
			scanner.ScanMarkup();
			если ~CheckSymbol({Scanner.Name}, "Element Name expected") то
				newTagName := Strings.NewString("");
				возврат
			всё;
			newTagName := scanner.GetStr();
			Strings.UpperCase(newTagName^);
		кон ParseStartTagName;

		проц ParseStartTag(перем e: XML.Element; перем empty: булево);
		перем s: String;
		нач
			s := newTagName;
			если elemReg # НУЛЬ то
				e := elemReg.InstantiateElement(s^)
			всё;
			если e = НУЛЬ то нов(e) всё;
			e.SetName(s^);
			openTagStack.Insert(s);
			scanner.ScanMarkup();
			нцПока scanner.sym = Scanner.Name делай
				e.AddAttribute(ParseAttribute());
			кц;
			если ~CheckSymbol({Scanner.TagEmptyElemClose, Scanner.TagClose}, "'/>' or '>' expected") то возврат всё;
			если scanner.sym = Scanner.TagEmptyElemClose то
				empty := истина
			аесли scanner.sym = Scanner.TagClose то
				если IsSolitaryTag(e.GetName()) то
					empty := истина;
				иначе
					empty := ложь;
				всё;
			всё
		кон ParseStartTag;

		проц ParseAttribute(): XML.Attribute;
		перем a: XML.Attribute; s: String;
		нач
			нов(a);
			s := scanner.GetStr();
			a.SetName(s^);
			scanner.ScanMarkup();
			если scanner.sym=Scanner.Equal то
				scanner.ScanAttributeValue();
				если ~CheckSymbol({Scanner.Literal}, "Attribute Value expected") то возврат a всё;
				s := scanner.GetStr();
				a.SetValue(s^);
				scanner.ScanMarkup();
			иначе
				s:=Strings.NewString("");
				a.SetValue(s^);
			всё;
			возврат a
		кон ParseAttribute;

		проц ParseEndTag():String;
		перем ds: DynamicStrings.DynamicString; s: String; msg: массив 14 из симв8;
		нач
			scanner.ScanMarkup();
			s := scanner.GetStr();
			Strings.UpperCase(s^);
			если (scanner.sym = Scanner.Name) то
				scanner.ScanMarkup();
				если ~CheckSymbol({Scanner.TagClose}, "'>' expected") то возврат НУЛЬ; всё;
				если openTagStack.IsMember(s) то
					возврат s;
				иначе
					нов(ds);
					msg := "ignoring '</"; ds.Append(msg); ds.Append(s^);
					msg := ">'"; ds.Append(msg); s := ds.ToArrOfChar();
					Error(s^);
					возврат НУЛЬ;
				всё;
			иначе
				нов(ds);
				msg := "ignoring '</"; ds.Append(msg); ds.Append(s^);
				s := ds.ToArrOfChar();
				Error(s^);
				возврат НУЛЬ;
			всё
		кон ParseEndTag;

	кон Parser;


	проц IsPubidLiteral(конст str: массив из симв8): булево;
	перем i, len: размерМЗ; ch: симв8;
	нач
		i := 0; len := длинаМассива(str); ch := str[0];
		нцДо
			ch := str[i]; увел(i)
		кцПри ((ch # 20X) и (ch # 0DX) и (ch # 0AX) и ((ch < 'a') или ('z' < ch)) и ((ch < 'A') и ('Z' < ch))
				и ((ch < '0') и ('9' < ch)) и (ch # '(') и (ch # ')') и (ch # '+') и (ch # ',') и (ch # '.')
				и (ch # '/') и (ch # ':') и (ch # '=') и (ch # '?') и (ch # ';') и (ch # '!') и (ch # '*') и (ch # '#')
				и (ch # '@') и (ch # '$') и (ch # '_') и (ch # '%')) или (i >= len);
		возврат i = len
	кон IsPubidLiteral;

	проц DefaultReportError(pos, line, col: цел32; msg: массив из симв8);
	нач
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСтроку8("pos "); ЛогЯдра.пЦел64(pos, 6);
		ЛогЯдра.пСтроку8(", line "); ЛогЯдра.пЦел64(line, 0); ЛогЯдра.пСтроку8(", column "); ЛогЯдра.пЦел64(col, 0);
		ЛогЯдра.пСтроку8("    "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	кон DefaultReportError;

	проц IsSolitaryTag(name: String): булево;
	нач
		если name^ = "AREA" то возврат истина всё;
		если name^ = "BASE" то возврат истина всё;
		если name^ = "BASEFONT" то возврат истина всё;
		если name^ = "BR" то возврат истина всё;
		если name^ = "COL" то возврат истина всё;
		если name^ = "FRAME" то возврат истина всё;
		если name^ = "HR" то возврат истина всё;
		если name^ = "IMG" то возврат истина всё;
		если name^ = "INPUT" то возврат истина всё;
		если name^ = "ISINDEX" то возврат истина всё;
		если name^ = "LINK" то возврат истина всё;
		если name^ = "META" то возврат истина всё;
		если name^ = "PARAM" то возврат истина всё;
		возврат ложь
	кон IsSolitaryTag;

	проц PrematureTagClosing(name, next: String): булево;
	нач
		если name^ = "COLGROUP" то
			если next^ # "COL" то возврат истина всё;
		аесли name^ = "DD" то
			если (next^ = "DD") или (next^ = "DT") или (next^ = "DL") то возврат истина всё;
		аесли name^ = "DT" то
			если (next^ = "DT") или (next^ = "DD") или (next^ = "DL") то возврат истина всё;
		аесли name^ = "HEAD" то
			если next^ = "BODY" то возврат истина всё;
		аесли name^ = "LI" то
			если next^ = "LI" то возврат истина всё;
		аесли name^ = "OPTION" то
			возврат истина;
		аесли name^ = "P" то
			если next^ = "P" то возврат истина всё;
		аесли name^ = "TBODY" то
			если (next^ = "TBODY") или (next^ = "THEAD") или (next^ = "TFOOT") то возврат истина всё;
		аесли name^ = "TD" то
			если (next^ = "TD") или (next^ = "TH") или (next^ = "TR") или (next^ = "THEAD") или (next^ = "TBODY") или (next^ = "TFOOT") то возврат истина всё;
		аесли name^ = "TFOOT" то
			если (next^ = "TBODY") или (next^ = "THEAD") или (next^ = "TFOOT") то возврат истина всё;
		аесли name^ = "TH" то
			если (next^ = "TD") или (next^ = "TH") или (next^ = "TR") или (next^ = "THEAD") или (next^ = "TBODY") или (next^ = "TFOOT") то возврат истина всё;
		аесли name^ = "THEAD" то
			если (next^ = "TBODY") или (next^ = "THEAD") или (next^ = "TFOOT") то возврат истина всё;
		аесли name^ = "TR" то
			если (next^ = "TR")  или (next^ = "THEAD") или (next^ = "TBODY") или (next^ = "TFOOT") то возврат истина всё;
		всё;
		возврат ложь

		(* The following code is html-standard. but it's too strict to get good results!
		IF name^ = "COLGROUP" THEN
			IF next^ # "COL" THEN RETURN TRUE END;
		ELSIF name^ = "DD" THEN
			IF ~IsFlow(next) THEN RETURN TRUE END;
		ELSIF name^ = "DT" THEN
			IF ~IsInline(next) THEN RETURN TRUE END;
		ELSIF name^ = "HEAD" THEN
			IF next^ = "BODY" THEN RETURN TRUE END;
		ELSIF name^ = "LI" THEN
			IF ~IsFlow(next) THEN RETURN TRUE END;
		ELSIF name^ = "OPTION" THEN
			RETURN TRUE;
		ELSIF name^ = "P" THEN
			IF ~IsInline(next) THEN RETURN TRUE END;
		ELSIF name^ = "TBODY" THEN
			IF next^ # "TR" THEN RETURN TRUE END;
		ELSIF name^ = "TD" THEN
			IF ~IsFlow(next) THEN RETURN TRUE END;
		ELSIF name^ = "TFOOT" THEN
			IF next^ # "TR" THEN RETURN TRUE END;
		ELSIF name^ = "TH" THEN
			IF ~IsFlow(next) THEN RETURN TRUE END;
		ELSIF name^ = "THEAD" THEN
			IF next^ # "TR" THEN RETURN TRUE END;
		ELSIF name^ = "TR" THEN
			IF (next^ # "TH") & (next^ # "TD") THEN RETURN TRUE END;
		END;
		RETURN FALSE
		*)
	кон PrematureTagClosing;

(*
	PROCEDURE IsFlow(name: String): BOOLEAN;
	BEGIN
		IF IsInline(name) THEN RETURN TRUE END;
		IF name^ = "P" THEN RETURN TRUE END;
		IF name^ = "H1" THEN RETURN TRUE END;
		IF name^ = "H2" THEN RETURN TRUE END;
		IF name^ = "H3" THEN RETURN TRUE END;
		IF name^ = "H4" THEN RETURN TRUE END;
		IF name^ = "H5" THEN RETURN TRUE END;
		IF name^ = "H6" THEN RETURN TRUE END;
		IF name^ = "UL" THEN RETURN TRUE END;
		IF name^ = "OL" THEN RETURN TRUE END;
		IF name^ = "PRE" THEN RETURN TRUE END;
		IF name^ = "DL" THEN RETURN TRUE END;
		IF name^ = "DIV" THEN RETURN TRUE END;
		IF name^ = "NOSCRIPT" THEN RETURN TRUE END;
		IF name^ = "BLOCKQUOTE" THEN RETURN TRUE END;
		IF name^ = "FORM" THEN RETURN TRUE END;
		IF name^ = "HR" THEN RETURN TRUE END;
		IF name^ = "TABLE" THEN RETURN TRUE END;
		IF name^ = "FIELDSET" THEN RETURN TRUE END;
		IF name^ = "ADDRESS" THEN RETURN TRUE END;
		RETURN FALSE
	END IsFlow;

	PROCEDURE IsInline(name: String): BOOLEAN;
	BEGIN
		IF name^ = "TT" THEN RETURN TRUE END;
		IF name^ = "I" THEN RETURN TRUE END;
		IF name^ = "B" THEN RETURN TRUE END;
		IF name^ = "BIG" THEN RETURN TRUE END;
		IF name^ = "SMALL" THEN RETURN TRUE END;
		IF name^ = "EM" THEN RETURN TRUE END;
		IF name^ = "STRONG" THEN RETURN TRUE END;
		IF name^ = "DFN" THEN RETURN TRUE END;
		IF name^ = "CODE" THEN RETURN TRUE END;
		IF name^ = "SAMP" THEN RETURN TRUE END;
		IF name^ = "KBD" THEN RETURN TRUE END;
		IF name^ = "VAR" THEN RETURN TRUE END;
		IF name^ = "CITE" THEN RETURN TRUE END;
		IF name^ = "ABBR" THEN RETURN TRUE END;
		IF name^ = "ACRONYM" THEN RETURN TRUE END;
		IF name^ = "A" THEN RETURN TRUE END;
		IF name^ = "IMG" THEN RETURN TRUE END;
		IF name^ = "OBJECT" THEN RETURN TRUE END;
		IF name^ = "BR" THEN RETURN TRUE END;
		IF name^ = "SCRIPT" THEN RETURN TRUE END;
		IF name^ = "MAP" THEN RETURN TRUE END;
		IF name^ = "Q" THEN RETURN TRUE END;
		IF name^ = "SUB" THEN RETURN TRUE END;
		IF name^ = "SUP" THEN RETURN TRUE END;
		IF name^ = "SPAN" THEN RETURN TRUE END;
		IF name^ = "BDO" THEN RETURN TRUE END;
		IF name^ = "INPUT" THEN RETURN TRUE END;
		IF name^ = "SELECT" THEN RETURN TRUE END;
		IF name^ = "TEXTAREA" THEN RETURN TRUE END;
		IF name^ = "LABEL" THEN RETURN TRUE END;
		IF name^ = "BUTTON" THEN RETURN TRUE END;
		RETURN FALSE
	END IsInline;
*)

кон HTMLParser.
