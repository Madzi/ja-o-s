модуль TestLisScanner;
(* (c) fof ETH Zürich, 2009 *)
(* LisCompiler.Compile --английскиеИмена --переведиНаРусский TestScanner.Mod *)
использует Потоки, Commands, Scanner := LisScanner, TextUtilities;


	(** debugging / reporting **)
	проц ReportKeywords*(context: Commands.Context);
	перем s: Scanner.ВидЛексемы; name: Scanner.КлючевоеСлово;
	нач
		нцДля s := 0 до Scanner.КонецТекста делай
			context.out.пЦел64(s,1); context.out.пСтроку8(": ");
			context.out.пСимв8('"');
			Scanner.keywordsLower.ДайСтрокуПоВидуЛексемы(s,name);
			context.out.пСтроку8(name);
			context.out.пСимв8('"');
			context.out.пСтроку8(", ");
			context.out.пСимв8('"');
			Scanner.keywordsUpper.ДайСтрокуПоВидуЛексемы(s,name);
			context.out.пСтроку8(name);
			context.out.пСимв8('"');
			context.out.пВК_ПС;
		кц;
	кон ReportKeywords;

	
	проц TestScanner*(context: Commands.Context);
	перем filename: массив 256 из симв8; reader: Потоки.Чтец; scanner: Scanner.Лексер;
		token: Scanner.Лексема;
	нач
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
		reader := TextUtilities.GetTextReader(filename);
		scanner := Scanner.NewScanner(filename,reader,0,НУЛЬ);
		нцДо
			если scanner.ДочитайЛексему(token) то
				Scanner.PrintToken(context.out,token,Scanner.ВариантыПеревода.Русский);

			иначе
				context.out.пСтроку8("GetNextToken returned FALSE");
			всё;
			context.out.пВК_ПС;
		кцПри scanner.былаЛиОшибкаРазбора или (token.видЛексемы=Scanner.КонецТекста)
	кон TestScanner;
	
кон TestLisScanner.

TestLisScanner.ReportKeywords ~
TestLisScanner.TestScanner Proba.ярм ~
