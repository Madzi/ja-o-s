модуль PartitionEditorComponents; (** AUTHOR "staubesv"; PURPOSE "Visual components used by the Partition Editor"; *)

использует
	Strings, PartitionTable := PartitionEditorTable,
	WMGraphics, WMRectangles, WMComponents, WMStandardComponents, WMEditors;

конст
	NoChangeFound = 0;
	SizeChanged* = PartitionTable.SizeChanged;
	StartLbaChanged* = PartitionTable.StartLbaChanged;
	StartChsChanged* = PartitionTable.StartChsChanged;
	EndLbaChanged* = PartitionTable.EndLbaChanged;
	EndChsChanged* = PartitionTable.EndChsChanged;

	LbaEditorWidth = 100;
	ChsEditorWidth = 40;
	LineHeight  = 20;

тип

	(* Visual representation of PartitionTable.Block datastructure *)
	BlockEditor = окласс (WMComponents.VisualComponent)
	перем
		lbaEditor : WMEditors.Editor;
		cylinderEditor, headEditor, sectorEditor : WMEditors.Editor;

		проц Set(block : PartitionTable.Block);
		перем nbr : массив 16 из симв8;
		нач
			Strings.IntToStr(block.lba, nbr);lbaEditor.SetAsString(nbr);
			Strings.IntToStr(block.cylinder, nbr); cylinderEditor.SetAsString(nbr);
			Strings.IntToStr(block.head, nbr); headEditor.SetAsString(nbr);
			Strings.IntToStr(block.sector, nbr); sectorEditor.SetAsString(nbr);
		кон Set;

		проц Get() : PartitionTable.Block;
		перем block : PartitionTable.Block; nbr : массив 16 из симв8;
		нач
			lbaEditor.GetAsString(nbr); Strings.StrToInt(nbr, block.lba);
			cylinderEditor.GetAsString(nbr); Strings.StrToInt(nbr, block.cylinder);
			headEditor.GetAsString(nbr); Strings.StrToInt(nbr, block.head);
			sectorEditor.GetAsString(nbr); Strings.StrToInt(nbr, block.sector);
			возврат block;
		кон Get;

		проц Clear;
		нач
			lbaEditor.SetAsString("");
			cylinderEditor.SetAsString("");
			headEditor.SetAsString("");
			sectorEditor.SetAsString("");
		кон Clear;

		проц GetTitle(конст caption : массив из симв8) : WMComponents.VisualComponent;
		перем label : WMStandardComponents.Label;
		нач
			CreateLabel(label, caption, LbaEditorWidth + 3*ChsEditorWidth); label.alignH.Set(WMGraphics.AlignCenter);
			label.bearing.Set(WMRectangles.MakeRect(5, 0, 5, 0));
			возврат label;
		кон GetTitle;

		проц GetLegend() : WMComponents.VisualComponent;
		перем panel : WMStandardComponents.Panel; label : WMStandardComponents.Label;
		нач
			нов(panel); panel.alignment.Set(WMComponents.AlignLeft);
			panel.bounds.SetWidth(LbaEditorWidth + 3*ChsEditorWidth);
			panel.bearing.Set(WMRectangles.MakeRect(5, 0, 5, 0));

			CreateLabel(label, "LBA", LbaEditorWidth); label.alignH.Set(WMGraphics.AlignCenter);
			panel.AddContent(label);

			CreateLabel(label, "C", ChsEditorWidth); label.alignH.Set(WMGraphics.AlignCenter);
			panel.AddContent(label);

			CreateLabel(label, "H", ChsEditorWidth); label.alignH.Set(WMGraphics.AlignCenter);
			panel.AddContent(label);

			CreateLabel(label, "S", ChsEditorWidth); label.alignH.Set(WMGraphics.AlignCenter);
			panel.AddContent(label);

			возврат panel;
		кон GetLegend;

		проц &{перекрыта}Init*;
		нач
			Init^;
			bounds.SetWidth(LbaEditorWidth + 3*ChsEditorWidth);
			bearing.Set(WMRectangles.MakeRect(5, 0, 5, 0));
			CreateEditor(lbaEditor, LbaEditorWidth); AddContent(lbaEditor);
			CreateEditor(cylinderEditor, ChsEditorWidth); AddContent(cylinderEditor);
			CreateEditor(headEditor, ChsEditorWidth); AddContent(headEditor);
			CreateEditor(sectorEditor, ChsEditorWidth); AddContent(sectorEditor);
		кон Init;

	кон BlockEditor;

тип

	(* Visual representation of a partition table slot containing a field for the flag, the type, a start block and an end block *)
	PartitionEditor = окласс (WMComponents.VisualComponent)
	перем
		typeEditor, flagEditor : WMEditors.Editor;
		startBlock, endBlock : BlockEditor;
		sizeEditor : WMEditors.Editor;

		проц Set(partition : PartitionTable.Partition);
		перем nbr : массив 16 из симв8;
		нач
			Strings.IntToStr(partition.type, nbr); typeEditor.SetAsString(nbr);
			Strings.IntToStr(кодСимв8(partition.flag), nbr); flagEditor.SetAsString(nbr);
			startBlock.Set(partition.start);
			endBlock.Set(partition.end);
			Strings.IntToStr(partition.size, nbr); sizeEditor.SetAsString(nbr);
		кон Set;

		проц Get() : PartitionTable.Partition;
		перем partition : PartitionTable.Partition ;nbr : массив 16 из симв8; value : цел32;
		нач
			typeEditor.GetAsString(nbr); Strings.StrToInt(nbr, partition.type);
			flagEditor.GetAsString(nbr); Strings.StrToInt(nbr, value); partition.flag := симв8ИзКода(value);
			partition.start := startBlock.Get();
			partition.end := endBlock.Get();
			sizeEditor.GetAsString(nbr); Strings.StrToInt(nbr, partition.size);
			возврат partition;
		кон Get;

		проц Clear;
		нач
			typeEditor.SetAsString("");
			flagEditor.SetAsString("");
			startBlock.Clear;
			endBlock.Clear;
			sizeEditor.SetAsString("");
		кон Clear;

		проц GetTitle() : WMComponents.VisualComponent;
		перем panel : WMStandardComponents.Panel; label : WMStandardComponents.Label;
		нач
			нов(panel); panel.alignment.Set(WMComponents.AlignTop);
			panel.bounds.SetHeight(LineHeight);

			CreateLabel(label, "Flag", ChsEditorWidth); label.alignH.Set(WMGraphics.AlignCenter);
			panel.AddContent(label);

			CreateLabel(label, "Type", ChsEditorWidth); label.alignH.Set(WMGraphics.AlignCenter);
			panel.AddContent(label);

			panel.AddContent(startBlock.GetTitle("Start"));
			panel.AddContent(endBlock.GetTitle("End"));

			CreateLabel(label, "Size", LbaEditorWidth); label.alignH.Set(WMGraphics.AlignCenter);
			panel.AddContent(label);

			возврат panel;
		кон GetTitle;

		проц GetLegend() : WMComponents.VisualComponent;
		перем panel : WMStandardComponents.Panel;
		нач
			нов(panel); panel.alignment.Set(WMComponents.AlignTop);
			panel.bounds.SetHeight(LineHeight);
			panel.bearing.Set(WMRectangles.MakeRect(2 * ChsEditorWidth, 0, 0, 0));

			panel.AddContent(startBlock.GetLegend());
			panel.AddContent(endBlock.GetLegend());

			возврат panel;
		кон GetLegend;

		проц &{перекрыта}Init*;
		перем rect : WMRectangles.Rectangle;
		нач
			Init^;
			CreateEditor(flagEditor, ChsEditorWidth); AddContent(flagEditor);
			CreateEditor(typeEditor, ChsEditorWidth); AddContent(typeEditor);
			rect := WMRectangles.MakeRect(5, 0, 5, 0);
			нов(startBlock); startBlock.alignment.Set(WMComponents.AlignLeft);
			startBlock.bearing.Set(rect);
			AddContent(startBlock);
			нов(endBlock); endBlock.alignment.Set(WMComponents.AlignLeft);
			endBlock.bearing.Set(rect);
			AddContent(endBlock);
			CreateEditor(sizeEditor, LbaEditorWidth); AddContent(sizeEditor);
		кон Init;

	кон PartitionEditor;

тип
	ChangeHandler* = проц {делегат} (changeType : цел32; перем partition : PartitionTable.Partition);

	(** The PartitionTableEditor component can display and edit the PartitionTable.PartitionTable datastructure *)
	PartitionTableEditor* = окласс (WMComponents.VisualComponent)
	перем
		partitionEditors : массив 4 из PartitionEditor;
		partitionTable : PartitionTable.PartitionTable;
		changeHandler* : ChangeHandler;

		проц Set*(конст partitionTable : PartitionTable.PartitionTable);
		перем i : цел32;
		нач
			сам.partitionTable := partitionTable;
			нцДля i := 0 до длинаМассива(partitionEditors)-1 делай
				partitionEditors[i].Set(partitionTable[i]);
			кц;
		кон Set;

		проц Get*() : PartitionTable.PartitionTable;
		перем i : цел32;
		нач
			нцДля i := 0 до длинаМассива(partitionEditors)-1 делай
				partitionTable[i] := partitionEditors[i].Get();
			кц;
			возврат partitionTable;
		кон Get;

		(** Discards all user changes by reloading the values from the currently set partition table *)
		проц Discard*;
		нач
			Set(partitionTable);
		кон Discard;

		(** Clears all editable fields of the editor *)
		проц Clear*;
		перем i : цел32;
		нач
			нцДля i := 0 до длинаМассива(partitionEditors)-1 делай
				partitionEditors[i].Clear;
			кц;
		кон Clear;

		(* This handler is called whenever a text of any of the editors changes *)
		проц HandleOnEnter(sender, data : динамическиТипизированныйУкль);
		перем changeHandler : ChangeHandler; partition, changeType : цел32;
		нач
			changeHandler := сам.changeHandler;
			если (changeHandler # НУЛЬ) то
				partition := -1;
				changeType := NoChangeFound;
				нцПока(changeType = NoChangeFound) и (partition < длинаМассива(partitionEditors) - 1) делай
					увел(partition);
					если (sender = partitionEditors[partition].sizeEditor) то
						changeType := SizeChanged;
					аесли (sender = partitionEditors[partition].startBlock.lbaEditor) то
						changeType := StartLbaChanged;
					аесли (sender = partitionEditors[partition].startBlock.cylinderEditor) или
							(sender = partitionEditors[partition].startBlock.headEditor) или
							(sender = partitionEditors[partition].startBlock.sectorEditor) то
						changeType := StartChsChanged;
					аесли (sender = partitionEditors[partition].endBlock.lbaEditor) то
						changeType := EndLbaChanged;
					аесли (sender = partitionEditors[partition].endBlock.cylinderEditor) или
							(sender = partitionEditors[partition].endBlock.headEditor) или
							(sender = partitionEditors[partition].endBlock.sectorEditor) то
						changeType := EndChsChanged;
					всё;
				кц;
				если (changeType # NoChangeFound) то
					partitionTable[partition] := partitionEditors[partition].Get();
					changeHandler(changeType, partitionTable[partition]);
					partitionEditors[partition].Set(partitionTable[partition]);
				всё;
			всё;
		кон HandleOnEnter;

		проц RegisterOnEnterHandlers;
		перем i : цел32;

			проц RegisterBlock(blockEditor : BlockEditor);
			нач
				blockEditor.lbaEditor.onEnter.Add(HandleOnEnter);
				blockEditor.cylinderEditor.onEnter.Add(HandleOnEnter);
				blockEditor.headEditor.onEnter.Add(HandleOnEnter);
				blockEditor.sectorEditor.onEnter.Add(HandleOnEnter);
			кон RegisterBlock;

		нач
			нцДля i := 0 до длинаМассива(partitionEditors)-1 делай
				partitionEditors[i].sizeEditor.onEnter.Add(HandleOnEnter);
				RegisterBlock(partitionEditors[i].startBlock);
				RegisterBlock(partitionEditors[i].endBlock);
			кц;
		кон RegisterOnEnterHandlers;

		проц &{перекрыта}Init*;
		перем
			panel : WMStandardComponents.Panel;
			label : WMStandardComponents.Label;
			vc : WMComponents.VisualComponent;
			caption : массив 16 из симв8;
			i : цел32;
		нач
			Init^;
			PartitionTable.Clear(partitionTable);
			fillColor.Set(WMGraphics.White);
			нцДля i := 0 до длинаМассива(partitionEditors)-1 делай
				нов(panel); panel.alignment.Set(WMComponents.AlignTop);
				panel.bounds.SetHeight(LineHeight);

				Strings.IntToStr(i+1, caption);
				CreateLabel(label, caption, 20); label.alignH.Set(WMGraphics.AlignCenter);

				нов(partitionEditors[i]); partitionEditors[i].alignment.Set(WMComponents.AlignLeft);
				partitionEditors[i].bounds.SetWidth(700);

				если (i = 0) то
					vc := partitionEditors[i].GetTitle();
					vc.bearing.Set(WMRectangles.MakeRect(20, 0, 0, 0));
					AddContent(vc);
				всё;

				AddContent(panel);
				panel.AddContent(label);
				panel.AddContent(partitionEditors[i]);
			кц;
			vc := partitionEditors[0].GetLegend(); vc.bearing.Set(WMRectangles.MakeRect(100, 0, 0, 0));
			AddContent(vc);
			Clear;
			RegisterOnEnterHandlers;
		кон Init;

	кон PartitionTableEditor;

проц CreateLabel(перем label : WMStandardComponents.Label; конст caption : массив из симв8; width : цел32);
нач
	нов(label);
	label.alignment.Set(WMComponents.AlignLeft);
	label.bounds.SetWidth(width);
	label.caption.SetAOC(caption);
кон CreateLabel;

проц CreateEditor(перем editor : WMEditors.Editor; width : цел32);
нач
	нов(editor);
	editor.alignment.Set(WMComponents.AlignLeft);
	editor.bounds.SetWidth(width);
	editor.multiLine.Set(ложь);
	editor.tv.textAlignV.Set(WMGraphics.AlignCenter);
	editor.tv.showBorder.Set(истина);
кон CreateEditor;

кон PartitionEditorComponents.

PartitionEditorComponents.Test ~

System.Free PartitionEditorComponents ~


