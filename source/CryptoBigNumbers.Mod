модуль CryptoBigNumbers;  (* g.f.	2001.10.07 *)

(* 2002.08.12	g.f.	added neg. numbers, GCD and ModInverse  *)
(* 2002.09.24	g.f.	inceased digit size from 8 bit to 32 bit *)
(* 2002.10.04	g.f.	faster version of ModExp (uses montgomery multiplications now) *)
(* 2005.07.07	g.f.	Fabian Nart's enhancements incorporated *)
(* 2010.01.12	g.f.	interface cleanup, most procedures got funtions *)


использует S := НИЗКОУР, Потоки, Random, Kernel, Out := ЛогЯдра;

конст
	BufferPoolSize = 16;

тип
	BNdigit = бцел32;
	digits = укль на массив из BNdigit;

	BigNumber* = окласс
			перем
				len-: размерМЗ;  (** number of significant 'digits' *)
				neg-: булево;
				d-: digits;

				проц & Init( bitsize: размерМЗ );
				перем n: размерМЗ;
				нач
					если bitsize # 0 то
						n := (bitsize + 31) DIV 32;
						увел( n, (-n) остОтДеленияНа 16 );
						нов( d, n );
					всё;
					len := 0;  neg := ложь
				кон Init;

				проц Mask*( bits: размерМЗ );
				перем w, b: размерМЗ;
				нач
					w := bits DIV 32;  b := bits остОтДеленияНа 32;  len := w;
					если b # 0 то  увел( len );
						d[w] := S.подмениТипЗначения( бцел32,  S.подмениТипЗначения( мнвоНаБитах32, d[w] ) * мнвоНаБитах32({0..b}) )
					всё
				кон Mask;


				проц IsZero*( ): булево;
				нач
					возврат (len = 0) или ((len = 1) и (d[0] = 0))
				кон IsZero;

				проц EQ* ( b: BigNumber ): булево;
				нач
					возврат Cmp( сам, b ) = 0
				кон EQ;

				проц NEQ* ( b: BigNumber ): булево;
				нач
					возврат Cmp( сам, b ) # 0
				кон NEQ;

				проц GT* ( b: BigNumber ): булево;
				нач
					возврат Cmp( сам, b ) > 0
				кон GT;

				проц GEQ* ( b: BigNumber ): булево;
				нач
					возврат Cmp( сам, b ) >= 0
				кон GEQ;



				проц Shift*( n: размерМЗ );
				перем right: булево;  w, bits, i, l: размерМЗ;  a, b: BNdigit;
				нач
					если len = 0 то  возврат  всё;
					если n < 0 то  right := истина;  n := матМодуль( n )  иначе  right := ложь  всё;
					w := n DIV 32;  bits := n остОтДеленияНа 32;
					если ~right то
						adjust( len + w + 1 );
						если w > 0 то
							нцДля i := len - 1 до 0 шаг -1 делай  d[i + w] := d[i]  кц;
							нцДля i := 0 до w - 1 делай  d[i] := 0  кц;
							увел( len, w )
						всё;
						если bits > 0 то
							d[len] := 0;
							нцДля i := len до 0 шаг -1 делай
								a := d[i];
								если i > 0 то  b := d[i - 1]  иначе  b := 0  всё;
								d[i] := логСдвиг( a, bits ) + логСдвиг( b, -32 + bits )
							кц;
							если d[len] # 0 то  увел( len )  всё;
						всё
					иначе
						если w > 0 то
							нцДля i := 0 до len - w - 1 делай  d[i] := d[i + w]  кц;
							умень( len, w )
						всё;
						если bits > 0 то
							l := len;
							нцДля i := 0 до  l - 1 делай  a := d[i];
								если i < l - 1 то  b := d[i + 1]  иначе  b := 0  всё;
								d[i] := логСдвиг( a, -bits ) + логСдвиг( b, 32 - bits )
							кц;
							если d[l - 1] = 0 то  умень( len )  всё;
						всё
					всё;
				кон Shift;


				проц Dec*;
				перем i: размерМЗ;
				нач
					i := 0;
					если IsZero( ) то  len := 1;  neg := истина;  d[0] := 1
					аесли neg то
						нцПока (d[i] = -1) и (i < len) делай  d[i] := 0;  увел( i )  кц;
						если i = len то  d[i] := 1;  увел( len )  иначе  увел( d[i] )  всё
					иначе
						нцПока d[i] = 0 делай  d[i] := -1;  увел( i )  кц;
						умень( d[i] );  fixlen( d, len )
					всё
				кон Dec;

				проц Inc*;
				перем i: размерМЗ;
				нач
					i := 0;
					если ~neg то
						нцПока (d[i] = -1) и (i < len) делай  d[i] := 0;  увел( i )  кц;
						если i = len то  d[i] := 1;  увел( len )  иначе  увел( d[i] )  всё
					иначе
						нцПока d[i] = 0 делай  d[i] := -1;  увел( i )  кц;
						умень( d[i] );  fixlen( d, len );
						если len = 0 то  neg := ложь  всё
					всё
				кон Inc;

				проц Negate*;
				нач
					если ~IsZero( ) то  neg := ~neg  всё
				кон Negate;

				проц BitSize*( ): размерМЗ;
				перем n: размерМЗ; t: BNdigit;
				нач
					если len = 0 то  возврат 0
					иначе  n := (len - 1) * 32
					всё;
					t := d[len - 1];
					нцПока t # 0 делай  увел( n );  t := логСдвиг( t, -1 )  кц;
					возврат n
				кон BitSize;

				проц BitSet*( n: размерМЗ ): булево;
				перем w, bit: размерМЗ;
				нач
					w := n DIV 32;  bit := n остОтДеленияНа 32;
					если w >= len то  возврат ложь
					иначе  возврат  bit в S.подмениТипЗначения( мнвоНаБитах32, d[w] )
					всё
				кон BitSet;


				проц adjust( newlen: размерМЗ );
				перем n, i: размерМЗ;  nd: digits;
				нач
					n := 16;
					нцПока n < newlen делай  увел( n, 16 )  кц;
					если длинаМассива( d ) < n то
						нов( nd, n );
						нцДля i := 0 до длинаМассива( d^ ) - 1 делай  nd[i] := d[i]  кц;
						d := nd
					всё;
				кон adjust;

			кон BigNumber;

	dig2 = массив 2 из BNdigit;
	dig3 = массив 3 из BNdigit;

	Montgomery = окласс
				перем
					bits: размерМЗ;	(* of R *)
					r, n, t1, t2: BigNumber;

				проц & Init( x: BigNumber );
				нач
					Copy( x, n );  bits := x.len*32;
					AssignInt( r, 1 );  r.Shift( bits );	(* r := R *)
					r := Sub( r, ModInverse( n, r ) );   (* r := R - (1/n)  (mod R) *)
					n.adjust( 2*x.len );  r.adjust( 2*x.len );
					нов( t1, 2*bits );  нов( t2, 2*bits );
				кон Init;

				проц Convert( перем val: BigNumber ); 	(* val := val ^ R mod n *)
				перем i: размерМЗ;
				нач
					нцДля i := 0 до bits - 1 делай
						val.Shift( 1 );
						если ucmp( val, n ) >= 0 то  val := Sub( val, n )  всё
					кц
				кон Convert;

				проц Reduce( перем val: BigNumber ); 	(* val := val ^ (1/R) mod n *)
				нач
					Copy( val, t1 );  t1.Mask( bits - 1 ); 	(* val mod R *)
					mul( t1.d, r.d, t2.d, t1.len, r.len, t2.len );  t2.Mask( bits - 1 ); 	(* mod R *)
					mul( t2.d, n.d, t1.d, t2.len, n.len, t1.len );
					add( t1.d, val.d, val.d, t1.len, val.len, val.len );  val.Shift( -bits ); 	(* div R *)
					если ucmp( val, n ) >= 0 то  sub( val.d, n.d, val.d, val.len, n.len, val.len )  всё;
				кон Reduce;


				проц Mult( a, b: BigNumber ): BigNumber;
				перем c: BigNumber;
				нач
					нов( c, 0 );
					mul( a.d, b.d, c.d, a.len, b.len, c.len );
					Reduce( c );
					возврат c
				кон Mult;

			кон  Montgomery;


перем
	bufferPool: массив BufferPoolSize из digits;
	nextFreeBuffer: цел32;

	randomgenerator: Random.Generator;


	проц RandomBytes*( перем buf: массив из симв8;  p, n: размерМЗ );
	перем i: размерМЗ;
	нач
		нцДля i := 0 до n - 1 делай buf[p + i] := симв8ИзКода( округлиВниз( randomgenerator.Uniform()*256 ) ) кц
	кон RandomBytes;



	проц adjust( перем d: digits;  dl, len: размерМЗ );
	перем n, i: размерМЗ;  nd: digits;
	нач
		утв( d # НУЛЬ );
		n := 16;
		нцПока n < len делай  увел( n, 16)  кц;
		если длинаМассива( d ) < n то
			нов( nd, n );
			нцДля i := 0 до dl - 1 делай nd[i] := d[i] кц;
			d := nd
		всё;
	кон adjust;


	(** random number with len 'bits' *)
	проц NewRand*( bits: размерМЗ;  top, bottom: цел8 ): BigNumber;
	перем n, len, i, topbit: размерМЗ;  topword: мнвоНаБитах32;  b: BigNumber;
	нач
		len := bits;  увел( len, (-len) остОтДеленияНа 32 );
		нов( b, len );
		n := len DIV 32;
		нцДля i := 0 до n -1 делай
			b.d[i] := randomgenerator.Integer()
		кц;
		b.len := (bits + 31) DIV 32;
		topbit := (bits - 1)  остОтДеленияНа 32;
		topword := мнвоНаБитах32( S.подмениТипЗначения( мнвоНаБитах32, b.d[b.len - 1] ) * мнвоНаБитахМЗ({0..topbit}) );
		если top > 0 то включиВоМнвоНаБитах( topword, topbit ) всё;
		b.d[b.len - 1] := S.подмениТипЗначения( цел32, topword );
		если (bottom > 0) и ~нечётноеЛи¿( b.d[0] ) то  увел( b.d[0] )  всё;
		возврат b
	кон NewRand;

	проц NewRandRange*( range: BigNumber ): BigNumber;	(** 0 < b < range DIV 2 - 1*)
	перем  b: BigNumber;
	нач
		b := NewRand( range.BitSize( ) - 1, 0, 0 );
		b.Dec;
		возврат b
	кон NewRandRange;

	проц fixlen( перем d: digits;  перем len: размерМЗ );
	нач
		нцПока (len > 0) и (d[len - 1] = 0) делай  умень( len )  кц;
	кон fixlen;

	проц h2i( c: симв8 ): цел32;
	перем v: цел32;
	нач
		просей c из
		| '0'..'9':  v := кодСимв8( c ) - кодСимв8( '0' )
		| 'a'..'f':  v := кодСимв8( c ) - кодСимв8( 'a' ) + 10
		| 'A'..'F':  v := кодСимв8( c ) - кодСимв8( 'A' ) + 10
		иначе  СТОП( 99 )
		всё;
		возврат v
	кон h2i;

	проц AssignHex*( перем b: BigNumber;  конст hex: массив из симв8;  len: размерМЗ );
	перем n, pos: размерМЗ; w: BNdigit;
	нач
		утв( len <= длинаМассива( hex ) - 1);
		нов( b, 4*len );  b.len := (4*len + 31) DIV 32;
		n := b.len - 1;  w := 0;  pos := 0;
		нцПока len > 0 делай
			w := w*16 + h2i( hex[pos] );  увел( pos );  умень( len );
			если len остОтДеленияНа 8 = 0 то  b.d[n] := w;  w := 0;  умень( n )  всё;
		кц;
		fixlen( b.d, b.len )
	кон AssignHex;

	проц AssignBin*( перем b: BigNumber;  конст buf: массив из симв8;  pos, len: размерМЗ );
	перем n: размерМЗ; w: BNdigit;
	нач
		утв( (pos + len) <= длинаМассива( buf ) );
		нов( b, 8*len );  b.len := (8*len + 31) DIV 32;
		n := b.len - 1;  w := 0;
		нцПока len > 0 делай
			w := w*256 + кодСимв8( buf[pos] );  увел( pos );  умень( len );
			если len остОтДеленияНа 4 = 0 то  b.d[n] := w;  w := 0;  умень( n )  всё;
		кц;
		fixlen( b.d, b.len )
	кон AssignBin;

	(** Returns the value of b as a binary string 'data' starting at ofs.
		The Length of 'data' must be longer or equal to 4*b.len + ofs. *)
	проц GetBinaryValue*( перем b: BigNumber; перем data: массив из симв8; ofs: размерМЗ );
	перем j, n: размерМЗ;  tmp: BNdigit;
	нач
		утв( длинаМассива( data ) >= 4 * b.len + ofs );
		нцДля n := b.len-1 до 0 шаг -1 делай
			tmp := b.d[n];
			нцДля j := 3 до 0 шаг - 1 делай
				data[ ofs + j ] := симв8ИзКода( tmp остОтДеленияНа 256 );
				tmp := tmp DIV 256
			кц;
			увел( ofs, 4 )
		кц
	кон GetBinaryValue;

	проц AssignInt*( перем b: BigNumber;  val: цел32 );
	нач
		нов( b, 64 );
		если val < 0 то  b.neg := истина;  val := матМодуль( val ) всё;
		если val # 0 то  b.len := 1;  b.d[0] := val  иначе  b.len := 0   всё
	кон AssignInt;

	проц cmpd( перем a, b: digits;  len: размерМЗ ): цел8;
	перем i: размерМЗ;
	нач
		i := len - 1;
		нцПока (i >= 0) и (a[i] = b[i]) делай  умень( i )  кц;
		если i < 0 то  возврат 0
		иначе
			если b[i] < a[i] то  возврат 1  иначе  возврат -1  всё
		всё
	кон cmpd;

	проц ucmp( перем a, b: BigNumber ): цел8;   (* 1: |a| > |b|;  0: a = b;  -1:  |a| < |b| *)
	нач
		если a.len > b.len то  возврат 1
		аесли a.len < b.len то  возврат -1
		иначе  возврат cmpd( a.d, b.d, a.len )
		всё
	кон ucmp;

	проц Cmp*( a, b: BigNumber ): цел8;   (** 1: a > b;  0: a = b;  -1:  a < b *)
	нач
		если a.neg # b.neg то
			если a.neg то  возврат -1  иначе  возврат 1  всё
		аесли a.neg то  возврат ucmp( a, b ) * (-1)
		иначе  возврат ucmp( a, b )
		всё
	кон Cmp;

	проц copy( a, b: digits;  len: размерМЗ );
	перем i: размерМЗ;
	нач
		нцДля i := 0 до len - 1 делай  b[i] := a[i]  кц
	кон copy;

	проц Copy*( перем a, b: BigNumber );   (** b := a *)
	нач
		утв( (a # НУЛЬ) и (адресОт( a ) # адресОт( b )) );
		если (b = НУЛЬ) или (длинаМассива( b.d^ ) < a.len) то  нов( b, a.len*32 )  всё;
		copy( a.d, b.d, a.len );  b.len := a.len
	кон Copy;

	проц Invert( x: BNdigit ): BNdigit;
	нач
		возврат S.подмениТипЗначения( BNdigit, -S.подмениТипЗначения( мнвоНаБитах32, x ) )
	кон Invert;

	проц add( a, b: digits; перем c: digits;  al, bl: размерМЗ;  перем cl: размерМЗ );
	перем i, n: размерМЗ;  A, B, x: BNdigit;  carry: булево;
	нач
		n := матМаксимум( al, bl );  carry := ложь;
		если длинаМассива( c^ ) < (n + 1) то  adjust( c, cl, n + 1 )  всё;
		нцДля i := 0 до n - 1 делай
			если i >= al то  A := 0  иначе  A := a[i]  всё;
			если i >= bl то  B := 0  иначе  B := b[i]  всё;
			x := A + B;
			если carry то  увел( x );  carry := Invert(A) <= B  иначе  carry := x < B  всё;
			c[i]:= x
		кц;
		если carry  то  c[n] := 1;  увел( n )  всё;
		cl := n
	кон add;

	проц sub( a, b: digits;  перем c: digits;  al, bl: размерМЗ;  перем cl: размерМЗ );
	перем i, n: размерМЗ;  A, B, x: BNdigit;  borrow: булево;
	нач
		n := матМаксимум( al, bl );  borrow := ложь;
		если длинаМассива( c^ ) < n то  adjust( c, cl, n )  всё;
		нцДля i := 0 до n - 1 делай
			если i >= al то  A := 0  иначе  A := a[i]  всё;
			если i >= bl то  B := 0  иначе  B := b[i]  всё;
			x := A - B;
			если borrow то  умень( x );  borrow := A <= B  иначе  borrow := A < B  всё;
			c[i]:= x
		кц;
		утв( ~borrow );
		нцПока (n > 0) и (c[n - 1] = 0) делай  умень( n )  кц;
		cl := n
	кон sub;

	проц Add*( a, b: BigNumber ): BigNumber;   (**  a + b *)
	перем sd: digits;  l, sl: размерМЗ;  c: BigNumber;
	нач
		утв( (a # НУЛЬ) и (b # НУЛЬ) );
		l := матМаксимум( a.len, b.len ) + 1;
		нов( c, l*32 );  sd := c.d;
		если a.neg = b.neg то  add( a.d, b.d, sd, a.len, b.len, sl );  c.neg := a.neg
		иначе
			если ucmp( a, b ) >= 0 то  sub( a.d, b.d, sd, a.len, b.len, sl );  c.neg :=  a.neg
			иначе  sub( b.d, a.d, sd, b.len, a.len, sl );  c.neg := ~a.neg
			всё
		всё;
		если sd # c.d то  adjust( c.d, 0, sl );  copy( sd, c.d, sl )  всё;
		c.len := sl;
		если c.IsZero( ) то  c.neg := ложь  всё;
		возврат c
	кон Add;

	проц Sub*( a, b: BigNumber ): BigNumber;   (**  a - b  *)
	перем sd: digits;  l, sl: размерМЗ;  c: BigNumber;
	нач
		утв( (a # НУЛЬ) и (b # НУЛЬ) );
		l := матМаксимум( a.len, b.len ) + 1;
		нов( c, l*32 );  sd := c.d;
		если a.neg # b.neg то  add( a.d, b.d, sd, a.len, b.len, sl );  c.neg := a.neg
		иначе
			если ucmp( a, b ) >= 0  то  sub( a.d, b.d, sd, a.len, b.len, sl );  c.neg :=  a.neg
			иначе  sub( b.d, a.d, sd, b.len, a.len, sl );  c.neg := ~a.neg
			всё
			всё;
		если sd # c.d то  adjust( c.d, 0, sl );  copy( sd, c.d, sl )  всё;
		c.len := sl;
		если c.IsZero( ) то  c.neg := ложь  всё;
		возврат c
	кон Sub;


	проц mul( a, b: digits; перем c: digits;  al, bl: размерМЗ;  перем cl: размерМЗ );  (* c = a*b *)
	перем
		prod, sum, tmp, mulc: BNdigit;  addc: булево;  i, j, pl: размерМЗ;
		p: digits;  tmp64: бцел64;
	нач
		pl := 0;  нов( p, al + bl + 2 );
		нцДля i := 0 до al + bl + 1 делай  p[i] := 0  кц;	(* clear acc *)
		нцДля i := 0 до bl - 1 делай
			mulc := 0;  addc := ложь;  pl := i;
			нцДля j := 0 до al - 1 делай
				tmp := p[pl];
				tmp64 := бцел64( a[j] )*b[i] + mulc;
				prod := BNdigit( tmp64 остОтДеленияНа 100000000H );
				mulc := BNdigit( tmp64 DIV 100000000H );
				sum := prod + tmp;
				если addc то  увел( sum );  addc := Invert(prod) <= tmp
				иначе  addc := sum < tmp
				всё;
				p[pl] := sum;  увел( pl );
			кц;
			если addc или (mulc # 0) то
				если addc то  увел( mulc )  всё;
				p[pl] := mulc;  увел( pl )
			всё;
		кц;
		c := p;  cl := pl;  fixlen( c, cl );
	кон mul;

	проц muls( a: digits;  b: BNdigit; c: digits;  al: размерМЗ;  перем cl: размерМЗ );  (* c = a*b *)
	перем carry: BNdigit;  tmp64: бцел64;  i: размерМЗ;
	нач
		carry := 0;  cl := al;
		нцДля i := 0 до al - 1 делай
			tmp64 := бцел64( a[i] )*b + carry;
			c[i] := BNdigit( tmp64 остОтДеленияНа 100000000H );
			carry := BNdigit( tmp64 DIV 100000000H );
		кц;
		если carry # 0 то  c[cl] := carry;  увел( cl )  всё
	кон muls;

	проц Mul*( a, b: BigNumber ): BigNumber;   (**  a * b  *)
	перем pd: digits;  pl: размерМЗ;  c: BigNumber;
	нач
		утв( (a # НУЛЬ) и (b # НУЛЬ) );
		если (a.len = 0) или (b.len = 0) то  AssignInt( c, 0 );  возврат c  всё;
		нов( c, 32 );
		если a.len >= b.len то
			mul( a.d, b.d, pd, a.len, b.len, pl )
		иначе
			mul( b.d, a.d, pd, b.len, a.len, pl )
		всё;
		c.d := pd;  c.len := pl;  c.neg := a.neg # b.neg;
		возврат c
	кон Mul;

	проц div64( конст a: dig2;  перем b: BNdigit ): цел32;   (* a div b *)
	перем bit, q: цел32;  r: BNdigit;  overflow: булево;
	нач
		если a[1] = 0 то
			если (a[0] < 80000000H) и (b < 80000000H ) то  возврат цел32( a[0] DIV b )
			аесли a[0] < b то  возврат 0
			аесли a[0] = b то  возврат 1
			всё;
			bit := 31
		аесли a[1] = b то  возврат -1
		иначе bit := 63
		всё;
		q := 0;  r := 0;
		нцПока (bit >= 0) и ~(bit остОтДеленияНа 32 в S.подмениТипЗначения( мнвоНаБитах32, a[bit DIV 32]) ) делай  умень( bit )  кц;
		нцПока bit >= 0 делай
			overflow := 31 в S.подмениТипЗначения( мнвоНаБитах32, r );  r := арифмСдвиг( r, 1 );
			если bit остОтДеленияНа 32 в S.подмениТипЗначения( мнвоНаБитах32, a[bit DIV 32] ) то  увел( r )  всё;
			если overflow или (b <= r) то  r := r - b;
				если bit < 32 то  включиВоМнвоНаБитах( S.подмениТипЗначения( мнвоНаБитах32, q ), bit )  иначе  q := -1  всё;
			всё;
			умень( bit )
		кц;
		возврат q
	кон div64;

	проц div96( конст a: dig3;  конст b: dig2 ): цел32;   (* a div b *)
	перем bit: цел32;  r: dig2;  q: цел32;  overflow, borrow: булево;

		проц ge( конст a, b: dig2 ): булево;
		нач
			если a[1] = b[1] то  возврат a[0] >= b[0]
			иначе  возврат a[1] >= b[1]
			всё
		кон ge;

		проц shift( перем x: dig2 );
		нач
			overflow := 31 в S.подмениТипЗначения( мнвоНаБитах32, x[1] );  x[1] := арифмСдвиг( x[1], 1 );
			если 31 в S.подмениТипЗначения( мнвоНаБитах32, x[0] ) то  увел( x[1] )  всё;
			x[0] := арифмСдвиг( x[0], 1 );
		кон shift;

	нач
		если a[2] = 0 то
			если a[1] < b[1] то  возврат 0  всё;
			bit := 63
		иначе  bit := 95
		всё;
		q := 0;  r[0] := 0;  r[1] := 0;
		нцПока (bit >= 0) и ~(bit остОтДеленияНа 32 в S.подмениТипЗначения( мнвоНаБитах32, a[bit DIV 32]) ) делай  умень( bit )  кц;
		нцПока bit >= 0 делай
			shift( r );	(* r := r*2 *)
			если bit остОтДеленияНа 32 в S.подмениТипЗначения( мнвоНаБитах32, a[bit DIV 32] ) то  увел( r[0] )  всё;
			если overflow или ge( r, b ) то
				borrow := r[0] <= b[0];  r[0] := r[0] - b[0];  r[1] := r[1] - b[1];
				если borrow  то  умень( r[1] )  всё;
				если bit < 32 то  включиВоМнвоНаБитах( S.подмениТипЗначения( мнвоНаБитах32, q ), bit )  иначе  q := -1  всё;
			всё;
			умень( bit )
		кц;
		возврат q
	кон div96;

	проц Div2*( a, b: BigNumber;  перем q, r: BigNumber );   (** q = a div b;  r = a mod b *)
	перем td, sd, bd, qd: digits;  x: цел32; i, tail, bl, tl, sl, ql, qi: размерМЗ;
		t3: dig3;  t2, d0: dig2;
		aq, ar: адресВПамяти;
	нач
		aq := адресОт( q );   ar := адресОт( r );
		утв( (a # НУЛЬ) и (b # НУЛЬ) и ~b.IsZero( ) и ~b.neg и (aq # ar) );
		нов( q, a.len*32 );  qd := q.d;

		x := ucmp( a, b );
		если x < 0 то  AssignInt( q, 0 );  Copy( a, r )
		аесли x = 0 то  AssignInt( q, 1 );  AssignInt( r, 0 )
		иначе
			td := GetBuffer();
			sd := GetBuffer();
			bd := b.d;  bl := b.len;  d0[1] := bd[bl - 1];
			если bl > 1 то  d0[0] := bd[bl - 2]  иначе  d0[0] := 0  всё;
			нцДля i := 1 до bl делай  td[bl - i] := a.d[a.len - i]  кц;
			tl := bl;  tail := a.len - bl;  ql := tail + 1;  qi := ql;
			нц
				если tl < bl то  x := 0;
				иначе i := tl  - 1;
					если d0[0] = 0 то
						если tl > bl то  t2[1] := td[i];  умень( i )  иначе  t2[1] := 0  всё;
						t2[0] := td[i];
						x := div64( t2, d0[1] );
					иначе
						если tl > bl то  t3[2] := td[i];  умень( i )  иначе  t3[2] := 0  всё;
						t3[1] := td[i];
						если i > 0 то  t3[0] := td[i - 1]  иначе  t3[0] := 0   всё;
						x := div96( t3, d0 );
					всё
				всё;
				если x # 0 то  muls( bd, x, sd, bl, sl );
					нцПока (sl > tl) или ((sl = tl) и (cmpd( sd, td, sl ) > 0)) делай
						sub( sd, bd, sd, sl, bl, sl );  умень( x );
					кц;
					sub( td, sd, td, tl, sl, tl );
				всё;
				если (qi = ql) и (x = 0) то  умень( ql );  умень( qi )  иначе  умень( qi );  qd[qi] := x  всё;
				если tail = 0 то  прервиЦикл  всё;
				умень( tail );
				нцДля i := tl до 1 шаг -1 делай  td[i] := td[i - 1]  кц;
				td[0] := a.d[tail];  увел( tl );
			кц;
			q.len := ql;
			нов( r, tl*32 );  copy( td, r.d, tl );  r.len := tl;
			RecycleBuffer( td );
			RecycleBuffer( sd )
		всё;
		если q.len = 0 то  q.neg := ложь  иначе  q.neg := a.neg  всё;
		если (r.len # 0) и a.neg то  q.Dec;  r := Sub( b, r )  всё;
	кон Div2;

	проц ModWord*( перем a: BigNumber;  b: BNdigit ): BNdigit;   (**  a mod b *)
	перем x: BNdigit;  td, sd, bd: digits;  tail, tl, sl, bl: размерМЗ;  t2: dig2;
	нач
		утв( a # НУЛЬ );
		td := GetBuffer();
		sd := GetBuffer();
		bd := GetBuffer();
		bd[0] := b;  bl := 1;  td[0] := a.d[a.len - 1];  tl := 1;  tail := a.len - 1;
		нц
			если tl > 1 то  t2[1] := td[1]  иначе  t2[1] := 0  всё;
			t2[0] := td[0];
			x := div64( t2, b );
			если x # 0 то  muls( bd, x, sd, bl, sl );
				нцПока (sl > tl) или ((sl = tl) и (cmpd( sd, td, sl ) > 0)) делай
					sub( sd, bd, sd, sl, bl, sl );  умень( x );
				кц;
				sub( td, sd, td, tl, sl, tl );
			всё;
			если tail <= 0 то  прервиЦикл  всё;
			умень( tail );
			если td[0] = 0 то  tl := 1  иначе td[1] := td[0];  tl := 2  всё;
			td[0] := a.d[tail];
		кц;
		x := td[0];
		RecycleBuffer( td );
		RecycleBuffer( sd );
		RecycleBuffer( bd );
		возврат x
	кон ModWord;

	проц Div*( a, b: BigNumber ): BigNumber; 	(**   a DIV b  *)
	перем dummy, q: BigNumber;
	нач
		Div2( a, b, q, dummy );
		возврат q
	кон Div;

	проц Mod*( a, b: BigNumber ): BigNumber; 	(**   a MOD b  *)
	перем dummy, r: BigNumber;
	нач
		Div2( a, b, dummy, r );
		возврат r
	кон Mod;


	проц Exp*( a, b: BigNumber ): BigNumber;   (**  a ^ b  *)
	перем v: digits; i, vl: размерМЗ;  e: BigNumber;
	нач
		нов( e, 8192 );
		нов( v, 256 );
		copy( a.d, v, a.len );  vl := a.len;
		если нечётноеЛи¿( b.d[0] ) то  copy( a.d, e.d, a.len );  e.len := a.len  иначе  e.len := 1; e.d[0] := 1  всё;
		нцДля i := 1 до b.BitSize( ) - 1 делай
			mul( v, v, v, vl, vl, vl );
			если b.BitSet( i ) то   mul( v, e.d, e.d, vl, e.len, e.len )  всё;
		кц;
		fixlen( e.d, e.len );
		возврат e
	кон Exp;

	проц ModMul*( a, b, m: BigNumber ): BigNumber;  (**  (a*b) mod m  *)
	перем p, r: BigNumber;
	нач
		p := Mul( a, b );  r := Mod( p, m );
		возврат r
	кон ModMul;

	проц wbits( exp: BigNumber ): размерМЗ;
	перем b, w: размерМЗ;
	нач
		(* window bits for exponent size,  for sliding window ModExp functions *)
		b := exp.BitSize( );
		если b <= 23 то  w := 1
		аесли b <= 79 то  w := 3
		аесли b <= 239 то  w := 4
		аесли b <= 671 то  w := 5
		иначе  w := 6
		всё;
		возврат w
	кон wbits;

	проц ModExp*( a, b, m: BigNumber ): BigNumber;	(**  a ^ b mod m *)
	перем
		a0: массив 32 из BigNumber;  res, d: BigNumber;
		wsize, v, wstart, e, i, j: размерМЗ;
		mg: Montgomery;
	нач
		утв( (a # НУЛЬ) и (b # НУЛЬ) и (m # НУЛЬ) );
		если b.IsZero( ) то
			если a.IsZero( ) то СТОП( 100 ) всё;
			AssignInt( res, 1 );  возврат  res
		всё;
		если m.IsZero( ) то  СТОП( 101 )  всё;
		если m.neg то  СТОП( 102 )  всё;

		нов( mg, m );
		a0[0] := Mod( a, m );  mg.Convert( a0[0] );

		wsize := wbits( b );
		если wsize > 1 то  (* precompute window multipliers *)
			d := mg.Mult( a0[0], a0[0] );  j := арифмСдвиг( 1, wsize - 1 );
			нцДля i := 1 до j - 1 делай  a0[i] := mg.Mult( a0[i - 1], d )  кц;
		всё;

		Copy( a0[0], res );  wstart := b.BitSize( ) - 2;
		нцПока wstart >= 0 делай  res := mg.Mult( res, res );
			если b.BitSet( wstart ) то
				v := 1;  e := 0;  i := 1;
				нцПока (i < wsize) и (wstart - i >= 0) делай
					если b.BitSet( wstart - i ) то  v := арифмСдвиг( v, i - e ) + 1;  e := i  всё;
					увел( i )
				кц;
				нцДля i := 1 до e делай  res := mg.Mult( res, res )  кц;
				res := mg.Mult( res, a0[v DIV 2] );	(*  v will be an odd number < 2^wsize *)
				умень( wstart, e + 1 );
			иначе умень( wstart )
			всё
		кц;
		mg.Reduce( res );
		возврат res
	кон ModExp;



	проц GCD*( a, b: BigNumber ): BigNumber;		(**  gcd( a, b ) *)
	перем x, y, r: BigNumber;
	нач
		утв( ~a.neg и ~b.neg );
		Copy( a, x );  Copy( b, y );
		нц
			если Cmp( x, y ) > 0 то  x := Mod( x, y );
				если x.IsZero( ) то  Copy( y, r );  прервиЦикл  всё
			иначе  y := Mod( y, x ) ;
				если y.IsZero( ) то  Copy( x, r );  прервиЦикл  всё
			всё;
		кц;
		возврат r
	кон GCD;

	проц ModInverse*( a, m: BigNumber ): BigNumber;	(** Return x so that (x * a) mod m = 1 *)
	перем
		q, t, x: BigNumber;  g, v: массив 3 из BigNumber;  p, i, s, tmp, n: размерМЗ;
	нач
		нцДля i := 0 до 2 делай  AssignInt( g[i], 0 ); AssignInt( v[i], 0 ) кц;
		Copy( a, g[0] );  Copy( m, g[1] );  AssignInt( v[0], 1 );  AssignInt( v[1], 0 );
		p := 0;  i := 1;  s := 2;  n := 0;
		нц
			Div2( g[p], g[i], q, g[s] );  t := Mul( q, v[i] );  v[s] := Add( v[p], t );  увел( n );
			если g[s].IsZero( ) то  прервиЦикл  всё;
			tmp := p;  p := i;  i := s;  s := tmp;
		кц;
		если (g[i].len = 1) и (g[i].d[0] = 1) то
			если нечётноеЛи¿( n ) то  v[i] := Sub( m, v[i] )  всё;
			x := Mod( v[i], m )
		иначе  AssignInt( x, 0 )
		всё;
		возврат x
	кон ModInverse;



	(*--------------------------- Text I/O ---------------------------------*)

	проц TextWrite*( w: Потоки.Писарь;  b: BigNumber );
	перем i: размерМЗ;
	нач
		если b.neg то  w.пСтроку8( "-" ) всё;
		если b.len = 0 то  w.пСтроку8( " 00000000" )
		иначе i := b.len;
			нцПока i > 0 делай
				умень( i );  w.п16ричное( b.d[i], -8 );
				если i > 0 то
					если i остОтДеленияНа 6 = 0 то  w.пВК_ПС
					иначе  w.пСтроку8( "  " )
					всё
				всё
			кц
		всё;
		w.пСимв8( '.' );
	кон TextWrite;

	(** writes a hexadecimal representation of b to the standard output *)
	проц Print*( b: BigNumber );
	перем i: размерМЗ;
	нач
		если b.neg то Out.пСтроку8( "-" ) всё;
		если b.len = 0 то  Out.пСтроку8( "00000000" )
		иначе  i := b.len;
			нцПока i > 0 делай
				умень( i );  Out.п16ричное( b.d[i], -8 );
				если i > 0 то
					если i остОтДеленияНа 6 = 0 то  Out.пВК_ПС
					иначе  Out.пСтроку8( "  " )
					всё
				всё
			кц
		всё;
		Out.пСимв8( '.' );  Out.пВК_ПС
	кон Print;


	проц nibble( r: Потоки.Чтец ): симв8;
	перем c: симв8;
	нач
		нцДо
			нцДо r.чСимв8( c ) кцПри (c > ' ') или (r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0);
		кцПри	(r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0) или
				(c >= '0') и (c <= '9') или
				(c >= 'A') и (c <= 'F') или
				(c >= 'a') и (c <= 'f') или (c = '.');
		возврат c
	кон nibble;

	проц TextRead*( r: Потоки.Чтец;  перем b: BigNumber );
	перем buf: массив 2048 из симв8; i: размерМЗ; n: симв8;
	нач
		i := 0;  n := nibble( r );
		нцПока n # '.' делай buf[i] := n;  увел( i );  n := nibble( r ) кц;
		AssignHex( b, buf, i );
	кон TextRead;



	(*--------------------------- File I/O ---------------------------------*)

	проц FileRead*( r: Потоки.Чтец;  перем b: BigNumber );
	перем i, j, v: цел32;
	нач
		r.чЦел32_мз( j );
		нов( b, 32 * j );
		b.len := j;
		нцДля i := 0 до j - 1 делай  r.чЦел32_мз( v ); b.d[ i ] := v  кц
	кон FileRead;

	проц FileWrite*( w: Потоки.Писарь;  b: BigNumber );
	перем i, j, v: цел32;
	нач
		j := b.len(цел32);
		w.пЦел32_мз( j );
		нцДля i := 0 до j - 1 делай  w.пЦел32_мз( v ); b.d[ i ] := v  кц
	кон FileWrite;


	(* ------------ buffer pooling to make this module thread-save (F.N.) -----------------------*)

	проц GetBuffer( ): digits;
	перем d: digits;
	нач {единолично}
		если nextFreeBuffer > -1 то
			d := bufferPool[ nextFreeBuffer ];
			умень( nextFreeBuffer )
		иначе
			нов( d, 256 )
		всё;
		возврат d
	кон GetBuffer;

	проц RecycleBuffer( d: digits );
	нач {единолично}
		если nextFreeBuffer < BufferPoolSize - 1 то
			увел( nextFreeBuffer );
			bufferPool[ nextFreeBuffer ] := d
		всё
	кон RecycleBuffer;

	проц InitRandomgenerator;
	нач
		нов( randomgenerator );
		randomgenerator.InitSeed( Kernel.GetTicks() );
	кон InitRandomgenerator;

нач
	утв( цел32({0}) = 1 );		(* little endian SETs! *)
	нцДля nextFreeBuffer := 0 до BufferPoolSize - 1 делай
		нов( bufferPool[nextFreeBuffer], 256 )
	кц;
	nextFreeBuffer := BufferPoolSize-1;
	InitRandomgenerator();
кон CryptoBigNumbers.

