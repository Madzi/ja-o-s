модуль WMDiagnostics; (** AUTHOR "staubesv"; PURPOSE "Visual Component for Diagnostics Interface"; *)

(* LisCompiler.Compile -w проба.ярм ~ *)
использует
	Locks, Strings, Diagnostics, Files, Потоки,
	WMRectangles, WMGraphics, WMGraphicUtilities, WMBitmapFont,
	WMEvents, WMProperties, WMGrids, WMTextView;

конст
	TypeInformation* = Diagnostics.TypeInformation;
	TypeWarning* = Diagnostics.TypeWarning;
	TypeError* = Diagnostics.TypeError;

	(* Error grid colors *)
	ColorError = цел32(0FF3030A0H);
	ColorWarning = цел32(0D0D040C0H);
	ColorPCPosition = 0007F00A0H;

	(* Textview position marker pictures *)
	PictureError = "PETIcons.tar://errorpos.png";
	PictureWarning = "PETIcons.tar://warningpos.png";
	(* PicturePCPosition = "PETIcons.tar://pcpos.png"; *)

	InitialArraySize = 16;

	Less = -1;
	Equal = 0;
	Greater = 1;

	SortByTypeAscending* = 0;
	SortByTypeDescending* = 1;
	SortByPositionAscending* = 2;
	SortByPositionDescending* = 3;

тип

	Entry* = запись
		type- : целМЗ;
		source- : Files.FileName;
		position-: Потоки.ТипМестоВПотоке;
		message- : массив 256 из симв8;
		(** некоторые сообщения могут относиться к другим файлам. 
			При этом выводится имя файла без пути. Мы должны будем
			в какой-то момент проверить, подходит ли файл, и 
			заполнить это. Если файл не текущий, то не нужно 
			вставлять закладку в текст текущего файла для этого
			сообщения. В перспективе будем вставлять другой вид
			закладки, для перехода к другому файлу *)
		текущийФайлЛи* : булево;
	кон;

	EntryArray = укль на массив из Entry;

тип

	Model* = окласс(Diagnostics.Diagnostics)
	перем
		entries : EntryArray;
		nofEntries- : цел32;

		nofInformations: размерМЗ;
		nofWarnings : размерМЗ;
		nofErrors : размерМЗ;

		lock : Locks.RWLock;

		onChanged- : WMEvents.EventSource;
		changed : булево;
		notificationEnabled : булево;
		(* имя файла без пути *)
		имяТекущегоФайла* : Files.FileName;

		проц &Init*; (** proctected *)
		нач
			entries := НУЛЬ;
			nofEntries := 0;
			nofErrors := 0; nofWarnings := 0; nofInformations := 0;
			нов(lock);
			нов(onChanged, НУЛЬ, НУЛЬ, НУЛЬ, НУЛЬ);
			changed := ложь;
			notificationEnabled := истина;
		кон Init;

		(** Inform views about updates *)
		проц EnableNotification*;
		нач
			AcquireWrite;
			notificationEnabled := истина;
			ReleaseWrite;
		кон EnableNotification;

		(** Don't inform views about updates *)
		проц DisableNotification*;
		нач
			AcquireWrite;
			notificationEnabled := ложь;
			ReleaseWrite;
		кон DisableNotification;

		(** Acquire read lock. *)
		проц AcquireRead*;
		нач
			lock.AcquireRead;
		кон AcquireRead;

		(** Release read lock *)
		проц ReleaseRead*;
		нач
			lock.ReleaseRead;
		кон ReleaseRead;

		(** Acquire write lock *)
		проц AcquireWrite*;
		нач
			lock.AcquireWrite;
		кон AcquireWrite;

		(** Release write lock. If the data has changed, all listeners will be notified when the last
			writer releases its lock *)
		проц ReleaseWrite*;
		перем notifyListeners : булево;
		нач
			(* If the last writer releases the lock and the model data has changed, we have to notify interested listeners *)
			notifyListeners := notificationEnabled и (lock.GetWLockLevel() = 1) и changed;
			если notificationEnabled то changed := ложь; всё;
			lock.ReleaseWrite;
			если notifyListeners то
				onChanged.Call(сам);
			всё;
		кон ReleaseWrite;

		(** Dispose all entries *)
		проц Clear*;
		нач
			AcquireWrite;
			changed := changed или (entries # НУЛЬ) или (nofEntries # 0) или (nofErrors # 0) или (nofWarnings # 0) или (nofInformations # 0);
			entries := НУЛЬ;
			nofEntries := 0;
			nofErrors := 0; nofWarnings := 0; nofInformations := 0;
			ReleaseWrite;
		кон Clear;

		(* Make sure that <entries> cannot hold at least one more entry *)
		проц CheckEntriesSize;
		перем newEntries : EntryArray; i : размерМЗ;
		нач
			утв(lock.HasWriteLock());
			если (entries = НУЛЬ) то
				нов(entries, InitialArraySize);
			аесли (nofEntries >= длинаМассива(entries)) то
				нов(newEntries, 2 * длинаМассива(entries));
				нцДля i := 0 до nofEntries - 1 делай
					newEntries[i] := entries[i];
				кц;
				entries := newEntries;
			всё;
		кон CheckEntriesSize;

		проц {перекрыта}Error*(конст source : массив из симв8; position : Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач
			Add(TypeError, source, position, message, nofErrors)
		кон Error;

		проц {перекрыта}Warning*(конст source : массив из симв8; position : Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач
			Add(TypeWarning, source, position, message, nofWarnings);
		кон Warning;

		проц {перекрыта}Information*(конст source : массив из симв8; position : Потоки.ТипМестоВПотоке; конст message : массив из симв8);
		нач
			Add(TypeInformation, source, position, message, nofInformations);
		кон Information;

		проц Exists(type: целМЗ; position : Потоки.ТипМестоВПотоке; конст message: массив из симв8) : булево;
		перем i : размерМЗ;
		нач
			i := 0;
			нцПока (i < nofEntries) и ((entries[i].type # type) или (entries[i].position # position) или (entries[i].message # message)) делай
				увел(i);
			кц;
			возврат (nofEntries > 0) и (i < nofEntries);
		кон Exists;

		проц Add(type: целМЗ; конст source : массив из симв8; position : Потоки.ТипМестоВПотоке; конст message : массив из симв8; перем counter: размерМЗ);
		перем путь, имяФайлаБезПути : Files.FileName;
		нач
			AcquireWrite;
			если ~Exists(type, position, message) то
				CheckEntriesSize;
				entries[nofEntries].type := type;
				копируйСтрокуДо0(source, entries[nofEntries].source);
				
				Files.SplitPath(source, путь, имяФайлаБезПути);
				
				entries[nofEntries].текущийФайлЛи := имяФайлаБезПути = имяТекущегоФайла;
				entries[nofEntries].position := position;
				копируйСтрокуДо0(message, entries[nofEntries].message);
				увел(nofEntries); увел(counter);
				changed := истина;
			всё;
			ReleaseWrite;
		кон Add;

		(* Returns a string summarizing the number of errors and warnings *)
		проц GetSummary(перем summary : массив из симв8);
		перем nbr : массив 8 из симв8;
		нач
			AcquireRead;
			summary := "";
			если (nofErrors > 0) то
				Strings.IntToStr(nofErrors, nbr); Strings.Append(summary, nbr);
				Strings.Append(summary, " ошиб.");
			иначе
				summary := "нет ошибок";
			всё;

			если (nofWarnings > 0) то
				Strings.Append(summary, ", "); Strings.IntToStr(nofWarnings, nbr);
				Strings.Append(summary, nbr);
				Strings.Append(summary, " предупрежд.");
			всё;
			ReleaseRead;
		кон GetSummary;

		проц Synchronize(перем entries : ViewEntryArray; перем nofEntries : цел32);
		перем dest : размерМЗ;

			проц Insert(type: целМЗ);
			перем i: размерМЗ;
			нач
				нцДля i := 0 до nofEntries - 1 делай
					если сам.entries[i].type = type то
						entries[dest].type := сам.entries[i].type;
						entries[dest].position := сам.entries[i].position;
						entries[dest].source := сам.entries[i].source;
						entries[dest].message := сам.entries[i].message;
						entries[dest].текущийФайлЛи := сам.entries[i].текущийФайлЛи;
						увел(dest);
					всё;
				кц;
			кон Insert;

		нач
			AcquireRead;
			если (сам.entries = НУЛЬ) то
				nofEntries := 0;
			иначе
				если (entries = НУЛЬ) или (длинаМассива(сам.entries) > длинаМассива(entries)) то нов(entries, длинаМассива(сам.entries)); всё;
				nofEntries := сам.nofEntries;
				Insert(Diagnostics.TypeError);
				Insert(Diagnostics.TypeWarning);
				Insert(Diagnostics.TypeInformation);
			всё;
			ReleaseRead;
		кон Synchronize;

	кон Model;

тип

	CompareProcedure = проц(конст entry1, entry2 : Entry) : целМЗ;


	ViewEntry* = запись (Entry)
		pos- : укль на массив из WMTextView.PositionMarker;
	кон;

	ViewEntryArray = укль на массив из ViewEntry;

	CellInfo* = окласс(WMGrids.CellPositionInfo)
	перем
		entryValid- : булево;
		entry- : ViewEntry;

		проц &Init(entryValid : булево; конст entry : ViewEntry; column, row : размерМЗ);
		нач
			сам.entryValid := entryValid;
			сам.entry := entry;
			pos.col := column;
			pos.row := row;
		кон Init;

	кон CellInfo;

тип

	DiagnosticsView* = окласс(WMGrids.GenericGrid)
	перем
		showMarkers- : WMProperties.BooleanProperty;
		showMarkersI : булево;

		sortBy- : WMProperties.Int32Property;
		sortByI : цел32;

		entries : ViewEntryArray;
		nofEntries : цел32;

		model : Model;
		textViews : укль на массив из WMTextView.TextView;

		summary : массив 256 из симв8;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrDiagnosticsView);
			SetDrawCellProc(DrawCell);
			нов(showMarkers, PrototypeShowMarkers, НУЛЬ, НУЛЬ); properties.Add(showMarkers);
			showMarkersI := showMarkers.Get();
			нов(sortBy, PrototypeSortBy, НУЛЬ, НУЛЬ); properties.Add(sortBy);
			sortByI := sortBy.Get();
			entries := НУЛЬ;
			nofEntries := 0;
			model := НУЛЬ;
			textViews := НУЛЬ;
			summary := "";
			nofCols.Set(3);
			nofRows.Set(1);
			onClick.Add(OnClickHandler);
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = showMarkers) то
				ShowMarkers(showMarkers.Get());
			аесли (property = sortBy) то
				SortBy(sortBy.Get());
			иначе
				PropertyChanged^(sender, property);
			всё
		кон PropertyChanged;

		проц {перекрыта}RecacheProperties*;
		нач
			RecacheProperties^;
			ShowMarkers(showMarkers.Get());
			SortBy(sortBy.Get());
		кон RecacheProperties;

		проц OnClickHandler(sender, data : динамическиТипизированныйУкль);
		конст Position = 0;
		перем column, row : размерМЗ; newMode: целМЗ;
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.OnClickHandler, sender, data)
			иначе
				если (data # НУЛЬ) и (data суть CellInfo) и ~(data(CellInfo).entryValid) то
					column := data(CellInfo).pos.col;
					row := data(CellInfo).pos.row;
					если (row = 0) то
						просей sortByI из
							|SortByTypeAscending:
								если (column = Position) то
									newMode := SortByTypeDescending;
								всё;
							|SortByTypeDescending:
								если (column = Position) то
									newMode := SortByTypeAscending;
								всё;
							|SortByPositionAscending:
								если (column = Position) то
									newMode := SortByPositionDescending;
								всё;
							|SortByPositionDescending:
								если (column = Position) то
									newMode := SortByPositionAscending;
								всё;
						иначе
							newMode := sortByI; (* don't change *)
						всё;
						sortBy.Set(newMode);
					всё;
				всё;
			всё;
		кон OnClickHandler;

		проц ShowMarkers(enable : булево);
		перем i, j : размерМЗ;
		нач
			Acquire;
			если (enable # showMarkersI) то
				showMarkersI := enable;
				если (textViews # НУЛЬ) то
					нцДля i := 0 до nofEntries - 1 делай
						нцДля j := 0 до длинаМассива(textViews) - 1 делай
							если (entries[i].pos[j] # НУЛЬ) то
								entries[i].pos[j].SetVisible(showMarkersI);
							всё;
						кц;
					кц;
				всё;
				Invalidate;
			всё;
			Release;
		кон ShowMarkers;

		проц SortBy(mode : целМЗ);
		нач
			утв(
				(mode = SortByTypeAscending) или (mode = SortByTypeDescending) или
				(mode = SortByPositionAscending) или (mode = SortByPositionDescending)
			);
			Acquire;
			если (mode # sortByI) то
				sortByI := mode;
				просей sortByI из
					|SortByTypeAscending: SortEntries(CompareByType, истина);
					|SortByTypeDescending: SortEntries(CompareByType, ложь);
					|SortByPositionAscending: SortEntries(CompareByPosition, истина);
					|SortByPositionDescending: SortEntries(CompareByPosition, ложь);
				всё;
				Invalidate;
			всё;
			Release;
		кон SortBy;

		проц {перекрыта}GetCellData*(column, row : размерМЗ) : динамическиТипизированныйУкль; (* override *)
		перем info : CellInfo; entryValid : булево; entry : ViewEntry;
		нач
			Acquire;
			если (0 <= row - 1) и (row - 1 < nofEntries) то
				entryValid := истина;
				entry := entries[row - 1];
			иначе
				entryValid := ложь;
			всё;
			нов(info, entryValid, entry, column, row);
			Release;
			возврат info;
		кон GetCellData;

		проц AddPositionMarkers;
		перем picture : Files.FileName; i, j : размерМЗ;
		нач (* caller holds lock *)
			если (textViews # НУЛЬ) то
				нцДля i := 0 до nofEntries - 1 делай

					если (entries[i].type = Diagnostics.TypeError) то
						picture := PictureError;
					аесли (entries[i].type = Diagnostics.TypeWarning) то
						picture := PictureWarning;
					иначе
						picture := "";
					всё;

					если entries[i].текущийФайлЛи и (entries[i].position # Потоки.НевернаяПозицияВПотоке) и (textViews # НУЛЬ) то
						нов(entries[i].pos, длинаМассива(textViews));
						нцДля j := 0 до длинаМассива(textViews) - 1 делай
							entries[i].pos[j] := textViews[j].CreatePositionMarker();
							если (picture # "") то entries[i].pos[j].Load(picture); всё;
							entries[i].pos[j].SetPosition(entries[i].position(размерМЗ));
						кц;
					всё;

				кц;
			всё;
		кон AddPositionMarkers;

		проц RemovePositionMarkers;
		перем i, j : размерМЗ;
		нач (* caller holds lock *)
			если (textViews # НУЛЬ) то
				нцДля i := 0 до nofEntries-1 делай
					нцДля j := 0 до длинаМассива(textViews)-1 делай
						если (entries[i].pos # НУЛЬ) и (entries[i].pos[j] # НУЛЬ) то
							textViews[j].RemovePositionMarker(entries[i].pos[j]);
							entries[i].pos[j] := НУЛЬ;
						всё;
					кц;
				кц;
			всё;
		кон RemovePositionMarkers;

		проц GetFirstPosition*(перем positions : массив из размерМЗ; перем type: цел32);
		перем i : размерМЗ;
		нач
			Acquire;
			утв((textViews # НУЛЬ) и (длинаМассива(textViews) = длинаМассива(positions)));
			если (nofEntries > 0) то
				нцДля i := 0 до длинаМассива(textViews)-1 делай
					если (entries[0].pos # НУЛЬ) и (entries[0].pos[i] # НУЛЬ) то
						positions[i] := entries[0].pos[i].GetPosition();
					иначе
						positions[i] := Потоки.НевернаяПозицияВПотоке;
					всё;
					type := entries[0].type;
				кц;
			иначе
				нцДля i := 0 до длинаМассива(positions)-1 делай positions[i] := Потоки.НевернаяПозицияВПотоке; кц;
			всё;
			Release;
		кон GetFirstPosition;

		проц GetNearestPosition*(cursorPosition, editorIndex : размерМЗ; forward : булево; перем nearestPosition : размерМЗ; перем number : размерМЗ);
		перем pos, i : размерМЗ;
		нач
			Acquire;
			утв((textViews # НУЛЬ) и (0 <= editorIndex) и (editorIndex < длинаМассива(textViews)));
			nearestPosition := -1; number := 1; (* row 0 is grid title *)
			i := 0;
			нц
				если (i >= nofEntries) то прервиЦикл; всё;
				если (entries[i].pos # НУЛЬ) и (entries[i].pos[editorIndex] # НУЛЬ) то
					pos := entries[i].pos[editorIndex].GetPosition();
					если forward и (pos > cursorPosition) то
						если (nearestPosition = -1) или (pos < nearestPosition) то nearestPosition := pos; number := i+1; всё;
					аесли ~forward и (pos < cursorPosition) то
						если (nearestPosition = -1) или (pos > nearestPosition) то nearestPosition := pos; number := i+1; всё всё всё;
				увел(i) кц;
			если (nearestPosition = -1) то
				nearestPosition := cursorPosition;
				если forward и (i > 1) то (* select maximum row *) number := i; всё всё;
			Release кон GetNearestPosition;

		проц SelectEntry*(number : размерМЗ; moveTo : булево);
		нач
			Acquire;
			трассируй(number, moveTo);
			если (0 <= number) и (number <= nofEntries) то
				SetSelection(0, number(цел32), 2, number(цел32));
				если moveTo то
					SetTopPosition(0, number(цел32), истина);
				всё;
			всё;
			Release;
		кон SelectEntry;

		проц SortEntries(compare : CompareProcedure; ascending : булево);
		перем result, i, j : цел32; temp : ViewEntry;
		нач
			(* caller must hold lock *)
			утв(compare # НУЛЬ);
			если (nofEntries > 1) то
				(* bubble sort *)
				нцДля i := 0 до nofEntries - 1 делай
					нцДля j := 1 до nofEntries - 1 делай
						result := compare(entries[j-1], entries[j]);
						если (ascending и (result = Greater)) или (~ascending и (result = Less)) то
							temp := entries[j - 1];
							entries[j - 1] := entries[j];
							entries[j] := temp;
						всё;
					кц;
				кц;
			всё;
		кон SortEntries;

		проц SetTextViews*(конст textViews : массив из WMTextView.TextView);
		перем i : размерМЗ;
		нач
			Acquire;
			RemovePositionMarkers;
			нов(сам.textViews, длинаМассива(textViews));
			нцДля i := 0 до длинаМассива(textViews)-1 делай
				утв(textViews[i] # НУЛЬ);
				сам.textViews[i]:= textViews[i];
			кц;
			AddPositionMarkers;
			Release;
		кон SetTextViews;

		проц SetModel*(model : Model);
		нач
			Acquire;
			если (сам.model # НУЛЬ) то сам.model.onChanged.Remove(ModelChanged); всё;
			сам.model := model;
			если (сам.model # НУЛЬ) то сам.model.onChanged.Add(ModelChanged); всё;
			Release;
			Invalidate;
		кон SetModel;

		проц ModelChanged(sender, data : динамическиТипизированныйУкль);
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.ModelChanged, sender, data);
			иначе
				RemovePositionMarkers;
				model.AcquireRead;
				model.Synchronize(entries, nofEntries);
				model.GetSummary(summary);
				model.ReleaseRead;
				nofRows.Set(nofEntries + 1); (* 1 for title row *)
				AddPositionMarkers;
				SetTopPosition(0, 0, истина);
				Invalidate;
			всё;
		кон ModelChanged;

		проц DrawCell(canvas : WMGraphics.Canvas; w, h : размерМЗ; state : мнвоНаБитахМЗ; x, y : размерМЗ);
		перем color: WMGraphics.Color; str : массив 1024 из симв8;
		нач
			color := WMGraphics.RGBAToColor(255, 255, 255, 255);
			если state * {WMGrids.CellFixed, WMGrids.CellSelected} = {WMGrids.CellFixed, WMGrids.CellSelected} то
				color := WMGraphics.RGBAToColor(0, 128, 255, 255)
			аесли WMGrids.CellFixed в state то
				color := WMGraphics.RGBAToColor(196, 196, 196, 255)
			аесли WMGrids.CellSelected в state то
				color := WMGraphics.RGBAToColor(196, 196, 255, 255)
			всё;
			canvas.SetColor(WMGraphics.RGBAToColor(0, 0, 0, 255));
			canvas.SetFont(WMBitmapFont.bimbofont);

			canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), color, WMGraphics.ModeCopy);
			если (WMGrids.CellFocused в state) и ~(WMGrids.CellHighlighted в state) то
				WMGraphicUtilities.DrawBevel(canvas, WMRectangles.MakeRect(0, 0, w, h), 1, истина, WMGraphics.RGBAToColor(0, 0, 0, 196),
				WMGraphics.ModeSrcOverDst)
			всё;

			если y = 0 то
				просей x из
					| 0 : str := "смещ"
					| 1 : str := "код"
					| 2 :
						str := "детали";
						Strings.Append(str, " ("); Strings.Append(str, summary); Strings.Append(str, ")");
				иначе
				всё;
				canvas.DrawString(4, h - 4, str);
			аесли (0 <= y - 1) и (y - 1 < nofEntries) то
				просей x из
					| 0 :
						если entries[y - 1].текущийФайлЛи и (entries[y - 1].pos # НУЛЬ) и (entries[y - 1].pos[0] # НУЛЬ) то 
							Strings.IntToStr(entries[y - 1].pos[0].GetPosition(), str);
						иначе
							(* это сработает в случае, если файл не текущий *)
							копируйСтрокуДо0(entries[y-1].source, str) всё;
						если entries[y - 1].type = TypeError то
							canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), ColorError, WMGraphics.ModeSrcOverDst);
						аесли entries[y - 1].type = TypeWarning то
							canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), ColorWarning, WMGraphics.ModeSrcOverDst);
						аесли entries[y - 1].type = TypeInformation то
							canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), ColorPCPosition, WMGraphics.ModeSrcOverDst);
						аесли entries[y - 1].type = TypeInformation то
							(* do nothing *)
						всё;
					| 2 : копируйСтрокуДо0(entries[y - 1].message, str)
				иначе
				всё;
				canvas.DrawString(4, h - 4, str);
			всё;

		кон DrawCell;

	кон DiagnosticsView;

перем
	StrDiagnosticsView : Strings.String;

	PrototypeShowMarkers : WMProperties.BooleanProperty;
	PrototypeSortBy : WMProperties.Int32Property;

проц CompareByPosition(конст e1, e2 : Entry) : целМЗ;
перем result : целМЗ;
нач
	если (e1.position < e2.position) то result := Less;
	аесли (e1.position > e2.position) то result := Greater;
	иначе
		result := Equal;
	всё;
	возврат result;
кон CompareByPosition;

проц CompareByType(конст e1, e2 : Entry) : целМЗ;
перем result : целМЗ;
нач
	если (e1.type < e2.type) то result := Less;
	аесли (e1.type > e2.type) то result := Greater;
	иначе
		result := CompareByPosition(e1, e2);
	всё;
	возврат result;
кон CompareByType;

проц InitStrings;
нач
	StrDiagnosticsView := Strings.NewString("DiagnosticsView");
кон InitStrings;

проц InitPrototypes;
нач
	нов(PrototypeShowMarkers, НУЛЬ, Strings.NewString("ShowMarkers"), Strings.NewString("Подсвечивать ошибки в исх.тексте?"));
	PrototypeShowMarkers.Set(истина);
	нов(PrototypeSortBy, НУЛЬ, Strings.NewString("SortBy"), Strings.NewString("Сорт. табл по: 0=типу | 1=месту | 2=коду"));
	PrototypeSortBy.Set(SortByTypeDescending);
кон InitPrototypes;

нач
	InitStrings;
	InitPrototypes;
кон WMDiagnostics.
