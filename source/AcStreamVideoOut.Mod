модуль AcStreamVideoOut; (** AUTHOR ""; PURPOSE ""; *)

использует
	Channels;

конст
	CmdSetEnabled = 0;
	CmdSetHorizActiveSize = 1;
	CmdSetHorizFrontPorch = 2;
	CmdSetHorizSyncWidth = 3;
	CmdSetHorizBackPorch = 4;
	CmdSetHorizSyncPolarity = 5;

	CmdSetVertActiveSize = 6;
	CmdSetVertFrontPorch = 7;
	CmdSetVertSyncWidth = 8;
	CmdSetVertBackPorch = 9;
	CmdSetVertSyncPolarity = 10;

тип
	Controller* = запись
		cfg-: портОбменаДанными дляВывода; (** configuration port *)
		enabled-: булево; (** TRUE IF the PWM output is enabled *)

		pixelClock-: вещ32; (** pixel clock in Hz *)

		horizActiveSize-: цел32;
		horizFrontPorch-: цел32;
		horizSyncWidth-: цел32;
		horizBackPorch-: цел32;
		horizSyncPolarity-: булево;

		vertActiveSize-: цел32;
		vertFrontPorch-: цел32;
		vertSyncWidth-: цел32;
		vertBackPorch-: цел32;
		vertSyncPolarity-: булево;
	кон;

	проц InitController*(перем ctl: Controller; cfg: портОбменаДанными дляВывода; pixelClock: вещ32);
	нач
		ctl.cfg := cfg;
		ctl.pixelClock := pixelClock;
		Enable(ctl,ложь);
(*
		(* setup default video output settings *)
		SetHorizActiveSize(ctl,1024);z
		SetHorizFrontPorch(ctl,24);
		SetHorizSyncWidth(ctl,136);
		SetHorizBackPorch(ctl,160);
		SetHorizSyncPolarity(ctl,true);

		SetVertActiveSize(ctl,768);
		SetVertFrontPorch(ctl,3);
		SetVertSyncWidth(ctl,6);
		SetVertBackPorch(ctl,29);
		SetVertSyncPolarity(ctl,true);
*)
	кон InitController;

	(**
		Enable/disable video output
	*)
	проц Enable*(перем ctl: Controller; enable: булево);
	нач
		если enable то
			ctl.cfg << CmdSetEnabled + логСдвиг(1,4);
		иначе
			ctl.cfg << CmdSetEnabled;
		всё;
		ctl.enabled := enable;
	кон Enable;

	проц SetHorizActiveSize*(перем ctl: Controller; n: цел32);
	нач
		ctl.cfg << CmdSetHorizActiveSize + логСдвиг(n-1,4);
		ctl.horizActiveSize := n;
	кон SetHorizActiveSize;

	проц SetHorizFrontPorch*(перем ctl: Controller; n: цел32);
	нач
		ctl.cfg << CmdSetHorizFrontPorch + логСдвиг(n-1,4);
		ctl.horizFrontPorch := n;
	кон SetHorizFrontPorch;

	проц SetHorizSyncWidth*(перем ctl: Controller; n: цел32);
	нач
		ctl.cfg << CmdSetHorizSyncWidth + логСдвиг(n-1,4);
		ctl.horizSyncWidth := n;
	кон SetHorizSyncWidth;

	проц SetHorizBackPorch*(перем ctl: Controller; n: цел32);
	нач
		ctl.cfg << CmdSetHorizBackPorch + логСдвиг(n-1,4);
		ctl.horizBackPorch := n;
	кон SetHorizBackPorch;

	проц SetHorizSyncPolarity*(перем ctl: Controller; polarity: булево);
	перем n: цел32;
	нач
		если polarity то n := 1; иначе n := 0; всё;
		ctl.cfg << CmdSetHorizSyncPolarity + логСдвиг(n,4);
		ctl.horizSyncPolarity := polarity;
	кон SetHorizSyncPolarity;

	проц SetVertActiveSize*(перем ctl: Controller; n: цел32);
	нач
		ctl.cfg << CmdSetVertActiveSize + логСдвиг(n-1,4);
		ctl.vertActiveSize := n;
	кон SetVertActiveSize;

	проц SetVertFrontPorch*(перем ctl: Controller; n: цел32);
	нач
		ctl.cfg << CmdSetVertFrontPorch + логСдвиг(n-1,4);
		ctl.vertFrontPorch := n;
	кон SetVertFrontPorch;

	проц SetVertSyncWidth*(перем ctl: Controller; n: цел32);
	нач
		ctl.cfg << CmdSetVertSyncWidth + логСдвиг(n-1,4);
		ctl.vertSyncWidth := n;
	кон SetVertSyncWidth;

	проц SetVertBackPorch*(перем ctl: Controller; n: цел32);
	нач
		ctl.cfg << CmdSetVertBackPorch + логСдвиг(n-1,4);
		ctl.vertBackPorch := n;
	кон SetVertBackPorch;

	проц SetVertSyncPolarity*(перем ctl: Controller; polarity: булево);
	перем n: цел32;
	нач
		если polarity то n := 1; иначе n := 0; всё;
		ctl.cfg << CmdSetVertSyncPolarity + логСдвиг(n,4);
		ctl.vertSyncPolarity := polarity;
	кон SetVertSyncPolarity;

кон AcStreamVideoOut.
