модуль WMShapes; (** AUTHOR "staubesv, PH"; PURPOSE "Basic geormetric shapes as visual components"; *)
(*! to do: thread safety*)
использует
	Strings, XML, WMRectangles, WMGraphics, WMGraphicUtilities, WMProperties, WMComponents, Math, ЛогЯдра;

тип
	(* generic line. can have an arrowhead on either end *)
	Line* = окласс(WMComponents.VisualComponent)
	перем
		color- : WMProperties.ColorProperty;
		colorI : WMGraphics.Color;

		isVertical- : WMProperties.BooleanProperty;
		isVerticalI : булево;

		start-, end-: WMProperties.PointProperty;
		startI, endI: WMGraphics.Point2d;

		arrowAtStart-, arrowAtEnd-:WMProperties.BooleanProperty;
		arrowAtStartI, arrowAtEndI: булево;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMShapes.GenLine");
			SetNameAsString(StrLine);
			нов(color, НУЛЬ, StrColor, StrLineColorDescription); properties.Add(color);
			color.Set(WMGraphics.Black); colorI := color.Get();
			нов(isVertical, НУЛЬ, StrIsVertical, StrIsVerticalDescription); properties.Add(isVertical);
			isVertical.Set(ложь); isVerticalI := isVertical.Get();
			нов(start, НУЛЬ, StrStart, StrStartDescription); properties.Add(start);
			start.SetCoordinate(0,0); startI := start.Get();
			нов(end, НУЛЬ, StrEnd, StrEndDescription); properties.Add(end);
			end.SetCoordinate(100,100); endI := end.Get();
			нов(arrowAtStart, НУЛЬ, StrArrowStart, StrArrowStartDescription); properties.Add(arrowAtStart);
			arrowAtStart.Set(ложь); arrowAtStartI := arrowAtStart.Get();
			нов(arrowAtEnd, НУЛЬ, StrArrowEnd, StrArrowEndDescription); properties.Add(arrowAtEnd);
			arrowAtEnd.Set(истина); arrowAtEndI := arrowAtEnd.Get();
			PropertyChanged(сам, start); (* recompute bounding box *)
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		перем dx, dy :размерМЗ; rect, rect0: WMRectangles.Rectangle;
		нач
			если (property = color) то colorI := color.Get(); Invalidate;
			аесли (property = isVertical) то isVerticalI := isVertical.Get(); Invalidate;
			аесли (property = start) или (property = end) то
				rect0:=bounds.Get();	startI := start.Get(); endI := end.Get();

				rect:=WMRectangles.MakeRect(матМинимум(startI.x,endI.x)+rect0.l-5, матМинимум(startI.y,endI.y)+rect0.t-5, матМаксимум(startI.x, endI.x)+rect0.l+5, матМаксимум(startI.y,endI.y)+rect0.t+5); (* add border for arrowhead display *)

				dx:=rect.l - rect0.l;
				dy:=rect.t - rect0.t;
				startI.x:=startI.x-dx; endI.x:=endI.x-dx;
				startI.y:=startI.y-dy; endI.y:=endI.y-dy;

				если ~WMRectangles.IsEqual(rect,rect0) то bounds.Set(rect); всё;
				start.Set(startI);
				end.Set(endI);
				Invalidate;
			аесли (property = arrowAtStart) то arrowAtStartI := arrowAtStart.Get(); Invalidate;
			аесли (property = arrowAtEnd) то	arrowAtEndI := arrowAtEnd.Get(); Invalidate;
			иначе PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		(* position a line line in parent coordinates*)
		проц Set*(x0,y0, x1, y1: цел32);
		перем rect:WMRectangles.Rectangle; changed:булево;
		нач
			rect:=bounds.Get();
			changed:=ложь;
			если (x0 # rect.l + startI.x) или (y0#rect.t+startI.y) то start.SetCoordinate(x0-rect.l, y0-rect.t); changed:=истина всё;
			если (x1 # rect.l + endI.x) или (y1#rect.t+endI.y) то end.SetCoordinate(x1-rect.l, y1-rect.t); changed:=истина всё;
			если changed то	PropertyChanged(сам, start); PropertyChanged(сам, end) всё;
		кон Set;

		(** Return if the line is hit at (x, y) in parent coordinates *)
		проц {перекрыта}IsHit*(x, y: размерМЗ): булево;
		перем r: WMRectangles.Rectangle; X0,Y0, X1,Y1: размерМЗ; hit:булево;
		нач
			если ~visible.Get() то hit:= ложь
			иначе
				r:=GetClientRect();
				X0:=startI.x+r.l; Y0:=startI.y+r.t;
				X1:=endI.x+r.l; Y1:=endI.y+r.t;
				если X0=X1 то hit:=WMRectangles.PointInRect(x, y, r) и (2>матМодуль(x-X0))
				аесли Y0=Y1 то hit:=WMRectangles.PointInRect(x, y, r) и (2>матМодуль(y-Y0))
				иначе hit:= WMRectangles.PointInRect(x, y, r) и (2>матМодуль((y-Y0) - ((x-X0)*(Y1-Y0)/(X1-X0))))
				всё;
			всё;
			возврат hit;
		кон IsHit;

		проц SetArrowheads*(arrows:мнвоНаБитахМЗ);
		нач
			если (0 в arrows)#arrowAtStartI то arrowAtStart.Set(0 в arrows); PropertyChanged(сам, arrowAtStart); всё;
			если (1 в arrows)#arrowAtEndI то arrowAtEnd.Set(1 в arrows); PropertyChanged(сам, arrowAtEnd); всё;
		кон SetArrowheads;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		конст pi=3.1516; headscale= 0.25;
		перем  alpha: вещ32;
			dx,dy: размерМЗ;
			size:цел32; head: вещ64;
		нач
			DrawBackground^(canvas);
			если (colorI # 0) то
				dx:=endI.x-startI.x; dy:=endI.y-startI.y;
				alpha:=arctan2(dx,dy);
				size:= 40; (*! to do: parametrize arrow size *)
				head:=size * headscale (*  + 2 *);
				canvas.Line(startI.x, startI.y, endI.x, endI.y, colorI, WMGraphics.ModeSrcOverDst);
				если arrowAtEndI то
					canvas.Line(endI.x,endI.y, endI.x - округлиВниз(0.5+head * Math.cos(alpha + pi/8)), endI.y - округлиВниз(0.5+head * Math.sin(alpha + pi/8)), colorI, WMGraphics.ModeSrcOverDst);
					canvas.Line(endI.x,endI.y, endI.x - округлиВниз(0.5+head * Math.cos(alpha - pi/8)), endI.y - округлиВниз(0.5+head * Math.sin(alpha - pi/8)), colorI, WMGraphics.ModeSrcOverDst);
				всё;
				если arrowAtStartI то
					canvas.Line(startI.x,startI.y, startI.x + округлиВниз(0.5+head * Math.cos(alpha + pi/8)), startI.y + округлиВниз(0.5+head * Math.sin(alpha + pi/8)), colorI, WMGraphics.ModeSrcOverDst);
					canvas.Line(startI.x,startI.y, startI.x + округлиВниз(0.5+head * Math.cos(alpha - pi/8)), startI.y + округлиВниз(0.5+head * Math.sin(alpha - pi/8)), colorI, WMGraphics.ModeSrcOverDst);
				всё
			всё;
		кон DrawBackground;

	кон Line;

тип

	Rectangle* = окласс(WMComponents.VisualComponent)
	перем
		clBorder- : WMProperties.ColorProperty;
		clBorderI : WMGraphics.Color;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMShapes.GenRectangle");
			SetNameAsString(StrRectangle);
			нов(clBorder, НУЛЬ, StrClBorder, StrClBorderDescription); properties.Add(clBorder);
			clBorder.Set(WMGraphics.Black); clBorderI := clBorder.Get();
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = clBorder) то
				clBorderI := clBorder.Get();
				Invalidate;
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем rect : WMRectangles.Rectangle;
		нач
			DrawBackground^(canvas);
			если (clBorderI # 0) то
				rect := GetClientRect();
				WMGraphicUtilities.DrawRect(canvas, rect, clBorderI, WMGraphics.ModeSrcOverDst);
			всё;
		кон DrawBackground;

	кон Rectangle;

тип

	Circle* = окласс(WMComponents.VisualComponent)
	перем
		color : WMProperties.ColorProperty;
		colorI : WMGraphics.Color;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMShapes.GenCircle");
			SetNameAsString(StrCircle);
			нов(color, НУЛЬ, Strings.NewString("Color"), Strings.NewString("Color")); properties.Add(color);
			color.Set(WMGraphics.Black); colorI := color.Get();
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = color) то
				colorI := color.Get();
				Invalidate;
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем rect : WMRectangles.Rectangle; radius : размерМЗ;
		нач
			DrawBackground^(canvas);
			если (colorI # 0) то
				rect := bounds.Get();
				canvas.SetColor(colorI);
				radius := матМинимум((rect.r - rect.l) DIV 2, (rect.b - rect.t) DIV 2) - 1;
				WMGraphicUtilities.Circle(canvas, (rect.r - rect.l) DIV 2, (rect.b - rect.t) DIV 2, radius);
			всё;
		кон DrawBackground;

	кон Circle;

тип

	Ellipse* = окласс(WMComponents.VisualComponent)
	перем
		color : WMProperties.ColorProperty;
		colorI : WMGraphics.Color;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMShapes.GenEllipse");
			SetNameAsString(StrEllipse);
			нов(color, НУЛЬ, StrColor, StrColorDescription);
			color.Set(WMGraphics.Black); colorI := color.Get();
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = color) то
				colorI := color.Get();
				Invalidate;
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем rect : WMRectangles.Rectangle;
		нач
			DrawBackground^(canvas);
			если (colorI # 0) то
				rect := bounds.Get();
				canvas.SetColor(colorI);
				WMGraphicUtilities.Ellipse(canvas, (rect.r - rect.l) DIV 2, (rect.b - rect.t) DIV 2, (rect.r - rect.l) DIV 2 - 1, (rect.b - rect.t) DIV 2 - 1);
			всё;
		кон DrawBackground;

	кон Ellipse;

перем
	StrLine, StrRectangle, StrCircle, StrEllipse : Strings.String;
	StrClBorder, StrClBorderDescription, StrColor, StrColorDescription, StrLineColorDescription,
	StrIsVertical, StrIsVerticalDescription,
	StrStart,StrEnd,StrArrowStart, StrArrowEnd,
	StrStartDescription, StrEndDescription, StrArrowStartDescription,StrArrowEndDescription: Strings.String;

проц GenLine*() : XML.Element;
перем line : Line;
нач
	нов(line); возврат line;
кон GenLine;

проц GenRectangle*() : XML.Element;
перем rectangle : Rectangle;
нач
	нов(rectangle); возврат rectangle;
кон GenRectangle;

проц GenCircle*() : XML.Element;
перем circle : Circle;
нач
	нов(circle); возврат circle;
кон GenCircle;

проц GenEllipse*() : XML.Element;
перем ellipse : Ellipse;
нач
	нов(ellipse); возврат ellipse;
кон GenEllipse;

проц InitStrings;
нач
	StrLine := Strings.NewString("Line");
	StrRectangle := Strings.NewString("StrRectangle");
	StrCircle := Strings.NewString("StrCircle");
	StrEllipse := Strings.NewString("StrEllipse");
	StrClBorder := Strings.NewString("ClBorder");
	StrClBorderDescription := Strings.NewString("Border color");
	StrColor := Strings.NewString("Color");
	StrColorDescription := Strings.NewString("Color");
	StrLineColorDescription := Strings.NewString("Color of line");
	StrStart := Strings.NewString("LineStart");
	StrStartDescription := Strings.NewString("start point of line");
	StrEnd := Strings.NewString("LineEnd");
	StrEndDescription := Strings.NewString("end point of line");
	StrArrowStart := Strings.NewString("ArrowAtStart");
	StrArrowStartDescription := Strings.NewString("arrows at start of line ?");
	StrArrowEnd := Strings.NewString("ArrowAtEnd");
	StrArrowEndDescription := Strings.NewString("arrows at end of line ?");
	StrIsVertical := Strings.NewString("IsVertical");
	StrIsVerticalDescription := Strings.NewString("Horizontal or vertical line?");
кон InitStrings;

проц arctan2(x,y: вещ32): вещ32; (*arctan in range 0..2pi*)
	нач
		если (x>0) и (y>=0) то возврат Math.arctan(y/x)
		аесли (x>0) и (y<0) то возврат Math.arctan(y/x)+2*Math.pi
		аесли x<0 то возврат Math.arctan(y/x)+Math.pi
		аесли (x=0) и (y>0) то возврат Math.pi/2
		аесли (x=0) и (y<0) то возврат 3*Math.pi/2
		иначе (*( x=0) & (y=0) *) возврат 0 (*or RETURN NaN ?*)
		всё
	кон arctan2;

нач
	InitStrings;
кон WMShapes.

System.FreeDownTo WMShapes ~
