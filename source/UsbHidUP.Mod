модуль UsbHidUP; (** AUTHOR "ottigerm"; PURPOSE "HID Items usage pages" *)
(**
 * Bluebottle USB HID Items Usage Pages for Generic Desktop Controls, Keyboard/Keypad and Button
 *
 * This module decodes usages for usage pages:
 *	Generic Desktop Controls
 *	Keyboard/Keypad
 *	Led
 *	Button
 *	Consumer
 *
 * References:
 *	USB, HID Usage Tables, 21.01.2005 Version 1.12
 *
 * Overview:
 *	Generic Desktop Controls(01H)	p. HID Usage Tables
 *	Keyboard/Keypad(07H)			p. HID Usage Tables
 *	Led(08H)						p. HID Usage Tables
 *	Button(09H)					p. HID Usage Tables
 *	Consumer(0CH)					p. HID Usage Tables
 *
 * History:
 *
 *	22.01.2007 Version 1.0
 *)

использует ЛогЯдра;

конст

	(** Usage Pages overtaken from HID Usage Tables*)
	GenericDesktopPage* 	= 01H;
	KeyboardPage*			= 06H;
	KeypadPage*			= 07H; (*Same as KeyboardPage*)
	LedPage*				= 08H;
	ButtonPage* 			= 09H;
	ConsumerPage*			= 0CH;

(* print out the decoded usage for usage page Generic Desktop
 * 	param 	pos, usage of the usage page Generic Desktop
 *)
проц PrintGenericDesktop(pos: цел32);
нач
	(*Generic Desktop Page (0x01)*)
	просей pos из
		|00H: ЛогЯдра.пСтроку8("Undefined" );
		|01H: ЛогЯдра.пСтроку8("Pointer" );
		|02H: ЛогЯдра.пСтроку8("Mouse" );
		|03H: ЛогЯдра.пСтроку8("Reserved" );
		|04H: ЛогЯдра.пСтроку8("Joystick" );
		|05H: ЛогЯдра.пСтроку8("Game Pad" );
		|06H: ЛогЯдра.пСтроку8("Keyboard" );
		|07H: ЛогЯдра.пСтроку8("Keypad" );
		|08H: ЛогЯдра.пСтроку8("Multi-axis Controller" );
		|09H: ЛогЯдра.пСтроку8("Tablet PC System Controls" );
		(*|0A-2FH: KernelLog.String("Reserved" );*)
		|30H: ЛогЯдра.пСтроку8("X" );
		|31H: ЛогЯдра.пСтроку8("Y" );
		|32H: ЛогЯдра.пСтроку8("Z" );
		|33H: ЛогЯдра.пСтроку8("Rx" );
		|34H: ЛогЯдра.пСтроку8("Ry" );
		|35H: ЛогЯдра.пСтроку8("Rz" );
		|36H: ЛогЯдра.пСтроку8("Slider" );
		|37H: ЛогЯдра.пСтроку8("Dial" );
		|38H: ЛогЯдра.пСтроку8("Wheel" );
		|39H: ЛогЯдра.пСтроку8("Hat switch" );
		|3AH: ЛогЯдра.пСтроку8("Counted Buffer" );
		|3BH: ЛогЯдра.пСтроку8("Byte Count" );
		|3CH: ЛогЯдра.пСтроку8("Motion Wakeup" );
		|3DH: ЛогЯдра.пСтроку8("Start" );
		|3EH: ЛогЯдра.пСтроку8("Select" );
		|3FH: ЛогЯдра.пСтроку8("Reserved" );
		|40H: ЛогЯдра.пСтроку8("Vx" );
		|41H: ЛогЯдра.пСтроку8("Vy" );
		|42H: ЛогЯдра.пСтроку8("Vz" );
		|43H: ЛогЯдра.пСтроку8("Vbrx" );
		|44H: ЛогЯдра.пСтроку8("Vbry" );
		|45H: ЛогЯдра.пСтроку8("Vbrz" );
		|46H: ЛогЯдра.пСтроку8("Vno" );
		|47H: ЛогЯдра.пСтроку8("Feature Notification" );
		|48H: ЛогЯдра.пСтроку8("Resolution Multiplier DV" );
		(*|49-7FH: KernelLog.String("Reserved" );*)
		|80H: ЛогЯдра.пСтроку8("System Control" );
		|81H: ЛогЯдра.пСтроку8("System Power Down" );
		|82H: ЛогЯдра.пСтроку8("System Sleep" );
		|83H: ЛогЯдра.пСтроку8("System Wake Up" );
		|84H: ЛогЯдра.пСтроку8("System Context Menu" );
		|85H: ЛогЯдра.пСтроку8("System Main Menu" );
		|86H: ЛогЯдра.пСтроку8("System App Menu" );
		|87H: ЛогЯдра.пСтроку8("System Menu Help" );
		|88H: ЛогЯдра.пСтроку8("System Menu Exit" );
		|89H: ЛогЯдра.пСтроку8("System Menu Select" );
		|8AH: ЛогЯдра.пСтроку8("System Menu Right" );
		|8BH: ЛогЯдра.пСтроку8("System Menu Left" );
		|8CH: ЛогЯдра.пСтроку8("System Menu Up" );
		|8DH: ЛогЯдра.пСтроку8("System Menu Down" );
		|8EH: ЛогЯдра.пСтроку8("System Cold Restart" );
		|8FH: ЛогЯдра.пСтроку8("System Warm Restart" );
		|90H: ЛогЯдра.пСтроку8("D-pad Up" );
		|91H: ЛогЯдра.пСтроку8("D-pad Down" );
		|92H: ЛогЯдра.пСтроку8("D-pad Right" );
		|93H: ЛогЯдра.пСтроку8("D-pad Left" );
		(*|94-9FH: KernelLog.String("Reserved" );*)
		|0A0H: ЛогЯдра.пСтроку8("System Dock" );
		|0A1H: ЛогЯдра.пСтроку8("System Undock" );
		|0A2H: ЛогЯдра.пСтроку8("System Setup" );
		|0A3H: ЛогЯдра.пСтроку8("System Break" );
		|0A4H: ЛогЯдра.пСтроку8("System Debugger Break" );
		|0A5H: ЛогЯдра.пСтроку8("Application Break" );
		|0A6H: ЛогЯдра.пСтроку8("Application Debugger Break" );
		|0A7H: ЛогЯдра.пСтроку8("System Speaker Mute" );
		|0A8H: ЛогЯдра.пСтроку8("System Hibernate" );
		(*|A9-AFH: KernelLog.String("Reserved" );*)
		|0B0H: ЛогЯдра.пСтроку8("System Display Invert" );
		|0B1H: ЛогЯдра.пСтроку8("System Display Internal" );
		|0B2H: ЛогЯдра.пСтроку8("System Display External" );
		|0B3H: ЛогЯдра.пСтроку8("System Display Both" );
		|0B4H: ЛогЯдра.пСтроку8("System Display Dual" );
		|0B5H: ЛогЯдра.пСтроку8("System Display Toggle Int/Ext" );
		|0B6H: ЛогЯдра.пСтроку8("System Display Swap Primary/Secondary" );
		|0B7H: ЛогЯдра.пСтроку8("System Display LCD Autoscale" );
		иначе ЛогЯдра.пСтроку8("Reserved");
	всё;
кон PrintGenericDesktop;

(* Print out the decoded usage for usage page Keyboard
 * 	param 	pos, usage of the usage page Keyboard
 *)
проц PrintKeyboardPage(pos: цел32);
нач
	(*Keyboard/Keypad Page (0x07)*)
	просей pos из
		|00H: ЛогЯдра.пСтроку8("Reserved (no event indicated)" );
		|01H: ЛогЯдра.пСтроку8("Keyboard ErrorRollOver" );
		|02H: ЛогЯдра.пСтроку8("Keyboard POSTFail" );
		|03H: ЛогЯдра.пСтроку8("Keyboard ErrorUndefined" );
		|04H: ЛогЯдра.пСтроку8("Keyboard a and A" );
		|05H: ЛогЯдра.пСтроку8("Keyboard b and B" );
		|06H: ЛогЯдра.пСтроку8("Keyboard c and C" );
		|07H: ЛогЯдра.пСтроку8("Keyboard d and D" );
		|08H: ЛогЯдра.пСтроку8("Keyboard e and E" );
		|09H: ЛогЯдра.пСтроку8("Keyboard f and F" );
		|0AH: ЛогЯдра.пСтроку8("Keyboard g and G" );
		|0BH: ЛогЯдра.пСтроку8("Keyboard h and H" );
		|0CH: ЛогЯдра.пСтроку8("Keyboard i and I" );
		|0DH: ЛогЯдра.пСтроку8("Keyboard j and J" );
		|0EH: ЛогЯдра.пСтроку8("Keyboard k and K" );
		|0FH: ЛогЯдра.пСтроку8("Keyboard l and L" );
		|10H: ЛогЯдра.пСтроку8("Keyboard m and M" );
		|11H: ЛогЯдра.пСтроку8("Keyboard n and N" );
		|12H: ЛогЯдра.пСтроку8("Keyboard o and O" );
		|13H: ЛогЯдра.пСтроку8("Keyboard p and P" );
		|14H: ЛогЯдра.пСтроку8("Keyboard q and Q" );
		|15H: ЛогЯдра.пСтроку8("Keyboard r and R" );
		|16H: ЛогЯдра.пСтроку8("Keyboard s and S" );
		|17H: ЛогЯдра.пСтроку8("Keyboard t and T" );
		|18H: ЛогЯдра.пСтроку8("Keyboard u and U" );
		|19H: ЛогЯдра.пСтроку8("Keyboard v and V" );
		|1AH: ЛогЯдра.пСтроку8("Keyboard w and W" );
		|1BH: ЛогЯдра.пСтроку8("Keyboard x and X" );
		|1CH: ЛогЯдра.пСтроку8("Keyboard y and Y" );
		|1DH: ЛогЯдра.пСтроку8("Keyboard z and Z" );
		|1EH: ЛогЯдра.пСтроку8("Keyboard 1 and !" );
		|1FH: ЛогЯдра.пСтроку8("Keyboard 2 and @");
		|20H: ЛогЯдра.пСтроку8("Keyboard 3 and #");
		|21H: ЛогЯдра.пСтроку8("Keyboard 4 and $");
		|22H: ЛогЯдра.пСтроку8("Keyboard 5 and %");
		|23H: ЛогЯдра.пСтроку8("Keyboard 6 and ^");
		|24H: ЛогЯдра.пСтроку8("Keyboard 7 and &");
		|25H: ЛогЯдра.пСтроку8("Keyboard 8 and *");
		|26H: ЛогЯдра.пСтроку8("Keyboard 9 and (");
		|27H: ЛогЯдра.пСтроку8("Keyboard 0 and )" );
		|28H: ЛогЯдра.пСтроку8("Keyboard Return (ENTER)" );
		|29H: ЛогЯдра.пСтроку8("Keyboard ESCAPE" );
		|2AH: ЛогЯдра.пСтроку8("Keyboard DELETE (Backspace)" );
		|2BH: ЛогЯдра.пСтроку8("Keyboard Tab" );
		|2CH: ЛогЯдра.пСтроку8("Keyboard Spacebar" );
		|2DH: ЛогЯдра.пСтроку8("Keyboard - and (underscore)" );
		|2EH: ЛогЯдра.пСтроку8("Keyboard = and +");
		|2FH: ЛогЯдра.пСтроку8("Keyboard [ and {");
		|30H: ЛогЯдра.пСтроку8("Keyboard ] and }");
		|31H: ЛогЯдра.пСтроку8("Keyboard \ and |");
		|32H: ЛогЯдра.пСтроку8("Keyboard Non-US # and ~");
		|33H: ЛогЯдра.пСтроку8("Keyboard ; and :" );
		|34H: ЛогЯдра.пСтроку8("Keyboard ");
		|35H: ЛогЯдра.пСтроку8("Keyboard Grave Accent and Tilde" );
		|36H: ЛогЯдра.пСтроку8("Keyboard, and <");
		|37H: ЛогЯдра.пСтроку8("Keyboard . and >");
		|38H: ЛогЯдра.пСтроку8("Keyboard / and ?" );
		|39H: ЛогЯдра.пСтроку8("Keyboard Caps Lock" );
		|3AH: ЛогЯдра.пСтроку8("Keyboard F1" );
		|3BH: ЛогЯдра.пСтроку8("Keyboard F2" );
		|3CH: ЛогЯдра.пСтроку8("Keyboard F3" );
		|3DH: ЛогЯдра.пСтроку8("Keyboard F4" );
		|3EH: ЛогЯдра.пСтроку8("Keyboard F5" );
		|3FH: ЛогЯдра.пСтроку8("Keyboard F6" );
		|40H: ЛогЯдра.пСтроку8("Keyboard F7" );
		|41H: ЛогЯдра.пСтроку8("Keyboard F8" );
		|42H: ЛогЯдра.пСтроку8("Keyboard F9" );
		|43H: ЛогЯдра.пСтроку8("Keyboard F10" );
		|44H: ЛогЯдра.пСтроку8("Keyboard F11" );
		|45H: ЛогЯдра.пСтроку8("Keyboard F12" );
		|46H: ЛогЯдра.пСтроку8("Keyboard PrintScreen" );
		|47H: ЛогЯдра.пСтроку8("Keyboard Scroll Lock" );
		|48H: ЛогЯдра.пСтроку8("Keyboard Pause" );
		|49H: ЛогЯдра.пСтроку8("Keyboard Insert" );
		|4AH: ЛогЯдра.пСтроку8("Keyboard Home" );
		|4BH: ЛогЯдра.пСтроку8("Keyboard PageUp" );
		|4CH: ЛогЯдра.пСтроку8("Keyboard Delete Forward" );
		|4DH: ЛогЯдра.пСтроку8("Keyboard End" );
		|4EH: ЛогЯдра.пСтроку8("Keyboard PageDown" );
		|4FH: ЛогЯдра.пСтроку8("Keyboard RightArrow" );
		|50H: ЛогЯдра.пСтроку8("Keyboard LeftArrow" );
		|51H: ЛогЯдра.пСтроку8("Keyboard DownArrow" );
		|52H: ЛогЯдра.пСтроку8("Keyboard UpArrow" );
		|53H: ЛогЯдра.пСтроку8("Keypad Num Lock and Clear" );
		|54H: ЛогЯдра.пСтроку8("Keypad /");
		|55H: ЛогЯдра.пСтроку8("Keypad *");
		|56H: ЛогЯдра.пСтроку8("Keypad -");
		|57H: ЛогЯдра.пСтроку8("Keypad +");
		|58H: ЛогЯдра.пСтроку8("Keypad ENTER" );
		|59H: ЛогЯдра.пСтроку8("Keypad 1 and End" );
		|5AH: ЛогЯдра.пСтроку8("Keypad 2 and Down Arrow" );
		|5BH: ЛогЯдра.пСтроку8("Keypad 3 and PageDn" );
		|5CH: ЛогЯдра.пСтроку8("Keypad 4 and Left Arrow" );
		|5DH: ЛогЯдра.пСтроку8("Keypad 5" );
		|5EH: ЛогЯдра.пСтроку8("Keypad 6 and Right Arrow" );
		|5FH: ЛогЯдра.пСтроку8("Keypad 7 and Home" );
		|60H: ЛогЯдра.пСтроку8("Keypad 8 and Up Arrow" );
		|61H: ЛогЯдра.пСтроку8("Keypad 9 and PageU");
		|62H: ЛогЯдра.пСтроку8("Keypad 0 and Insert" );
		|63H: ЛогЯдра.пСтроку8("Keypad . and Delete" );
		|64H: ЛогЯдра.пСтроку8("Keyboard Non-US \ and |");
		|65H: ЛогЯдра.пСтроку8("Keyboard Application" );
		|66H: ЛогЯдра.пСтроку8("Keyboard Power" );
		|67H: ЛогЯдра.пСтроку8("Keypad =");
		|68H: ЛогЯдра.пСтроку8("Keyboard F13" );
		|69H: ЛогЯдра.пСтроку8("Keyboard F14" );
		|6AH: ЛогЯдра.пСтроку8("Keyboard F15" );
		|6BH: ЛогЯдра.пСтроку8("Keyboard F16" );
		|6CH: ЛогЯдра.пСтроку8("Keyboard F17" );
		|6DH: ЛогЯдра.пСтроку8("Keyboard F18" );
		|6EH: ЛогЯдра.пСтроку8("Keyboard F19" );
		|6FH: ЛогЯдра.пСтроку8("Keyboard F20" );
		|70H: ЛогЯдра.пСтроку8("Keyboard F21" );
		|71H: ЛогЯдра.пСтроку8("Keyboard F22" );
		|72H: ЛогЯдра.пСтроку8("Keyboard F23" );
		|73H: ЛогЯдра.пСтроку8("Keyboard F24" );
		|74H: ЛогЯдра.пСтроку8("Keyboard Execute" );
		|75H: ЛогЯдра.пСтроку8("Keyboard Help" );
		|76H: ЛогЯдра.пСтроку8("Keyboard Menu" );
		|77H: ЛогЯдра.пСтроку8("Keyboard Select" );
		|78H: ЛогЯдра.пСтроку8("Keyboard Stop" );
		|79H: ЛогЯдра.пСтроку8("Keyboard Again" );
		|7AH: ЛогЯдра.пСтроку8("Keyboard Undo" );
		|7BH: ЛогЯдра.пСтроку8("Keyboard Cut" );
		|7CH: ЛогЯдра.пСтроку8("Keyboard Copy" );
		|7DH: ЛогЯдра.пСтроку8("Keyboard Paste" );
		|7EH: ЛогЯдра.пСтроку8("Keyboard Find" );
		|7FH: ЛогЯдра.пСтроку8("Keyboard Mute" );
		|80H: ЛогЯдра.пСтроку8("Keyboard Volume Up" );
		|81H: ЛогЯдра.пСтроку8("Keyboard Volume Down" );
		|82H: ЛогЯдра.пСтроку8("Keyboard Locking Caps Lock" );
		|83H: ЛогЯдра.пСтроку8("Keyboard Locking Num Lock" );
		|84H: ЛогЯдра.пСтроку8("Keyboard Locking Scroll Lock" );
		|85H: ЛогЯдра.пСтроку8("Keypad Comma" );
		|86H: ЛогЯдра.пСтроку8("Keypad Equal Sign" );
		|87H: ЛогЯдра.пСтроку8("Keyboard International1" );
		|88H: ЛогЯдра.пСтроку8("Keyboard International2" );
		|89H: ЛогЯдра.пСтроку8("Keyboard International3" );
		|8AH: ЛогЯдра.пСтроку8("Keyboard International4" );
		|8BH: ЛогЯдра.пСтроку8("Keyboard International5" );
		|8CH: ЛогЯдра.пСтроку8("Keyboard International6" );
		|8DH: ЛогЯдра.пСтроку8("Keyboard International7" );
		|8EH: ЛогЯдра.пСтроку8("Keyboard International8" );
		|8FH: ЛогЯдра.пСтроку8("Keyboard International9" );
		|90H: ЛогЯдра.пСтроку8("Keyboard LANG1" );
		|91H: ЛогЯдра.пСтроку8("Keyboard LANG2" );
		|92H: ЛогЯдра.пСтроку8("Keyboard LANG3" );
		|93H: ЛогЯдра.пСтроку8("Keyboard LANG4" );
		|94H: ЛогЯдра.пСтроку8("Keyboard LANG5" );
		|95H: ЛогЯдра.пСтроку8("Keyboard LANG6" );
		|96H: ЛогЯдра.пСтроку8("Keyboard LANG7" );
		|97H: ЛогЯдра.пСтроку8("Keyboard LANG8" );
		|98H: ЛогЯдра.пСтроку8("Keyboard LANG9" );
		|99H: ЛогЯдра.пСтроку8("Keyboard Alternate Erase" );
		|9AH: ЛогЯдра.пСтроку8("Keyboard SysReq/Attention" );
		|9BH: ЛогЯдра.пСтроку8("Keyboard Cancel" );
		|9CH: ЛогЯдра.пСтроку8("Keyboard Clear" );
		|9DH: ЛогЯдра.пСтроку8("Keyboard Prior" );
		|9EH: ЛогЯдра.пСтроку8("Keyboard Return" );
		|9FH: ЛогЯдра.пСтроку8("Keyboard Separator" );
		|0A0H: ЛогЯдра.пСтроку8("Keyboard Out" );
		|0A1H: ЛогЯдра.пСтроку8("Keyboard Oper" );
		|0A2H: ЛогЯдра.пСтроку8("Keyboard Clear/Again" );
		|0A3H: ЛогЯдра.пСтроку8("Keyboard CrSel/Props" );
		|0A4H: ЛогЯдра.пСтроку8("Keyboard ExSel" );
		(*|0A5-CFH: KernelLog.String("Reserved" );*)
		|0B0H: ЛогЯдра.пСтроку8("Keypad 00" );
		|0B1H: ЛогЯдра.пСтроку8("Keypad 000" );
		|0B2H: ЛогЯдра.пСтроку8("Thousands Separator" );
		|0B3H: ЛогЯдра.пСтроку8("Decimal Separator" );
		|0B4H: ЛогЯдра.пСтроку8("Currency Unit" );
		|0B5H: ЛогЯдра.пСтроку8("Currency Sub-unit" );
		|0B6H: ЛогЯдра.пСтроку8("Keypad (");
		|0B7H: ЛогЯдра.пСтроку8("Keypad )" );
		|0B8H: ЛогЯдра.пСтроку8("Keypad {");
		|0B9H: ЛогЯдра.пСтроку8("Keypad }");
		|0BAH: ЛогЯдра.пСтроку8("Keypad Tab" );
		|0BBH: ЛогЯдра.пСтроку8("Keypad Backspace" );
		|0BCH: ЛогЯдра.пСтроку8("Keypad A" );
		|0BDH: ЛогЯдра.пСтроку8("Keypad B" );
		|0BEH: ЛогЯдра.пСтроку8("Keypad C" );
		|0BFH: ЛогЯдра.пСтроку8("Keypad D" );
		|0C0H: ЛогЯдра.пСтроку8("Keypad E" );
		|0C1H: ЛогЯдра.пСтроку8("Keypad F" );
		|0C2H: ЛогЯдра.пСтроку8("Keypad XOR" );
		|0C3H: ЛогЯдра.пСтроку8("Keypad ^");
		|0C4H: ЛогЯдра.пСтроку8("Keypad %");
		|0C5H: ЛогЯдра.пСтроку8("Keypad <");
		|0C6H: ЛогЯдра.пСтроку8("Keypad >");
		|0C7H: ЛогЯдра.пСтроку8("Keypad &");
		|0C8H: ЛогЯдра.пСтроку8("Keypad &&");
		|0C9H: ЛогЯдра.пСтроку8("Keypad |");
		|0CAH: ЛогЯдра.пСтроку8("Keypad ||");
		|0CBH: ЛогЯдра.пСтроку8("Keypad :" );
		|0CCH: ЛогЯдра.пСтроку8("Keypad #");
		|0CDH: ЛогЯдра.пСтроку8("Keypad Space" );
		|0CEH: ЛогЯдра.пСтроку8("Keypad @");
		|0CFH: ЛогЯдра.пСтроку8("Keypad !" );
		|0D0H: ЛогЯдра.пСтроку8("Keypad Memory Store" );
		|0D1H: ЛогЯдра.пСтроку8("Keypad Memory Recall" );
		|0D2H: ЛогЯдра.пСтроку8("Keypad Memory Clear" );
		|0D3H: ЛогЯдра.пСтроку8("Keypad Memory Add" );
		|0D4H: ЛогЯдра.пСтроку8("Keypad Memory Subtract" );
		|0D5H: ЛогЯдра.пСтроку8("Keypad Memory Multiply" );
		|0D6H: ЛогЯдра.пСтроку8("Keypad Memory Divide" );
		|0D7H: ЛогЯдра.пСтроку8("Keypad +/-");
		|0D8H: ЛогЯдра.пСтроку8("Keypad Clear" );
		|0D9H: ЛогЯдра.пСтроку8("Keypad Clear Entry" );
		|0DAH: ЛогЯдра.пСтроку8("Keypad Binary" );
		|0DBH: ЛогЯдра.пСтроку8("Keypad Octal" );
		|0DCH: ЛогЯдра.пСтроку8("Keypad Decimal" );
		|0DDH: ЛогЯдра.пСтроку8("Keypad Hexadecimal" );
		(*|0DE-DFH: KernelLog.String("Reserved" );*)
		|0E0H: ЛогЯдра.пСтроку8("Keyboard LeftControl" );
		|0E1H: ЛогЯдра.пСтроку8("Keyboard LeftShift" );
		|0E2H: ЛогЯдра.пСтроку8("Keyboard LeftAlt" );
		|0E3H: ЛогЯдра.пСтроку8("Keyboard Left GUI" );
		|0E4H: ЛогЯдра.пСтроку8("Keyboard RightControl" );
		|0E5H: ЛогЯдра.пСтроку8("Keyboard RightShift" );
		|0E6H: ЛогЯдра.пСтроку8("Keyboard RightAlt" );
		|0E7H: ЛогЯдра.пСтроку8("Keyboard Right GUI" );
		иначе ЛогЯдра.пСтроку8("Reserved");
	всё;
кон PrintKeyboardPage;

(* print out the decoded usage for usage page Led
 * 	param 	pos, usage of the usage page Led
 *)
проц PrintLedPage(pos: цел32);
нач
	(*LEDPage (0x08)*)
	просей pos из
		00H:  ЛогЯдра.пСтроку8("Undefined");
		|01H: ЛогЯдра.пСтроку8("Num Lock");
		|02H: ЛогЯдра.пСтроку8(" Caps Lock");
		|03H: ЛогЯдра.пСтроку8(" Scroll Lock");
		|04H: ЛогЯдра.пСтроку8(" Compose");
		|05H: ЛогЯдра.пСтроку8(" Kana");
		|06H: ЛогЯдра.пСтроку8(" Power");
		|07H: ЛогЯдра.пСтроку8(" Shift");
		|08H: ЛогЯдра.пСтроку8(" Do Not Disturb");
		|09H: ЛогЯдра.пСтроку8(" Mute");
		|0AH: ЛогЯдра.пСтроку8(" Tone Enable");
		|0BH: ЛогЯдра.пСтроку8(" High Cut Filter");
		|0CH: ЛогЯдра.пСтроку8(" Low Cut Filter");
		|0DH: ЛогЯдра.пСтроку8(" Equalizer Enable");
		|0EH: ЛогЯдра.пСтроку8(" Sound Field On");
		|0FH: ЛогЯдра.пСтроку8(" Surround On");
		|10H: ЛогЯдра.пСтроку8(" Repeat");
		|11H: ЛогЯдра.пСтроку8(" Stereo");
		|12H: ЛогЯдра.пСтроку8(" Sampling Rate Detect");
		|13H: ЛогЯдра.пСтроку8(" Spinning");
		|14H: ЛогЯдра.пСтроку8(" CAV");
		|15H: ЛогЯдра.пСтроку8(" CLV");
		|16H: ЛогЯдра.пСтроку8(" Recording Format Detect");
		|17H: ЛогЯдра.пСтроку8(" Off-Hook");
		|18H: ЛогЯдра.пСтроку8(" Ring");
		|19H: ЛогЯдра.пСтроку8(" Message Waiting");
		|1AH: ЛогЯдра.пСтроку8(" Data Mode");
		|1BH: ЛогЯдра.пСтроку8(" Battery Operation");
		|1CH: ЛогЯдра.пСтроку8(" Battery OK");
		|1DH: ЛогЯдра.пСтроку8(" Battery Low");
		|1EH: ЛогЯдра.пСтроку8(" Speaker");
		|1FH: ЛогЯдра.пСтроку8(" Head Set");
		|20H: ЛогЯдра.пСтроку8(" Hold");
		|21H: ЛогЯдра.пСтроку8(" Microphone");
		|22H: ЛогЯдра.пСтроку8(" Coverage");
		|23H: ЛогЯдра.пСтроку8(" Night Mode");
		|24H: ЛогЯдра.пСтроку8(" Send Calls");
		|25H: ЛогЯдра.пСтроку8(" Call Pickup");
		|26H: ЛогЯдра.пСтроку8(" Conference");
		|27H: ЛогЯдра.пСтроку8(" Stand-by");
		|28H: ЛогЯдра.пСтроку8(" Camera On");
		|29H: ЛогЯдра.пСтроку8(" Camera Off");
		|2AH: ЛогЯдра.пСтроку8(" On-Line");
		|2BH: ЛогЯдра.пСтроку8(" Off-Line");
		|2CH: ЛогЯдра.пСтроку8(" Busy");
		|2DH: ЛогЯдра.пСтроку8(" Ready");
		|2EH: ЛогЯдра.пСтроку8(" Paper-Out");
		|2FH: ЛогЯдра.пСтроку8(" Paper-Jam");
		|30H: ЛогЯдра.пСтроку8(" Remote");
		|31H: ЛогЯдра.пСтроку8(" Forward");
		|32H: ЛогЯдра.пСтроку8(" Reverse");
		|33H: ЛогЯдра.пСтроку8(" Stop");
		|34H: ЛогЯдра.пСтроку8(" Rewind");
		|35H: ЛогЯдра.пСтроку8(" Fast Forward");
		|36H: ЛогЯдра.пСтроку8(" Play");
		|37H: ЛогЯдра.пСтроку8(" Pause");
		|38H: ЛогЯдра.пСтроку8(" Record");
		|39H: ЛогЯдра.пСтроку8(" Error");
		|3AH: ЛогЯдра.пСтроку8(" Usage Selected Indicator");
		|3BH: ЛогЯдра.пСтроку8(" Usage In Use Indicator");
		|3CH: ЛогЯдра.пСтроку8(" Usage Multi Mode Indicator");
		|3DH: ЛогЯдра.пСтроку8(" Indicator On");
		|3EH: ЛогЯдра.пСтроку8(" Indicator Flash");
		|3FH: ЛогЯдра.пСтроку8(" Indicator Slow Blink");
		|40H: ЛогЯдра.пСтроку8(" Indicator Fast Blink");
		|41H: ЛогЯдра.пСтроку8(" Indicator Off");
		|42H: ЛогЯдра.пСтроку8(" Flash On Time");
		|43H: ЛогЯдра.пСтроку8(" Slow Blink On Time");
		|44H: ЛогЯдра.пСтроку8(" Slow Blink Off Time");
		|45H: ЛогЯдра.пСтроку8(" Fast Blink On Time");
		|46H: ЛогЯдра.пСтроку8(" Fast Blink Off Time");
		|47H: ЛогЯдра.пСтроку8(" Usage Indicator Color");
		|48H: ЛогЯдра.пСтроку8(" Indicator Red");
		|49H: ЛогЯдра.пСтроку8(" Indicator Green");
		|4AH: ЛогЯдра.пСтроку8(" Indicator Amber");
		|4BH: ЛогЯдра.пСтроку8(" Generic Indicator");
		|4CH: ЛогЯдра.пСтроку8(" System Suspend");
		|4DH: ЛогЯдра.пСтроку8(" External Power Connected");
	иначе
		ЛогЯдра.пСтроку8("Reserved ");
	всё;
кон PrintLedPage;

(* print out the decoded usage for usage page Button
 *	param 	pos, usage of the usage page Button
 *)
проц PrintButtonPage(pos: цел32);
нач
	(*Button Page (0x09)*)
	просей pos из
		00H: ЛогЯдра.пСтроку8("No button pressed" );
		|01H: ЛогЯдра.пСтроку8("Button 1 (primary/trigger)" );
		|02H: ЛогЯдра.пСтроку8("Button 2 (secondary)" );
		|03H: ЛогЯдра.пСтроку8("Button 3 (tertiary)" );
		|04H: ЛогЯдра.пСтроку8("Button 4");
		(*|FFFFH: KernelLog.String("Button 65535");*)
		иначе
			ЛогЯдра.пСтроку8("Button "); ЛогЯдра.пЦел64(pos,0);
	всё;
кон PrintButtonPage;

(* print out the decoded usage for usage page Consumer
 *	param 	pos, usage of the usage page Consumer
 *)
проц PrintConsumerPage(pos: цел32);
нач
	(*Consumer Page (0x0C)*)
	просей pos из
		00H: ЛогЯдра.пСтроку8("Unassigned");
		|01H: ЛогЯдра.пСтроку8("Consumer Control");
		|02H: ЛогЯдра.пСтроку8("Numeric Key Pad");
		|03H: ЛогЯдра.пСтроку8("Programmable Buttons");
		|04H: ЛогЯдра.пСтроку8("Microphone");
		|05H: ЛогЯдра.пСтроку8("Headphone");
		|06H: ЛогЯдра.пСтроку8("Graphic Equalizer");
		(*|07-1F Reserved*)
		|20H: ЛогЯдра.пСтроку8("+10");
		|21H: ЛогЯдра.пСтроку8("+100");
		|22H: ЛогЯдра.пСтроку8(" AM/PM");
		(*|23-3F Reserved*)
		|30H: ЛогЯдра.пСтроку8(" Power");
		|31H: ЛогЯдра.пСтроку8(" Reset");
		|32H: ЛогЯдра.пСтроку8(" Sleep");
		|33H: ЛогЯдра.пСтроку8(" Sleep After");
		|34H: ЛогЯдра.пСтроку8(" Sleep Mode");
		|35H: ЛогЯдра.пСтроку8(" Illumination");
		|36H: ЛогЯдра.пСтроку8(" Function Buttons");
		(*|37-3F Reserved*)
		|40H: ЛогЯдра.пСтроку8("Menu");
		|41H: ЛогЯдра.пСтроку8(" Menu Pick");
		|42H: ЛогЯдра.пСтроку8(" Menu Up");
		|43H: ЛогЯдра.пСтроку8(" Menu Down");
		|44H: ЛогЯдра.пСтроку8(" Menu Left");
		|45H: ЛогЯдра.пСтроку8(" Menu Right");
		|46H: ЛогЯдра.пСтроку8(" Menu Escape");
		|47H: ЛогЯдра.пСтроку8(" Menu Value Increase");
		|48H: ЛогЯдра.пСтроку8(" Menu Value Decrease");
		(*|49-5F Reserved*)
		|60H: ЛогЯдра.пСтроку8(" Data On Screen");
		|61H: ЛогЯдра.пСтроку8(" Closed Caption");
		|62H: ЛогЯдра.пСтроку8(" Closed Caption Select");
		|63H: ЛогЯдра.пСтроку8(" VCR/TV ");
		|64H: ЛогЯдра.пСтроку8(" Broadcast Mode");
		|65H: ЛогЯдра.пСтроку8(" Snapshot");
		|66H: ЛогЯдра.пСтроку8(" Stil");
		(*|67-7F Reserved*)
		|80H: ЛогЯдра.пСтроку8(" Selection");
		|81H: ЛогЯдра.пСтроку8(" Assign Selection");
		|82H: ЛогЯдра.пСтроку8(" Mode Step");
		|83H: ЛогЯдра.пСтроку8(" Recall Last");
		|84H: ЛогЯдра.пСтроку8(" Enter Channel");
		|85H: ЛогЯдра.пСтроку8(" Order Movie");
		|86H: ЛогЯдра.пСтроку8(" Channel LC");
		|87H: ЛогЯдра.пСтроку8(" Media Selection");
		|88H: ЛогЯдра.пСтроку8(" Media Select Computer ");
		|89H: ЛогЯдра.пСтроку8(" Media Select TV");
		|8AH: ЛогЯдра.пСтроку8(" Media Select WWW");
		|8BH: ЛогЯдра.пСтроку8(" Media Select DVD");
		|8CH: ЛогЯдра.пСтроку8(" Media Select Telephone");
		|8DH: ЛогЯдра.пСтроку8(" Media Select Program Guide");
		|8EH: ЛогЯдра.пСтроку8(" Media Select Video Phone");
		|8FH: ЛогЯдра.пСтроку8(" Media Select Games");
		|90H: ЛогЯдра.пСтроку8(" Media Select Messages");
		|91H: ЛогЯдра.пСтроку8(" Media Select CD");
		|92H: ЛогЯдра.пСтроку8(" Media Select VCR");
		|93H: ЛогЯдра.пСтроку8(" Media Select Tuner");
		|94H: ЛогЯдра.пСтроку8(" Quit");
		|95H: ЛогЯдра.пСтроку8(" Help");
		|96H: ЛогЯдра.пСтроку8(" Media Select Tape");
		|97H: ЛогЯдра.пСтроку8(" Media Select Cable");
		|98H: ЛогЯдра.пСтроку8(" Media Select Satellite");
		|99H: ЛогЯдра.пСтроку8(" Media Select Security");
		|9AH: ЛогЯдра.пСтроку8(" Media Select Home");
		|9BH: ЛогЯдра.пСтроку8(" Media Select Call");
		|9CH: ЛогЯдра.пСтроку8(" Channel Increment");
		|9DH: ЛогЯдра.пСтроку8(" Channel Decrement");
		|9EH: ЛогЯдра.пСтроку8(" Media Select SAP");
		|9FH: ЛогЯдра.пСтроку8(" Reserved");
		|0A0H: ЛогЯдра.пСтроку8(" VCR Plus");
		|0A1H: ЛогЯдра.пСтроку8(" Once");
		|0A2H: ЛогЯдра.пСтроку8(" Daily");
		|0A3H: ЛогЯдра.пСтроку8(" Weekly");
		|0A4H: ЛогЯдра.пСтроку8(" Monthly");
		(*|0A5-AF Reserved*)
		|0B0H: ЛогЯдра.пСтроку8(" Play");
		|0B1H: ЛогЯдра.пСтроку8(" Pause");
		|0B2H: ЛогЯдра.пСтроку8(" Record");
		|0B3H: ЛогЯдра.пСтроку8(" Fast Forward");
		|0B4H: ЛогЯдра.пСтроку8(" Rewind");
		|0B5H: ЛогЯдра.пСтроку8(" Scan Next Track");
		|0B6H: ЛогЯдра.пСтроку8(" Scan Previous Track");
		|0B7H: ЛогЯдра.пСтроку8(" Stop OSC");
		|0B8H: ЛогЯдра.пСтроку8(" Eject OSC");
		|0B9H: ЛогЯдра.пСтроку8(" Random Play");
		|0BAH: ЛогЯдра.пСтроку8(" Select Disc");
		|0BBH: ЛогЯдра.пСтроку8(" Enter Disc");
		|0BCH: ЛогЯдра.пСтроку8(" Repeat");
		|0BDH: ЛогЯдра.пСтроку8(" Tracking");
		|0BEH: ЛогЯдра.пСтроку8(" Track Normal");
		|0BFH: ЛогЯдра.пСтроку8(" Slow Tracking");
		|0C0H: ЛогЯдра.пСтроку8(" Frame Forward");
		|0C1H: ЛогЯдра.пСтроку8(" Frame Back");
		|0C2H: ЛогЯдра.пСтроку8(" Mark OSC");
		|0C3H: ЛогЯдра.пСтроку8(" Clear Mark");
		|0C4H: ЛогЯдра.пСтроку8(" Repeat From Mark");
		|0C5H: ЛогЯдра.пСтроку8(" Return To Mark");
		|0C6H: ЛогЯдра.пСтроку8(" Search Mark Forward");
		|0C7H: ЛогЯдра.пСтроку8(" Search Mark Backwards");
		|0C8H: ЛогЯдра.пСтроку8(" Counter Reset");
		|0C9H: ЛогЯдра.пСтроку8(" Show Counter");
		|0CAH: ЛогЯдра.пСтроку8(" Tracking Increment");
		|0CBH: ЛогЯдра.пСтроку8(" Tracking Decrement");
		|0CCH: ЛогЯдра.пСтроку8(" Stop/Eject");
		|0CDH: ЛогЯдра.пСтроку8(" Play/Pause");
		|0CEH: ЛогЯдра.пСтроку8(" Play/Skip");
		(*|0CF-DF Reserved*)
		|0E0H: ЛогЯдра.пСтроку8(" Volume");
		|0E1H: ЛогЯдра.пСтроку8(" Balance");
		|0E2H: ЛогЯдра.пСтроку8(" Mute");
		|0E3H: ЛогЯдра.пСтроку8(" Bass");
		|0E4H: ЛогЯдра.пСтроку8(" Treble");
		|0E5H: ЛогЯдра.пСтроку8(" Bass Boost");
		|0E6H: ЛогЯдра.пСтроку8(" Surround Mode");
		|0E7H: ЛогЯдра.пСтроку8(" Loudness");
		|0E8H: ЛогЯдра.пСтроку8(" MPX");
		|0E9H: ЛогЯдра.пСтроку8(" Volume Increment");
		|0EAH: ЛогЯдра.пСтроку8(" Volume Decrement");
		(*|0EB-EF Reserved*)
		|0F0H: ЛогЯдра.пСтроку8(" Speed Select");
		|0F1H: ЛогЯдра.пСтроку8(" Playback Speed");
		|0F2H: ЛогЯдра.пСтроку8(" Standard Play");
		|0F3H: ЛогЯдра.пСтроку8(" Long Play");
		|0F4H: ЛогЯдра.пСтроку8(" Extended Play");
		|0F5H: ЛогЯдра.пСтроку8(" Slow");
		(*|0F6-FF Reserved*)
		|100H: ЛогЯдра.пСтроку8(" Fan Enable");
		|101H: ЛогЯдра.пСтроку8(" Fan Speed");
		|102H: ЛогЯдра.пСтроку8(" Light Enable");
		|103H: ЛогЯдра.пСтроку8(" Light Illumination Level");
		|104H: ЛогЯдра.пСтроку8(" Climate Control Enable");
		|105H: ЛогЯдра.пСтроку8(" Room Temperature");
		|106H: ЛогЯдра.пСтроку8(" Security Enable");
		|107H: ЛогЯдра.пСтроку8(" Fire Alarm");
		|108H: ЛогЯдра.пСтроку8(" Police Alarm");
		|109H: ЛогЯдра.пСтроку8(" Proximity");
		|10AH: ЛогЯдра.пСтроку8(" Motion");
		|10BH: ЛогЯдра.пСтроку8(" Duress Alarm");
		|10CH: ЛогЯдра.пСтроку8(" Holdup Alarm");
		|10DH: ЛогЯдра.пСтроку8(" Medical Alarm");
		(*|10E-14F Reserved*)
		|150H: ЛогЯдра.пСтроку8(" Balance Right");
		|151H: ЛогЯдра.пСтроку8(" Balance Left");
		|152H: ЛогЯдра.пСтроку8(" Bass Increment");
		|153H: ЛогЯдра.пСтроку8(" Bass Decrement");
		|154H: ЛогЯдра.пСтроку8(" Treble Increment");
		|155H: ЛогЯдра.пСтроку8(" Treble Decrement");
		(*156-15F Reserved*)
		|160H: ЛогЯдра.пСтроку8(" Speaker System");
		|161H: ЛогЯдра.пСтроку8(" Channel Left");
		|162H: ЛогЯдра.пСтроку8(" Channel Right");
		|163H: ЛогЯдра.пСтроку8(" Channel Center");
		|164H: ЛогЯдра.пСтроку8(" Channel Front");
		|165H: ЛогЯдра.пСтроку8(" Channel Center Front");
		|166H: ЛогЯдра.пСтроку8(" Channel Side");
		|167H: ЛогЯдра.пСтроку8(" Channel Surround");
		|168H: ЛогЯдра.пСтроку8(" Channel Low Frequency");
		(*|16B-16F Reserved*)
		|170H: ЛогЯдра.пСтроку8(" Sub-channel");
		|171H: ЛогЯдра.пСтроку8(" Sub-channel Increment");
		|172H: ЛогЯдра.пСтроку8(" Sub-channel Decrement");
		|173H: ЛогЯдра.пСтроку8(" Alternate Audio Increment");
		|174H: ЛогЯдра.пСтроку8(" Alternate Audio Decrement");
		(*|175-17F Reserved*)
		|180H: ЛогЯдра.пСтроку8(" Application Launch Buttons");
		|181H: ЛогЯдра.пСтроку8(" AL Launch Button Configuration");
		|182H: ЛогЯдра.пСтроку8(" AL Programmable Button");
		|183H: ЛогЯдра.пСтроку8(" AL Consumer Control");
		|184H: ЛогЯдра.пСтроку8(" AL Word Processor");
		|185H: ЛогЯдра.пСтроку8(" AL Text Editor");
		|186H: ЛогЯдра.пСтроку8(" AL Spreadsheet");
		|187H: ЛогЯдра.пСтроку8(" AL Graphics Editor");
		|188H: ЛогЯдра.пСтроку8(" AL Presentation App");
		|189H: ЛогЯдра.пСтроку8(" AL Database App");
		|18AH: ЛогЯдра.пСтроку8(" AL Email Reader");
		|18BH: ЛогЯдра.пСтроку8(" AL Newsreader");
		|18CH: ЛогЯдра.пСтроку8(" AL Voicemail");
		|18DH: ЛогЯдра.пСтроку8(" AL Contacts/Address Book");
		|18EH: ЛогЯдра.пСтроку8(" AL Calendar/Schedule");
		|18FH: ЛогЯдра.пСтроку8(" AL Task/Project Manager");
		|190H: ЛогЯдра.пСтроку8(" AL Log/Journal/Timecard");
		|191H: ЛогЯдра.пСтроку8(" AL Checkbook/Finance");
		|192H: ЛогЯдра.пСтроку8(" AL Calculator");
		|193H: ЛогЯдра.пСтроку8(" AL A/V Capture/Playback");
		|194H: ЛогЯдра.пСтроку8(" AL Local Machine Browser");
		|195H: ЛогЯдра.пСтроку8(" AL LAN/WAN Browser");
		|196H: ЛогЯдра.пСтроку8(" AL Internet Browser");
		|197H: ЛогЯдра.пСтроку8(" AL Remote Networking/ISP");
		|198H: ЛогЯдра.пСтроку8(" AL Network Conference");
		|199H: ЛогЯдра.пСтроку8(" AL Network Chat");
		|19AH: ЛогЯдра.пСтроку8(" AL Telephony/Dialer");
		|19BH: ЛогЯдра.пСтроку8(" AL Logon");
		|19CH: ЛогЯдра.пСтроку8(" AL Logoff");
		|19DH: ЛогЯдра.пСтроку8(" AL Logon/Logoff");
		|19EH: ЛогЯдра.пСтроку8(" AL Terminal Lock/Screensaver");
		|19FH: ЛогЯдра.пСтроку8(" AL Control Panel");
		|1A0H: ЛогЯдра.пСтроку8(" AL Command Line Processor/Run");
		|1A1H: ЛогЯдра.пСтроку8(" AL Process/Task Manager");
		|1A2H: ЛогЯдра.пСтроку8(" AL Select Task/Application");
		|1A3H: ЛогЯдра.пСтроку8(" AL Next Task/Application");
		|1A4H: ЛогЯдра.пСтроку8(" AL Previous Task/Application");
		|1A5H: ЛогЯдра.пСтроку8(" AL Preemptive Halt");
		|1A6H: ЛогЯдра.пСтроку8(" AL Integrated Help Center");
		|1A7H: ЛогЯдра.пСтроку8(" AL Documents");
		|1A8H: ЛогЯдра.пСтроку8(" AL Thesaurus");
		|1A9H: ЛогЯдра.пСтроку8(" AL Dictionary");
		|1AAH: ЛогЯдра.пСтроку8(" AL Desktop");
		|1ABH: ЛогЯдра.пСтроку8(" AL Spell Check");
		|1ACH: ЛогЯдра.пСтроку8(" AL Grammar Check");
		|1ADH: ЛогЯдра.пСтроку8(" AL Wireless Status");
		|1AEH: ЛогЯдра.пСтроку8(" AL Keyboard Layout");
		|1AFH: ЛогЯдра.пСтроку8(" AL Virusu Protection");
		|1B0H: ЛогЯдра.пСтроку8(" AL Encryption");
		|1B1H: ЛогЯдра.пСтроку8(" AL Screen Saver");
		|1B2H: ЛогЯдра.пСтроку8(" AL Alarms");
		|1B3H: ЛогЯдра.пСтроку8(" AL Clock");
		|1B4H: ЛогЯдра.пСтроку8(" AL File Browser");
		|1B5H: ЛогЯдра.пСтроку8(" AL Power Status");
		|1B6H: ЛогЯдра.пСтроку8(" AL Image Browser");
		|1B7H: ЛогЯдра.пСтроку8(" AL Audio Browser");
		|1B8H: ЛогЯдра.пСтроку8(" AL Movie Browser");
		|1B9H: ЛогЯдра.пСтроку8(" AL Digital Rights Manager");
		|1BAH: ЛогЯдра.пСтроку8(" AL Digital Wallet");
		(*|1BB Reserved*)
		|1BCH: ЛогЯдра.пСтроку8(" AL Instant Messaging");
		|1BDH: ЛогЯдра.пСтроку8(" AL OEM Features/ Tips/Tutorial");
		|1BEH: ЛогЯдра.пСтроку8(" AL OEM Help");
		|1BFH: ЛогЯдра.пСтроку8(" AL Online Community");
		|1C0H: ЛогЯдра.пСтроку8(" AL Entertainment Content");
		|1C1H: ЛогЯдра.пСтроку8(" AL Online Shopping Browser");
		|1C2H: ЛогЯдра.пСтроку8(" AL SmartCard Information/Help");
		|1C3H: ЛогЯдра.пСтроку8(" AL Market Monitor/Finance");
		|1C4H: ЛогЯдра.пСтроку8(" AL Customized Corporate News");
		|1C5H: ЛогЯдра.пСтроку8(" AL Online Activity Browser");
		|1C6H: ЛогЯдра.пСтроку8(" AL Research/Search Browser");
		|1C7H: ЛогЯдра.пСтроку8(" AL Audio Player");
		(*|1C8-1FF Reserved*)
		|200H: ЛогЯдра.пСтроку8(" Generic GUI Application");
		|201H: ЛогЯдра.пСтроку8(" AC New");
		|202H: ЛогЯдра.пСтроку8(" AC Open");
		|203H: ЛогЯдра.пСтроку8(" AC Close");
		|204H: ЛогЯдра.пСтроку8(" AC Exit");
		|205H: ЛогЯдра.пСтроку8(" AC Maximize");
		|206H: ЛогЯдра.пСтроку8(" AC Minimize");
		|207H: ЛогЯдра.пСтроку8(" AC Save");
		|208H: ЛогЯдра.пСтроку8(" AC Print");
		|209H: ЛогЯдра.пСтроку8(" AC Properties");
		|21AH: ЛогЯдра.пСтроку8(" AC Undo");
		|21BH: ЛогЯдра.пСтроку8(" AC Copy");
		|21CH: ЛогЯдра.пСтроку8(" AC Cut");
		|21DH: ЛогЯдра.пСтроку8(" AC Paste");
		|21EH: ЛогЯдра.пСтроку8(" AC Select All");
		|21FH: ЛогЯдра.пСтроку8(" AC Find");
		|220H: ЛогЯдра.пСтроку8(" AC Find and Replace");
		|221H: ЛогЯдра.пСтроку8(" AC Search");
		|222H: ЛогЯдра.пСтроку8(" AC Go To");
		|223H: ЛогЯдра.пСтроку8(" AC Home");
		|224H: ЛогЯдра.пСтроку8(" AC Back");
		|225H: ЛогЯдра.пСтроку8(" AC Forward");
		|226H: ЛогЯдра.пСтроку8(" AC Stop");
		|227H: ЛогЯдра.пСтроку8(" AC Refresh");
		|228H: ЛогЯдра.пСтроку8(" AC Previous Link");
		|229H: ЛогЯдра.пСтроку8(" AC Next Link");
		|22AH: ЛогЯдра.пСтроку8(" AC Bookmarks");
		|22BH: ЛогЯдра.пСтроку8(" AC History");
		|22CH: ЛогЯдра.пСтроку8(" AC Subscriptions");
		|22DH: ЛогЯдра.пСтроку8(" AC Zoom In");
		|22EH: ЛогЯдра.пСтроку8(" AC Zoom Out");
		|22FH: ЛогЯдра.пСтроку8(" AC Zoom");
		|230H: ЛогЯдра.пСтроку8(" AC Full Screen View");
		|231H: ЛогЯдра.пСтроку8(" AC Normal View");
		|232H: ЛогЯдра.пСтроку8(" AC View Toggle");
		|233H: ЛогЯдра.пСтроку8(" AC Scroll Up");
		|234H: ЛогЯдра.пСтроку8(" AC Scroll Down");
		|235H: ЛогЯдра.пСтроку8(" AC Scroll");
		|236H: ЛогЯдра.пСтроку8(" AC Pan Left");
		|237H: ЛогЯдра.пСтроку8(" AC Pan Right");
		|238H: ЛогЯдра.пСтроку8(" AC Pan");
		|239H: ЛогЯдра.пСтроку8(" AC New Window");
		|23AH: ЛогЯдра.пСтроку8(" AC Tile Horizontally");
		|23BH: ЛогЯдра.пСтроку8(" AC Tile Vertically");
		|23CH: ЛогЯдра.пСтроку8(" AC Format");
		|23DH: ЛогЯдра.пСтроку8(" AC Edit");
		|23EH: ЛогЯдра.пСтроку8(" AC Bold");
		|23FH: ЛогЯдра.пСтроку8(" AC Italics");
		|240H: ЛогЯдра.пСтроку8(" AC Underline");
		|241H: ЛогЯдра.пСтроку8(" AC Strikethrough");
		|242H: ЛогЯдра.пСтроку8(" AC Subscript");
		|243H: ЛогЯдра.пСтроку8(" AC Superscript");
		|244H: ЛогЯдра.пСтроку8(" AC All Caps");
		|245H: ЛогЯдра.пСтроку8(" AC Rotate");
		|246H: ЛогЯдра.пСтроку8(" AC Resize");
		|247H: ЛогЯдра.пСтроку8(" AC Flip horizontal");
		|248H: ЛогЯдра.пСтроку8(" AC Flip Vertical");
		|249H: ЛогЯдра.пСтроку8(" AC Mirror Horizontal");
		|24AH: ЛогЯдра.пСтроку8(" AC Mirror Vertical");
		|24BH: ЛогЯдра.пСтроку8(" AC Font Select");
		|24CH: ЛогЯдра.пСтроку8(" AC Font Color");
		|24DH: ЛогЯдра.пСтроку8(" AC Font Size");
		|24EH: ЛогЯдра.пСтроку8(" AC Justify Left");
		|24FH: ЛогЯдра.пСтроку8(" AC Justify Center H");
		|250H: ЛогЯдра.пСтроку8(" AC Justify Right");
		|251H: ЛогЯдра.пСтроку8(" AC Justify Block H");
		|252H: ЛогЯдра.пСтроку8(" AC Justify Top");
		|253H: ЛогЯдра.пСтроку8(" AC Justify Center V");
		|254H: ЛогЯдра.пСтроку8(" AC Justify Bottom");
		|255H: ЛогЯдра.пСтроку8(" AC Justify Block V");
		|256H: ЛогЯдра.пСтроку8(" AC Indent Decrease");
		|257H: ЛогЯдра.пСтроку8(" AC Indent Increase");
		|258H: ЛогЯдра.пСтроку8(" AC Numbered List");
		|259H: ЛогЯдра.пСтроку8(" AC Restart Numbering");
		|25AH: ЛогЯдра.пСтроку8(" AC Bulleted List");
		|25BH: ЛогЯдра.пСтроку8(" AC Promote");
		|25CH: ЛогЯдра.пСтроку8(" AC Demote");
		|25DH: ЛогЯдра.пСтроку8(" AC Yes");
		|25EH: ЛогЯдра.пСтроку8(" AC No");
		|25FH: ЛогЯдра.пСтроку8(" AC Cancel");
		|260H: ЛогЯдра.пСтроку8(" AC Catalog");
		|261H: ЛогЯдра.пСтроку8(" AC Buy/Checkout");
		|262H: ЛогЯдра.пСтроку8(" AC Add to Cart");
		|263H: ЛогЯдра.пСтроку8(" AC Expand");
		|264H: ЛогЯдра.пСтроку8(" AC Expand All");
		|265H: ЛогЯдра.пСтроку8(" AC Collapse");
		|266H: ЛогЯдра.пСтроку8(" AC Collapse All");
		|267H: ЛогЯдра.пСтроку8(" AC Print Preview");
		|268H: ЛогЯдра.пСтроку8(" AC Paste Special");
		|269H: ЛогЯдра.пСтроку8(" AC Insert Mode");
		|26AH: ЛогЯдра.пСтроку8(" AC Delete");
		|26BH: ЛогЯдра.пСтроку8(" AC Lock");
		|26CH: ЛогЯдра.пСтроку8(" AC Unlock");
		|26DH: ЛогЯдра.пСтроку8(" AC Protect");
		|26EH: ЛогЯдра.пСтроку8(" AC Unprotect");
		|26FH: ЛогЯдра.пСтроку8(" AC Attach Comment");
		|270H: ЛогЯдра.пСтроку8(" AC Delete Comment");
		|271H: ЛогЯдра.пСтроку8(" AC View Comment");
		|272H: ЛогЯдра.пСтроку8(" AC Select Word");
		|273H: ЛогЯдра.пСтроку8(" AC Select Sentence");
		|274H: ЛогЯдра.пСтроку8(" AC Select Paragraph");
		|275H: ЛогЯдра.пСтроку8(" AC Select Column");
		|276H: ЛогЯдра.пСтроку8(" AC Select Row");
		|277H: ЛогЯдра.пСтроку8(" AC Select Table");
		|278H: ЛогЯдра.пСтроку8(" AC Select Object");
		|279H: ЛогЯдра.пСтроку8(" AC Redo/Repeat");
		|27AH: ЛогЯдра.пСтроку8(" AC Sort");
		|27BH: ЛогЯдра.пСтроку8(" AC Sort Ascending");
		|27CH: ЛогЯдра.пСтроку8(" AC Sort Descending");
		|27DH: ЛогЯдра.пСтроку8(" AC Filter");
		|27EH: ЛогЯдра.пСтроку8(" AC Set Clock");
		|27FH: ЛогЯдра.пСтроку8(" AC View Clock");
		|280H: ЛогЯдра.пСтроку8(" AC Select Time Zone");
		|281H: ЛогЯдра.пСтроку8(" AC Edit Time Zones");
		|282H: ЛогЯдра.пСтроку8(" AC Set Alarm");
		|283H: ЛогЯдра.пСтроку8(" AC Clear Alarm");
		|284H: ЛогЯдра.пСтроку8(" AC Snooze Alarm");
		|285H: ЛогЯдра.пСтроку8(" AC Reset Alarm");
		|286H: ЛогЯдра.пСтроку8(" AC Synchronize");
		|287H: ЛогЯдра.пСтроку8(" AC Send/Receive");
		|288H: ЛогЯдра.пСтроку8(" AC Send To");
		|289H: ЛогЯдра.пСтроку8(" AC Reply");
		|28AH: ЛогЯдра.пСтроку8(" AC Reply All");
		|28BH: ЛогЯдра.пСтроку8(" AC Forward Msg");
		|28CH: ЛогЯдра.пСтроку8(" AC Send");
		|28DH: ЛогЯдра.пСтроку8(" AC Attach File");
		|28EH: ЛогЯдра.пСтроку8(" AC Upload");
		|28FH: ЛогЯдра.пСтроку8(" AC Download (Save Target As)");
		|290H: ЛогЯдра.пСтроку8(" AC Set Borders");
		|291H: ЛогЯдра.пСтроку8(" AC Insert Row");
		|292H: ЛогЯдра.пСтроку8(" AC Insert Column");
		|293H: ЛогЯдра.пСтроку8(" AC Insert File");
		|294H: ЛогЯдра.пСтроку8(" AC Insert Picture");
		|295H: ЛогЯдра.пСтроку8(" AC Insert Object");
		|296H: ЛогЯдра.пСтроку8(" AC Insert Symbol");
		|297H: ЛогЯдра.пСтроку8(" AC Save and Close");
		|298H: ЛогЯдра.пСтроку8(" AC Rename");
		|299H: ЛогЯдра.пСтроку8(" AC Merge");
		|29AH: ЛогЯдра.пСтроку8(" AC Split");
		|29BH: ЛогЯдра.пСтроку8(" AC Disribute Horizontally");
		|29CH: ЛогЯдра.пСтроку8(" AC Distribute Vertically");
		(*|29D-FFFF Reserved*)
	иначе
		ЛогЯдра.пСтроку8("Reserved");
	всё;
кон PrintConsumerPage;

(*print out the decoded usage for a usage page
 *	param	page,	usage page
 *		 	pos, 	usage of the usage page [page]
 *)
проц PrintUsagePage*(page, pos: цел32);
нач
	просей page из
		|GenericDesktopPage: 	PrintGenericDesktop(pos);
		|KeyboardPage:			PrintKeyboardPage(pos);
		|KeypadPage:			PrintKeyboardPage(pos);
		|LedPage:				PrintLedPage(pos);
		|ButtonPage:			PrintButtonPage(pos);
		|ConsumerPage:			PrintConsumerPage(pos);
	иначе
		ЛогЯдра.пСтроку8("NO IMPLEMENTATION FOR USAGE PAGE(");ЛогЯдра.пЦел64(page,0); ЛогЯдра.пСтроку8(") USAGE(");ЛогЯдра.пЦел64(pos,1); ЛогЯдра.пСтроку8(")");
	всё;
кон PrintUsagePage;

проц PrintUsagePageName*(page: цел32);
нач
	просей page из
		|GenericDesktopPage: 	ЛогЯдра.пСтроку8("GenericDesktop");
		|KeyboardPage:			ЛогЯдра.пСтроку8("KeyboardPage");
		|KeypadPage:			ЛогЯдра.пСтроку8("KeypadPage");
		|LedPage:				ЛогЯдра.пСтроку8("LedPage");
		|ButtonPage:			ЛогЯдра.пСтроку8("ButtonPage");
		|ConsumerPage:			ЛогЯдра.пСтроку8("ConsumerPage");
	иначе
		ЛогЯдра.пСтроку8("n/a");
	всё;
кон PrintUsagePageName;

кон UsbHidUP.

System.Free UsbHidUP~
