модуль Kernel;	(** AUTHOR "pjm, G.F."; PURPOSE "Implementation-independent kernel interface"; *)

использует Трассировка, Machine, Heaps, Objects;

конст
	TimerFree = 0; TimerSleeping = 1; TimerWoken = 2; TimerExpired = 3;	(* Timer state *)

	Second* = Machine.Second;

тип
	(** Finalizer for FinalizedCollection.Add. *)
	Finalizer* = Heaps.Finalizer;	(** PROCEDURE (obj: ANY) *)


	(** Enumerator for FinalizedCollection.Enumerate. *)
	Enumerator* = проц {делегат} (obj: динамическиТипизированныйУкль; перем cont: булево);

	FinalizerNode = укль на запись (Heaps.FinalizerNode)
		nextObj {неОтслСборщиком}: FinalizerNode;	(* in collection *)
	кон;

	(** Polling timer. *)
	MilliTimer* = запись start, target: цел32 кон;

тип
	(** Delay timer. *)
	Timer* = окласс
		перем timer: Objects.Timer; state: цел8;

		проц HandleTimeout;
		нач {единолично}
			если state # TimerFree то state := TimerExpired всё
		кон HandleTimeout;

		(** Delay the calling process the specified number of milliseconds or until Wakeup is called. Only one process may sleep on a specific timer at a time. *)
		проц Sleep*(ms: цел32);
		нач {единолично}
			утв(state = TimerFree);	(* only one process may sleep on a timer *)
			state := TimerSleeping;
			Objects.SetTimeout(timer, HandleTimeout, ms);
			дождись(state # TimerSleeping);
			если state # TimerExpired то Objects.CancelTimeout( timer ) всё;
			state := TimerFree
		кон Sleep;


		(** Wake up the process sleeping on the timer, if any. *)
		проц Wakeup*;
		нач {единолично}
			если state = TimerSleeping то state := TimerWoken всё
		кон Wakeup;

		(** Initializer. *)
		проц &Init*;
		нач
			state := TimerFree;  нов( timer )
		кон Init;

	кон Timer;

тип
	(** A collection of objects that are finalized automatically by the garbage collector. *)
	FinalizedCollection* = окласс(Heaps.FinalizedCollection)
		перем root: FinalizerNode;	(* weak list of contents linked by nextObj *)

		(** Add obj to collection. Parameter fin specifies finalizer, or NIL if not required. *)	(* may be called multiple times *)
		проц Add*(obj: динамическиТипизированныйУкль; fin: Finalizer);
		перем n: FinalizerNode;
		нач
			нов(n); n.collection := сам; n.finalizer := fin;
			Heaps.AddFinalizer(obj, n);
			нач {единолично}
				n.nextObj := root.nextObj; root.nextObj := n	(* add to collection *)
			кон
		кон Add;

		(** Remove one occurrence of obj from collection. *)
		проц Remove*(obj: динамическиТипизированныйУкль);
		перем p, n: FinalizerNode;
		нач {единолично}
			p := root; n := p.nextObj;
			нцПока (n # НУЛЬ) и (n.objWeak # obj) делай
				p := n; n := n.nextObj
			кц;
			если n # НУЛЬ то p.nextObj := n.nextObj всё;
			(* leave in global finalizer list *)
		кон Remove;

		(** Remove all occurrences of obj from collection. *)
		проц {перекрыта}RemoveAll*(obj: динамическиТипизированныйУкль);
		перем p, n: FinalizerNode;
		нач {единолично}
			p := root; n := p.nextObj;
			нцПока n # НУЛЬ делай
				если n.objWeak = obj то
					p.nextObj := n.nextObj
				иначе
					p := n
				всё;
				n := n.nextObj
			кц
		кон RemoveAll;

		(** Enumerate all objects in the collection (Enumerator may not call Remove, Add, Enumerate or Clear). *)
		проц Enumerate*(enum: Enumerator);
		перем fn, next: FinalizerNode; cont: булево;
		нач {единолично}
			fn := root.nextObj; cont := истина;
			нцПока fn # НУЛЬ делай
				next := fn.nextObj;	(* current (or other) object may be removed by enum call *)
				enum(fn.objWeak, cont);
				если cont то fn := next иначе fn := НУЛЬ всё
			кц
		кон Enumerate;

		(** Enumerate all objects in the collection not being finalized (Enumerator may not call Remove, Add, Enumerate or Clear). *)
		проц EnumerateN*( enum: Enumerator );
		перем fn, next: FinalizerNode; cont: булево; obj: динамическиТипизированныйУкль;
		нач {единолично}
			fn := root.nextObj; cont := истина;
			нцПока fn # НУЛЬ делай
				next := fn.nextObj;	(* current (or other) object may be removed by enum call *)
				obj := НУЛЬ;

				Machine.Acquire( Machine.Heaps );	(* prevent GC from running *)

				если (fn.objWeak # НУЛЬ ) и (fn.objStrong = НУЛЬ ) то (* object is not yet on the finalizers list *)
					obj := fn.objWeak; (* now object is locally referenced, will therefore not be GCed *)
				всё;

				Machine.Release( Machine.Heaps );

				если obj # НУЛЬ то enum( obj, cont ); всё;
				если cont то fn := next иначе fn := НУЛЬ всё
			кц
		кон EnumerateN;

		(** Initialize new collection. May also be called to clear an existing collection. *)
		проц &Clear*;
		нач {единолично}
			нов(root); root.nextObj := НУЛЬ	(* head *)
		кон Clear;

	кон FinalizedCollection;



перем
	second*: цел32;	(** number of timer counts per second (Hz) *)


(** Activate the garbage collector immediately. *)
проц GC*;
нач
	Heaps.GC
кон GC;



(** -- Timers -- *)

(** Get the current timer count. Timer increment rate is stored in "second" variable in Hz. *)

проц GetTicks*(): цел32;
нач
	возврат Machine.ticks
кон GetTicks;

(** Set timer to expire in approximately "ms" milliseconds. *)
проц SetTimer*(перем t: MilliTimer; ms: цел32);
нач
	если Second # 1000 то	(* convert to ticks *)
		утв((ms >= 0) и (ms <= матМаксимум(цел32) DIV Second));
		ms := ms * Second DIV 1000
	всё;
	если ms < 5 то увел(ms) всё;	(* Nyquist adjustment *)
	t.start := Machine.ticks;
	t.target := t.start + ms
кон SetTimer;

(** Test whether a timer has expired. *)
проц Expired*(конст t: MilliTimer): булево;
нач
	возврат Machine.ticks - t.target >= 0
кон Expired;

(** Return elapsed time on a timer in milliseconds. *)
проц Elapsed*(конст t: MilliTimer): цел32;
нач
	возврат (Machine.ticks - t.start) * (1000 DIV Second)
кон Elapsed;

(** Return time left on a timer in milliseconds. *)
проц Left*(конст t: MilliTimer): цел32;
нач
	возврат (t.target - Machine.ticks) * (1000 DIV Second)
кон Left;

нач
	утв(1000 остОтДеленияНа Second = 0);	(* for Elapsed *)
	second := Second;
	Heaps.GC := Heaps.InvokeGC;
(*	Machine.Acquire(Machine.TraceOutput);
	Trace.String( Machine.version ); Trace.String(": Kernel: Initialized and started."); Trace.Ln;
	Machine.Release(Machine.TraceOutput);	*)
кон Kernel.

(**
Notes:
o The FinalizedCollection object implements collections of finalized objects.
o Objects added to a finalized collection (with Add) are removed automatically by the garbage collector when no references to them exist any more. They can also be removed explicitly with Remove.
o All the objects currently in a collection can be enumerated by Enumerate, which takes an enumerator procedure as parameter. The enumerator can also be a method in an object, which is useful when state information is required during the enumeration. The enumerator may not call other methods of the same collection.
o An object in a finalized collection can have an finalizer procedure associated with it, which gets called by a separate process when there are no references left to the object any more. A finalizer is usually used for some cleanup functions, e.g. releasing external resources. It is executed exactly once per object. During the next garbage collector cycle the object is finally removed.
*)

(*
to do:
o cancel finalizer when removing object
o fix module free race: module containing finalizer is freed. although the finalizer list is cleared, the FinalizerCaller has already taken a reference to a finalizer, but hasn't called it yet.
o consider: a module has a FinalizedCollection, without finalizers (NIL). when the module is freed, the objects are still in the finalization list, and will get finalized in the next garbage collection. The FinalizedCollection will survive the first collection, as the objects all have references to it through their c field. After all objects have been finalized, the FinalizedCollection itself is collected. No dangling pointers occur, except the untraced module field references from the type descriptors, which are only used for tracing purposes.
o check cyclic dependencies between finalized objects.
o GetTime(): SIGNED32 - return current time in ms
o Delay(td: SIGNED32) - wait td ms
o AwaitTime(t: SIGNED32) - wait at least until time t
o Wakeup(obj: ANY) - wake up object that is waiting
*)
