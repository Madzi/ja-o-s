модуль SVGRenderer;

использует SVG, SVGColors, SVGGradients, SVGFilters, SVGUtilities, XMLObjects,
	Gfx, GfxBuffer, GfxPaths, GfxImages, GfxMatrix, Math, Raster;

тип
	RenderTargetPool=окласс
		перем list: XMLObjects.ArrayCollection; (* container for pooled render targets *)

		проц &New*;
		нач
			нов(list)
		кон New;

		(* Allocate an unused or new document *)
		проц Alloc*(перем doc: SVG.Document; width, height: размерМЗ);
		перем p: динамическиТипизированныйУкль;
		нач
			если list.GetNumberOfElements()=0 то
				doc := SVG.NewDocument(width, height);
			иначе
				p := list.GetElement(list.GetNumberOfElements()-1);
				doc := p(SVG.Document);
				list.Remove(doc);
				Raster.Clear(doc);
			всё
		кон Alloc;

		(* Add doc to pool *)
		проц Free*(doc: SVG.Document);
		нач
			list.Add(doc)
		кон Free;
	кон RenderTargetPool;
	Renderer*=окласс
		перем
			gradients*: SVGGradients.GradientDict; (* container of all defined gradients *)
			filters*: SVGFilters.FilterDict; (* container of all defined filters *)

			ctxt: GfxBuffer.Context; (* context of the graphics library *)
			mode: мнвоНаБитахМЗ; (* drawing mode for the graphics library (filled, stroked, ...) *)
			rasterMode: Raster.Mode; (* mode for the raster library (srcOverDst) *)

			filterStack: SVGFilters.FilterStack; (* stack of active filters *)
			renderTarget: SVG.Document; (* current render target *)
			renderTargetPool: RenderTargetPool; (* pool of unused render targets *)

		проц &New*;
		нач
			нов(gradients);
			нов(filters);
			нов(filterStack);
			нов(renderTargetPool);

			Raster.InitMode(rasterMode, Raster.srcOverDst);
		кон New;

		(* Fill target white *)
		проц FillWhite*(state: SVG.State);
		перем
			white: Raster.Pixel;
			copyMode: Raster.Mode;
		нач
			Raster.SetRGB(white,0FFH,0FFH,0FFH);
			Raster.InitMode(copyMode, Raster.srcCopy);
			Raster.Fill(state.target, 0, 0, state.target.width, state.target.height, white, copyMode)
		кон FillWhite;

		(* Prepare for rendering *)
		проц RenderBegin(state: SVG.State; recordPathMode: булево);
		нач
			нов(ctxt);

			если state.style.stroke.type = SVG.PaintURI то state.transparencyUsed := истина всё;
			если state.style.fill.type = SVG.PaintURI то state.transparencyUsed := истина всё;

			если state.transparencyUsed то
				renderTargetPool.Alloc(renderTarget, state.target.width, state.target.height);
				GfxBuffer.Init(ctxt, renderTarget);
			иначе
				GfxBuffer.Init(ctxt, state.target);
			всё;

			если recordPathMode то
				mode := {Gfx.Record};
			иначе
				mode := {};
				SetMatrix(state.userToWorldSpace,ctxt);
			всё;
		кон RenderBegin;

		(* Finalize the rendering *)
		проц RenderEnd(state: SVG.State; recordPathMode: булево);
		перем pattern: Gfx.Pattern;
			worldBBox, objectBBox: SVG.Box;
			minx, miny, maxx, maxy: цел32;
			strokeWidth: SVG.Length;
		нач
			если recordPathMode то
				mode:={};

			(* Calculate bounding box of path *)
				GetBBoxes(ctxt.path, state.userToWorldSpace, worldBBox, objectBBox);

			(* Calculate and set stroke width *)
				strokeWidth := state.userToWorldSpace.TransformLength(state.style.strokeWidth);
				Gfx.SetLineWidth(ctxt, устарПреобразуйКБолееУзкомуЦел(strokeWidth));

			(* Enlarge bounding box for thick strokes *)
				worldBBox.x := worldBBox.x - strokeWidth;
				worldBBox.y := worldBBox.y - strokeWidth;
				worldBBox.width := worldBBox.width + 2*strokeWidth;
				worldBBox.height := worldBBox.height + 2*strokeWidth;

			(* Set the stroke style *)
				если state.style.stroke.type = SVG.PaintColor то
					Gfx.SetStrokeColor(ctxt, GetColor(state.style.stroke.color));
					mode := mode + {Gfx.Stroke}
				аесли state.style.stroke.type = SVG.PaintURI то
					pattern  := gradients.GetGradientAsPattern(ctxt, state.style.stroke.uri, worldBBox, objectBBox, state.userToWorldSpace, state.viewport);
					если pattern#НУЛЬ то
						Gfx.SetFillPattern(ctxt, pattern);
						mode := mode + {Gfx.Stroke}
					всё
				всё;

			(* Set the fill style *)
				если state.style.fill.type = SVG.PaintColor то
					Gfx.SetFillColor(ctxt, GetColor(state.style.fill.color));
					mode := mode + {Gfx.Fill}
				аесли state.style.fill.type = SVG.PaintURI то
					pattern  := gradients.GetGradientAsPattern(ctxt, state.style.fill.uri, worldBBox, objectBBox, state.userToWorldSpace, state.viewport);
					если pattern#НУЛЬ то
						Gfx.SetFillPattern(ctxt, pattern);
						mode := mode + {Gfx.Fill}
					всё
				всё;

			(* Render the recorded path *)
				Gfx.Render(ctxt, mode)
			иначе
			(* Can not calculate bounding box of unrecorded path -> assume it is as large as the target *)
				worldBBox.x := 0;
				worldBBox.y := 0;
				worldBBox.width := state.target.width-1;
				worldBBox.height := state.target.height-1;
			всё;

			если state.transparencyUsed то
				minx := округлиВниз(worldBBox.x);
				miny := округлиВниз(worldBBox.y);
				maxx := округлиВниз(worldBBox.x+worldBBox.width)+1;
				maxy := округлиВниз(worldBBox.y+worldBBox.height)+1;

				если minx<округлиВниз(state.viewport.x) то minx := округлиВниз(state.viewport.x) всё;
				если miny<округлиВниз(state.viewport.y) то miny := округлиВниз(state.viewport.y) всё;
				если maxx>округлиВниз(state.viewport.x+state.viewport.width) то maxx := округлиВниз(state.viewport.x+state.viewport.width) всё;
				если maxy>округлиВниз(state.viewport.y+state.viewport.height) то maxy :=округлиВниз(state. viewport.y+state.viewport.height) всё;

				если (minx<=maxx) и (miny<=maxy) то
					Raster.Copy(renderTarget, state.target, minx, miny, maxx, maxy, minx, miny,  rasterMode)
				всё;

				renderTargetPool.Free(renderTarget)
			всё
		кон RenderEnd;

		(* Prepare use of a filter *)
		проц BeginFilter*(filter: SVGFilters.Filter; state: SVG.State);
		нач
			filterStack.Push(filter);
			если filter#НУЛЬ то
				state.Push();
				state.target := SVG.NewDocument(state.target.width, state.target.height);
			всё
		кон BeginFilter;

		(* Finalize use of a filter *)
		проц EndFilter*(state: SVG.State);
		перем filterTarget: SVG.Document;
			filter: SVGFilters.Filter;
		нач
			filterStack.Pop(filter);
			если filter#НУЛЬ то
				filterTarget := state.target;
				state.Pop();
				filter.Apply(filterTarget, state.target);
			всё
		кон EndFilter;

		(* Draw a cubic bezier curve *)
		проц Bezier3To(current, bezier1,  bezier2: SVG.Coordinate);
		нач
			Gfx.BezierTo (ctxt,
				устарПреобразуйКБолееУзкомуЦел(current.x), устарПреобразуйКБолееУзкомуЦел(current.y),
				устарПреобразуйКБолееУзкомуЦел(bezier1.x), устарПреобразуйКБолееУзкомуЦел(bezier1.y),
				устарПреобразуйКБолееУзкомуЦел(bezier2.x), устарПреобразуйКБолееУзкомуЦел(bezier2.y));
		кон Bezier3To;

		(* Draw a quadrativ bezier curve *)
		проц Bezier2To(start, bezier, end: SVG.Coordinate);
		перем bezier1, bezier2: SVG.Coordinate;
		нач
			bezier1.x := (bezier.x-start.x)*2.0/3.0+start.x;
			bezier1.y := (bezier.y-start.y)*2.0/3.0+start.y;
			bezier2.x := end.x-(end.x-bezier.x)*2.0/3.0;
			bezier2.y := end.y-(end.y-bezier.y)*2.0/3.0;
			Bezier3To(end, bezier1, bezier2);
		кон Bezier2To;

		(* Draw an arc *)
		проц ArcTo(radius, flags, start, end: SVG.Coordinate; xrot: SVG.Length);
		перем cos, sin, rx2, ry2, tmp: SVG.Length;
			diff0, diff, center0, center, d1, d2: SVG.Coordinate;
		нач
		(* Interpret out-of-range parameters *)
			если (start.x=end.x) и (start.y=end.y) то возврат всё;
			если (radius.x=0) или (radius.y=0) то
				Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(end.x), устарПреобразуйКБолееУзкомуЦел(end.y));
				возврат
			всё;
			radius.x := матМодуль(radius.x);
			radius.y := матМодуль(radius.y);
			если flags.x#0 то flags.x := 1.0 всё;
			если flags.y#0 то flags.y := 1.0 всё;

		(* Calculate center *)
			cos := Math.cos(устарПреобразуйКБолееУзкомуЦел(xrot/180.0*Math.pi));
			sin := Math.sin(устарПреобразуйКБолееУзкомуЦел(xrot/180.0*Math.pi));
			diff.x := (start.x-end.x)/2;
			diff.y := (start.y-end.y)/2;
			diff0.x := cos*diff.x+sin*diff.y;
			diff0.y := -sin*diff.x+cos*diff.y;
			tmp := diff0.x*diff0.x/(radius.x*radius.x)+diff0.y*diff0.y/(radius.y*radius.y);
			если tmp > 1 то
				tmp := Math.sqrt(устарПреобразуйКБолееУзкомуЦел(tmp));
				radius.x := tmp*radius.x;
				radius.y := tmp*radius.y;
				tmp := 0;
			иначе
				rx2 := radius.x*radius.x;
				ry2 := radius.y*radius.y;
				tmp := rx2*diff0.y*diff0.y+ry2*diff0.x*diff0.x;
				tmp := (rx2*ry2-tmp)/tmp;
				если tmp <= 0 то tmp := 0 всё;
				tmp := Math.sqrt(устарПреобразуйКБолееУзкомуЦел(tmp));
				если flags.x=flags.y то tmp := -tmp всё;
			всё;
			center0.x := tmp*radius.x*diff0.y/radius.y;
			center0.y := -tmp*radius.y*diff0.x/radius.x;
			center.x := cos*center0.x-sin*center0.y+(start.x+end.x)/2;
			center.y := sin*center0.x+cos*center0.y+(start.y+end.y)/2;

		(* Calculate conjugate diameter pair *)
			d1.x := center.x+radius.x*cos;
			d1.y := center.y+radius.x*sin;
			d2.x := center.x-radius.y*sin;
			d2.y := center.y+radius.y*cos;
			если (flags.y = 0.0) = (d1.x*d2.y-d1.y*d2.x > 0.0) то
				tmp := d1.x;
				d1.x := d2.x;
				d2.x := tmp;
				tmp := d1.y;
				d1.y := d2.y;
				d2.y := tmp;
			всё;

		(* Draw arc *)
			Gfx.ArcTo(ctxt,
				устарПреобразуйКБолееУзкомуЦел(end.x), устарПреобразуйКБолееУзкомуЦел(end.y),
				устарПреобразуйКБолееУзкомуЦел(center.x), устарПреобразуйКБолееУзкомуЦел(center.y),
				устарПреобразуйКБолееУзкомуЦел(d1.x), устарПреобразуйКБолееУзкомуЦел(d1.y),
				устарПреобразуйКБолееУзкомуЦел(d2.x), устарПреобразуйКБолееУзкомуЦел(d2.y))
		кон ArcTo;

		(* Render an image *)
		проц RenderImage*(x, y, width, height: SVG.Length; image: SVG.Document; state: SVG.State);
		перем filter: GfxImages.Filter;
		нач
			GfxImages.InitLinearFilter(filter);
			state.userToWorldSpace := state.userToWorldSpace.Translate(-x, -y);
			state.userToWorldSpace := state.userToWorldSpace.Scale(width / image.width, height / image.height);
			x := x*image.width / width;
			y := y*image.height / height;
			state.userToWorldSpace := state.userToWorldSpace.Translate(x, y);

		(* Gfx cannot record calls to DrawImageAt *)
			RenderBegin(state, ложь);
			Gfx.DrawImageAt(ctxt, устарПреобразуйКБолееУзкомуЦел(x), устарПреобразуйКБолееУзкомуЦел(y), image, filter);
			RenderEnd(state, ложь);
		кон RenderImage;

		(* Render a rectangle*)
		проц RenderRect*(x, y, width, height: SVG.Length; state: SVG.State);
		нач
			RenderBegin(state,истина);
			Gfx.DrawRect(ctxt, устарПреобразуйКБолееУзкомуЦел(x), устарПреобразуйКБолееУзкомуЦел(y), устарПреобразуйКБолееУзкомуЦел(x+width), устарПреобразуйКБолееУзкомуЦел(y+height), mode);
			RenderEnd(state, истина);
		кон RenderRect;

		(* Render a rounded rectangle *)
		проц RenderRoundedRect*(x, y, width, height, rx, ry: SVG.Length; state: SVG.State);
		нач
			RenderBegin(state, истина);

			если rx>width/2 то rx := width/2 всё;
			если ry>height/2 то ry := height/2 всё;

			Gfx.Begin(ctxt, mode);
			Gfx.MoveTo(ctxt, устарПреобразуйКБолееУзкомуЦел(x+width/2), устарПреобразуйКБолееУзкомуЦел(y));
			Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(x+width-rx), устарПреобразуйКБолееУзкомуЦел(y));
			Gfx.ArcTo(ctxt,устарПреобразуйКБолееУзкомуЦел(x+width), устарПреобразуйКБолееУзкомуЦел(y+ry),
				устарПреобразуйКБолееУзкомуЦел(x+width-rx), устарПреобразуйКБолееУзкомуЦел(y+ry),
				устарПреобразуйКБолееУзкомуЦел(x+width-rx), устарПреобразуйКБолееУзкомуЦел(y),
				устарПреобразуйКБолееУзкомуЦел(x+width), устарПреобразуйКБолееУзкомуЦел(y+ry));
			Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(x+width), устарПреобразуйКБолееУзкомуЦел(y+height-ry));
			Gfx.ArcTo(ctxt,устарПреобразуйКБолееУзкомуЦел(x+width-rx), устарПреобразуйКБолееУзкомуЦел(y+height),
				устарПреобразуйКБолееУзкомуЦел(x+width-rx), устарПреобразуйКБолееУзкомуЦел(y+height-ry),
				устарПреобразуйКБолееУзкомуЦел(x+width), устарПреобразуйКБолееУзкомуЦел(y+height-ry),
				устарПреобразуйКБолееУзкомуЦел(x+width-rx), устарПреобразуйКБолееУзкомуЦел(y+height));
			Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(x+rx), устарПреобразуйКБолееУзкомуЦел(y+height));
			Gfx.ArcTo(ctxt,устарПреобразуйКБолееУзкомуЦел(x), устарПреобразуйКБолееУзкомуЦел(y+height-ry),
				устарПреобразуйКБолееУзкомуЦел(x+rx), устарПреобразуйКБолееУзкомуЦел(y+height-ry),
				устарПреобразуйКБолееУзкомуЦел(x+rx), устарПреобразуйКБолееУзкомуЦел(y+height),
				устарПреобразуйКБолееУзкомуЦел(x), устарПреобразуйКБолееУзкомуЦел(y+height-ry));
			Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(x), устарПреобразуйКБолееУзкомуЦел(y+ry));
			Gfx.ArcTo(ctxt,устарПреобразуйКБолееУзкомуЦел(x+rx), устарПреобразуйКБолееУзкомуЦел(y),
				устарПреобразуйКБолееУзкомуЦел(x+rx), устарПреобразуйКБолееУзкомуЦел(y+ry),
				устарПреобразуйКБолееУзкомуЦел(x), устарПреобразуйКБолееУзкомуЦел(y+ry),
				устарПреобразуйКБолееУзкомуЦел(x+rx), устарПреобразуйКБолееУзкомуЦел(y));
			Gfx.Close(ctxt);
			Gfx.End(ctxt);

			RenderEnd(state, истина);
		кон RenderRoundedRect;

		(* Render a circle *)
		проц RenderCircle*(x, y, r: SVG.Length; state: SVG.State);
		нач
			RenderBegin(state, истина);
			Gfx.DrawCircle(ctxt, устарПреобразуйКБолееУзкомуЦел(x), устарПреобразуйКБолееУзкомуЦел(y), устарПреобразуйКБолееУзкомуЦел(r), mode);
			RenderEnd(state, истина);
		кон RenderCircle;

		(* Render an ellipse *)
		проц RenderEllipse*(x, y, rx, ry: SVG.Length; state: SVG.State);
		нач
			RenderBegin(state, истина);
			Gfx.DrawEllipse(ctxt, устарПреобразуйКБолееУзкомуЦел(x), устарПреобразуйКБолееУзкомуЦел(y), устарПреобразуйКБолееУзкомуЦел(rx), устарПреобразуйКБолееУзкомуЦел(ry), mode);
			RenderEnd(state, истина);
		кон RenderEllipse;

		(* Render a line *)
		проц RenderLine*(x1, y1, x2, y2: SVG.Length; state: SVG.State);
		нач
			RenderBegin(state, истина);
			Gfx.DrawLine(ctxt, устарПреобразуйКБолееУзкомуЦел(x1), устарПреобразуйКБолееУзкомуЦел(y1), устарПреобразуйКБолееУзкомуЦел(x2), устарПреобразуйКБолееУзкомуЦел(y2), mode-{Gfx.Fill, Gfx.Clip, Gfx.EvenOdd});
			RenderEnd(state, истина);
		кон RenderLine;

		(* Render an open polyline or a closed polygon *)
		проц RenderPoly*(points: SVG.String; closed: булево; state: SVG.State);
		перем i: размерМЗ;
			current: SVG.Coordinate;
		нач
			RenderBegin(state, истина);
			Gfx.Begin(ctxt, mode);

			i := 0;
			SVGUtilities.SkipWhiteSpace(i, points);
			SVG.ParseCoordinate(points, i, current, ложь);
			SVGUtilities.SkipWhiteSpace(i, points);
			Gfx.MoveTo(ctxt, устарПреобразуйКБолееУзкомуЦел(current.x), устарПреобразуйКБолееУзкомуЦел(current.y));

			нцПока points[i] # 0X делай
				SVG.ParseCoordinate(points, i, current, ложь);
				SVGUtilities.SkipWhiteSpace(i, points);
				Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(current.x), устарПреобразуйКБолееУзкомуЦел(current.y));
			кц;
			если closed то
				Gfx.Close(ctxt);
			всё;
			Gfx.End(ctxt);

			RenderEnd(state, истина);
		кон RenderPoly;

		(* Render a general path element *)
		проц RenderPath*(d: SVG.String; state: SVG.State);
		перем i: размерМЗ;
			subPathStart, current, last: SVG.Coordinate;
			relative: булево;
			command, prevCommand: симв8;
			arcR, arcFlags: SVG.Coordinate;
			arcAngle: SVG.Length;
			bezier1, bezier2: SVG.Coordinate;
		нач
			RenderBegin(state, истина);
			Gfx.Begin(ctxt, mode);

			current.x := 0;
			current.y := 0;
			command := 0X;
			i := 0;
			SVGUtilities.SkipWhiteSpace(i, d);

			если (d[i] # 'm') и (d[i] # 'M') то
				SVG.Error("PathData error: missing moveto")
			всё;

			нцПока d[i] # 0X делай
				SVGUtilities.SkipWhiteSpace(i, d);

				prevCommand := command;
				last := current;
				если SVGUtilities.IsAlpha(d[i]) то
					relative := SVGUtilities.IsLowercase(d[i]);
					command := d[i];
					увел(i);
					SVGUtilities.SkipWhiteSpace(i, d);
				всё;

				просей command из
					'm', 'M':
						SVG.ParseCoordinate(d, i, current, relative);
						subPathStart := current;
						Gfx.MoveTo(ctxt, устарПреобразуйКБолееУзкомуЦел(current.x), устарПреобразуйКБолееУзкомуЦел(current.y));
					| 'z', 'Z':
						current := subPathStart;
						Gfx.Close(ctxt);
					| 'l', 'L':
						SVG.ParseCoordinate(d, i, current, relative);
						Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(current.x), устарПреобразуйКБолееУзкомуЦел(current.y));
					| 'h', 'H':
						SVG.ParseCoordinate1(d, i, current.x, relative);
						Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(current.x), устарПреобразуйКБолееУзкомуЦел(current.y));
					| 'v', 'V':
						SVG.ParseCoordinate1(d, i, current.y, relative);
						Gfx.LineTo(ctxt, устарПреобразуйКБолееУзкомуЦел(current.x), устарПреобразуйКБолееУзкомуЦел(current.y));
					| 'c', 'C':
						bezier1:=current;
						bezier2:=current;
						SVG.ParseCoordinate(d, i, bezier1, relative);
						SVG.ParseCoordinate(d, i, bezier2, relative);
						SVG.ParseCoordinate(d, i, current, relative);
						Bezier3To(current, bezier1, bezier2);
					| 's', 'S':
						просей prevCommand из
							's','S','c','C':
								bezier1.x:=current.x-(bezier2.x-current.x);
								bezier1.y:=current.y-(bezier2.y-current.y);
						иначе
							bezier1:=current;
						всё;
						bezier2:=current;
						SVG.ParseCoordinate(d, i, bezier2, relative);
						SVG.ParseCoordinate(d, i, current, relative);
						Bezier3To(current, bezier1, bezier2);
					| 'q', 'Q':
						bezier1:=current;
						SVG.ParseCoordinate(d, i, bezier1, relative);
						SVG.ParseCoordinate(d, i, current, relative);
						Bezier2To(last, bezier1, current);
					| 't', 'T':
						просей prevCommand из
							't','T','q','Q':
								bezier1.x:=current.x-(bezier1.x-current.x);
								bezier1.y:=current.y-(bezier1.y-current.y);
						иначе
							bezier1:=current;
						всё;
						SVG.ParseCoordinate(d, i, current, relative);
						Bezier2To(last, bezier1, current);
					| 'a', 'A':
						SVG.ParseCoordinate(d, i, arcR, ложь);
						SVG.ParseCoordinate1(d, i, arcAngle, ложь);
						SVG.ParseCoordinate(d, i, arcFlags, ложь);
						SVG.ParseCoordinate(d, i, current, relative);
						ArcTo(arcR,arcFlags,last,current,arcAngle);
				иначе
					SVG.Error("PathData error: unknown command");
					d[i] := 0X;
				всё;
				SVGUtilities.SkipWhiteSpace(i, d)
			кц;
			Gfx.End(ctxt);
			RenderEnd(state, истина);
		кон RenderPath;

	кон Renderer;

(* Convert SVG.Color to Gfx.Color *)
проц GetColor(color: SVG.Color): Gfx.Color;
перем c: Gfx.Color;
нач
	SVGColors.Split(color, c.r, c.g, c.b, c.a);
	возврат c
кон GetColor;

(* Calculate the bounding box of a path in worldspace and in object space. Also transform the path to worldspace *)
проц GetBBoxes(path: GfxPaths.Path; objectToWorldSpace: SVG.Transform; перем worldBBox, objectBBox: SVG.Box);
перем
	x1World, y1World, x2World, y2World: вещ32;
	x1Object, y1Object, x2Object, y2Object: вещ32;
	mat: GfxMatrix.Matrix;
нач
	GfxMatrix.Init(mat, устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.a), устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.b),
		устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.c), устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.d),
		устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.e), устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.f));

	GfxPaths.GetBox(path, x1Object, y1Object, x2Object, y2Object);
	objectBBox.x:=x1Object;
	objectBBox.y:=y1Object;
	objectBBox.width :=x2Object-x1Object;
	objectBBox.height :=y2Object-y1Object;

	GfxMatrix.ApplyToRect(mat, x1Object, y1Object, x2Object, y2Object, x1World, y1World, x2World, y2World);
	worldBBox.x := x1World;
	worldBBox.y := y1World;
	worldBBox.width := x2World-x1World;
	worldBBox.height := y2World-y1World;

	GfxPaths.Apply(path, mat);
кон GetBBoxes;

(* Set the matrix to transform to world space. *)
проц SetMatrix(objectToWorldSpace: SVG.Transform; ctxt: GfxBuffer.Context);
перем mat: GfxMatrix.Matrix;
нач
	GfxMatrix.Init(mat, устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.a), устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.b),
		устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.c), устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.d),
		устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.e), устарПреобразуйКБолееУзкомуЦел(objectToWorldSpace.f));
	Gfx.SetCTM(ctxt, mat);
кон SetMatrix;

кон SVGRenderer.
