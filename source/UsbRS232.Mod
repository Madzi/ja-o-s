модуль UsbRS232;  (** AUTHOR "staubesv"; PURPOSE "MCT USB-RS232 Converter Driver"; *)
(**
 *	This module implements a Serials.Port object. It is a Linux port of mct-u232.c, mct-u232.h.
 *
 * Usage:
 *
 *	UsbRS232.Install ~ loads this driver
 *	System.Free UsbRS232 ~
 *
 * Status: BETA
 *
 * Licence: GPL
 *
 * References:

 *	mct_u232.c Linux USB device driver
 *	mct-u232.h contains some documentation
 *
 * History:
 *
 *	20.01.2006	First Release (staubesv)
 *	14.06.2006	Adapted to changes in Serials.Mod (staubesv)
 *	05.07.2006	Adapted to Usbdi (staubesv)
 *	05.01.2007	Introduced Port.Send procedure for better performance (staubesv)
 *
 * Todo:
 *	- Cleanup mess with constants
 *	- testing
 *)

использует НИЗКОУР, Kernel, ЛогЯдра, Modules, Serials, Usbdi;

конст

	Name = "UsbRs232";
	Description = "USB-RS232 Interface Converter Driver";
	Priority = 0;

	BufSize = 1024;

	TraceSend = {0};				(* Display sent characters *)
	TraceReceive = {1};			(* Display received characters *)
	TraceCommands = {2};
	TraceReceiveData = {3};
	TraceReceiveStatus = {4};
	TraceAll = {0..31};
	TraceNone = {};

	Trace = TraceNone;

	Debug = истина;
	Verbose = истина;

	ModuleName = "UsbRS232";

	(* Expected endpoints *)
	EpBulkOut = 02H; 			(* Used to send data *)
	EpInterruptInData = 082H; 	(* Used to receive data *)
	EpInterruptInStatus = 081H; 	(* Signals exception conditions *)

	IdVendorMct = 0711H; 		(* Magic Control Technology *)
	IdProductU232P9 = 230H; 	(* MCT U232-P9 *)
	IdProductU232P25 = 210H; 	(* MCT U232-P25 *)
	IdProductDUH3SP = 200H;	(* D-Link DU-H3SP USB Bay *)

	IdVendorBelkin = 50DH; 		(* Belkin *)
	IdProductF5U109 = 109H; 	(* Belkin F5U109 *)

	(* Vendor specific requests *)
	MctGetModemStatus = 2; 	(* Get modem status register (MSR) *)
	MctSetBaudrate = 5; 		(* Set baudrate divisor *)
	MctGetLineCtrl = 6; 			(* Get line control register (LCR) *)
	MctSetLineCtrl = 7; 			(* Set line control register (LCR) *)
	MctSetModemCtrl = 10; 		(* Set modem control register (MCR)*)
	MctSetUnknown1 = 11; 		(* Both requests are sent after SetBaudrate requests by windows driver *)
	MctSetUnknown2 = 12;

	(* line control register (LCR) *)
	MctLcrSetBreak = 6;
	MctData5 = {};
	MctData6 = {0};
	MctData7 = {1};
	MctData8 = {0,1};
	MctParityNone = {};
	MctParityEven = {3, 4};
	MctParityOdd = {3};
	MctParityMark = {3, 5};
	MctParitySpace = {3,4,5};
	MctStop1= {};
	MctStop2 = {2}; (* 1.5 stop bits for 5 data bits, 2 stop bits for 6, 7 & 8 data bits *)

	(* Modem control register (MCR) *)
	MctMcrRts = 1; 		(* Activate RTS *)
	MctMcrDtr = 0; 		(* Activate DTR *)
	MctMcrOut2 = 3; 	(* Activate Out2 *)

	(* Modem status register (MSR) *)
	MctMsrCd = 7; 		(* current CD *)
	MctMsrRi = 6; 		(* current RI *)
	MctMsrDsr = 5;		(* current DSR *)
	MctMsrCts = 4; 		(* current CTS *)
	MctMsrDcd = 3; 		(* delta CD, unused *)
	MctMsrDri = 2; 		(* delta RI, unused *)
	MctMsrDdsr = 1; 	(* delta DSR, unused *)
	MctMsrDcts = 0; 	(* delta CTS, unused *)

	(* Line status register (LSR) *)
	MctLsrErr = 7; 		(*  PE / FE / BI, unused *)
	MctLsrTemt = 6; 		(* transmit register empty *)
	MctLsrThre = 5; 		(* transmit holding register empty *)
	MctLsrBi = 4; 		(* break indicator *)
	MctLsrFe = 3; 		(* framing error *)
	MctLsrPe = 2; 		(* parity error *)
	MctLsrOe = 1; 		(* overrun error *)
	MctLsrDr = 0; 		(* receive data ready *)

тип

	UsbRs232Driver = окласс (Usbdi.Driver)
	перем
		port : Port;
		controlPipe : Usbdi.Pipe;
		interruptInData, interruptInStatus : Usbdi.Pipe;
		bulkOut : Usbdi.Pipe;
		data1, data4, data64 : Usbdi.BufferPtr; (* datax -> ARRAY x OF CHAR *)
		status2 : Usbdi.BufferPtr;
		diagnostics : мнвоНаБитахМЗ;
		msr : мнвоНаБитахМЗ; (* Modem Status Register: Updated by UpdateStatus() & GetMSR *)
		lsr : мнвоНаБитахМЗ;   (* Line Status Register: Updated by UpdateStatus() *)
		mcr : мнвоНаБитахМЗ; (* Modem Control Register *)
		lcr : мнвоНаБитахМЗ;   (* Line Control Register *)
		mc : мнвоНаБитахМЗ; (*  Serials.DTR, Serials.RTS, Serials.DSR, Serials.CTS, Serials.RI, Serials.DCD & Serials.BI (Break Interrupt) *)

		проц {перекрыта}Connect() : булево;
		перем ignore : Usbdi.Status;
		нач
			(* Get default control pipe *)
			controlPipe := device.GetPipe(0);
			если controlPipe = НУЛЬ то
				если Debug то ShowModule("Couldn't get default control pipe."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			(* Get both interrupt pipes *)
			interruptInData := device.GetPipe(EpInterruptInData);
			interruptInStatus := device.GetPipe(EpInterruptInStatus);
			если (interruptInData = НУЛЬ) или (interruptInStatus = НУЛЬ) то
				если Debug то ShowModule("Couldn't get interrupt in pipes."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			(* Get bulk out pipe *)
			bulkOut := device.GetPipe(EpBulkOut);
			если bulkOut = НУЛЬ то
				если Debug то ShowModule("Couldn't get bulk out pipe."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			(* the interrupt pipe for endpoint EpInterruptStatus is used to asynchronously receive status information... set up the USB transfers. *)
			interruptInStatus.SetTimeout(0);
			interruptInStatus.SetCompletionHandler(UpdateStatus);
			ignore := interruptInStatus.Transfer(2, 0, status2^);

			interruptInData.SetTimeout(0);
			interruptInData.SetCompletionHandler(UpdateData);
			ignore := interruptInData.Transfer(64, 0, data64^);

			(* Get Modem Status Register (MSR) *)
			если ~GetMSR(msr) то
				если Debug то ShowModule("GetMSR failed during connect."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			(* Register port at Serials *)
			нов(port); нов(port.data1, 1); port.driver := сам;
			Serials.RegisterPort(port, Description);
			возврат истина;
		кон Connect;

		проц {перекрыта}Disconnect;
		нач
			Serials.UnRegisterPort(сам.port);
		кон Disconnect;

		(* Interrupt on Completion - Handler for EpInterruptInData; data contained in variable data64 *)
		проц UpdateData(status : Usbdi.Status; actLen : размерМЗ);
		перем ignore : Usbdi.Status; i : размерМЗ;
		нач
			если Trace * TraceReceiveData # {} то
				ShowModule("UpdateData: Received "); ЛогЯдра.пЦел64(actLen, 0); ЛогЯдра.пСтроку8(" Bytes:");
				нцДля i := 0 до actLen - 1 делай ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64(кодСимв8(data64[i]), 0); кц;
				ЛогЯдра.пВК_ПС;
			всё;
			если (status = Usbdi.Ok) или (status = Usbdi.ShortPacket) то
				port.HandleData(data64^, actLen);
				ignore := interruptInData.Transfer(64, 0, data64^);
			иначе
				если Debug то ShowModule("UpdateData failed."); ЛогЯдра.пВК_ПС; всё;
			всё;
		кон UpdateData;

		(* Interrupt on Completion - Handler for EpInterruptInStatus; data contained in variable status2 *)
		проц UpdateStatus(status : Usbdi.Status; actLen : размерМЗ);
		перем ignore : Usbdi.Status;
		нач
			если Trace * TraceReceiveStatus # {} то ShowModule("UpdateStatus: Received "); ЛогЯдра.пЦел64(actLen, 0); ЛогЯдра.пСтроку8(" Bytes: ");  всё;
			если (status = Usbdi.Ok) или (status = Usbdi.ShortPacket) то
				если actLen>=1 то
					msr := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, status2[0]); (* status2[0] is the MSR *)
					если MctMsrRi в msr то включиВоМнвоНаБитах(mc, Serials.RI); иначе исключиИзМнваНаБитах(mc, Serials.RI); всё;
					если MctMsrDsr в msr то включиВоМнвоНаБитах(mc, Serials.DSR); иначе исключиИзМнваНаБитах(mc, Serials.DSR); всё;
					если MctMsrCts в msr то включиВоМнвоНаБитах(mc, Serials.CTS); иначе исключиИзМнваНаБитах(mc, Serials.CTS); всё;
					если MctMsrCd в msr то включиВоМнвоНаБитах(mc,  Serials.DCD); иначе исключиИзМнваНаБитах(mc, Serials.DCD); всё;
					если Trace * TraceReceiveStatus # {} то
						если Serials.RI в mc то ЛогЯдра.пСтроку8("[RI]"); всё;
						если Serials.DSR в mc то ЛогЯдра.пСтроку8("[DSR]"); всё;
						если Serials.CTS в mc то ЛогЯдра.пСтроку8("[CTS]"); всё;
						если Serials.DCD в mc то ЛогЯдра.пСтроку8("[DCD]"); всё;
					всё;
				всё;
				если actLen>=2 то (* okay... I was expecting 2 bytes of data *)
					lsr := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, status2[1]); (* status2[1] is the LSR *)
					diagnostics := {};
					если MctLsrOe в lsr то включиВоМнвоНаБитах(diagnostics, Serials.OverrunError); всё;
					если MctLsrPe в lsr то включиВоМнвоНаБитах(diagnostics, Serials.ParityError); всё;
					если MctLsrFe в lsr то включиВоМнвоНаБитах(diagnostics, Serials.FramingError); всё;
					если MctLsrBi в lsr то
						включиВоМнвоНаБитах(mc, Serials.BreakInterrupt); включиВоМнвоНаБитах(diagnostics, Serials.BreakInterrupt);
					иначе
						исключиИзМнваНаБитах(mc, Serials.BreakInterrupt);
					всё;
					если Trace * TraceReceiveStatus # {} то
						если Serials.BreakInterrupt в mc то ЛогЯдра.пСтроку8("[BI]"); всё;
						ЛогЯдра.пСтроку8(" Errors: ");
						если diagnostics = {} то ЛогЯдра.пСтроку8("none");
						иначе
							если Serials.OverrunError в diagnostics то ЛогЯдра.пСтроку8("[Overrun]"); всё;
							если Serials.ParityError в diagnostics то ЛогЯдра.пСтроку8("[Parity]"); всё;
							если Serials.FramingError в diagnostics то ЛогЯдра.пСтроку8("[Framing]"); всё;
							если Serials.BreakInterrupt в diagnostics то ЛогЯдра.пСтроку8("BreakInterrupt]"); всё;
						всё;
					всё;
				всё;
				ignore := interruptInStatus.Transfer(2, 0, status2^);
			иначе
				если Debug то ShowModule("UpdateStatus failed."); ЛогЯдра.пВК_ПС; всё;
			всё;
			если Trace * TraceReceiveStatus # {} то ЛогЯдра.пВК_ПС; всё;
		кон UpdateStatus;

		(* Set baudrate divisor; returns TRUE if operation succeeded, FALSE otherwise *)
		проц SetBaudrate(baudrate : цел32) : булево;
		перем divisor : цел32; status : Usbdi.Status;
		нач
			если Trace * TraceCommands # {} то ShowModule("SetBaudrate to "); ЛогЯдра.пЦел64(baudrate, 0); ЛогЯдра.пСтроку8(" bps."); ЛогЯдра.пВК_ПС; всё;
			port.portbps := 0; (* indicates invalid value *)

			если ((device.descriptor.idVendor = IdVendorMct) и (device.descriptor.idProduct = IdProductU232P25)) или
			   ((device.descriptor.idVendor = IdVendorMct) и (device.descriptor.idProduct = IdProductU232P9)) то (* use one-byte coded value ... *)
				если baudrate = 300 то divisor := 01H
				аесли baudrate = 600 то divisor := 02H;
				аесли baudrate = 1200 то divisor := 03H;
				аесли baudrate = 2400 то divisor := 04H;
				аесли baudrate = 4800 то divisor := 06H;
				аесли baudrate = 9600 то divisor := 08H;
				аесли baudrate = 19200 то divisor := 09H;
				аесли baudrate = 38400 то divisor := 0AH;
				аесли baudrate = 57600 то divisor := 0BH;
				аесли baudrate = 115200 то divisor := 0CH;
				иначе
					если Debug то ShowModule("SetBaudrate: Wrong baud rate selected."); ЛогЯдра.пВК_ПС; всё;
					возврат ложь;
				всё;

				data1[0] := симв8ИзКода(divisor);
				status := device.Request(Usbdi.ToDevice + Usbdi.Vendor + Usbdi.Device, MctSetBaudrate, 0, 0, 1, data1^);

			иначе (* standart UART way ... *)
				если (115200 остОтДеленияНа baudrate) # 0 то
					если Debug то ShowModule("SetBaudrate: Wrong baud rate selected."); ЛогЯдра.пВК_ПС; всё;
					возврат ложь;
				иначе
					divisor := 115200 DIV baudrate;
				всё;

				data4[0] := симв8ИзКода(divisor);
				data4[1] := симв8ИзКода(логСдвиг(divisor, -8));
				data4[2] := симв8ИзКода(логСдвиг(divisor, -16));
				data4[3] := симв8ИзКода(логСдвиг(divisor, -24));
				status := device.Request(Usbdi.ToDevice + Usbdi.Vendor + Usbdi.Device, MctSetBaudrate, 0, 0, 4, data4^);
			всё;

			если status # Usbdi.Ok то
				если Debug то ShowModule("SetBaudrate failed (MctSetBaudrate)"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			(* don't known what the following two vendor-specific requests are good for ... just imitating the windows driver *)
			data1[0] := симв8ИзКода(0);

			status := device.Request(Usbdi.ToDevice + Usbdi.Vendor + Usbdi.Device, MctSetUnknown1, 0, 0, 1, data1^);
			если status # Usbdi.Ok то
				если Debug то ShowModule("SetBaudrate failed (MctSetUnknown1). "); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			status := device.Request(Usbdi.ToDevice + Usbdi.Vendor + Usbdi.Device, MctSetUnknown2, 0, 0, 1, data1^);
			если status # Usbdi.Ok то
				если Debug то ShowModule("SetBaudrate failed (MctSetUnknown1). "); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			port.portbps := baudrate;
			возврат истина;
		кон SetBaudrate;

		(* Vendor-specific request: Used to set the Line Control Register (LCR) *)
		проц SetLCR(set : мнвоНаБитахМЗ) : булево;
		перем status : Usbdi.Status;
		нач
			data1[0] := НИЗКОУР.подмениТипЗначения(симв8, set);
			status := device.Request(Usbdi.ToDevice + Usbdi.Vendor + Usbdi.Device, MctSetLineCtrl, 0, 0, 1, data1^);
			если status # Usbdi.Ok то
				если Debug то ShowModule("SetLCR failed."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			иначе
				lcr := set;
				возврат истина;
			всё;
		кон SetLCR;

		(* vendor-specific request: Used to get the Line Control Register (LCR) *)
		проц GetLCR(перем lcr : мнвоНаБитахМЗ) : булево;
		перем status : Usbdi.Status;
		нач
			status := device.Request(Usbdi.ToHost + Usbdi.Vendor + Usbdi.Device, MctGetLineCtrl, 0, 0, 1, data1^);
			если status # Usbdi.Ok то
				если Debug то ShowModule("GetLCR failed."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			иначе
				lcr := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, data1[0]);
				возврат истина;
			всё;
		кон GetLCR;

		(* Vendor-specific request: used to set RTS & DTR Bits of the Modem Control Register (MCR) *)
		(* Updates DTR&RTS in mc *)
		проц SetMCR(set : мнвоНаБитахМЗ) : булево;
		перем status : Usbdi.Status;
		нач
			если Serials.DTR в set то включиВоМнвоНаБитах(mcr, MctMcrDtr); иначе исключиИзМнваНаБитах(mcr, MctMcrDtr); всё;
			если Serials.RTS в set то включиВоМнвоНаБитах(mcr, MctMcrRts); иначе исключиИзМнваНаБитах(mcr, MctMcrRts); всё;
			включиВоМнвоНаБитах(mcr, MctMcrOut2); (* Always enable Out2 *)

			data1[0] := симв8ИзКода(НИЗКОУР.подмениТипЗначения(цел32, mcr));

			status := device.Request(Usbdi.ToDevice + Usbdi.Vendor + Usbdi.Device, MctSetModemCtrl, 0, 0, 1, data1^);
			если status # Usbdi.Ok то
				если Debug то ShowModule("SetMCR failed: "); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			иначе
				если Serials.DTR в set то включиВоМнвоНаБитах(mc, Serials.DTR) иначе исключиИзМнваНаБитах(mc, Serials.DTR) всё;
				если Serials.RTS в set то включиВоМнвоНаБитах(mc, Serials.RTS) иначе исключиИзМнваНаБитах (mc, Serials.RTS) всё;
				возврат истина;
			всё;
		кон SetMCR;

		(* Updates the msr & mc variable *)
		проц GetMSR(перем value : мнвоНаБитахМЗ) : булево;
		перем status : Usbdi.Status;
		нач
			status := device.Request(Usbdi.ToHost + Usbdi.Vendor + Usbdi.Device, MctGetModemStatus, 0, 0, 1, data1^);
			если status # Usbdi.Ok то
				если Debug то ShowModule("GetMSR failed."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			иначе
				если Trace * TraceCommands # {} то ShowModule("GetMSR succeeded (value: "); ЛогЯдра.пМнвоНаБитахМЗКакБиты(НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, data1[0]), 0, 8); ЛогЯдра.пСтроку8(")");  ЛогЯдра.пВК_ПС; всё;
				msr := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, data1[0]);
				если MctMsrRi в msr то включиВоМнвоНаБитах(mc, Serials.RI); иначе исключиИзМнваНаБитах(mc, Serials.RI); всё;
				если MctMsrDsr в msr то включиВоМнвоНаБитах(mc, Serials.DSR); иначе исключиИзМнваНаБитах(mc, Serials.DSR); всё;
				если MctMsrCts в msr то включиВоМнвоНаБитах(mc, Serials.CTS); иначе исключиИзМнваНаБитах(mc, Serials.CTS);  всё;
				если MctMsrCd в msr то включиВоМнвоНаБитах(mc, Serials.DCD); иначе исключиИзМнваНаБитах(mc, Serials.DCD); всё;
				value := msr;
				возврат истина;
			всё;
		кон GetMSR;

		проц &Init*;
		нач
			нов(data1, 1); нов(data4, 4); нов(data64, 64); нов(status2, 2);
			msr := {}; lsr := {}; mcr := {}; lcr := {};
		кон Init;

	кон UsbRs232Driver;

тип

	Port = окласс(Serials.Port)
	перем
		driver : UsbRs232Driver;
		portbps : цел32;
		data1 : Usbdi.BufferPtr; (* datax -> ARRAY x OF CHAR *)
		buf: массив BufSize из симв8;
		head, tail: размерМЗ;
		open: булево;
		diagnostic: цел32;

		проц {перекрыта}Open*(bps, data, parity, stop : цел32; перем res: целМЗ);
		нач {единолично}
			если open то
				если Verbose то ShowModule(name); ЛогЯдра.пСтроку8(" already open"); ЛогЯдра.пВК_ПС; всё;
				res := Serials.PortInUse;
				возврат;
			всё;
			SetPortState(bps, data, parity, stop, res);
			если res = Serials.Ok то
				open := истина;
				head := 0; tail:= 0;
				если Verbose то ShowModule(name); ЛогЯдра.пСтроку8("opened"); ЛогЯдра.пВК_ПС; всё;
			всё
		кон Open;

		(** Send len characters from buf to output, starting at ofs. res is non-zero on error. *)
		проц {перекрыта}ЗапишиВПоток*(конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем status : Usbdi.Status; buffer: укль на массив из симв8;
		нач {единолично}
			если ~open то res := Serials.Closed; возврат; всё;
			если Trace * TraceSend # {} то ShowModule("Sending "); ЛогЯдра.пЦел64(len, 0); ЛогЯдра.пСтроку8(" bytes"); ЛогЯдра.пВК_ПС; всё;
			нов (buffer, len); копируйСтрокуДо0 (buf, buffer^);
			status := driver.bulkOut.Transfer(len, ofs, buffer^);
			если status = Usbdi.Ok то
				res := Serials.Ok;
				charactersSent := charactersSent + len;
			иначе
				res := Serials.TransportError;
				если Debug то ShowModule("Transmission failed, res: "); ЛогЯдра.пЦел64(status, 0); ЛогЯдра.пВК_ПС; всё;
			всё;
		кон ЗапишиВПоток;

		(** Send a single character to the UART. *)
		проц {перекрыта}SendChar*(ch: симв8; перем res : целМЗ);
		перем status : Usbdi.Status;
		нач {единолично}
			если ~open то res := Serials.Closed; возврат; всё;
			data1[0] := ch;
			если Trace * TraceSend # {} то ShowModule("Sending character ORD: "); ЛогЯдра.пЦел64(кодСимв8(data1[0]), 0); ЛогЯдра.пВК_ПС; всё;
			status := driver.bulkOut.Transfer(1, 0, data1^);
			если status # Usbdi.Ok то
				res := Serials.Ok;
				увел(charactersSent);
			иначе
				res := Serials.TransportError;
				если Debug то ShowModule("Transmission of character failed."); ЛогЯдра.пВК_ПС; всё;
			всё;
		кон SendChar;

		(** Wait for the next character is received in the input buffer. The buffer is fed by HandleData *)
		проц {перекрыта}ReceiveChar*(перем ch: симв8; перем res: целМЗ);
		нач {единолично}
			если ~open то res := Serials.Closed; возврат; всё;
			дождись(tail # head);
			если tail = -1 то
				res := Serials.Closed;
			иначе
				ch := buf[head]; head := (head+1) остОтДеленияНа BufSize;
				res := diagnostic
			всё
		кон ReceiveChar;

		(** On detecting an interupt request, transfer the characters from the UART buffer to the input buffer *)
		проц HandleData(data :  Usbdi.Buffer; actLen : размерМЗ);
		перем n, i : размерМЗ; 	ch : симв8;
		нач {единолично}
			charactersReceived := charactersReceived + actLen;
			i := 0;
			нц
				если i >= actLen то прервиЦикл; всё;
				ch := data[i];
				n := (tail + 1) остОтДеленияНа BufSize;
				если n # head то
					buf[tail] := ch; tail := n
				иначе
					если Debug то ЛогЯдра.пСтроку8("Port: HandleData: Buffer overflow detected."); ЛогЯдра.пВК_ПС; всё;
				всё;
				увел(i);
				diagnostic := НИЗКОУР.подмениТипЗначения(цел32, driver.diagnostics); (* includes Serials.OE, Serials.PE, Serials.FE & Serials.BI *)
			кц;
		кон HandleData;

		проц {перекрыта}Available*(): размерМЗ;
		нач {единолично}
			возврат (tail - head) остОтДеленияНа BufSize
		кон Available;

		проц SetPortState(bps, data, parity, stop : цел32; перем res: целМЗ);
		перем s : мнвоНаБитахМЗ;
		нач
			если (bps > 0) и (115200 остОтДеленияНа bps = 0) то

				если (data >= 5) и (data <= 8) и (parity >= Serials.ParNo) и (parity <= Serials.ParSpace) и (stop >= Serials.Stop1) и (stop <= Serials.Stop1dot5) то

					если ~driver.SetBaudrate(bps) то
						res := Serials.WrongBPS; возврат;
					всё;

					(* Prepare parameters destined to LCR data, stop, parity *)
					просей data из	(* word length *)
						   5: s := MctData5;
						| 6: s := MctData6;
						| 7: s := MctData7;
						| 8: s := MctData8;
					всё;

					просей parity из
						   Serials.ParNo: 		s := s + MctParityNone;
						| Serials.ParOdd: 	s := s + MctParityOdd;
						| Serials.ParEven: 	s := s + MctParityEven;
						| Serials.ParMark: 	s := s + MctParityMark;
						| Serials.ParSpace: 	s := s + MctParitySpace;
					всё;

					если (stop = Serials.Stop1dot5) и (data # 5) то res := Serials.WrongStop; возврат; всё;
					если stop # Serials.Stop1 то s := s + MctStop2;  всё;

					(* Finalize the LCR *)
					если ~driver.SetLCR(s) то
						res := Serials.WrongData;возврат;
					всё;

					(* Set DTR, RTS in the MCR *)
					s := {}; включиВоМнвоНаБитах(s, Serials.DTR); включиВоМнвоНаБитах(s, Serials.RTS);
					если ~driver.SetMCR(s) то
						res := Serials.WrongData; возврат;
					всё;
					res := Serials.Ok
				иначе
					res := Serials.WrongData (* bad data/parity/stop *)
				всё
			иначе
				res := Serials.WrongBPS (* bad BPS *)
			всё;
		кон SetPortState;

		(** Get the port state: speed, no. of data bits, parity, no. of stop bits *)
		проц {перекрыта}GetPortState*(перем openstat : булево; перем bps, data, parity, stop : цел32);
		перем set : мнвоНаБитахМЗ; res : булево;
		нач
			(* get parameters *)
			openstat := open;
			bps := portbps;

			res := driver.GetLCR(set);
			если set * {0, 1} = MctData8 то data := 8;
			аесли set * {0, 1} = MctData7 то data := 7;
			аесли set * {0, 1} = MctData6 то data := 6;
			иначе data := 5;
			всё;

			если set * MctStop2 # {} то
				если set * {0, 1} = MctData5 то stop := Serials.Stop1dot5; 	иначе stop := Serials.Stop2; всё;
			иначе
				stop := Serials.Stop1;
			всё;

			если set * {3..5} = MctParitySpace то parity := Serials.ParSpace;
			аесли set * {3..5} = MctParityMark то parity := Serials.ParMark;
			аесли set * {3..5} = MctParityEven то parity := Serials.ParEven;
			аесли set * {3..5} = MctParityOdd то parity := Serials.ParOdd;
			иначе parity := Serials.ParNo;
			всё;
			если Trace * TraceCommands # {} то
				ShowModule("GetPortState of port "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(":");
				если res то
					ЛогЯдра.пСтроку8(" State: "); если open то ЛогЯдра.пСтроку8("Open"); иначе ЛогЯдра.пСтроку8("Closed"); всё;
					ЛогЯдра.пСтроку8(" DataBits: "); ЛогЯдра.пЦел64(data, 0);
					ЛогЯдра.пСтроку8(" StopBits: "); если stop=3 то ЛогЯдра.пСтроку8("1.5"); иначе ЛогЯдра.пЦел64(stop, 0); всё;
					ЛогЯдра.пСтроку8(" Parity: ");
					просей parity из
						0 : ЛогЯдра.пСтроку8("None");
						|1 : ЛогЯдра.пСтроку8("Odd");
						|2 : ЛогЯдра.пСтроку8("Even");
						|3 : ЛогЯдра.пСтроку8("Mark");
						|4 : ЛогЯдра.пСтроку8("Space");
					иначе
						ЛогЯдра.пСтроку8("Unknown");
					всё;
					ЛогЯдра.пСтроку8(" Bps: "); ЛогЯдра.пЦел64(bps, 0); ЛогЯдра.пВК_ПС;
				иначе
					ЛогЯдра.пСтроку8("Status request failed."); ЛогЯдра.пВК_ПС;
				всё;
			всё;
		кон GetPortState;

		(** ClearMC - Clear the specified modem control lines.  s may contain DTR, RTS & Break. *)
		проц {перекрыта}ClearMC*(s: мнвоНаБитахМЗ);
		перем  temp : мнвоНаБитахМЗ; ignore : булево;
		нач {единолично}
			если s * {Serials.DTR, Serials.RTS} # {} то
				temp := driver.mcr;
				если s * {Serials.DTR} # {} то исключиИзМнваНаБитах(temp, Serials.DTR); всё;
				если s * {Serials.RTS} # {} то исключиИзМнваНаБитах(temp, Serials.RTS); всё;
				ignore := driver.SetMCR(temp);
			всё;
			если Serials.Break в s то
				ignore := driver.GetLCR(temp);
				исключиИзМнваНаБитах(temp, Serials.Break);
				ignore := driver.SetLCR(temp);
			всё;
		кон ClearMC;

		(** SetMC - Set the specified modem control lines.  s may contain DTR, RTS & Break. *)
		проц {перекрыта}SetMC*(s: мнвоНаБитахМЗ);
		перем ignore : булево;
		нач {единолично}
			если s * {Serials.DTR, Serials.RTS} # {} то ignore := driver.SetMCR(s * {Serials.DTR, Serials.RTS}); всё;
			если Serials.Break в s то
				ignore := driver.SetLCR({Serials.Break});
			всё;
		кон SetMC;

		(** GetMC - Return the state of the specified modem control lines. s contains the current state of DSR, CTS, RI, DCD & Break Interrupt. *)
		проц {перекрыта}GetMC*(перем s: мнвоНаБитахМЗ);
		нач {единолично}
			s := driver.msr; (* Inlcudes CTS, DSR, RI, CD *)
			если MctLsrBi в driver.lsr то включиВоМнвоНаБитах(s, Serials.Break); всё;
		кон GetMC;

		проц {перекрыта}Закрой*;
		перем timer : Kernel.Timer; counter : цел32;
		нач {единолично}
			если ~open то
				если Verbose то ShowModule(name); ЛогЯдра.пСтроку8(" not open"); ЛогЯдра.пВК_ПС; всё;
				возврат;
			иначе
				если ~(MctLsrTemt в driver.lsr) то (* wait for last byte to leave *)
					нов(timer); counter := 0;
					нцДо
						timer.Sleep(1);
						увел(counter);
					кцПри (MctLsrTemt в driver.lsr) или (counter>100); (* No remaining word in the FIFO or transmit shift register *)
				всё;
				tail := -1; (* Force a pending Receive to terminate in error. *)
				open := ложь;
				если Verbose то ShowModule(name); ЛогЯдра.пСтроку8(" closed"); ЛогЯдра.пВК_ПС всё;
			всё;
		кон Закрой;

	кон Port;

проц ShowModule(конст string : массив из симв8);
нач
	ЛогЯдра.пСтроку8(ModuleName); ЛогЯдра.пСтроку8(": "); ЛогЯдра.пСтроку8(string);
кон ShowModule;

проц Probe(dev : Usbdi.UsbDevice; id : Usbdi.InterfaceDescriptor) : Usbdi.Driver;
перем driver : UsbRs232Driver;
нач
	если ((dev.descriptor.idVendor = IdVendorMct) и (dev.descriptor.idProduct = IdProductU232P9)) то (* MCT U232-P9 *)
	аесли ((dev.descriptor.idVendor = IdVendorMct) и (dev.descriptor.idProduct = IdProductU232P25)) то (* MCT U232-P25 *)
	аесли ((dev.descriptor.idVendor = IdVendorMct) и (dev.descriptor.idProduct = IdProductDUH3SP)) то (* D-Link USB Bay *)
	аесли ((dev.descriptor.idVendor = IdVendorBelkin) и (dev.descriptor.idProduct = IdProductF5U109)) то (* Belkin F5U109 *)
	иначе (* device not supported *)
		возврат НУЛЬ;
	всё;
	нов(driver);
	возврат driver;
кон Probe;

проц Cleanup;
нач
	Usbdi.drivers.Remove(Name);
кон Cleanup;

проц Install*;
кон Install;

нач
	Modules.InstallTermHandler(Cleanup);
	Usbdi.drivers.Add(Probe, Name, Description, Priority)
кон UsbRS232.

UsbRS232.Install ~   System.Free UsbRS232 ~
