модуль IMAP; (** AUTHOR "retmeier"; PURPOSE "IMAP Client Library, implements a subset of RFC 2060"; *)

использует DNS, IP, Потоки, TCP, Strings, ЛогЯдра, Classes := TFClasses, IMAPUtilities;

конст
	DEBUG = ложь;
	DEBUGLEVEL = 1; (* 2 logs full parse list, not recommended :-) *)

	(** state machine constants *)
	DEAD* = -1; (** No Connection *)
	NOAUTH* = 0; (** Connected, but not logged in *)
	AUTH* = 1; (** Logged in *)
	SELECT* = 2; (** Mailbox selected *)

	(* entry structure type constants *)
	LITERAL* = 0;
	STRING* = 1;
	LIST* = 2;
	ATOM* = 3;
	HEADER* = 4;

	(** return values *)
	OK* = 0;
	BAD = 1;
	READBACKERR = 2;
	SENDERR = 3;

тип
	String = Strings.String;

	Entry* = укль на запись
		data-: String;
		list-: Classes.List;
		type-: цел32;
		command-: массив 40 из симв8;
		number-: цел32
	кон;

		(* An IMAP Response consists of one or more lines. Each line of an answer is represented with an Entry-Object with
		an attached List (also consisting of Entry-Objects of different Types like Atom, List, String or Literal, representing the
		IMAP return value in internalized version). The command field says of which type the answer is and how it has to be
		parsed. The last command field in the sequence of lines of the IMAP-Answer shows if the command succeeded
		(command = OK) or not (command # OK).. See also RFC 2060. After the syntactical analysis the typed list is further parsed
		semantically by the different procedures. *)


	(** Connection Object represents IMAP Connection/Session *)
	Connection* = окласс
		перем
			in: Потоки.Чтец;
			out: Потоки.Писарь;
			tag: цел32;
			state: цел32;
			tagString: массив 80 из симв8;
			buffer: массив 80 из симв8;
			connection : TCP.Connection;
			capability: Classes.List;
			ret: Classes.List;
			first: булево;
			logout: булево;

		(** connect to server, works in any state, changes to NOAUTH *)
		проц &Init* (перем host: массив из симв8; port: цел32; перем result: цел32);
		перем
			ip: IP.Adr;
			res: целМЗ;
			ret: Classes.List;
		нач
			logout := ложь;
			first := истина;
			state := DEAD;

			если DEBUG то ЛогЯдра.пСтроку8("WELCOME TO IMAP"); ЛогЯдра.пВК_ПС 	всё;
			DNS.HostByName(host, ip, res);
			если res = DNS.Ok то
				нов(connection);
				connection.Open(TCP.NilPort, ip, port, res);
				если res = TCP.Ok то
					если DEBUG то 	ЛогЯдра.пСтроку8("connected..."); ЛогЯдра.пВК_ПС; всё;
					(* one would assume that a rejected tcp connection would'nt set TCP.Ok... but it does ! *)
					Потоки.НастройЧтеца(in, connection.ПрочтиИзПотока);
					Потоки.НастройПисаря(out, connection.ЗапишиВПоток);
					state := NOAUTH;
					если ReadResponse(ret) то
						если CheckResultCode(ret) то
							result := OK;
						иначе
							если DEBUG то ЛогЯдра.пСтроку8("STATUS FAILURE"); ЛогЯдра.пВК_ПС всё;
							state := DEAD;
							result := BAD
						всё;
					иначе
						если DEBUG то ЛогЯдра.пСтроку8("CONNECT FAILURE OR BYE"); ЛогЯдра.пВК_ПС всё;
						state := DEAD;
						result := BAD
					всё
				всё;
			иначе
				если DEBUG то ЛогЯдра.пСтроку8("DNS FAILURE"); ЛогЯдра.пВК_ПС всё;
				state := DEAD;
				result := BAD
			всё
		кон Init;

		(* build a command with one argument *)
		проц MakeOneArgumentCommand(command: массив из симв8; перем argument: массив из симв8): String;
		перем
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
			arg, string: Strings.String;
		нач
			нов(buffer, 16);
			w := buffer.GetWriter();
			w.пСтроку8(command);
			w.пСтроку8(" ");
			arg := IMAPUtilities.NewString(argument);
			IMAPUtilities.MakeQuotedString(arg);
			w.пСтроку8(arg^);
			string := buffer.GetString();
			возврат string;
		кон MakeOneArgumentCommand;

		(* build a command with two arguments *)
		проц MakeTwoArgumentCommand(command: массив из симв8; перем argument1, argument2: массив из симв8): String;
		перем
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
			arg, string: String;
		нач
			нов(buffer, 16);
			w := buffer.GetWriter();
			w.пСтроку8(command);
			w.пСтроку8(" ");
			arg := IMAPUtilities.NewString(argument1);
			IMAPUtilities.MakeQuotedString(arg);
			w.пСтроку8(arg^);
			w.пСтроку8(" ");
			arg := IMAPUtilities.NewString(argument2);
			IMAPUtilities.MakeQuotedString(arg);
			w.пСтроку8(arg^);
			string := buffer.GetString();
			возврат string;
		кон MakeTwoArgumentCommand;

		(** login into server, works in NOAUTH, changes to AUTH *)
		проц Login*(username: массив из симв8; password: массив из симв8):цел32;
		перем
			string: Strings.String;
		нач {единолично}
			если state # NOAUTH то возврат BAD всё;
			string := MakeTwoArgumentCommand("LOGIN", username, password);
			возврат SendToIMAPServer(string^,ret, AUTH, NOAUTH )
		кон Login;

		(** Logout of remote system, changes state to DEAD *)
		проц Logout*():цел32;
		перем
			string: Strings.String;
			value: цел32;
		нач {единолично}
			logout := истина;
			string := Strings.NewString("LOGOUT");
			value := SendToIMAPServer(string^,ret, DEAD, DEAD);
			если connection # НУЛЬ то
				connection.Закрой;
				connection := НУЛЬ
			всё;
			возврат value
		кон Logout;

		(** get current state *)
		проц GetCurrentState*():цел32;
		нач {единолично}
			возврат state
		кон GetCurrentState;

		(** remove deleted mails, state stays in SELECT *)
		проц Expunge*(перем ret: Classes.List): цел32;
		нач {единолично}
			возврат SendToIMAPServer("EXPUNGE",ret,SELECT,SELECT)
		кон Expunge;

		(** create mailbox, works in AUTH and SELECT, state not changed *)
		проц Create*(mailbox: массив из симв8; перем ret: Classes.List): цел32;
		перем
			string: Strings.String;
		нач {единолично}
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			string := MakeOneArgumentCommand("CREATE", mailbox);
			возврат SendToIMAPServer(string^,ret,state,state)
		кон Create;

		(** delete mailbox, works in AUTH and SELECT, state not changed *)
		проц Delete*(mailbox: массив из симв8; перем ret: Classes.List): цел32;
		перем
			string: Strings.String;
		нач {единолично}
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			string := MakeOneArgumentCommand("DELETE", mailbox);
			возврат SendToIMAPServer(string^,ret,state,state)
		кон Delete;

		(** change name of mailbox, works in AUTH and SELECT, state not changed *)
		проц Rename*(from, to: массив из симв8; перем ret: Classes.List): цел32;
		перем
			string: Strings.String;
		нач {единолично}
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			string := MakeTwoArgumentCommand("RENAME",from, to);
			возврат SendToIMAPServer(string^,ret,state,state)
		кон Rename;

		(* send select command to server, changes to given mailbox *)
		проц Select*(mailbox:массив из симв8; перем ret: Classes.List):цел32;
		перем
			string: Strings.String;
		нач
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			string := MakeOneArgumentCommand("SELECT", mailbox);
			возврат SendToIMAPServer(string^,ret,SELECT, state)
		кон Select;

		(** examine mailbox, works in AUTH and SELECT, state not changed *)
		проц Examine*(mailbox: массив из симв8): цел32;
		перем
			string: Strings.String;
		нач {единолично}
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			string := MakeOneArgumentCommand("EXAMINE", mailbox);
			возврат SendToIMAPServer(string^, ret, SELECT, state)
		кон Examine;

		(** list the mailboxes, works in in AUTH and SELECT, state not changed *)
		проц List*(refName, mailbox: массив из симв8; перем ret: Classes.List):цел32;
		перем
			string: Strings.String;
		нач {единолично}
			если(state # AUTH) и (state # SELECT) то возврат BAD всё;
			string := MakeTwoArgumentCommand("LIST", refName, mailbox);
			возврат SendToIMAPServer(string^, ret, state, state)
		кон List;

		(** subscribe for mailbox, works in AUTH and SELECT, state not changed *)
		проц Subscribe*(mailbox: массив из симв8): цел32;
		перем
			string: Strings.String;
		нач {единолично}
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			string := MakeOneArgumentCommand("SUBSCRIBE", mailbox);
			возврат SendToIMAPServer(string^, ret, state, state)
		кон Subscribe;

		(** unsubscribe for mailbox, works in AUTH and SELECT, state not changed *)
		проц Unsubscribe*(mailbox: массив из симв8): цел32;
		перем
			string: Strings.String;
		нач {единолично}
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			string := MakeOneArgumentCommand("UNSUBSCRIBE", mailbox);
			возврат SendToIMAPServer(buffer, ret, state, state)
		кон Unsubscribe;

		(** appends a message to a mailbox. Works in AUTH and SELECT state *)
		проц Append*(перем mailbox, message: массив из симв8; перем ret: Classes.List): цел32;
		перем
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
			path, string: String;
			i: размерМЗ;
		нач {единолично}
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			нов(buffer, 16);
			w := buffer.GetWriter();
			w.пСтроку8("APPEND ");
			path := IMAPUtilities.NewString(mailbox);
			IMAPUtilities.MakeQuotedString(path);
			w.пСтроку8(path^);
			i := IMAPUtilities.StringLength(message);
			w.пСтроку8(" {");
			w.пЦел64(i,0);
			w.пСтроку8("}");
			string := buffer.GetString();
			возврат SendContinuedCommand(string^, message, ret, state, state);
		кон Append;

		(** copy specified in "what" to mailbox "to", works in SELECT, state not changed *)
		проц UIDCopy*(what, to: массив из симв8; перем ret: Classes.List): цел32;
		перем
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
			path,string: Strings.String;
		нач {единолично}
			если (state # SELECT) то возврат BAD всё;
			нов(buffer, 16);
			w := buffer.GetWriter();
			w.пСтроку8("UID COPY ");
			w.пСтроку8(what);
			w.пСтроку8(" ");
			path := IMAPUtilities.NewString(to);
			IMAPUtilities.MakeQuotedString(path);
			w.пСтроку8(path^);
			string := buffer.GetString();
			возврат SendToIMAPServer(string^,ret,state,state)
		кон UIDCopy;

		(** close current mailbox *)
		проц Close*():цел32;
		нач {единолично}
			копируйСтрокуДо0("CLOSE",buffer);
			возврат SendToIMAPServer("CLOSE",ret,AUTH,state)
		кон Close;

		(** noop operation, allow the server to send us status updates *)
		(** IMAP sends these from itself every few minutes, works in any state *)
		проц Noop*(перем ret: Classes.List):цел32;
		нач {единолично}
			возврат SendToIMAPServer("NOOP",ret,state,state)
		кон Noop;

		(* send status command to server *)
		проц Status*(перем mailbox, items: массив из симв8; перем ret: Classes.List):цел32;
		перем
			string: Strings.String;
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
		нач {единолично}
			если (state # AUTH) и (state # SELECT) то возврат BAD всё;
			нов(buffer, 16);
			w := buffer.GetWriter();
			string := MakeOneArgumentCommand("STATUS", mailbox);
			w.пСтроку8(string^);
			w.пСтроку8(" ");
			w.пСтроку8(items);
			string := buffer.GetString();
			возврат SendToIMAPServer(string^,ret,state,state)
		кон Status;

		(* Search after criteria, see RFC 2060 *)
		проц Search*(конст criteria: массив из симв8; перем ret: Classes.List): цел32;
		перем
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
			string: String;
		нач {единолично}
			если state # SELECT то возврат BAD всё;
			нов(buffer, 16);
			w := buffer.GetWriter();
			w.пСтроку8("SEARCH ");
			w.пСтроку8(criteria);
			string := buffer.GetString();
			возврат SendToIMAPServer(string^,ret,SELECT,state);
		кон Search;

		(* get information about a message from server. information is specified by "items". *)
		проц Fetch*(set: массив из симв8; items: массив из симв8;  перем ret: Classes.List):цел32;
		перем
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
			string: String;
		нач {единолично}
			если state # SELECT то возврат BAD всё;
			нов(buffer, 16);
			w := buffer.GetWriter();
			w.пСтроку8("FETCH ");
			w.пСтроку8(set);
			w.пСтроку8(" ");
			w.пСтроку8(items);
			string := buffer.GetString();
			возврат SendToIMAPServer(string^,ret,SELECT,SELECT)
		кон Fetch;

		проц UIDFetch*(set: массив из симв8; items: массив из симв8; перем ret:Classes.List): цел32;
		перем
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
			string: String;
		нач {единолично}
			если state # SELECT то возврат BAD всё;
			нов(buffer, 16);
			w := buffer.GetWriter();
			w.пСтроку8("UID FETCH ");
			w.пСтроку8(set);
			w.пСтроку8(" ");
			w.пСтроку8(items);
			string := buffer.GetString();
			возврат SendToIMAPServer(string^, ret, SELECT, state);
		кон UIDFetch;

		(* store a specified flag on the server *)
		проц UIDStore*(set: массив из симв8; flags: массив из симв8; plus: булево; перем ret: Classes.List):цел32;
		перем
			buffer: Strings.Buffer;
			w: Потоки.Писарь;
			string: String;
		нач {единолично}
			если state # SELECT то возврат BAD всё;
			нов(buffer, 16);
			w := buffer.GetWriter();
			w.пСтроку8("UID STORE ");
			w.пСтроку8(set);
			если plus то
				w.пСтроку8(" +FLAGS (");
			иначе
				w.пСтроку8(" -FLAGS (");
			всё;
			w.пСтроку8(flags);
			w.пСтроку8(")");
			string := buffer.GetString();
			возврат SendToIMAPServer(string^,ret,SELECT,state)
		кон UIDStore;

		(* parsing procedures *)

		(* append tag to command string and send it to the sender *)
		проц SendIMAPCommand(command: массив из симв8): булево;
		перем buffer: массив 10 из симв8;
		нач
			увел(tag);
			tagString := "AOS";
			Strings.IntToStr(tag,buffer);
			Strings.Append(tagString,buffer);
			out.пСтроку8(tagString);out.пСтроку8(" "); (* construct IMAP Tag and send it out *)
			out.пСтроку8(command);out.пВК_ПС(); (* send command *)
			out.ПротолкниБуферВПоток();
			если DEBUG то
				ЛогЯдра.пСтроку8("IMAP: sending to server: <"); ЛогЯдра.пСтроку8(tagString);
				ЛогЯдра.пСтроку8(" "); ЛогЯдра.пСтроку8(command); ЛогЯдра.пСтроку8(">"); ЛогЯдра.пВК_ПС
			всё;
			возврат out.кодВозвратаПоследнейОперации = Потоки.Успех;
		кон SendIMAPCommand;

		(* Checks if result code sent by IMAP Server is "OK" *)
		проц CheckResultCode(list: Classes.List):булево;
		перем ent: Entry; entP:динамическиТипизированныйУкль;
		нач
			entP := list.GetItem(list.GetCount()-1);ent := entP(Entry);
			возврат ent.command = "OK";
		кон CheckResultCode;

		проц SendContinuedCommand(перем command, continuation: массив из симв8; перем ret: Classes.List; newstate, failstate: цел32): цел32;
		нач
			если state = DEAD то возврат BAD всё;
			если ~SendIMAPCommand(command) то
				возврат SENDERR;
			всё;
			если ~ReadResponse(ret) то
				возврат READBACKERR;
			всё;
			если ~SendContinuation(continuation) то
				возврат SENDERR;
			всё;
			если ~ReadResponse(ret) то
				возврат READBACKERR;
			всё;
			если CheckResultCode(ret) то
				state := newstate;
				возврат OK;
			всё;
			state := failstate;
			возврат BAD;
		кон SendContinuedCommand;

		проц SendContinuation(перем continuation: массив из симв8): булево;
		нач
			out.пСтроку8(continuation);
			out.пВК_ПС();
			out.ПротолкниБуферВПоток();
			возврат out.кодВозвратаПоследнейОперации = Потоки.Успех;
		кон SendContinuation;

		(* internal send command, send command to server and syntactivally parse the return value of the server, generate
		the parsed Entry-List (see at the top). *)
		проц SendToIMAPServer(command: массив из симв8; перем ret: Classes.List; newstate,failstate: цел32):цел32;
		нач
			если state = DEAD то возврат BAD всё; (* nothing to do without connection *)
			если SendIMAPCommand(command) то (* send command to server *)
				если ReadResponse(ret) то (* get and parse answer *)
					если CheckResultCode(ret) то (* is return state = OK? -> go to new state *)
						state := newstate;
						если DEBUG то ЛогЯдра.пСтроку8(" SUCCESS! state: "); ЛогЯдра.пЦел64(state,4); ЛогЯдра.пВК_ПС;
							если DEBUGLEVEL > 1 то DBGList(ret) всё;
						всё;
						возврат OK; (* OK *)
					иначе
						state := failstate; (* not ok -> fail state *)
						если DEBUG то ЛогЯдра.пСтроку8(" FAILED GetResultCode! state: "); ЛогЯдра.пЦел64(state,4); ЛогЯдра.пВК_ПС; всё;
						возврат BAD
					всё;
				иначе (* readback failed *)
					state := failstate;
					если DEBUG то ЛогЯдра.пСтроку8(" FAILED ReadBack! state: "); ЛогЯдра.пЦел64(state,4); ЛогЯдра.пВК_ПС; всё;
					возврат READBACKERR
				всё;
			иначе
				state := failstate;
				если DEBUG то ЛогЯдра.пСтроку8(" FAILED Send state: "); ЛогЯдра.пЦел64(state,4); ЛогЯдра.пВК_ПС; всё;
				возврат SENDERR; (* send failed *)
			всё
		кон SendToIMAPServer;

		(* Read response from IMAP Server and parse it *)
		проц ReadResponse(перем ret: Classes.List): булево;
		перем
			buffer, tag: String;
			i: цел32;
		нач
			(* Syntax of answer is tag-or-star SP [ number ]  status [SP "["response code"]"] SP text *)
			если state = DEAD то возврат ложь всё; (* perhaps, the server killed the connection... *)
			нов(ret);
			нцДо
				in.ПропустиБелоеПоле();
				если in.кодВозвратаПоследнейОперации # Потоки.Успех то
					если DEBUG то ЛогЯдра.пСтроку8("IMAP: ReadResponse: Read failed at SkipWhiteSpace") всё;
					возврат ложь
				всё;
		        	если ~ ReadToken(tag) то
		        		если DEBUG то ЛогЯдра.пСтроку8("IMAP: ReadResponse: Read failed at ReadToken") всё;
		        		возврат ложь;
		        	всё;
		        	если DEBUG то ЛогЯдра.пСтроку8("tag is: "); ЛогЯдра.пСтроку8(tag^); ЛогЯдра.пВК_ПС всё;
				если (tag^ # "+") то
					если ~ ReadUToken(buffer) то
						если DEBUG то ЛогЯдра.пСтроку8("IMAP: ReadResponse: Read failed at ReadUtoken") всё;
						возврат ложь;
					всё;
					(* got number in buffer ?*)
					если (buffer[0] >="0") и (buffer[0] <= "9") то
						Strings.StrToInt(buffer^,i);
						если ~ ReadUToken(buffer) то (* get command *)
							если DEBUG то ЛогЯдра.пСтроку8("IMAP: ReadResponse: Read failed at ReadUtoken2") всё;
							возврат ложь;
						всё;
						если ~ Parse(buffer^,i,истина,ret) то
							если DEBUG то ЛогЯдра.пСтроку8("IMAP: ReadResponse: Parse failed 1") всё;
							возврат ложь;
						всё;
					иначе
						 если ~ Parse(buffer^,-1,ложь,ret) то возврат ложь всё
					всё;
				иначе
					(* command continuation request response *)
					in.ПропустиДоКонцаСтрокиТекстаВключительно();

					возврат истина;
				всё;
			кцПри (tag^ = tagString) или ((tagString = "") и (tag^ = "*") и (buffer^="OK")) или (state = DEAD);
			(* IMAP ends a command with the tag except the welcome string ( * OK ) *)
			возврат истина;
		кон ReadResponse;

		проц Parse(command: массив из симв8; num:цел32; numflag:булево;
		перем ret:Classes.List):булево;
		(* result of command is given back by ret, all unsoliticed messages received while parsing answers to
		command are handled as SIDE EFFECT *)
		перем
				dummy: булево;
				i: цел32;
				list: Classes.List;
				header, ent: Entry;
				content: Classes.List;
				ent2, ent3: Entry;
		нач
			если DEBUG то ЛогЯдра.пСтроку8("IMAP: Parse: command is "); ЛогЯдра.пСтроку8(command); ЛогЯдра.пВК_ПС всё;
			i := 0;
			нов(header);
			если numflag то
				header.number := num
			всё;
			header.type := HEADER;
			header.data := НУЛЬ;
			header.list := НУЛЬ;
			копируйСтрокуДо0(command,header.command);
			ret.Add(header);
			если ~ReadResponseCode() то возврат ложь всё;

			если command = "CAPABILITY" то (* IMAP Server sends it's capability string, save for later use *)
				нцПока ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() делай
					нов(ent);
					если ~ReadAtom(ent.data) то возврат ложь всё;
					если DEBUG то ЛогЯдра.пСтроку8("IMAP: Parse: Capability: "); ЛогЯдра.пСтроку8(ent.data^); ЛогЯдра.пВК_ПС всё;
					in.ПропустиБайты(1);
					ent.type := ATOM;
					ret.Add(ent);
					capability := ret
				кц;

			аесли command = "EXISTS" то (* IMAP Server sends how many messages there are, also save *)
				возврат истина; (* no need to skip white spaces, we're at the end of the line anyway *)

			аесли command = "EXPUNGE" то возврат истина;

			аесли command = "FETCH" то
				нов(list);
				если ~ReadList(list) то возврат ложь всё;
				header.list := list;

			аесли command = "FLAGS" то
				нов(list);
				если ~ReadList(list) то возврат ложь всё;
				header.list := list;

			аесли command = "RECENT" то
				возврат истина; (* already at the end of the line -> return *)

			аесли command = "STATUS" то
				если in.ПодглядиСимв8() = 22X то (* " *)
					если ~ReadQuotedString(header.data) то возврат ложь; всё;
				иначе
					если ~ReadAtom(header.data) то возврат ложь; всё;
				всё;
				нов(list);
				если ~ReadList(list) то возврат ложь всё;
				header.list := list;
				если DEBUG то DBGList(list) всё;

			аесли (command = "SORT") или (command = "SEARCH") то
				нов(list);
					если in.ПодглядиСимв8() # 0DX то
						если ~ReadNumberEnumeration(list) то СТОП(333); возврат ложь всё;
					всё;
				header.list := list;

			аесли command = "BAD" то
				если DEBUG то ЛогЯдра.пСтроку8("IMAP: server said: BAD"); ЛогЯдра.пВК_ПС всё;
				dummy := NextLine(); (* skip to next line whenever possible *)
				возврат ложь;

			аесли command = "BYE" то
				если ~logout то
					(* kill connection immediatly *)
					state := DEAD;
					если DEBUG то ЛогЯдра.пСтроку8("IMAP: kicked out by server"); ЛогЯдра.пВК_ПС всё;
					connection.Закрой; connection := НУЛЬ;
					возврат ложь; (* server has closed connection now, so do we... *)

				всё;

			аесли (command = "LIST")  то
				нов(list);
				если ~ReadList(list) то возврат ложь всё;
				нов(ent);
				ent.type := LIST;
				ent.list := list;
				нов(content);
				content.Add(ent);

				in.ПропустиБелоеПоле();
				нов(ent2);
				если in.ПодглядиСимв8() = 22X то
					если ~ReadQuotedString(ent2.data) то возврат ложь всё;
				аесли in.ПодглядиСимв8() = "{" то
					если ~ReadLiteral(ent2.data) то возврат ложь всё;
				иначе
					если ~ReadAtom(ent2.data) то возврат ложь всё;
				всё;
				content.Add(ent2);

				in.ПропустиБелоеПоле();
				нов(ent3);
				если in.ПодглядиСимв8() = 22X то
					если ~ReadQuotedString(ent3.data) то возврат ложь всё;
				аесли in.ПодглядиСимв8() = "{" то
					если ~ReadLiteral(ent3.data) то возврат ложь всё;
				иначе
					если ~ReadAtom(ent3.data) то возврат ложь всё;
				всё;
				content.Add(ent3);


				header.list := content;
				header.type := LIST;
				если DEBUG то DBGList(content) всё;

			аесли (command = "LSUB")   то
				ЛогЯдра.пСтроку8(command);ЛогЯдра.пСтроку8(": Not yet implemented");ЛогЯдра.пВК_ПС;
				СТОП(999);

			аесли (command = "PREAUTH") то
				state := AUTH;

			аесли (command = "OK") то
			(* no need to do anything... *)

			аесли (command = "NO") то
				если DEBUG то ЛогЯдра.пСтроку8("IMAP: server said: NO"); ЛогЯдра.пВК_ПС всё;
				возврат ложь;

			иначе
				ЛогЯдра.пСтроку8("IMAP: unknown keyword <<"); ЛогЯдра.пСтроку8(command);
				ЛогЯдра.пСтроку8(">>. This is a IMAP parser error..."); ЛогЯдра.пВК_ПС;
				возврат ложь
			всё;

			(* go ahead to next line, skip garbage *)
			если ~NextLine() то возврат ложь всё;
			возврат истина; (* everything is ok *)
		кон Parse;

		(* read text in [] and parse it's content *)
		проц ReadResponseCode():булево;
		перем
			command,argument: String;
			argi: цел32;
			list,ret: Classes.List;
			ent: Entry;
		нач
			in.ПропустиПробелыИТабуляции();
			если in.кодВозвратаПоследнейОперации # Потоки.Успех то
				если DEBUG то 	ЛогЯдра.пСтроку8("IMAP: ReadResponseCode failed"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь всё;
			если in.ПодглядиСимв8() # "[" то
				(* no response code *)
					если DEBUG то ЛогЯдра.пСтроку8("No response code available");ЛогЯдра.пВК_ПС; всё;
					возврат истина;
			иначе
				in.ПропустиБайты(1);
				если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;
				если ~ReadPToken(command) то
					если DEBUG то ЛогЯдра.пСтроку8("IMAP: ReadResponseCode: ReadPToken failed "); ЛогЯдра.пВК_ПС; всё;
					возврат ложь
				всё;
				утв(command#НУЛЬ,1011);
				in.ПропустиБелоеПоле();
				если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;

				если command^ = "ALERT" то
					(* not supported and skipped *)
					in.ПропустиБайты(1);(* ] *)
					in.ПропустиБелоеПоле();
					если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;
					если ~ReadText(argument) то возврат ложь всё;
					(* alert message is in argument *)

				аесли command^ = "NEWNAME" то
					(* not supported, will just skip text... *)
					in.ПропустиДоКонцаСтрокиТекстаВключительно();

				аесли command^ = "PARSE" то
					(* skipped *)
					in.ПропустиБайты(1);(* ] *)
					in.ПропустиБелоеПоле();
					если ~ReadText(argument) то возврат ложь всё;

				аесли command^ = "PERMANENTFLAGS" то
					(* store in folder structure *)
					in.ПропустиБелоеПоле();
					нов(list);
					если ~ReadList(list) то возврат ложь всё;

				аесли command^ = "READ-ONLY" то

				аесли command^ = "READ-WRITE" то

				аесли command^ = "TRYCREATE" то
					(* informational only, just ignore :-*)
					in.ПропустиДоКонцаСтрокиТекстаВключительно();

				аесли command^ = "UIDVALIDITY" то
					если ~ReadPToken(argument) то возврат ложь всё;
					Strings.StrToInt(argument^,argi);

				аесли command^ = "UIDNEXT" то
					если ~ReadPToken(argument) то возврат ложь всё;
					Strings.StrToInt(argument^,argi);

				аесли command^ = "UNSEEN" то
					если ~ReadPToken(argument) то возврат ложь всё;
					Strings.StrToInt(argument^,argi);

				аесли command^ = "CAPABILITY" то
					нов(ret);
					нцПока (in.ПодглядиСимв8() # "]") делай
						нов(ent);
						если ~ReadPToken(ent.data) то возврат ложь всё;
						ent.type := ATOM;
						ret.Add(ent)
					кц;
				аесли command^ = "COPYUID" то
					(* ignore *)
					нцПока (in.чИДайСимв8() # "]" ) делай
					кц;
				аесли command^ = "APPENDUID" то
					(* ignore *)
					нцПока (in.чИДайСимв8() # "]" ) делай
					кц;

				иначе
					если DEBUG то
						ЛогЯдра.пСтроку8("IMAP: ReadResponseCode: unknown response code: ");
						ЛогЯдра.пСтроку8("->");ЛогЯдра.пСтроку8(command^);
						ЛогЯдра.пСтроку8("<-");ЛогЯдра.пВК_ПС
					всё;
				всё
			всё;
			возврат истина
		кон ReadResponseCode;

		(* skip to next line *)
		проц NextLine():булево;
		нач
				in.ПропустиДоКонцаСтрокиТекстаВключительно();
				возврат in.кодВозвратаПоследнейОперации = Потоки.Успех;
		кон NextLine;

		(* read text until end of line reached *)
		проц ReadText(перем text: String):булево;
		перем
			b: Strings.Buffer;
			w: Потоки.Писарь;
			c: симв8;
		нач
			нов(b, 16);
			w := b.GetWriter();
			нцПока ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() делай (* read until end of line *)
				c := in.чИДайСимв8();
				если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;
				если c ="\" то (* escaped char *)
					c := in.чИДайСимв8();
					если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё
				всё;
				w.пСимв8(c);
			кц;
			text := b.GetString();
			возврат истина
		кон ReadText;

		проц ReadNumberEnumeration(перем list: Classes.List):булево;
		перем
			ent: Entry;
			size: цел32;
		нач
			нов(ent);
			size := 0;
			in.ПропустиБелоеПоле();
			нцПока in.ПодглядиСимв8() # 0DX делай
				если ~ReadAtom(ent.data) то ЛогЯдра.пСтроку8("RNE failed"); возврат ложь; всё;
				утв(ent.data # НУЛЬ, 999);
				list.Add(ent);
				нов(ent);
				увел(size);
				утв(size < 2500000, 666);
				если in.ПодглядиСимв8() # 0DX то
					in.ПропустиБелоеПоле();
				всё;
			кц;
			возврат size #  0;
		кон ReadNumberEnumeration;

		(* read IMAP formatted list *)
		проц ReadList(перем list: Classes.List):булево;
		перем
			ent: Entry;
			nlist: Classes.List;
		нач
			in.ПропустиПробелыИТабуляции();
			если ~(in.кодВозвратаПоследнейОперации=Потоки.Успех) то возврат ложь всё;
			если in.ПодглядиСимв8() # "(" то возврат ложь всё; (* list starts with ( *)
			in.ПропустиБайты(1);
			если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;
			утв( in.ПодглядиСимв8() # 0DX,1012);

			нцПока in.ПодглядиСимв8() # ")" делай (* list ends with ) *)
				нов(ent);
				in.ПропустиПробелыИТабуляции();
				если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;

				если in.ПодглядиСимв8() = "{" то
					если ~ReadLiteral(ent.data) то возврат ложь всё;
					ent.type :=  LITERAL;
					ent.list := НУЛЬ;
				аесли in.ПодглядиСимв8() = 22X то (* " *)
					если ~ReadQuotedString(ent.data) то возврат ложь всё;
					ent.type := STRING;
					ent.list := НУЛЬ;
				аесли in.ПодглядиСимв8() = "(" то
					нов(nlist);
					если ~ReadList(nlist) то возврат ложь всё;
					ent.list := nlist;
					ent.data := НУЛЬ;
					ent.type := LIST;
				аесли in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() то
					возврат ложь;
				иначе
					если ~ReadAtom(ent.data) то возврат ложь всё;
					ent.type := ATOM;
					ent.list := НУЛЬ
				всё;
				(* now either " " or ) OR eoln.  *)
				если in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() то возврат ложь всё;
				list.Add(ent);
				если ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() то
					in.ПропустиПробелыИТабуляции();
					если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;
				всё;
			кц;
			если ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() и (in.ПодглядиСимв8() = ")") то in.ПропустиБайты(1) всё;
			возврат in.кодВозвратаПоследнейОперации = Потоки.Успех;
		кон ReadList;

		(* read IMAP Literal: zero or more bytes prepended with size in {} *)
		проц ReadLiteral(перем buffer:String): булево;
		перем
			data: массив 80 из симв8;
			i,size: цел32; len: размерМЗ;
		нач
			i := 0;
			утв(in.ПодглядиСимв8() = "{",1013);
			in.ПропустиБайты(1);
			нцПока ((in.ПодглядиСимв8() # "}") и ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() и (i<256)) делай
				data[i]:= in.чИДайСимв8();
				увел(i);
			кц;
			если (in.ПодглядиСимв8() # "}") то возврат ложь всё;
			Strings.StrToInt(data,size);
			in.ПропустиДоКонцаСтрокиТекстаВключительно();
			если ~(in.кодВозвратаПоследнейОперации=Потоки.Успех) то возврат ложь всё;
			нов(buffer,size+1);
			in.чБайты(buffer^,0,size,len);
			если ~(in.кодВозвратаПоследнейОперации=Потоки.Успех) то возврат ложь всё;
			если size=len то
				buffer^[size] := 0X;
				возврат истина;
			иначе
				возврат ложь;
			всё;
		кон ReadLiteral;

		(* read IMAP quoted string: sequence of zero or more 7bit chars, " at each end *)
		проц ReadQuotedString(перем buffer:String): булево;
		(* Read Quoted String *)
		перем
			b: Strings.Buffer;
			w: Потоки.Писарь;
			c: симв8;
		нач
			нов(b, 16);
			w := b.GetWriter();
			утв(in.ПодглядиСимв8() = 22X,1014);
			in.ПропустиБайты(1);
			нцПока ((in.ПодглядиСимв8() # 22X) и ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿()) делай
				c := in.чИДайСимв8();
				если ~(in.кодВозвратаПоследнейОперации=Потоки.Успех) то возврат ложь всё;
				если c="\" то (* escaped char *)
					c := in.чИДайСимв8();
					если ~(in.кодВозвратаПоследнейОперации=Потоки.Успех) то возврат ложь всё;
				всё;
				w.пСимв8(c);

			кц;
			in.ПропустиБайты(1);
			если ~(in.кодВозвратаПоследнейОперации=Потоки.Успех) то возврат ложь всё;
			buffer := b.GetString();
			возврат истина;
		кон ReadQuotedString;

		(* read IMAP atom, zero or more nonspecial characters see RFC2060 *)
		проц ReadAtom(перем buffer:String): булево;
		перем
			b: Strings.Buffer;
			w: Потоки.Писарь;
			end: булево;
		нач
			нов(b, 16);
			w := b.GetWriter();
			если in.ПодглядиСимв8() ="\" то возврат ReadFlag(buffer) всё; (* if it starts with an \, it must be a flag... *)
			утв (in.ПодглядиСимв8() # 0DX,1015);
			end := ложь;
			нцПока ((in.ПодглядиСимв8() # " ") и ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() и (in.ПодглядиСимв8() # "(") и (in.ПодглядиСимв8() # ")") и (in.ПодглядиСимв8() # "{") и (in.ПодглядиСимв8() # "*")
						и (in.ПодглядиСимв8() # "%") и (in.ПодглядиСимв8() # "\")) делай
				утв (in.ПодглядиСимв8() # 0DX,1017);
				w.пСимв8(in.чИДайСимв8());
				если ~(in.кодВозвратаПоследнейОперации=Потоки.Успех) то возврат ложь всё;
			кц;
			buffer := b.GetString();
			возврат истина;
		кон ReadAtom;

		проц ReadFlag(перем buffer:String): булево;
		(* same for flag *)
		перем
			b: Strings.Buffer;
			w: Потоки.Писарь;
			i: цел32;
			end: булево;
		нач
			нов(b, 16);
			w := b.GetWriter();
			утв (in.ПодглядиСимв8() # 0DX,1018);
			end := ложь;
			нцПока ((in.ПодглядиСимв8() # " ") и ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() и (in.ПодглядиСимв8() # "(") и (in.ПодглядиСимв8() # ")") и (in.ПодглядиСимв8() # "{")
						и (in.ПодглядиСимв8() # "%") и ~ end) делай (* stop chars !!! *)  (*(Streams.Peek(in) # "\") *)

				утв (in.ПодглядиСимв8() # 0DX,1020);
				если ((in.ПодглядиСимв8() = "\") и (i>1)) то end := истина всё; (* hack for \atom in flags *)
				w.пСимв8(in.чИДайСимв8());
				если ~(in.кодВозвратаПоследнейОперации=Потоки.Успех) то возврат ложь всё;
				увел(i);
			кц;
			buffer := b.GetString();
			Strings.UpperCase(buffer^);
			возврат истина;
		кон ReadFlag;

		(* read token, ended by space *)
		проц ReadToken(перем token: String):булево;
		перем
			b: Strings.Buffer;
			w: Потоки.Писарь;
			c: симв8;
			i: цел32;
		нач
			нов(b,16);
			w := b.GetWriter();
			i := 0;
			in.ПропустиБелоеПоле();
			нцПока ((in.ПодглядиСимв8() # " ") и (~ in.ВКонцеСтрокиТекстаИлиФайлаЛи¿())) делай
				утв (in.ПодглядиСимв8() # 0DX,1021);
				w.пСимв8(in.чИДайСимв8());
				увел(i);
			кц;
			если ~ in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() то
				c := in.чИДайСимв8(); (* overread space (ignored) *)
			всё;
			token := b.GetString();
			возврат i # 0;
		кон ReadToken;

		(* Read token and convert to uppercase *)
		проц ReadUToken(перем resp: String):булево;
		нач
			если ReadToken(resp) то
				IMAPUtilities.UpperCase(resp^);
				возврат истина;
			иначе
				возврат ложь;
			всё
		кон ReadUToken;

		(* read token in [] *)
		проц ReadPToken(перем buffer: String):булево;
		перем
			b: Strings.Buffer;
			w: Потоки.Писарь;
		нач
			нов(b, 16);
			w := b.GetWriter();

			утв (in.ПодглядиСимв8() # 0DX,1022);
			in.ПропустиБелоеПоле();
			если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;
			нцПока ((in.ПодглядиСимв8() # " ") и ~in.ВКонцеСтрокиТекстаИлиФайлаЛи¿() и (in.ПодглядиСимв8() # "]")) делай

				утв (in.ПодглядиСимв8() # 0DX,1023);
				w.пСимв8(in.чИДайСимв8());
				если in.кодВозвратаПоследнейОперации # Потоки.Успех то возврат ложь всё;
			кц;
			buffer := b.GetString();
			возврат истина;
		кон ReadPToken;

		(* DEBUG Procedures *)

		(* print human readable version of parsed list *)
		проц DBGList*(перем listP: Classes.List);
		перем
			ent: Entry;
			entP: динамическиТипизированныйУкль;
			text: String;
			i: размерМЗ;
			list: Classes.List;
		нач
			утв(listP # НУЛЬ,1024);
			list := listP(Classes.List);
			ЛогЯдра.пСтроку8("-> processing list:"); ЛогЯдра.пВК_ПС;
			нцДля i := 0 до list.GetCount()-1 делай
				entP := list.GetItem(i);ent := entP(Entry);
				если ent.type=LITERAL то
					text := ent.data;
					DBGLiteral(ent.data);
				аесли ent.type=STRING то
					DBGString(ent.data);
				аесли ent.type=ATOM то
					DBGAtom(ent.data);
				аесли ent.type=LIST то
					DBGList(ent.list);
				аесли ent.type=HEADER то
					ЛогЯдра.пСтроку8("HEADER");ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8(ent.command);ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("Number");
					ЛогЯдра.пЦел64(ent.number,5);ЛогЯдра.пВК_ПС;
					если ent.list # НУЛЬ то
						DBGList(ent.list);
					всё;
				иначе
					СТОП(1028);
				всё;
			кц;
			ЛогЯдра.пСтроку8("<- processing list finished:"); ЛогЯдра.пВК_ПС;
		кон DBGList;

		проц DBGLiteral(перем text:String);
		перем
			i,j: размерМЗ;
		нач
			ЛогЯдра.пСтроку8("processing Literal:");ЛогЯдра.пВК_ПС;
			j := IMAPUtilities.StringLength(text^);
			нцДля i := 0 до j-1 делай
				ЛогЯдра.пСимв8(text^[i]);
			кц;
			ЛогЯдра.пВК_ПС;
		кон DBGLiteral;

		проц DBGString(перем text:String);
		нач
			ЛогЯдра.пСтроку8("processing String:");ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8(text^);ЛогЯдра.пВК_ПС;
		кон DBGString;

		проц DBGAtom(перем text:String);
		нач
			ЛогЯдра.пСтроку8("processing Atom:");ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8(text^);ЛогЯдра.пВК_ПС;
		кон DBGAtom;


	кон Connection;


	(* To make IMAP Mail-Compatible, one has to implement following procedure

	PROCEDURE GetMailMessage(message: SIGNED32):Mail.Message;

	one could implement this for full RFC2060 conformance:

	PROCEDURE Authenticate();
	PROCEDURE StartTLS();

	*)

кон IMAP.
