
модуль SSHKeys;  (* g.f.	2002.08.29 *)

(*	2002.10.15	g.f.	RSA hostkeys added*)

использует
	RSA := CryptoRSA, DSA := CryptoDSA, B:= CryptoBigNumbers, U := CryptoUtils, SHA1 := CryptoSHA1,
	MD5 := CryptoMD5, P := CryptoPrimes, G := SSHGlobals,
	Files, Потоки, Out := ЛогЯдра, WMDialogs, Strings;

конст
	Ok = WMDialogs.ResOk;
	Abort = WMDialogs.ResAbort;

тип
	BigNumber = B.BigNumber;



	проц SSHDSSVerify( dsa: DSA.Key;  sig: DSA.Signature;  конст hash: массив из симв8 ): булево;
	перем h: SHA1.Hash;  digest: массив 20 из симв8;
	нач
		нов( h );
		h.Initialize;  h.Update( hash, 0, 20 );  h.GetHash( digest, 0 );
		возврат dsa.Verify( digest, 20, sig )
	кон SSHDSSVerify;

	проц SSHRSAVerify( rsa: RSA.Key;  sig: BigNumber;  конст hash: массив из симв8 ): булево;
	перем h1: SHA1.Hash;  h2: MD5.Hash;  digest: массив 20 из симв8;
	нач
		нов( h1 );
		h1.Initialize;  h1.Update( hash, 0, 20 );  h1.GetHash( digest, 0 );
		если rsa.Verify( digest, 20, sig ) то   возврат истина
		иначе
			(* SSH_BUG_RSASIGMD5 in server implementation ? *)
			нов( h2 );
			h2.Initialize;  h2.Update( hash, 0, 20 );  h2.GetHash( digest, 0 );
			возврат rsa.Verify( digest, 16, sig )
		всё
	кон SSHRSAVerify;



	проц GetKeyStart( конст keykind, host: массив из симв8 ): Files.Reader;
	перем
		f: Files.File; r: Files.Reader;
		buf1, buf2: массив 512 из симв8;
		i: цел32;
		names: Strings.StringArray;
	нач
		f := Files.Old( G.HostkeysFile );
		если f = НУЛЬ то  возврат НУЛЬ
		иначе
			Files.OpenReader( r, f, 0 );
			нцДо
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( buf1 );
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( buf2 );
				если buf2 = keykind то
					names := Strings.Split( buf1, ',' );
					i := 0;
					нцДо
						если names[i]^ = host то  возврат r  всё;
						увел( i )
					кцПри i >= длинаМассива( names^ )
				всё;
				r.ПропустиДоКонцаСтрокиТекстаВключительно;
			кцПри r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0;
			возврат НУЛЬ
		всё;
	кон GetKeyStart;


	проц WriterAtEnd(): Потоки.Писарь;
	перем
		f: Files.File;  w: Files.Writer;  r: Files.Reader; c: симв8;
	нач
		f := Files.Old( G.HostkeysFile );
		если f = НУЛЬ то
			f := Files.New( G.HostkeysFile );  Files.Register( f );
			Files.OpenWriter( w, f, 0 );
			возврат w
		иначе
			Files.OpenReader( r, f, f.Length() -1 );
			r.чСимв8( c );
			Files.OpenWriter( w, f, f.Length() );
			если c >= ' ' то  w.пВК_ПС  всё;
			возврат w
		всё;
	кон WriterAtEnd;


	проц CompareDSAKeys( конст host: массив из симв8; key: DSA.Key ): булево;
	перем
		r: Потоки.Чтец;  w: Потоки.Писарь;
		msg: массив 512 из симв8;
		res: целМЗ;
		knownKey: DSA.Key;
	нач
		r := GetKeyStart( "ssh-dss", host );
		если r = НУЛЬ то
			msg := "no suitable dsa hostkey found in ";
			Strings.Append( msg, G.HostkeysFile );
			Strings.AppendChar( msg, 0DX );
			Strings.Append( msg, "will you trust the connection anyway?" );
			res := WMDialogs.Message( 1, "Load Public Server Hostkey", msg, {Ok, Abort} );
			если res # Ok то  возврат ложь
			иначе
				w := WriterAtEnd();
				w.пСтроку8( host );  w.пСимв8( ' ' );
				DSA.StorePublicKey( w, key );
				w.ПротолкниБуферВПоток;
				возврат истина
			всё
		всё;
		knownKey := DSA.LoadPublicKey( r );
		если B.Cmp( key.y, knownKey.y ) # 0 то
			Out.пСтроку8( "### error: hostkey of remote host has changed" ); Out.пВК_ПС;
			возврат ложь
		всё;
		возврат истина
	кон CompareDSAKeys;


	проц CompareRSAKeys( конст host: массив из симв8; key: RSA.Key ): булево;
	перем
		r: Потоки.Чтец;  w: Потоки.Писарь;
		msg: массив 512 из симв8;
		res: целМЗ;
		knownKey: RSA.Key;
	нач
		r := GetKeyStart( "ssh-rsa", host );
		если r = НУЛЬ то
			msg := "no suitable rsa hostkey found in ";
			Strings.Append( msg, G.HostkeysFile );
			Strings.AppendChar( msg, 0DX );
			Strings.Append( msg, "will you trust the connection anyway?" );
			res := WMDialogs.Message( 1, "Load Public Server Hostkey", msg, {Ok, Abort} );
			если res # Ok то  возврат ложь
			иначе
				w := WriterAtEnd();
				w.пСтроку8( host );  w.пСимв8( ' ' );
				RSA.StorePublicKey( w, key );
				w.ПротолкниБуферВПоток;
				возврат истина
			всё
		всё;
		knownKey := RSA.ExtractPublicKey( r );
		если B.Cmp( key.modulus, knownKey.modulus ) # 0 то
			Out.пСтроку8( "### error: hostkey of remote host has changed" ); Out.пВК_ПС;
			возврат ложь
		всё;
		возврат истина
	кон CompareRSAKeys;


	проц VerifyIdentity*( конст keyblob, signature, host, hash: массив из симв8 ): булево;
	перем
		name1, name2: массив 128 из симв8; i, j: размерМЗ; len: цел32;
		dsa: DSA.Key; dsasig: DSA.Signature;
		rsa: RSA.Key; e, n, rsasig: BigNumber;
		p, q, g, pub, r, s: BigNumber;
	нач
		i := 0;  U.GetString( keyblob, i, name1 );
		j := 0;  U.GetString( signature, j, name2 );
		если name1 # name2 то  возврат ложь  всё;
		если name1 = "ssh-dss" то
			U.GetBigNumber( keyblob, i, p );
			U.GetBigNumber( keyblob, i, q );
			U.GetBigNumber( keyblob, i, g );
			U.GetBigNumber( keyblob, i, pub );
			dsa := DSA.PubKey( p, q, g, pub );
			U.GetLength( signature, j, len );
			если len # 40 то  возврат ложь  всё;
			B.AssignBin( r, signature, j, 20 ); увел( j, 20 );
			B.AssignBin( s, signature, j, 20 );
			нов( dsasig, r, s );
			если SSHDSSVerify( dsa, dsasig, hash ) то  возврат CompareDSAKeys( host, dsa )
			иначе  возврат ложь
			всё
		аесли name1 = "ssh-rsa" то
			U.GetBigNumber( keyblob, i, e );
			U.GetBigNumber( keyblob, i, n );
			rsa := RSA.PubKey( e, n );
			U.GetBigNumber( signature, j, rsasig );
			если (rsa.name = "unkown") (* from openssh! *) или SSHRSAVerify( rsa, rsasig, hash ) то
				возврат CompareRSAKeys( host, rsa )
			иначе  возврат ложь
			всё
		иначе
			Out.пСтроку8( "### error: unsupported public hostkey type: " );  Out.пСтроку8( name1 ); Out.пВК_ПС;
			возврат ложь
		всё;
	кон VerifyIdentity;



	проц RSAKeyGen*;
	конст
		headline1 = "enter passphrase for new rsa key";
		headline2 = "repeat passphrase for new rsa key";
		Size = 2048;

	перем
		pw1, pw2: массив 32 из симв8; ignore, res: цел32; ok: булево;
		p, q, e: BigNumber;
		priv, pub: RSA.Key;
		f: Files.File; w: Files.Writer;
	нач
		нцДо
			ignore := WMDialogs.QueryPassword( headline1, pw1 );
			ok := Strings.Length( pw1 ) > 5;
			если ~ok то
				res := WMDialogs.Message( 1, "RSA Keygen", "pease insert a longer key", {Ok, Abort} );
				если res = Abort то  возврат  всё
			всё
		кцПри ok;
		ignore := WMDialogs.QueryPassword( headline2, pw2 );
		если pw1 # pw2 то
			res := WMDialogs.Message( 1, "RSA Keygen", "passphrases don't match", {Ok} );
			возврат
		всё;
		p := P.NewPrime( Size DIV 2, ложь );
		q := P.NewPrime( Size DIV 2, ложь );
		B.AssignInt( e, 3 );
		RSA.MakeKeys( p, q, e, "Aos rsa-key", pub, priv );
		f := Files.New( G.PrivateKeyFile );  Files.OpenWriter( w, f, 0 );
		RSA.StorePrivateKey( w, priv, pw1 );
		w.ПротолкниБуферВПоток;
		Files.Register( f );
		f := Files.New( G.PublicKeyFile );  Files.OpenWriter( w, f, 0 );
		RSA.StorePublicKey( w, pub );
		w.ПротолкниБуферВПоток;
		Files.Register( f );
	кон RSAKeyGen;

нач
кон SSHKeys.
.


SSHKeys.RSAKeyGen ~

System.Free SSHKeys ~
