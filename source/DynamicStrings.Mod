модуль DynamicStrings;	(** Stefan Walthert  *)
(** AUTHOR "swalthert"; PURPOSE "Dynamic strings"; *)

использует
	НИЗКОУР, Потоки, Strings;

конст
	CR* = 0DX; (** the Oberon end of line character *)
	Tab* = 09X; (** the horizontal tab character *)
	LF* = 0AX; (** the UNIX end of line character *)

	InitialSize = 256;
	HashTableSize = 256;
	InitialStringArraySize = 8;

	(* Gather statistics of string pool operations *)
	Statistics = истина;

тип
	String* = Strings.String;

	DynamicString* = окласс
	перем
		buffer: String; (* { (buffer # NIL) & (LEN(buffer) = bufferSize) } *)
		bufferSize: размерМЗ; (* {bufferSize >= InitialSize} *)

		length : размерМЗ; (* current length of string excluding 0X *)

		проц &Init*;
		нач
			bufferSize := InitialSize;
			нов(buffer, bufferSize);
			Clear;
		кон Init;

		(* Set string to empty string without changing bufferSize *)
		проц Clear*;
		нач
			buffer[0] := 0X;
			length := 0;
		кон Clear;

		проц AdjustBufferSize(minSize: размерМЗ);
		перем newBuffer : String;
		нач
			если minSize >= bufferSize то
				нцДо bufferSize := 2 * bufferSize; кцПри (bufferSize > minSize);
				нов(newBuffer, bufferSize);
				копируйСтрокуДо0(buffer^, newBuffer^);
				buffer := newBuffer;
			всё;
		кон AdjustBufferSize;

		проц Put*(ch: симв8; at: размерМЗ);
		нач
			если (at + 1 >= bufferSize) то AdjustBufferSize(at + 1); всё;
			buffer[at] := ch;
			length := StringLength(buffer^); (* not optimized *)
		кон Put;

		проц Get*(at: размерМЗ): симв8;
		нач
			если at + 1 > bufferSize то
				возврат 0X;
			иначе
				возврат buffer[at];
			всё;
		кон Get;

		проц AppendCharacter*(ch : симв8);
		нач
			если (ch # 0X) то
				если (length + 1 + 1 >= bufferSize) то AdjustBufferSize(length + 1 + 1); всё;
				buffer[length] := ch;
				buffer[length + 1] := 0X;
				увел(length);
			всё;
		кон AppendCharacter;

		проц Append*(конст this: массив из симв8);
		перем thisLength : размерМЗ;
		нач
			thisLength := StringLength(this);
			если (length + thisLength + 1 >= bufferSize) то AdjustBufferSize(length + thisLength + 1); всё;
			Strings.Append(buffer^, this);
			length := length + thisLength;
		кон Append;

		проц Extract*(offset, len: размерМЗ): String;
		перем s: String; i: размерМЗ;
		нач
			если offset < length то
				если offset + len > length то len := length - offset всё;
				нов(s, len + 1);
				нцДля i := 0 до len - 1 делай
					s[i] := buffer[i + offset]
				кц;
				s[len] := 0X;
			иначе
				нов(s, 1); s[0] := 0X;
			всё;
			возврат s
		кон Extract;

		проц Length*(): размерМЗ;
		нач
			возврат length;
		кон Length;

		проц ToArrOfChar*(): String;
		перем string: String;
		нач
			нов(string, length + 1);
			копируйСтрокуДо0(buffer^, string^);
			возврат string;
		кон ToArrOfChar;

		проц FromArrOfChar*(s: String);
		нач
			length := StringLength(s^);
			нов(buffer, length + 1);
			копируйСтрокуДо0(s^, buffer^);
		кон FromArrOfChar;

		(** Copy <len> characters starting at <offset> from string <ds> into this dynamic string*)
		проц CopyFrom*(ds : DynamicString; offset, len : размерМЗ);
		перем i : размерМЗ;
		нач
			утв((ds # НУЛЬ) и (offset >= 0) и (len >= 0));
			если (offset < length) то
				если (offset + len > length) то len := length - offset; всё;
				AdjustBufferSize(len + 1);
				нцДля i := 0 до len - 1 делай
					buffer[i] := ds.buffer[i + offset];
				кц;
				buffer[len] := 0X;
				length := len;
			иначе
				buffer[0] := 0X;
				length := 0;
			всё;
		кон CopyFrom;

		проц EqualsTo*(конст string : массив из симв8; ignoreCase : булево) : булево;
		перем len : размерМЗ; result : булево; i : размерМЗ;
		нач
			len := StringLength(string);
			result := (len = length);
			если result то
				i := 0;
				если ignoreCase то
					нцПока result и (i < length) делай
						result := Strings.UP(string[i]) = Strings.UP(buffer[i]);
						увел(i);
					кц;
				иначе
					нцПока result и (i < length) делай
						result := string[i] = buffer[i];
						увел(i);
					кц;
				всё;
			всё;
			возврат result;
		кон EqualsTo;

	кон DynamicString;

тип

	StringEntry = запись
		value : String;
		length : размерМЗ;
	кон;

	StringEntryArray = укль на массив из StringEntry;

	HashTableEntry = запись
		strings : StringEntryArray; (* {strings # NIL} *)
		nofStrings : размерМЗ; (* { (0 <= nofStrings) & (nofStrings < LEN(strings)) } *)
	кон;

	Pool* = окласс
	перем
		hashtable : массив HashTableSize из HashTableEntry;

		проц &Init*;
		перем i : размерМЗ;
		нач
			нцДля i := 0 до длинаМассива(hashtable) - 1 делай
				нов(hashtable[i].strings, InitialStringArraySize);
			кц;
			Clear;
		кон Init;

		проц Clear*;
		перем i, j : размерМЗ;
		нач
			нцДля i := 0 до длинаМассива(hashtable) - 1 делай
				нцДля j := 0 до длинаМассива(hashtable[i].strings) - 1 делай
					hashtable[i].strings[j].value := НУЛЬ;
					hashtable[i].strings[j].length := 0;
				кц;
				hashtable[i].nofStrings := 0;
			кц;
		кон Clear;

		(* Compute index into hashtable (copied from StringPool.Mod) *)
		проц Hash(ds : DynamicString) : размерМЗ;
		перем index, i : размерМЗ; ch : симв8;
		нач
			утв(ds # НУЛЬ);
			index := 0;
			i := 0; ch := ds.buffer[0]; index := 0;
			нцПока (ch # 0X) делай
				index := размерМЗ(мнвоНаБитахМЗ(вращБит(index, 7)) / мнвоНаБитахМЗ(кодСимв8(ch)));
				увел(i); ch := ds.buffer[i]
			кц;
			index := index остОтДеленияНа HashTableSize;
			утв((0 <= index) и (index < HashTableSize));
			возврат index;
		кон Hash;

		проц Find(ds : DynamicString) : Strings.String;
		перем string : String; entry : HashTableEntry; i : размерМЗ;
		нач
			утв(ds # НУЛЬ);
			string := НУЛЬ;
			entry := hashtable[Hash(ds)];
			(* skip entries that are shorter and/or lexically smaller *)
			i := 0; нцПока (i < entry.nofStrings) и LessThan(ds, entry.strings[i]) делай увел(i); кц;
			(* compare candidates *)
			нцПока (string = НУЛЬ) и (i < entry.nofStrings) и ~GreaterThan(ds, entry.strings[i]) делай
				если (ds.length = entry.strings[i].length) и Equals(ds, entry.strings[i]) то
					string := entry.strings[i].value;
				всё;
				увел(i);
			кц;
			возврат string;
		кон Find;

		(* Double the size of the StringEntryArray within <entry> *)
		проц Grow(перем strings : StringEntryArray);
		перем newStrings : StringEntryArray; i : размерМЗ;
		нач
			нов(newStrings, 2 * длинаМассива(strings));
			нцДля i := 0 до длинаМассива(strings) - 1 делай
				newStrings[i] := strings[i];
			кц;
			strings := newStrings;
		кон Grow;

		проц Add(ds : DynamicString; index : размерМЗ; перем string : String);
		перем (*entry : HashTableEntry;*)  i, j : размерМЗ; (*! careful: entry is a record, not a pointer *)
		нач (* assumption: ds is not yet contained in pool! *)
			утв(ds # НУЛЬ);
			утв((0 <= index) и (index < HashTableSize));
			(*
			entry := hashtable[index];
			*)
			если (hashtable[index].nofStrings >= длинаМассива(hashtable[index].strings)) то Grow(hashtable[index].strings); всё;
			(* skip entries that are lexically less than *)
			i := 0; нцПока (i < hashtable[index].nofStrings) и LessThan(ds, hashtable[index].strings[i]) делай увел(i); кц;
			(* move strings to the right to make place for new string at index i *)
			j := hashtable[index].nofStrings - 1;
			нцПока (j >= i) делай
				hashtable[index].strings[j + 1] := hashtable[index].strings[j];
				умень(j);
			кц;
			(* insert new string *)
			string := ds.ToArrOfChar();
			hashtable[index].strings[i].value := string;
			hashtable[index].strings[i].length := ds.length;
			увел(hashtable[index].nofStrings);
			утв(string # НУЛЬ);
		кон Add;

		(** Get string from pool. If the string is not contained in the pool, a copy of it is added to the pool *)
		проц Get*(ds : DynamicString) : Strings.String;
		перем string : String;
		нач
			утв(ds # НУЛЬ);
			если Statistics то увел(NnofRequests); всё;
			string := Find(ds);
			если (string = НУЛЬ) то
				если Statistics то увел(NnofAdded); всё;
				Add(ds, Hash(ds), string);
			аесли Statistics то
				увел(NnofHits);
			всё;
			утв(string # НУЛЬ);
			возврат string;
		кон Get;

		проц Dump*(out : Потоки.Писарь);
		перем index, entry : размерМЗ;
		нач
			утв(out # НУЛЬ);
			out.пСтроку8("String pool dump:");
			нцДля index := 0 до длинаМассива(hashtable) - 1 делай
				если (hashtable[index].nofStrings # 0) то
					out.пЦел64(hashtable[index].nofStrings, 0); out.пСтроку8(" entries at index ");
					out.пЦел64(index, 0); out.пСтроку8(": "); out.пВК_ПС;
					нцДля entry := 0 до hashtable[index].nofStrings - 1 делай
						out.пСтроку8("    "); out.пСтроку8(hashtable[index].strings[entry].value^);
						out.пСтроку8(", length = "); out.пЦел64(hashtable[index].strings[entry].length, 0);
						out.пВК_ПС;
					кц;
				всё;
			кц;
			out.пВК_ПС;
		кон Dump;

	кон Pool;

перем
	(* Statistics, not multi-instance capable *)
	NnofRequests, NnofHits, NnofAdded : размерМЗ;

(* Returns TRUE if the string ds.buffer is shorter or lexically smaller than entry.value *)
проц LessThan(ds : DynamicString; конст entry : StringEntry) : булево;
перем lessThan : булево; i : размерМЗ;
нач
	i := 0;
	lessThan := (ds.length < entry.length);
	нцПока ~lessThan и (i < entry.length) делай
		lessThan := (ds.buffer[i] < entry.value[i]);
		увел(i);
	кц;
	возврат lessThan;
кон LessThan;

(* Returns TRUE if the string ds.buffer is larger or lexically greater than entry.value *)
проц GreaterThan(ds : DynamicString; конст entry : StringEntry) : булево;
перем greaterThan : булево; i : размерМЗ;
нач
	i := 0;
	greaterThan := (ds.length > entry.length);
	нцПока ~greaterThan и (i < ds.length) делай
		greaterThan := (ds.buffer[i] > entry.value[i]);
		увел(i);
	кц;
	возврат greaterThan;
кон GreaterThan;

(* Return TRUE if the string ds.buffer has same length and content as entry.value *)
проц Equals(ds : DynamicString; конст entry : StringEntry) : булево;
перем equals : булево; i : размерМЗ;
нач
	i := 0;
	equals := (ds.length = entry.length);
	нцПока equals и (i < ds.length) делай
		equals := (ds.buffer[i] = entry.value[i]);
		увел(i);
	кц;
	возврат equals;
кон Equals;

проц StringLength*(конст str: массив из симв8): размерМЗ;
	перем i, l: размерМЗ;
нач
	l := длинаМассива(str); i := 0;
	нцПока (i < l) и (str[i] # 0X) делай
		увел(i)
	кц;
	возврат i
кон StringLength;

проц StringAppend*(перем to: массив из симв8; конст this: массив из симв8);
нач Strings.Append (to, this);
кон StringAppend;

проц Lower*(конст str: массив из симв8; перем lstr: массив из симв8);
	перем i: размерМЗ;
нач
	i := 0;
	нцПока str[i] # 0X делай
		lstr[i] := LowerCh(str[i]); увел(i)
	кц;
	lstr[i] := 0X
кон Lower;

проц LowerCh*(ch: симв8): симв8;
нач
	просей ch из
		"A" .. "Z": ch := симв8ИзКода(кодСимв8(ch)-кодСимв8("A")+кодСимв8("a"))
(*		|"Ä": ch := "ä"
		|"Ö": ch := "ö"
		|"Ü": ch := "ü" *)
	иначе
	всё;
	возврат ch
кон LowerCh;

проц IntToStr*(val: цел32; перем str: массив из симв8);
	перем
		i, j: размерМЗ;
		digits: массив 16 из симв8;
нач
	если val = матМинимум(цел32) то
		копируйСтрокуДо0("-2147483648", str);
		возврат
	всё;
	если val < 0 то
		val := -val; str[0] := "-"; j := 1
	иначе
		j := 0
	всё;
	i := 0;
	нцДо
		digits[i] := симв8ИзКода (val остОтДеленияНа 10 + кодСимв8("0")); увел(i); val := val DIV 10
	кцПри val = 0;
	умень(i);
	нцПока i >= 0 делай
		str[j] := digits[i]; увел(j); умень(i)
	кц;
	str[j] := 0X
кон IntToStr;

проц StrToInt*(конст str: массив из симв8): цел32;
	перем val, d: цел32; i: размерМЗ; ch: симв8; neg: булево;
нач
	val := 0; i := 0; ch := str[0];
	нцПока (ch # 0X) и (ch <= " ") делай
		увел(i); ch := str[i]
	кц;
	neg := ложь; если ch = "+" то увел(i); ch := str[i] всё;
	если ch = "-" то neg := истина; увел(i); ch := str[i] всё;
	нцПока (ch # 0X) и (ch <= " ") делай
		увел(i); ch := str[i]
	кц;
	val := 0;
	нцПока (ch >= "0") и (ch <= "9") делай
		d := кодСимв8(ch)-кодСимв8("0");
		увел(i); ch := str[i];
		если val <= ((матМаксимум(цел32)-d) DIV 10) то
			val := 10*val+d
		аесли neg и (val = 214748364) и (d = 8) и ((ch < "0") или (ch > "9")) то
			val := матМинимум(цел32); neg := ложь
		иначе
			СТОП(99)
		всё
	кц;
	если neg то val := -val всё;
	возврат val
кон StrToInt;

проц HexStrToInt*(конст str: массив из симв8): цел32;
перем val, d: цел32; i: размерМЗ; ch: симв8;
нач
	i := 0; ch := str[0];
	нцПока (ch # 0X) и (ch <= " ") делай
		увел(i); ch := str[i]
	кц;
	val := 0;
	нцПока (("0" <= ch) и (ch <= "9")) или (("A" <= ch) и (ch <= "F")) делай
		если (("0" <= ch) и (ch <= "9")) то d := кодСимв8(ch)-кодСимв8("0")
		иначе d := кодСимв8(ch) - кодСимв8("A") + 10
		всё;
		увел(i); ch := str[i];
		если val <= ((матМаксимум(цел32)-d) DIV 10H) то
			val := 10H*val+d
		иначе
			СТОП(99)
		всё
	кц;
	возврат val
кон HexStrToInt;

проц Search*(конст pat, src: массив из симв8; перем pos: размерМЗ);
	конст MaxPat = 128;
	перем
		buf: массив MaxPat из симв8;
		len, i, srclen: размерМЗ;

	проц Find(beg: размерМЗ);
		перем
			i, j, b, e: размерМЗ;
			ch: симв8;
			ref: массив MaxPat из симв8;
	нач
		ch := src[pos]; увел(pos);
		ref[0] := ch;
		i := 0; j := 0; b := 0; e := 1;
		нцПока (pos <= srclen) и (i < len) делай
			если buf[i] = ch то
				увел(i); j := (j + 1) остОтДеленияНа MaxPat
			иначе
				i := 0; b := (b + 1) остОтДеленияНа MaxPat; j := b
			всё;
			если j # e то
				ch := ref[j]
			иначе
				если pos >= srclen то
					ch := 0X
				иначе
					ch := src[pos]
				всё;
				увел(pos); ref[j] := ch; e := (e + 1) остОтДеленияНа MaxPat; увел(beg);
			всё
		кц;
		если i = len то
			pos := beg-len
		иначе
			pos := -1
		всё
	кон Find;

нач
	len := StringLength(pat);
	если MaxPat < len то
		len := MaxPat
	всё;
	если len <= 0 то
		pos := -1;
		возврат
	всё;
	i := 0;
	нцДо
		buf[i] := pat[i]; увел(i)
	кцПри i >= len;
	srclen := StringLength(src);
	если pos < 0 то
		pos := 0
	аесли pos >= srclen то
		pos := -1;
		возврат
	всё;
	Find(pos)
кон Search;

проц ClearStatistics*;
нач
	NnofRequests := 0;
	NnofHits := 0;
	NnofAdded := 0;
кон ClearStatistics;

нач
	ClearStatistics;
кон DynamicStrings.

DynamicStrings.ClearStatistics ~

WMPerfMonPluginModVars.Install StringPool
	DynamicStrings.NnofRequests DynamicStrings.NnofHits DynamicStrings.NnofAdded
~
