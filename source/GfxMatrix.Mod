(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль GfxMatrix; (** portable *)	(* eos  *)
(** AUTHOR "eos"; PURPOSE "Affine transformations in 2D"; *)

	(*
		21.02.2000 - bugfix in Scaled: didn't recognize downscaling
		15.04.2000 - added Atan2
	*)

	использует
		Потоки, Math;


	конст
		Eps = 1.0E-5;


	тип
		(**
			Transformation matrix
				3x2_matrices can represent any combination of affine transformations, i.e. of translation, rotation, scaling and
				shearing.

			Translate by tx, ty:
				[  1  0 ]
				[  0  1 ]
				[ tx ty ]

			Scale by sx, sy:
				[ sx   0 ]
				[  0  sy ]
				[  0   0 ]

			Rotate counter-clockwise by angle phi:
				[  cos(phi)   sin(phi) ]
				[ -sin(phi)  cos(phi) ]
				[       0             0       ]

			Shear along x_axis by factor f:
				[ 1   0 ]
				[ f    1 ]
				[ 0   0 ]
		**)

		Matrix* = массив 3, 2 из вещ32;


	перем
		Identity*: Matrix;	(** identity matrix (read_only) **)


	(**--- Matrix Computation ---**)

	(** initialize matrix with given values **)
	проц Init* (перем m: Matrix; m00, m01, m10, m11, m20, m21: вещ32);
	нач
		m[0, 0] := m00; m[0, 1] := m01;
		m[1, 0] := m10; m[1, 1] := m11;
		m[2, 0] := m20; m[2, 1] := m21
	кон Init;

	(**
		Procedures Get3PointTransform, Get2PointTransform and Invert may not be able to find a solution. In that case,
		they return a singular matrix with all elements set to zero.
	**)

	(** calculate matrix that maps p0 to p1, q0 to q1, and r0 to r1 **)
	проц Get3PointTransform* (px0, py0, px1, py1, qx0, qy0, qx1, qy1, rx0, ry0, rx1, ry1: вещ32; перем res: Matrix);
		перем m: массив 6, 7 из вещ32; i, j, k: цел32; max, t: вещ32; v: массив 6 из вещ32;
	нач
		(* initialize set of linear equations for matrix coefficients *)
		m[0, 0] := px0; m[0, 1] := py0; m[0, 2] := 1.0; m[0, 3] := 0.0; m[0, 4] := 0.0; m[0, 5] := 0.0; m[0, 6] := px1;
		m[1, 0] := qx0; m[1, 1] := qy0; m[1, 2] := 1.0; m[1, 3] := 0.0; m[1, 4] := 0.0; m[1, 5] := 0.0; m[1, 6] := qx1;
		m[2, 0] := rx0; m[2, 1] := ry0; m[2, 2] := 1.0; m[2, 3] := 0.0; m[2, 4] := 0.0;  m[2, 5] := 0.0; m[2, 6] := rx1;
		m[3, 0] := 0.0; m[3, 1] := 0.0; m[3, 2] := 0.0; m[3, 3] := px0; m[3, 4] := py0; m[3, 5] := 1.0; m[3, 6] := py1;
		m[4, 0] := 0.0; m[4, 1] := 0.0; m[4, 2] := 0.0; m[4, 3] := qx0; m[4, 4] := qy0; m[4, 5] := 1.0; m[4, 6] := qy1;
		m[5, 0] := 0.0; m[5, 1] := 0.0; m[5, 2] := 0.0; m[5, 3] := rx0; m[5, 4] := ry0; m[5, 5] := 1.0; m[5, 6] := ry1;

		(* Gaussian elimination with pivoting *)
		нцДля i := 0 до 5 делай
			k := i; max := матМодуль(m[i, i]);
			нцДля j := i+1 до 5 делай
				если матМодуль(m[j, i]) > max то
					k := j; max := матМодуль(m[j, i])
				всё
			кц;
			если max < Eps то	(* matrix is singular *)
				Init(res, 0, 0, 0, 0, 0, 0);
				возврат
			всё;
			если k # i то	(* swap rows to bring largest element up *)
				нцДля j := i до 6 делай
					t := m[i, j]; m[i, j] := m[k, j]; m[k, j] := t
				кц
			всё;
			нцДля k := i+1 до 5 делай
				t := m[k, i]/m[i, i];
				нцДля j := i+1 до 6 делай
					m[k, j] := m[k, j] - t * m[i, j]
				кц
			кц
		кц;

		(* solve equations *)
		нцДля i := 5 до 0 шаг -1 делай
			t := m[i, 6];
			нцДля j := i+1 до 5 делай
				t := t - v[j] * m[i, j]
			кц;
			v[i] := t/m[i, i]
		кц;

		Init(res, v[0], v[3], v[1], v[4], v[2], v[5])
	кон Get3PointTransform;

	(** calculate matrix that maps p0 to p1 and q0 to q1 **)
	проц Get2PointTransform* (px0, py0, px1, py1, qx0, qy0, qx1, qy1: вещ32; перем res: Matrix);
		перем rx0, ry0, rx1, ry1: вещ32;
	нач
		rx0 := px0 + py0 - qy0; ry0 := py0 + qx0 - px0;
		rx1 := px1 + py1 - qy1; ry1 := py1 + qx1 - px1;
		Get3PointTransform(px0, py0, px1, py1, qx0, qy0, qx1, qy1, rx0, ry0, rx1, ry1, res)
	кон Get2PointTransform;

	(** calculate inverse of matrix **)
	проц Invert* (m: Matrix; перем res: Matrix);
		перем det, inv: вещ32;
	нач
		det := m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0];
		если матМодуль(det) >= Eps то	(* matrix can be inverted; use Cramer's rule *)
			inv := 1/det;
			res[0, 0] := +inv * m[1, 1];
			res[0, 1] := -inv * m[0, 1];
			res[1, 0] := -inv * m[1, 0];
			res[1, 1] := +inv * m[0, 0];
			res[2, 0] := +inv * (m[1, 0] * m[2, 1] - m[1, 1] * m[2, 0]);
			res[2, 1] := +inv * (m[0, 1] * m[2, 0] - m[0, 0] * m[2, 1])
		иначе
			Init(res, 0, 0, 0, 0, 0, 0)
		всё
	кон Invert;


	(**--- Detection of Special Cases ---**)

	(** return determinant of matrix **)
	проц Det* (перем m: Matrix): вещ32;
	нач
		возврат m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0]
	кон Det;

	(** return whether matrix is singular **)
	проц Singular* (перем m: Matrix): булево;
	нач
		возврат матМодуль(m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0]) < Eps
	кон Singular;

	(** return whether matrix changes vector lengths **)
	проц Scaled* (перем m: Matrix): булево;
	нач
		возврат матМодуль(матМодуль(m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0]) - 1) > Eps
	кон Scaled;

	(** return whether matrix includes rotation, shear, or mirror transformation **)
	проц Rotated* (перем m: Matrix): булево;
	нач
		возврат (m[0, 0] < -Eps) или (m[1, 1] < -Eps) или (матМодуль(m[0, 1]) > Eps) или (матМодуль(m[1, 0]) > Eps)
	кон Rotated;

	(** return whether matrices should be considered equal **)
	проц Equal* (перем m, n: Matrix): булево;
	нач
		возврат
			(матМодуль(m[0, 0] - n[0, 0]) < Eps) и (матМодуль(m[0, 1] - n[0, 1]) < Eps) и
			(матМодуль(m[1, 0] - n[1, 0]) < Eps) и (матМодуль(m[1, 1] - n[1, 1]) < Eps) и
			(матМодуль(m[2, 0] - n[2, 0]) < Eps) и (матМодуль(m[2, 1] - n[2, 1]) < Eps)
	кон Equal;


	(**--- Matrix Concatenation ---**)

	(**
		Combinations of single transformations are evaluated from left to right. Executing Translate, Rotate or Scale
		pre-concatenates a corresponding matrix to the left of the given matrix parameter. This has the effect that
		the new transformation is applied before all previously accumulated transformations. Every transformation is
		therefore executed in the context of the coordinate system defined by the concatenation of all transformations
		to its right.
	**)

	(** translation by (dx, dy) **)
	проц Translate* (m: Matrix; dx, dy: вещ32; перем res: Matrix);
	нач
		res[0, 0] := m[0, 0]; res[0, 1] := m[0, 1];
		res[1, 0] := m[1, 0]; res[1, 1] := m[1, 1];
		res[2, 0] := m[2, 0] + dx * m[0, 0] + dy * m[1, 0];
		res[2, 1] := m[2, 1] + dx * m[0, 1] + dy * m[1, 1]
	кон Translate;

	(** scale by (sx, sy) **)
	проц Scale* (m: Matrix; sx, sy: вещ32; перем res: Matrix);
	нач
		res[0, 0] := sx * m[0, 0]; res[0, 1] := sx * m[0, 1];
		res[1, 0] := sy * m[1, 0]; res[1, 1] := sy * m[1, 1];
		res[2, 0] := m[2, 0]; res[2, 1] := m[2, 1]
	кон Scale;

	(** scale at (ox, oy) by (sx, sy) **)
	проц ScaleAt* (m: Matrix; ox, oy, sx, sy: вещ32; перем res: Matrix);
		перем tx, ty: вещ32;
	нач
		res[0, 0] := sx * m[0, 0]; res[0, 1] := sx * m[0, 1];
		res[1, 0] := sy * m[1, 0]; res[1, 1] := sy * m[1, 1];
		tx := ox * (1-sx); ty := oy * (1-sy);
		res[2, 0] := tx * m[0, 0] + ty * m[1, 0] + m[2, 0];
		res[2, 1] := tx * m[0, 1] + ty * m[1, 1] + m[2, 1]
	кон ScaleAt;

	(** rotate counter-clockwise by angle specified by its sine and cosine **)
	проц Rotate* (m: Matrix; sin, cos: вещ32; перем res: Matrix);
	нач
		res[0, 0] := cos * m[0, 0] + sin * m[1, 0]; res[0, 1] := cos * m[0, 1] + sin * m[1, 1];
		res[1, 0] := -sin * m[0, 0] + cos * m[1, 0]; res[1, 1] := -sin * m[0, 1] + cos * m[1, 1];
		res[2, 0] := m[2, 0]; res[2, 1] := m[2, 1]
	кон Rotate;

	(** rotate counter-clockwise around (ox, oy) by angle specified by its sine and cosine **)
	проц RotateAt* (m: Matrix; ox, oy, sin, cos: вещ32; перем res: Matrix);
		перем tx, ty: вещ32;
	нач
		res[0, 0] := cos * m[0, 0] + sin * m[1, 0]; res[0, 1] := cos * m[0, 1] + sin * m[1, 1];
		res[1, 0] := -sin * m[0, 0] + cos * m[1, 0]; res[1, 1] := -sin * m[0, 1] + cos * m[1, 1];
		tx := ox * (1-cos) + oy * sin; ty := oy * (1-cos) - ox * sin;
		res[2, 0] := tx * m[0, 0] + ty * m[1, 0] + m[2, 0];
		res[2, 1] := tx * m[0, 1] + ty * m[1, 1] + m[2, 1]
	кон RotateAt;

	(** concatenate matrices **)
	проц Concat* (m, n: Matrix; перем res: Matrix);
	нач
		res[0, 0] := m[0, 0] * n[0, 0] + m[0, 1] * n[1, 0];
		res[0, 1] := m[0, 0] * n[0, 1] + m[0, 1] * n[1, 1];
		res[1, 0] := m[1, 0] * n[0, 0] + m[1, 1] * n[1, 0];
		res[1, 1] := m[1, 0] * n[0, 1] + m[1, 1] * n[1, 1];
		res[2, 0] := m[2, 0] * n[0, 0] + m[2, 1] * n[1, 0] + n[2, 0];
		res[2, 1] := m[2, 0] * n[0, 1] + m[2, 1] * n[1, 1] + n[2, 1]
	кон Concat;


	(**--- Arctan of Vector ---**)

	проц Atan2* (x, y: вещ32): вещ32;
		перем phi: вещ32;
	нач
		если (матМодуль(x) < 1.0) и (матМодуль(y) >= матМодуль(x * матМаксимум(вещ32))) то	(* y/x would overflow *)
			если y >= 0.0 то phi := Math.pi/2
			иначе phi := -Math.pi/2
			всё
		аесли x > 0.0 то	(* 1st or 4th quadrant *)
			phi := Math.arctan(y/x)
		аесли x < 0.0 то	(* 2nd or 3rd quadrant *)
			phi := Math.arctan(y/x) + Math.pi
		всё;
		возврат phi
	кон Atan2;


	(**--- Matrix Application ---**)

	(** apply transformation matrix to point **)
	проц Apply* (перем m: Matrix; xin, yin: вещ32; перем xout, yout: вещ32);
	нач
		xout := xin * m[0, 0] + yin * m[1, 0] + m[2, 0];
		yout := xin * m[0, 1] + yin * m[1, 1] + m[2, 1]
	кон Apply;

	(** apply transformation matrix to vector (ignoring translation) **)
	проц ApplyToVector* (перем m: Matrix; xin, yin: вещ32; перем xout, yout: вещ32);
	нач
		xout := xin * m[0, 0] + yin * m[1, 0];
		yout := xin * m[0, 1] + yin * m[1, 1]
	кон ApplyToVector;

	(** apply transformation matrix to distance **)
	проц ApplyToDist* (перем m: Matrix; din: вещ32; перем dout: вещ32);
		перем x, y: вещ32;
	нач
		x := din * m[0, 0]; y := din * m[0, 1];
		если матМодуль(y) < 1.0E-3 то dout := x
		иначе dout := Math.sqrt(x * x + y * y)
		всё
	кон ApplyToDist;

	(** apply transformation matrix to axis-aligned rectangle; result is enclosing axis-aligned rectangle **)
	проц ApplyToRect* (перем m: Matrix; ilx, ily, irx, iuy: вещ32; перем olx, oly, orx, ouy: вещ32);
		перем l, h: вещ32;
	нач
		olx := m[2, 0]; orx := m[2, 0];
		l := ilx * m[0, 0]; h := irx * m[0, 0];
		если l <= h то olx := olx + l; orx := orx + h иначе olx := olx + h; orx := orx + l всё;
		l := ily * m[1, 0]; h := iuy * m[1, 0];
		если l <= h то olx := olx + l; orx := orx + h иначе olx := olx + h; orx := orx + l всё;
		oly := m[2, 1]; ouy := m[2, 1];
		l := ilx * m[0, 1]; h := irx * m[0, 1];
		если l <= h то oly := oly + l; ouy := ouy + h иначе oly := oly + h; ouy := ouy + l всё;
		l := ily * m[1, 1]; h := iuy * m[1, 1];
		если l <= h то oly := oly + l; ouy := ouy + h иначе oly := oly + h; ouy := ouy + l всё
	кон ApplyToRect;

	(** apply inverse of matrix to point **)
	проц Solve* (перем m: Matrix; u, v: вещ32; перем x, y: вещ32);
		перем det: вещ32;
	нач
		det := m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0];
		если матМодуль(det) >= Eps то	(* matrix can be inverted *)
			u := u - m[2, 0]; v := v - m[2, 1];
			x := (m[1, 1] * u - m[1, 0] * v)/det;
			y := (m[0, 0] * v - m[0, 1] * u)/det
		всё
	кон Solve;


	(**--- Matrix I/O ---**)

	проц Write* (перем w: Потоки.Писарь; перем m: Matrix);
		перем i: цел32;
	нач
		нцДля i := 0 до 2 делай
			w.пВещ32_мз(m[i, 0]); w.пВещ32_мз(m[i, 1])
		кц
	кон Write;

	проц Read* (перем r: Потоки.Чтец; перем m: Matrix);
		перем i: цел32;
	нач
		нцДля i := 0 до 2 делай
			r.чВещ32_мз(m[i, 0]); r.чВещ32_мз(m[i, 1])
		кц
	кон Read;


нач
	Init(Identity, 1, 0, 0, 1, 0, 0)
кон GfxMatrix.
