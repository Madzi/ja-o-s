модуль OdUtil;
(* WebDAV, Copyright 2003-7, Edgar Schwarz.
Author.    Edgar Schwarz, edgar@edgarschwarz.de, (es)
Contents. Some Strings: Logging, Lists, ...
Remarks. Local variables are copied for lack of access in parent.
Remarks. TODO: Merge Strings from DAVChain.
*)
использует Clock, Потоки, Files, TFLog, Strings, ЛогЯдра;

конст
	eth = ложь;
(* $IF ~eth THEN *)
	ModeWriter * = 1;
	ModeFile * = 2;
(* $END *)

тип
	Array8 = массив 8 из симв8;

тип
	Log* = окласс (TFLog.Log)
	перем
		appName : массив 64 из симв8;
		logPos : цел32;
		logLine : массив 1024 из симв8;
		kind : цел32;
		locked : булево;
		disableLogToOut : булево;
		f : Files.File;
(* $IF ~eth THEN *)
		w : Потоки.Писарь;
		fsw : Files.Writer;
		mode: цел16;
(* $ELSE
		w : Files.Writer;
$END *)

		проц &{перекрыта}Init*(logName : массив из симв8);
		нач
			копируйСтрокуДо0(logName, appName)
		кон Init;

		проц {перекрыта}SetLogFile*(fn : массив из симв8);
		нач {единолично}
			если f # НУЛЬ то Files.Register(f) всё;
			если fn = "" то f := НУЛЬ
			иначе
				f := Files.Old(fn);
				если f = НУЛЬ то f := Files.New(fn) всё;
(* $IF ~eth THEN *)
				Files.OpenWriter(fsw, f, f.Length());
				w := fsw;
				mode := ModeFile;
(* $ELSE
				Files.OpenWriter(w, f, f.Length())
$END *)
			всё
		кон SetLogFile;

		проц SetLogWriter * (w: Потоки.Писарь);
		нач {единолично}
			сам.w := w;
			mode := ModeWriter;
		кон SetLogWriter;

		проц {перекрыта}SetLogToOut*(enabled : булево);
		нач {единолично}
			disableLogToOut := ~enabled
		кон SetLogToOut;

		проц SetKind(kind : цел32);
		нач {единолично}
			сам.kind := kind
		кон SetKind;

		проц InternalLn;
		нач
			logPos := 0; kind := TFLog.Unknown;
			если ~disableLogToOut то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				если kind = TFLog.Information то ЛогЯдра.пСтроку8("[I] ") всё;
				если kind = TFLog.Warning то ЛогЯдра.пСтроку8("[W] ") всё;
				если kind = TFLog.Error то ЛогЯдра.пСтроку8("[E] ") всё;
				ЛогЯдра.пСтроку8(appName); ЛогЯдра.пСтроку8(" : "); ЛогЯдра.пСтроку8(logLine);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			logLine[0] := 0X;
если ~eth то
			если mode = ModeWriter то w.пВК_ПС(); w.ПротолкниБуферВПоток();
			аесли (mode = ModeFile) и (f # НУЛЬ) то w.пВК_ПС(); w.ПротолкниБуферВПоток(); f.Update() всё
иначе (*
			IF f # NIL THEN Streams.WriteLn(w); Streams.Update(w); f.Update() END;
*) всё;
		кон InternalLn;

		проц {перекрыта}Ln*;
		нач {единолично}
			InternalLn
		кон Ln;

		проц {перекрыта}Enter*;
		нач {единолично}
			дождись(~locked); locked := истина
		кон Enter;

		проц {перекрыта}Exit*;
		нач {единолично}
			InternalLn;
			locked := ложь
		кон Exit;

		проц InternalChar(x: симв8);
		нач
			если logPos >= длинаМассива(logLine) - 1 то InternalLn всё;
			logLine[logPos] := x; logLine[logPos + 1] := 0X; увел(logPos);
если ~eth то
			если mode = ModeWriter то w.пСимв8(x);
			аесли (mode = ModeFile) и (f # НУЛЬ ) то w.пСимв8(x) всё
иначе (*
			IF f # NIL THEN Streams.Write(w, x) END
*) всё
		кон InternalChar;

		проц {перекрыта}Char*(x: симв8);
		нач {единолично}
			InternalChar(x)
		кон Char;

		проц {перекрыта}InternalString*(перем x: массив из симв8);
		перем i : цел32;
		нач
			нцПока (i < длинаМассива(x)) и (x[i] # 0X) делай InternalChar(x[i]); увел(i) кц
		кон InternalString;

		проц {перекрыта}String*(x: массив из симв8);
		нач {единолично}
			InternalString(x)
		кон String;

		проц {перекрыта}Hex*(x, w: цел32);
		перем i, j: цел32; buf: массив 10 из симв8;
		нач {единолично}
			если w >= 0 то j := 8 иначе j := 2; w := -w всё;
			нцДля i := j+1 до w делай InternalChar(" ") кц;
			нцДля i := j-1 до 0 шаг -1 делай
				buf[i] := симв8ИзКода(x остОтДеленияНа 10H + 48);
				если buf[i] > "9" то
					buf[i] := симв8ИзКода(кодСимв8(buf[i]) - 48 + 65 - 10)
				всё;
				x := x DIV 10H
			кц;
			buf[j] := 0X;
			InternalString(buf)
		кон Hex;

		проц {перекрыта}Int*(x: цел64; w: цел32);
		перем i: цел32; x0: цел64; a: массив 21 из симв8;
		нач {единолично}
			если x < 0 то
				если x = матМинимум( цел64 ) то
					умень(w, 20);
					нцПока w > 0 делай Char(" "); умень(w) кц;
					 a := "-9223372036854775808"; InternalString(a);
					возврат
				иначе
					умень(w); x0 := -x
				всё
			иначе
				x0 := x
			всё;
			i := 0;
			нцДо
				a[i] := симв8ИзКода(x0 остОтДеленияНа 10 + 30H); x0 := x0 DIV 10; увел(i)
			кцПри x0 = 0;
			нцПока w > i делай InternalChar(" "); умень(w) кц;
			если x < 0 то InternalChar("-") всё;
			нцДо умень(i); InternalChar(a[i]) кцПри i = 0
		кон Int;

		проц {перекрыта}TimeStamp*;
		тип TimeDate = запись h, m, s, day,month,year: цел32 кон;
			перем s : массив 32 из симв8;
				now : TimeDate;

			проц LZ(v, len: цел32; перем s: массив из симв8; перем pos: цел32);
			перем i: цел32;
			нач
				нцДля i := 1 до len делай s[pos+len-i] := симв8ИзКода(кодСимв8("0")+v остОтДеленияНа 10); v := v DIV 10 кц;
				увел(pos, len)
			кон LZ;

			проц GetTime(перем dt: TimeDate);
			нач
				Clock.Get(dt.h, dt.year);
				dt.s := dt.h остОтДеленияНа 64; dt.h := dt.h DIV 64;
				dt.m := dt.h остОтДеленияНа 64; dt.h := dt.h DIV 64;
				dt.h := dt.h остОтДеленияНа 24;
				dt.day := dt.year остОтДеленияНа 32; dt.year := dt.year DIV 32;
				dt.month := dt.year остОтДеленияНа 16; dt.year := dt.year DIV 16;
				увел(dt.year, 1900)
			кон GetTime;

			проц TimeDateToStr(dt: TimeDate; перем s: массив из симв8);
			перем p: цел32;
			нач
				LZ(dt.day, 2, s, p); s[p] := "."; увел(p);
				LZ(dt.month, 2, s, p); s[p] := "."; увел(p);
				LZ(dt.year, 2, s, p); s[p] := " "; увел(p);
				LZ(dt.h, 2, s, p); s[p] := ":"; увел(p);
				LZ(dt.m, 2, s, p); s[p] := ":"; увел(p);
				LZ(dt.s, 2, s, p); s[p] := 0X
			кон TimeDateToStr;

		нач
			GetTime(now);
			TimeDateToStr(now, s);
			InternalString(s); InternalChar(" ")
		кон TimeStamp;

		проц {перекрыта}Close*;
		нач
			если f # НУЛЬ то Files.Register(f)
			всё
		кон Close;

	кон Log;

конст
	CollCh * = "/";
тип
	Link * = окласс (* POINTER TO RECORD *)
		 перем next * : Link;
		проц &link*;
			нач next := НУЛЬ;
			кон link;
		проц add * (new: Link);
	   	 перем old: Link;
			нач old := сам;
				нцПока old.next # НУЛЬ делай old := old.next;  кц;
				old.next := new;
			кон add;
	кон Link;
(* Simple list of strings which allow appending and iteration.  Usage:  *)
конст
	LineLen * = 128;
тип
	Line * = массив LineLen из симв8;
	Lines * = окласс (* POINTER TO RECORD *)
		перем
			line * : Line;
			next * : Lines;

		проц &init*;
			нач line[0] := 0X; next := сам;
			кон init;

		(** Append a new line at the end. *)
		проц add * (конст line: массив из симв8);
		    перем old, new: Lines;
			нач old := сам;
				если old = old.next то (* first entry *)
					копируйСтрокуДо0(line, old.line); old.next := НУЛЬ;
				иначе (* additional entry *)
					нов(new); копируйСтрокуДо0(line, new.line); new.next := НУЛЬ;
					нцПока old.next # НУЛЬ делай old := old.next;  кц;
					old.next := new;
				всё;
			кон add;
		(** Find lines which also are in SELF. *)
		проц in * (target: Lines): Lines;
		    перем in, self: Lines;
			нач
				нов(in);
				нцПока target # НУЛЬ делай
					self := сам;
					нц
						если self = НУЛЬ то прервиЦикл; всё;
						если self.line = target.line то in.add(target.line); прервиЦикл; всё;
						self := self.next;
					кц;
					target := target.next;
				кц;
				если in = in.next то in := НУЛЬ; всё;
				возврат in;
			кон in;

		(** Find lines which aren't  in SELF. *)
		проц notIn * (target: Lines): Lines;
		    перем notIn, self: Lines;
			нач
				нов(notIn);
				нцПока target # НУЛЬ делай
					self := сам;
					нц
						если self = НУЛЬ то notIn.add(target.line); прервиЦикл; всё;
						если self.line = target.line то прервиЦикл; всё;
						self := self.next;
					кц;
					target := target.next;
				кц;
				если notIn = notIn.next то notIn := НУЛЬ; всё;
				возврат notIn;
			кон notIn;
	кон Lines;

Dict *= окласс
	перем
		key* : массив 64 из симв8;
		value* : динамическиТипизированныйУкль;
		next* : Dict;

	проц &Init*;
	нач
		key := "";
		next := НУЛЬ;
	кон Init;

	проц EqualsI(перем buf: массив из симв8; with: массив из симв8): булево;
	перем j: цел32;
	нач
		j := 0; нцПока (with[j] # 0X) и (ASCII_вЗаглавную(buf[j]) = ASCII_вЗаглавную(with[j])) делай увел(j) кц;
		возврат ASCII_вЗаглавную(with[j]) = ASCII_вЗаглавную(buf[j])
	кон EqualsI;

	проц has*(fieldName: массив из симв8) : булево;
	перем af: Dict;
	нач
		af := сам;
		нцПока (af # НУЛЬ) и (~EqualsI(af.key, fieldName)) делай af := af.next кц;
		возврат af # НУЛЬ
	кон has;

	проц get*(fieldName: массив из симв8; перем value : динамическиТипизированныйУкль) : булево;
	перем af: Dict;
	нач
		af := сам;
		нцПока (af # НУЛЬ) и (~EqualsI(af.key, fieldName)) делай af := af.next кц;
		если af # НУЛЬ то
			value := af.value;
			возврат истина
		иначе
			возврат ложь
		всё
	кон get;

	проц set*(fieldName: массив из симв8;  value: динамическиТипизированныйУкль);
	перем a: Dict;
	нач
		если сам.key = "" то копируйСтрокуДо0(fieldName, key); всё; (* Use also anchor. *)
		a := сам; нцПока (a.next # НУЛЬ) и (a.key # fieldName) делай a := a.next кц;
		если (a.key # fieldName) то
			нов(a.next); a := a.next
		всё;
		копируйСтрокуДо0(fieldName, a.key); a.value := value;
	кон set;
кон Dict;

перем
	MsgLog * : Log; (* If # NIL use it for Msg<n> *)

(*****************  Procedures  ************************)
(* Add a trailing '/' *)
проц padColl*(перем conf: массив из симв8);
нач
	если conf = "" то
		копируйСтрокуДо0(CollCh, conf);
	аесли conf[Strings.Length(conf)-1] # CollCh то
		Strings.Append(conf, CollCh);
	всё;
кон padColl;

(* Remove a trailing '/' *)
проц unpadColl*(перем conf: массив из симв8);
нач
	если conf # "" то
		если (conf[Strings.Length(conf)-1] = CollCh) то
			conf[Strings.Length(conf)-1] := 0X;
		всё;
	всё;
кон unpadColl;
(* don't write anything. Just for counting what's written to a writer. *)
проц Dev0 * (конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
нач кон Dev0;



(* Some utility procedures to write a couple of strings. with a linefeed. A blank is inserted automatically.
	To concat two string on the output add a "" parameter as glue in between. *)
проц I * (i: цел32): Array8;
перем s: Array8;
нач
	Strings.IntToStr(i, s);
	возврат s;
кон I;

проц B * (b: булево): Array8;
перем s: Array8;
нач
	если b то s := "TRUE";
	иначе s := "FALSE"; всё;
	возврат s;
кон B;

проц msg1(конст s1: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		MsgLog.String(s1);
	иначе
		ЛогЯдра.пСтроку8(s1);
	всё;
кон msg1;
проц Msg1 * (конст s1: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		MsgLog.Enter; msg1(s1); MsgLog.Exit;
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; msg1(s1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	всё;
кон Msg1;

проц msg2(конст s1,s2: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		если s1 # "" то MsgLog.String(s1); MsgLog.Char(' '); всё; msg1(s2);
	иначе
		если s1 # "" то ЛогЯдра.пСтроку8(s1); ЛогЯдра.пСимв8(' '); всё; msg1(s2);
	всё;
кон msg2;
проц Msg2*(конст s1,s2: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		MsgLog.Enter; msg2(s1, s2); MsgLog.Exit;
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; msg2(s1,s2); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	всё;
кон Msg2;

проц msg3(конст s1,s2,s3: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		если s1 # "" то MsgLog.String(s1); MsgLog.Char(' '); всё; msg2(s2,s3);
	иначе
		если s1 # "" то ЛогЯдра.пСтроку8(s1); ЛогЯдра.пСимв8(' '); всё; msg2(s2,s3);
	всё;
кон msg3;
проц Msg3*(конст s1,s2,s3: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		MsgLog.Enter; msg3(s1, s2, s3); MsgLog.Exit;
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; msg3(s1,s2,s3); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	всё;
кон Msg3;

проц msg4(конст s1,s2,s3,s4: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		если s1 # "" то MsgLog.String(s1); MsgLog.Char(' '); всё; msg3(s2,s3,s4);
	иначе
		если s1 # "" то ЛогЯдра.пСтроку8(s1); ЛогЯдра.пСимв8(' '); всё; msg3(s2,s3,s4);
	всё;
кон msg4;
проц Msg4*(конст s1,s2,s3,s4: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		MsgLog.Enter; msg4(s1, s2, s3, s4); MsgLog.Exit;
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; msg4(s1,s2,s3,s4); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	всё;
кон Msg4;

проц msg5(конст s1,s2,s3,s4,s5: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		если s1 # "" то MsgLog.String(s1); MsgLog.Char(' '); всё; msg4(s2,s3,s4,s5);
	иначе
		если s1 # "" то ЛогЯдра.пСтроку8(s1); ЛогЯдра.пСимв8(' '); всё; msg4(s2,s3,s4,s5);
	всё;
кон msg5;
проц Msg5*(конст s1,s2,s3,s4,s5: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		MsgLog.Enter; msg5(s1, s2, s3, s4, s5); MsgLog.Exit;
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; msg5(s1,s2,s3,s4,s5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	всё;
кон Msg5;

проц msg6(конст s1,s2,s3,s4,s5,s6: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		если s1 # "" то MsgLog.String(s1); MsgLog.Char(' '); всё; msg5(s2,s3,s4,s5,s6);
	иначе
		если s1 # "" то ЛогЯдра.пСтроку8(s1); ЛогЯдра.пСимв8(' '); всё; msg5(s2,s3,s4,s5,s6);
	всё;
кон msg6;

проц Msg6*(конст s1,s2,s3,s4,s5,s6: массив из симв8);
нач
	если MsgLog # НУЛЬ то
		MsgLog.Enter; msg6(s1, s2, s3, s4, s5, s6); MsgLog.Exit;
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; msg6(s1,s2,s3,s4,s5,s6); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	всё;
кон Msg6;

нач
	MsgLog := НУЛЬ;
кон OdUtil.

System.Free OdUtil ~
