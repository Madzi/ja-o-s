(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль VNC; (** AUTHOR "pjm/jkreienb"; PURPOSE "VNC client"; *)

(*
VNC viewer for Aos - based on Oberon VNC viewer by Jörg Kreienbühl.
This version is based on the window manager.

References:
1. Tristan Richardson and Kenneth R. Wood, "The RFB Protocol: Version 3.3", ORL, Cambridge, January 1998
*)

использует НИЗКОУР, Потоки, ЛогЯдра, Objects, Commands, Network, IP, TCP, DNS, DES,
	Inputs, Raster, WMWindowManager, Rect := WMRectangles, Dialogs := WMDialogs, Beep, Files;

конст
	OpenTimeout = 10000;
	CloseTimeout = 2000;
	PollTimeout = 0;	(* set to 0 for old-style polling on every received event *)
	Shared = истина;

	AlphaCursor = 128;

	InBufSize = 8192;	(* network input buffer *)
	OutBufSize = 4096;	(* network output buffer *)
	ImgBufSize = 8192;	(* image buffer for ReceiveRaw *)

	BellDelay = 20;	(* ms *)
	BellFreq = 550;	(* Hz *)

	Trace = ложь;
	TraceVisual = истина;
	TraceAudio = ложь;

	Ok = TCP.Ok;

тип
	Connection* = укль на запись
		next: Connection;	(* link in connection pool *)
		pcb: TCP.Connection;
		w: Window;
		res: целМЗ;
		id: цел32;
		receiver: Receiver;
		sender: Sender;
		nb: Raster.Image;
		fmt: Raster.Format;	(* network transfer format *)
		mode: Raster.Mode;
		bytesPerPixel: цел32;	(* network transfer format size *)
		rcvbuf, imgbuf: укль на массив из симв8;
		rcvbufpos, rcvbuflen: размерМЗ;
		fip: IP.Adr
	кон;

тип
	EnumProc = проц (c: Connection; out : Потоки.Писарь);

	ConnectionPool = окласс
		перем head, tail: Connection; id: цел32;

		проц Empty(): булево;
		нач	(* read head pointer atomically *)
			возврат head = НУЛЬ
		кон Empty;

		проц Add(c: Connection);
		нач {единолично}
			c.next := НУЛЬ; c.id := id; увел(id);
			если head = НУЛЬ то head := c иначе tail.next := c всё;
			tail := c
		кон Add;

		проц Remove(c: Connection);
		перем p, q: Connection;
		нач {единолично}
			p := НУЛЬ; q := head;
			нцПока (q # НУЛЬ) и (q # c) делай p := q; q := q.next кц;
			если q = c то	(* found *)
				если p # НУЛЬ то p.next := q.next иначе head := НУЛЬ; tail := НУЛЬ всё
			всё
		кон Remove;

		проц Enumerate(p: EnumProc; out : Потоки.Писарь);
		перем c: Connection;
		нач	(* may traverse list concurrently with Add and Remove *)
			c := head; нцПока c # НУЛЬ делай p(c, out); c := c.next кц
		кон Enumerate;

		проц Find(id: цел32): Connection;
		перем c: Connection;
		нач	(* may traverse list concurrently with Add and Remove *)
			c := head; нцПока (c # НУЛЬ) и (c.id # id) делай c := c.next кц;
			возврат c
		кон Find;

		проц &Init*;
		нач
			head := НУЛЬ; tail := НУЛЬ; id := 0
		кон Init;

	кон ConnectionPool;

тип
	Window = окласс (WMWindowManager.BufferWindow)
		перем sender: Sender;

		проц {перекрыта}PointerDown*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		нач
			если sender # НУЛЬ то sender.Pointer(x, y, keys) всё
		кон PointerDown;

		проц {перекрыта}PointerMove*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		нач
			если sender # НУЛЬ то sender.Pointer(x, y, keys) всё
		кон PointerMove;

		проц {перекрыта}WheelMove*(dz : размерМЗ);
		нач
			если sender # НУЛЬ то sender.Wheel(dz) всё
		кон WheelMove;

		проц {перекрыта}PointerUp*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		нач
			если sender # НУЛЬ то sender.Pointer(x, y, keys) всё
		кон PointerUp;

		проц {перекрыта}KeyEvent*(ucs : размерМЗ; flags: мнвоНаБитахМЗ; keysym: размерМЗ);
		нач
			если (keysym # Inputs.KsNil) и (sender # НУЛЬ) то sender.Key(keysym, flags) всё
		кон KeyEvent;

		проц {перекрыта}Close*;
		нач
			если sender # НУЛЬ то CloseVNC(sender.c) всё
		кон Close;

	кон Window;

тип
	Receiver = окласс
		перем c: Connection; exception, double: булево;

		проц &Init*(c: Connection);
		нач
			сам.c := c; exception := ложь; double := ложь
		кон Init;

	нач {активное, SAFE}
		если exception то
			если истина или Trace то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Receiver exception"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё;
			если double то возврат всё;
			double := истина
		иначе
			exception := истина;
			если Trace то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Receiver enter"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё;
			нцДо
				если (PollTimeout = 0) и (c.sender # НУЛЬ) то c.sender.HandleTimeout всё;
				AwaitResponse(c)
			кцПри c.res # Ok;
			если Trace то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Receiver exit"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё
		всё;
		если c.sender # НУЛЬ то c.sender.Terminate всё;
		если c.w # НУЛЬ то
			c.w.manager.Remove(c.w);
			c.w := НУЛЬ
		всё
	кон Receiver;

тип
	Sender = окласс
		перем
			c: Connection;
			head, middle, tail, lx, ly: размерМЗ;
			res: целМЗ;
			lkeys : мнвоНаБитахМЗ;
			buf: массив OutBufSize из симв8;
			done, poll: булево;
			timer: Objects.Timer;

		проц Available(): размерМЗ;
		нач
			возврат (head - tail - 1) остОтДеленияНа длинаМассива(buf)
		кон Available;

		проц Put(x: симв8);
		нач
			утв((tail+1) остОтДеленияНа длинаМассива(buf) # head);
			buf[tail] := x; tail := (tail+1) остОтДеленияНа длинаМассива(buf)
		кон Put;

		проц PutInt(x: цел32);
		нач
			Put(симв8ИзКода(x DIV 100H)); Put(симв8ИзКода(x остОтДеленияНа 100H))
		кон PutInt;

		проц Pointer(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		нач {единолично}
			если (x >= 0) и (x < c.w.img.width) и (y >= 0) и (y < c.w.img.height) и (Available() >= 6) то
				если Trace то
					ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Ptr "); ЛогЯдра.пЦел64(x, 5); ЛогЯдра.пЦел64(y, 5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё;
				Put(5X);	(* PointerEvent (sec. 5.2.6) *)
				Put(симв8ИзКода(цел32(keys)));
				PutInt(x(цел32)); PutInt(y(цел32));
				lx := x; ly := y; lkeys := keys
			всё
		кон Pointer;

		проц Wheel(dz : размерМЗ);
		перем keys : мнвоНаБитахМЗ;
		нач {единолично}
			если (Available() >= 6) то
				если Trace то
					ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Wheel "); ЛогЯдра.пЦел64(dz, 5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё;
				Put(5X);	(* PointerEvent (sec. 5.2.6) *)
				keys := lkeys;
				если dz < 0 то включиВоМнвоНаБитах(keys, 3) всё;
				если dz > 0 то включиВоМнвоНаБитах(keys, 4) всё;
				Put(симв8ИзКода(цел32(keys)));
				PutInt(lx(цел32)); PutInt(ly(цел32))
			всё
		кон Wheel;

		проц Key(keysym: размерМЗ; flags: мнвоНаБитахМЗ);
		нач {единолично}
			если Available() >= 8 то
				Put(4X);	(* KeyEvent (sec. 5.2.5) *)
				если Inputs.Release в flags то Put(0X) иначе Put(1X) всё;
				PutInt(0); PutInt(0); PutInt(keysym(цел32))
			всё
		кон Key;

		проц Paste(r: Потоки.Чтец);
		перем key: цел32;
		нач {единолично}
			нц
				key := кодСимв8(r.чИДайСимв8());
				если r.кодВозвратаПоследнейОперации # 0 то прервиЦикл всё;
				дождись(Available() >= 16);
					(* down key *)
				Put(4X);	(* KeyEvent (sec. 5.2.5) *)
				Put(1X); PutInt(0); PutInt(0); PutInt(key);
					(* up key *)
				Put(4X);	(* KeyEvent (sec. 5.2.5) *)
				Put(0X); PutInt(0); PutInt(0); PutInt(key)
			кц
		кон Paste;

		проц AwaitEvent;
		нач {единолично}
			дождись((head # tail) или poll или done);
			если ~done и (Available() >= 10) то
				Put(3X);	(* FramebufferUpdateRequest (sec. 5.2.4) *)
				Put(1X);	(* incremental *)
				PutInt(0); PutInt(0); PutInt(c.w.img.width(цел32)); PutInt(c.w.img.height(цел32))
			всё;
			middle := tail; poll := ложь
		кон AwaitEvent;

		проц SendEvents;
		нач
			если middle >= head то
				c.pcb.ЗапишиВПоток(buf, head, middle-head, ложь, res)
			иначе	(* split buffer *)
				c.pcb.ЗапишиВПоток(buf, head, длинаМассива(buf)-head, ложь, res);
				если res = Ok то c.pcb.ЗапишиВПоток(buf, 0, middle, ложь, res) всё
			всё;
			head := middle
		кон SendEvents;

		проц Terminate;
		нач {единолично}
			done := истина
		кон Terminate;

		проц HandleTimeout;
		нач {единолично}
			poll := истина;
			если (PollTimeout > 0) и ~done то
				Objects.SetTimeout(timer, сам.HandleTimeout, PollTimeout)
			всё
		кон HandleTimeout;

		проц &Init*(c: Connection);
		нач
			нов(timer);
			сам.c := c; head := 0; middle := 0; tail := 0; res := Ok; done := ложь
		кон Init;

	нач {активное}
		если Trace то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Sender enter"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё;
		нц
			AwaitEvent;
			если done то прервиЦикл всё;
			если TraceAudio то Beep.Beep(BellFreq) всё;
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Events "); ЛогЯдра.пЦел64(head, 5); ЛогЯдра.пЦел64(middle, 5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			SendEvents;
			если TraceAudio то Beep.Beep(0) всё;
			если res # Ok то прервиЦикл всё
		кц;
		Objects.CancelTimeout(timer);
		если Trace то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Sender exit"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё
	кон Sender;

тип
	Bell = окласс
		перем timer: Objects.Timer;

		проц Ring;
		нач {единолично}
			если timer = НУЛЬ то нов(timer) всё;
			Objects.SetTimeout(timer, сам.HandleTimeout, BellDelay);	(* ignore race with expired, but unscheduled timer *)
			Beep.Beep(BellFreq)
		кон Ring;

		проц HandleTimeout;
		нач {единолично}
			Beep.Beep(0)
		кон HandleTimeout;

	кон Bell;

перем
	pool: ConnectionPool;
	bell: Bell;

проц ReceiveBytes(c: Connection; перем buf: массив из симв8; size: размерМЗ; перем len: цел32);
перем dst, n: размерМЗ;
нач
	если c.res = Ok то
		dst := 0; len := 0;
		нц
			если size <= 0 то прервиЦикл всё;
			n := матМинимум(c.rcvbuflen, size);	(* n is number of bytes to copy from buffer now *)
			если n = 0 то	(* buffer empty *)
					(* attempt to read at least size bytes, but at most a full buffer *)
				c.pcb.ПрочтиИзПотока(c.rcvbuf^, 0, длинаМассива(c.rcvbuf), size, n, c.res);
				если c.res # Ok то прервиЦикл всё;
				c.rcvbufpos := 0; c.rcvbuflen := n;
				n := матМинимум(n, size)	(* n is number of bytes to copy from buffer now *)
			всё;
			утв(dst+n <= длинаМассива(buf));	(* index check *)
			НИЗКОУР.копируйПамять(адресОт(c.rcvbuf[c.rcvbufpos]), адресОт(buf[dst]), n);
			увел(c.rcvbufpos, n); умень(c.rcvbuflen, n);
			увел(dst, n); умень(size, n); увел(len, n(цел32))
		кц
	иначе
		buf[0] := 0X; len := 0
	всё
кон ReceiveBytes;

проц Receive(c: Connection; перем ch: симв8);
перем len: цел32; buf: массив 1 из симв8;
нач
	если c.rcvbuflen > 0 то
		ch := c.rcvbuf[c.rcvbufpos]; увел(c.rcvbufpos); умень(c.rcvbuflen)
	иначе
		ReceiveBytes(c, buf, 1, len);
		ch := buf[0]
	всё
кон Receive;

проц ReceiveInt(c: Connection; перем x: цел32);
перем len: цел32; buf: массив 2 из симв8;
нач
	ReceiveBytes(c, buf, 2, len);
	x := Network.GetNet2(buf, 0)
кон ReceiveInt;

проц ReceiveLInt(c: Connection; перем x: цел32);
перем len: цел32; buf: массив 4 из симв8;
нач
	ReceiveBytes(c, buf, 4, len);
	x := Network.GetNet4(buf, 0)
кон ReceiveLInt;

проц ReceiveIgnore(c: Connection; len: цел32);
перем ch: симв8;
нач
	нцПока (len > 0) и (c.res = Ok) делай Receive(c, ch); умень(len) кц
кон ReceiveIgnore;

проц Send(c: Connection; x: симв8);
перем buf: массив 1 из симв8;
нач
	buf[0] := x; c.pcb.ЗапишиВПоток(buf, 0, 1, ложь, c.res)
кон Send;


(* Get the server's version number and send our version number. *)

проц DoVersion(c: Connection): булево;
перем buf: массив 16 из симв8; len: цел32;
нач
	ReceiveBytes(c, buf, 12, len);
	если c.res = Ok то
		если Trace то
			buf[11] := 0X;
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Version="); ЛогЯдра.пСтроку8(buf); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё;
		buf := "RFB 003.003"; buf[11] := 0AX;
		c.pcb.ЗапишиВПоток(buf, 0, 12, ложь, c.res)
	всё;
	возврат c.res = Ok
кон DoVersion;

(* Authenticate ourself with the server. *)

проц DoAuthentication(c: Connection; перем pwd: массив из симв8): булево;
перем x, len, len0: цел32; buf: массив 64 из симв8; cipher: массив 16 из симв8; d: DES.DES;
нач
	ReceiveLInt(c, x);
	если c.res = Ok то
		если Trace то
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Scheme="); ЛогЯдра.пЦел64(x, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё;
		если x = 0 то	(* failed *)
			ReceiveLInt(c, len);	(* read reason *)
			нцПока (len > 0) и (c.res = Ok) делай
				len0 := матМинимум(len, длинаМассива(buf));
				ReceiveBytes(c, buf, len0, len0);
				умень(len, len0)
			кц;
			если Trace и (c.res = Ok) то	(* write last part of reason (typically only one part) *)
				если len0 = длинаМассива(buf) то умень(len0) всё;
				buf[len0] := 0X;
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Reason="); ЛогЯдра.пСтроку8(buf); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё
		аесли x = 2 то	(* VNC authentication *)
			ReceiveBytes(c, buf, 16, len);	(* challenge *)
			если c.res = Ok то
				нов(d);
				d.SetKey(pwd);
				d.Encrypt(buf, 0, cipher, 0);	(* Two 8-Byte-Blocks *)
				d.Encrypt(buf, 8, cipher, 8);
				c.pcb.ЗапишиВПоток(cipher, 0, 16, ложь, c.res);
				если c.res = Ok то
					ReceiveLInt(c, x);
					если c.res = Ok то
						c.res := x	(* 0=Ok, 1=failed, 2=too-many *)
					всё
				всё
			всё
		иначе	(* no or unknown authentication *)
			(* skip *)
		всё
	всё;
	возврат c.res = Ok
кон DoAuthentication;

(* Set up an RFB encodings message.  "code" contains the codes in preferred order.  "len" returns the message length. *)

проц PutEncodings(перем buf: массив из симв8; ofs: цел32; code: массив из симв8; перем len: цел32);
перем i: цел32;
нач
	buf[ofs] := 2X;	(* SetEncodings (sec. 5.2.3) *)
	buf[ofs+1] := 0X;	(* padding *)
	i := 0;
	нцПока code[i] # 0X делай
		Network.PutNet4(buf, ofs + 4*(i+1), кодСимв8(code[i])-кодСимв8("0"));
		увел(i)
	кц;
	Network.PutNet2(buf, ofs+2, i);	(* number-of-encodings *)
	len := 4*(i+1)
кон PutEncodings;

(* Initialise the transfer format. *)

проц DoInit(c: Connection): булево;
перем len, len0, w, h: цел32; buf: массив 64 из симв8; pixel: Raster.Pixel; ptr: WMWindowManager.PointerInfo;
нач
	если Shared то Send(c, 1X) иначе Send(c, 0X) всё;
	если c.res = Ok то
		ReceiveBytes(c, buf, 24, len);	(* initialization message *)
		если c.res = Ok то
			w := Network.GetNet2(buf, 0); h := Network.GetNet2(buf, 2);
			len := Network.GetNet4(buf, 20);
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Server: width="); ЛогЯдра.пЦел64(w, 1);
				ЛогЯдра.пСтроку8(" height="); ЛогЯдра.пЦел64(h, 1);
				ЛогЯдра.пСтроку8(" bpp="); ЛогЯдра.пЦел64(кодСимв8(buf[4]), 1);
				ЛогЯдра.пСтроку8(" depth="); ЛогЯдра.пЦел64(кодСимв8(buf[5]), 1);
				ЛогЯдра.пСтроку8(" bigendian="); ЛогЯдра.пЦел64(кодСимв8(buf[6]), 1);
				ЛогЯдра.пСтроку8(" truecolor="); ЛогЯдра.пЦел64(кодСимв8(buf[7]), 1); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8(" redmax="); ЛогЯдра.пЦел64(Network.GetNet2(buf, 8), 1);
				ЛогЯдра.пСтроку8(" greenmax="); ЛогЯдра.пЦел64(Network.GetNet2(buf, 10), 1);
				ЛогЯдра.пСтроку8(" bluemax="); ЛогЯдра.пЦел64(Network.GetNet2(buf, 12), 1);
				ЛогЯдра.пСтроку8(" redshift="); ЛогЯдра.пЦел64(кодСимв8(buf[14]), 1);
				ЛогЯдра.пСтроку8(" greenshift="); ЛогЯдра.пЦел64(кодСимв8(buf[15]), 1);
				ЛогЯдра.пСтроку8(" blueshift="); ЛогЯдра.пЦел64(кодСимв8(buf[16]), 1);
				ЛогЯдра.пСтроку8(" len="); ЛогЯдра.пЦел64(len, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			нцПока (len > 0) и (c.res = Ok) делай
				len0 := матМинимум(len, длинаМассива(buf));
				ReceiveBytes(c, buf, len0, len0);
				умень(len, len0)
			кц;
			если c.res = Ok то
				если Trace то	(* write last part of name (typically only one part) *)
					если len0 = длинаМассива(buf) то умень(len0) всё;
					buf[len0] := 0X;
					ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Name="); ЛогЯдра.пСтроку8(buf); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё;
					(* choose our preferred format *)
				Raster.InitMode(c.mode, Raster.srcCopy);
				нов(c.w, w, h, ложь);

				нов(ptr); ptr.hotX := 2; ptr.hotY := 2;
				нов(ptr.img); Raster.Create(ptr.img, 4, 4, Raster.BGRA8888);
				Raster.SetRGBA(pixel, 255, 255, 255, AlphaCursor);
				Raster.Fill(ptr.img, 0, 0, 4, 4, pixel, c.mode);
				Raster.SetRGBA(pixel, 0, 0, 0, AlphaCursor);
				Raster.Fill(ptr.img, 1, 1, 3, 3, pixel, c.mode);
				c.w.SetPointerInfo(ptr);

				WMWindowManager.DefaultAddWindow(c.w);

				Raster.SetRGB(pixel, 0, 0, 0);
				Raster.Fill(c.w.img, 0, 0, c.w.img.width, c.w.img.height, pixel, c.mode);
				c.w.Invalidate(Rect.MakeRect(0, 0, c.w.img.width, c.w.img.height));
				нов(c.nb);
				если c.w.img.fmt.code в {Raster.bgr888, Raster.bgra8888} то
					c.fmt := Raster.BGRA8888
				иначе
					c.fmt := Raster.BGR565
				всё;
				c.bytesPerPixel := c.fmt.bpp DIV 8;
				утв(ImgBufSize >= w*c.bytesPerPixel);	(* at least one full line will fit buffer *)
				нов(c.imgbuf, ImgBufSize);
					(* set up client format message *)
				buf[0] := 0X;	(* SetPixelFormat message (sec. 5.2.1) *)
				buf[1] := 0X; buf[2] := 0X; buf[3] := 0X;	(* padding *)
				buf[4] := симв8ИзКода(c.bytesPerPixel*8);	(* bits-per-pixel (8, 16 or 32) on wire *)
				buf[5] := симв8ИзКода(c.fmt.bpp);	(* depth (8, 16, 24 or 32) *)
				buf[6] := 0X;	(* big-endian-flag *)
				buf[7] := 1X;	(* true-colour-flag *)
				просей c.fmt.code из
					Raster.bgr565:
						Network.PutNet2(buf, 8, 31);	(* red-max *)
						Network.PutNet2(buf, 10, 63);	(* green-max *)
						Network.PutNet2(buf, 12, 31);	(* blue-max *)
						buf[14] := симв8ИзКода(11);	(* red-shift *)
						buf[15] := симв8ИзКода(5);	(* green-shift *)
						buf[16] := симв8ИзКода(0)	(* blue-shift *)
					|Raster.bgra8888:
						Network.PutNet2(buf, 8, 255);	(* red-max *)
						Network.PutNet2(buf, 10, 255);	(* green-max *)
						Network.PutNet2(buf, 12, 255);	(* blue-max *)
						buf[14] := симв8ИзКода(16);	(* red-shift *)
						buf[15] := симв8ИзКода(8);	(* green-shift *)
						buf[16] := симв8ИзКода(0)	(* blue-shift *)
				всё;
				PutEncodings(buf, 20, "15420", len);	(* 0=raw, 1=copy rectangle, 2=RRE, 4=CoRRE, 5=hextile *)
				если Trace то
					ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Client:"); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСрезБуфера16рично(buf, 0, 20+len); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё;
				c.pcb.ЗапишиВПоток(buf, 0, 20+len, ложь, c.res)
			всё
		всё
	всё;
	возврат c.res = Ok
кон DoInit;

(* Send a framebuffer update request. *)

проц SendRequest(c: Connection; inc: булево; x, y, w, h: размерМЗ);
перем buf: массив 10 из симв8;
нач
	если Trace то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Req"); ЛогЯдра.пЦел64(x, 5); ЛогЯдра.пЦел64(y, 5);
		ЛогЯдра.пЦел64(w, 5); ЛогЯдра.пЦел64(h, 5);
		если inc то ЛогЯдра.пСтроку8(" inc") всё;
		ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
	buf[0] := 3X;	(* FramebufferUpdateRequest (sec. 5.2.4) *)
	если inc то buf[1] := 1X иначе buf[1] := 0X всё;
	Network.PutNet2(buf, 2, x(цел32)); Network.PutNet2(buf, 4, y(цел32));
	Network.PutNet2(buf, 6, w(цел32)); Network.PutNet2(buf, 8, h(цел32));
	c.pcb.ЗапишиВПоток(buf, 0, 10, ложь, c.res)
кон SendRequest;

(* Update an area of the display. *)

проц UpdateDisplay(c: Connection; x, y, w, h: цел32);
(*VAR pixel: Raster.Pixel; mode: Raster.Mode;*)
нач
(*
	Raster.SetRGB(pixel, 255, 255, 255);
	Raster.InitMode(mode, Raster.InvDst);
	Raster.Fill(c.w.img, 0, 0, 5, 5, pixel, mode);
	IF (x # 0) OR (y # 0) THEN c.w.AddDirty(0, 0, 10, 10) END;
*)
	c.w.Invalidate(Rect.MakeRect(x, y, x + w, y + h))
кон UpdateDisplay;

(* Receive a raw rectangle. *)

проц ReceiveRaw(c: Connection; x, y, w, h: цел32);
перем bh, h0, len, i: цел32;
нач
	если Trace то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Raw"); ЛогЯдра.пЦел64(x, 5); ЛогЯдра.пЦел64(y, 5);
		ЛогЯдра.пЦел64(w, 5); ЛогЯдра.пЦел64(h, 5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
	bh := (длинаМассива(c.imgbuf^) DIV (w*c.bytesPerPixel))(цел32);	(* number of lines that will fit in buffer *)
	Raster.Init(c.nb, w, bh, c.fmt, w*c.bytesPerPixel, адресОт(c.imgbuf[0]));
	нцПока h > 0 делай
		если h >= bh то h0 := bh иначе h0 := h всё;
		len := h0*w*c.bytesPerPixel;
		ReceiveBytes(c, c.imgbuf^, len, len);
		если c.res # Ok то возврат всё;
		если c.bytesPerPixel = 4 то	(* fix alpha values *)
			нцДля i := 0 до len-1 шаг 4 делай c.imgbuf[i+Raster.a] := 0FFX кц
		всё;
		Raster.Copy(c.nb, c.w.img, 0, 0, w, h0, x, y, c.mode);
		умень(h, h0); увел(y, h0)
	кц
кон ReceiveRaw;

(* Receive a copy rectangle message. *)

проц ReceiveCopyRect(c: Connection; x, y, w, h: цел32);
перем sx, sy: цел32;
нач
	ReceiveInt(c, sx);	(* src-x-position *)
	если c.res = Ok то
		ReceiveInt(c, sy);	(* src-y-position *)
		если c.res = Ok то
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Copy"); ЛогЯдра.пЦел64(x, 5); ЛогЯдра.пЦел64(y, 5);
				ЛогЯдра.пЦел64(w, 5); ЛогЯдра.пЦел64(h, 5); ЛогЯдра.пЦел64(sx, 5); ЛогЯдра.пЦел64(sy, 5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			Raster.Copy(c.w.img, c.w.img, sx, sy, sx+w, sy+h, x, y, c.mode)
		всё
	всё
кон ReceiveCopyRect;

(* Receive a pixel. *)

проц ReceivePixel(c: Connection; перем pixel: Raster.Pixel);
перем len: цел32; buf: массив 4 из симв8;
нач
	ReceiveBytes(c, buf, c.bytesPerPixel, len);
	c.fmt.unpack(c.fmt, адресОт(buf[0]), 0, pixel);
	pixel[Raster.a] := 0FFX
кон ReceivePixel;

(* Receive an RRE rectangle message. *)

проц ReceiveRRE(c: Connection; x, y, w, h: цел32);
перем n, len, sx, sy: цел32; pixel: Raster.Pixel; buf: массив 8 из симв8;
нач
	если Trace то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("RRE"); ЛогЯдра.пЦел64(x, 5); ЛогЯдра.пЦел64(y, 5);
		ЛогЯдра.пЦел64(w, 5); ЛогЯдра.пЦел64(h, 5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
	ReceiveLInt(c, n);	(* number-of-subrectangles *)
	если c.res = Ok то
		ReceivePixel(c, pixel);
		если c.res = Ok то
			Raster.Fill(c.w.img, x, y, x+w, y+h, pixel, c.mode);
			нцПока n > 0 делай
				ReceivePixel(c, pixel);
				если c.res # Ok то возврат всё;
				ReceiveBytes(c, buf, 8, len);
				если c.res # Ok то возврат всё;
				sx := x+Network.GetNet2(buf, 0); sy := y+Network.GetNet2(buf, 2);
				Raster.Fill(c.w.img, sx, sy, sx+Network.GetNet2(buf, 4), sy+Network.GetNet2(buf, 6), pixel, c.mode);
				умень(n)
			кц
		всё
	всё
кон ReceiveRRE;

(* Receive a CoRRE rectangle message. *)

проц ReceiveCoRRE(c: Connection; x, y, w, h: цел32);
перем n, len, sx, sy: цел32; pixel: Raster.Pixel; buf: массив 4 из симв8;
нач
	если Trace то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("CoRRE"); ЛогЯдра.пЦел64(x, 5); ЛогЯдра.пЦел64(y, 5);
		ЛогЯдра.пЦел64(w, 5); ЛогЯдра.пЦел64(h, 5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
	ReceiveLInt(c, n);	(* number-of-subrectangles *)
	если c.res = Ok то
		ReceivePixel(c, pixel);
		если c.res = Ok то
			Raster.Fill(c.w.img, x, y, x+w, y+h, pixel, c.mode);
			нцПока n > 0 делай
				ReceivePixel(c, pixel);
				если c.res # Ok то возврат всё;
				ReceiveBytes(c, buf, 4, len);
				если c.res # Ok то возврат всё;
				sx := x+кодСимв8(buf[0]); sy := y+кодСимв8(buf[1]);
				Raster.Fill(c.w.img, sx, sy, sx+кодСимв8(buf[2]), sy+кодСимв8(buf[3]), pixel, c.mode);
				умень(n)
			кц
		всё
	всё
кон ReceiveCoRRE;

(* Receive a hextile rectangle message. *)

проц ReceiveHextile(c: Connection; x, y, w, h: цел32);
конст
	Raw = 0; BackgroundSpecified = 1; ForegroundSpecified = 2; AnySubrects = 3; SubrectsColoured = 4;
перем
	row, col, i, tw, th, wmin, hmin, sx, sy, sw, sh: цел32;
	bg, fg, pixel: Raster.Pixel; sub: мнвоНаБитахМЗ; ch: симв8;
нач
	если Trace то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Hex"); ЛогЯдра.пЦел64(x, 5); ЛогЯдра.пЦел64(y, 5);
		ЛогЯдра.пЦел64(w, 5); ЛогЯдра.пЦел64(h, 5); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
	wmin := (w-1) остОтДеленияНа 16 + 1; hmin := (h-1) остОтДеленияНа 16 + 1;
	нцДля row := 0 до (h-1) DIV 16 делай
		если row < (h-1) DIV 16 то th := 16 иначе th := hmin всё;
		нцДля col := 0 до (w-1) DIV 16 делай
			если col < (w-1) DIV 16 то tw := 16 иначе tw := wmin всё;
			Receive(c, ch);
			если c.res # Ok то возврат всё;
			sub := мнвоНаБитахМЗ(кодСимв8(ch));
			если Raw в sub то
				ReceiveRaw(c, x + 16*col, y + 16*row, tw, th)
			иначе
				если BackgroundSpecified в sub то ReceivePixel(c, bg) всё;
				если ForegroundSpecified в sub то ReceivePixel(c, fg) всё;
				Raster.Fill(c.w.img, x + 16*col, y + 16*row, x + 16*col + tw, y + 16*row + th, bg, c.mode);
				если AnySubrects в sub то
					Receive(c, ch);
					если c.res # Ok то возврат всё;
					нцДля i := 1 до кодСимв8(ch) делай
						если SubrectsColoured в sub то ReceivePixel(c, pixel) иначе pixel := fg всё;
						Receive(c, ch);
						если c.res # Ok то возврат всё;
						sx := кодСимв8(ch) DIV 16; sy := кодСимв8(ch) остОтДеленияНа 16;
						Receive(c, ch);
						если c.res # Ok то возврат всё;
						sw := кодСимв8(ch) DIV 16 + 1; sh := кодСимв8(ch) остОтДеленияНа 16 + 1;
						Raster.Fill(c.w.img, x + 16*col + sx, y + 16*row + sy, x + 16*col + sx + sw,
							y + 16*row + sy + sh, pixel, c.mode)
					кц
				всё
			всё
		кц;
		если TraceVisual то UpdateDisplay(c, x, y + 16*row, w, th) всё
	кц
кон ReceiveHextile;

(* Receive a rectangle message. *)

проц ReceiveRectangle(c: Connection);
перем len, x, y, w, h: цел32; buf: массив 12 из симв8;
нач
	ReceiveBytes(c, buf, 12, len);
	x := Network.GetNet2(buf, 0); y := Network.GetNet2(buf, 2);
	w := Network.GetNet2(buf, 4); h := Network.GetNet2(buf, 6);
	просей Network.GetNet4(buf, 8) из	(* encoding-type *)
		0: ReceiveRaw(c, x, y, w, h)
		|1: ReceiveCopyRect(c, x, y, w, h)
		|2: ReceiveRRE(c, x, y, w, h)
		|4: ReceiveCoRRE(c, x, y, w, h)
		|5: ReceiveHextile(c, x, y, w, h)
	всё;
	UpdateDisplay(c, x, y, w, h)
кон ReceiveRectangle;

(* Receive and react on one message from the server. *)

проц AwaitResponse(c: Connection);
перем len: цел32; ch: симв8;
нач
	Receive(c, ch);
	если c.res = Ok то
		просей кодСимв8(ch) из
			0:	(* FramebufferUpdate (sec. 5.3.1) *)
				Receive(c, ch);	(* padding *)
				если c.res = Ok то ReceiveInt(c, len) всё;	(* number-of-rectangles *)
				нцПока (c.res = Ok) и (len > 0) делай
					ReceiveRectangle(c); умень(len)
				кц
			|1:	(* SetColourMapEntries (sec. 5.3.2) *)
				Receive(c, ch);	(* padding *)
				если c.res = Ok то ReceiveInt(c, len) всё;	(* first-colour *)
				если c.res = Ok то ReceiveInt(c, len) всё;	(* number-of-colours *)
				если (c.res = Ok) и (len > 0) то ReceiveIgnore(c, len*6) всё
			|2:	(* Bell (sec. 5.3.3) *)
				bell.Ring
			|3:	(* ServerCutText (sec. 5.3.4) *)
				ReceiveIgnore(c, 3);	(* padding *)
				ReceiveLInt(c, len);
				если (c.res = Ok) и (len > 0) то ReceiveIgnore(c, len) всё
		всё
	всё
кон AwaitResponse;

(* Open a VNC connection to the specified server and port. *)

проц OpenVNC*(c: Connection; server: IP.Adr; port: цел32; pwd: массив из симв8);
нач
	нов(c.pcb); c.fip := server;
	c.pcb.Open(TCP.NilPort, server, port, c.res);
	c.pcb.DelaySend(ложь);
	если c.res = Ok то
		c.pcb.AwaitState(TCP.OpenStates, TCP.ClosedStates, OpenTimeout, c.res)
	всё;
	если c.res = Ok то
		нов(c.rcvbuf, InBufSize); c.rcvbufpos := 0; c.rcvbuflen := 0;
		если DoVersion(c) и DoAuthentication(c, pwd) и DoInit(c) то
			SendRequest(c, ложь, 0, 0, c.w.img.width, c.w.img.height);
			если c.res = Ok то
				нов(c.receiver, c);
				нов(c.sender, c);
				c.w.sender := c.sender;
				если PollTimeout # 0 то c.sender.HandleTimeout всё	(* start the timer *)
			иначе
				CloseVNC(c)
			всё
		иначе
			CloseVNC(c)
		всё
	всё;
	если Trace и (c # НУЛЬ) то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("OpenVNC="); ЛогЯдра.пЦел64(c.res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё
кон OpenVNC;

(* Close a VNC connection. *)

проц CloseVNC*(перем c: Connection);
перем res: целМЗ;
нач
	pool.Remove(c);
	c.pcb.Закрой();
	c.pcb.AwaitState(TCP.ClosedStates, {}, CloseTimeout, res);
	если Trace то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("CloseVNC="); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
	(*c.pcb := NIL*)
кон CloseVNC;

проц PrintConnection(c: Connection; out : Потоки.Писарь);
перем res: целМЗ; name: массив 128 из симв8;
нач
	out.пЦел64(c.id, 1);
	просей c.fmt.code из
		Raster.bgr565:
			out.пСтроку8(" 16-bit")
		|Raster.bgra8888:
			out.пСтроку8(" 32-bit")
	всё;
	если (c.w # НУЛЬ) и (c.w.img # НУЛЬ) то
		out.пСимв8(" "); out.пЦел64(c.w.img.width, 1);
		out.пСимв8("x"); out.пЦел64(c.w.img.height, 1)
	всё;
	DNS.HostByNumber(c.fip, name, res);
	out.пСимв8(" "); out.пСтроку8(name);
	out.пВК_ПС
кон PrintConnection;

проц Show*(context : Commands.Context);
нач
	если ~pool.Empty() то
		context.out.пСтроку8("VNC connections"); context.out.пВК_ПС;
		pool.Enumerate(PrintConnection, context.out);
	иначе
		context.out.пСтроку8("No open connections"); context.out.пВК_ПС
	всё;
кон Show;

проц ReadString(r: Потоки.Чтец; перем s: массив из симв8);
перем i: цел32;
нач
	i := 0; нцПока (r.кодВозвратаПоследнейОперации = 0) и (r.ПодглядиСимв8() # " ") делай r.чСимв8(s[i]); увел(i) кц;
	s[i] := 0X; r.ПропустиБайты(1)
кон ReadString;

проц Open*(context : Commands.Context); (** server[pwd|?] port *)
перем
	server: IP.Adr; res: целМЗ; port: цел32;
	c: Connection; pwd: массив 32 из симв8; svr, title: массив 128 из симв8;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(svr); context.arg.ПропустиБелоеПоле;
	если (context.arg.ПодглядиСимв8() < "0") или (context.arg.ПодглядиСимв8() > "9") то context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(pwd) всё;
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(port, ложь);
	если (context.arg.кодВозвратаПоследнейОперации = Потоки.Успех) или (context.arg.кодВозвратаПоследнейОперации = Потоки.КонецФайла) то
		DNS.HostByName(svr, server, res);
		если (res = Ok) и (port # 0) то
			если pwd = "?" то
				если Dialogs.QueryPassword("Enter VNC Password", pwd) # Dialogs.ResOk то возврат всё
			всё;
			нов(c);
			OpenVNC(c, server, port, pwd);
			если c.res = Ok то
				pool.Add(c);
				копируйСтрокуДо0(svr, title); Files.AppendStr(" Port ", title); Files.AppendInt(port, title); Files.AppendStr(" - VNC ", title); Files.AppendInt(c.id, title);
				c.w.SetTitle(WMWindowManager.NewString(title));
				Show(context)
			иначе
				context.error.пСтроку8("Error "); context.error.пЦел64(c.res, 1); context.error.пВК_ПС
			всё
		иначе
			context.error.пСтроку8("Error: not found"); context.error.пВК_ПС
		всё
	иначе
		context.error.пСтроку8("Error: expected server[ pwd] port"); context.error.пВК_ПС
	всё;
кон Open;

проц Paste*(context : Commands.Context);	(** connection text *)
перем i: цел32; c: Connection;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(i, ложь);
	c := pool.Find(i);
	если (c # НУЛЬ) и (c.sender # НУЛЬ) то
		если context.arg.ПодглядиСимв8() = " " то context.arg.ПропустиБайты(1) всё;
		c.sender.Paste(context.arg);
	всё;
кон Paste;

нач
	нов(bell); нов(pool)
кон VNC.

VNC.Open portnoy.ethz.ch 5901 ~
VNC.Show
VNC.Paste 0 Hello world~

System.Free VNC ~
