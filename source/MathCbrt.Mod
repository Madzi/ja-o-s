(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль MathCbrt;   (** AUTHOR "adf"; PURPOSE "Compute the cube root"; *)

(* To change to 64-bit reals, address the code fragments written in light red. *)

(* Algorithm is from: J.F. Hart, E.W. Cheney, C.L. Lawson, H.J. Maehly, C.K. Mesztenyi, J.R. Rice, H.G. Thacher, Jr.,
	and C. Witzgall, "Computer Approximations," in: The SIAM Series in Applied Mathematics, Wiley, New York, 1968. *)

использует NbrInt, NbrRe, NbrCplx, DataErrors, MathReSeries;

перем
	cbrt2: NbrRe.Real;
	(* Whenever NbrRe.Real is a 32-bit real, define the following arrays. *)
	cbrtP, cbrtQ: массив 3 из NbrRe.Real;
	(* Or whenever NbrRe.Real is a 64-bit real, define the following arrays. *)
	(*  cbrtP, cbrtQ: ARRAY 5 OF NbrRe.Real;  *)

	проц Fn*( x: NbrRe.Real ): NbrRe.Real;
	перем isNeg: булево;
		i, exponent, mod: NbrInt.Integer;  cbrtRadix, cubeRoot, mantissa, ratFn, update: NbrRe.Real;
	нач
		если NbrRe.Radix = 2 то cbrtRadix := cbrt2
		аесли NbrRe.Radix = 4 то cbrtRadix := cbrt2 * cbrt2
		аесли NbrRe.Radix = 8 то cbrtRadix := 2
		аесли NbrRe.Radix = 16 то cbrtRadix := 2 * cbrt2
		аесли NbrRe.Radix = 32 то cbrtRadix := 2 * cbrt2 * cbrt2
		иначе DataErrors.Error( "Not implemented for this CPU." );  возврат 0
		всё;
		(* Obtain original mantissa and exponent. *)
		mantissa := NbrRe.Mantissa( x );  exponent := NbrRe.Exponent( x );
		если mantissa > 0 то isNeg := ложь иначе isNeg := истина;  mantissa := NbrRe.Abs( mantissa ) всё;
		(* Reduce the input to the range: 1/radix <= x <= 1. *)
		mantissa := mantissa / NbrRe.Radix;
		(* Initial estimate for cube root, scaled by 1 / cbrtRadix. *)
		ratFn := MathReSeries.TruncatedRationalFunction( cbrtP, cbrtQ, mantissa );
		если isNeg то mantissa := -ratFn иначе mantissa := ratFn всё;
		(* Convert back to a real number. *)
		mod := exponent остОтДеленияНа 3;
		если mod = 0 то cubeRoot := NbrRe.Re( cbrtRadix * mantissa, exponent DIV 3 )
		аесли mod = 1 то cubeRoot := NbrRe.Re( cbrtRadix * cbrtRadix * mantissa, (exponent + 1) DIV 3 )
		иначе  cubeRoot := NbrRe.Re( NbrRe.Radix * mantissa, (exponent - 1) DIV 3 )
		всё;
		(* Three Newton iterations to enhance accuracy. *)
		нцДля i := 1 до 3 делай
			update := cubeRoot - (cubeRoot - x / (cubeRoot * cubeRoot)) / 3;  cubeRoot := update
		кц;
		возврат cubeRoot
	кон Fn;

	проц CplxFn*( z: NbrCplx.Complex ): NbrCplx.Complex;
	перем abs, arg, im, re: NbrRe.Real;  cbrt: NbrCplx.Complex;
	нач
		im := NbrCplx.Im( z );
		если im = 0 то
			re := NbrCplx.Re( z );
			если re # 0 то NbrCplx.Set( Fn( re ), 0, cbrt ) иначе cbrt := 0 всё
		иначе abs := NbrCplx.Abs( z );  arg := NbrCplx.Arg( z );  NbrCplx.SetPolar( Fn( abs ), arg / 3, cbrt )
		всё;
		возврат cbrt
	кон CplxFn;

нач
	(* Whenever NbrRe.Real is a 32-bit real, use the following eonstants. *)
	cbrt2 := 1.25992105;
	(* Constants from Table CBRT 0723 from "Computer Approximations". *)
	cbrtP[0] := 0.17782942E-1;  cbrtP[1] := 0.885812034;  cbrtP[2] := 1.76794059;
	cbrtQ[0] := 0.99419338E-1;  cbrtQ[1] := 1.57522016;  cbrtQ[2] := 1.0
	(* Or, whenever NbrRe.Real is a 64-bit real, use the following eonstants. *)
	(*  cbrt2 := 1.25992104989487316476D0;
	(* Constants from Table CBRT 0725 from "Computer Approximations". *)
	cbrtP[0] := 5.6235404D-4;  cbrtP[1] := 0.11020808784D0;  cbrtP[2] := 2.0968321394D0;
	cbrtP[3] := 6.4753110699D0;  cbrtP[4] := 2.6119694699D0;
	cbrtQ[0] := 4.6449016D-3;  cbrtQ[1] := 0.36413680255D0;  cbrtQ[2] := 3.7287466072D0;
	cbrtQ[3] := 6.1973781157D0;  cbrtQ[4] := 1.0D0  *)
кон MathCbrt.
