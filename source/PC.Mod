(* Paco, Copyright 2000 - 2002, Patrik Reali, ETH Zurich *)

модуль PC; (** AUTHOR "prk / be"; PURPOSE "Parallel Compiler: main module"; *)

использует
	Commands, Modules, Потоки, Files, Configuration, Diagnostics, CompilerInterface,
	Texts, TextUtilities, Strings, UTF8Strings, DynamicStrings, XMLObjects, XML, XMLScanner, XMLParser,
	StringPool, PCM, PCS, PCT, PCP, PCLIR, PCBT, PCOF, PCOM, PCV, PCC;

конст
	Name = "PACO";
	Description = "Parallel Active Oberon Compiler";
	FileExtension = "MOD";

	DefaultErrorFile = "Errors.XML";
	ErrorTag = "Error";
	ErrCodeAttr = "code";

	(* compiler options: -> PCM *)
	DefCodeOpt = {PCM.ArrayCheck, PCM.AssertCheck, PCM.TypeCheck, PCM.PtrInit, PCM.FullStackInit};
	DefParserOpt = {};
	DefDest = "386";

	Debug = истина;

	NoBreakPC = -1;

перем
	ErrorFile: массив 256 из симв8;

тип
	StringBuf = массив 256 из симв8;

	OptionString* = массив 256 из симв8;

перем
	LastDest: массив 16 из симв8; (* last code generator loaded *)

проц OutMsg(scanner: PCS.Scanner);
перем s: PCS.Scanner;  t: PCS.Token; name: StringBuf;
нач
	s := PCS.ForkScanner(scanner);
	s.Get(t);
	если t = PCS.module то
		s.Get(t);
		если t = PCS.ident то
			StringPool.GetString(s.name, name);
			PCM.LogWStr(" compiling "); PCM.LogWStr(PCM.prefix); PCM.LogWStr(name);
			если PCM.suffix # Modules.extension[0] то
				PCM.LogWStr(PCM.suffix)
			аесли Modules.ModuleByName(name) # НУЛЬ то
				PCM.LogWStr(" (in use)")
			всё;
			PCM.LogWStr(" ...");
			PCM.LogFlush;
		всё;
	всё;
кон OutMsg;

проц Configure(конст base, dest: массив из симв8;  errorIsFatal: булево);
перем name: массив 32 из симв8;  i, j: цел32;  p: проц;
нач
	i := 0;
	нцПока (base[i] # 0X) делай  name[i] := base[i]; увел(i)  кц;
	j := 0;
	нцПока dest[j] # 0X делай  name[i] := dest[j]; увел(i); увел(j)  кц;
	name[i] := 0X;
	дайПроцПоИмени (name, "Install", p);
	если p # НУЛЬ то
		p; (*call Install*)
		PCV.SetBasicSizes;
	аесли errorIsFatal то
		PCM.LogWStr("Cannot install code-generator (no Install procedure)");
		PCM.LogWLn;
		PCM.error := истина
	всё
кон Configure;

проц LoadBackEnd(конст dest: массив из симв8);
нач
	копируйСтрокуДо0(dest, LastDest);
	Configure("PCG", dest, истина);
	если ~PCM.error то
		PCP.Assemble := НУЛЬ;	(*default = no assembler*)
		Configure("PCA", dest, ложь)
	всё;
кон LoadBackEnd;

проц GetOptions(r: Потоки.Чтец; перем opts: массив из симв8);
перем i: цел32; ch: симв8;
нач
	i := 0;
	нцПока opts[i] # 0X делай увел(i) кц;
	r.ПропустиБелоеПоле;
	ch := r.ПодглядиСимв8();
	нцПока (ch = "\") делай
		r.чСимв8(ch); (* skip \ *)
		r.чСимв8(ch);
		нцПока (ch > " ") делай
			opts[i] := ch;  увел(i); r.чСимв8(ch)
		кц;
		opts[i] := " "; увел(i);
		r.ПропустиБелоеПоле;
		ch := r.ПодглядиСимв8()
	кц;
	opts[i] := 0X
кон GetOptions;

(** Extract input file prefix from global options string, exported for PC.Mod *)
проц GetSourcePrefix*(конст options : OptionString; перем prefix : массив из симв8);
перем ch, lastCh : симв8; i : цел32;
нач
	prefix := "";
	i := 0; ch := 0X;
	нц
		lastCh := ch;
		ch := options[i]; увел(i);
		если (ch = 0X) или (i >= длинаМассива(options)) то прервиЦикл; всё;
		если (ch = "p") то
			если (i = 0) или (lastCh = " ") то (* be sure that "p" is the first character of an option *)
				SubString(options, i, prefix);
			всё;
		всё;
	кц;
кон GetSourcePrefix;

проц SubString(конст options : массив из симв8; перем from : цел32; перем str: массив из симв8);
перем ch: симв8;  j: цел32;
нач
	утв(from < длинаМассива(options));
	ch := options[from]; увел(from); j := 0;
	нцПока (ch # 0X) и (ch # " ") и (from < длинаМассива(options)) и (j < длинаМассива(str)-1) делай
		str[j] := ch; ch := options[from]; увел(j); увел(from);
	кц;
	str[j] := 0X;
кон SubString;

проц ParseOptions(конст options: массив из симв8; перем prefix, extension, dest, dump, objF: массив из симв8;  перем cOpt, pOpt: мнвоНаБитахМЗ);
перем  i: цел32;  ch: симв8; ignore : OptionString;
нач
	(* default options *)
	cOpt := DefCodeOpt;
	pOpt := DefParserOpt;
	копируйСтрокуДо0("", prefix);
	копируйСтрокуДо0(Modules.extension[0], extension);
	копируйСтрокуДо0(DefDest, dest);
	копируйСтрокуДо0("", dump);
	(* parse options *)
	i := 0;
	нцДо
		ch := options[i]; увел(i);
		(* fof: note that symmetric difference works as a switch: {1,2}/{2}={1}, {1,2}/{3}={1,2,3} *)
		если ch = "s" то pOpt := pOpt / {PCM.NewSF}
		аесли ch = "e" то pOpt := pOpt / {PCM.ExtSF}
		аесли ch = "n" то pOpt := pOpt / {PCM.NoFiles}
		аесли ch = "f" то pOpt := pOpt / {PCM.Breakpoint}
		аесли ch = "o" то pOpt := pOpt / {PCM.NoOpOverloading}	(* do NOT allow operator overloading *)
		аесли ch = "N" то cOpt := cOpt / {PCM.NilCheck}
		аесли ch = "c" то pOpt := pOpt / {PCM.CacheImports}
		аесли ch = "x" то cOpt := cOpt / {PCM.ArrayCheck}
		аесли ch = "a" то cOpt := cOpt / {PCM.AssertCheck}
		аесли ch = "z" то cOpt := cOpt / {PCM.FullStackInit}
		аесли ch = "b" то pOpt := pOpt / {PCM.BigEndian}
		аесли ch = "." то умень(i); SubString(options, i, extension)
		аесли ch = "p" то SubString(options, i, ignore);	(* Skip prefix for input filenames (only as global option) *)
		аесли ch = "P" то SubString(options, i, prefix);	(* Prefix for output filenames *)
		аесли ch = "d" то SubString(options, i, dest)
		аесли ch = "D" то SubString(options, i, dump)
		аесли ch = "O" то cOpt := cOpt / {PCM.Optimize}
		аесли ch = "F" то SubString(options, i, objF)
		аесли ch = "W" то pOpt := pOpt / {PCM.Warnings}
		аесли ch = "S" то pOpt := pOpt / {PCM.SkipOldSFImport}
		аесли ch = "M" то pOpt := pOpt / {PCM.MultipleModules}
		аесли ch = "A" то cOpt := cOpt / {PCM.AlignedStack}
		всё
	кцПри ch = 0X;
кон ParseOptions;

проц EmitScope(scope: PCT.Scope);
перем name: StringBuf;
нач
	если (scope.code # НУЛЬ) и (scope.code суть PCLIR.Code) то
		если Debug то PCT.GetScopeName(scope, name) всё;
		PCLIR.Emit(scope.code(PCLIR.Code));
		scope.code := НУЛЬ
	всё
кон EmitScope;

проц Module*(scanner: PCS.Scanner; конст source, options: массив из симв8; breakpc: цел32; log: Потоки.Писарь;
	diagnostics : Diagnostics.Diagnostics; перем error: булево);
перем
	scope: PCT.ModScope; dest, objF: массив 16 из симв8;
	size: цел32; R: PCM.Rider; new, extend, nofile, skip: булево;
	version: симв8; res: целМЗ;
	str: StringBuf;
	msg: массив 32 из симв8;
	finished: булево; copyscanner: PCS.Scanner; sym: цел8;
нач {единолично}
	PCM.Init (source, log, diagnostics); (* also resets PCM.count!! *)
	ParseOptions(options, PCM.prefix, PCM.suffix, dest, PCM.dump, objF, PCM.codeOptions, PCM.parserOptions);
	если dest # LastDest то LoadBackEnd(dest) всё;

	new := PCM.NewSF в PCM.parserOptions;
	extend := PCM.ExtSF в PCM.parserOptions;
	nofile := PCM.NoFiles в PCM.parserOptions;
	skip := PCM.SkipOldSFImport в PCM.parserOptions;
	PCM.bigEndian := PCM.BigEndian в PCM.parserOptions;
	PCM.breakpc := матМаксимум(цел32);
	если PCM.Breakpoint в PCM.parserOptions то
		если breakpc = NoBreakPC то
			PCM.LogWLn; PCM.LogWStr("No PC Selected");
			возврат
		всё;
		PCM.breakpc := breakpc
	всё;

	finished := ~ (PCM.MultipleModules в PCM.parserOptions);

	нцДо

	OutMsg(scanner);
	new := PCM.NewSF в PCM.parserOptions;
	extend := PCM.ExtSF в PCM.parserOptions;
	nofile := PCM.NoFiles в PCM.parserOptions;
	skip := PCM.SkipOldSFImport в PCM.parserOptions;
	PCM.bigEndian := PCM.BigEndian в PCM.parserOptions;
	PCM.breakpc := матМаксимум(цел32);
	если PCM.Breakpoint в PCM.parserOptions то
		если breakpc = NoBreakPC то
			PCM.LogWLn; PCM.LogWStr("No PC Selected");
			возврат
		всё;
		PCM.breakpc := breakpc
	всё;

	если PCLIR.CG.Init() то
		нов(scope); PCT.InitScope(scope, НУЛЬ, {}, ложь);
		PCP.ParseModule(scope, scanner);


		если ~PCM.error и ~nofile то
			version := PCM.FileVersion;
			StringPool.GetString(scope.owner.name, str);
			PCM.Open(str, R, version);
			если ~(PCM.Breakpoint в PCM.parserOptions) то
				если PCM.CacheImports в PCM.parserOptions то
					PCT.Unregister(PCT.database, scope.owner.name);
				всё;
				PCOM.Export(R, scope.owner, new, extend, skip, msg);
				PCM.LogWStr(msg)
			всё;
			если ~PCM.error то
				PCT.TraverseScopes(scope, EmitScope);
				если objF # "" то
					Configure("PCOF", objF, истина)
				иначе
					PCOF.Install
				всё;
				если ~PCM.error и ~(PCM.Breakpoint в PCM.parserOptions) то  PCBT.generate(R, scope, size)  всё;
			всё
		всё;
		если ~PCM.error то
			PCM.LogWStr("  "); PCM.LogWNum(size); PCM.LogWStr(" done ");
			если PCM.bigEndian то  PCM.LogWStr("(BigEndian Mode)") всё;
			PCM.LogWLn
		иначе
			finished := истина;
			PCM.LogWStr(" not done"); PCM.LogWLn
		всё;
		PCLIR.CG.Done(res); (* ignore res ? *)
	иначе
		finished := истина;
		PCM.LogWLn; PCM.LogWStr("  Code generator not installed");
		PCM.LogWLn; PCM.error := истина;
	всё;
	PCC.Cleanup;
	error := PCM.error;
	PCM.Reset;
	PCBT.context := НУЛЬ;
	PCM.LogFlush;

	copyscanner := PCS.ForkScanner(scanner);
	copyscanner.Get(sym);
	finished := finished или (sym # PCS.module);

	кцПри finished

кон Module;

(** Compile code contained in t, beginning at position pos *)

проц CompileText*(t: Texts.Text; конст source: массив из симв8; pos, pc: цел32; конст opt: массив из симв8; log: Потоки.Писарь;
diagnostics : Diagnostics.Diagnostics; перем error: булево);
нач
	если t = НУЛЬ то
		log.пСтроку8 ("No text available"); log.пВК_ПС; log.ПротолкниБуферВПоток;
		error := истина; возврат
	всё;
	Module(PCS.InitWithText(t, pos), source, opt, pc, log, diagnostics, error);
кон CompileText;

проц CompileInterface(t: Texts.Text; конст source: массив из симв8; pos: цел32; конст pc,opt: массив из симв8; log: Потоки.Писарь;
diagnostics : Diagnostics.Diagnostics; перем error: булево);
перем pcNum: цел32;
нач
	Strings.StrToInt(pc, pcNum);
	CompileText(t,source,pos,pcNum, opt,log, diagnostics, error);
кон CompileInterface;


(** Compile file *)

проц CompileFile*(конст name, opt: массив из симв8; pc: цел32; log: Потоки.Писарь;
	diagnostics : Diagnostics.Diagnostics; перем error: булево);
перем
	atu: Texts.Text; format: цел32; res: целМЗ;
нач
	нов(atu);
	TextUtilities.LoadAuto(atu, name, format, res);
	если res # 0 то
		log.пСтроку8 (name); log.пСтроку8 (" not found"); log.пВК_ПС; log.ПротолкниБуферВПоток;
		error := истина; возврат
	всё;
	log.пСтроку8 (name);
	Module(PCS.InitWithText(atu, 0), name, opt, pc, log, diagnostics, error);
кон CompileFile;

(** Compile ascii file *)

проц CompileAsciiFile*(конст name, opt: массив из симв8; pc: цел32; log: Потоки.Писарь;
	diagnostics : Diagnostics.Diagnostics; перем error: булево);
перем
	f: Files.File; r: Files.Reader;
нач
	f := Files.Old(name);
	если f = НУЛЬ то
		log.пСтроку8 (name); log.пСтроку8 (" not found");
		log.пВК_ПС; log.ПротолкниБуферВПоток;
		error := истина; возврат
	всё;
	log.пСтроку8 (name);
	Files.OpenReader(r, f, 0);
	Module(PCS.InitWithReader(r, f.Length()(размерМЗ),0), name, opt, pc, log, diagnostics, error);
кон CompileAsciiFile;

проц Compile*(context : Commands.Context);
перем
	globalOpt, localOpt: OptionString;
	fullname, prefix, filename: массив 256 из симв8;
	count: цел32;
	error: булево;
	diagnostics : Diagnostics.DiagnosticsList;
нач
	PCT.InitDB(PCT.database);
	error := ложь;
	globalOpt := ""; GetOptions(context.arg, globalOpt);
	GetSourcePrefix(globalOpt, prefix);
	count := 0;
	нов(diagnostics);
	нцПока  ~context.arg.ВКонцеСтрокиТекстаИлиФайлаЛи¿() и ~error делай
		context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
		если filename # "" то
			увел(count);
			копируйСтрокуДо0(globalOpt, localOpt);
			GetOptions(context.arg, localOpt);
			копируйСтрокуДо0(prefix, fullname); Strings.Append(fullname, filename);
			diagnostics.Reset;
			CompileFile(fullname, localOpt, матМаксимум(цел32), context.out, diagnostics, error);
			diagnostics.ToStream(context.out, Diagnostics.All);
			PCM.LogFlush;
			если count остОтДеленияНа 32 = 0 то PCT.InitDB(PCT.database) всё;
		всё
	кц;
	PCT.InitDB(PCT.database);
кон Compile;

проц TrapHandler(pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
нач
	PCM.LogWStr("could not load error messages: "); PCM.LogWLn;
	PCM.LogWStr(ErrorFile); PCM.LogWStr(" invalid (pos ");
	PCM.LogWNum(pos); PCM.LogWStr(", line ");
	PCM.LogWNum(line); PCM.LogWStr(", row ");
	PCM.LogWNum(row); PCM.LogWStr("   ");
	PCM.LogWStr(msg); PCM.LogWStr(")"); PCM.LogWLn;
кон TrapHandler;

(** (Re)load error messages *)
проц InitErrMsg*; (** ~ *)
перем
	f: Files.File; scanner: XMLScanner.Scanner; parser: XMLParser.Parser; errors: XML.Document;
	e: XML.Element; enum, msgEnum: XMLObjects.Enumerator; p: динамическиТипизированныйУкль;
	code: цел32; i: размерМЗ; str: XML.String;
	dynStr: DynamicStrings.DynamicString;
	r : Files.Reader;
	res : целМЗ;
нач
	Configuration.Get("Paco.ErrorMessages", ErrorFile, res);
	если (res # Configuration.Ok) то ErrorFile := DefaultErrorFile всё;
	f := Files.Old(ErrorFile);
	если f = НУЛЬ то
		PCM.LogWStr("could not load error messages: ");
		PCM.LogWStr(ErrorFile); PCM.LogWStr(" not found"); PCM.LogWLn;
		возврат;
	всё;
	(* f # NIL *)
	Files.OpenReader(r, f, 0);
	нов(scanner, r);
	нов(parser, scanner); parser.reportError := TrapHandler;
	errors := parser.Parse();
	e := errors.GetRoot();
	enum := e.GetContents();
	нцПока enum.HasMoreElements() делай
		p := enum.GetNext();
		если p суть XML.Element то
			e := p(XML.Element);
			str := e.GetName();
			если str^ = ErrorTag то
					(* extract error code *)
				str := e.GetAttributeValue(ErrCodeAttr);
				Strings.StrToInt(str^, code);
					(* extract error message *)
				msgEnum := e.GetContents();
				нов(dynStr);
				нцПока msgEnum.HasMoreElements() делай
					p := msgEnum.GetNext();
					если p суть XML.Chars то
						str := p(XML.Chars).GetStr();
						dynStr.Append(str^);
					аесли p суть XML.CDataSect то
						str := p(XML.CDataSect).GetStr();
						dynStr.Append(str^);
					аесли p суть XML.CharReference то
						нов(str, 5);
						i := 0;
						если UTF8Strings.EncodeChar(p(XML.CharReference).GetCode(), str^, i) то
							dynStr.Append(str^);
						всё;
					иначе
						(* ignore *)
					всё;
				кц;
				str := dynStr.ToArrOfChar();
				PCM.SetErrorMsg(code, str^);
				dynStr.Init();
			всё;
		всё;
	кц;
кон InitErrMsg;

проц Cleanup;
нач
	CompilerInterface.Unregister(Name);
кон Cleanup;

нач
	LastDest := "";
	PCM.LogWStr("Parallel Compiler / prk"); PCM.LogWLn;
	PCV.Install;
	InitErrMsg;
	Modules.InstallTermHandler(Cleanup);
	CompilerInterface.Register(Name, Description, FileExtension, CompileInterface);
кон PC.

(*
	21.11.07	fof	new compiler option /M added (multiple modules within one file allowed, MODULE ident .... ident. MODULE ident ... ident. etc.)
	10.08.07	sst	new compiler option /p added
	15.11.06	ug	new compiler option /S added, FileVersion incremented
	25.11.03	mb	added InitErrMsg: read error messages from XML file
	20.09.03	prk	"/Dcode" compiler option added
	24.06.03	prk	Check that name after END is the same as declared after MODULE
	25.02.03	prk	PC split into PC (Aos pure) and PC (Oberon dependent)
*)
