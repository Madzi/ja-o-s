(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль TCPServices; (** AUTHOR "pjm"; PURPOSE "Abstract TCP services"; *)

использует ЛогЯдра, IP, TCP, TLS, Configuration, Strings;

конст
	Ok* = TCP.Ok;
	Trace = истина;

тип
	Service* = окласс	(** TCP service object - handles opening and closing of connections *)
		перем res: целМЗ; service, client: TCP.Connection; root, agent: Agent; new: NewAgent;

		(** Start is called indirectly by OpenService. *)
		проц &Start*(port: цел32; new: NewAgent; перем res: целМЗ);
		нач
			нов(service); service.Open(port, IP.NilAdr, TCP.NilPort, res);
			если res = Ok то
				нов(root, НУЛЬ, НУЛЬ); root.next := НУЛЬ;
				сам.new := new
			иначе
				service := НУЛЬ
			всё;
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Service "); ЛогЯдра.пЦел64(port, 1);
				ЛогЯдра.пСтроку8(" open "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё
		кон Start;

		проц Remove(a: Agent);
		перем p: Agent;
		нач {единолично}
			p := root;
			нцПока (p.next # НУЛЬ) и (p.next # a) делай p := p.next кц;
			если p.next = a то p.next := a.next всё
		кон Remove;

		проц Stop*;
		перем p, c: Agent;
		нач	(* traversal can run concurrently with Remove and may see removed elements *)
			service.Закрой();
			p := root.next;
			нцПока p # НУЛЬ делай
				c := p; p := p.next;	(* p.next is modified by Remove *)
				c.Stop()
			кц;
			нач {единолично}
				дождись(root.next = НУЛЬ);	(* wait for all agents to remove themselves *)
				дождись(new = НУЛЬ)	(* wait for service to terminate *)
			кон
		кон Stop;

	нач {активное}
		если service # НУЛЬ то
			нц
				service.Accept(client, res);
				если res # Ok то прервиЦикл всё;
				agent := new(client, сам);
				нач {единолично}
					agent.next := root.next; root.next := agent
				кон
			кц;
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Service "); ЛогЯдра.пЦел64(service.lport, 1);
				ЛогЯдра.пСтроку8(" result "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё
		всё;
		нач {единолично}
			new := НУЛЬ	(* signal to Stop *)
		кон
	кон Service;

тип
	TLSService* = окласс (Service)	(** TCP service object - handles opening and closing of connections *)
		перем
		  policy: TLS.Policy;
		  ctx: TLS.Context; cipherSuites: массив TLS.Suites из цел32;

		(** Start is called indirectly by OpenService. *)
		проц &{перекрыта}Start*(port: цел32; new: NewAgent; перем res: целМЗ);
			перем
				newService: TLS.Connection;
		   certificate : массив 500 из симв8;
		   pHex, qHex, eHex : массив 1000 из симв8;
		   intstring : массив 20 из симв8;
		   pLen, qLen, eLen : цел32;
		нач
			(* Set the policy *)
			нов( policy );
			cipherSuites[ 0 ] := TLS.TlsRsaWithRc4128Md5;
			(* cipherSuites[ 0 ] := TLS.TlsRsaWith3DesEdeCbcSha; *)

			policy.SetCipherSuites( cipherSuites, 1 );

			(* set up context *)
			нов( ctx, policy );
			Configuration.Get("TLS.Certificate", certificate, res);
			res := ctx.LoadRsaCertificate( certificate );
			Configuration.Get("TLS.pHex", pHex, res);
			Configuration.Get("TLS.qHex", qHex, res);
			Configuration.Get("TLS.eHex", eHex, res);
			Configuration.Get("TLS.pLen", intstring, res); Strings.StrToInt(intstring, pLen);
			Configuration.Get("TLS.qLen", intstring, res); Strings.StrToInt(intstring, qLen);
			Configuration.Get("TLS.eLen", intstring, res); Strings.StrToInt(intstring, eLen);

			ctx.LoadRsaPrivateKey( pHex, qHex, eHex, устарПреобразуйКБолееУзкомуЦел(pLen), устарПреобразуйКБолееУзкомуЦел(qLen), устарПреобразуйКБолееУзкомуЦел(eLen));

			(* set up passive secure connection *)
			нов(newService); сам.service := newService;
			service(TLS.Connection).SetContext( ctx );
			service.Open( port, IP.NilAdr, TCP.NilPort, res );

			если res = Ok то
				нов(root, НУЛЬ, НУЛЬ); root.next := НУЛЬ;
				сам.new := new
			иначе
				service := НУЛЬ
			всё;
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Service "); ЛогЯдра.пЦел64(port, 1);
				ЛогЯдра.пСтроку8(" open "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё
		кон Start;
	кон TLSService;


тип
	Agent* = окласс	(** abstract TCP agent object - should be extended with an active body using "client". *)
		перем
			client-: TCP.Connection;	(** TCP connection to the client *)
			next: Agent; s-: Service;

		(** Start is called indirectly by the Service object. *)
		проц &Start*(c: TCP.Connection; s: Service);
		перем str: массив 32 из симв8;
		нач
			сам.client := c; сам.s := s;
			если Trace и (c # НУЛЬ) то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Agent "); ЛогЯдра.пЦел64(c.lport, 1);
				ЛогЯдра.пСтроку8(" on interface "); IP.AdrToStr(c.int.localAdr, str); ЛогЯдра.пСтроку8(str);
				ЛогЯдра.пСтроку8(" connected to "); IP.AdrToStr(c.fip, str); ЛогЯдра.пСтроку8(str);
				ЛогЯдра.пСимв8(":"); ЛогЯдра.пЦел64(c.fport, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё
		кон Start;

		проц Stop;	(* called from outside to stop an agent *)
		нач
			client.Закрой()
		кон Stop;

		(** Terminate is called by the body of the extended object to terminate itself. *)
		проц Terminate*;
		нач
			client.Закрой();
			s.Remove(сам)
		кон Terminate;

	кон Agent;

		(** A "factory" procedure for agent extensions.  Used by a service object. *)
	NewAgent* = проц {делегат} (c: TCP.Connection; s: Service): Agent;


кон TCPServices.

System.FreeDownTo TCP ~
(**
Notes

This module provides a framework for TCP services utilizing active objects as agents.  A Service object is responsible for managing incoming connections from clients.  It creates one (active) Agent object instance per client, to provide the actual service.

A user of this module should extend the Agent object with an active body.  The body can use the client field to access its client connection.  The client field is a TCP connection object with the Send and Receive methods for sending and receiving data.  When the connection is closed by the client, the Receive method will return an error code (res # 0).  In this case the Agent object must call the Terminate method in its base record, to signal to the Service object that it is terminating.

Because the Service object needs to create arbitrary Agent extension objects, it needs a "factory procedure" to allocate such agent extensions.  The factory procedure is passed to the Service object when it is allocated, and it is called by the Service object every time it needs to create a new agent, i.e., every time a new client connection arrives.  The factory procedure should allocate the extended object instance, and return it.  This is perhaps best illustrated by an example.

The following agent implements the TCP discard service.  This service accepts connections, and discards everything that arrives on the connection, until it is closed by the client.

TYPE
	DiscardAgent = OBJECT (TCPServices.Agent)
		VAR len, res: SIGNED32; buf: ARRAY 4096 OF CHAR;

	BEGIN {ACTIVE}
		REPEAT
			client.Receive(buf, 0, LEN(buf), LEN(buf), len, res)
		UNTIL res # Ok;
		Terminate
	END DiscardAgent;

PROCEDURE NewDiscardAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
VAR a: DiscardAgent;
BEGIN
	NEW(a, c, s); RETURN a
END NewDiscardAgent;

To open the discard service:
	VAR discard: TCPServices.Service;
	TCPServices.OpenService(discard, 9, NewDiscardAgent);	(* use TCP port 9 *)

This creates a Service object, which waits actively for TCP connections on port 9.  Every time a connection arrives, it calls NewDiscardAgent to allocate a DiscardAgent active object.  The DiscardAgent accesses the client connection through the client field.

Currently there is no limit to the number of connections that can be accepted by a Service object.  A simple denial-of-service attack would be to open many connections to an existing port.

To close the discard service:
	TCPServices.CloseService(discard);
*)

(*
to do:
o limit number of client connections
o re-use agents?
o clean up "dead" clients periodically (user-specified timeout?)
*)
