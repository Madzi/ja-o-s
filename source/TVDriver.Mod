модуль TVDriver;	(** AUTHOR "oljeger@student.ethz.ch"; PURPOSE "Interface for TV card drivers"; *)

использует
	НИЗКОУР, Plugins, Modules, ЛогЯдра, Strings, ActiveTimers;

конст
	RegistryName = "TVDriver";
	RegistryDesc = "TV drivers for Bluebottle";

	WesternEuropeanChnlSet* = 0;
	VbiMaxLines* = 16;
	VbiLineSize* = 2048;
	VbiBufferItems = 20;
	VbiDataSize* = VbiLineSize * VbiMaxLines * 2;
	VbiBufferSize* = VbiDataSize *  VbiBufferItems;

тип

	(** The VbiBuffer objects contain a ring buffer with raw VBI data extracted by the TV card *)
	VbiBuffer* = окласс
		перем
			data*: массив VbiBufferSize из симв8;
			readPos*: цел32;
			insertPos*: цел32;
			vbiSize*: цел32;
			timeout: булево;
			timer: ActiveTimers.Timer;

		проц &Init*;
		нач
			нов(timer);
			timeout := ложь;
		кон Init;

		проц TimeoutHandler;
		нач {единолично}
			timeout := истина
		кон TimeoutHandler;

		(** Wait for teletext data *)
		проц AwaitData*;
		нач {единолично}
			timer.SetTimeout(TimeoutHandler, 200);
			timeout := ложь;
			дождись ((vbiSize > 0) или timeout);
			timer.CancelTimeout;
		кон AwaitData;

		проц Finalize*;
		нач {единолично}
			timeout := истина;
			timer.Finalize;
		кон Finalize;

	кон VbiBuffer;

	(** The TVTuner objects represents the tuning facilities of a video capture card. *)
	TVTuner* = окласс
	перем
		frequency*: цел32;

	(** Initialize the tuner object. *)
		проц & Init* (vcd: VideoCaptureDevice);
		нач СТОП(99)	(** abstract *)
		кон Init;

	(** Open the VBI device. *)
	(** return value: -1 if the device is already open, otherwise 0 *)
		проц OpenVbi* (): цел32;
		нач СТОП(99)	(** abstract *)
		кон OpenVbi;

	(** Close the VBI device. *)
		проц CloseVbi*;
		нач СТОП(99)	(** abstract *)
		кон CloseVbi;

	(** Open the tuner. *)
		проц Open*;
		нач СТОП(99)	(** abstract *)
		кон Open;

	(** Close the tuner. *)
		проц Close*;
		нач СТОП(99)	(** abstract *)
		кон Close;

	(** Set the channel set. *)
		проц SetChannelSet* (chnlSet: цел32);
		нач СТОП(99)	(** abstract *)
		кон SetChannelSet;

	(** Get the channel set. *)
		проц GetChannelSet* (): цел32;
		нач СТОП(99)	(** abstract *)
		кон GetChannelSet;

	(** Set a channel. *)
	(** chnl:	channel to set (see range of channel set). *)
		проц SetChannel* (chnl: цел32);
		нач СТОП(99)	(** abstract *)
		кон SetChannel;

	(** Get channel. *)
	(** return value:	channel *)
		проц GetChannel* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetChannel;

		проц GetMaxChannel* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetMaxChannel;

	(** Set TV frequency. *)
		проц SetTVFrequency* (freq: цел32);
		нач
			NotifyChannelSwitchObservers (freq);
			SetTVFrequencyImpl (freq);
		кон SetTVFrequency;

		проц SetTVFrequencyImpl* (freq: цел32);
		нач
		СТОП(99);	(** abstract *)
		кон SetTVFrequencyImpl;

	(** Get tuner frequency. *)
	(** return value:	tuner frequency *)
		проц GetFrequency* (): цел32;
		нач
			возврат frequency
		кон GetFrequency;

	(** Set radio frequency. *)
	(** freq:	frequency expressed as frequency * 100. *)
		проц SetRadioFrequency* (freq: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetRadioFrequency;

		проц InstallChannelSwitchHandler* (handler: ChannelSwitchHandler);
		перем
			chObs: ChannelSwitchObserver;
		нач
			нов (chObs);
			chObs.handler := handler;
			(* insert new observer in front of the list *)
			chObs.next := channelSwitchObservers;
			channelSwitchObservers := chObs;
		кон InstallChannelSwitchHandler;

		проц NotifyChannelSwitchObservers (freq: цел32);
		перем
			chObs: ChannelSwitchObserver;
		нач
			chObs := channelSwitchObservers;
			нцПока chObs # НУЛЬ делай
				chObs.handler (freq, сам);
				chObs := chObs.next;
			кц;
		кон NotifyChannelSwitchObservers;

	(** Get the tuner status. *)
	(** return value:	tuner status *)
		проц GetTunerStatus* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetTunerStatus;

	(** Calculates the field strength of the radio signal. *)
		проц CalcFieldStrength* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон CalcFieldStrength;

	(** Returns wether tuner is locked to a stable video signal or not. *)
	(** return value:	TRUE if tuner is locked to a stable video signal, otherwise FALSE *)
		проц IsLocked* (): булево;
		нач СТОП(99);	(** abstract *)
		кон IsLocked;

	(** Returns wether the sound is received in stereo or mono. *)
	(** return value:	TRUE if stereo is enabled, otherwise FALSE *)
		проц IsStereo* (): булево;
		нач СТОП(99);	(** abstract *)
		кон IsStereo;

	(** Set the hue. *)
		проц SetHue* (hue: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetHue;

	(** Get the hue *)
	(** return value:	the hue offset *)
		проц GetHue* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetHue;

	(** Set the brightness. *)
		проц SetBrightness* (brightness: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetBrightness;

	(** Get the brightness. *)
	(** return value:	the brightness offset *)
		проц GetBrightness* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetBrightness;

	(** Set the chroma saturation. *)
	(** Adds a gain adjustment to the U- and V-component of the video signal. *)
		проц SetChromaSaturation* (saturation: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetChromaSaturation;

	(** Get chroma saturation. *)
	(** return value:	the chroma saturation *)
		проц GetChromaSaturation* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetChromaSaturation;

	(** Set chroma V saturation *)
	(** Adds a gain adjustment to the V-component of the video signal. *)
		проц SetChromaVSaturation* (saturation: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetChromaVSaturation;

	(** Get the chroma V saturation. *)
	(** return value:	the chroma V saturation *)
		проц GetChromaVSaturation* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetChromaVSaturation;

	(** Set the chroma U saturation. *)
	(** Adds a gain adjustment to the U-component of the video signal. *)
		проц SetChromaUSaturation* (saturation: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetChromaUSaturation;

	(** Get the chroma U saturation. *)
	(** return value:	the chroma U saturation *)
		проц GetChromaUSaturation* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetChromaUSaturation;

	(** Set the luma notch filters. *)
	(** Sets the 3 high bits of EControl/OControl *)
	(** The luma decimation filter is used to reduce the high-frequency component of the luma signal. *)
	(** Useful when scaling to CIF resolution or lower. *)
	(** For monochrome video, the luma notch filter should not be used. This will output full bandwidth luminance. *)
	(** notch:	3 bits of EControl/OControl *)
	(**				[0] 0 enables luma decimation using selectable H filter, 1 disables luma decimation *)
	(**				[1] 0 composite video, 1 Y/C component video *)
	(**				[2] 0 enables the luma notch filter, 1 disables the luma notch filter *)
		проц SetLumaNotch* (notch: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetLumaNotch;

	(** Get status of luma notch filters. *)
	(** return value:	3 bits of EControl/OControl *)
		проц GetLumaNotch* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetLumaNotch;

	(** Set the contrast. *)
	(** This value ist multiplied by the luminance value to provide contrast adjustment. *)
		проц SetContrast* (contrast: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetContrast;

	(** Get contrast value. *)
	(** return value:	value of contrast multiplication factor *)
		проц GetContrast* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetContrast;

	(** Enable or disable color bars. *)
	(** enable:	TRUE to enable color bars, FALSE for disabling color bars *)
		проц SetColorBars* (enable: булево);
		нач СТОП(99);	(** abstract *)
		кон SetColorBars;

	кон TVTuner;

	ChannelSwitchHandler* = проц {делегат} (freq: цел32; tuner: TVTuner);

	ChannelSwitchObserver = окласс
	перем
		next: ChannelSwitchObserver;
		handler: ChannelSwitchHandler;
	кон ChannelSwitchObserver;

	NotificationHandler* = проц {делегат};

	(** The VideoCaptureDevice represents a video capture card with its tuning and audio components. *)
	VideoCaptureDevice* = окласс (Plugins.Plugin)
	(** Each VideoCaptureDevice must have a tuner, an audio and a vbiBuffer object. These can be accessed
			via the appropriate GetXXX procedures *)

		проц & Init* (base: адресВПамяти; irq, product, rev: цел32);
		нач СТОП(99);	(** abstract *)
		кон Init;

	(** Get the ringbuffer that contains the raw VBI data *)
		проц GetVbiBuffer* (): VbiBuffer;
		нач СТОП(99);	(** abstract *)
		кон GetVbiBuffer;

	(** Get the TVTuner object of this VideoCaptureDevice object. *)
	(** return value:	TVTuner object of this card *)
		проц GetTuner* (): TVTuner;
		нач СТОП(99);	(** abstract *)
		кон GetTuner;

	(** Get the Audio object of this VideoCaptureDevice object. *)
	(** return value:	Audio object of this card *)
		проц GetAudio* (): Audio;
		нач СТОП(99);	(** abstract *)
		кон GetAudio;

	(** Install a notification handler for RISC events. *)
	(** handler:	procedure of type NotificationHandler *)
		проц InstallNotificationHandler* (handler: NotificationHandler);
		нач СТОП(99);	(** abstract *)
		кон InstallNotificationHandler;

	(** Opens the VideoCaptureDevice object. *)
		проц VideoOpen*;
		нач СТОП(99);	(** abstract *)
		кон VideoOpen;

	(** Closes the VideoCaptureDevice object. *)
		проц VideoClose*;
		нач СТОП(99);	(** abstract *)
		кон VideoClose;

	(** Is the VideoCaptureDevice opened? *)
		проц IsVideoOpen* (): булево;
		нач СТОП(99);	(** abstract *)
		кон IsVideoOpen;

	(** Is the Vbi signal (Teletext) begin captured? *)
		проц IsVbiOpen* (): булево;
		нач СТОП(99);	(** abstract *)
		кон IsVbiOpen;

	(** Set a list of clipping region. *)
		проц SetClipRegion*;
		нач СТОП(99);	(** abstract *)
		кон SetClipRegion;

	(** Get the device status. *)
	(** return value:	device status *)
		проц GetStatus* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetStatus;

	(** Set the video input format *)
	(** format:	format of video signal (Bt848IFormPalBDGHI etc.) *)
		проц SetInputFormat* (format: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetInputFormat;

	(** Get the video input format. *)
		проц GetInputFormat* (): цел32;
		нач СТОП(99);	(** abstract *)
		кон GetInputFormat;

		проц SetPixelFormat* (format: цел32);
		нач СТОП(99);	(** abstract *)
		кон SetPixelFormat;

	(** Select RCA as input. *)
		проц SetInputDev0*;
		нач СТОП(99);	(** abstract *)
		кон SetInputDev0;

	(** Select the tuner as input. *)
		проц SetInputDev1*;
		нач СТОП(99);	(** abstract *)
		кон SetInputDev1;

	(** Select S-VHS (composite camera) as input. *)
		проц SetInputDev2*;
		нач СТОП(99);	(** abstract *)
		кон SetInputDev2;

	(** Select S-VHS as input. *)
		проц SetInputDevSVideo*;
		нач СТОП(99);	(** abstract *)
		кон SetInputDevSVideo;

	(** Select MUX 3 as input. *)
		проц SetInputDev3*;
		нач СТОП(99);	(** abstract *)
		кон SetInputDev3;

	(** Set the video data buffer. *)
	(** addr:	physical address of target buffer for video data. *)
	(** width:	line length of video frame buffer *)
		проц SetVideo* (addr: адресВПамяти; width: размерМЗ);
		нач СТОП(99);	(** abstract *)
		кон SetVideo;

	(** Capture a single frame. *)
		проц CaptureSingle*;
		нач СТОП(99);	(** abstract *)
		кон CaptureSingle;

	(** Start continuous frame capture. *)
		проц CaptureContinuous*;
		нач СТОП(99);	(** abstract *)
		кон CaptureContinuous;

	(** Stop continuous video data capture. *)
		проц StopCaptureContinuous*;
		нач СТОП(99);	(** abstract *)
		кон StopCaptureContinuous;

	(** Set the frame geometry. *)
	(** columns:	number of columns of a frame *)
	(** rows:	number of rows of a frame *)
	(** frames:	number of frames allocated *)
	(** format:	AosEvenOnly, AosOddOnly, none (interlaced) *)
		проц SetGeometry* (columns, rows, frames: цел32; format: мнвоНаБитахМЗ);
		нач СТОП(99);	(** abstract *)
		кон SetGeometry;

	кон VideoCaptureDevice;

	(** The Audio object represents the audio facilities of the video capture card. *)
	Audio* = окласс
		проц & Init* (vcd: VideoCaptureDevice);
		нач СТОП(99);	(** abstract *)
		кон Init;

	(** Select the tuner for audio input. *)
		проц SetAudioTuner*;
		нач СТОП(99);	(** abstract *)
		кон SetAudioTuner;

	(** Select external input for audio input. *)
		проц SetAudioExtern*;
		нач СТОП(99);	(** abstract *)
		кон SetAudioExtern;

	(** Select internal input for audio input. *)
		проц SetAudioIntern*;
		нач СТОП(99);	(** abstract *)
		кон SetAudioIntern;

	(** Set audio to mute. *)
		проц SetAudioMute*;
		нач СТОП(99);	(** abstract *)
		кон SetAudioMute;

	(** Unmute audio. *)
		проц SetAudioUnmute*;
		нач СТОП(99);	(** abstract *)
		кон SetAudioUnmute;

	(** Returns the mute state. *)
	(** return value:	TRUE if audio is mute, otherwise FALSE *)
		проц IsAudioMute* (): булево;
		нач СТОП(99);	(** abstract *)
		кон IsAudioMute;

	кон Audio;

перем
	channelSwitchObservers: ChannelSwitchObserver;
	devices*:Plugins.Registry;

(** Get a video device. *)
(** idx:	index of video card. *)
(** return value:	a VideoCaptureDevice object *)
проц GetVideoDevice* (idx: цел32): VideoCaptureDevice;
перем
	devName : массив 32 из симв8;
	nr: массив 4 из симв8;
	dev: Plugins.Plugin;
нач
	devName := RegistryName;
	Strings.IntToStr(idx, nr);
	Strings.Append(devName, nr);
	dev := devices.Get(devName);
	если dev # НУЛЬ то
		возврат dev(VideoCaptureDevice)
	иначе
		возврат НУЛЬ
	всё
кон GetVideoDevice;

проц GetDefaultDevice*() : VideoCaptureDevice;
перем p : Plugins.Plugin;
нач
	p := devices.Get ("");
	если p = НУЛЬ то
		ЛогЯдра.пСтроку8 ("{TVDriver} Error: No video capture device detected.");
		ЛогЯдра.пВК_ПС;
		возврат НУЛЬ
	всё;
	утв (p суть VideoCaptureDevice);
	возврат p(VideoCaptureDevice);
кон GetDefaultDevice;

проц Cleanup;
нач
	Plugins.main.Remove (devices)
кон Cleanup;

нач
	нов(devices, RegistryName, RegistryDesc);
	Modules.InstallTermHandler (Cleanup);
кон TVDriver.


Note: The TV Application gets non-default devices via the GetVideoDevice() routine.
All VideoCaptureDevices which are added to the devices registry must have the empty string as name!

Example:
MODULE MyTVDriver;

PROCEDURE Install*;
VAR
	dev: TVDriver.VideoCaptureDevice;
	res: INTEGER;
BEGIN
	NEW(dev);
	... set the device properties ...
	dev.SetName("");
	TVDriver.devices.Add(dev, res);
	ASSERT (res = Plugins.Ok)
END Install;
