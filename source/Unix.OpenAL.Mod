модуль OpenAL; (** AUTHOR "fnecati"; PURPOSE "OpenAL cross platform audio library"; *)

использует
	НИЗКОУР, Unix, Modules, Strings, ЛогЯдра;

конст
	libname="libopenal.so";

тип
  String256*=массив 256 из симв8;
  PString256*= укль на массив из String256;


  ALbyte    * =  симв8;  (* 1-byte signed *)
  ALshort   * =  цел16;  (* 2-byte signed *)
  ALint     * =  цел32;  (* 4-byte signed *)

  ALfloat   * =  вещ32;
  ALdouble  * =  вещ64;
  ALboolean * =  булево;

  ALubyte   * =  симв8;     (* 1-byte unsigned *)
  ALushort  * =  цел16;     (* 2-byte unsigned *)
  ALuint    * =  цел32;    (* 4-byte signed *)

  ALsizei   * =  цел32;  (* 4-byte signed *)

  ALenum    * =  цел32;

  PALboolean * =  адресВПамяти; (*   TO ALboolean;*)
  PALfloat   * =  адресВПамяти; (*   TO ALfloat;*)
  PALdouble  * =  адресВПамяти; (*   TO ALdouble;*)
  PALbyte    * =  адресВПамяти; (*   TO ALbyte;*)
  PALshort   * =  адресВПамяти; (*   TO ALshort;*)
  PALint     * =  адресВПамяти; (*   TO ALint; *)
  PALubyte   * =  адресВПамяти; (*   TO ALubyte; *)
  PALushort  * =  адресВПамяти; (*   TO ALushort; *)
  PALuint    * =  адресВПамяти; (*   TO ALuint; *)

конст
  AL_INVALID                              * =  (-1);
  AL_NONE                                 * =  0; (* "no distance model" or "no buffer" *)
  AL_FALSE                                * =  0;
  AL_TRUE                                 * =  1;
  AL_SOURCE_ABSOLUTE                      * =  201H;
  AL_SOURCE_RELATIVE                      * =  202H; (** Indicate Source has relative coordinates. *)
 (**
 * Directional source, inner cone angle, in degrees.
 * Range:    [0-360]
 * Default:  360
 *)
  AL_CONE_INNER_ANGLE                     * =  1001H;

(**
 * Directional source, outer cone angle, in degrees.
 * Range:    [0-360]
 * Default:  360
 *)
  AL_CONE_OUTER_ANGLE                     * =  1002H;

  (**
 * Specify the pitch to be applied, either at source,
 *  or on mixer results, at listener.
 * Range:   [0.5-2.0]
 * Default: 1.0
 *)
  AL_PITCH                                * =  1003H;

  (**
 * Specify the current location in three dimensional space.
 * OpenAL, like OpenGL, uses a right handed coordinate system,
 *  where in a frontal default view X (thumb) points right,
 *  Y points up (index finger), and Z points towards the
 *  viewer/camera (middle finger).
 * To switch from a left handed coordinate system, flip the
 *  sign on the Z coordinate.
 * Listener position is always in the world coordinate system.
 *)
  AL_POSITION                             * =  1004H;

  AL_DIRECTION                            * =  1005H; (** Specify the current direction. *)
  AL_VELOCITY                             * =  1006H; (** Specify the current velocity in three dimensional space. *)

  (**
 * Indicate whether source is looping.
 * Type: ALboolean?
 * Range:   [AL_TRUE, AL_FALSE]
 * Default: FALSE.
 *)
  AL_LOOPING                              * =  1007H;

  (**
 * Indicate the buffer to provide sound samples.
 * Type: ALuint.
 * Range: any valid Buffer id.
 *)
  AL_BUFFER                               * =  1009H;

  (**
 * Indicate the gain (volume amplification) applied.
 * Type:   ALfloat.
 * Range:  ]0.0-  ]
 * A value of 1.0 means un-attenuated/unchanged.
 * Each division by 2 equals an attenuation of -6dB.
 * Each multiplicaton with 2 equals an amplification of +6dB.
 * A value of 0.0 is meaningless with respect to a logarithmic
 *  scale; it is interpreted as zero volume - the channel
 *  is effectively disabled.
 *)
  AL_GAIN                                 * =  100AH;

  (*
 * Indicate minimum source attenuation
 * Type: ALfloat
 * Range:  [0.0 - 1.0]
 *
 * Logarthmic
 *)
  AL_MIN_GAIN                             * =  100DH;

 (**
 * Indicate maximum source attenuation
 * Type: ALfloat
 * Range:  [0.0 - 1.0]
 *
 * Logarthmic
 *)
  AL_MAX_GAIN                             * =  100EH;

  (**  Indicate listener orientation.  at/up  *)
  AL_ORIENTATION                          * =  100FH;


(*   AL_CHANNEL_MASK                         * =  3000H; *)

  (**  Source state information. *)
  AL_SOURCE_STATE                         * =  1010H;
  AL_INITIAL                              * =  1011H;
  AL_PLAYING                              * =  1012H;
  AL_PAUSED                               * =  1013H;
  AL_STOPPED                              * =  1014H;

  (**  Buffer Queue params *)
  AL_BUFFERS_QUEUED                       * =  1015H;
  AL_BUFFERS_PROCESSED                    * =  1016H;

 (**  Source buffer position information *)
  AL_SEC_OFFSET                   * = 1024H;
  AL_SAMPLE_OFFSET                * = 1025H;
  AL_BYTE_OFFSET                  * = 1026H;

(*
 * Source type (Static, Streaming or undetermined)
 * Source is Static if a Buffer has been attached using AL_BUFFER
 * Source is Streaming if one or more Buffers have been attached using alSourceQueueBuffers
 * Source is undetermined when it has the NULL buffer attached
 *)
  AL_SOURCE_TYPE                   * = 1027H;
  AL_STATIC                        * = 1028H;
  AL_STREAMING                     * = 1029H;
  AL_UNDETERMINED                  * = 1030H;

(** Sound samples: format specifier. *)
  AL_FORMAT_MONO8                         * =  1100H;
  AL_FORMAT_MONO16                        * =  1101H;
  AL_FORMAT_STEREO8                       * =  1102H;
  AL_FORMAT_STEREO16                      * =  1103H;

(**
 * source specific reference distance
 * Type: ALfloat
 * Range:  0.0 - +inf
 *
 * At 0.0, no distance attenuation occurs.  Default is
 * 1.0.
 *)
  AL_REFERENCE_DISTANCE            * = 1020H;

(**
 * source specific rolloff factor
 * Type: ALfloat
 * Range:  0.0 - +inf
 *
 *)
  AL_ROLLOFF_FACTOR                * = 1021H;

(**
 * Directional source, outer cone gain.
 *
 * Default:  0.0
 * Range:    [0.0 - 1.0]
 * Logarithmic
 *)
  AL_CONE_OUTER_GAIN               * = 1022H;

(**
 * Indicate distance above which sources are not
 * attenuated using the inverse clamped distance model.
 *
 * Default: +inf
 * Type: ALfloat
 * Range:  0.0 - +inf
 *)
  AL_MAX_DISTANCE                  * = 1023H;

(**
 * Sound samples: frequency, in units of Hertz [Hz].
 * This is the number of samples per second. Half of the
 *  sample frequency marks the maximum significant
 *  frequency component.
 *)

  AL_FREQUENCY                            * =  2001H;
  AL_BITS                                 * =  2002H;
  AL_CHANNELS                             * =  2003H;
  AL_SIZE                                 * =  2004H;
 (* AL_DATA                                 * =  2005H;*)
(**
 * Buffer state.
 *
 * Not supported for public use (yet).
 *)

  AL_UNUSED                               * =  2010H;
  AL_PENDING                              * =  2011H;
  AL_PROCESSED                            * =  2012H;

(** Errors: No Error. *)
  AL_NO_ERROR                             * =  AL_FALSE;

  AL_INVALID_NAME                         * =  0A001H; (**  Invalid Name paramater passed to AL call. *)
  AL_INVALID_ENUM                         * =  0A002H; (**  Invalid parameter passed to AL call. *)
  AL_INVALID_VALUE                        * =  0A003H; (**  Invalid enum parameter value. *)
  AL_INVALID_OPERATION                    * =  0A004H; (**  Illegal call. *)
  AL_OUT_OF_MEMORY                        * =  0A005H;

(** Context strings: Vendor Name. *)
  AL_VENDOR                               * =  0B001H;
  AL_VERSION                              * =  0B002H;
  AL_RENDERER                             * =  0B003H;
  AL_EXTENSIONS                           * =  0B004H;

  AL_DOPPLER_FACTOR                       * =  0C000H; (** Doppler scale.  Default 1.0 *)
  AL_DOPPLER_VELOCITY                     * =  0C001H; (** Tweaks speed of propagation. *)
  AL_SPEED_OF_SOUND                * = 0C003H;   (** Speed of Sound in units per second *)

(**
 * Distance models
 *
 * used in conjunction with DistanceModel
 *
 * implicit: NONE, which disances distance attenuation.
 *)
  AL_DISTANCE_MODEL                       * =  0D000H;
  AL_INVERSE_DISTANCE                     * =  0D001H;
  AL_INVERSE_DISTANCE_CLAMPED             * =  0D002H;
  AL_LINEAR_DISTANCE               * = 0D003H;
  AL_LINEAR_DISTANCE_CLAMPED       * = 0D004H;
  AL_EXPONENT_DISTANCE             * = 0D005H;
  AL_EXPONENT_DISTANCE_CLAMPED     * = 0D006H;
(* **************** *)
(* **************** *)

(* AL Context API types *)

тип
  ALCbyte    * =  симв8; (* 1-byte signed *)
  ALCshort   * =  цел16;  (* 2-byte signed *)
  ALCint     * =  цел32;  (* 4-byte signed *)

  ALCfloat   * =  вещ32;
  ALCdouble  * =  вещ64;
  ALCboolean * =  булево;

  ALCubyte   * =  симв8;     (* 1-byte unsigned *)
  ALCushort  * =  цел16;     (* 2-byte unsigned *)
  ALCuint    * =  цел32;    (* 4-byte signed *)

  ALCsizei   * =  цел32;  (* 4-byte signed *)

  ALCenum    * =  цел32;

  ALCcontext * =  адресВПамяти;
  ALCdevice  * =  адресВПамяти;

  PALCboolean * =  адресВПамяти; (*   TO ALCBoolean; *)
  PALCfloat   * =  адресВПамяти; (*   TO ALCfloat; *)
  PALCdouble  * =  адресВПамяти; (*   TO ALCDouble; *)
  PALCchar    * =  адресВПамяти; (*   TO ALCChar; *)
  PALCbyte    * =  адресВПамяти; (*   TO ALCbyte; *)
  PALCshort   * =  адресВПамяти; (*   TO ALCshort; *)
  PALCint     * =  адресВПамяти; (*   TO ALCint; *)
  PALCubyte   * =  адресВПамяти; (*   TO ALCubyte; *)
  PALCushort  * =  адресВПамяти; (*   TO ALCushort; *)
  PALCuint    * =  адресВПамяти; (*   TO ALCuint; *)

конст
  ALC_INVALID                             * =  (-1);
  ALC_FALSE                               * =  AL_FALSE;
  ALC_TRUE                                * =  AL_TRUE;

(** The Specifier string for default device *)
  ALC_MAJOR_VERSION                       * =  1000H;
  ALC_MINOR_VERSION                       * =  1001H;
  ALC_ATTRIBUTES_SIZE                     * =  1002H;
  ALC_ALL_ATTRIBUTES                      * =  1003H;
  ALC_DEFAULT_DEVICE_SPECIFIER            * =  1004H;
  ALC_DEVICE_SPECIFIER                    * =  1005H;
  ALC_EXTENSIONS                          * =  1006H;


 (** Capture extension *)
  ALC_CAPTURE_DEVICE_SPECIFIER     * = 310H;
  ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER * = 311H;
  ALC_CAPTURE_SAMPLES              * = 312H;

  ALC_FREQUENCY                           * =  1007H;  (* followed by <int> Hz *)
  ALC_REFRESH                             * =  1008H; (* followed by <int> Hz *)
  ALC_SYNC                                * =  1009H; (* followed by AL_TRUE, AL_FALSE *)
  ALC_MONO_SOURCES                 * = 1010H; (*  followed by <int> Num of requested Mono (3D) Sources *)
  ALC_STEREO_SOURCES               * = 1011H; (* followed by <int> Num of requested Stereo Sources *)

 (** errors *)
   ALC_NO_ERROR                            * =  ALC_FALSE;
  ALC_INVALID_DEVICE                      * =  0A001H;
  ALC_INVALID_CONTEXT                     * =  0A002H;
  ALC_INVALID_ENUM                        * =  0A003H;
  ALC_INVALID_VALUE       * =  0A004H;
  ALC_OUT_OF_MEMORY                       * =  0A005H;


(*!************ alext.h *********)
(****************************)

  AL_FORMAT_IMA_ADPCM_MONO16_EXT           * =10000H;
  AL_FORMAT_IMA_ADPCM_STEREO16_EXT         * =10001H;
  AL_FORMAT_WAVE_EXT                       * =10002H;

  AL_FORMAT_VORBIS_EXT                     * =10003H;


  AL_FORMAT_QUAD8_LOKI                     * =10004H;
  AL_FORMAT_QUAD16_LOKI                    * =10005H;


  AL_FORMAT_MONO_FLOAT32                   * =10010H;
  AL_FORMAT_STEREO_FLOAT32                 * =10011H;

  AL_FORMAT_MONO_DOUBLE_EXT                * =10012H;
  AL_FORMAT_STEREO_DOUBLE_EXT              * =10013H;

  ALC_CHAN_MAIN_LOKI                       * =500001H;
  ALC_CHAN_PCM_LOKI                        * =500002H;
  ALC_CHAN_CD_LOKI                         * =500003H;

  ALC_DEFAULT_ALL_DEVICES_SPECIFIER        * =1012H;
  ALC_ALL_DEVICES_SPECIFIER                * =1013H;

  AL_FORMAT_QUAD8                          * =1204H;
  AL_FORMAT_QUAD16                         * =1205H;
  AL_FORMAT_QUAD32                         * =1206H;
  AL_FORMAT_REAR8                          * =1207H;
  AL_FORMAT_REAR16                         * =1208H;
  AL_FORMAT_REAR32                         * =1209H;
  AL_FORMAT_51CHN8                         * =120AH;
  AL_FORMAT_51CHN16                        * =120BH;
  AL_FORMAT_51CHN32                        * =120CH;
  AL_FORMAT_61CHN8                         * =120DH;
  AL_FORMAT_61CHN16                        * =120EH;
  AL_FORMAT_61CHN32                        * =120FH;
  AL_FORMAT_71CHN8                         * =1210H;
  AL_FORMAT_71CHN16                        * =1211H;
  AL_FORMAT_71CHN32                        * =1212H;

  AL_FORMAT_MONO_MULAW                     * =10014H;
  AL_FORMAT_STEREO_MULAW                   * =10015H;
  AL_FORMAT_QUAD_MULAW                     * =10021H;
  AL_FORMAT_REAR_MULAW                     * =10022H;
  AL_FORMAT_51CHN_MULAW                    * =10023H;
  AL_FORMAT_61CHN_MULAW                    * =10024H;
  AL_FORMAT_71CHN_MULAW                    * =10025H;

  AL_FORMAT_MONO_IMA4                      * =1300H;
  AL_FORMAT_STEREO_IMA4                    * =1301H;

  ALC_CONNECTED                            * =313H;

  AL_SOURCE_DISTANCE_MODEL*               =  200H;

  AL_BYTE_RW_OFFSETS_SOFT*             = 1031H;
  AL_SAMPLE_RW_OFFSETS_SOFT*           = 1032H;

  AL_LOOP_POINTS_SOFT                * =     2015H;



перем

  oallib: адресВПамяти;

(* Renderer State management *)
alEnable-: проц { C } (capability : ALenum);
alDisable -: проц { C } (capability : ALenum);
alIsEnabled-: проц { C } (capability : ALenum) : ALboolean;
(* alHint-: PROCEDURE { C } (target, mode : ALenum); *)

(* State retrieval *)
alGetBoolean- : проц { C } (param : ALenum) : ALboolean;
alGetInteger-: проц { C } (param : ALenum) : ALint;
alGetFloat-: проц { C } (param : ALenum) : ALfloat;
alGetDouble-: проц { C } (param : ALenum) : ALdouble;
alGetBooleanv-: проц { C } (param : ALenum; перем data : ALboolean);
alGetIntegerv-: проц { C } (param : ALenum; перем data : ALint);
alGetFloatv-: проц { C } (param : ALenum;  перем data : ALfloat);
alGetDoublev-: проц { C } (param : ALenum; перем data : ALdouble);
alGetString-: проц { C } (param : ALenum) : PALubyte;
(*
 * Error support.
 * Obtain the most recent error generated in the AL state machine.
 *)
alGetError-: проц { C } () : ALenum;

(*
 * Extension support.
 * Query for the presence of an extension, and obtain any appropriate
 * function pointers and enum values.
 *)
alIsExtensionPresent-: проц { C } (конст fname : массив из симв8) : ALboolean;
alGetProcAddress-: проц { C } (конст fname : массив из симв8): адресВПамяти;
alGetEnumValue-: проц { C } (перем ename : ALubyte) : ALenum;

 (*  LISTENER
  Listener represents the location and orientation of the
  'user' in 3D-space.

  Properties include: -

  Gain         AL_GAIN         ALfloat
  Position     AL_POSITION     ALfloat[3]
  Velocity     AL_VELOCITY     ALfloat[3]
  Orientation  AL_ORIENTATION  ALfloat[6] (Forward then Up vectors)
*)

(* Set Listener parameters *)
alListenerf-: проц { C } (param : ALenum; value : ALfloat);
alListener3f-: проц { C } (param : ALenum; v1, v2, v3 : ALfloat);
alListenerfv-: проц { C } (param : ALenum;  values : PALfloat);
alListeneri-: проц { C } (param : ALenum; value : ALint);
alListener3i-: проц { C } (param : ALenum; v1, v2, v3 : ALint);
alListeneriv-: проц { C } (param : ALenum;  values : PALint);

(* Get Listener parameters *)
alGetListenerf-: проц { C } (param : ALenum; перем value : ALfloat);
alGetListener3f-: проц { C } (param : ALenum; перем v1, v2,  v3 : ALfloat);
alGetListenerfv-: проц { C } (param : ALenum; values : PALfloat);
alGetListeneri-: проц { C } (param : ALenum; перем value : ALint);
alGetListener3i-: проц { C } (param : ALenum; перем v1, v2, v3:  ALint);
alGetListeneriv-: проц { C } (param : ALenum; values : PALint);

(*
  SOURCE
  Sources represent individual sound objects in 3D-space.
  Sources take the PCM data provided in the specified Buffer,
  apply Source-specific modifications, and then
  submit them to be mixed according to spatial arrangement etc.

  Properties include: -

  Gain                              AL_GAIN                 ALfloat
  Min Gain                          AL_MIN_GAIN             ALfloat
  Max Gain                          AL_MAX_GAIN             ALfloat
  Position                          AL_POSITION             ALfloat[3]
  Velocity                          AL_VELOCITY             ALfloat[3]
  Direction                         AL_DIRECTION            ALfloat[3]
  Head Relative Mode                AL_SOURCE_RELATIVE      ALint (AL_TRUE or AL_FALSE)
  Reference Distance                AL_REFERENCE_DISTANCE   ALfloat
  Max Distance                      AL_MAX_DISTANCE         ALfloat
  RollOff Factor                    AL_ROLLOFF_FACTOR       ALfloat
  Inner Angle                       AL_CONE_INNER_ANGLE     ALint or ALfloat
  Outer Angle                       AL_CONE_OUTER_ANGLE     ALint or ALfloat
  Cone Outer Gain                   AL_CONE_OUTER_GAIN      ALint or ALfloat
  Pitch                             AL_PITCH                ALfloat
  Looping                           AL_LOOPING              ALint (AL_TRUE or AL_FALSE)
  MS Offset                         AL_MSEC_OFFSET          ALint or ALfloat
  Byte Offset                       AL_BYTE_OFFSET          ALint or ALfloat
  Sample Offset                     AL_SAMPLE_OFFSET        ALint or ALfloat
  Attached Buffer                   AL_BUFFER               ALint
  State (Query only)                AL_SOURCE_STATE         ALint
  Buffers Queued (Query only)       AL_BUFFERS_QUEUED       ALint
  Buffers Processed (Query only)    AL_BUFFERS_PROCESSED    ALint
 *)


(* Create Source objects *)
alGenSources-: проц { C } (n : ALsizei;  sources : PALuint);

(* Delete Source objects *)
alDeleteSources-: проц { C } (n : ALsizei; sources : PALuint);

(* Verify a handle is a valid Source *)
alIsSource-: проц { C } (id : ALuint) : ALboolean;

(* Set Source parameters *)
alSourcef-: проц { C } (source : ALuint; param : ALenum; value : ALfloat);
alSource3f-: проц { C } (source : ALuint; param : ALenum; v1, v2, v3 : ALfloat);
alSourcefv-: проц { C } (source : ALuint; param : ALenum; values : PALfloat);
alSourcei-: проц { C } (source : ALuint; param : ALenum; value : ALint);
alSource3i-: проц { C } (source : ALuint; param : ALenum; v1, v2, v3 : ALint);
alSourceiv-: проц { C } (source : ALuint; param : ALenum; values : PALint);

(* Get Source parameters *)
alGetSourcef-: проц { C } (source : ALuint; param : ALenum; перем value : ALfloat);
alGetSource3f-: проц { C } (source : ALuint; param : ALenum; перем v1, v2, v3 : ALfloat);
alGetSourcefv-: проц { C } (source : ALuint; param : ALenum; values : PALfloat);
alGetSourcei-: проц { C } (source : ALuint; param : ALenum; перем value : ALint);
alGetSource3i-: проц { C } (source : ALuint; param : ALenum; перем v1, v2, v3 : ALint);
alGetSourceiv-: проц { C } (source : ALuint; param : ALenum; values : PALint);

(* Source vector based playback calls *)

(* Play, replay, or resume (if paused) a list of Sources *)
alSourcePlayv-: проц { C } (n : ALsizei; sources : PALuint);

(* Pause a list of Sources *)
alSourcePausev-: проц { C } (n : ALsizei;  sources : PALuint);
(* Stop a list of Sources *)
alSourceStopv-: проц { C } (n : ALsizei;  sources : PALuint);
(* Rewind a list of Sources *)
alSourceRewindv-: проц { C } (n : ALsizei;  sources : PALuint);

(* Source based playback calls *)
(* Play, replay, or resume a Source *)
alSourcePlay-: проц { C } ( source : ALuint);
(* Pause a Source *)
alSourcePause-: проц { C } ( source : ALuint);
(* Stop a Source *)
alSourceStop-: проц { C } (source : ALuint);
(* Rewind a Source (set playback postiton to beginning)  *)
alSourceRewind-: проц { C }  (source : ALuint);

(*  Source Queuing  *)
alSourceQueueBuffers-: проц { C } (source : ALuint; n : ALsizei; buffers : PALuint);
alSourceUnqueueBuffers-: проц { C } (source : ALuint; n : ALsizei; buffers : PALuint);

(*
  BUFFER
  Buffer objects are storage space for sample data.
  Buffers are referred to by Sources. One Buffer can be used
  by multiple Sources.

  Properties include: -

  Frequency (Query only)    AL_FREQUENCY      ALint
  Size (Query only)         AL_SIZE           ALint
  Bits (Query only)         AL_BITS           ALint
  Channels (Query only)     AL_CHANNELS       ALint
 *)

(* Create Buffer objects *)
alGenBuffers-: проц { C } (n : ALsizei;  buffers : PALuint);
(* Delete Buffer objects *)
alDeleteBuffers-: проц { C } (n : ALsizei;  buffers : PALuint);
(* Verify a handle is a valid Buffer *)
alIsBuffer-: проц { C } (buffer : ALuint) : ALboolean;
(* Specify the data to be copied into a buffer *)
alBufferData-: проц { C } (buffer : ALuint; format : ALenum;  data: адресВПамяти ;  size, freq : ALsizei);

(* Set Buffer parameters *)
alBufferf-: проц { C } (buffer : ALuint; param : ALenum; value : ALfloat);
alBuffer3f-: проц { C } (buffer : ALuint; param : ALenum;  v1, v2, v3: ALfloat);
alBufferfv-: проц { C } (buffer : ALuint; param : ALenum;  value : PALfloat);
alBufferi-: проц { C } (buffer : ALuint; param : ALenum;  value : ALint);
alBuffer3i-: проц { C } (buffer : ALuint; param : ALenum;  v1, v2, v3 : ALint);
alBufferiv-: проц { C } (buffer : ALuint; param : ALenum;  value : PALint);

(* Get Buffer parameters *)
alGetBufferf-: проц { C } (buffer : ALuint; param : ALenum; перем value : ALfloat);
alGetBuffer3f-: проц { C } (buffer : ALuint; param : ALenum; перем v1, v2, v3: ALfloat);
alGetBufferfv-: проц { C } (buffer : ALuint; param : ALenum;  value : PALfloat);
alGetBufferi-: проц { C } (buffer : ALuint; param : ALenum; перем value : ALint);
alGetBuffer3i-: проц { C } (buffer : ALuint; param : ALenum; перем v1, v2, v3 : ALint);
alGetBufferiv-: проц { C } (buffer : ALuint; param : ALenum;  value : PALint);

(* Global Parameters *)
alDistanceModel-: проц { C } (value : ALenum);
alDopplerFactor-: проц { C } (value : ALfloat);
alSpeedOfSound-: проц { C } (value : ALfloat);
alDopplerVelocity-: проц { C } (value : ALfloat);




(* Device Management *)
(*! alcOpenDevice-: PROCEDURE { C } (CONST deviceName : ARRAY OF CHAR) : ALCdevice; *)
alcOpenDeviceXXX-: проц { C } (deviceName : адресВПамяти) : ALCdevice;

alcCloseDevice-: проц { C } (device : ALCdevice): ALCboolean;
(* Context Management *)
alcCreateContext-: проц { C } (device : ALCdevice; attrList : PALCint) : ALCcontext;
alcMakeContextCurrent-: проц { C } ( context : ALCcontext) : ALCboolean;
alcProcessContext-: проц { C } (context : ALCcontext);
alcGetCurrentContext-: проц { C } () : ALCcontext;
alcGetContextsDevice-: проц { C } (context : ALCcontext) : ALCdevice;
alcSuspendContext-: проц { C } (context : ALCcontext);
alcDestroyContext-: проц { C } (context : ALCcontext);


 (* Error support.
 * Obtain the most recent Context error
 *)
alcGetError-: проц { C } (device : ALCdevice) : ALCenum;


(*
 * Extension support.
 * Query for the presence of an extension, and obtain any appropriate
 * function pointers and enum values.
 *)
(*! alcIsExtensionPresent- : PROCEDURE { C } (device : ALCdevice; CONST extName : ARRAY OF CHAR) : ALCboolean; *)
alcIsExtensionPresentXXX- : проц { C } (device : ALCdevice; extName : адресВПамяти) : ALCboolean;

(*! alcGetProcAddress-: PROCEDURE { C } (device : ALCdevice; CONST funcName:  ARRAY OF CHAR): ADDRESS; *)
alcGetProcAddressXXX-: проц { C } (device : ALCdevice; funcName:  адресВПамяти): адресВПамяти;

alcGetEnumValue-: проц { C } (device : ALCdevice; перем enumName : ALCubyte) : ALCenum;




(* Query functions *)
alcGetString-: проц { C } (device : ALCdevice; param : ALCenum) : PALCchar;
alcGetIntegerv -: проц { C } ( device : ALCdevice; param : ALCenum; size : ALCsizei;  data : PALCint);

(* Capture functions *)
(*! alcCaptureOpenDevice-:  PROCEDURE { C } (CONST devicename: ARRAY OF CHAR; frequency: ALCuint; format: ALCenum; buffersize: ALCsizei): ALCdevice; *)
alcCaptureOpenDeviceXXX-:  проц { C } (devicename: адресВПамяти;
										frequency: ALCuint; format: ALCenum; buffersize: ALCsizei): ALCdevice;



alcCaptureCloseDevice-: проц { C } (device: ALCdevice): ALCboolean;
alcCaptureStart-:  проц { C } (device: ALCdevice);
alcCaptureStop-: проц { C } (device: ALCdevice);
alcCaptureSamples-: проц { C } (device: ALCdevice; buffer: адресВПамяти; samples: ALCsizei);

(*
(* extensions *)
alBufferDataStatic-: PROCEDURE { C } (buffer: ALint; format: ALenum; data: ADDRESS; lenx: ALsizei;  freq: ALsizei);
alcSetThreadContext-: PROCEDURE { C }(context: ALCcontext ): ALCboolean;
alcGetThreadContext-: PROCEDURE { C } (): ALCcontext;
alBufferSubDataSOFT-: PROCEDURE { C } (buffer: ALuint; format: ALenum; data: ADDRESS; offset: ALsizei; lengthx: ALsizei);
*)

проц LoadFunctions;
нач
		oallib := Unix.Dlopen(libname, 2 );
		утв(oallib # 0, 103);

		Unix.Dlsym( oallib, "alEnable", адресОт( alEnable));
		Unix.Dlsym( oallib, "alDisable", адресОт( alDisable));
		Unix.Dlsym( oallib, "alIsEnabled", адресОт( alIsEnabled));
		Unix.Dlsym( oallib, "alGetBoolean", адресОт( alGetBoolean));
		Unix.Dlsym( oallib, "alGetInteger", адресОт( alGetInteger));
		Unix.Dlsym( oallib, "alGetFloat", адресОт( alGetFloat));
		Unix.Dlsym( oallib, "alGetDouble", адресОт( alGetDouble));
		Unix.Dlsym( oallib, "alGetBooleanv", адресОт( alGetBooleanv));
		Unix.Dlsym( oallib, "alGetIntegerv", адресОт( alGetIntegerv));
		Unix.Dlsym( oallib, "alGetFloatv", адресОт( alGetFloatv));
		Unix.Dlsym( oallib, "alGetDoublev", адресОт( alGetDoublev));
		Unix.Dlsym( oallib, "alGetString", адресОт( alGetString));
		Unix.Dlsym( oallib, "alGetError", адресОт( alGetError));
		Unix.Dlsym( oallib, "alIsExtensionPresent", адресОт( alIsExtensionPresent));
		Unix.Dlsym( oallib, "alGetProcAddress", адресОт( alGetProcAddress));
		Unix.Dlsym( oallib, "alGetEnumValue", адресОт( alGetEnumValue));

		Unix.Dlsym( oallib, "alListenerf", адресОт( alListenerf));
		Unix.Dlsym( oallib, "alListener3f", адресОт( alListener3f));
		Unix.Dlsym( oallib, "alListenerfv", адресОт( alListenerfv));
		Unix.Dlsym( oallib, "alListeneri", адресОт( alListeneri));
		Unix.Dlsym( oallib, "alListener3i", адресОт( alListener3i));
		Unix.Dlsym( oallib, "alListeneriv", адресОт( alListeneriv));

		Unix.Dlsym( oallib, "alGetListenerf", адресОт( alGetListenerf));
		Unix.Dlsym( oallib, "alGetListener3f", адресОт( alGetListener3f));
		Unix.Dlsym( oallib, "alGetListenerfv", адресОт( alGetListenerfv));
		Unix.Dlsym( oallib, "alGetListeneri", адресОт( alGetListeneri));
		Unix.Dlsym( oallib, "alGetListener3i", адресОт( alGetListener3i));
		Unix.Dlsym( oallib, "alGetListeneriv", адресОт( alGetListeneriv));

		Unix.Dlsym( oallib, "alGenSources", адресОт( alGenSources));
		Unix.Dlsym( oallib, "alDeleteSources", адресОт( alDeleteSources));
		Unix.Dlsym( oallib, "alIsSource", адресОт( alIsSource));
		Unix.Dlsym( oallib, "alSourcei", адресОт( alSourcei));

		Unix.Dlsym( oallib, "alSourcef", адресОт( alSourcef));
		Unix.Dlsym( oallib, "alSource3f", адресОт( alSource3f));
		Unix.Dlsym( oallib, "alSourcefv", адресОт( alSourcefv));
		Unix.Dlsym( oallib, "alSourcei", адресОт( alSourcei));
		Unix.Dlsym( oallib, "alSource3i", адресОт( alSource3i));
		Unix.Dlsym( oallib, "alSourceiv", адресОт( alSourceiv));

		Unix.Dlsym( oallib, "alGetSourcef", адресОт( alGetSourcef));
		Unix.Dlsym( oallib, "alGetSource3f", адресОт( alGetSource3f));
		Unix.Dlsym( oallib, "alGetSourcefv", адресОт( alGetSourcefv));
		Unix.Dlsym( oallib, "alGetSourcei", адресОт( alGetSourcei));
		Unix.Dlsym( oallib, "alGetSource3i", адресОт( alGetSource3i));
		Unix.Dlsym( oallib, "alGetSourceiv", адресОт( alGetSourceiv));

		Unix.Dlsym( oallib, "alSourcePlayv", адресОт( alSourcePlayv));
		Unix.Dlsym( oallib, "alSourcePausev", адресОт( alSourcePausev));
		Unix.Dlsym( oallib, "alSourceStopv", адресОт( alSourceStopv));
		Unix.Dlsym( oallib, "alSourceRewindv", адресОт( alSourceRewindv));
		Unix.Dlsym( oallib, "alSourcePlay", адресОт( alSourcePlay));
		Unix.Dlsym( oallib, "alSourcePause", адресОт( alSourcePause));
		Unix.Dlsym( oallib, "alSourceStop", адресОт( alSourceStop));
		Unix.Dlsym( oallib, "alSourceRewind", адресОт( alSourceRewind));

		Unix.Dlsym( oallib, "alGenBuffers", адресОт( alGenBuffers));
		Unix.Dlsym( oallib, "alDeleteBuffers", адресОт( alDeleteBuffers));
		Unix.Dlsym( oallib, "alIsBuffer", адресОт( alIsBuffer));
		Unix.Dlsym( oallib, "alBufferData", адресОт( alBufferData));

		Unix.Dlsym( oallib, "alBufferf", адресОт( alBufferf));
		Unix.Dlsym( oallib, "alBuffer3f", адресОт( alBuffer3f));
		Unix.Dlsym( oallib, "alBufferfv", адресОт( alBufferfv));
		Unix.Dlsym( oallib, "alBufferi", адресОт( alBufferi));
		Unix.Dlsym( oallib, "alBuffer3i", адресОт( alBuffer3i));
		Unix.Dlsym( oallib, "alBufferiv", адресОт( alBufferiv));

		Unix.Dlsym( oallib, "alGetBufferf", адресОт( alGetBufferf));
		Unix.Dlsym( oallib, "alGetBuffer3f", адресОт( alGetBuffer3f));
		Unix.Dlsym( oallib, "alGetBufferfv", адресОт( alGetBufferfv));
		Unix.Dlsym( oallib, "alGetBufferi", адресОт( alGetBufferi));
		Unix.Dlsym( oallib, "alGetBuffer3i", адресОт( alGetBuffer3i));
		Unix.Dlsym( oallib, "alGetBufferiv", адресОт( alGetBufferiv));

		Unix.Dlsym( oallib, "alSourceQueueBuffers", адресОт( alSourceQueueBuffers));
		Unix.Dlsym( oallib, "alSourceUnqueueBuffers", адресОт( alSourceUnqueueBuffers));
		Unix.Dlsym( oallib, "alDistanceModel", адресОт( alDistanceModel));
		Unix.Dlsym( oallib, "alDopplerFactor", адресОт( alDopplerFactor));
		Unix.Dlsym( oallib, "alSpeedOfSound", адресОт( alSpeedOfSound));
		Unix.Dlsym( oallib, "alDopplerVelocity", адресОт( alDopplerVelocity));


		Unix.Dlsym( oallib, "alcOpenDevice", адресОт( alcOpenDeviceXXX));
		Unix.Dlsym( oallib, "alcCloseDevice", адресОт( alcCloseDevice));
		Unix.Dlsym( oallib, "alcCreateContext", адресОт( alcCreateContext));
		Unix.Dlsym( oallib, "alcMakeContextCurrent", адресОт( alcMakeContextCurrent));
		Unix.Dlsym( oallib, "alcProcessContext", адресОт( alcProcessContext));
		Unix.Dlsym( oallib, "alcGetCurrentContext", адресОт( alcGetCurrentContext));
		Unix.Dlsym( oallib, "alcGetContextsDevice", адресОт( alcGetContextsDevice));
		Unix.Dlsym( oallib, "alcSuspendContext", адресОт( alcSuspendContext));
		Unix.Dlsym( oallib, "alcDestroyContext", адресОт( alcDestroyContext));
		Unix.Dlsym( oallib, "alcGetError", адресОт( alcGetError));
		Unix.Dlsym( oallib, "alcGetString", адресОт( alcGetString));
		Unix.Dlsym( oallib, "alcGetIntegerv", адресОт( alcGetIntegerv));

		Unix.Dlsym( oallib, "alcIsExtensionPresent", адресОт( alcIsExtensionPresentXXX));
		Unix.Dlsym( oallib, "alcGetProcAddress", адресОт( alcGetProcAddressXXX));
		Unix.Dlsym( oallib, "alcGetEnumValue", адресОт( alcGetEnumValue));

		Unix.Dlsym( oallib, "alcCaptureOpenDevice", адресОт( alcCaptureOpenDeviceXXX));
		Unix.Dlsym( oallib, "alcCaptureCloseDevice", адресОт( alcCaptureCloseDevice));
		Unix.Dlsym( oallib, "alcCaptureStart", адресОт( alcCaptureStart));
		Unix.Dlsym( oallib, "alcCaptureStop", адресОт( alcCaptureStop));
		Unix.Dlsym( oallib, "alcCaptureSamples", адресОт( alcCaptureSamples));

(*
		(* extensions *)
		Unix.Dlsym( oallib, "alBufferDataStatic", ADDRESSOF( alBufferDataStatic));
		Unix.Dlsym( oallib, "alcSetThreadContext", ADDRESSOF( alcSetThreadContext));
		Unix.Dlsym( oallib, "alcGetThreadContext", ADDRESSOF( alcGetThreadContext));
		Unix.Dlsym( oallib, "alBufferSubDataSOFT", ADDRESSOF( alBufferSubDataSOFT));
*)

кон LoadFunctions;

проц OnClose;
нач
 если oallib # 0 то
       Unix.Dlclose(oallib);
       ЛогЯдра.пСтроку8(libname); ЛогЯдра.пСтроку8(' unloaded.'); ЛогЯдра.пВК_ПС;
всё;
кон OnClose;

(* utilities, and wrappers  *)

проц alcOpenDevice*( конст deviceName: массив из симв8 ): ALCdevice;
перем dev: ALCdevice;
нач
	dev := alcOpenDeviceXXX( адресОт( deviceName[0] ) );
	возврат dev
кон alcOpenDevice;

проц alcCaptureOpenDevice*( конст deviceName: массив из симв8;
								frequency: ALCuint; format: ALCenum; buffersize: ALCsizei): ALCdevice;
перем dev: ALCdevice;
нач
	dev := alcCaptureOpenDeviceXXX( адресОт( deviceName[0] ), frequency, format, buffersize );
	возврат dev
кон alcCaptureOpenDevice;

проц alcIsExtensionPresent* (device : ALCdevice; конст extName : массив из симв8) : ALCboolean;
перем res: ALCboolean;
нач
	res := alcIsExtensionPresentXXX(device, адресОт(extName[0]));
	возврат res;
кон alcIsExtensionPresent;

проц alcGetProcAddress* (device : ALCdevice; конст funcName: массив из симв8): адресВПамяти;
перем adr: адресВПамяти;
нач
	adr := alcGetProcAddressXXX (device, адресОт(funcName[0]));
	возврат adr;
кон alcGetProcAddress;

(* Get string from address *)
проц GetStringFromAddr(adr: адресВПамяти): Strings.String;
перем  sadr, sadr1: адресВПамяти;
		i, cnt: цел32;
		ch: симв8;
		s: Strings.String;
нач
	sadr := adr;

	(* find length *)
	cnt :=0;
	sadr1 := sadr;
	если sadr1 # 0 то
		НИЗКОУР.прочтиОбъектПоАдресу(sadr1,ch);
		нцПока (ch # 0X) делай  увел(cnt); увел(sadr1); НИЗКОУР.прочтиОбъектПоАдресу(sadr1,ch); кц;
	всё;

	если cnt = 0 то  (* empty string *)
		нов(s,1); s[0]:=0X; 	возврат s
	всё;

	нов(s, cnt+1);
	i:=0;
	sadr1 := sadr;
	НИЗКОУР.прочтиОбъектПоАдресу(sadr1,ch);
	нцПока (i< cnt) и (ch # 0X) делай
		s^[i] := ch; увел(i); увел(sadr1);
		НИЗКОУР.прочтиОбъектПоАдресу(sadr1,ch);
	кц;
	возврат s;

кон GetStringFromAddr;


проц ALGetString*( name: ALCenum): Strings.String;
перем  sadr: адресВПамяти;
нач
	sadr := alGetString( name);
	возврат GetStringFromAddr(sadr);
кон ALGetString;

проц ALCGetString*(device: ALCdevice; name: ALCenum): Strings.String;
перем  sadr: адресВПамяти;
нач
	sadr := alcGetString(device, name);
	возврат GetStringFromAddr(sadr);
кон ALCGetString;


проц GetDevStringFromAddr(adr: адресВПамяти): Strings.String;
перем  sadr, sadr1: адресВПамяти;
		i, cnt: цел32;
		ch: симв8;
		s: Strings.String;
нач
	sadr := adr;

	(* find length *)
	cnt :=0;
	sadr1 := sadr;
	если sadr1 # 0 то
		НИЗКОУР.прочтиОбъектПоАдресу(sadr1,ch);
		нцПока (ch # 0X) делай  увел(cnt); увел(sadr1); НИЗКОУР.прочтиОбъектПоАдресу(sadr1,ch); кц;
	всё;

	если cnt = 0 то  (* empty string or end of list *)
		возврат НУЛЬ
	всё;

	(* copy chars to string *)
	нов(s, cnt+1);
	i:=0;
	sadr1 := sadr;
	НИЗКОУР.прочтиОбъектПоАдресу(sadr1,ch);
	нцПока (i< cnt) и (ch # 0X) делай
		s^[i] := ch; увел(i); увел(sadr1);
		НИЗКОУР.прочтиОбъектПоАдресу(sadr1,ch);
	кц;
	возврат s;

кон GetDevStringFromAddr;

(** go through device list,  (each device terminated with a single NULL, list terminated with double NULL *)
проц ALCGetDeviceList*(device: ALCdevice; name: ALCenum): PString256;
перем  sadr, sadr0: адресВПамяти;
	slen, index: размерМЗ;
	str: Strings.String;
	dynstr: PString256;
нач

		(* count the number of devices in the list *)
		sadr0 := alcGetString(device, name);
		sadr := sadr0;
		str := GetDevStringFromAddr(sadr);
		index := 0;
		нцПока (str  # НУЛЬ) и (index<10)  делай (* limit the count *)
      			slen := Strings.Length(str^);
			sadr := sadr + slen +1;
			str := GetDevStringFromAddr(sadr);
			увел(index);
		кц;


		(* copy to string list *)
		нов(dynstr, index);
		sadr0 := alcGetString(device, name);
		sadr := sadr0;
		str := GetDevStringFromAddr(sadr);
		index := 0;
		нцПока (str  # НУЛЬ) и (index<10)  делай
		       копируйСтрокуДо0(str^, dynstr^[index]);
      			slen := Strings.Length(str^);
			sadr := sadr + slen +1;
			str := GetDevStringFromAddr(sadr);
			увел(index);
		кц;

	возврат dynstr;
кон ALCGetDeviceList;


нач
LoadFunctions;
Modules.InstallTermHandler(OnClose) ;

кон OpenAL.

System.Free OpenAL~

