модуль Bimso;	(** AUTHOR "TF"; PURPOSE "Template/Example  for component windows"; *)

(** This program shows the implementation of a multi instance component containing window *)

использует
	Strings, WMGraphics, WMMessages, WMComponents, WMStandardComponents,
	Modules, ЛогЯдра, WMRectangles, WMGraphicUtilities, Random, Kernel, WMDialogs,
	WM := WMWindowManager;


конст
	MaxLevel = 1000;

тип
	KillerMsg = окласс
	кон KillerMsg;

	Window* = окласс (WMComponents.FormWindow)
	перем b : массив 4 из WMStandardComponents.Button;
		startButton : WMStandardComponents.Button;
		start, alive : булево;
		c, cflash : массив 4 из WMGraphics.Color;
		random : Random.Generator;
		game : массив MaxLevel из цел8;
		level : цел32;
		step : цел32;
		error : булево;
		timer : Kernel.Timer;
		s, levelStr : массив 32 из симв8;

		проц CreateForm() : WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			i : цел32;
		нач
			нов(panel); panel.bounds.SetExtents(200, 200);
			panel.fillColor.Set(цел32(0DDFFDDFFH)); panel.takesFocus.Set(истина);

			нов(b[0]); b[0].alignment.Set(WMComponents.AlignTop); b[0].bounds.SetHeight(50);
			b[0].bearing.Set(WMRectangles.MakeRect(50,0,50,0));
			panel.AddContent(b[0]);

			нов(b[1]); b[1].alignment.Set(WMComponents.AlignBottom); b[1].bounds.SetHeight(50);
			b[1].bearing.Set(WMRectangles.MakeRect(50,0,50,0));
			panel.AddContent(b[1]);

			нов(b[2]); b[2].alignment.Set(WMComponents.AlignLeft); b[2].bounds.SetWidth(50);
			panel.AddContent(b[2]);

			нов(b[3]); b[3].alignment.Set(WMComponents.AlignRight); b[3].bounds.SetWidth(50);
			panel.AddContent(b[3]);

			нов(startButton); startButton.alignment.Set(WMComponents.AlignClient);
			startButton.caption.SetAOC("Start!"); startButton.onClick.Add(Start);
			panel.AddContent(startButton);

			(* set the default colours of the buttons *)
			нцДля i := 0 до 3 делай b[i].clDefault.Set(c[i]) кц;
			(* set the same color for the case where the mouse is over *)
			нцДля i := 0 до 3 делай b[i].clHover.Set(c[i]) кц;
			(* set the flashcolor for the case where the button is pressed*)
			нцДля i := 0 до 3 делай b[i].clPressed.Set(cflash[i]) кц;

			нцДля i := 0 до 3 делай b[i].onClick.Add(Evaluate) кц;

			возврат panel
		кон CreateForm;

		проц &New*;
		перем vc : WMComponents.VisualComponent;
			i : цел32;
		нач
			IncCount;
			(* initialize the button colours *)
			cflash[0] := WMGraphics.Green; cflash[1] := WMGraphics.Red;
			cflash[2] := WMGraphics.Blue; cflash[3] := WMGraphics.Magenta;
			(* scale the default colours to 50% of the flash colour *)
			нцДля i := 0 до 3 делай c[i] := WMGraphicUtilities.ScaleColor(cflash[i], 128) кц;

			(* a timer may not be shared between processes so instantiate a new one *)
			нов(timer);

			(* To create a multi language app, try loading the respective XML instead of CreateForm()
			if the XML was not found or does not contain all needed elements, use CreateForm as fallback *)
			vc := CreateForm();

			(* create a new random number generator *)
			нов(random);
			random.InitSeed(Kernel.GetTicks());

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);

			 WM.DefaultAddWindow(сам);
			SetTitle(Strings.NewString("Bimso Game"));
		кон New;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount
		кон Close;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то Close
			иначе Handle^(x)
			всё
		кон Handle;

		проц CreateLevel;
		нач
			game[level] := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(random.Dice(4)));
			увел(level)
		кон CreateLevel;

		проц ShowLevel;
		перем i : цел32;
		нач
			нцДля i := 0 до level - 1 делай
				b[game[i]].clDefault.Set(cflash[game[i]]);
				timer.Sleep(150);
				b[game[i]].clDefault.Set(c[game[i]]);
				timer.Sleep(150);
			кц;
		кон ShowLevel;

		проц EnableInput;
		перем i : цел32;
		нач
			нцДля i := 0 до 3 делай b[i].enabled.Set(истина) кц;
		кон EnableInput;

		проц DisableInput;
		перем i : цел32;
		нач
			нцДля i := 0 до 3 делай b[i].enabled.Set(ложь) кц;
		кон DisableInput;

		проц Evaluate(sender, data : динамическиТипизированныйУкль);
		перем i : цел32;
		нач  {единолично}
			нцДля i := 0 до 3 делай
				если sender = b[i] то
					если game[step] = i то увел(step) иначе error := истина всё;
					возврат
				всё
			кц
		кон Evaluate;

		проц Start(sender, data : динамическиТипизированныйУкль);
		нач
			Started;
			startButton.visible.Set(ложь)
		кон Start;

		проц Started;
		нач	{единолично}
			start := истина
		кон Started;

		проц Play;
		нач {единолично}
			step := 0;
			EnableInput;
			дождись(error или (step = level))
		кон Play;

	нач {активное}
		alive := истина;
		нцПока alive делай
			start := ложь;
			error := ложь; level := 0;
			startButton.visible.Set(истина);
			нач {единолично}
				дождись(start)
			кон;
			error := ложь;
			нцДо
				ЛогЯдра.пВК_ПС;
				DisableInput;
				timer.Sleep(500);
				CreateLevel;
				ShowLevel;
				Play;
			кцПри error или (level >= MaxLevel);
			если level = MaxLevel то
				WMDialogs.Information("You win !!", "There are no more levels")
			иначе
				Strings.IntToStr(level, levelStr);
				s := "You made it to level "; Strings.Append(s, levelStr);
				WMDialogs.Information("You lose !!", s)
			всё;
			alive := WMDialogs.Message(WMDialogs.TQuestion, "Play again ?", "Do you want to play again ?",
					{WMDialogs.ResYes, WMDialogs.ResNo}) = WMDialogs.ResYes

		кц;
		Close
	кон Window;

перем
	nofWindows : цел32;

проц Open*;
перем winstance : Window;
нач
	нов(winstance);
кон Open;

проц IncCount;
нач {единолично}
	увел(nofWindows);
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows);
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон Bimso.

System.Free Bimso ~
Bimso.Open ~
