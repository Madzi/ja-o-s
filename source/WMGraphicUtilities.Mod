модуль WMGraphicUtilities;	(** AUTHOR "TF"; PURPOSE "Tools using WMGraphics"; *)

использует
	WMGraphics, WMRectangles;

тип
	EllipsePixelsFiller* = проц(конст canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; dx, dy : размерМЗ; lineColor, fillColor : WMGraphics.Color; mode : целМЗ);

(* factor in 1/256, alpha remains unchanged *)
проц ScaleColor*(color : WMGraphics.Color; factor : цел32): WMGraphics.Color;
перем r, g, b, a : цел32;
нач
	WMGraphics.ColorToRGBA(color, r, g, b, a);
	r := матМинимум(r * factor DIV 256, 255);
	g := матМинимум(g * factor DIV 256, 255);
	b := матМинимум(b * factor DIV 256, 255);
	возврат WMGraphics.RGBAToColor(r, g, b, a)
кон ScaleColor;

(** linear interpolation percent in [0..256] *)
проц InterpolateLinear*(a, b, percent : цел32) : цел32;
нач
	возврат ((a * (256 - percent)) + b * percent) DIV 256
кон InterpolateLinear;

(* interpolate between two colors; percent [0..256]*)
проц InterpolateColorLinear*(cl0, cl1: WMGraphics.Color; percent : цел32) : WMGraphics.Color;
перем r0, g0, b0, a0, r1, g1, b1, a1: цел32;
нач
	WMGraphics.ColorToRGBA(cl0, r0, g0, b0, a0);
	WMGraphics.ColorToRGBA(cl1, r1, g1, b1, a1);
	возврат WMGraphics.RGBAToColor(InterpolateLinear(r0, r1, percent),
			InterpolateLinear(g0, g1, percent),
			InterpolateLinear(b0, b1, percent),
			InterpolateLinear(a0, a1, percent))
кон InterpolateColorLinear;

(** Draw a 3d effect border *)
проц DrawBevel*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; borderWidth : размерМЗ; down : булево; color: WMGraphics.Color; mode : целМЗ);
перем i: размерМЗ; ul, dr : WMGraphics.Color;
нач
	если down то ul := ScaleColor(color, 128); dr :=  ScaleColor(color, 256 + 128)
	иначе dr := ScaleColor(color, 128); ul :=  ScaleColor(color, 256 + 128)
	всё;
	нцДля i := 0 до borderWidth - 1 делай
		canvas.Fill(WMRectangles.MakeRect(rect.l + i , rect.t + i, rect.r - i, rect.t + i + 1), ul, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.l + i, rect.t + i + 1, rect.l + i + 1, rect.b - i), ul, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.l + 1 + i, rect.b - 1 - i, rect.r - i, rect.b - i), dr, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.r - 1 - i, rect.t + 1 + i, rect.r - i, rect.b - i - 1), dr, mode)
	кц
кон DrawBevel;

(** Draw a 3d effect panel *)
проц DrawBevelPanel*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; borderWidth : размерМЗ; down : булево; color: WMGraphics.Color; mode : целМЗ);
нач
	canvas.Fill(WMRectangles.ResizeRect(rect, -1), color, mode);
	DrawBevel(canvas, rect, borderWidth, down, color, mode)
кон DrawBevelPanel;

проц FillGradientHorizontal*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; clLeft, clRight: WMGraphics.Color; mode : целМЗ);
перем dist, i : размерМЗ; f : цел32; cl: WMGraphics.Color;
нач
	dist := rect.r - rect.l;
	нцДля i := 0 до dist - 1 делай
		f := округлиВниз(256 * i / dist);
		cl := InterpolateColorLinear(clLeft, clRight, f);
		canvas.Fill(WMRectangles.MakeRect(rect.l + i, rect.t, rect.l + i + 1, rect.b), cl, mode)
	кц;
кон FillGradientHorizontal;

проц FillGradientVertical*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; clTop, clBottom: WMGraphics.Color; mode : целМЗ);
перем dist, i : размерМЗ; f : цел32; cl: WMGraphics.Color;
нач
	dist := rect.b - rect.t;
	нцДля i := 0 до dist - 1 делай
		f := округлиВниз(256 * i / dist);
		cl := InterpolateColorLinear(clTop, clBottom, f);
		canvas.Fill(WMRectangles.MakeRect(rect.l, rect.t + i, rect.r, rect.t + i + 1), cl, mode)
	кц;
кон FillGradientVertical;

проц FillRoundHorizontalBar*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; down : булево; color: WMGraphics.Color; mode : целМЗ);
перем cl2: WMGraphics.Color; d : размерМЗ;
нач
	cl2 := ScaleColor(color, 200);
	если down то d := (rect.b - rect.t) * 5 DIV 16;
	иначе d := (rect.b - rect.t) * 11 DIV 16
	всё;
	FillGradientVertical(canvas, WMRectangles.MakeRect(rect.l, rect.t, rect.r, rect.t + d), color, cl2, WMGraphics.ModeCopy);
	FillGradientVertical(canvas, WMRectangles.MakeRect(rect.l, rect.t + d, rect.r, rect.b), cl2, color, WMGraphics.ModeCopy);
кон FillRoundHorizontalBar;

проц FillRoundVerticalBar*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; down : булево; color: WMGraphics.Color; mode : целМЗ);
перем cl2: WMGraphics.Color; d : размерМЗ;
нач
	cl2 := ScaleColor(color, 200);
	если down то d := (rect.r - rect.l) * 5 DIV 16;
	иначе d := (rect.r - rect.l) * 11 DIV 16
	всё;
	FillGradientHorizontal(canvas, WMRectangles.MakeRect(rect.l, rect.t, rect.l + d, rect.b), color, cl2, WMGraphics.ModeCopy);
	FillGradientHorizontal(canvas, WMRectangles.MakeRect(rect.l + d, rect.t, rect.r, rect.b), cl2, color, WMGraphics.ModeCopy);
кон FillRoundVerticalBar;

проц DrawRect*(canvas : WMGraphics.Canvas; r : WMRectangles.Rectangle; color : WMGraphics.Color; mode : целМЗ);
нач
	canvas.Fill(WMRectangles.MakeRect(r.l, r.t, r.r, r.t + 1), color, mode);
	canvas.Fill(WMRectangles.MakeRect(r.l, r.t, r.l + 1, r.b), color, mode);
	canvas.Fill(WMRectangles.MakeRect(r.l, r.b - 1, r.r, r.b), color, mode);
	canvas.Fill(WMRectangles.MakeRect(r.r - 1, r.t, r.r, r.b), color, mode)
кон DrawRect;

проц RectGlassShade*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; borderWidth : цел32; down : булево);
перем i, ul, dr, da, w : цел32;
нач
	если borderWidth <= 0 то возврат всё;
	если down то ul := 090H; dr := цел32(0FFFFFF90H)
	иначе dr := 090H; ul := цел32(0FFFFFF90H)
	всё;
	da := 90H DIV borderWidth;
	нцДля i := 0 до borderWidth - 1 делай
		(* top *)
		canvas.Fill(WMRectangles.MakeRect(rect.l + i , rect.t + i, rect.r - i, rect.t + i + 1), ul, WMGraphics.ModeSrcOverDst);
		(* left *)
		canvas.Fill(WMRectangles.MakeRect(rect.l + i, rect.t + i + 1, rect.l + i + 1, rect.b - i), ul, WMGraphics.ModeSrcOverDst);
		(* bottom *)
		canvas.Fill(WMRectangles.MakeRect(rect.l + 1 + i, rect.b - 1 - i, rect.r - i, rect.b - i), dr, WMGraphics.ModeSrcOverDst);
		(* right *)
		canvas.Fill(WMRectangles.MakeRect(rect.r - 1 - i, rect.t + 1 + i, rect.r - i, rect.b - i - 1), dr, WMGraphics.ModeSrcOverDst);
		умень(ul, da); умень(dr, da)
	кц;
	i := 3; ul := цел32(0FFFFFF40H); w := 5;
	canvas.Fill(WMRectangles.MakeRect(rect.l + i , rect.t + i, rect.l + i + w, rect.t + i + 2), ul, WMGraphics.ModeSrcOverDst);
	canvas.Fill(WMRectangles.MakeRect(rect.l + i, rect.t + i, rect.l + i + 2, rect.t + i + w), ul, WMGraphics.ModeSrcOverDst);
кон RectGlassShade;

проц ExtRectGlassShade*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; openSides : мнвоНаБитахМЗ; borderWidth : цел32; down : булево);
перем i, ul, dr, da, w, a, b, c, d : цел32;
нач
	если borderWidth <= 0 то возврат всё;
	если down то ul := 090H; dr := цел32(0FFFFFF90H)
	иначе dr := 090H; ul := цел32(0FFFFFF90H)
	всё;
	da := 90H DIV borderWidth;
	нцДля i := 0 до borderWidth - 1 делай
		если  (0 в openSides) то a := 0 иначе a := i всё;
		если  (1 в openSides) то b := 0 иначе b := i + 1 всё;
		если  (2 в openSides) то c := 0 иначе c := i всё;
		если  (3 в openSides) то d := 0 иначе d := i + 1 всё;
		(* top *)
		если ~(0 в openSides) то canvas.Fill(WMRectangles.MakeRect(rect.l + b , rect.t + i, rect.r - d, rect.t + i + 1), ul, WMGraphics.ModeSrcOverDst) всё;
		(* left *)
		если ~(1 в openSides) то canvas.Fill(WMRectangles.MakeRect(rect.l + i, rect.t + a, rect.l + i + 1, rect.b - c), ul, WMGraphics.ModeSrcOverDst) всё;
		(* bottom *)
		если ~(2 в openSides) то canvas.Fill(WMRectangles.MakeRect(rect.l + b, rect.b - 1 - i, rect.r - d, rect.b - i), dr, WMGraphics.ModeSrcOverDst) всё;
		(* right *)
		если ~(3 в openSides) то canvas.Fill(WMRectangles.MakeRect(rect.r - 1 - i, rect.t + a, rect.r - i, rect.b - c), dr, WMGraphics.ModeSrcOverDst) всё;
		умень(ul, da); умень(dr, da)
	кц;
	i := 3; ul := цел32(0FFFFFF40H); w := 5;
(*	canvas.Fill(WMRectangles.MakeRect(rect.l + i , rect.t + i, rect.l + i + w, rect.t + i + 2), ul, WMGraphics.ModeSrcOverDst);
	canvas.Fill(WMRectangles.MakeRect(rect.l + i, rect.t + i, rect.l + i + 2, rect.t + i + w), ul, WMGraphics.ModeSrcOverDst);
*)
кон ExtRectGlassShade;

(** repeats img in x-direction and scales it in y-direction to fill the specified rectangle *)
проц RepeatImageHorizontal*(canvas : WMGraphics.Canvas; x, y, dx, dy : размерМЗ; img : WMGraphics.Image);
перем i : размерМЗ;
нач
	i := dx DIV img.width + 1;
	canvas.SetClipRect(WMRectangles.MakeRect(0, 0, x+dx, canvas.clipRect.b));
	нцПока i > 0 делай
		canvas.ScaleImage(img,
			WMRectangles.MakeRect(0, 0, img.width, img.height),
			WMRectangles.MakeRect(x, y, x+img.width, y+dy),
			WMGraphics.ModeSrcOverDst, 10);
		увел(x, img.width);
		умень(i)
	кц;
кон RepeatImageHorizontal;

(** repeats img as often as necessairy to fill an area with height dy starting at (x,y) *)
проц RepeatImageVertical*(canvas : WMGraphics.Canvas; x, y, dx, dy : размерМЗ; img : WMGraphics.Image);
перем i : размерМЗ;
нач
	i := dy DIV img.height + 1;
	canvas.SetClipRect(WMRectangles.MakeRect(0, 0, canvas.clipRect.r, y+dy));
	нцПока i > 0 делай
		canvas.ScaleImage(img,
			WMRectangles.MakeRect(0, 0, img.width, img.height),
			WMRectangles.MakeRect(x, y, x+dx, y+img.height),
			WMGraphics.ModeSrcOverDst, 10);
		увел(y, img.height);
		умень(i)
	кц
кон RepeatImageVertical;

проц Circle*(конст c: WMGraphics.Canvas; CX, CY, R : размерМЗ);
перем
	X, Y : размерМЗ;
	XChange, YChange : размерМЗ;
	RadiusError : размерМЗ;
нач
	X := R;
	Y := 0;
	XChange :=  1- 2*R;
	YChange := 1;
	RadiusError := 0;

	нцПока ( X>= Y ) делай
		c.Fill(WMRectangles.MakeRect(CX+X, CY+Y,CX+X+1,CY+Y+1),c.color,1);
		c.Fill(WMRectangles.MakeRect(CX-X, CY+Y,CX-X+1, CY+Y+1),c.color,1);
		c.Fill(WMRectangles.MakeRect(CX-X, CY-Y,CX-X+1, CY-Y+1),c.color,1);
		c.Fill(WMRectangles.MakeRect(CX+X, CY-Y,CX+X+1, CY-Y+1),c.color,1);
		c.Fill(WMRectangles.MakeRect(CX+Y, CY+X,CX+Y+1,CY+X+1),c.color,1);
		c.Fill(WMRectangles.MakeRect(CX-Y, CY+X,CX-Y+1, CY+X+1),c.color,1);
		c.Fill(WMRectangles.MakeRect(CX-Y, CY-X,CX-Y+1, CY-X+1),c.color,1);
		c.Fill(WMRectangles.MakeRect(CX+Y, CY-X,CX+Y+1, CY-X+1),c.color,1);
		увел(Y);
		увел(RadiusError, YChange);
		увел(YChange,2);
		если ( 2*RadiusError + XChange > 0 ) то
			умень(X);
			увел(RadiusError, XChange);
			увел(XChange,2);
		всё;
	кц;
кон Circle;

(* Bresenham Type Algorithm For Drawing Ellipses *)
проц Ellipse*(конст c: WMGraphics.Canvas; CX, CY, XRadius, YRadius : размерМЗ);
перем
	X, Y : размерМЗ;
	XChange, YChange : размерМЗ;
	EllipseError : размерМЗ;
	TwoASquare, TwoBSquare : размерМЗ;
	StoppingX, StoppingY : размерМЗ;
нач
	TwoASquare := 2*XRadius*XRadius;
	TwoBSquare := 2*YRadius*YRadius;
	X := XRadius;
	Y := 0;
	XChange :=  YRadius*YRadius*(1-2*XRadius);
	YChange :=  XRadius*XRadius;
	EllipseError := 0;
	StoppingX := TwoBSquare*XRadius;
	StoppingY := 0;
	нцПока ( StoppingX>= StoppingY ) делай (* 1st set of points, y>1 *)
		c.Fill(WMRectangles.MakeRect(CX+X, CY+Y-1,CX+X+1,CY+Y+1),c.color,1); (*point in quadrant 1*)
		c.Fill(WMRectangles.MakeRect(CX-X, CY+Y-1,CX-X+1, CY+Y+1),c.color,1); (*point in quadrant 2*)
		c.Fill(WMRectangles.MakeRect(CX-X, CY-Y-1,CX-X+1, CY-Y+1),c.color,1); (*point in quadrant 3*)
		c.Fill(WMRectangles.MakeRect(CX+X, CY-Y-1,CX+X+1, CY-Y+1),c.color,1); (*point in quadrant 4*)
		увел(Y);
		увел(StoppingY, TwoASquare);
		увел(EllipseError, YChange);
		увел(YChange,TwoASquare);
		если ((2*EllipseError + XChange) > 0 ) то
			умень(X);
			умень(StoppingX, TwoBSquare);
			увел(EllipseError, XChange);
			увел(XChange,TwoBSquare)
		всё;
	кц;
	(* 1st point set is done; start the 2nd set of points *)
	X := 0;
	Y := YRadius;
	XChange := YRadius*YRadius;
	YChange := XRadius*XRadius*(1-2*YRadius);
	EllipseError := 0;
	StoppingX := 0;
	StoppingY := TwoASquare*YRadius;
	нцПока ( StoppingX<= StoppingY ) делай  (*2nd set of points, y < 1*)
		c.Fill(WMRectangles.MakeRect(CX+X, CY+Y,CX+X+1,CY+Y+1),c.color,1); (*point in quadrant 1*)
		c.Fill(WMRectangles.MakeRect(CX-X, CY+Y,CX-X+1, CY+Y+1),c.color,1); (*point in quadrant 2*)
		c.Fill(WMRectangles.MakeRect(CX-X, CY-Y,CX-X+1, CY-Y+1),c.color,1); (*point in quadrant 3*)
		c.Fill(WMRectangles.MakeRect(CX+X, CY-Y,CX+X+1, CY-Y+1),c.color,1); (*point in quadrant 4*)
		увел(X);
		увел(StoppingX, TwoBSquare);
		увел(EllipseError, XChange);
		увел(XChange,TwoBSquare);
		если ((2*EllipseError + YChange) > 0 ) то
			умень(Y);
			умень(StoppingY, TwoASquare);
			увел(EllipseError, YChange);
			увел(YChange,TwoASquare)
		всё;
	кц;
кон Ellipse;

проц DrawEllipse*(конст canvas: WMGraphics.Canvas; CX, CY, XRadius, YRadius : размерМЗ; lineColor, fillColor : WMGraphics.Color; mode : целМЗ);
нач
	если (XRadius > 0) и (YRadius > 0) то
		EllipseBresenham(canvas, WMRectangles.MakeRect(CX, CY, CX, CY), XRadius, YRadius, FillSolidEllipsePixels, lineColor, fillColor, mode);
	всё;
кон DrawEllipse;

проц DrawCircle*(конст canvas: WMGraphics.Canvas; CX, CY, radius : размерМЗ; lineColor, fillColor : WMGraphics.Color; mode : целМЗ);
нач
	если radius > 0 то
		EllipseBresenham(canvas, WMRectangles.MakeRect(CX, CY, CX, CY), radius, radius, FillSolidEllipsePixels, lineColor, fillColor, mode);
	всё;
кон DrawCircle;

проц DrawRoundRect*(конст canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; rx, ry : размерМЗ; lineColor, fillColor : WMGraphics.Color; mode : целМЗ);
	перем innerRect : WMRectangles.Rectangle;
нач
	если (lineColor = fillColor) и (lineColor = WMGraphics.Transparent) то возврат всё;
    если (rect.r <= rect.l) или (rect.b <= rect.t) или (rx <= 0) или (ry <= 0) то возврат всё;

	(* Set coordinates to reflect the centers of 4 quarter circles *)
	innerRect := rect;
	увел(innerRect.l, rx); увел(innerRect.t, ry);	умень(innerRect.r, rx); умень(innerRect.b, ry);

	если (innerRect.r < innerRect.l) или (innerRect.b < innerRect.t) то возврат всё;

	(*IF lineColor = WMGraphics.Transparent THEN lineColor := fillColor; END;*)

	если (lineColor # fillColor) и (lineColor # WMGraphics.Transparent) то (* draw rect *)
		умень(rect.r); (* skip for filling part *)
		canvas.Fill(WMRectangles.MakeRect(innerRect.l, rect.t, innerRect.r, rect.t + 1), lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.l, innerRect.t + 1, rect.l+1, innerRect.b - 1), lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(innerRect.l, rect.b - 1,  innerRect.r, rect.b),      lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.r, innerRect.t + 1, rect.r+1, innerRect.b - 1), lineColor, mode);
		увел(rect.l); (* skip for filling part *)
	всё;

	если fillColor = WMGraphics.Transparent то (* draw round corners *)
		EllipseBresenham(canvas, innerRect, rx, ry, DrawEllipsePixels, lineColor, fillColor, mode);
	иначе (* filling part -- fill round corners, fill center rect *)
		canvas.Fill(WMRectangles.MakeRect(rect.l, innerRect.t + 1, rect.r, innerRect.b-1), fillColor, mode);
		EllipseBresenham(canvas, innerRect, rx, ry, FillSolidEllipsePixels, lineColor, fillColor, mode);
	всё;
кон DrawRoundRect;

проц DrawEllipsePixels(конст canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; dx, dy : размерМЗ; lineColor, unused : WMGraphics.Color; mode : целМЗ);
нач
	если lineColor # WMGraphics.Transparent то
		canvas.Fill(WMRectangles.MakeRect(rect.l-dx,   rect.t-dy,   rect.l-dx+1, rect.t-dy+1), lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.r+dx-1, rect.t-dy,   rect.r+dx,   rect.t-dy+1), lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.l-dx,   rect.b+dy-1, rect.l-dx+1, rect.b+dy),   lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.r+dx-1, rect.b+dy-1, rect.r+dx,   rect.b+dy),   lineColor, mode);
	всё;
кон DrawEllipsePixels;

проц FillSolidEllipsePixels(конст canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; dx, dy : размерМЗ; lineColor, fillColor : WMGraphics.Color; mode : целМЗ);
нач
	если (lineColor # fillColor) и (lineColor # WMGraphics.Transparent) то
		умень( rect.r ); умень( rect.b ); (* skip for filling part *)
		canvas.Fill(WMRectangles.MakeRect(rect.l-dx, rect.t-dy, rect.l-dx+1, rect.t-dy+1), lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.r+dx, rect.t-dy, rect.r+dx+1, rect.t-dy+1), lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.l-dx, rect.b+dy, rect.l-dx+1, rect.b+dy+1), lineColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.r+dx, rect.b+dy, rect.r+dx+1, rect.b+dy+1), lineColor, mode);
		увел( rect.l ); (* skip for filling part *)
	всё;
	если fillColor # WMGraphics.Transparent то (* filling part *)
		canvas.Fill(WMRectangles.MakeRect(rect.l-dx, rect.t-dy, rect.r+dx, rect.t-dy+1), fillColor, mode);
		canvas.Fill(WMRectangles.MakeRect(rect.l-dx, rect.b+dy, rect.r+dx, rect.b+dy+1), fillColor, mode);
	всё;
кон FillSolidEllipsePixels;

проц EllipseBresenham(конст canvas : WMGraphics.Canvas; innerRect : WMRectangles.Rectangle; rx, ry : размерМЗ; drawPoints : EllipsePixelsFiller; lineColor, fillColor : WMGraphics.Color; mode : целМЗ);
перем
	X, Y, prevDistance : размерМЗ;
	XChange, YChange : размерМЗ;
	RadiusError : размерМЗ;
	TwoASquare, TwoBSquare : размерМЗ;
	StoppingX, StoppingY : размерМЗ;
нач
	RadiusError := 0;
	если lineColor = WMGraphics.Transparent то lineColor := fillColor; всё;

	если rx = ry то (* circle *)
		X := rx;
		prevDistance := rx;
		Y := 0;
		XChange :=  1 - 2 * rx;
		YChange := 1;

		drawPoints(canvas, innerRect, X, Y, lineColor, fillColor, mode); (* start points *)
		нцПока ( Y < X ) делай
			если (X = prevDistance) то (* skip fill *)
				drawPoints(canvas, innerRect, Y, X, lineColor, WMGraphics.Transparent, mode);
			иначе
				drawPoints(canvas, innerRect, Y, X, lineColor, fillColor, mode);
			всё;

			prevDistance := X; (* save value *)

			увел(RadiusError, YChange);
			увел(YChange,2);
			если (2*RadiusError + XChange > 0) то
				умень(X);
				увел(RadiusError, XChange);
				увел(XChange,2);
			всё;

			увел(Y);

			drawPoints(canvas, innerRect, X, Y, lineColor, fillColor, mode);
		кц;
	иначе
		X := rx;
		Y := 0;

		TwoASquare := 2*rx*rx;
		TwoBSquare := 2*ry*ry;
		XChange :=  ry*ry*(1-2*rx);
		YChange :=  rx*rx;
		StoppingX := TwoBSquare*rx;
		StoppingY := 0;

		нцПока ( StoppingX >= StoppingY ) делай (* 1st set of points, y > 1 *)
			drawPoints(canvas, innerRect, X, Y, lineColor, fillColor, mode);

			увел(StoppingY, TwoASquare);
			увел(RadiusError, YChange);
			увел(YChange,TwoASquare);
			если ((2*RadiusError + XChange) > 0 ) то
				умень(X);
				умень(StoppingX, TwoBSquare);
				увел(RadiusError, XChange);
				увел(XChange,TwoBSquare);
			всё;

			увел(Y);
		кц;
		(* 1st point set is done; start the 2nd set of points *)
		X := 0;
		Y := ry;
		prevDistance := ry;

		XChange := ry*ry;
		YChange := rx*rx*(1-2*ry);
		RadiusError := 0;
		StoppingX := 0;
		StoppingY := TwoASquare*ry;
		нцПока ( StoppingX < StoppingY ) делай  (*2nd set of points, y < 1*)
			если (Y = prevDistance) то (* skip fill *)
				drawPoints(canvas, innerRect, X, Y, lineColor, WMGraphics.Transparent, mode);
			иначе
				drawPoints(canvas, innerRect, X, Y, lineColor, fillColor, mode);
			всё;

			prevDistance := Y;

			увел(StoppingX, TwoBSquare);
			увел(RadiusError, XChange);
			увел(XChange,TwoBSquare);
			если ((2*RadiusError + YChange) > 0 ) то
				умень(Y);
				умень(StoppingY, TwoASquare);
				увел(RadiusError, YChange);
				увел(YChange,TwoASquare);
			всё;

			увел(X);
		кц;
	всё;
кон EllipseBresenham;

кон WMGraphicUtilities.
