модуль W3dMenu;	(** AUTHOR "TF"; PURPOSE "3d Menu (case study)"; *)

использует
(* Low level *)
	ЛогЯдра, Kernel, MathL, Modules, Files, Commands, Inputs, Strings,
(* Window Manager *)
	WM := WMWindowManager, Messages := WMMessages, Rect := WMRectangles, Raster, WMGraphics,
(* 3d framework *)
	Classes := TFClasses, Vectors := W3dVectors, Matrix := W3dMatrix,
	AbstractWorld := W3dAbstractWorld, World := W3dWorld, ObjectGenerator := W3dObjectGenerator,
(* XML framework *)
	XML, Scanner := XMLScanner, XMLParser, Objects := XMLObjects;

конст BoxDistance = 200;
	SphereSel = 1;
	BlurpSel = 2;

тип
	ReloadMsg = окласс
	перем
		name:массив 100 из симв8
	кон ReloadMsg;

	Symbol = окласс
	перем
		pos : Vectors.TVector3d;
		command : массив 128 из симв8;
		obj : AbstractWorld.Object;
		world : AbstractWorld.World;
		tex : AbstractWorld.Texture;
		index : цел32;

		проц &Init*(world : AbstractWorld.World; pos : Vectors.TVector3d; command : массив из симв8);
		нач
			копируйСтрокуДо0(command, сам.command); сам.pos := pos; сам.world := world
		кон Init;
	кон Symbol;

	UpdateProc = проц {делегат};

	Blurp = окласс
		перем
			timer : Kernel.Timer;
			alive : булево;
			obj, o2 : AbstractWorld.Object;
			update : UpdateProc;
			i, direct : цел32;
			dead, run, anirun : булево;
			pos : Vectors.TVector3d;
			tex : AbstractWorld.Texture;
			world : AbstractWorld.World;

		 проц &Init*(world: AbstractWorld.World; update: UpdateProc);
		 нач
		 	сам.update := update; сам.world := world;
		 	сам.obj := world.CreateObject(); сам.o2 := world.CreateObject(); direct := 1; tex := НУЛЬ;
		 	world.AddObject(obj);
		 	(* a bit a trick *)
			world.SetAnimated(obj, истина); world.SetAnimated(o2, истина);
		кон Init;

		проц Update;
		перем temp : AbstractWorld.Object;
		нач
			o2.Clear;
			если run то
				ObjectGenerator.TexBox(Matrix.Translation4x4(pos.x, pos.y + i * 2, pos.z),  105 + i*4,  105 + i*4,  105 + i*4, o2, 0FF0000H, tex);
				i := i + direct;
				если i > 8 то нач {единолично} anirun := ложь кон всё
			всё;
			temp := obj; world.ReplaceObject(obj, o2); obj := o2; o2 := temp;
			update
		кон Update;

		проц Set(pos : Vectors.TVector3d; tex : AbstractWorld.Texture);
		нач {единолично}
			run := истина; anirun := истина; i := 0; timer.Wakeup; сам.pos := pos; сам.tex := tex; direct := 1
		кон Set;

		проц Stop;
		нач {единолично}
			если run то run := ложь; Update всё
		кон Stop;

		проц Kill;
		нач {единолично}
			alive := ложь; timer.Wakeup
		кон Kill;

		проц AwaitDead;
		нач {единолично}
			дождись(dead)
		кон AwaitDead;

	нач {активное}
		dead := ложь; alive := истина; нов(timer);
		нцПока alive делай
			timer.Sleep(10);
			нач {единолично} дождись(anirun и run или ~alive) кон;
			если alive то Update всё
		кц;
		нач {единолично} dead := истина кон
	кон Blurp;

	Window = окласс ( WM.BufferWindow )
	перем
		(* Navigation *)
		lookat: Vectors.TVector3d;
		radius, angle, height : вещ64;
		mouseKeys, keyflags : мнвоНаБитахМЗ;
		oldX, oldY : размерМЗ;

		(* 3d World *)
		world : World.World;
		mx, my, mz : вещ64;

		infoList : Classes.List;
		index : цел32;
		aniObj, aniObj2 : AbstractWorld.Object;

		selectionMethod : мнвоНаБитахМЗ;
		blurp : Blurp;
		selectedSymbol : Symbol;

		проц SetSelection(pos : Vectors.TVector3d; l : вещ64; visible : булево);
		перем temp : AbstractWorld.Object;
		нач
			aniObj2.Clear;
			если visible то
				если SphereSel в selectionMethod то
					ObjectGenerator.Sphere(Matrix.Translation4x4(pos.x, pos.y + 80, pos.z), 30, 15, aniObj2, 0FFFF00H)
				всё;
			всё;
			temp := aniObj; world.ReplaceObject(aniObj, aniObj2); aniObj := aniObj2; aniObj2 := temp;
			RenderAnimation
		кон SetSelection;

		проц AddSelectionObjects;
		нач
			нов(blurp, world, RenderAnimation);
			aniObj := world.CreateObject(); world.SetAnimated(aniObj, истина);
			aniObj2 := world.CreateObject(); world.SetAnimated(aniObj2, истина);
			world.AddObject(aniObj)
		кон AddSelectionObjects;

		проц ParseLine(line : XML.Element; pos: Vectors.TVector3d);
		перем cont : Objects.Enumerator; p : динамическиТипизированныйУкль; el : XML.Element; s, t : Strings.String;
			x: Symbol;
		нач
			cont := line.GetContents(); cont.Reset();
			нцПока cont.HasMoreElements() делай
				p := cont.GetNext();
				el := p(XML.Element);
				s := el.GetName();
				если s^ = "ImgBox" то
					s := el.GetAttributeValue("cmd"); если s = НУЛЬ то нов(x, world, pos, "hello") иначе нов(x, world, pos, s^) всё;
					x.index := index; увел(index); infoList.Add(x);

					x.pos := pos; mx := матМаксимум(pos.x, mx);
					pos.x := pos.x + BoxDistance;
					s := el.GetAttributeValue("img");
					если s = НУЛЬ то нов(s, 16) всё;
					x.obj := world.CreateObject(); x.obj.SetIndex(x.index); world.AddObject(x.obj);
					x.tex := TextureByName(s^, x.obj);
					ObjectGenerator.TexBox(Matrix.Translation4x4(x.pos.x, x.pos.y, x.pos.z), 100, 100, 100, x.obj, 0FFAA00H,
						x.tex)

				аесли s^="SymbolBox" то
					s := el.GetAttributeValue("cmd"); если s = НУЛЬ то нов(x, world, pos, "hello") иначе нов(x, world, pos, s^) всё;
					x.index := index; увел(index); winstance.infoList.Add(x);

					x.pos := pos; mx := матМаксимум(pos.x, mx);
					pos.x := pos.x + BoxDistance;
					s := el.GetAttributeValue("img"); если s = НУЛЬ то нов(s, 16) всё;
					t := el.GetAttributeValue("title"); если t = НУЛЬ то нов(t, 16) всё;
					x.obj := world.CreateObject(); x.obj.SetIndex(x.index); world.AddObject(x.obj);
					x.tex := GenTexture(s^, t^, x.obj);
					ObjectGenerator.TexBox(Matrix.Translation4x4(x.pos.x, x.pos.y, x.pos.z), 100, 100, 100, x.obj, 0FFAA00H,
						x.tex)
				всё
			кц
		кон ParseLine;

		проц ParseLayer(layer : XML.Element; pos : Vectors.TVector3d);
		перем cont : Objects.Enumerator; p : динамическиТипизированныйУкль; el : XML.Element;s : Strings.String;

		нач
			cont := layer.GetContents(); cont.Reset();
			нцПока cont.HasMoreElements() делай
				p := cont.GetNext();
				el := p(XML.Element);
				s := el.GetName(); если s^ = "Line" то
					ParseLine(el, pos); mz := матМаксимум(pos.z, mz);
					pos.z := pos.z + BoxDistance
				всё
			кц
		кон ParseLayer;

		проц Load(filename: массив из симв8);
		перем f : Files.File;
			scanner : Scanner.Scanner;
			parser : XMLParser.Parser;
			reader : Files.Reader;
			doc : XML.Document;
			p : динамическиТипизированныйУкль;
			root: XML.Element;
			el : XML.Content;
			s : Strings.String;
			cont : Objects.Enumerator;
			pos : Vectors.TVector3d;
			obj : AbstractWorld.Object;
		нач
			world.Clear; infoList.Clear; mx := 0; my := 0; mz := 0;
			если blurp # НУЛЬ то blurp.Kill; blurp.AwaitDead всё;

			index := 1;
			ЛогЯдра.пСтроку8(filename); ЛогЯдра.пВК_ПС;
			f := Files.Old(filename);
			если f # НУЛЬ то
				нов(reader, f, 0);
				нов(scanner, reader); нов(parser, scanner); doc := parser.Parse();

				root := doc.GetRoot();
				cont := root.GetContents(); cont.Reset();
				нцПока cont.HasMoreElements() делай
					p := cont.GetNext();
					если p суть XML.Element то
						el := p(XML.Element);
						s := el(XML.Element).GetName(); если s^ = "Layer" то
							ParseLayer(el(XML.Element), pos); my := матМаксимум(pos.z, my);
							pos.y := pos.y + BoxDistance
						всё
					всё
				кц
			всё;
			lookat := Vectors.Vector3d(mx / 2, my / 2, mz / 2);

			obj := world.CreateObject(); obj.SetIndex(index); world.AddObject(obj);
			ObjectGenerator.Box(Matrix.Translation4x4(mx/2, my/2 - 50 - 5,  mz/2), mx +100, 10, mz + 100, obj, 0FFFFCCH);
			AddSelectionObjects;
			Render
		кон Load;

		проц &New*(fileName: массив из симв8);
		перем w, h : цел32;
		нач
			если winstance = НУЛЬ то  winstance := сам  всё;  (* fld, adapt to new semantics of NEW *)
			manager := WM.GetDefaultManager();
			h := 480; w := 640;
			Init(w, h, ложь);

			(* Init navigation parameters *)
			radius := 800; angle := -MathL.pi / 2; height := 200;

			(* Setup the 3d World *)
			нов(world, w, h, 000000088H); world.quality := 1;
			нов(infoList); Load(fileName);
			selectionMethod := { BlurpSel };

			WM.DefaultAddWindow(сам);
			SetTitle(Strings.NewString("Menu 3d"));
			Render
		кон New;

		проц {перекрыта}Close*;
		нач
			если blurp # НУЛЬ то blurp.Kill; blurp.AwaitDead всё;
			Close^;
			winstance := НУЛЬ
		кон Close;

		(* BEGIN Navigation and Rendering *)

		проц RenderAnimation;
		нач
			world.Render(img, истина);
			Invalidate(Rect.MakeRect(0,0,img.width, img.height))
		кон RenderAnimation;

		проц Render;
		перем pos, dir, up : Vectors.TVector3d;
		нач {единолично}
			pos := Vectors.VAdd3(lookat, Vectors.Vector3d(MathL.cos(angle) * radius, 0, MathL.sin(angle) * radius)); pos.y := height;
	(*		lookat := Vectors.Vector3d(lookat.x, lookat.y, lookat.z); *)
			dir := Vectors.VNormed3(Vectors.VSub3(lookat, pos));
			up := Vectors.VNeg3(Vectors.VNormed3(Vectors.Cross(Vectors.Cross(Vectors.Vector3d(0, 1, 0), dir), dir)));

			world.SetCamera(pos, dir, up); world.Render(img, ложь);
			Invalidate(Rect.MakeRect(0, 0, img.width, img.height))
		кон Render;

		проц {перекрыта}PointerDown*(x, y : размерМЗ; keys :мнвоНаБитахМЗ);
		нач
			mouseKeys := (keys * {0, 1, 2});
			oldX := x; oldY := y
		кон PointerDown;

		проц {перекрыта}PointerMove*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		перем idx : цел32;
			info : Symbol; dummy : динамическиТипизированныйУкль;
		нач
			если mouseKeys = {} то
				idx := world.GetOwnerIndex(x, y) - 1;
				если (idx >= 0) то
					infoList.Lock;
					info := НУЛЬ;
					если idx < infoList.GetCount() то dummy := infoList.GetItem(idx); info := dummy(Symbol) всё;
					infoList.Unlock;
					если selectedSymbol # info то
						если info # НУЛЬ то
							SetSelection(info.pos, 0, истина);
							если BlurpSel в selectionMethod то blurp.Set(info.pos, info.tex)
							иначе blurp.Stop;
							всё
						иначе blurp.Stop; SetSelection(Vectors.Vector3d(0,0,0), 0, ложь)
						всё
					всё;
					selectedSymbol := info
				иначе
					если selectedSymbol # НУЛЬ то
						selectedSymbol := НУЛЬ; blurp.Stop;
						SetSelection(Vectors.Vector3d(0,0,0), 0, ложь)
					всё
				всё;
				возврат
			всё;
			если mouseKeys * {0} # {} то
				если mouseKeys * {2} # {} то
					radius := radius - (y - oldY) * 10; если radius < 10 то radius := 10 всё;
				иначе
					height := height + (y - oldY)
				всё;
				angle := angle - (x - oldX) / img.width * 3.141;
				Render
			всё;
			oldX := x; oldY := y
		кон PointerMove;

		проц {перекрыта}PointerUp*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
		нач
			если mouseKeys = {0} то
				если selectedSymbol # НУЛЬ то
					если keyflags * Inputs.Shift # {} то
						lookat := selectedSymbol.pos; Render
			(*		ELSE
						Commands.Call(selectedSymbol.command, {}, res, msg);
						IF res # 0 THEN
							KernelLog.Enter; KernelLog.String(msg); KernelLog.Exit
						END *)
					всё
				всё
			всё;
			mouseKeys := (keys * {0, 1, 2});
		кон PointerUp;

(*		PROCEDURE KeyPressed(ch : CHAR; flags : SET; keysym : SIZE);
		BEGIN
			keyflags := flags;
			IF ch = "s" THEN
				IF SphereSel IN selectionMethod THEN selectionMethod := selectionMethod - {SphereSel}
				ELSE selectionMethod := selectionMethod + {SphereSel}
				END
			ELSIF ch = "b" THEN
				IF BlurpSel IN selectionMethod THEN selectionMethod := selectionMethod - {BlurpSel}
				ELSE selectionMethod := selectionMethod + {BlurpSel}
				END
			END
		END KeyPressed;
*)

		проц {перекрыта}Handle*(перем m : Messages.Message);
		нач
			если m.msgType = Messages.MsgExt то
				если m.ext суть ReloadMsg то Load(m.ext(ReloadMsg).name) всё
			иначе
				Handle^(m)
			всё
		кон Handle;
		(* END Navigation and Rendering *)
	кон Window;

	TextureInfo = окласс
		перем
			img : Raster.Image;
			name : массив 128 из симв8
	кон TextureInfo;

перем
	winstance : Window;
	textures: Classes.List;

проц GenTexture(icon, title: массив из симв8; obj : AbstractWorld.Object): AbstractWorld.Texture;
перем res : булево;
	 mode: Raster.Mode;
	 pix : Raster.Pixel;
	 tex : AbstractWorld.Texture;
	 img : Raster.Image;
	timg: Raster.Image;
	tw, th, dx, dy : размерМЗ;
нач
	timg := WMGraphics.LoadImage(icon, истина);
	Raster.InitMode(mode, Raster.srcCopy);
	нов(img); Raster.Create(img, 64, 64, Raster.BGR565);
	Raster.SetRGB(pix, 0C0H, 0C0H, 0C0H); Raster.Fill(img, 1, 1, 62, 62, pix, mode);
	Raster.SetRGB(pix, 0H, 0H, 0H);
	Raster.Fill(img, 0, 11, 63, 12, pix, mode);
	Raster.Fill(img, 7, 12, 56, 62, pix, mode);
	Raster.SetRGB(pix, 0FFH, 0FFH, 0FFH);
	Raster.Fill(img, 9, 14, 54, 60, pix, mode);
	timg := WMGraphics.LoadImage(icon, истина);	tex := НУЛЬ;
	если res то
		tw := матМинимум(timg.width, 46); th := матМинимум(timg.height, 46);
		dx := (46 - tw) DIV 2 + 9;
		dy := (46 - th) DIV 2 + 14;
		Raster.Copy(timg, img, 0, 0, tw, th, dx, dy, mode)
	всё;
	tex := obj.AddTexture(img);
	возврат tex
кон GenTexture;

проц TextureByName(name: массив из симв8; obj : AbstractWorld.Object): AbstractWorld.Texture;
перем i : размерМЗ;
	dummy : динамическиТипизированныйУкль;
	ti : TextureInfo; mode: Raster.Mode;
	timg: Raster.Image;
нач
	textures.Lock;
	нцДля i := 0 до textures.GetCount()-1 делай
		dummy := textures.GetItem(i); ti := dummy(TextureInfo);
		если ti.name = name то
			если ti.img = НУЛЬ то
				textures.Unlock;
				возврат НУЛЬ
			иначе textures.Unlock;
				возврат obj.AddTexture(ti.img)
			всё
		всё
	кц;
	textures.Unlock;
	нов(ti); копируйСтрокуДо0(name, ti.name);
	timg := WMGraphics.LoadImage(name, истина);
	если timg # НУЛЬ то
		нов(ti.img); Raster.Create(ti.img, timg.width, timg.height, Raster.BGR565);
		Raster.InitMode(mode, Raster.srcCopy);
		Raster.Copy(timg, ti.img, 0, 0, timg.width, timg.height, 0, 0, mode)
	всё;
	если ti.img # НУЛЬ то возврат obj.AddTexture(ti.img) иначе возврат НУЛЬ всё
кон TextureByName;

(*	PROCEDURE MatchI(VAR buf: ARRAY OF CHAR; with: ARRAY OF CHAR): BOOLEAN;
	VAR i: SIGNED32;
	BEGIN
		i := 0; WHILE (with[i] # 0X) & (CAP(buf[i]) = CAP(with[i])) DO INC(i) END;
		RETURN with[i] = 0X
	END MatchI;
*)

проц Open*(context : Commands.Context);
перем name : массив 100 из симв8;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) то
		если winstance = НУЛЬ то нов(winstance, name) всё;
	всё;
кон Open;

проц Cleanup;
нач
	если winstance # НУЛЬ то winstance.Close всё
кон Cleanup;


нач
	нов(textures);
	Modules.InstallTermHandler(Cleanup)
кон W3dMenu.

W3dMenu.Open W3dFun.XML ~
W3dMenu.Open W3dMenu.XML ~
W3dMenu.Open W3dNetTool.XML ~
W3dMenu.Open W3dPersonal.XML ~
System.Free W3dMenu ~

Compiler.Compile \s W3dVectors.Mod W3dMatrix.Mod W3dGeometry.Mod W3dAbstractWorld.Mod W3dObjectGenerator.Mod
W3dRasterizer.Mod W3dWorld.Mod W3dExplorer.Mod W3dClusterWatch.Mod W3dMenu.Mod~

oberon.bmp objecttracker.bmp networktracker.bmp launcher.bmp tetris.bmp iconvnc.bmp iconhome.bmp iconreload.bmp
iconbones.bmp iconbunny.bmp iconfrog.bmp iconfire.bmp iconfun.bmp iconmemory.bmp iconnettools.bmp iconkeycode.bmp
iconxml.bmp

PC.Compile \s TFVectors.Mod TFMatrix.Mod TFGeometry.Mod TFAbstractWorld.Mod TFObjectGenerator.Mod
Float.TFRasterizer3d.Mod TFWorld3d.Mod TFExplorer.Mod Menu3d.Mod ~
~

System.Free W3dMenu W3dWorld W3dRasterizer W3dObjectGenerator W3dAbstractWorld W3dGeometry W3dMatrix W3dVectors~

EditTools.OpenAscii W3dFun.XML ~
EditTools.OpenAscii W3dMenu.XML ~
EditTools.OpenAscii W3dNetTools.XML ~
EditTools.OpenAscii W3dPersonal.XML ~
