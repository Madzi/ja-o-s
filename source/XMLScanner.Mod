модуль XMLScanner;	(** AUTHOR "swalthert"; PURPOSE "XML scanner"; *)

использует
	ЛогЯдра, Потоки, Strings, DynamicStrings, ReaderSchitUTF8LF;

конст

	(* String pooling settings *)
	Str_ElementName* = 1;
	Str_AttributeName* = 2;
	Str_CharRef* = 10;
	Str_EntityRef* = 11;
	Str_EntityValue* = 12;
	Str_AttributeValue* = 13;
	Str_Comment* = 20;
	Str_ProcessingInstruction* = 21;
	Str_CDataSection* = 22;
	Str_SystemLiteral* = 23;
	Str_PublicLiteral* = 24;
	Str_CharData* = 25;
	Str_Other* = 30;

	(** Scanner: Tokens *)
	Invalid* = -1;
	TagElemStartOpen* = 0;	(** '<' *)
	TagElemEndOpen* = 1;	(** '</' *)
	TagDeclOpen* = 2;	(** '<!NAME' *)
	TagClose* = 3;	(** '>' *)
	TagEmptyElemClose* = 4;	(** '/>' *)
	TagXMLDeclOpen* = 5;	(** '<?xml' *)
	TagPIOpen* = 6;	(** '<?', PITarget := GetStr() *)
	TagPIClose* = 7;	(** '?>' *)
	TagCondSectOpen* = 8;	(** '<![' *)
	TagCondSectClose* = 9;	(** ']]>' *)
	BracketOpen* = 10;	(** '[' *)
	BracketClose* = 11;	(** ']' *)
	ParenOpen* = 12;	(** '(' *)
	ParenClose* = 13;	(** ')' *)
	Comment* = 14;	(** '<!--' chars '-->', chars := GetStr() *)
	CDataSect* = 15;	(** '<![CDATA[' chars ']]>', chars := GetStr() *)
	CharRef* = 16;	(** '&#' number ';' or '&#x' hexnumber ';', number, hexnumber := GetStr() *)
	EntityRef* = 17;	(** '&' name ';', name := GetStr() *)
	ParamEntityRef* = 18;	(** '%' name ';', name := GetStr() *)
	CharData* = 19;	(** chars := GetStr() *)
	Literal* = 20;	(** '"'chars'"' or "'"chars"'", chars := GetStr() *)
	Name* = 21;	(**	Name ::= (Letter | '_' | ':') {NameChar}
										NameChar ::= Letter | Digit | '.' | '-' | '_' | ':' | CombiningChar | Extender
										chars := GetStr() *)
	Nmtoken* = 22;	(**	Nmtoken ::= NameChar {NameChar}, chars := GetStr() *)
	PoundName* = 23;	(** '#'name, name := GetStr() *)
	Question* = 24;	(** '?' *)
	Asterisk* = 25;	(** '*' *)
	Plus* = 26;	(** '+' *)
	Or* = 27;	(** '|' *)
	Comma* = 28;	(** ',' *)
	Percent* = 29;	(** '%' *)
	Equal* = 30;	(** '=' *)
	Eof* = 31;

	LF = 0AX;
	CR = 0DX;

тип
	String = Strings.String;

	Scanner* = окласс
		перем
			sym-: цел8;	(** current token *)
			line-, col-, oldpos, pos: цел32;
			reportError*: проц {делегат} (pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
			nextCh: симв8;	(* look-ahead *)
			dynstr: DynamicStrings.DynamicString;	(* buffer for CharData, Literal, Name, CharRef, EntityRef and ParamEntityRef *)
			r : Потоки.Чтец;
			stringPool : DynamicStrings.Pool;
			stringPooling : мнвоНаБитахМЗ;

		(** Initialize scanner to read from the given ascii file *)
		проц & Init*(r: Потоки.Чтец);
		перем ro : ReaderSchitUTF8LF.ЧтецЮникодаОбёртка;
		нач
			утв(r # НУЛЬ);
			reportError := DefaultReportError;
			нов(ro, r); r := ro;
			сам.r := r;
			нов(dynstr);
			line := 1; pos := 0; col := 0;
			stringPool := НУЛЬ;
			stringPooling := {};
			NextCh();
		кон Init;

		проц SetStringPooling*(stringPooling : мнвоНаБитахМЗ);
		нач
			сам.stringPooling := stringPooling;
			если (stringPooling = {}) то
				stringPool := НУЛЬ;
			аесли (stringPool = НУЛЬ) то
				нов(stringPool);
			всё;
			утв((stringPool = НУЛЬ) = (stringPooling = {}));
		кон SetStringPooling;

		проц Error(конст msg: массив из симв8);
		нач
			sym := Invalid;
			reportError(GetPos(), line, col, msg)
		кон Error;

		проц NextCh;
		нач
			если (nextCh = CR) или (nextCh = LF) то увел(line); col := 0;
			иначе увел(col)
			всё;
			если r.кодВозвратаПоследнейОперации # Потоки.Успех то
				nextCh := 0X; sym := Eof
			иначе
				nextCh := r.чИДайСимв8(); pos := цел32(r.МестоВПотоке());
			всё
		кон NextCh;

		проц ReadTillChar(ch: симв8);
		нач
			dynstr.Clear;
			нцПока (nextCh # ch) и (sym # Eof) делай
				dynstr.AppendCharacter(nextCh);
				NextCh();
			кц;
			если sym = Eof то sym := Invalid всё
		кон ReadTillChar;

		проц SkipWhiteSpaces;
		нач
			нцПока IsWhiteSpace(nextCh) и (sym # Eof) делай
				NextCh()
			кц
		кон SkipWhiteSpaces;

		проц ScanPoundName;
		нач
			dynstr.Clear;
			dynstr.AppendCharacter(nextCh);
			NextCh();
			нцПока (('a' <= nextCh) и (nextCh <= 'z')) или (('A' <= nextCh) и (nextCh <= 'Z')) или
				(('0' <= nextCh) и (nextCh <= '9')) или (nextCh = '.') или (nextCh = '-') или (nextCh = '_') или (nextCh = ':') делай
				dynstr.AppendCharacter(nextCh);
				NextCh();
			кц;
			если sym # Eof то sym := PoundName иначе sym := Invalid всё
		кон ScanPoundName;

		(* Possible results:
				Name
				Nmtoken
				Invalid	*)
		проц ScanNm;
		нач
			SkipWhiteSpaces();
			если (('0' <= nextCh) и (nextCh <= '9')) или (nextCh = '.') или (nextCh = '-') то
				sym := Nmtoken
			аесли (('a' <= nextCh) и (nextCh <= 'z')) или (('A' <= nextCh) и (nextCh <= 'Z')) или (nextCh = '_') или (nextCh = ':') то
				sym := Name
			иначе
				sym := Invalid; возврат
			всё;
			dynstr.Clear;
			dynstr.AppendCharacter(nextCh);
			NextCh();
			нцПока ((('a' <= nextCh) и (nextCh <= 'z')) или (('A' <= nextCh) и (nextCh <= 'Z')) или
					(('0' <= nextCh) и (nextCh <= '9')) или (nextCh = '.') или (nextCh = '-') или (nextCh = '_')
					или (nextCh = ':')) и (sym # Eof) делай
				dynstr.AppendCharacter(nextCh);
				NextCh();
			кц;
			если sym = Eof то sym := Invalid всё
		кон ScanNm;

		(* Scan Comment after comment open tag '<!--', write characters to dynstr.
				Possible results:
				Invalid
				Comment	*)
		проц ScanComment;
		нач
			dynstr.Clear;
			нц
				нцПока (nextCh # '-') и (sym # Eof) делай
					dynstr.AppendCharacter(nextCh);
					NextCh()
				кц;
				если nextCh = '-' то
					NextCh();
					если nextCh = '-' то
						NextCh();
						если nextCh = '>' то
							NextCh(); sym := Comment; возврат
						иначе
							sym := Invalid; возврат
						всё
					иначе
						dynstr.AppendCharacter('-');
					всё
				иначе
					sym := Invalid; возврат
				всё
			кц
		кон ScanComment;

		(* Possible results:
				CharData
				TagCDataSectClose
				Invalid	*)
		проц ScanCDataSect;
		перем bc: цел32; escape : булево;
		нач
			если sym = Eof то
				sym := Invalid;
				возврат
			всё;
			dynstr.Clear;
			нц
				нцПока (nextCh # ']') и  (sym # Eof) делай
					dynstr.AppendCharacter(nextCh);
					NextCh()
				кц;
				если nextCh = ']' то
					bc := 1; escape := ложь; NextCh();
					нцПока nextCh = ']' делай
						увел(bc); NextCh();
						если nextCh = '>' то
							NextCh(); escape := истина;
						всё
					кц;
					если escape то
						нцПока (bc > 2) делай
							умень(bc);
							dynstr.AppendCharacter(']');
						кц;
						sym := CDataSect; возврат
					иначе
						нцПока (bc > 0) делай
							умень(bc); dynstr.AppendCharacter(']');
						кц;
					всё;
				иначе
					sym := CharData; возврат
				всё
			кц
		кон ScanCDataSect;

		(* possible results:
			Invalid
			ParamEntityRef *)
		проц ScanPEReference;
		нач
			ReadTillChar(';'); NextCh();
			если sym # Invalid то sym := ParamEntityRef всё
		кон ScanPEReference;

		(* possible results:
			Invalid
			CharRef
			EntityRef *)
		проц ScanReference;
		нач
			если nextCh = '#' то
				NextCh();
				ReadTillChar(';'); NextCh();
				если sym # Invalid то sym := CharRef всё;
			иначе
				ReadTillChar(';'); NextCh();
				если sym # Invalid то sym := EntityRef всё
			всё
		кон ScanReference;

		(** possible results:
			Invalid
			TagPIClose
			CharData	*)
		проц ScanPInstruction*;
		нач
			если sym = Eof то
				sym := Invalid;
				возврат
			всё;
			dynstr.Clear;
			нц
				нцПока (nextCh # '?') и (sym # Eof) делай
					dynstr.AppendCharacter(nextCh);
					NextCh();
				кц;
				если nextCh = '?' то
					NextCh();
					если nextCh = '>' то
						sym := TagPIClose; NextCh(); возврат
					иначе
						dynstr.AppendCharacter('?');
					всё
				аесли sym = Eof то
					sym := Invalid; возврат
				иначе
					sym := CharData; возврат
				всё
			кц
		кон ScanPInstruction;

		(** Possible results:
			Invalid
			TagPIOpen
			TagCondSectOpen
			TagDeclOpen
			TagXMLDeclOpen
			TagClose
			TagEmptyElemClose
			TagPIClose
			TagCondSectClose
			Comment
			CharRef
			EntityRef
			ParamEntityRef
			Literal
			Name
			Nmtoken
			PoundName
			Question
			Asterisk
			Plus
			Or
			Comma
			Percent
			Equal
			ParenOpen
			ParenClose
			BracketOpen
			BracketClose	*)
		проц ScanMarkup*;
		перем ch: симв8;
		нач
			SkipWhiteSpaces();
			oldpos := GetPos();
			если sym = Eof то
				sym := Eof; возврат
			всё;
			просей nextCh из
			| '<': NextCh();
					если nextCh = '!' то
						NextCh();
						если nextCh = '-' то
							NextCh();
							если nextCh = '-' то
								NextCh(); ScanComment()
							иначе
								Error("'<!--' expected")
							всё
						аесли nextCh = '[' то
							sym := TagCondSectOpen
						иначе
							ScanNm();
							если sym = Name то
								sym := TagDeclOpen
							иначе
								Error("'<!NAME' expected")
							всё
						всё
					аесли nextCh = '?' то
						NextCh(); ScanNm();
						если sym = Name то
							sym := TagPIOpen
						иначе
							Error("'<?' Name expected")
						всё
					иначе
						Error("'<?' Name or '<!--' expected")
					всё
			| '/': NextCh();
					если nextCh = '>' то
						NextCh(); sym := TagEmptyElemClose
					иначе
						sym := Invalid
					всё
			| '>': NextCh(); sym := TagClose
			| '%': NextCh();
					если nextCh = ' ' то
						sym := Percent
					иначе
						ScanPEReference()
					всё
			| '?': NextCh();
					если nextCh = '>' то
						NextCh();
						sym := TagPIClose
					иначе
						sym := Question
					всё
			| '*': NextCh(); sym := Asterisk
			| '+': NextCh(); sym := Plus
			| '|': NextCh(); sym := Or
			| ',': NextCh(); sym := Comma
			| '(': NextCh(); sym := ParenOpen
			| ')': NextCh(); sym := ParenClose
			| '[': NextCh(); sym := BracketOpen
			| ']': NextCh();
					если nextCh = ']' то
						NextCh();
						если nextCh = '>' то
							NextCh(); sym := TagCondSectClose
						иначе
							Error("']]>' expected")
						всё
					иначе
						sym := BracketClose
					всё
			| '=': NextCh(); sym := Equal
			| '"', "'": ch := nextCh; NextCh(); ReadTillChar(ch); NextCh();
					если sym # Invalid то sym := Literal всё;
			| '#': ScanPoundName()
			иначе ScanNm()
			всё
		кон ScanMarkup;

		(** possible results:
			TagElemEndOpen
			TagPIOpen
			TagDocTypeOpen
			CDataSect
			TagElemStartOpen
			Comment
			CharData
			CharRef
			EntityRef
			Eof *)
		проц ScanContent*;
		перем op : цел32;
		нач
			op := GetPos();
			SkipWhiteSpaces(); oldpos := GetPos();
			если sym = Eof то nextCh := 0X всё;
			просей nextCh из
			| 0X: sym := Eof
			| '<': NextCh();
					просей nextCh из
					| '/': sym := TagElemEndOpen; NextCh()
					| '?': NextCh(); ScanNm();
							если (sym = Name) то
								если dynstr.EqualsTo("xml", истина) то
									sym := TagXMLDeclOpen
								иначе
									sym := TagPIOpen
								всё
							иначе
								Error("'<? Name' expected")
							всё
					| '!': NextCh();
							если nextCh = '-' то
								NextCh();
								если nextCh = '-' то
									NextCh(); ScanComment()
								иначе
									Error("'<!--' expected")
								всё
							аесли nextCh = '[' то
								NextCh(); ScanNm();
								если (sym = Name) и dynstr.EqualsTo("CDATA", ложь) и (nextCh = '[') то
									NextCh(); ScanCDataSect()
								иначе
									Error("'<[CDATA[' expected'")
								всё
							иначе
								ScanNm();
								если  sym = Name то
									sym := TagDeclOpen
								иначе
									Error("'<!xml' or '<!NAME' expected")
								всё
							всё
					иначе
						sym:=TagElemStartOpen
					всё
(*			| '?': NextCh();
					IF nextCh = '>' THEN
						NextCh(); sym := TagPIClose
					ELSE
						Error("'?>' expected")
					END
*)			| '&': NextCh(); ScanReference()
			иначе
				dynstr.Clear;
				нцДо
					dynstr.AppendCharacter(nextCh);
					NextCh();
				кцПри (nextCh='<') или (sym = Eof);
				oldpos := op;
				sym := CharData
			всё
		кон ScanContent;

		проц GetString*(type : цел32): String;
		перем string : String;
		нач
			если (type в stringPooling) то
				string := stringPool.Get(dynstr);
			иначе
				string := dynstr.ToArrOfChar();
			всё;
			возврат string;
		кон GetString;

		проц GetPos*(): цел32;
		нач
			возврат цел32(r.МестоВПотоке()) - 1
		кон GetPos;

		проц GetOldPos*(): цел32;
		нач
			возврат oldpos
		кон GetOldPos;

	кон Scanner;

	проц IsWhiteSpace(ch: симв8): булево;
	нач
		возврат (ch = 020X) или (ch = 9X) или (ch = 0DX) или (ch = 0AX)
	кон IsWhiteSpace;

	проц DefaultReportError(pos, line, col: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
	нач
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСтроку8("pos "); ЛогЯдра.пЦел64(pos, 6);
		ЛогЯдра.пСтроку8(", line "); ЛогЯдра.пЦел64(line, 0); ЛогЯдра.пСтроку8(", col "); ЛогЯдра.пЦел64(col, 0);
		ЛогЯдра.пСтроку8("    "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	кон DefaultReportError;

кон XMLScanner.
