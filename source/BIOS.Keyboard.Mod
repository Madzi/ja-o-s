(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Keyboard; (** AUTHOR "pjm"; PURPOSE "PC keyboard driver"; *)

(* temporary Native-based version *)

использует НИЗКОУР, Machine, ЛогЯдра, Modules, Kernel, Objects, Inputs, Commands, Files;

конст
		(* do not change these values, as they are used in the keyboard tables from Native *)
	ScrollLock = 0; NumLock = 1; CapsLock = 2; LAlt = 3; RAlt = 4;
	LCtrl = 5; RCtrl = 6; LShift = 7; RShift = 8; GreyEsc = 9;
	Resetting = 10; SetTypematic = 11; SendingLEDs = 12;
	LMeta = 13; RMeta = 14;

	DeadKey = 0;

	TraceKeys = ложь;

тип
	Keyboard = окласс
		перем last: Inputs.KeyboardMsg;

		проц HandleInterrupt;
		перем m: мнвоНаБитахМЗ; i, k : целМЗ; release: булево; msg: Inputs.KeyboardMsg; c: симв8; 
		нач {единолично}
			Machine.Portin8(060H, c);	(* get scan code *)
			Machine.Portin8(061H, НИЗКОУР.подмениТипЗначения(симв8, m));
			включиВоМнвоНаБитах(m, 7); Machine.Portout8(061H, НИЗКОУР.подмениТипЗначения(симв8, m));
			исключиИзМнваНаБитах(m, 7); Machine.Portout8(061H, НИЗКОУР.подмениТипЗначения(симв8, m));	(* ack *)
			если TraceKeys то ЛогЯдра.п16ричное(кодСимв8(c), -3) всё;
			msg.flags := {};
			k := MapScanCode(c, msg.keysym, release);
			если release то включиВоМнвоНаБитах(msg.flags, Inputs.Release) всё;
			если k >= 0 то msg.ch := симв8ИзКода(k) иначе msg.ch := 0X всё;
			если TraceKeys и (msg.keysym # Inputs.KsNil) то
				ЛогЯдра.п16ричное(msg.keysym, 9); ЛогЯдра.пВК_ПС
			всё;
			нцДля i := LAlt до RShift делай
				если i в flags то включиВоМнвоНаБитах(msg.flags, mapflag[i]) всё
			кц;
			нцДля i := LMeta до RMeta делай
				если i в flags то включиВоМнвоНаБитах(msg.flags, i-LMeta+Inputs.LeftMeta) всё
			кц;
			если (msg.flags # last.flags) или (msg.ch # 0X) или (msg.keysym # Inputs.KsNil) то
				last := msg; Inputs.keyboard.Handle(msg)
			всё
		кон HandleInterrupt;

		проц &Init*;
		нач
			last.ch := 0X; (*last.key := 0X;*) last.flags := {0..31};
			Objects.InstallHandler(сам.HandleInterrupt, Machine.IRQ0+1)
		кон Init;

		проц Finalize;
		нач
			Objects.RemoveHandler(сам.HandleInterrupt, Machine.IRQ0+1)
		кон Finalize;

	кон Keyboard;

перем
	dkey: целМЗ;
	lastport: целМЗ;
	lastvalue: НИЗКОУР.октет;
	keyval: целМЗ;
	table: адресВПамяти;
	flags: мнвоНаБитахМЗ;
	keytable: укль на массив из симв8;
	keyboard: Keyboard;
	mapflag: массив RShift+1 из цел8;

(* ---- Keyboard Driver ---- *)

(* Translation table format:

	table = { scancode unshifted-code shifted-code flags }  0FFX .
	scancode = <scancode byte from keyboard, bit 7 set for "grey" extended keys>
	unshifted-code = <CHAR produced by this scancode, without shift>
	shifted-code = <CHAR produced by this scancode, with shift>
	flags = <bit-mapped flag byte indicating special behaviour>

	flag bit	function
		0	01	DeadKey: Set dead key flag according to translated key code (1-7)
		1	02	NumLock: if set, the state of NumLock will reverse the action of shift (for num keypad) *** no longer ***
		2	04	CapsLock: if set, the state of CapsLock will reverse the action of shift (for alpha keys)
		3	08	LAlt:  \ the state of these two flags in the table and the current state of the two...
		4	10	RAlt: / ...Alt keys must match exactly, otherwise the search is continued.
		5	20	\
		6	40	>  dead key number (0-7), must match current dead key flag
		7	80	/

	The table is scanned sequentially (speed not critical).  Ctrl-Break, Ctrl-F10 and Ctrl-Alt-Del
	are always defined and are not in the table.   The control keys are also always defined. *)

(* TableUS - US keyboard translation table (dead keys: ^=1, '=2, `=3, ~=4, "=5) *)

проц TableUS(): адресВПамяти;
машКод
#если I386 то
	CALL L1
L1:
	POP EAX
	ADD EAX, DWORD L2 - L1
	JMP DWORD L3
L2:
#аесли AMD64 то
	CALL L1
L1:
	POP RAX
	ADD RAX, DWORD L2 - L1
	JMP DWORD L3
L2:
#иначе
	unimplemented
#кон
		; alphabet
	DB 1EH, 'a', 'A', 4H,	30H, 'b', 'B', 4H,	2EH, 'c', 'C', 4H,	20H, 'd', 'D', 4H
	DB 12H, 'e', 'E', 4H,	21H, 'f', 'F', 4H,	22H, 'g', 'G', 4H,	23H, 'h', 'H', 4H
	DB 17H, 'i', 'I', 4H,	24H, 'j', 'J', 4H,	25H, 'k', 'K', 4H,	26H, 'l', 'L', 4H
	DB 32H, 'm', 'M', 4H,	31H, 'n', 'N', 4H,	18H, 'o', 'O', 4H,	19H, 'p', 'P', 4H
	DB 10H, 'q', 'Q', 4H,	13H, 'r', 'R', 4H,	1FH, 's', 'S', 4H,	14H, 't', 'T', 4H
	DB 16H, 'u', 'U', 4H,	2FH, 'v', 'V', 4H,	11H, 'w', 'W', 4H,	2DH, 'x', 'X', 4H
	DB 15H, 'y', 'Y', 4H,	2CH, 'z', 'Z', 4H
		; Oberon accents (LAlt & RAlt)
;	DB 1EH, 'ä', 'Ä', 0CH,	12H, 'ë', 0FFH, 0CH,	18H, 'ö', 'Ö', 0CH,	16H, 'ü', 'Ü', 0CH
;	DB 17H, 'ï', 0FFH, 0CH,	1FH, 'ß', 0FFH, 0CH,	2EH, 'ç', 0FFH, 0CH,	31H, 'ñ', 0FFH, 0CH
;	DB 1EH, 'ä', 'Ä', 14H,	12H, 'ë', 0FFH, 14H,	18H, 'ö', 'Ö', 14H,	16H, 'ü', 'Ü', 14H
;	DB 17H, 'ï', 0FFH, 14H,	1FH, 'ß', 0FFH, 14H,	2EH, 'ç', 0FFH, 14H,	31H, 'ñ', 0FFH, 14H
;		; dead keys (LAlt & RAlt)
;	DB 07H, 0FFH, 1H, 9H,	28H, 2H, 5H, 9H,	29H, 3H, 4H, 9H,
;	DB 07H, 0FFH, 1H, 11H,	28H, 2H, 5H, 11H,	29H, 3H, 4H, 11H,
;		; following keys
;	DB 1EH, 'â', 0FFH, 20H,	12H, 'ê', 0FFH, 20H,	17H, 'î', 0FFH, 20H,	18H, 'ô', 0FFH, 20H
;	DB 16H, 'û', 0FFH, 20H,	1EH, 'à', 0FFH, 60H,	12H, 'è', 0FFH, 60H,	17H, 'ì', 0FFH, 60H
;	DB 18H, 'ò', 0FFH, 60H,	16H, 'ù', 0FFH, 60H,	1EH, 'á', 0FFH, 40H,	12H, 'é', 0FFH, 40H
;	DB 1EH, 'ä', 'Ä', 0A4H,	12H, 'ë', 0FFH, 0A0H,	17H, 'ï', 0FFH, 0A0H,	18H, 'ö', 'Ö', 0A4H
;	DB 16H, 'ü', 'Ü', 0A4H,	31H, 'ñ', 0FFH, 80H

	DB 1EH, 83H, 80H, 0CH,	12H, 91H, 0FFH, 0CH,	18H, 84H, 81H, 0CH,	16H, 85H, 82H, 0CH
	DB 17H, 92H, 0FFH, 0CH,	1FH, 96H, 0FFH, 0CH,	2EH, 93H, 0FFH, 0CH,	31H, 95H, 0FFH, 0CH
	DB 1EH, 83H, 80H, 14H,	12H, 91H, 0FFH, 14H,	18H, 84H, 81H, 14H,	16H, 85H, 82H, 14H
	DB 17H, 92H, 0FFH, 14H,	1FH, 96H, 0FFH, 14H,	2EH, 93H, 0FFH, 14H,	31H, 95H, 0FFH, 14H
		; dead keys (LAlt & RAlt)
	DB 07H, 0FFH, 1H, 9H,	28H, 2H, 5H, 9H,	29H, 3H, 4H, 9H,
	DB 07H, 0FFH, 1H, 11H,	28H, 2H, 5H, 11H,	29H, 3H, 4H, 11H,
		; following keys
	DB 1EH, 86H, 0FFH, 20H,	12H, 87H, 0FFH, 20H,	17H, 88H, 0FFH, 20H,	18H, 89H, 0FFH, 20H
	DB 16H, 8AH, 0FFH, 20H,	1EH, 8BH, 0FFH, 60H,	12H, 8CH, 0FFH, 60H,	17H, 8DH, 0FFH, 60H
	DB 18H, 8EH, 0FFH, 60H,	16H, 8FH, 0FFH, 60H,	1EH, 94H, 0FFH, 40H,	12H, 90H, 0FFH, 40H
	DB 1EH, 83H, 80H, 0A4H,	12H, 91H, 0FFH, 0A0H,	17H, 92H, 0FFH, 0A0H,	18H, 84H, 81H, 0A4H
	DB 16H, 85H, 82H, 0A4H,	31H, 95H, 0FFH, 80H

	DB 1EH, 'a', 'A', 0CH,	12H, 'e', 0FFH, 0CH,	18H, 'o', 'O', 0CH,	16H, 'u', 'U', 0CH
	DB 17H, 'i', 0FFH, 0CH,	1FH, 's', 0FFH, 0CH,	2EH, 'c', 0FFH, 0CH,	31H, 'n', 0FFH, 0CH
	DB 1EH, 'a', 'A', 14H,	12H, 'e', 0FFH, 14H,	18H, 'o', 'O', 14H,	16H, 'u', 'U', 14H
	DB 17H, 'i', 0FFH, 14H,	1FH, 's', 0FFH, 14H,	2EH, 'c', 0FFH, 14H,	31H, 'n', 0FFH, 14H
		; dead keys (LAlt & RAlt)
	DB 07H, 0FFH, 1H, 9H,	28H, 2H, 5H, 9H,	29H, 3H, 4H, 9H,
	DB 07H, 0FFH, 1H, 11H,	28H, 2H, 5H, 11H,	29H, 3H, 4H, 11H,
		; following keys
	DB 1EH, 'a', 0FFH, 20H,	12H, 'e', 0FFH, 20H,	17H, 'i', 0FFH, 20H,	18H, 'o', 0FFH, 20H
	DB 16H, 'u', 0FFH, 20H,	1EH, 'a', 0FFH, 60H,	12H, 'e', 0FFH, 60H,	17H, 'i', 0FFH, 60H
	DB 18H, 'o', 0FFH, 60H,	16H, 'u', 0FFH, 60H,	1EH, 'a', 0FFH, 40H,	12H, 'e', 0FFH, 40H
	DB 1EH, 'a', 'A', 0A4H,	12H, 'e', 0FFH, 0A0H,	17H, 'i', 0FFH, 0A0H,	18H, 'o', 'O', 0A4H
	DB 16H, 'u', 'U', 0A4H,	31H, 'n', 0FFH, 80H
		; numbers at top
	DB 0BH, '0', ')', 0H,	02H, '1', '!', 0H,	03H, '2', '@', 0H,	04H, '3', '#', 0H
	DB 05H, '4', '$', 0H,	06H, '5', '%', 0H,	07H, '6', '^', 0H,	08H, '7', '&', 0H
	DB 09H, '8', '*', 0H,	0AH, '9', '(', 0H
		; symbol keys
	DB 28H, 27H, 22H, 0H,	33H, ',', '<', 0H,	0CH, '-', '_', 0H,	34H, '.', '>', 0H
	DB 35H, '/', '?', 0H,	27H, ';', ':', 0H,	0DH, '=', '+', 0H,	1AH, '[', '{', 0H
	DB 2BH, '\', '|', 0H,	1BH, ']', '}', 0H,	29H, '`', '~', 0H
		; control keys
	DB 0EH, 7FH, 7FH, 0H	; backspace
	DB 0FH, 09H, 09H, 0H	; tab
	DB 1CH, 0DH, 0DH, 0H	; enter
	DB 39H, 20H, 20H, 0H	; space
	DB 01H, 1BH, 1BH, 0H	; esc
		; keypad
	DB 4FH, 0A9H, '1', 2H	; end/1
	DB 50H, 0C2H, '2', 2H	; down/2
	DB 51H, 0A3H, '3', 2H	; pgdn/3
	DB 4BH, 0C4H, '4', 2H	; left/4
	DB 4CH, 0FFH, '5', 2H	; center/5
	DB 4DH, 0C3H, '6', 2H	; right/6
	DB 47H, 0A8H, '7', 2H	; home/7
	DB 48H, 0C1H, '8', 2H	; up/8
	DB 49H, 0A2H, '9', 2H	; pgup/9
	DB 52H, 0A0H, '0', 2H	; insert/0
	DB 53H, 0A1H, 2EH, 2H	; del/.
		; grey keys
	DB 4AH, '-', '-', 0H	; grey -
	DB 4EH, '+', '+', 0H	; grey +
	DB 0B5H, '/', '/', 0H	; grey /
	DB 37H, '*', '*', 0H	; grey *
	DB 0D0H, 0C2H, 0C2H, 0H	; grey down
	DB 0CBH, 0C4H, 0C4H, 0H	; grey left
	DB 0CDH, 0C3H, 0C3H, 0H	; grey right
	DB 0C8H, 0C1H, 0C1H, 0H	; grey up
	DB 09CH, 0DH, 0DH, 0H	; grey enter
	DB 0D2H, 0A0H, 0A0H, 0H	; grey ins
	DB 0D3H, 0A1H, 0A1H, 0H	; grey del
	DB 0C9H, 0A2H, 0A2H, 0H	; grey pgup
	DB 0D1H, 0A3H, 0A3H, 0H	; grey pgdn
	DB 0C7H, 0A8H, 0A8H, 0H	; grey home
	DB 0CFH, 0A9H, 0A9H, 0H	; grey end
		; function keys
	DB 3BH, 0A4H, 0FFH, 0H	; F1
	DB 3CH, 0A5H, 0FFH, 0H	; F2
	DB 3DH, 0A6H, 0FFH, 0H	; F3
	DB 3EH, 0A7H, 0FFH, 0H	; F4
	DB 3FH, 0F5H, 0FFH, 0H	; F5
	DB 40H, 0F6H, 0FFH, 0H	; F6
	DB 41H, 0F7H, 0FFH, 0H	; F7
	DB 42H, 0F8H, 0FFH, 0H	; F8
	DB 43H, 0F9H, 0FFH, 0H	; F9
	DB 44H, 0FAH, 0FFH, 0H	; F10
	DB 57H, 0FBH, 0FFH, 0H	; F11
	DB 58H, 0FCH, 0FFH, 0H	; F12
	DB 0FFH
L3:
кон TableUS;

проц TableFromFile(name: массив из симв8): адресВПамяти;
перем f: Files.File; r: Files.Rider; len: размерМЗ;
нач
	ЛогЯдра.пСтроку8("Keyboard: "); ЛогЯдра.пСтроку8(name);
	f := Files.Old(name);
	если f # НУЛЬ то
		len := f.Length()(размерМЗ);
		если len остОтДеленияНа 4 = 0 то
			нов(keytable, len+1);
			f.Set(r, 0); f.ReadBytes(r, keytable^, 0, len);
			если r.res = 0 то
				ЛогЯдра.пСтроку8(" loaded."); ЛогЯдра.пВК_ПС;
				keytable[len] := 0FFX;
				возврат адресОт(keytable[0])
			иначе
				ЛогЯдра.пСтроку8(" res="); ЛогЯдра.пЦел64(r.res, 1)
			всё
		иначе
			ЛогЯдра.пСтроку8(" len="); ЛогЯдра.пЦел64(len, 1)
		всё
	иначе
		ЛогЯдра.пСтроку8(" not found.")
	всё;
	ЛогЯдра.пВК_ПС;
	возврат TableUS()
кон TableFromFile;

(* Translate - Translate scan code "c" to key. *)

проц Translate(flags: мнвоНаБитахМЗ; c: симв8): целМЗ;
конст
	Alt = {LAlt, RAlt}; Ctrl = {LCtrl, RCtrl}; Shift = {LShift, RShift};
перем a: адресВПамяти; s1: симв8; s: мнвоНаБитахМЗ; k, dkn: целМЗ;
нач {единолично}
	если (c = 46X) и (flags * Ctrl # {}) то возврат -2 всё;	(* Ctrl-Break - break *)
	если (c = 44X) и (flags * Ctrl # {}) то возврат 0FFH всё;	(* Ctrl-F10 - exit *)
	если (c = 53X) и (flags * Ctrl # {}) и (flags * Alt # {}) то возврат 0A1H всё;	(* Ctrl-Alt-Del - Del *)
	если GreyEsc в flags то c := симв8ИзКода(кодСимв8(c)+80H) всё;
	a := table;
	нц
		НИЗКОУР.прочтиОбъектПоАдресу(a, s1);
		если s1 = 0FFX то	(* end of table, unmapped key *)
			k := -1; dkey := 0; прервиЦикл
		аесли s1 = c то	(* found scan code in table *)
			НИЗКОУР.прочтиОбъектПоАдресу(a+3, НИЗКОУР.подмениТипЗначения(симв8, s));	(* flags from table *)
			dkn := НИЗКОУР.подмениТипЗначения(целМЗ, s) DIV 32 остОтДеленияНа 8;
			s := s * {DeadKey, NumLock, CapsLock, LAlt, RAlt, LCtrl, RCtrl}; k := 0;
			если ((s * Alt = flags * Alt) или (NumLock в s) или (s1 > 03BX)) и (dkn = dkey) то	(* Alt & dead keys match exactly *)
				если flags * Shift # {} то включиВоМнвоНаБитах(s, LShift) всё;	(* check if shift pressed *)
					(* handle CapsLock *)
				если (CapsLock в s) и (CapsLock в flags) то s := s / {LShift} всё;
					(* handle NumLock *)
				если NumLock в s то
					если NumLock в flags то s := s + {LShift} иначе s := s - {LShift} всё
				всё;
					(* get key code *)
				если LShift в s то НИЗКОУР.прочтиОбъектПоАдресу(a+2, НИЗКОУР.подмениТипЗначения(симв8, k))	(* shifted value *)
				иначе НИЗКОУР.прочтиОбъектПоАдресу(a+1, НИЗКОУР.подмениТипЗначения(симв8, k))	(* unshifted value *)
				всё;
				если (DeadKey в s) и (k <= 7) то	(* dead key *)
					dkey := k; k := -1	(* set new dead key state *)
				аесли k = 0FFH то	(* unmapped key *)
					k := -1; dkey := 0	(* reset dead key state *)
				иначе	(* mapped key *)
					если flags * Ctrl # {} то
						если ((k >= 64) и (k <= 95)) или ((k >= 97) и (k <= 122)) то
							k := k остОтДеленияНа 32	(* control *)
						аесли k = 13 то	(* Ctrl-Enter *)
							k := 10
						всё
					всё;
					если flags * Alt # {} то	(* Alt-keypad *)
						если (k >= кодСимв8('0')) и (k <= кодСимв8('9')) и (NumLock в s) то	(* keypad num *)
							если keyval = -1 то keyval := k-кодСимв8('0')
							иначе keyval := (10*keyval + (k-кодСимв8('0'))) остОтДеленияНа 1000
							всё;
							k := -1
						всё
					всё;
					dkey := 0	(* reset dead key state *)
				всё;
				прервиЦикл
			всё
		всё;
		увел(a, 4)
	кц; (* LOOP *)
	возврат k
кон Translate;

(* Wait - Wait for keyboard serial port to acknowledge byte. *)

проц Wait;
перем t: Kernel.MilliTimer; s: мнвоНаБитахМЗ;
нач
	Kernel.SetTimer(t, 20);	(* wait up to 17 ms *)
	нцДо
		Machine.Portin8(64H, НИЗКОУР.подмениТипЗначения(симв8, s))
	кцПри ~(1 в s) или Kernel.Expired(t)
кон Wait;

(* SendByte - Send a byte to the keyboard. *)

проц SendByte(port: целМЗ; value: НИЗКОУР.октет);
нач
	Wait; Machine.Portout8(port, НИЗКОУР.подмениТипЗначения(симв8, value));
	lastport := port; lastvalue := value
кон SendByte;

(* ShiftKey - Handle shift keys. *)

проц ShiftKey(left, right: целМЗ; in: булево);
нач
	если in то
		если GreyEsc в flags то включиВоМнвоНаБитах(flags, right)
		иначе включиВоМнвоНаБитах(flags, left)
		всё
	иначе
		если GreyEsc в flags то исключиИзМнваНаБитах(flags, right)
		иначе исключиИзМнваНаБитах(flags, left)
		всё
	всё
кон ShiftKey;

(* LedKey - Handle "lock" keys. *)

проц LedKey(перем flags: мнвоНаБитахМЗ; lock: цел8; c: симв8;
		перем k: целМЗ);
нач
	если flags * {LAlt, RAlt, LCtrl, RCtrl, LShift, RShift} = {} то
		flags := flags / мнвоНаБитахМЗ({lock})
	иначе
		k := Translate(flags, c)
	всё
кон LedKey;

(* MapScanCode - Map a scan code "c" to a key code. *)

проц MapScanCode(c: симв8; перем keysym: целМЗ; перем release: булево): целМЗ;
перем c1 : симв8; k: целМЗ; oldleds: мнвоНаБитахМЗ;
нач
	SendByte(64H, 0ADX); Wait;	(* disable keyboard *)
	k := -1; oldleds := flags * {ScrollLock, NumLock, CapsLock};
	keysym := Inputs.KsNil;	(* no key *)
	если c = 0X то	(* overrun, ignore *)
	аесли c = 0FAX то	(* keyboard ack *)
		если Resetting в flags то
			исключиИзМнваНаБитах(flags, Resetting); включиВоМнвоНаБитах(flags, SendingLEDs);
			SendByte(60H, 0EDX)	(* set keyboard LEDs *)
		аесли SendingLEDs в flags то
			SendByte(60H, НИЗКОУР.подмениТипЗначения(симв8, oldleds));
			исключиИзМнваНаБитах(flags, SendingLEDs)
		аесли SetTypematic в flags то
			исключиИзМнваНаБитах(flags, SetTypematic); включиВоМнвоНаБитах(flags, Resetting);
			SendByte(60H, 020X)	(* 30Hz, 500 ms *)
		иначе (* assume ack was for something else *)
		всё
	аесли c = 0FEX то	(* keyboard resend *)
		SendByte(lastport, lastvalue)
	аесли c = 038X то	(* Alt make *)
		ShiftKey(LAlt, RAlt, истина); keysym := Inputs.KsAltL
	аесли c = 01DX то	(* Ctrl make *)
		ShiftKey(LCtrl, RCtrl, истина); keysym := Inputs.KsControlL
	аесли c = 02AX то	(* LShift make *)
		если ~(GreyEsc в flags) то
			включиВоМнвоНаБитах(flags, LShift); keysym := Inputs.KsShiftL
		всё
	аесли c = 036X то	(* RShift make *)
		если ~(GreyEsc в flags) то
			включиВоМнвоНаБитах(flags, RShift); keysym := Inputs.KsShiftR
		всё
	аесли c = 05BX то	(* LMeta make *)
		включиВоМнвоНаБитах(flags, LMeta); keysym := Inputs.KsMetaL
	аесли c = 05CX то	(* RMeta make *)
		включиВоМнвоНаБитах(flags, RMeta); keysym := Inputs.KsMetaR
	аесли c = 03AX то	(* Caps make *)
		LedKey(flags, CapsLock, c, k);
		keysym := Inputs.KsCapsLock
	аесли c = 046X то	(* Scroll make *)
		LedKey(flags, ScrollLock, c, k);
		если k = -2 то keysym := Inputs.KsBreak (* Break *) иначе keysym := Inputs.KsScrollLock всё
	аесли c = 045X то	(* Num make *)
		LedKey(flags, NumLock, c, k);
		keysym := Inputs.KsNumLock;
	аесли c = 0B8X то	(* Alt break *)
		ShiftKey(LAlt, RAlt, ложь); keysym := Inputs.KsAltL;
		если (keyval >= 0) и (keyval < 255) то k := keyval всё;	(* exclude 255 - reboot *)
		keyval := -1
	аесли c = 09DX то	(* Ctrl break *)
		ShiftKey(LCtrl, RCtrl, ложь); keysym := Inputs.KsControlL
	аесли c = 0AAX то	(* LShift break *)
		если ~(GreyEsc в flags) то
			исключиИзМнваНаБитах(flags, LShift); keysym := Inputs.KsShiftL
		всё
	аесли c = 0B6X то	(* RShift break *)
		если ~(GreyEsc в flags) то
			исключиИзМнваНаБитах(flags, RShift); keysym := Inputs.KsShiftR
		всё
	аесли c = 0DBX то	(* LMeta break *)
		исключиИзМнваНаБитах(flags, LMeta); keysym := Inputs.KsMetaL
	аесли c = 0DCX то	(* RMeta break *)
		исключиИзМнваНаБитах(flags, RMeta); keysym := Inputs.KsMetaR
	аесли c = 05DX то	(* Menu make *)
		keysym := Inputs.KsMenu	(* Windows menu *)
	аесли c = 0B5X то 
		keysym := Inputs.KsKPDivide	(* kp / *)
	иначе (* Other make *)
		если c < 080X то	
			c1 := c; 
			release := ложь;
		иначе (* отжатие *)
			c1 := симв8ИзКода(кодСимв8(c) остОтДеленияНа 80H);
			release := истина;
		всё;
		k := Translate(flags, c1);
		если c1 = 0EX то keysym := Inputs.KsBackSpace	(* backspace *)
		аесли c1 = 0FX то keysym := Inputs.KsTab	(* tab *)
		аесли c1 = 1CX то keysym := Inputs.KsReturn	(* enter *)
		аесли c1 = 01X то keysym := Inputs.KsEscape	(* esc *)
		аесли c1 = 3DX то keysym := Inputs.KsF3	(* f3 *)
		аесли c1 = 4AX то keysym := Inputs.KsKPSubtract	(* kp - *)
		аесли c1 = 4EX то keysym := Inputs.KsKPAdd	(* kp + *)
		аесли c1 = 37X то keysym := Inputs.KsKPMultiply	(* kp * *)
		аесли k >= 0 то keysym := KeySym(симв8ИзКода(k))
		иначе (* skip *)
		всё
	всё;
	если c = 0E0X то включиВоМнвоНаБитах(flags, GreyEsc) иначе исключиИзМнваНаБитах(flags, GreyEsc) всё;
	если flags * {ScrollLock, NumLock, CapsLock} # oldleds то
		включиВоМнвоНаБитах(flags, SendingLEDs);
		SendByte(60H, 0EDX)	(* set keyboard LEDs *)
	всё;
	SendByte(64H, 0AEX);	(* enable keyboard *)
		(* now do additional mappings *)
	возврат k
кон MapScanCode;

(* Map Oberon character code to X11 keysym (/usr/include/X11/keysymdef.h). *)

проц KeySym(ch: симв8): целМЗ;
перем x: целМЗ;
нач
	если (ch >= 1X) и (ch <= 7EX) то x := кодСимв8(ch)	(* ascii *)
	аесли ch = 0A0X то x := Inputs.KsInsert	(* insert *)
	аесли ch = 0A1X то x := Inputs.KsDelete	(* delete *)
	аесли ch = 0A8X то x := Inputs.KsHome	(* home *)
	аесли ch = 0A9X то x := Inputs.KsEnd	(* end *)
	аесли ch = 0A2X то x := Inputs.KsPageUp	(* pgup *)
	аесли ch = 0A3X то x := Inputs.KsPageDown	(* pgdn *)
	аесли ch = 0C4X то x := Inputs.KsLeft	(* left *)
	аесли ch = 0C1X то x := Inputs.KsUp	(* up *)
	аесли ch = 0C3X то x := Inputs.KsRight	(* right *)
	аесли ch = 0C2X то x := Inputs.KsDown	(* down *)
	аесли ch = 0A4X то x := Inputs.KsF1	(* f1 *)
	аесли ch = 0A5X то x := Inputs.KsF2	(* f2 *)
	(*ELSIF ch = 0xxX THEN x := Inputs.KsF3*)	(* f3 *)
	аесли ch = 0A7X то x := Inputs.KsF4	(* f4 *)
	аесли ch = 0F5X то x := Inputs.KsF5	(* f5 *)
	аесли ch = 0F6X то x := Inputs.KsF6	(* f6 *)
	аесли ch = 0F7X то x := Inputs.KsF7	(* f7 *)
	аесли ch = 0F8X то x := Inputs.KsF8	(* f8 *)
	аесли ch = 0F9X то x := Inputs.KsF9	(* f9 *)
	аесли ch = 0FAX то x := Inputs.KsF10	(* f10 *)
	аесли ch = 0FBX то x := Inputs.KsF11	(* f11 *)
	аесли ch = 0FCX то x := Inputs.KsF12	(* f12 *)
	иначе x := 0
	всё;
	возврат x
кон KeySym;

(* InitKeyboard - Initialise the keyboard. *)

проц InitKeyboard;
перем s: мнвоНаБитахМЗ; c: симв8; i: цел8; k: массив 32 из симв8;
нач
	keyval := -1; dkey := 0;
	mapflag[LAlt] := Inputs.LeftAlt; mapflag[RAlt] := Inputs.RightAlt;
	mapflag[LCtrl] := Inputs.LeftCtrl; mapflag[RCtrl] := Inputs.RightCtrl;
	mapflag[LShift] := Inputs.LeftShift; mapflag[RShift] := Inputs.RightShift;
		(* Get table *)
	Machine.GetConfig("Keyboard", k);
	i := 0; нцПока (k[i] # 0X) и (k[i] # '.') делай увел(i) кц;
	если k[i] = '.' то table := TableFromFile(k)
	иначе table := TableUS()
	всё;
		(* Get compatibility option *)
	flags := {};
	нов(keyboard);
		(* clear the keyboard's internal buffer *)
	i := 8;
	нц
		Machine.Portin8(64H, НИЗКОУР.подмениТипЗначения(симв8, s));
		если ~(0 в s) или (i = 0) то прервиЦикл всё;
		Machine.Portin8(60H, c);	(* read byte *)
		Machine.Portin8(61H, НИЗКОУР.подмениТипЗначения(симв8, s));
		включиВоМнвоНаБитах(s, 7); Machine.Portout8(61H, НИЗКОУР.подмениТипЗначения(симв8, s));
		исключиИзМнваНаБитах(s, 7); Machine.Portout8(61H, НИЗКОУР.подмениТипЗначения(симв8, s));	(* ack *)
		умень(i)
	кц;
	flags := {SetTypematic};
	Machine.GetConfig("NumLock", k);
	если k[0] = '1' то включиВоМнвоНаБитах(flags, NumLock) всё;
	SendByte(60H, 0F3X)	(* settypedel, will cause Ack from keyboard *)
кон InitKeyboard;

проц SetLayout*(context : Commands.Context); (** KeyboardLayoutFile ~ *)
перем layoutFilename : массив 256 из симв8;
нач {единолично}
	если (keyboard # НУЛЬ) и context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(layoutFilename) то
		table := TableFromFile(layoutFilename);
	иначе
		context.error.пСтроку8("Keyboard: No keyboard found."); context.error.пВК_ПС;
	всё;
кон SetLayout;

проц Install*;
кон Install;

проц Cleanup;
нач
	если (keyboard # НУЛЬ) и (Modules.shutdown = Modules.None) то
		keyboard.Finalize; keyboard := НУЛЬ
	всё
кон Cleanup;

нач
	InitKeyboard;
	Modules.InstallTermHandler(Cleanup)
кон Keyboard.

(*
19.08.1999	pjm	Split from Aos.Input
20.09.2006	Added SetLayout (staubesv)
*)

Keyboard.Install ~

Keyboard.SetLayout KeyCH.Bin ~
Keyboard.SetLayout KeyUS.Bin ~

System.Free Keyboard ~
