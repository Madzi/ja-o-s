модуль UDPChatBase; (** AUTHOR "SAGE"; PURPOSE "UDP Chat base" *)

использует
	Kernel, Strings, IO := Потоки, IP, FS := Files;

конст

	serverPort*				= 14000;
	UserFile					= "Sage.UDPChatUsers.dat";	(* user file *)

	clientKeepAliveInterval* = 20000;
	clientKeepAliveAwait* = clientKeepAliveInterval * 3 + clientKeepAliveInterval DIV 2;

	UDPHdrLen = 8;
	MaxUDPDataLen* = 10000H - UDPHdrLen;

	VERSION*				= 0002H; (* Identifies the packet as an ICQ packet *)

	ACK*					= 000AH; (* Acknowledgement *)

	SEND_MESSAGE*		= 010EH; (* Send message through server (to offline user) *)
	LOGIN*					= 03E8H; (* Login on server *)
	CONTACT_LIST*			= 0406H; (* Inform the server of my contact list *)
	SEARCH_UIN*			= 041AH; (* Search for user using his/her UIN *)
	SEARCH_USER*			= 0424H; (* Search for user using his/her name or e-mail *)
	KEEP_ALIVE*			= 042EH; (* Sent to indicate connection is still up *)
	SEND_TEXT_CODE*		= 0438H; (* Send special message to server as text *)
	LOGIN_1*				= 044CH; (* Sent during login *)
	INFO_REQ*				= 0460H; (* Request basic information about a user *)
	EXT_INFO_REQ*			= 046AH; (* Request extended information about a user *)
	CHANGE_PASSWORD*	= 049CH; (* Change the user's password *)
	STATUS_CHANGE*		= 04D8H; (* User has changed online status (Away etc) *)
	LOGIN_2*				= 0528H; (* Sent during login *)

	UPDATE_INFO*			= 050AH; (* Update my basic information *)
	UPDATE_EXT_INFO*		= 04B0H; (* Update my extended information *)
	ADD_TO_LIST*			= 053CH; (* Add user to my contact list *)
	REQ_ADD_TO_LIST*		= 0456H; (* Request authorization to add to contact list *)
	QUERY_SERVERS*		= 04BAH; (* Query the server about address to other servers *)
	QUERY_ADDONS*		= 04C4H; (* Query the server about globally defined add-ons *)
	NEW_USER_1*			= 04ECH; (* Ask for permission to add a new user *)
	NEW_USER_REG*		= 03FCH; (* Register a new user *)
	NEW_USER_INFO*		= 04A6H; (* Send basic information about a new user *)
	CMD_X1*				= 0442H; (* *Unknown *)
	MSG_TO_NEW_USER*	= 0456H; (* Send a message to a user not on my contact list
				(this one is also used to request permission to add someone with 'authorize'
				status to your contact list)
				*)

	LOGIN_REPLY*			= 005AH; (* Login reply *)
	USER_ONLINE*			= 006EH; (* User on contact list is online/has changed online status *)
	USER_OFFLINE*			= 0078H; (* User on contact list has gone offline *)
	USER_FOUND*			= 008CH; (* User record found matching search criteria *)
	RECEIVE_MESSAGE*		= 00DCH; (* Message sent while offline/through server *)
	END_OF_SEARCH*		= 00A0H; (* No more USER_FOUND will be sent *)
	INFO_REPLY*			= 0118H; (* Return basic information about a user *)
	EXT_INFO_REPLY*		= 0122H; (* Return extended information about a user *)
	STATUS_UPDATE*		= 01A4H; (* User on contact list has changed online status (Away etc) *)

	REPLY_X1*				= 021CH; (* *Unknown (returned during login) *)
	REPLY_X2*				= 00E6H; (* *Unknown (confirm my UIN?) *)
	UPDATE_REPLY*			= 01E0H; (* Confirmation of basic information update *)
	UPDATE_EXT_REPLY*		= 00C8H; (* Confirmation of extended information update *)
	NEW_USER_UIN*		= 0046H; (* Confirmation of creation of new user and newly assigned UIN *)
	NEW_USER_REPLY*		= 00B4H; (* Confirmation of new user basic information *)
	QUERY_REPLY*			= 0082H; (* Response to QUERY_SEVERS or QUERY_ADDONS *)
	SYSTEM_MESSAGE*		= 01C2H; (* System message with URL'ed button *)

	MESSAGE_TYPE_NORMAL* = 0001H; (*the message is a normal message*)
	MESSAGE_TYPE_URL* = 0004H; (*the message is an URL, and actually consists of two parts,
											separated by the code FE.
											The first part is the description of the URL, and the second part is the
											actual URL.*)
	MESSAGE_TYPE_DATA* = 0008H;

тип

	String = Strings.String;

	ACKRec* = укль на запись
		seqNum*: цел16;
	кон;

	Client* = окласс
	перем
		ip*: IP.Adr;
		port*: цел32;
		inSeqNum*, outSeqNum*: цел16;
		uin*: цел32;

		keepAliveTimer*: Kernel.MilliTimer;

		ACKList-: List;

	проц &New*;
	нач
		нов (ACKList);
	кон New;

	проц Finalize*;
	нач
		ACKList.Clear;
	кон Finalize;

	кон Client;

	UserInfo* = укль на запись
		uin*: цел32;
		shortName*, fullName*, eMail*: массив 65 из симв8;
	кон;

	User* = укль на запись (UserInfo)
		password*: цел32;
	кон;

	Users* = окласс
	перем
		list: List;
		lastUIN: цел32;

		проц &New*;
		нач
			(* Reading of passwords *)
			нов (list);
			lastUIN := 1000;
			Load;
		кон New;

		проц Load;
		перем
			u: User;
			f: FS.File;
			r: FS.Reader;
		нач
			f := FS.Old (UserFile);
			если f # НУЛЬ то
				FS.OpenReader (r, f, 0);
				нцПока r.кодВозвратаПоследнейОперации = IO.Успех делай
					нов (u);
					r.чЦел32_мз (u.uin);
					r.чЦел32_мз (u.password);
					r.чСтроку8˛включаяСимв0 (u.shortName);
					r.чСтроку8˛включаяСимв0 (u.fullName);
					r.чСтроку8˛включаяСимв0 (u.eMail);
					если r.кодВозвратаПоследнейОперации = IO.Успех то
						если u.uin > lastUIN то
							lastUIN := u.uin
						всё;
						list.Add (u);
					всё;
				кц;
			всё;
		кон Load;

		проц Store*;
		перем
			f: FS.File; w: FS.Writer;
			i: цел32;
			u: User;
			ptr: динамическиТипизированныйУкль;
		нач
			если list.GetCount () > 0 то
				f := FS.New (UserFile);
				если (f # НУЛЬ) то
					FS.OpenWriter(w, f, 0);
					i := 0;
					нцПока (w.кодВозвратаПоследнейОперации = IO.Успех) и (i < list.GetCount ())  делай
						ptr := list.GetItem (i);
						u := ptr (User);
						w.пЦел32_мз(u.uin);
						w.пЦел32_мз(u.password);
						w.пСтроку8˛включаяСимв0(u.shortName);
						w.пСтроку8˛включаяСимв0(u.fullName);
						w.пСтроку8˛включаяСимв0(u.eMail);
						увел (i);
					кц;
					если w.кодВозвратаПоследнейОперации = IO.Успех то
						w.ПротолкниБуферВПоток;
						FS.Register (f)
					всё
				всё
			всё
		кон Store;

		проц Add* (password, shortName, fullName, eMail: String): User;
		перем
			u: User;
		нач
			нов (u);
			увел (lastUIN);
			u.uin := lastUIN;
			u.password := Code (password^);
			копируйСтрокуДо0 (shortName^, u.shortName);
			копируйСтрокуДо0 (fullName^, u.fullName);
			копируйСтрокуДо0 (eMail^, u.eMail);
			list.Add (u);
			возврат u;
		кон Add;

		проц Find* (uin: цел32): User;
		перем
			i: цел32;
			u: User;
			ptr: динамическиТипизированныйУкль;
		нач
			i := 0;
			нцПока i < list.GetCount () делай
				ptr := list.GetItem (i);
				u := ptr (User);
				если uin = u.uin то
					возврат u;
				всё;
				увел (i);
			кц;
			возврат НУЛЬ;
		кон Find;

		проц PasswordCorrect* (uin: цел32; password: String): булево;
		перем
			u: User;
		нач
			u := Find (uin);
			если u # НУЛЬ то
				если Code (password^) = u.password то
					возврат истина;
				всё;
			всё;
			возврат ложь;
		кон PasswordCorrect;

	кон Users;

	Buffer* = окласс (Strings.Buffer)
		проц AddInt* (n, len: размерМЗ);
		перем
			i: размерМЗ;
			b: размерМЗ; res: целМЗ;
			s: массив 4 из симв8;
		нач
			утв (len <= 4);
			i := 0; b := 1;
			нцПока i < len делай
				s[i] := симв8ИзКода (n DIV b);
				b := b * 100H;
				увел (i);
			кц;
			Add (s, 0, len, истина, res)
		кон AddInt;
	кон Buffer;

	PArray = укль на массив из динамическиТипизированныйУкль;

	(** Lockable Object List. *)
	List* = окласс
	перем
		list: PArray;
		count: цел32;
		readLock: цел32;

		проц &New*;
		нач
			нов (list, 8); readLock := 0
		кон New;

		(** return the number of objects in the list. If count is used for indexing elements (e.g. FOR - Loop)
			in a multi-process situation, the process calling the GetCount method should call Lock before
			GetCount and Unlock after the last use of an index based on GetCount *)
		проц GetCount*() : цел32;
		нач
			возврат count
		кон GetCount;

		проц Grow;
		перем
			old: PArray;
			i: цел32;
		нач
			old := list;
			нов (list, длинаМассива(list) * 2);
			нцДля i := 0 до count - 1 делай list[i] := old[i] кц;
		кон Grow;

		(** Add an object to the list. Add may block if number of calls to Lock is bigger than the number of calls
		to Unlock *)
		проц Add*(x : динамическиТипизированныйУкль);
		нач {единолично}
			дождись (readLock = 0);
			если count = длинаМассива (list) то Grow всё;
			list[count] := x;
			увел (count)
		кон Add;

		(** return the index of an object. In a multi-process situation, the process calling the IndexOf method
			should call Lock before IndexOf and Unlock after the last use of an index based on IndexOf.
			If the object is not found, -1 is returned *)
		проц IndexOf * (x : динамическиТипизированныйУкль) : цел32;
		перем
			i: цел32;
		нач
			i := 0 ; нцПока i < count делай если list[i] = x то возврат i всё; увел(i) кц;
			возврат -1
		кон IndexOf;

		(** Remove an object from the list. Remove may block if number of calls to Lock is bigger than
		the number of calls to Unlock *)
		проц Remove* (x : динамическиТипизированныйУкль);
		перем
			i: цел32;
		нач {единолично}
			дождись (readLock = 0);
			i:=0; нцПока (i < count) и (list[i] # x) делай увел(i) кц;
			если i < count то
				нцПока (i < count - 1) делай list[i] := list[i + 1]; увел(i) кц;
				умень(count);
				list[count] := НУЛЬ
			всё
		кон Remove;

		(** Removes all objects from the list. Clear may block if number of calls to Lock is bigger than
		the number of calls to Unlock *)
		проц Clear*;
		перем
			i: цел32;
		нач {единолично}
			дождись(readLock = 0);
			нцДля i := 0 до count - 1 делай list[i] := НУЛЬ кц;
			count := 0
		кон Clear;

		(** return an object based on an index. In a multi-process situation, GetItem is only safe in a locked
		region Lock / Unlock *)
		проц GetItem* (i: цел32) : динамическиТипизированныйУкль;
		нач
			утв ((i >= 0) и (i < count), 101);
			возврат list[i]
		кон GetItem;

		(** Lock previousents modifications to the list. All calls to Lock must be followed by a call to Unlock.
		Lock can be nested*)
		проц Lock*;
		нач {единолично}
			увел(readLock); утв(readLock > 0)
		кон Lock;

		(** Unlock removes one modification lock. All calls to Unlock must be preceeded by a call to Lock. *)
		проц Unlock*;
		нач {единолично}
			умень(readLock); утв(readLock >= 0)
		кон Unlock;
	кон List;

	(*IntervalTimer* = OBJECT (WMComponents.Component)
	VAR
		running, terminated: BOOLEAN;
		interval: SIGNED32;
		t: Kernel.Timer;
		onTimer- : WMEvents.EventSource;

		PROCEDURE &Init;
		BEGIN
			Init^;
			NEW (t);
			interval := 500;

			(* event *)
			NEW (onTimer, SELF, GSonTimer, GSonTimerInfo, SELF.StringToCompCommand);
			events.Add (onTimer);

			BEGIN {EXCLUSIVE}
				running := TRUE
			END;
		END Init;

		PROCEDURE SetInterval* (i: SIGNED32);
		BEGIN
			BEGIN {EXCLUSIVE}
				interval := i
			END;
		END SetInterval;

		PROCEDURE Finalize*;
		BEGIN
			Finalize^;
			running := FALSE;
			t.Wakeup;
			BEGIN {EXCLUSIVE}
				AWAIT (terminated)
			END;
		END Finalize;

	BEGIN {ACTIVE}
		BEGIN {EXCLUSIVE}
			AWAIT (running)
		END;
		terminated := FALSE;
		WHILE running DO
			onTimer.Call (NIL);
			t.Sleep (interval);
		END;
		BEGIN {EXCLUSIVE}
			terminated := TRUE
		END;
	END IntervalTimer;*)

(*VAR
	GSonTimer, GSonTimerInfo: String;*)

	(*PROCEDURE Init;
	BEGIN
		GSonTimer := Strings.NewString("onTimer");
		GSonTimerInfo := Strings.NewString("Is called when timer ticks");
	END Init;*)

проц Code (s: массив из симв8): цел32;
перем
	i: цел16; a, b, c: цел32;
нач
	a := 0; b := 0; i := 0;
	нцПока s[i] # 0X делай
		c := b; b := a; a := (c остОтДеленияНа 509 + 1) * 127 + кодСимв8(s[i]);
		увел(i)
	кц;
	если b >= 32768 то b := b - 65536 всё;
	возврат b * 65536 + a
кон Code;

проц ServerPacketInit* (command, seqnum: цел16; buf: Buffer);
нач
	buf.Clear;
	buf.AddInt (VERSION, 2);
	buf.AddInt (command, 2);
	buf.AddInt (seqnum, 2);
кон ServerPacketInit;

проц ClientPacketInit* (command, seqnum: цел16; uin: цел32; buf: Buffer);
нач
	ServerPacketInit (command, seqnum, buf);
	buf.AddInt (uin, 4);
кон ClientPacketInit;

проц BufGetSInt* (buf: String; перем receiveBufOffset: цел32): цел16;
перем
	n: цел16;
нач
	n := кодСимв8 (buf^[receiveBufOffset]);
	увел (receiveBufOffset);
	возврат n;
кон BufGetSInt;

проц BufGetInt* (buf: String; перем receiveBufOffset: цел32): цел16;
перем
	b, n, i: цел16;
нач
	i := 0; b := 1; n := 0;
	нцПока i < 2 делай
		увел (n, кодСимв8 (buf^[receiveBufOffset + i]) * b);
		b := b * 100H;
		увел (i);
	кц;
	увел (receiveBufOffset, 2);
	возврат n;
кон BufGetInt;

проц BufGetLInt* (buf: String; перем receiveBufOffset: цел32): цел32;
перем
	i: цел16;
	b, n: цел32;
нач
	i := 0; b := 1; n := 0;
	нцПока i < 4 делай
		увел (n, кодСимв8 (buf^[receiveBufOffset + i]) * b);
		b := b * 100H;
		увел (i);
	кц;
	увел (receiveBufOffset, 4);
	возврат n;
кон BufGetLInt;

проц BufGetString* (buf: String; перем receiveBufOffset: цел32): String;
перем
	len: цел32;
	string: String;
нач
	len := BufGetInt (buf, receiveBufOffset);
	нов (string, len);
	Strings.Copy (buf^, receiveBufOffset, len, string^);
	увел (receiveBufOffset, len);
	возврат string;
кон BufGetString;

проц isNextSeqNum* (current, previous: цел16): булево;
нач
	если (previous < current) или ((previous > current) и (previous > 0) и (current < 0))  то
		возврат истина;
	иначе
		возврат ложь;
	всё;
кон isNextSeqNum;

проц SeqNumInACKList* (reqList: List; seqNum: цел16;
	перем req: ACKRec): булево;
перем
	i: цел32;
	ptr: динамическиТипизированныйУкль;
нач
	i := 0;
	нцПока i < reqList.GetCount () делай
		ptr := reqList.GetItem (i);
		req := ptr (ACKRec);
		если seqNum = req.seqNum то
			возврат истина;
		всё;
		увел (i);
	кц;
	возврат ложь;
кон SeqNumInACKList;

проц CommandDecode* (command: цел16; перем str: массив из симв8);
нач

	просей command из
	| ACK: str := "ACK";

	| SEND_MESSAGE: str := "SEND_MESSAGE";
	| LOGIN: str := "LOGIN";
	| KEEP_ALIVE: str := "KEEP_ALIVE";
	| SEND_TEXT_CODE: str := "SEND_TEXT_CODE";
	| INFO_REQ: str := "INFO_REQ";
	| NEW_USER_REG: str := "NEW_USER_REG";

	| LOGIN_REPLY: str := "LOGIN_REPLY";
	| USER_ONLINE: str := "USER_ONLINE";
	| USER_OFFLINE: str := "USER_OFFLINE";
	| RECEIVE_MESSAGE: str := "RECEIVE_MESSAGE";
	| INFO_REPLY: str := "INFO_REPLY";
	| NEW_USER_REPLY: str := "NEW_USER_REPLY";
	иначе
		str := "Unknown";
	всё;

кон CommandDecode;

(* BEGIN *)
	(*Init;*)

кон UDPChatBase.
