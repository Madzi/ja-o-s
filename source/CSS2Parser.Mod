модуль CSS2Parser;	(** Stefan Walthert  *)
(** AUTHOR "swalthert"; PURPOSE ""; *)

использует
	ЛогЯдра, Strings, Scanner := CSS2Scanner, XMLObjects, CSS2, Files;

тип
	String = CSS2.String;

	Parser* = окласс
		перем
			reportError*: проц(pos, line, row: цел32; msg: массив из симв8);
			scanner: Scanner.Scanner;

		проц & Init*(scanner: Scanner.Scanner);
		нач
			reportError := DefaultReportError;
			сам.scanner := scanner;
			scanner.Scan()
		кон Init;

		проц CheckSymbol(expectedSymbols: мнвоНаБитахМЗ; errormsg: массив из симв8): булево;
		нач
			если scanner.sym в expectedSymbols то
				возврат истина
			иначе
				Error(errormsg);
				возврат ложь
			всё
		кон CheckSymbol;

		проц Error(msg: массив из симв8);
		нач
			reportError(scanner.GetPos(), scanner.line, scanner.row, msg)
		кон Error;

		проц Parse*(): CSS2.StyleSheet;
		перем styleSheet: CSS2.StyleSheet; s: String;
		нач
			нов(styleSheet);
			s := scanner.GetStr();
			если (scanner.sym = Scanner.AtKeyword) и (s^ = 'charset') то
				scanner.Scan();
				если ~CheckSymbol({Scanner.String}, "charset expected") то возврат styleSheet всё;
				s := scanner.GetStr(); styleSheet.SetCharSet(s^);
				scanner.Scan();
				если ~CheckSymbol({Scanner.Semicolon}, "';' expected") то возврат styleSheet всё;
				scanner.Scan()
			всё;
			нцПока scanner.sym в {Scanner.Cdo, Scanner.Cdc} делай scanner.Scan() кц;
			s := scanner.GetStr();
			нцПока (scanner.sym = Scanner.AtKeyword) и (s^ = 'import') делай
				ParseImport(styleSheet);
				s := scanner.GetStr()
			кц;
			нцПока scanner.sym # Scanner.Eof делай
				если scanner.sym = Scanner.AtKeyword то
					s := scanner.GetStr();
					если s^ = 'media' то
						ParseMedia(styleSheet)
					аесли s^ = 'page' то
						styleSheet.AddPage(ParsePage())
					аесли s^ = 'font-face' то
						styleSheet.AddFontFace(ParseFontFace())
					иначе	(* skip unknown atkeyword *)
						IgnoreKeyword()
					всё
				аесли scanner.sym # Scanner.Eof то
					styleSheet.AddRuleSet(ParseRuleSet())
				всё;
				нцПока scanner.sym в {Scanner.Cdo, Scanner.Cdc} делай scanner.Scan() кц
			кц;
			возврат styleSheet
		кон Parse;

		проц ParseImport(styleSheet: CSS2.StyleSheet);
		перем s: String; newParser: Parser; newScanner: Scanner.Scanner; file: Files.File;
			importedStyleSheet: CSS2.StyleSheet; media, media2, media3: мнвоНаБитахМЗ; ruleSets: XMLObjects.Enumerator;
			ruleSet: динамическиТипизированныйУкль;
		нач
			scanner.Scan();
			если ~CheckSymbol({Scanner.String, Scanner.URI}, "URI expected") то возврат всё;
			s := scanner.GetStr();
			file := Files.Old(s^);
			если file # НУЛЬ то
				нов(newScanner, file);
				нов(newParser, newScanner); newParser.reportError := reportError;
				importedStyleSheet := newParser.Parse()
			всё;
			scanner.Scan();
			если scanner.sym # Scanner.Ident то
				включиВоМнвоНаБитах(media, CSS2.All)
			иначе
				s := scanner.GetStr();
				включиВоМнвоНаБитах(media, GetMedium(s^));
				scanner.Scan();
				нцПока scanner.sym = Scanner.Comma делай
					scanner.Scan();
					если ~CheckSymbol({Scanner.Ident}, "medium identifier expected") то возврат всё;
					s := scanner.GetStr();
					включиВоМнвоНаБитах(media, GetMedium(s^));
					scanner.Scan()
				кц
			всё;
			ruleSets := importedStyleSheet.GetRuleSets();
			нцПока ruleSets.HasMoreElements() делай
				ruleSet := ruleSets.GetNext();
				media2 := ruleSet(CSS2.RuleSet).GetMedia();
				media3 := media + media2;
				если (media3 - {CSS2.All} # {}) то media3 := media3 - {CSS2.All} всё;
				ruleSet(CSS2.RuleSet).SetMedia(media3);
				styleSheet.AddRuleSet(ruleSet(CSS2.RuleSet))
			кц;
			если ~CheckSymbol({Scanner.Semicolon}, "';' expected") то возврат всё;
			scanner.Scan()
		кон ParseImport;

		проц ParseMedia(styleSheet: CSS2.StyleSheet);
		перем s: String; media: мнвоНаБитахМЗ; ruleSet: CSS2.RuleSet;
		нач
			scanner.Scan();
			если ~CheckSymbol({Scanner.Ident}, "medium identifier expected") то возврат всё;
			s := scanner.GetStr();
			включиВоМнвоНаБитах(media, GetMedium(s^));
			scanner.Scan();
			нцПока scanner.sym = Scanner.Comma делай
				scanner.Scan();
				если ~CheckSymbol({Scanner.Ident}, "medium identifier expected") то возврат всё;
				s := scanner.GetStr();
				включиВоМнвоНаБитах(media, GetMedium(s^));
				scanner.Scan()
			кц;
			если ~CheckSymbol({Scanner.BraceOpen}, "'{' expected") то возврат всё;
			scanner.Scan();
			нцПока (scanner.sym # Scanner.BraceClose) и (scanner.sym # Scanner.Eof) и (scanner.sym # Scanner.Invalid) делай
				ruleSet := ParseRuleSet();
				ruleSet.SetMedia(media);
				styleSheet.AddRuleSet(ruleSet)
			кц;
			если ~CheckSymbol({Scanner.BraceClose}, "'}' expected") то возврат всё;
			scanner.Scan()
		кон ParseMedia;

		проц ParsePage(): CSS2.Page;
		перем page: CSS2.Page; s: String;
		нач
			scanner.Scan();
			если ~CheckSymbol({Scanner.Ident, Scanner.Colon, Scanner.BraceOpen},
					"page selector, pseudo page or '{' expected") то возврат page всё;
			нов(page);
			если scanner.sym = Scanner.Ident то
				s := scanner.GetStr();
				page.SetSelector(s^);
				scanner.Scan()
			всё;
			если ~CheckSymbol({Scanner.Colon, Scanner.BraceOpen}, "pseudo page or '{' expected") то возврат page всё;
			если scanner.sym = Scanner.Colon то
				scanner.Scan();
				если ~CheckSymbol({Scanner.Ident}, "pseudo page identifier expected") то возврат page всё;
				s := scanner.GetStr();
				page.SetPseudoPage(GetPseudoPage(s^));
				scanner.Scan()
			всё;
			если ~CheckSymbol({Scanner.BraceOpen}, "'{' expected") то возврат page всё;
			scanner.Scan();
			page.AddDeclaration(ParseDeclaration());
			нцПока scanner.sym = Scanner.Semicolon делай
				scanner.Scan();
				page.AddDeclaration(ParseDeclaration());
			кц;
			если ~CheckSymbol({Scanner.BraceClose}, "'}' expected") то возврат page всё;
			scanner.Scan();
			возврат page
		кон ParsePage;

		проц ParseFontFace(): CSS2.FontFace;
		перем fontFace: CSS2.FontFace;
		нач
			scanner.Scan();
			если ~CheckSymbol({Scanner.BraceOpen}, "'{' expected") то возврат fontFace всё;
			нов(fontFace);
			scanner.Scan();
			fontFace.AddDeclaration(ParseDeclaration());
			нцПока scanner.sym = Scanner.Semicolon делай
				scanner.Scan();
				fontFace.AddDeclaration(ParseDeclaration());
			кц;
			если ~CheckSymbol({Scanner.BraceClose}, "'}' expected") то возврат fontFace всё;
			scanner.Scan();
			возврат fontFace
		кон ParseFontFace;

		проц ParseRuleSet(): CSS2.RuleSet;
		перем ruleSet: CSS2.RuleSet;
		нач
			нов(ruleSet);
			ruleSet.AddSelector(ParseSelector());
			нцПока scanner.sym = Scanner.Comma делай
				scanner.Scan();
				ruleSet.AddSelector(ParseSelector())
			кц;
			если ~CheckSymbol({Scanner.BraceOpen}, "'{' expected") то возврат ruleSet всё;
			scanner.Scan();
			ruleSet.AddDeclaration(ParseDeclaration());
			нцПока scanner.sym = Scanner.Semicolon делай
				scanner.Scan();
				если scanner.sym # Scanner.BraceClose то ruleSet.AddDeclaration(ParseDeclaration()) всё
			кц;
			если ~CheckSymbol({Scanner.BraceClose}, "'}' expected") то возврат ruleSet всё;
			scanner.Scan();
			возврат ruleSet
		кон ParseRuleSet;

		проц ParseSelector(): CSS2.Selector;
		перем selector: CSS2.Selector;
		нач
			нов(selector);
			selector.AddSimpleSelector(ParseSimpleSelector());
			нцПока scanner.sym в {Scanner.Ident, Scanner.Asterisk, Scanner.Hash, Scanner.Dot, Scanner.BracketOpen,
					Scanner.Colon, Scanner.Greater, Scanner.Plus} делай
				selector.AddSimpleSelector(ParseSimpleSelector())
			кц;
			возврат selector
		кон ParseSelector;

		проц ParseSimpleSelector(): CSS2.SimpleSelector;
		перем simpleSelector: CSS2.SimpleSelector; s: String;
		нач
			нов(simpleSelector);
			если scanner.sym = Scanner.Plus то
				simpleSelector.SetCombinator(CSS2.Sibling); scanner.Scan()
			аесли scanner.sym = Scanner.Greater то
				simpleSelector.SetCombinator(CSS2.Child); scanner.Scan()
			иначе
				simpleSelector.SetCombinator(CSS2.Descendant)
			всё;
			если scanner.sym = Scanner.Ident то
				s := scanner.GetStr();
				simpleSelector.SetElementName(s^); scanner.Scan()
			иначе
				нов(s, 2); s[0] := '*'; s[1] := 0X;
				simpleSelector.SetElementName(s^);
				если scanner.sym = Scanner.Asterisk то scanner.Scan() всё
			всё;
			нцПока scanner.sym в {Scanner.Hash, Scanner.Dot, Scanner.BracketOpen, Scanner.Colon} делай
				просей scanner.sym из
				| Scanner.Hash: simpleSelector.AddSubSelector(ParseId())
				| Scanner.Dot: simpleSelector.AddSubSelector(ParseClass())
				| Scanner.BracketOpen: simpleSelector.AddSubSelector(ParseAttribute())
				| Scanner.Colon: simpleSelector.AddSubSelector(ParsePseudo())
				иначе	(* do nothing *)
				всё
			кц;
			возврат simpleSelector
		кон ParseSimpleSelector;

		проц ParseId(): CSS2.Id;
		перем id: CSS2.Id; s: String;
		нач
			если ~CheckSymbol({Scanner.Hash}, "'#'element id expected") то возврат id всё;
			нов(id);
			s := scanner.GetStr();
			id.SetValue(s^);
			scanner.Scan();
			возврат id
		кон ParseId;

		проц ParseClass(): CSS2.Class;
		перем class: CSS2.Class; s: String;
		нач
			если ~CheckSymbol({Scanner.Dot}, "'.'class value expected") то возврат class всё;
			scanner.Scan();
			если ~CheckSymbol({Scanner.Ident}, "class value expected") то возврат class всё;
			нов(class);
			s := scanner.GetStr();
			class.SetValue(s^);
			scanner.Scan();
			возврат class
		кон ParseClass;

		проц ParseAttribute(): CSS2.Attribute;
		перем attribute: CSS2.Attribute; s: String;
		нач
			если ~CheckSymbol({Scanner.BracketOpen}, "'[' expected") то возврат attribute всё;
			scanner.Scan();
			если ~CheckSymbol({Scanner.Ident}, "attribute name expected") то возврат attribute всё;
			нов(attribute);
			s := scanner.GetStr();
			attribute.SetName(s^);
			scanner.Scan();
			если scanner.sym в {Scanner.Equal, Scanner.Includes, Scanner.Dashmatch} то
				просей scanner.sym из
				| Scanner.Equal: attribute.SetRelation(CSS2.Equal)
				| Scanner.Includes: attribute.SetRelation(CSS2.Includes)
				| Scanner.Dashmatch: attribute.SetRelation(CSS2.Dashmatch)
				всё;
				scanner.Scan();
				если ~CheckSymbol({Scanner.Ident, Scanner.String}, "attribute value expected") то возврат attribute всё;
				s := scanner.GetStr();
				attribute.SetValue(s^);
				scanner.Scan()
			всё;
			если ~CheckSymbol({Scanner.BracketClose}, "']' expected") то возврат attribute всё;
			scanner.Scan();
			возврат attribute
		кон ParseAttribute;

		проц ParsePseudo(): CSS2.Pseudo;
		перем pseudo: CSS2.Pseudo; s: String;
		нач
			если ~CheckSymbol({Scanner.Colon}, "':' expected") то возврат pseudo всё;
			scanner.Scan();
			если ~CheckSymbol({Scanner.Ident, Scanner.Function}, "':'type expected") то возврат pseudo всё;
			s := scanner.GetStr();
			нов(pseudo);
			pseudo.SetType(s^);
			если (scanner.sym = Scanner.Function) и (s^ = 'lang') то
				scanner.Scan();
				если ~CheckSymbol({Scanner.Ident}, "language expected") то возврат pseudo всё;
				s := scanner.GetStr();
				pseudo.SetLanguage(s^);
				scanner.Scan();
				если ~CheckSymbol({Scanner.ParenClose}, "')' expected") то возврат pseudo всё
			всё;
			scanner.Scan();
			возврат pseudo
		кон ParsePseudo;

		проц ParseDeclaration(): CSS2.Declaration;
		перем declaration: CSS2.Declaration; s: String;
		нач
			если ~CheckSymbol({Scanner.Ident}, "declaration property expected") то возврат declaration всё;
			нов(declaration);
			s := scanner.GetStr();
			declaration.SetProperty(s^);
			scanner.Scan();
			если ~CheckSymbol({Scanner.Colon}, "':' expected") то возврат declaration всё;
			scanner.Scan();
			declaration.AddTerm(ParseTerm());
			нцПока ~(scanner.sym в {Scanner.Semicolon, Scanner.BraceClose, Scanner.Important, Scanner.Eof})
					и (scanner.sym # Scanner.Invalid) делай	(* expr *)
				declaration.AddTerm(ParseTerm())
			кц;
			если scanner.sym = Scanner.Important то
				declaration.SetImportant(истина);
				scanner.Scan()
			всё;
			возврат declaration
		кон ParseDeclaration;

		проц ParseTerm(): CSS2.Term;
		перем term: CSS2.Term; s: String;
		нач
			нов(term);
			если scanner.sym = Scanner.Slash то
				term.SetOperator(CSS2.Slash); scanner.Scan()
			аесли scanner.sym = Scanner.Comma то
				term.SetOperator(CSS2.Comma); scanner.Scan()
			всё;
			если scanner.sym = Scanner.Minus то
				term.SetUnaryOperator(CSS2.Minus); scanner.Scan()
			аесли scanner.sym = Scanner.Plus то
				term.SetUnaryOperator(CSS2.Plus); scanner.Scan()
			всё;
			просей scanner.sym из
			| Scanner.Number:
					если scanner.numberType = Scanner.Integer то
						term.SetType(CSS2.IntNumber); term.SetIntVal(scanner.intVal)
					аесли scanner.numberType = Scanner.Real то
						term.SetType(CSS2.RealNumber); term.SetRealVal(scanner.realVal)
					всё
			| Scanner.Percentage:
					term.SetType(CSS2.Percent);
					если scanner.numberType = Scanner.Integer то
						term.SetRealVal(scanner.intVal / 100)
					аесли scanner.numberType = Scanner.Real то
						term.SetRealVal(scanner.realVal / 100)
					всё
			| Scanner.Dimension:
					если scanner.numberType = Scanner.Integer то
						term.SetType(CSS2.IntDimension); term.SetIntVal(scanner.intVal)
					аесли scanner.numberType = Scanner.Real то
						term.SetType(CSS2.RealDimension); term.SetRealVal(scanner.realVal)
					всё;
					s := scanner.GetStr();
					term.SetUnit(GetTermUnit(s^))
			| Scanner.Function:
					s := scanner.GetStr();
					если (s^ = 'rgb') или (s^ = 'rgba') то
						scanner.Scan();
						term.SetType(CSS2.Color); term.SetIntVal(ParseRGB(s^ = 'rgba'))
					иначе
						term.SetType(CSS2.Function); term.SetStringVal(s^);
						scanner.Scan();
						term.AddTerm(ParseTerm());
						нцПока scanner.sym в {Scanner.Slash, Scanner.Comma} делай
							term.AddTerm(ParseTerm())
						кц;
						если ~CheckSymbol({Scanner.ParenClose}, "')' expected") то возврат term всё;
					всё
			| Scanner.String:
					s := scanner.GetStr();
					term.SetType(CSS2.StringVal); term.SetStringVal(s^)
			| Scanner.Ident:
					s := scanner.GetStr();
					term.SetType(CSS2.StringIdent); term.SetStringVal(s^)
			| Scanner.URI:
					s := scanner.GetStr();
					term.SetType(CSS2.URI); term.SetStringVal(s^)
			(* | Scanner.Unicoderange	*)
			| Scanner.Hash:
					s := scanner.GetStr();
					term.SetType(CSS2.Color); term.SetIntVal(ComputeRGB(s^))
			иначе
				Error("unknown symbol")
			всё;
			scanner.Scan();
			возврат term
		кон ParseTerm;

		проц ParseRGB(hasAlpha: булево): цел32;
		перем term: CSS2.Term; r, g, b, a: цел32;
		нач
			term := ParseTerm();
			если (term # НУЛЬ) и (term.GetOperator() = CSS2.Undefined) и (term.GetUnaryOperator() = CSS2.Plus) то
				если (term.GetType() = CSS2.Percent) то r := округлиВниз(0.5 + term.GetRealVal() * 255)
				аесли (term.GetType() = CSS2.IntNumber) то r := term.GetIntVal()
				аесли (term.GetType() = CSS2.RealNumber) то r := округлиВниз(0.5 + term.GetRealVal())
				иначе Error("<number>'%' expected"); возврат 0
				всё
			иначе
				Error("<number>'%' expected"); возврат 0
			всё;
			term := ParseTerm();
			если (term # НУЛЬ) и (term.GetOperator() = CSS2.Comma) и (term.GetUnaryOperator() = CSS2.Plus) то
				если (term.GetType() = CSS2.Percent) то g := округлиВниз(0.5 + term.GetRealVal() * 255)
				аесли (term.GetType() = CSS2.IntNumber) то g := term.GetIntVal()
				аесли (term.GetType() = CSS2.RealNumber) то g := округлиВниз(0.5 + term.GetRealVal())
				иначе Error("<number>'%' expected"); возврат 0
				всё
			иначе
				Error("<number>'%' expected"); возврат 0
			всё;
			term := ParseTerm();
			если (term # НУЛЬ) и (term.GetOperator() = CSS2.Comma) и (term.GetUnaryOperator() = CSS2.Plus) то
				если (term.GetType() = CSS2.Percent) то b := округлиВниз(0.5 + term.GetRealVal() * 255)
				аесли (term.GetType() = CSS2.IntNumber) то b := term.GetIntVal()
				аесли (term.GetType() = CSS2.RealNumber) то b := округлиВниз(0.5 + term.GetRealVal())
				иначе Error("<number>'%' expected"); возврат 0
				всё
			иначе
				Error("<number>'%' expected"); возврат 0
			всё;
			если hasAlpha то
				term := ParseTerm();
				если (term # НУЛЬ) и (term.GetOperator() = CSS2.Comma) и (term.GetUnaryOperator() = CSS2.Plus) то
					если (term.GetType() = CSS2.Percent) то a := округлиВниз(0.5 + term.GetRealVal() * 255)
					аесли (term.GetType() = CSS2.IntNumber) то a := term.GetIntVal()
					аесли (term.GetType() = CSS2.RealNumber) то a := округлиВниз(0.5 + term.GetRealVal())
					иначе Error("<number>'%' expected"); возврат 0
					всё
				всё
			иначе
				a := 0
			всё;
			если ~CheckSymbol({Scanner.ParenClose}, "')' expected") то возврат 0 всё;
			возврат арифмСдвиг(a, 24) + арифмСдвиг(r, 16) + арифмСдвиг(g, 8) + b
		кон ParseRGB;

		проц IgnoreKeyword;
		нач
			нцПока (scanner.sym # Scanner.BraceOpen) и (scanner.sym # Scanner.Semicolon) и (scanner.sym # Scanner.Eof)
					и (scanner.sym # Scanner.Invalid) делай
				scanner.Scan();
				если scanner.sym = Scanner.AtKeyword то IgnoreKeyword() всё
			кц;
			если ~CheckSymbol({Scanner.BraceOpen, Scanner.Semicolon}, "'{' or ';' expected") то возврат всё;
			если scanner.sym = Scanner.BraceOpen то
				нцПока (scanner.sym # Scanner.BraceClose) и (scanner.sym # Scanner.Eof) и (scanner.sym # Scanner.Invalid) делай
					scanner.Scan();
					если scanner.sym = Scanner.AtKeyword то IgnoreKeyword() всё
				кц;
				если ~CheckSymbol({Scanner.BraceClose}, "'}' expected") то возврат всё
			всё;
			scanner.Scan()
		кон IgnoreKeyword;

	кон Parser;

	проц GetMedium(mediumStr: массив из симв8): цел8;
	нач
		если mediumStr = 'all' то возврат CSS2.All
		аесли mediumStr = 'aural' то возврат CSS2.Aural
		аесли mediumStr = 'braille' то возврат CSS2.Braille
		аесли mediumStr = 'embossed' то возврат CSS2.Embossed
		аесли mediumStr = 'handheld' то возврат CSS2.Handheld
		аесли mediumStr = 'print' то возврат CSS2.Print
		аесли mediumStr = 'projection' то возврат CSS2.Projection
		аесли mediumStr = 'screen' то возврат CSS2.Screen
		аесли mediumStr = 'tty' то возврат CSS2.TTY
		аесли mediumStr = 'tv' то возврат CSS2.TV
		иначе возврат CSS2.All
		всё
	кон GetMedium;

	проц GetPseudoPage(pseudoPageStr: массив из симв8): цел8;
	нач
		если pseudoPageStr = 'left' то возврат CSS2.Left
		аесли pseudoPageStr = 'right' то возврат CSS2.Right
		аесли pseudoPageStr = 'first' то возврат CSS2.First
		иначе возврат CSS2.Undefined
	всё
	кон GetPseudoPage;

(*	PROCEDURE GetPseudoType(typeStr: ARRAY OF CHAR): SIGNED8;
	BEGIN
		IF typeStr = 'first-child' THEN RETURN CSS2.FirstChild
		ELSIF typeStr = 'link' THEN RETURN CSS2.Link
		ELSIF typeStr = 'visited' THEN RETURN CSS2.Visited
		ELSIF typeStr = 'hover' THEN RETURN CSS2.Hover
		ELSIF typeStr = 'active' THEN RETURN CSS2.Active
		ELSIF typeStr = 'focus' THEN RETURN CSS2.Focus
		ELSIF typeStr = 'first-line' THEN RETURN CSS2.FirstLine
		ELSIF typeStr = 'first-letter' THEN RETURN CSS2.FirstLetter
		ELSIF typeStr = 'before' THEN RETURN CSS2.Before
		ELSIF typeStr = 'after' THEN RETURN CSS2.After
		ELSE RETURN CSS2.Undefined
		END
	END GetPseudoType;*)

	проц GetTermUnit(unitStr: массив из симв8): цел8;
	нач
		если unitStr = 'em' то возврат CSS2.em
		аесли unitStr = 'ex' то возврат CSS2.ex
		аесли unitStr = 'px' то возврат CSS2.px
		аесли unitStr = 'in' то возврат CSS2.in
		аесли unitStr = 'cm' то возврат CSS2.cm
		аесли unitStr = 'mm' то возврат CSS2.mm
		аесли unitStr = 'pt' то возврат CSS2.pt
		аесли unitStr = 'pc' то возврат CSS2.pc
		аесли unitStr = 'deg' то возврат CSS2.deg
		аесли unitStr = 'grad' то возврат CSS2.grad
		аесли unitStr = 'rad' то возврат CSS2.rad
		аесли unitStr = 'ms' то возврат CSS2.ms
		аесли unitStr = 's' то возврат CSS2.s
		аесли unitStr = 'Hz' то возврат CSS2.Hz
		аесли unitStr = 'kHz' то возврат CSS2.kHz
		иначе возврат CSS2.Undefined
		всё
	кон GetTermUnit;

	проц ComputeRGB(перем s: массив из симв8): цел32;
	перем col: цел32; r, g, b, a: цел32;
	нач
		HexStrToInt(s, col);
		если (Strings.Length(s) = 6) или (Strings.Length(s) = 8) то
			возврат col
		аесли (Strings.Length(s) = 3) или (Strings.Length(s) = 4) то
			a := col DIV 1000H; r := (col DIV 100H) остОтДеленияНа 10H; g := (col DIV 10H) остОтДеленияНа 10H; b := col остОтДеленияНа 10H;
			возврат арифмСдвиг(a, 28) + арифмСдвиг(a, 24) + арифмСдвиг(r, 20) + арифмСдвиг(r, 16) + арифмСдвиг(g, 12) + арифмСдвиг(g, 8) + арифмСдвиг(b, 4) + b
		иначе
			возврат 0
		всё
	кон ComputeRGB;

	проц HexStrToInt(перем str: массив из симв8; перем val: цел32);
	перем i, d: цел32; ch: симв8;
	нач
		i := 0; ch := str[0];
		нцПока (ch # 0X) и (ch <= " ") делай
			увел(i); ch := str[i]
		кц;
		val := 0;
		нцПока (("0" <= ch) и (ch <= "9")) или (("A" <= ch) и (ch <= "F")) делай
			если (("0" <= ch) и (ch <= "9")) то d := кодСимв8(ch)-кодСимв8("0")
			иначе d := кодСимв8(ch) - кодСимв8("A") + 10
			всё;
			увел(i); ch := str[i];
			val := арифмСдвиг(val, 4)+d
		кц
	кон HexStrToInt;

(*	PROCEDURE SetKeyword();
	VAR s: DynamicStrings.String;
	BEGIN
		sym := Import;
		s := GetStr();
		IF s^ = 'import' THEN keyword := Import
		ELSIF s^ = 'page' THEN keyword := Page
		ELSIF s^ = 'media' THEN keyword := Media
		ELSIF s^ = 'font-face' THEN keyword := FontFace
		ELSIF s^ = 'charset' THEN keyword := CharSet
		ELSE keyword := Unknown
		END
	END SetKeyword; *)

	проц DefaultReportError(pos, line, row: цел32; msg: массив из симв8);
	нач
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСтроку8("pos "); ЛогЯдра.пЦел64(pos, 6);
		ЛогЯдра.пСтроку8(", line "); ЛогЯдра.пЦел64(line, 0); ЛогЯдра.пСтроку8(", row "); ЛогЯдра.пЦел64(row, 0);
		ЛогЯдра.пСтроку8("    "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	кон DefaultReportError;

кон CSS2Parser.
