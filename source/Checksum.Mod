модуль Checksum;	(** AUTHOR "GF"; PURPOSE "compute md5, sha1 and sha3 checksums" *)

использует Commands, Files, Потоки, Hashes := CryptoHashes;

перем
	hexdigits: массив 17 из симв8;


	проц MD5*( c: Commands.Context );
	нач
		Do( c, Hashes.NewHash( "CryptoMD5" ) )
	кон MD5;

	проц SHA1*( c: Commands.Context );
	нач
		Do( c, Hashes.NewHash( "CryptoSHA1" ) )
	кон SHA1;

	проц SHA3*( c: Commands.Context );
	перем h: Hashes.Hash;
	нач
		h := Hashes.NewHash( "CryptoSHA3" );
	(*	h.SetNameAndSize( "", 32 )	*)	(* 256 bit, default *)
		Do( c, h )
	кон SHA3;


	проц Do( c: Commands.Context; h: Hashes.Hash );
	перем
		f: Files.File;  r: Files.Reader;
		fname: массив 256 из симв8;
	нач
		нцПока OpenNextFile( c, f ) делай
			Files.OpenReader( r, f, 0 );
			ComputeHash( r, h, c.out );
			f.GetName( fname );
			c.out.пСтроку8( '    ' );  c.out.пСтроку8( fname );  c.out.пВК_ПС;
			c.out.ПротолкниБуферВПоток
		кц;
	кон Do;

	проц OpenNextFile( c: Commands.Context;  перем f: Files.File ): булево;
	перем
		fname: массив 64 из симв8;
	нач
		если c.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( fname ) то
			f := Files.Old( fname );
			если f = НУЛЬ то
				c.error.пСтроку8( "File " );  c.error.пСтроку8( fname );  c.error.пСтроку8( " not fond" );
				c.error.пВК_ПС; c.error.ПротолкниБуферВПоток;
				возврат ложь
			иначе
				возврат истина
			всё
		иначе
			возврат ложь
		всё
	кон OpenNextFile;


	проц ComputeHash( r: Files.Reader; h: Hashes.Hash; out: Потоки.Писарь );
	перем
		buf: массив 128 из симв8;
		got: размерМЗ; i, x: цел32;
	нач
		h.Initialize;
		нцПока r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0 делай
			r.чБайты( buf, 0, 64, got );
			h.Update( buf, 0, got )
		кц;
		h.GetHash( buf, 0 );
		нцДля i := 0 до h.size - 1 делай
			x := кодСимв8( buf[i] );
			out.пСимв8( hexdigits[x DIV 16] );
			out.пСимв8( hexdigits[x остОтДеленияНа 16] )
		кц;
	кон ComputeHash;


нач
	hexdigits := "0123456789abcdef"
кон Checksum.



	Checksum.MD5
			Files.Mod
			Unix.UnixFiles.Mod
			NoFile.Mod
			~

	Checksum.SHA1  Files.Mod  ~

	Checksum.SHA3  Files.Mod  ~

	System.Free Checksum ~
