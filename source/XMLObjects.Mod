модуль XMLObjects; (** AUTHOR "swalthert"; PURPOSE "XML collections and enumerators"; *)

использует
	Strings;

тип
	String = Strings.String;

	Collection* = окласс

		проц GetNumberOfElements*(): цел32;
		нач
			возврат 0
		кон GetNumberOfElements;

		проц GetEnumerator*(): Enumerator;
		нач
			возврат НУЛЬ
		кон GetEnumerator;

		проц Add*(p: динамическиТипизированныйУкль);
		кон Add;

		проц Remove*(p: динамическиТипизированныйУкль);
		кон Remove;

	кон Collection;

	ListElem = окласс
	перем
		elem: динамическиТипизированныйУкль;
		next: ListElem;

		проц &Init*(elem : динамическиТипизированныйУкль);
		нач
			сам.elem := elem;
			next := НУЛЬ;
		кон Init;

	кон ListElem;

	List* = окласс (Collection)
	перем
		first, last: ListElem;
		nofElems: цел32;

		проц & Init*;
		нач
			nofElems := 0
		кон Init;

		проц {перекрыта}GetNumberOfElements*(): цел32;
		нач
			возврат nofElems
		кон GetNumberOfElements;

		проц {перекрыта}GetEnumerator*(): Enumerator;
		перем le: ListEnumerator;
		нач
			нов(le, сам);
			возврат le
		кон GetEnumerator;

		проц {перекрыта}Add*(p: динамическиТипизированныйУкль);
		перем newListElem: ListElem;
		нач {единолично}
			если p # НУЛЬ то
				нов(newListElem, p);
				если last = НУЛЬ то
					first := newListElem;
					last := newListElem
				иначе
					last.next := newListElem;
					last := last.next
				всё;
				увел(nofElems)
			всё
		кон Add;

		проц {перекрыта}Remove*(p: динамическиТипизированныйУкль);
		перем le: ListElem;
		нач {единолично}
			если (p # НУЛЬ) и (first # НУЛЬ) то
				если first.elem = p то
					first := first.next; умень(nofElems)
				иначе
					le := first;
					нцПока (le.next # НУЛЬ) и (le.next.elem # p) делай
						le := le.next
					кц;
					если le.next # НУЛЬ то (* le.next.elem = o *)
						le.next := le.next.next; умень(nofElems)
					всё
				всё
			всё
		кон Remove;

	кон List;

	PTRArray* = укль на массив из динамическиТипизированныйУкль;

	ArrayCollection* = окласс (Collection)
	перем
		elems: PTRArray;
		nofElems: цел32;

		проц  &Init*;
		нач
			nofElems := 0;
			нов(elems, 1)
		кон Init;

		проц {перекрыта}GetNumberOfElements*(): цел32;
		нач
			возврат nofElems
		кон GetNumberOfElements;

		проц {перекрыта}GetEnumerator*(): Enumerator;
		перем ace: ArrayEnumerator;
		нач
			нов(ace, elems);
			возврат ace
		кон GetEnumerator;

		проц Grow;
		перем i: цел32; oldElems: PTRArray;
		нач
			oldElems := elems;
			нов(elems, 2 * длинаМассива(elems));
			нцДля i := 0 до nofElems - 1 делай
				elems[i] := oldElems[i]
			кц
		кон Grow;

		проц {перекрыта}Add*(p: динамическиТипизированныйУкль);
		нач {единолично}
			если p # НУЛЬ то
				если nofElems = длинаМассива(elems) то Grow() всё;
				elems[nofElems] := p;
				увел(nofElems)
			всё
		кон Add;

		проц {перекрыта}Remove*(p: динамическиТипизированныйУкль);
		перем i: цел32;
		нач {единолично}
			i := 0;
			нцПока (i < nofElems) и (elems[i] # p) делай
				увел(i)
			кц;
			если i < nofElems то
				нцПока (i < nofElems - 1) делай
					elems[i] := elems[i + 1]; увел(i)
				кц;
				умень(nofElems); elems[nofElems] := НУЛЬ
			всё
		кон Remove;

		проц GetElement*(i: цел32): динамическиТипизированныйУкль;
		нач
			если (0 <= i) и (i < nofElems) то возврат elems[i]
			иначе возврат НУЛЬ
			всё
		кон GetElement;

		проц Invert*(ptr1, ptr2: динамическиТипизированныйУкль): булево;
		перем pos1, pos2, i: цел32; ptr: динамическиТипизированныйУкль;
		нач {единолично}
			i := 0;  pos1 := -1;  pos2 := -1;
			нцПока (i < nofElems) и ((pos1 < 0) или (pos2 < 0)) делай
				если elems[i] = ptr1 то  pos1 := i  всё;
				если elems[i] = ptr2 то  pos2 := i  всё;
				увел(i)
			кц;
			если (pos1 >= 0) и (pos2 >= 0) и (pos1 # pos2)  то
				ptr := elems[pos1];  elems[pos1] := elems[pos2];  elems[pos2] := ptr;
				возврат истина
			всё;
			возврат ложь
		кон Invert;

		проц GetElementPos*(ptr: динамическиТипизированныйУкль): цел32;
		перем i: цел32;
		нач
			нцПока i < nofElems делай
				если elems[i] = ptr то  возврат i всё;
				увел(i)
			кц;
			возврат -1
		кон GetElementPos;

		проц MoveUp*(ptr: динамическиТипизированныйУкль;  i: цел32): булево;
		перем p: динамическиТипизированныйУкль;
		нач {единолично}
			если ptr # НУЛЬ то  i := GetElementPos(ptr)  всё;
			если (i > 0) и (i < nofElems)  то
				p := elems[i];  elems[i] := elems[i-1];  elems[i-1] := p;
				возврат истина
			всё;
			возврат ложь
		кон MoveUp;

		проц MoveDown*(ptr: динамическиТипизированныйУкль;  i: цел32): булево;
		перем p: динамическиТипизированныйУкль;
		нач {единолично}
			если ptr # НУЛЬ то  i := GetElementPos(ptr)  всё;
			если (i >=0) и (i < nofElems-1)  то
				p := elems[i];  elems[i] := elems[i+1];  elems[i+1] := p;
				возврат истина
			всё;
			возврат ложь
		кон MoveDown;

	кон ArrayCollection;

	Enumerator* = окласс

		проц HasMoreElements*(): булево;
		нач
			возврат ложь
		кон HasMoreElements;

		проц GetNext*(): динамическиТипизированныйУкль;
		нач
			возврат НУЛЬ
		кон GetNext;

		проц Reset*;
		кон Reset;

	кон Enumerator;

	ListEnumerator* = окласс (Enumerator)
	перем
		coll: List;
		current: ListElem;

		проц & Init*(list: List);
		нач
			coll := list;
			current := list.first
		кон Init;

		проц {перекрыта}HasMoreElements*(): булево;
		нач
			возврат current # НУЛЬ
		кон HasMoreElements;

		проц {перекрыта}GetNext*(): динамическиТипизированныйУкль;
		перем p: динамическиТипизированныйУкль;
		нач
			если HasMoreElements() то p := current.elem; current := current.next; иначе p := НУЛЬ; всё;
			возврат p
		кон GetNext;

		проц {перекрыта}Reset*;
		нач
			Init(coll)
		кон Reset;

	кон ListEnumerator;

	ArrayEnumerator* = окласс (Enumerator)
	перем
		array: PTRArray;
		current: цел32;

		проц & Init*(array: PTRArray);
		нач
			сам.array := array;
			current := 0
		кон Init;

		проц {перекрыта}HasMoreElements*(): булево;
		нач
			возврат (current < длинаМассива(array)) и (array[current] # НУЛЬ)
		кон HasMoreElements;

		проц {перекрыта}GetNext*(): динамическиТипизированныйУкль;
		перем p: динамическиТипизированныйУкль;
		нач
			если HasMoreElements() то
				p := array[current]; увел(current);
			иначе
				p := НУЛЬ;
			всё;
			возврат p
		кон GetNext;

		проц {перекрыта}Reset*;
		нач
			Init(array)
		кон Reset;

	кон ArrayEnumerator;

	Dictionary* = окласс

		проц GetNumberOfElements*(): цел32;
		нач
			возврат 0
		кон GetNumberOfElements;

		проц Get*(key: массив из симв8): динамическиТипизированныйУкль;
		кон Get;

		проц GetEnumerator*(): Enumerator;
		нач
			возврат НУЛЬ
		кон GetEnumerator;

		проц Add*(key: массив из симв8; p: динамическиТипизированныйУкль);
		кон Add;

		проц Remove*(key: массив из симв8);
		кон Remove;

	кон Dictionary;

	StringArray = укль на массив из String;

	ArrayDict* = окласс (Dictionary)
	перем
		nofElems: цел32;
		keys: StringArray;
		elems: PTRArray;

		проц & Init*;
		нач
			nofElems := 0;
			нов(keys, 16);
			нов(elems, 16)
		кон Init;

		проц {перекрыта}GetNumberOfElements*(): цел32;
		нач
			возврат nofElems
		кон GetNumberOfElements;

		проц {перекрыта}Get*(key: массив из симв8): динамическиТипизированныйУкль;
		перем i: цел32;
		нач
			i := 0;
			нцПока (i < nofElems) и (keys[i]^ # key) делай
				увел(i)
			кц;
			если i < nofElems то возврат elems[i]
			иначе возврат НУЛЬ
			всё
		кон Get;

		проц {перекрыта}GetEnumerator*(): Enumerator;
		перем ace: ArrayEnumerator;
		нач
			нов(ace, elems);
			возврат ace
		кон GetEnumerator;

		проц Grow;
		перем i: цел32; oldKeys: StringArray; oldElems: PTRArray;
		нач
			oldKeys := keys; oldElems := elems;
			нов(keys, 2 * длинаМассива(keys)); нов(elems, 2 * длинаМассива(elems));
			нцДля i := 0 до nofElems - 1 делай
				keys[i] := oldKeys[i]; elems[i] := oldElems[i]
			кц
		кон Grow;

		проц {перекрыта}Add*(key: массив из симв8; p: динамическиТипизированныйУкль);
		нач {единолично}
			если Get(key) = НУЛЬ то
				если nofElems = длинаМассива(elems) то Grow() всё;
				нов(keys[nofElems], StringLength(key) + 1); копируйСтрокуДо0(key, keys[nofElems]^);
				elems[nofElems] := p;
				увел(nofElems)
			всё
		кон Add;

		проц {перекрыта}Remove*(key: массив из симв8);
		перем i: цел32;
		нач {единолично}
			i := 0;
			нцПока (i < nofElems) и (keys[i]^ # key) делай
				увел(i)
			кц;
			если i < nofElems то
				нцПока (i < nofElems - 1) делай
					elems[i] := elems[i + 1];
					keys[i] := keys[i + 1];
					увел(i)
				кц;
				умень(nofElems); keys[nofElems] := НУЛЬ; elems[nofElems] := НУЛЬ
			всё
		кон Remove;

	кон ArrayDict;

проц StringLength(конст string: массив из симв8): размерМЗ;
перем i, l: размерМЗ;
нач
	i := 0; l := длинаМассива(string);
	нцПока (i < l) и (string[i] # 0X) делай
		увел(i)
	кц;
	возврат i
кон StringLength;

кон XMLObjects.
