модуль SVNWebDAV; (** AUTHOR "rstoll"; *)

использует
	WebHTTP, Files, Strings, Потоки, Dates,
	XML, XMLObjects,ЛогЯдра,
	SVNAdmin, SVNUtil, SVNOutput,
	OdSvn, OdXml;


	проц Checkout* (svn : OdSvn.OdSvn; конст pathName : массив из симв8; конст workName : массив из симв8; перем res : целМЗ );
	перем
		name, err, vcc, repoUUID, bln, bc, version, workUrl : массив 256 из симв8;
		props: WebHTTP.AdditionalField;
		ver : цел32; pos : размерМЗ;
	нач
		svn.checkout := истина;
		res := SVNOutput.ResOK;
		Files.SplitPath ( pathName, workUrl, name );

		svn.repositoryPathLength := 999; (* we don't want a update-target in the update request *)

		если Files.Old ( workName ) = НУЛЬ то
			копируйСтрокуДо0 ( pathName, workUrl );
			svn.FileUpdate ( ложь );
		иначе
			svn.FileUpdate ( истина );
		всё;

		svn.Propfind ( workUrl, "D:version-controlled-configuration.D:resourcetype.D2:baseline-relative-path.D2:repository-uuid", props, err );

		если (svn.pfStatus >= 400) или (svn.pfStatus = 0) то
			PrintError ( svn, res );
			возврат;
		всё;

		если ~WebHTTP.GetAdditionalFieldValue(props, "DAV:version-controlled-configuration", vcc) то всё;
		если ~WebHTTP.GetAdditionalFieldValue(props, "http://subversion.tigris.org/xmlns/dav/repository-uuid", repoUUID) то всё;

		svn.Propfind ( vcc, "D:checked-in", props, err );
		если ~WebHTTP.GetAdditionalFieldValue ( props, "DAV:checked-in", bln ) то всё;

		svn.Propfind ( bln, "D:baseline-collection.D:version-name", props, err );
		если ~WebHTTP.GetAdditionalFieldValue ( props, "DAV:baseline-collection", bc ) то всё;
		если ~WebHTTP.GetAdditionalFieldValue ( props, "DAV:version-name", version ) то всё;

		если err # "" то
			svn.context.out.пСтроку8 ( err ); svn.context.out.пВК_ПС;
			возврат;
		всё;

		Strings.StrToIntPos(version, ver, pos);

		svn.UpdateReport ( pathName, vcc, workUrl, -1, ver, workName, res );
		svn.checkout := ложь;
	кон Checkout;





	(* SVN update using .svn directories *)
	проц Update* (svn : OdSvn.OdSvn; конст pathName : массив из симв8; pathNameVersion : цел32;  конст workName : массив из симв8; перем res : целМЗ );
	перем
		name, err, vcc, repoUUID, bln, bc, version, workUrl : массив 256 из симв8;
		props: WebHTTP.AdditionalField;
		ver : цел32; pos : размерМЗ;
	нач
		res := SVNOutput.ResOK;
		Files.SplitPath ( pathName, workUrl, name );

		если Files.Old ( workName ) = НУЛЬ то
			копируйСтрокуДо0 ( pathName, workUrl );
			svn.FileUpdate ( ложь );
		иначе
			svn.FileUpdate ( истина );
		всё;

		svn.traverseDummy := истина; (* first call *)
		SVNAdmin.Traverse ( workName, UpdateHandler, svn, ложь, res ); (* don't read the version url from wcprops *)

		svn.Propfind ( workUrl, "D:version-controlled-configuration.D:resourcetype.D2:baseline-relative-path.D2:repository-uuid", props, err );

		если (svn.pfStatus >= 400) или (svn.pfStatus = 0) то
			PrintError ( svn, res );
			возврат;
		всё;

		если ~WebHTTP.GetAdditionalFieldValue(props, "DAV:version-controlled-configuration", vcc) то всё;
		если ~WebHTTP.GetAdditionalFieldValue(props, "http://subversion.tigris.org/xmlns/dav/repository-uuid", repoUUID) то всё;

		svn.Propfind ( vcc, "D:checked-in", props, err );
		если ~WebHTTP.GetAdditionalFieldValue ( props, "DAV:checked-in", bln ) то всё;

		svn.Propfind ( bln, "D:baseline-collection.D:version-name", props, err );
		если ~WebHTTP.GetAdditionalFieldValue ( props, "DAV:baseline-collection", bc ) то всё;
		если ~WebHTTP.GetAdditionalFieldValue ( props, "DAV:version-name", version ) то всё;

		если err # "" то
			svn.context.out.пСтроку8 ( err ); svn.context.out.пВК_ПС;
			возврат;
		всё;

		Strings.StrToIntPos(version, ver, pos);

		svn.UpdateReport ( pathName, vcc, workUrl, pathNameVersion, ver, workName, res );
	кон Update;

	проц Commit* ( svn : OdSvn.OdSvn; конст pathName, workName, message : массив из симв8; перем res : целМЗ );
	перем
		act: OdSvn.Activity;
		uuid : Strings.String;
		name, err, vcc, bln, workUrl, wbl : массив 256 из симв8;
		props, patch : WebHTTP.AdditionalField;
		resHeader: WebHTTP.ResponseHeader;
	нач
		uuid := SVNUtil.GetUUID();
		нов ( act, svn.client, uuid^ );
		утв ( act # НУЛЬ );
		act.make;

		если act.getUrl() = НУЛЬ то
			svn.pfStatus := 0;
			PrintError ( svn, res );
			возврат;
		всё;

		Files.SplitPath ( pathName, workUrl, name );

		если Files.Old ( workName ) = НУЛЬ то
			копируйСтрокуДо0 ( pathName, workUrl );
		всё;

		svn.Propfind ( workUrl, "D:version-controlled-configuration", props, err );
		если ~WebHTTP.GetAdditionalFieldValue(props, "DAV:version-controlled-configuration", vcc) то всё;

		если (svn.pfStatus >= 400) или (svn.pfStatus = 0) то
			PrintError ( svn, res );
			возврат;
		всё;

		svn.Propfind ( vcc, "D:checked-in", props, err );
		если ~WebHTTP.GetAdditionalFieldValue ( props, "DAV:checked-in", bln ) то всё;

		svn.Checkout ( bln, resHeader, err );
		копируйСтрокуДо0 ( resHeader.location, wbl );

		WebHTTP.SetAdditionalFieldValue ( patch, "log xmlns=http://subversion.tigris.org/xmlns/svn/", message );
		svn.Proppatch ( wbl, patch, err );

		svn.Propfind ( workUrl, "D:checked-in", props, err );
		если ~WebHTTP.GetAdditionalFieldValue(props, "DAV:checked-in", svn.ver) то всё;

		svn.Checkout ( svn.ver, resHeader, err );
		копируйСтрокуДо0 ( resHeader.location, svn.wrk );

		svn.removeDir := ложь;
		svn.countChanges := 0;
		SVNAdmin.Traverse ( workName, CommitHandler, svn, истина, res );

		(* don't merge if there are no modified files *)
		если svn.countChanges > 0 то
			svn.Merge ( workUrl, act.getUrl(), resHeader, err );
			если resHeader.statuscode = 200 то
				ParseMergeContent ( svn, workUrl, workName );
			всё;
		всё;

		act.delete;
	кон Commit;


	проц ParseMergeContent ( svn : OdSvn.OdSvn; конст baseUrl, basePath : массив из симв8 );
	перем
		root, e, e2 : XML.Element;
		enum : XMLObjects.Enumerator;
		str, md5 : Strings.String;
		p : динамическиТипизированныйУкль;
		xml : OdXml.OdXml;
		vcc, vurl, ver, tmp, tmp2, path, name, date : массив 256 из симв8;
		creationdate, creator, status : массив 33 из симв8;
		version : массив 10 из симв8;
		ft,fd : цел32; res: целМЗ;
		adminDir : SVNAdmin.Entry;
		f : Files.File;
		len: размерМЗ;
	нач
		нов ( xml );
		нов ( adminDir, НУЛЬ );

		root := svn.resultDoc.GetRoot();
		str := root.GetName();

		если str^ # "D:merge-response" то возврат всё;

		enum := root.GetContents();
		если ~enum.HasMoreElements() то возврат всё;

		p := enum.GetNext();
		e := p ( XML.Element );
		str := e.GetName();
		если str^ # "D:updated-set" то возврат всё;

		enum := e.GetContents();
		если ~enum.HasMoreElements() то возврат всё;

		p := enum.GetNext();
		e := p ( XML.Element );
		str := e.GetName();
		если str^ # "D:response" то возврат всё;

		e2 := xml.SplitElement ( e, "DAV:href" );
		утв ( e2 # НУЛЬ );

		OdXml.GetCharData ( e2, vcc );

		e2 := xml.SplitElement ( e, "DAV:propstat.DAV:prop.DAV:creationdate" ); утв ( e2 # НУЛЬ );
		OdXml.GetCharData ( e2, creationdate );

		e2 := xml.SplitElement ( e, "DAV:propstat.DAV:prop.DAV:version-name" ); утв ( e2 # НУЛЬ );
		OdXml.GetCharData ( e2, version );

		e2 := xml.SplitElement ( e, "DAV:propstat.DAV:prop.DAV:creator-displayname" ); утв ( e2 # НУЛЬ );
		OdXml.GetCharData ( e2, creator );

		len := Strings.Length ( baseUrl );
		утв ( ~Strings.EndsWith ( "/", basePath ) );

		нцПока enum.HasMoreElements() делай
			p := enum.GetNext();
			если p суть XML.Element то
				e := p ( XML.Element );
				str := e.GetName();

				e2 := xml.SplitElement ( e, "DAV:href" ); утв ( e2 # НУЛЬ );
				OdXml.GetCharData ( e2, vurl );

				(*e2 := xml.SplitElement ( e, "DAV:propstat.DAV:prop.DAV:resourcetype.DAV:collection" );
				IF e2 = NIL THEN
					(*KernelLog.String ( "NIL collection" );*)
				ELSE
					(*KernelLog.String ( "NOT NIL collection" );*)
				END;*)

				e2 := xml.SplitElement ( e, "DAV:propstat.DAV:prop.DAV:checked-in.DAV:href" ); утв ( e2 # НУЛЬ );
				OdXml.GetCharData ( e2, ver );

				e2 := xml.SplitElement ( e, "DAV:propstat.DAV:status" ); утв ( e2 # НУЛЬ );
				OdXml.GetCharData ( e2, status );

				если Strings.Match ( "HTTP/1.? 200 OK", status ) то
					str := Strings.Substring2 ( len, vurl );
					SVNUtil.UrlDecode ( str^, tmp2 );
					Strings.Concat ( basePath, tmp2, tmp );

					adminDir.SetPath ( tmp, res ); утв ( res = SVNOutput.ResOK );
					adminDir.CreateTempfile;

					f := Files.Old ( tmp );
					если f # НУЛЬ то
						Files.SplitPath ( tmp, path, name );
						если adminDir.IsItemVersioned ( name ) то
							adminDir.ReadWriteLines ( 1 );
							adminDir.ReadWriteString ( version );
							adminDir.ReadWriteLines ( 2 );
							adminDir.ReadWriteString ( "" ); (* remove scheduled stuff *)
							adminDir.ReadWriteString ( creationdate );
							md5 := SVNUtil.GetChecksum ( tmp );
							adminDir.ReadWriteString ( md5^ );
							f.GetDate ( ft, fd );
							Strings.FormatDateTime ( SVNOutput.DateFormat, Dates.OberonToDateTime ( fd, ft ), date );
							adminDir.ReadWriteString ( date );
							adminDir.ReadWriteString ( version );
							adminDir.ReadWriteString ( creator );
							adminDir.ReadWriteRest;
							adminDir.WriteUpdate;

							SVNAdmin.CopyToBaseFile ( tmp );

							SVNAdmin.WriteWCPROPS ( path, name, ver );
						иначе
							svn.context.out.пСтроку8 ( "ERROR: received merge request, but item is not versioned" );
							svn.context.out.пВК_ПС;
						всё;
					иначе
						adminDir.ReadWriteLines ( 3 );
						adminDir.ReadWriteString ( version );
						adminDir.ReadWriteLines ( 2 );
						adminDir.ReadWriteString ( "" ); (* remove scheduled stuff *)
						adminDir.ReadWriteLines ( 2 );
						adminDir.ReadWriteString ( creationdate ); (* correct? *)
						adminDir.ReadWriteString ( version );
						adminDir.ReadWriteString ( creator );

						(* remove schedule entries *)
						adminDir.ReadWriteToEOE;
						нцПока ~adminDir.IsEOF () делай
							adminDir.ReadWriteLines ( 1 );
							adminDir.ReadWriteLine ( tmp2 );
							если tmp2 = "dir" то
								adminDir.ReadWriteEOE;
							иначе
								adminDir.ReadWriteToEOE;
							всё;
						кц;
						adminDir.WriteUpdate;

						SVNAdmin.WriteWCPROPS ( tmp, "", ver );
					всё;
				всё;
			всё;
		кц;
	кон ParseMergeContent;

	проц UpdateHandler* ( конст path : массив из симв8; entry : SVNAdmin.EntryEntity; data : динамическиТипизированныйУкль ) : булево;
	перем
		svn : OdSvn.OdSvn;
	нач
		(* TODO:
			- search for files/directories which are on a separate revision
			- search for missing resources..and restore them:
				- dirs: neuer request!
				- files: restore von base copy
		*)
(*		KernelLog.String ( "path: " );
		KernelLog.String ( path );
		KernelLog.String ( "//" );
		KernelLog.String ( entry.Name );
		KernelLog.Ln;*)

		svn := data ( OdSvn.OdSvn );

		если svn.traverseDummy то
			svn.traverseDummy := ложь;
			svn.repositoryPathLength := Strings.Length ( entry.RepositoryRoot ) - Strings.IndexOfByte ( '/', 7, entry.RepositoryRoot );
		всё;

		возврат истина;
	кон UpdateHandler;

	проц CommitHandler* ( конст path : массив из симв8; entry : SVNAdmin.EntryEntity; data : динамическиТипизированныйУкль ) : булево;
	перем
		str : Strings.String;
		err, wrk, tmp2 : массив 256 из симв8;
		props: WebHTTP.AdditionalField;
		svn : OdSvn.OdSvn;
		res : целМЗ;
		resHeader: WebHTTP.ResponseHeader;
		m : SVNOutput.Message;
	нач
		svn := data ( OdSvn.OdSvn );

		нов ( str, 256 );
		str := Strings.Substring2 ( Strings.Length ( entry.RepositoryRoot ), entry.Url );
		если str^[0] = Files.PathDelimiter то str := Strings.Substring2 ( 1, str^ ) всё;

		если ~svn.removeDir то
			svn.removeDir := истина;
			Strings.Truncate ( svn.wrk, Strings.Length(svn.wrk) - Strings.Length(str^) );
		всё;

		Strings.Concat ( svn.wrk, str^, tmp2 );
		SVNUtil.UrlEncode ( tmp2, wrk );

		если entry.NodeKind = "file" то
			Files.JoinPath ( path, entry.Name, tmp2 );

			если entry.Schedule = "add" то (* DONE *)
				svn.Propfind ( wrk, "D:version-controlled-configuration.D2:repository-uuid", props, err );
				если svn.pfStatus = 404 то (* 404 = not found *)
					svn.context.out.пСтроку8 ( "Adding " ); svn.context.out.пСтроку8 ( tmp2 ); svn.context.out.пВК_ПС;

					Put ( wrk, tmp2, svn, res );
					ExpectedResult ( 201, svn, wrk, tmp2, "add file" ); (* 201 = created *)
				иначе
					svn.context.out.пСтроку8 ( " ERROR: " ); svn.context.out.пСтроку8 ( wrk ); svn.context.out.пСтроку8 ( " is already on the server!" ); svn.context.out.пВК_ПС;
					svn.countChanges := 0; (* abort commit *)
					возврат ложь;
				всё;
			аесли entry.Schedule = "delete" то
				(* TODO: remove entry in entries file and all-wcprops file: there is no report packet !!!  *)
				(*IF ~entry.GlobalRemoval THEN (* file will be deleted anyway if the top directory is removed *)
					svn.context.out.String ( "Deleting " ); svn.context.out.String ( tmp2 ); svn.context.out.Ln;

					Delete ( wrk, svn, res );
					ExpectedResult ( 204, svn, wrk, tmp2, "delete file" ); (* 204 = no content *)
				END;*)
			иначе (* DONE *)
				если ~SVNUtil.CheckChecksum ( tmp2, entry.Checksum ) то
					svn.context.out.пСтроку8 ( "Sending " );
					svn.context.out.пСтроку8 ( tmp2 );
					svn.context.out.пВК_ПС;

					svn.Checkout ( entry.VersionUrl, resHeader, err );
					если resHeader.statuscode >= 400 то
						нов ( m, svn.context );
						если resHeader.statuscode = 409 то
							m.Print ( SVNOutput.ResCOMMITOUTOFDATE, tmp2 );
						иначе
							svn.context.out.пСтроку8 ( "HTTP error! Statuscode: " ); svn.context.out.пЦел64 ( resHeader.statuscode, 0 );
							svn.context.out.пВК_ПС;
							m.Print ( SVNOutput.ResCOMMITUNSPECIFIED, tmp2 );
						всё;

						svn.countChanges := 0; (* abort commit *)
						возврат ложь;
					иначе
						Put ( wrk, tmp2, svn, res );
						ExpectedResult ( 204, svn, wrk, tmp2, "add file" ); (* 204 = no content *)
					всё;
				всё;
			всё;
		иначе
			если entry.Schedule = "add" то (* DONE *)
				svn.context.out.пСтроку8 ( "Adding " ); svn.context.out.пСтроку8 ( path ); svn.context.out.пВК_ПС;

				Mkcol ( wrk, svn, res );
				ExpectedResult ( 201, svn, wrk, tmp2, "add directory" );
			аесли entry.Schedule = "delete" то
				(* TODO: remove entry in entries file *)
				(*svn.context.out.String ( "Deleting " ); svn.context.out.String ( tmp2 ); svn.context.out.Ln;

				Delete ( wrk, svn, res );
				ExpectedResult ( 204, svn, wrk, tmp2, "delete file" ); (* 204 = no content *)
				*)
			иначе
				(* do nothing *)
			всё;
		всё;

		возврат истина;
	кон CommitHandler;

	проц ExpectedResult ( status : цел32; svn : OdSvn.OdSvn; конст wrk, lcl, message : массив из симв8 );
	нач
		если svn.pfStatus # status то
			svn.context.out.пСтроку8 ( "ERROR: failed to " );
			svn.context.out.пСтроку8 ( message );
			svn.context.out.пСтроку8 ( ": " );
			svn.context.out.пСтроку8 ( lcl );
			svn.context.out.пВК_ПС;
			svn.context.out.пСтроку8 ( "url: " );
			svn.context.out.пСтроку8 ( wrk );
			svn.context.out.пВК_ПС;
		иначе
			увел ( svn.countChanges );
		всё;
	кон ExpectedResult;

	проц Mkcol* ( конст url : массив из симв8; svn : OdSvn.OdSvn; перем res : целМЗ );
	перем
		resHeader: WebHTTP.ResponseHeader;
		out : Потоки.Чтец;
	нач
		svn.client.Mkcol ( url, resHeader, out, res );
		svn.pfStatus := resHeader.statuscode;
	кон Mkcol;

	проц Delete* ( конст url : массив из симв8; svn : OdSvn.OdSvn; перем res : целМЗ );
	перем
		resHeader: WebHTTP.ResponseHeader;
		out : Потоки.Чтец;
	нач
		svn.client.Delete ( url, resHeader, out, res );
		svn.pfStatus := resHeader.statuscode;
	кон Delete;

	(* sends 'workName' to the svn.wrk address *)
	проц Put* ( конст workUrl, workName: массив из симв8; svn : OdSvn.OdSvn; перем res : целМЗ );
	перем
		f : Files.File;
		resHeader: WebHTTP.ResponseHeader;
		reqHeader: WebHTTP.RequestHeader;
		in : Files.Reader;
		out : Потоки.Чтец;
		lenStr: массив 10 из симв8;
		m : SVNOutput.Message;
	нач
		f := Files.Old(workName);
		если f # НУЛЬ то
			Files.OpenReader ( in, f, 0 );

			WebHTTP.SetAdditionalFieldValue ( reqHeader.additionalFields, "Content-Type", "application/octet-stream" );
			Strings.IntToStr ( f.Length(), lenStr );
			WebHTTP.SetAdditionalFieldValue ( reqHeader.additionalFields, "Content-Length", lenStr );

			svn.client.Put ( workUrl, reqHeader, resHeader, out, in, res );
			svn.pfStatus := resHeader.statuscode;
		иначе
			svn.context.out.пСтроку8 ( " ERROR: PUT " );

			нов ( m, svn.context );
			m.Print ( SVNOutput.ResFILENOTFOUND, workName );
		всё;
	кон Put;

	проц PrintError ( svn : OdSvn.OdSvn; перем res : целМЗ );
	нач
		svn.context.out.пСтроку8 ( "Server response: " ); svn.context.out.пЦел64 ( svn.pfStatus, 0 ); svn.context.out.пВК_ПС;
		если svn.pfStatus = 401 то
			res := SVNOutput.ResNOTAUTHORIZED;
		иначе
			res := SVNOutput.ResUNEXPECTEDSERVERRESPONSE;
		всё;
	кон PrintError;

кон SVNWebDAV.
