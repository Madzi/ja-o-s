модуль Strings; (** AUTHOR "be,tf, staubesv"; PURPOSE "String functions" *)

использует Потоки, Dates, RC := RealConversions;

конст
	Ok* = 0;

тип
	String* = укль на массив из симв8;

	StringArray* = укль на массив из String;

перем
	DateFormat*, TimeFormat*: массив 32 из симв8;	(** date and time format strings used by DateToStr/TimeToStr *)

тип

	(** The stringmaker creates an automatically growing character array from the input with an Streams writer *)
	Buffer* = окласс
	перем
		length : размерМЗ;
		data : String;
		w : Потоки.Писарь;

		проц &Init*(initialSize : размерМЗ);
		нач
			если initialSize < 16 то initialSize := 256 всё;
			нов(data, initialSize); length := 0;
		кон Init;

		проц Add*(конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем newSize, i : размерМЗ; n : String;
		нач
			если length + len + 1 >= длинаМассива(data) то
				newSize := матМаксимум(длинаМассива(data) * 2, length + len + 1);
				нов(n, newSize);
				нцДля i := 0 до length - 1 делай n[i] := data[i] кц;
				data := n;
			всё;
			нцПока len > 0 делай
				data[length] := buf[ofs];
				увел(ofs); увел(length); умень(len);
			кц;
			data[length] := 0X;
			res := Ok;
		кон Add;

		(** resets the length of the string to 0. The buffer is reused*)
		проц Clear*;
		нач
			data[0] := 0X;
			length := 0
		кон Clear;

		(** returns an Streams.Writer to the string *)
		проц GetWriter*() : Потоки.Писарь;
		нач
			если w = НУЛЬ то нов(w, сам.Add, 256) всё;
			возврат w
		кон GetWriter;

		(** returns the number of bytes written to the string. The Streams.Writer is updated *)
		проц GetLength*() : размерМЗ;
		нач
			если w # НУЛЬ то w.ПротолкниБуферВПоток всё;
			возврат length
		кон GetLength;

		(** returns the current string buffer. If the string maker is reused, the content of the string may or may not
			vary. The application might need to copy the returned string. The Streams.Writer is updated *)
		проц GetString*() : String;
		нач
			если w # НУЛЬ то w.ПротолкниБуферВПоток всё;
			возврат data
		кон GetString;

		проц Write*(out : Потоки.Писарь);
		нач
			если w # НУЛЬ то w.ПротолкниБуферВПоток всё;
			out.пБайты(data^, 0, length)
		кон Write;

	кон Buffer;

	(* Крайне неудачным решением является использование знака «+» для конкатенации строк. Пользуйтесь «+*» .
	Но и +* не особо хорошо, поскольку это оператор с приоритетом как у множения. *)
	операция "+"*( a: String; конст b: массив из симв8 ): String;
	перем str: String;
	нач
		нов( str, длинаМассива( a ) + Length( b ) );
		копируйСтрокуДо0( a^, str^ );  Append( str^, b );
		возврат str
	кон "+";

	операция "+*"*( a: String; конст b: массив из симв8 ): String;
	перем str: String;
	нач
		нов( str, длинаМассива( a ) + Length( b ) );
		копируйСтрокуДо0( a^, str^ );  Append( str^, b );
		возврат str кон "+*";

	(* Крайне неудачным решением является использование знака «+» для конкатенации строк. Пользуйтесь «+*» *)
	операция "+"*( a: String; b: симв8 ): String;
	перем str: String;
	нач
		нов( str, длинаМассива( a ) + 1 );
		копируйСтрокуДо0( a^, str^ );  AppendChar( str^, b );
		возврат str кон "+";

	операция "+*"*( a: String; b: симв8 ): String;
	перем str: String;
	нач
		нов( str, длинаМассива( a ) + 1 );
		копируйСтрокуДо0( a^, str^ );  AppendChar( str^, b );
		возврат str кон "+*";

	(* Крайне неудачным решением является использование знака «+» для конкатенации строк. Пользуйтесь «+*» *)
	операция "+"*( a, b: String ): String;
	перем str: String;
	нач
		нов( str, длинаМассива( a ) + длинаМассива( b ) );
		копируйСтрокуДо0( a^, str^ );  Append( str^, b^ );
		возврат str кон "+";

	операция "+*"*( a, b: String ): String;
	перем str: String;
	нач
		нов( str, длинаМассива( a ) + длинаМассива( b ) );
		копируйСтрокуДо0( a^, str^ );  Append( str^, b^ );
		возврат str кон "+*";

	операция "+"*( a: String; b: цел64 ): String;
	перем
		digits: массив 32 из симв8;
		str: String;
	нач
		IntToStr( b, digits );
		нов( str, длинаМассива( a ) + Length( digits ) );
		копируйСтрокуДо0( a^, str^ );  Append( str^, digits );
		возврат str
	кон "+";

	операция "+"*( a: String; b: вещ64 ): String;
	перем
		digits: массив 32 из симв8;
		str: String;
	нач
		RC.RealToString( b, 18, digits );
		нов( str, длинаМассива( a ) + Length( digits ) );
		копируйСтрокуДо0( a^, str^ );  Append( str^, digits );
		возврат str
	кон "+";

(** string handling *)

(** Возвращает длину строки в байтах, не считая завершающий символ с кодом 0. *)
проц Length* (конст string: массив из симв8): размерМЗ;
перем len: размерМЗ;
нач
	len := 0; нцПока (string[len] # 0X) делай увел(len) кц;
	возврат len
кон Length;

(** Find position of character, returns -1 if not found*)
проц Find* (конст string: массив из симв8; pos: размерМЗ; ch: симв8): размерМЗ;
нач
	нцПока (string[pos] # 0X ) и (string[pos] # ch) делай увел(pos) кц;
	если string[pos] = 0X то pos := -1 всё;
	возврат pos
кон Find;

(** returns the number of occurences of ch within string *)
проц Count* (конст string: массив из симв8; ch: симв8): размерМЗ;
перем count, pos: размерМЗ;
нач
	count := 0; pos := Find (string, 0, ch);
	нцПока pos # -1 делай увел (count); pos := Find (string, pos + 1, ch) кц;
	возврат count
кон Count;

(** truncates string to length *)
проц Truncate* (перем string: массив из симв8; length: размерМЗ);
нач
	если длинаМассива(string) > length то string[length] := 0X всё;
кон Truncate;

(**
 * Returns the position of the first occurrence of pattern in the string or -1 if no occurrence is found.
 * Rabin-Karp algorithm, adopted from Sedgewick.
 *)
проц Pos*(конст pattern, string: массив из симв8): размерМЗ;
конст
	q = 8204957;	(* prime number, {(d+1) * q <= MAX(SIGNED32)} *)
	d = 256;			(* number of different characters *)
перем h1, h2, dM: цел32; i, j, m, n: размерМЗ; found : булево;
нач
	m := Length(pattern); n := Length(string);
	если (m > n) то возврат -1 всё;

	dM := 1; нцДля i := 0 до m-2 делай dM := (d*dM) остОтДеленияНа q кц;
	h1 := 0; нцДля i := 0 до m-1 делай h1 := (h1*d + кодСимв8(pattern[i])) остОтДеленияНа q кц;
	h2 := 0; нцДля i := 0 до m-1 делай h2 := (h2*d + кодСимв8(string[i])) остОтДеленияНа q кц;
	i := 0; found := ложь;

	если (h1 = h2) то (* verify *)
		j := 0; found := истина;
		нцПока (j < m) делай
			если (string[j] # pattern[j]) то found := ложь; j := m; всё; (* hash values are equal, but strings are not *)
			увел(j);
		кц;
	всё;

	нцПока ~found и (i < n-m) делай
		h2 := (h2 + d*q - кодСимв8(string[i])*dM) остОтДеленияНа q;
		h2 := (h2*d + кодСимв8(string[i+m])) остОтДеленияНа q;
		увел(i);

		если (h1 = h2) то (* verify *)
			j := 0; found := истина;
			нцПока (j < m) делай
				если (string[i + j] # pattern[j]) то found := ложь; j := m; всё; (* hash values are equal, but strings are not *)
				увел(j);
			кц
		всё;
	кц;

	если found то
		возврат i;
	иначе
		возврат -1
	всё
кон Pos;

(** More generic version of Pos. Basically the same search algorithm, but can also perform case-insensitive searching and/or
 * backwards directed searching.
 * Returns the position of the first character of the first occurence of 'pattern' in 'text'  in search direction or -1 if pattern not found *)
проц GenericPos*(конст pattern: массив из симв8; from : размерМЗ; конст string: массив из симв8; ignoreCase, backwards : булево): размерМЗ;
конст
	q = 8204957;	(* prime number, {(d+1) * q <= MAX(SIGNED32)} *)
	d = 256;			(* number of different characters *)
перем ch, chp : симв8; h1, h2, dM: цел32; i, j, patternLength, stringLength: размерМЗ; found : булево;
нач
	patternLength := Length(pattern); stringLength := Length(string);

	(* check whether the search pattern can be contained in the text regarding the search direction *)
	если backwards то
		если (patternLength > from + 1) то возврат -1; всё;
	иначе
		если (from + patternLength > stringLength) то возврат -1; всё;
	всё;

	dM := 1; нцДля i := 0 до patternLength-2 делай dM := (d*dM) остОтДеленияНа q кц;

	(* calculate hash value for search pattern string *)
	h1 := 0; нцДля i := 0 до patternLength-1 делай
		если backwards то
			ch := pattern[patternLength-1-i];
		иначе
			ch := pattern[i];
		всё;
		если ignoreCase то UpperCaseChar(ch); всё;
		h1 := (h1*d + кодСимв8(ch)) остОтДеленияНа q;
	кц;

	(* calculate hash value for the first 'patternLength' characters of the text to be searched *)
	h2 := 0; нцДля i := 0 до patternLength-1 делай
		если backwards то
			ch := string[from - i];
		иначе
			ch := string[from + i];
		всё;
		если ignoreCase то UpperCaseChar(ch); всё;
		h2 := (h2*d + кодСимв8(ch)) остОтДеленияНа q;
	кц;

	i := from; found := ложь;

	если (h1 = h2) то (* Hash values match, compare strings *)
		j := 0; found := истина;
		нцПока (j < patternLength) делай
			ch := string[from + j];
			chp := pattern[j];
			если ignoreCase то UpperCaseChar(ch); UpperCaseChar(chp); всё;
			если (ch # chp) то found := ложь; j := patternLength; всё; (* hash values are equal, but strings are not *)
			увел(j);
		кц;
	всё;

	нц
		(* check wether we're finished *)
		если found то прервиЦикл; всё;
		если backwards то
			если (i < patternLength) то прервиЦикл; всё;
		иначе
			если (i >= stringLength-patternLength) то прервиЦикл; всё;
		всё;

		(* remove last character from hash value *)
		ch := string[i];
		если ignoreCase то UpperCaseChar(ch); всё;
		h2 := (h2 + d*q - кодСимв8(ch)*dM) остОтДеленияНа q;

		(* add next character to hash value *)
		если backwards то
			ch := string[i-patternLength];
		иначе
			ch := string[i+patternLength];
		всё;
		если ignoreCase то UpperCaseChar(ch); всё;
		h2 := (h2*d + кодСимв8(ch)) остОтДеленияНа q;

		если backwards то умень(i); иначе увел(i); всё;

		если (h1 = h2) то (* verify *)
			j := 0; found := истина;
			нцПока (j < patternLength) делай
				если backwards то
					ch := string[i - patternLength + 1 + j];
				иначе
					ch := string[i + j];
				всё;
				chp := pattern[j];
				если ignoreCase то UpperCaseChar(ch); UpperCaseChar(chp); всё;
				если (ch # chp) то found := ложь; j := patternLength; всё; (* hash values are equal, but strings are not *)
				увел(j);
			кц
		всё;
	кц;

	если found то
		если backwards то возврат i - patternLength + 1;
		иначе возврат i;
		всё;
	иначе
		возврат -1;
	всё;
кон GenericPos;

(** Simple pattern matching with support for "*" and "?" wildcards  - returns TRUE if name matches mask. Patent pending ;-) *)
проц Match*(конст mask, name: массив из симв8): булево;
перем m,n, om, on: размерМЗ; f: булево;
нач
	m := 0; n := 0; om := -1;
	f := истина;
	нц
		если (mask[m] = "*") то
			om := m; увел(m);
			нцПока (name[n] # 0X) и (name[n] # mask[m]) делай увел(n) кц;
			on := n
		аесли (mask[m] = "?") то
			если (name[n] = 0X) то f := ложь; прервиЦикл всё;
			увел(m); увел(n)
		иначе
			если (mask[m] # name[n]) то
				если (om = -1) то f := ложь; прервиЦикл
				аесли (name[n] # 0X) то (* try the next position *)
					m := om; n := on + 1;
					если (name[n] = 0X) то f := ложь; прервиЦикл всё
				иначе
					f := ложь; прервиЦикл
				всё
			иначе увел(m); увел(n)
			всё
		всё;
		если (mask[m] = 0X) и ((name[n] = 0X) или (om=-1)) то прервиЦикл всё
	кц;
	возврат f и (name[n] = 0X)
кон Match;

(** copies src[soff ... soff + len - 1] to dst[doff ... doff + len - 1] *)
проц Move* (конст src: массив из симв8; soff, len: размерМЗ; перем dst: массив из симв8; doff: размерМЗ);
нач
	(* reverse copy direction in case src and dst denote the same string *)
	если soff < doff то
		увел (soff, len - 1); увел (doff, len - 1);
		нцПока len > 0 делай dst[doff] := src[soff]; умень (soff); умень (doff); умень (len) кц
	иначе
		нцПока len > 0 делай dst[doff] := src[soff]; увел (soff); увел (doff); умень (len) кц
	всё;
кон Move;

(** concatenates s1 and s2: s := s1 || s2 *)
проц Concat* (конст s1, s2: массив из симв8; перем s: массив из симв8);
перем len1, len2 : размерМЗ;
нач
	len1 := Length (s1); len2 := Length (s2);
	Move(s2, 0, len2, s, len1);
	Move (s1, 0, len1, s, 0);
	Truncate (s, len1 + len2);
кон Concat;

(** concatenates s1 and s2: s := s1 || s2. The resulting string is truncated to the length of s if necessary *)
проц ConcatX*(конст s1, s2 : массив из симв8; перем s : массив из симв8);
перем len1, len2 : размерМЗ;
нач
	len1 := Length (s1); len2 := Length (s2);
	если (len1 + 1 >= длинаМассива(s)) то
		копируйСтрокуДо0(s1, s);
	иначе
		если (len1 + len2 + 1 > длинаМассива(s)) то
			len2 := длинаМассива(s) - 1 - len1;
		всё;
		Move(s2, 0, len2, s, len1);
		Move (s1, 0, len1, s, 0);
		Truncate (s, len1 + len2);
	всё;
кон ConcatX;

(** appends appendix to s: s := s || appendix *)
проц Append* (перем s: массив из симв8; конст appendix: массив из симв8);
нач Concat (s, appendix, s)
кон Append;

(** appends appendix to s: s := s || appendix. The resulting string is truncated to the length of s if necessary *)
проц AppendX* (перем s: массив из симв8; конст appendix: массив из симв8);
нач ConcatX (s, appendix, s)
кон AppendX;

(** appends an integer number to a string *)
проц AppendInt*(перем s: массив из симв8; num: цел64);
перем number: массив 21 из симв8;
нач
	IntToStr(num,number); Append(s,number);
кон AppendInt;

(** appends a character to a string s := s || char *)
проц AppendChar*(перем s: массив из симв8; ch: симв8);
перем cs: массив 2 из симв8;
нач
	cs[0] := ch; cs[1] := 0X; Append(s,cs);
кон AppendChar;

(** copies src[index ... index + len-1] to dst *)
проц Copy* (конст src: массив из симв8; index, len: размерМЗ; перем dst: массив из симв8);
нач
	Move (src, index, len, dst, 0);
	Truncate (dst, len);
кон Copy;

(** deletes positions index ... index + count - 1 from 's' *)
проц Delete* (перем s: массив из симв8; index, count: размерМЗ);
перем len: размерМЗ;
нач
	len := Length (s);
	Move (s, index + count, len - index - count, s, index);
	Truncate (s, len - count);
кон Delete;

(** inserts 'src' at position 'index' into 'dst' *)
проц Insert* (конст src: массив из симв8; перем dst: массив из симв8; index: размерМЗ);
перем slen, dlen: размерМЗ;
нач
	slen := Length (src); dlen := Length (dst);
	Move (dst, index, dlen-index, dst, index+slen);
	Move (src, 0, slen, dst, index);
	Truncate (dst, slen + dlen);
кон Insert;

(** removes all occurrences of 'c' at the head of 'string' *)
проц TrimLeft* (перем string: массив из симв8; c: симв8);
перем len, index: размерМЗ;
нач
	len := Length (string); index := 0;
	нцПока (index # len) и (string[index] = c) делай увел (index) кц;
	Delete (string, 0, index);
кон TrimLeft;

(** removes all occurrences of 'c' at the end of 'string' *)
проц TrimRight* (перем string: массив из симв8; c: симв8);
перем len, index: размерМЗ;
нач
	len := Length (string); index := len;
	нцПока (index # 0) и (string[index - 1] = c) делай умень (index) кц;
	Delete (string, index, len - index);
кон TrimRight;

(** removes all occurrences of 'c' at both ends of 'string' *)
проц Trim* (перем string: массив из симв8; c: симв8);
нач
	TrimLeft(string, c);
	TrimRight(string, c)
кон Trim;

(**
 * Splits 'string' into multiple strings separated by 'separator'.
 * Result properties:
 *	separator = 0X:	LEN(StringArray) = 1
 *	separator # 0X:	LEN(StringArray) = 1 + <Number of occurences of 'ch' in 'string'>
 *	StringArray[i] # NIL (0 <= i <= LEN(StringArray)-1)
 *)
проц Split*(конст string : массив из симв8; separator : симв8) : StringArray;
перем count, index, pos, next: размерМЗ; result : StringArray;
нач
	count := Count (string, separator);
	нов (result, count + 1); pos := 0;
	нцДля index := 0 до count делай
		next := Find (string, pos, separator);
		если next = -1 то next := Length (string) всё;
		нов (result[index], next - pos + 1);
		Copy (string, pos, next - pos, result[index]^);
		pos := next + 1;
	кц;
	возврат result;
кон Split;

проц Join*(конст strings : StringArray; startIndex, endIndex : размерМЗ; separator : симв8) : String;
перем string : String; length, pos, i : размерМЗ;
нач
	утв((strings # НУЛЬ) и (длинаМассива(strings) >= 1));
	утв((0 <= startIndex) и (startIndex <= endIndex) и (endIndex < длинаМассива(strings)));
	length := 1; (* 0X termination *)
	если (separator # 0X) то length := length + (endIndex - startIndex); всё;
	нцДля i := startIndex до endIndex делай
		length := length + Length(strings[i]^);
	кц;
	pos := 0;
	нов(string, length);
	нцДля i := startIndex до endIndex делай
		length := Length(strings[i]^);
		Move(strings[i]^, 0, length, string^, pos);
		pos := pos + length;
		если (i < endIndex) и (separator # 0X) то string[pos] := separator; увел(pos); всё;
	кц;
	string^[длинаМассива(string)-1] := 0X;
	утв((string # НУЛЬ) и (длинаМассива(string) > 0) и (string^[длинаМассива(string)-1] = 0X));
	возврат string;
кон Join;

(** returns the corresponding lower-case letter for "A" <= ch <= "Z" *)
проц LOW*(ch: симв8): симв8;
нач
	если (ch >= "A") и (ch <= "Z") то возврат симв8ИзКода(кодСимв8(ch) - кодСимв8("A") + кодСимв8("a"))
	иначе возврат ch
	всё
кон LOW;

(** converts s to lower-case letters *)
проц LowerCase*(перем s: массив из симв8);
перем i: размерМЗ;
нач
	i := 0;
	нцПока (s[i] # 0X) делай
		s[i] := LOW(s[i]);
		увел(i)
	кц
кон LowerCase;

(** returns the corresponding upper-case letter for "a" <= ch <= "z" *)
проц UP*(ch : симв8) : симв8;
нач
	если ("a" <= ch) и (ch <= "z") то ch := ASCII_вЗаглавную(ch); всё;
	возврат ch;
кон UP;

проц UpperCaseChar*(перем ch : симв8);
нач
	если ("a" <= ch) и (ch <= "z") то ch := ASCII_вЗаглавную(ch); всё;
кон UpperCaseChar;

(** converts s to upper-case letters *)
проц UpperCase*(перем s: массив из симв8);
перем i: размерМЗ; c : симв8;
нач
	i := 0;
	нцПока (s[i] # 0X) делай
		c := s[i];
		если ('a' <= c) и (c <= 'z') то s[i] := ASCII_вЗаглавную(c) всё;
		увел(i)
	кц
кон UpperCase;

(* ASCII printable characters *)
проц IsPrintable*(ch:симв8):булево;
нач
	возврат (ch>=20X) и (ch<=7EX)
кон IsPrintable;

(** conversion functions *)

(** converts a boolean value to a string *)
проц BoolToStr*(b: булево; перем s: массив из симв8);
конст True = "True"; False = "False";
нач
	если b то копируйСтрокуДо0(True, s)
	иначе копируйСтрокуДо0(False, s)
	всё
кон BoolToStr;

(** converts a string to a boolean value: b := CAP(s[0]) = "T" *)
проц StrToBool*(конст s: массив из симв8; перем b: булево);
нач b := ASCII_вЗаглавную(s[0]) = "T"
кон StrToBool;

(** converts an integer value to a string *)
проц IntToStr*(x: цел64; перем s: массив из симв8);
перем i, j: размерМЗ; x0: цел64; digits: массив 21 из симв8;
нач
	если x < 0 то
		если x = матМинимум( цел64 ) то
			копируйСтрокуДо0("-9223372036854775808", s) ;
			возврат;
		иначе
			x0 := -x; s[0] := "-"; j := 1;
		всё;
	иначе
		x0 := x; j := 0;
	всё;

	i := 0;
	нцДо digits[i] := симв8ИзКода( x0 остОтДеленияНа 10 + 30H );  x0 := x0 DIV 10;  увел( i ) кцПри x0 = 0;
	нцДо умень( i );  s[j] := digits[i]; увел(j) кцПри i = 0;
	s[j] := 0X;
кон IntToStr;

(** converts a string to an integer. Leading whitespace is ignored *)
проц StrToInt*(конст str: массив из симв8; перем val: цел32);
перем i: размерМЗ; d: цел32; neg: булево;
нач
	i := 0; нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц;
	neg := ложь;
	если (str[i] = "+") то увел(i)
	аесли (str[i] = "-") то neg := истина; увел(i)
	всё;

	val := 0;
	нцПока (str[i] >= "0") и (str[i] <= "9") делай
		d := кодСимв8(str[i])-кодСимв8("0");
		если (val <= ((матМаксимум(цел32)-d) DIV 10)) то val := 10*val+d
		аесли neg и (val = 214748364) и (d = 8) и ((str[i+1] < "0") или (str[i+1] > "9")) то
			(* SIGNED32 range: -2147483648 ... 2147483647 _> need special handling for -2147483648 here *)
			val := матМинимум(цел32); neg := ложь
		иначе
			СТОП(99)
		всё;
		увел(i)
	кц;
	если neg то val := -val всё
кон StrToInt;

(** converts a string to a huge integer. Leading whitespace is ignored *)
проц StrToHInt*(конст str: массив из симв8; перем val: цел64);
перем i: размерМЗ; d: цел64; neg: булево;
нач
	i := 0; нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц;
	neg := ложь;
	если (str[i] = "+") то увел(i)
	аесли (str[i] = "-") то neg := истина; увел(i)
	всё;

	val := 0;
	нцПока (str[i] >= "0") и (str[i] <= "9") делай
		d := кодСимв8(str[i])-кодСимв8("0");
		если (val <= ((матМаксимум(цел64)-d) DIV 10)) то val := 10*val+d
		аесли neg и (val = 922337203685477580) и (d = 8) и ((str[i+1] < "0") или (str[i+1] > "9")) то
			(* SIGNED64 range: -9223372036854775808 ... 9223372036854775807 _> need special handling for -9223372036854775808 here *)
			val := матМинимум(цел64); neg := ложь
		иначе
			СТОП(99)
		всё;
		увел(i)
	кц;
	если neg то val := -val всё
кон StrToHInt;

(** converts a string to a size. Leading whitespace is ignored *)
проц StrToSize*(конст str: массив из симв8; перем val: размерМЗ);
перем i: размерМЗ; d: размерМЗ; neg: булево;
нач
	i := 0; нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц;
	neg := ложь;
	если (str[i] = "+") то увел(i)
	аесли (str[i] = "-") то neg := истина; увел(i)
	всё;

	val := 0;
	нцПока (str[i] >= "0") и (str[i] <= "9") делай
		d := кодСимв8(str[i])-кодСимв8("0");
		если (val <= ((матМаксимум(размерМЗ)-d) DIV 10)) то val := 10*val+d
		аесли neg и (val = матМаксимум(размерМЗ) DIV 10) и (d = матМаксимум(размерМЗ) остОтДеленияНа 10) и ((str[i+1] < "0") или (str[i+1] > "9")) то
			(* SIZE range needs special handling *)
			val := матМинимум(размерМЗ); neg := ложь
		иначе
			СТОП(99)
		всё;
		увел(i)
	кц;
	если neg то val := -val всё
кон StrToSize;

(** Convert the substring beginning at position i in str into an integer. Leading whitespace is ignored.
	After the conversion i points to the first character after the integer. *)
проц StrToIntPos*(конст str: массив из симв8; перем val: цел32; перем i: размерМЗ);
перем noStr: массив 16 из симв8;
нач
	нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц;
	val := 0;
	если str[i] = "-" то
		noStr[val] := str[i]; увел(val); увел(i);
		нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц
	всё;
	нцПока (str[i] >= "0") и (str[i] <= "9") делай noStr[val] := str[i]; увел(val); увел(i) кц;
	noStr[val] := 0X;
	StrToInt(noStr, val)
кон StrToIntPos;

(** Convert the substring beginning at position i in str into a huge integer. Leading whitespace is ignored.
	After the conversion i points to the first character after the integer. *)
проц StrToHIntPos*(конст str: массив из симв8; перем val: цел64; перем i: размерМЗ);
перем noStr: массив 32 из симв8;
нач
	нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц;
	val := 0;
	если str[i] = "-" то
		noStr[val] := str[i]; увел(val); увел(i);
		нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц
	всё;
	нцПока (str[i] >= "0") и (str[i] <= "9") делай noStr[val] := str[i]; увел(val); увел(i) кц;
	noStr[val] := 0X;
	StrToHInt(noStr, val)
кон StrToHIntPos;

(** Convert the substring beginning at position i in str into a size. Leading whitespace is ignored.
	After the conversion i points to the first character after the integer. *)
проц StrToSizePos*(конст str: массив из симв8; перем val: размерМЗ; перем i: размерМЗ);
перем noStr: массив 32 из симв8;
нач
	нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц;
	val := 0;
	если str[i] = "-" то
		noStr[val] := str[i]; увел(val); увел(i);
		нцПока (str[i] # 0X) и (str[i] <= " ") делай увел(i) кц
	всё;
	нцПока (str[i] >= "0") и (str[i] <= "9") делай noStr[val] := str[i]; увел(val); увел(i) кц;
	noStr[val] := 0X;
	StrToSize(noStr, val)
кон StrToSizePos;

(** converts an integer value to a hex string *)
проц IntToHexStr*(h : цел64; width: целМЗ; перем s: массив из симв8);
перем c: симв8;
нач
	если (width <= 0) то width := 8 всё;

	умень(width); (* opov *)
	s[width+1] := 0X;
	нцПока (width >= 0) делай
		c := симв8ИзКода(h остОтДеленияНа 10H + кодСимв8("0"));
		если (c > "9") то c := симв8ИзКода((h остОтДеленияНа 10H - 10) + кодСимв8("A")) всё;
		s[width] := c; h := h DIV 10H; умень(width)
	кц
кон IntToHexStr;

(** converts a hex string to an integer. Leading whitespace is ignored. res=Ok indicates success, val=0 on failure. *)
проц HexStrToInt*(конст string: массив из симв8; перем val: цел32; перем res: целМЗ);
перем length, i : размерМЗ; ch: симв8; negative : булево;
нач
	length := длинаМассива(string); val := 0; res := -1;
	(* skip whitespace *)
	i := 0; нцПока (i < length) и (string[i] # 0X) и (string[i] <= " ") делай увел(i); кц;
	если (i < length) то
		если (string[i] = "+") или (string[i] = "-") то
			negative := (string[i] = "-"); увел(i);
		иначе
			negative := ложь;
		всё;
		нц
			если (i >= length) или (string[i] = 0X) то прервиЦикл; всё;
			ch := string[i];
			если (ch >= "0") и (ch <= "9") то val := 16 * val + кодСимв8(ch) - кодСимв8("0");
			аесли (ASCII_вЗаглавную(ch) >= "A") и (ASCII_вЗаглавную(ch) <= "F") то val := 16 * val + кодСимв8(ASCII_вЗаглавную(ch)) - кодСимв8("A") + 10;
			иначе прервиЦикл;
			всё;
			увел(i);
		кц;
		если (i < length) и (string[i] = "H") то увел(i); всё; (* skip optional "H" *)
		если (i < length) и (string[i] = 0X) то
			если negative то val := -val всё;
			res := Ok;
		всё;
	всё;
кон HexStrToInt;

(** converts a hex string to a size. Leading whitespace is ignored. res=Ok indicates success, val=0 on failure. *)
проц HexStrToSize*(конст string: массив из симв8; перем val: размерМЗ; перем res: целМЗ);
перем length, i : размерМЗ; ch: симв8;
нач
	length := длинаМассива(string); val := 0; res := -1;
	(* skip whitespace *)
	i := 0; нцПока (i < length) и (string[i] # 0X) и (string[i] <= " ") делай увел(i); кц;
	если (i < length) то
		нц
			если (i >= length) или (string[i] = 0X) то прервиЦикл; всё;
			ch := string[i];
			если (ch >= "0") и (ch <= "9") то val := 16 * val + кодСимв8(ch) - кодСимв8("0");
			аесли (ASCII_вЗаглавную(ch) >= "A") и (ASCII_вЗаглавную(ch) <= "F") то val := 16 * val + кодСимв8(ASCII_вЗаглавную(ch)) - кодСимв8("A") + 10;
			иначе прервиЦикл;
			всё;
			увел(i);
		кц;
		если (i < length) и (string[i] = "H") то увел(i); всё; (* skip optional "H" *)
		если (i < length) и (string[i] = 0X) то
			res := Ok;
		всё;
	всё;
кон HexStrToSize;

(** converts a real value to a string *)
проц FloatToStr*(x: вещ64; n, f, D: цел32; перем str: массив из симв8);
нач
	RC.RealToStringFix( x, n, f, D, str )
кон FloatToStr;

проц AddressToStr*(adr : адресВПамяти; перем str : массив из симв8);
нач
	IntToHexStr(adr, 2*размер16_от(адресВПамяти), str);
кон AddressToStr;

(** converts a string to a real value *)
проц StrToFloat*(конст s: массив из симв8; перем r: вещ64);
нач
	RC.StringToReal(s,r);
кон StrToFloat;

(** converts a set to a string *)
проц SetToStr*(set: мнвоНаБитахМЗ; перем s: массив из симв8);
перем i, j, k: цел16; noFirst: булево;
нач
	s[0] := "{"; i := 0; k := 1; noFirst := ложь;
	нцПока i <= матМаксимум(мнвоНаБитахМЗ) делай
		если i в set то
			если noFirst то s[k] := ","; увел(k) иначе noFirst := истина всё;
			если i >= 10 то s[k] := симв8ИзКода(i DIV 10 + 30H); увел(k) всё;
			s[k] := симв8ИзКода(i остОтДеленияНа 10 + 30H); увел(k);
			j := i; увел(i);
			нцПока (i <= матМаксимум(мнвоНаБитахМЗ)) и (i в set) делай увел(i) кц;
			если i-2 > j то
				s[k] := "."; s[k+1] := "."; увел(k, 2); j := i - 1;
				если j >= 10 то s[k] := симв8ИзКода(j DIV 10 + 30H); увел(k) всё;
				s[k] := симв8ИзКода(j остОтДеленияНа 10 + 30H); увел(k)
			иначе i := j
			всё
		всё;
		увел(i)
	кц;
	s[k] := "}"; s[k+1] := 0X
кон SetToStr;

(** converts a string to a set *)
проц StrToSet*(конст str: массив из симв8; перем set: мнвоНаБитахМЗ);
перем i, d, d1: цел16; dot: булево;
нач
	set := {}; dot := ложь;
	i := 0;
	нцПока (str[i] # 0X) и (str[i] # "}") делай
		нцПока (str[i] # 0X) и ((str[i] < "0") или ("9" < str[i])) делай увел(i) кц;
		d := 0; нцПока ("0" <= str[i]) и (str[i] <= "9") делай d := d*10 + кодСимв8(str[i]) - 30H; увел(i) кц;
		если (str[i] = 0X) то возврат; всё;
		если d <= матМаксимум(мнвоНаБитахМЗ) то включиВоМнвоНаБитах(set, d) всё;
		если dot то
			нцПока (d1 <= матМаксимум(мнвоНаБитахМЗ)) и (d1 < d) делай включиВоМнвоНаБитах(set, d1); увел(d1) кц;
			dot := ложь
		всё;
		нцПока (str[i] = " ") делай увел(i) кц;
		если (str[i] = ".") то d1 := d + 1; dot := истина всё
	кц
кон StrToSet;

(** converts a time to a string, using the 'TimeFormat' format. C.f. FormatDateTime *)
проц TimeToStr*(time: Dates.DateTime; перем s: массив из симв8);
нач FormatDateTime(TimeFormat, time, s)
кон TimeToStr;

(** converts a string to a time *)
проц StrToTime*(конст str: массив из симв8; перем dt: Dates.DateTime);
перем i: размерМЗ;
нач
	i := 0;
	нцПока (str[i] # 0X) и ((str[i] < "0") или (str[i] > "9")) делай увел(i) кц;
	StrToIntPos(str, dt.hour, i);
	нцПока (str[i] # 0X) и ((str[i] < "0") или (str[i] > "9")) делай увел(i) кц;
	StrToIntPos(str, dt.minute, i);
	нцПока (str[i] # 0X) и ((str[i] < "0") или (str[i] > "9")) делай увел(i) кц;
	StrToIntPos(str, dt.second, i);
	утв(Dates.ValidDateTime(dt));
кон StrToTime;

(** converts a date to a string, using the 'DateFormat' format. C.f. FormatDateTime *)
проц DateToStr*(date: Dates.DateTime; перем s: массив из симв8);
нач FormatDateTime(DateFormat, date, s)
кон DateToStr;

(** Convert a string of the form 'day month year' into an date value. Leading whitespace is ignored. *)
проц StrToDate*(конст str: массив из симв8; перем dt: Dates.DateTime);
перем i: размерМЗ;
нач
	i := 0;
	нцПока (str[i] # 0X) и ((str[i] < "0") или (str[i] > "9")) делай увел(i) кц;
	StrToIntPos(str, dt.day, i);
	нцПока (str[i] # 0X) и ((str[i] < "0") или (str[i] > "9")) делай увел(i) кц;
	StrToIntPos(str, dt.month, i);
	нцПока (str[i] # 0X) и ((str[i] < "0") или (str[i] > "9")) делай увел(i) кц;
	StrToIntPos(str, dt.year, i);
	утв(Dates.ValidDateTime(dt));
кон StrToDate;

(** converts a TDateTime into a string.
	Format rules:
	yyyy	->	four-digit year, e.g. 2001
	yy		->	two-digit year, e.g. 01
	mmmm ->	clear-text month, e.g. May
	mmm  ->  clear-text month, abbreviated, e.g. Sep
	mm	->	two-digit month, e.g. 05
	m		->	month, e.g. 5
	dd		->	two-digit day, e.g. 02
	d		->	day, e.g. 2 or 15
	wwww	-> clear-text week-day, e.g. Monday
	www	->  clear-text week-day, e.g. Mon

	hh		->	two-digit hour, e.g. 08
	h		->	hour, e.g. 8
	nn		-> two-digit minute, e.g. 03
	n		-> minute, e.g. 3
	ss		-> two-digit second, e.g. 00
	s		-> second, e.g. 0
	any other characters will be copied 1:1 to the result string

	Examples:
	"yyyy.mm.dd hh:nn:ss"	-> "2002.01.01 17:08:00"
	"yyyyyy.m.ddd"				-> "002002.1.001"
	"wwww, mmmm d, yyyy"			-> "Tuesday, September 11, 2001"
*)
проц FormatDateTime*(конст format: массив из симв8; dt: Dates.DateTime; перем result: массив из симв8);
перем i,k,l,len,n: размерМЗ; m,y,w,dw: цел32;

	проц IntToStr(v: цел32; len: размерМЗ; перем s: массив из симв8; перем pos: размерМЗ);
	перем i: размерМЗ;
	нач
		нцДля i := 1 до len делай s[pos+len-i] := симв8ИзКода(кодСимв8("0") + v остОтДеленияНа 10); v := v DIV 10 кц;
		увел(pos, len)
	кон IntToStr;

нач
	k := 0;
	если Dates.ValidDateTime(dt) то
		i := 0;
		нцПока (format[i] # 0X) делай
			n := 1; нцПока (format[i+n] = format[i]) делай увел(n) кц;
			len := n;
			просей format[i] из
			|"w": Dates.WeekDate(dt, y, w, dw); умень(dw);
					если (len >= 4) то len := 10 всё;
					l := 0; нцПока (l < len) и (Dates.Days[dw,l] # 0X) делай result[k] := Dates.Days[dw,l]; увел(k); увел(l) кц;
			|"y": IntToStr(dt.year, n, result, k);
			|"m": если (n >= 3) то
						m := dt.month-1; утв((m>=0) и (m<12));
						если (len > 3) то len := 12 всё;
						l := 0; нцПока (l < len) и (Dates.Months[m,l] # 0X) делай result[k] := Dates.Months[m, l]; увел(k); увел(l) кц
					иначе
						если (len=1) и (dt.month > 9) то len := 2; всё;
						IntToStr(dt.month, len, result, k)
					всё;
			|"d": если (len=1) и (dt.day > 9) то len := 2 всё;
					IntToStr(dt.day, len, result, k);
			|"h": если (len=1) и (dt.hour > 9) то len := 2 всё;
					IntToStr(dt.hour, len, result, k);
			|"n": если (len=1) и (dt.minute > 9) то len := 2 всё;
					IntToStr(dt.minute, len, result, k);
			|"s": если (len=1) и (dt.second > 9) то len := 2 всё;
					IntToStr(dt.second, len, result, k);
			иначе result[k] := format[i]; увел(k); n := 1
			всё;
			увел(i, n)
		кц
	всё;
	result[k] := 0X
кон FormatDateTime;

проц ShowTimeDifference*(t1, t2 : Dates.DateTime; out : Потоки.Писарь);
перем days, hours, minutes, seconds : цел32; show : булево;
нач
	Dates.TimeDifference(t1, t2, days, hours, minutes, seconds);
	show := ложь;
	если (days > 0) то out.пЦел64(days, 0); out.пСтроку8("d "); show := истина; всё;
	если show или (hours > 0) то out.пЦел64(hours, 0); out.пСтроку8("h "); show := истина;  всё;
	если show или (minutes > 0) то out.пЦел64(minutes, 0); out.пСтроку8("m "); show := истина; всё;
	out.пЦел64(seconds, 0); out.пСтроку8("s");
кон ShowTimeDifference;

проц NewString*(конст str : массив из симв8) : String;
перем l : размерМЗ; s : String;
нач
	l := Length(str) + 1;
	нов(s, l);
	копируйСтрокуДо0(str, s^);
	возврат s
кон NewString;

(* Копирует str в s, если длины хватает. Перевыделяет s и копирует, если не хватает *)
проц SetAOC*(конст str: массив из симв8; перем s: String);
перем l: размерМЗ;
нач
	l := Length(str) + 1;
	если (s = НУЛЬ) или (длинаМассива(s^) < l) то
		нов(s,l)
	всё;
	копируйСтрокуДо0(str, s^);
кон SetAOC;

(* Gets extension of the given name, returns file (without extension) and ext *)
проц GetExtension* (конст name : массив из симв8; перем file, ext: массив из симв8);
перем len, index: размерМЗ;
нач
	len := Length (name); index := len;
	нцПока (index # 0) и (name[index- 1] # '.') делай умень (index) кц;
	если index = 0 то
		Copy (name, 0, len, file);
		Truncate (ext, 0);
	иначе
		Copy (name, 0, index - 1, file);
		Copy (name, index, len - index, ext);
	всё
кон GetExtension;

(* Returns a new string that is a concatenation of s1 and s2: s := s1 || s2 *)
проц ConcatToNew*(конст s1, s2 : массив из симв8) : String;
перем
	s : String;
нач
	нов(s, Length(s1) + Length(s2) + 1);
	Concat(s1, s2, s^);
	возврат s;
кон ConcatToNew;

(* Tests if string s ends with the specified suffix *)
проц EndsWith*(конст suffix, s : массив из симв8) : булево;
нач
	возврат StartsWith(suffix, Length(s)-Length(suffix), s);
кон EndsWith;

(* Tests if two strings are equal *)
(* This procedure makes sense, because "proc(..)^ = proc(..)^" is not supported by the compiler! *)
проц Equal*(s1, s2 : String) : булево;
нач
	утв(s1 # НУЛЬ);
	утв(s2 # НУЛЬ);
	возврат s1^ = s2^;
кон Equal;

(** Returns TRUE if the 0X-terminated string contains the character 'ch', FALSE otherwise. *)
проц ContainsChar*(конст string : массив из симв8; ch : симв8; ignoreCase : булево) : булево;
нач
	если ignoreCase то
		возврат (Find (string, 0, LOW (ch)) # -1) и (Find (string, 0, UP (ch)) # -1)
	иначе
		возврат Find (string, 0, ch) # -1
	всё
кон ContainsChar;

(* Returns the index within string s of the first occurrence of the specified character *)
проц IndexOfByte2*(ch : симв8; конст s : массив из симв8) : размерМЗ;
нач
	возврат IndexOfByte(ch, 0, s);
кон IndexOfByte2;

(* Returns the index within string s of the first occurrence of the specified character, starting the search at the specified index *)
проц IndexOfByte*(ch : симв8; fromIndex : размерМЗ; конст s : массив из симв8) : размерМЗ;
перем
	lenString, i : размерМЗ;
нач
	lenString := Length(s);
	если fromIndex < 0 то
		fromIndex := 0;
	аесли fromIndex >= lenString то
		возврат -1;
	всё;
	нцДля i := fromIndex до lenString-1 делай
		если s[i] = ch то возврат i; всё;
	кц;
	возврат -1;
кон IndexOfByte;

(* Returns the index within string s of the last occurrence of the specified character *)
проц LastIndexOfByte2*(ch : симв8; конст s : массив из симв8) : размерМЗ;
нач
	возврат LastIndexOfByte(ch, Length(s)-1, s);
кон LastIndexOfByte2;

(* Returns the index within string s of the last occurrence of the specified character, searching backward starting at the specified index *)
проц LastIndexOfByte*(ch : симв8; fromIndex : размерМЗ; конст s : массив из симв8) : размерМЗ;
перем
	lenString, i : размерМЗ;
нач
	lenString := Length(s);
	если fromIndex >= lenString то
		fromIndex := lenString - 1;
	всё;
	нцДля i := fromIndex до 0 шаг -1 делай
		если s[i] = ch то возврат i; всё;
	кц;
	возврат -1;
кон LastIndexOfByte;

(* Returns a new string that is a copy of s in lower-case letters *)
проц LowerCaseInNew*(конст s : массив из симв8) : String;
перем
	n : String;
нач
	n := NewString(s);
	LowerCase(n^);
	возврат n;
кон LowerCaseInNew;

(* Tests if string s starts with the specified prefix *)
проц StartsWith2*(конст prefix, s : массив из симв8) : булево;
нач
	возврат StartsWith(prefix, 0, s);
кон StartsWith2;

(* Tests if string s starts with the specified prefix beginning a specified index *)
проц StartsWith*(конст prefix : массив из симв8; toffset : размерМЗ; конст s : массив из симв8) : булево;
перем
	lenString, lenPrefix, i : размерМЗ;
нач
	lenString := Length(s);
	lenPrefix := Length(prefix);
	если (toffset < 0) или (toffset > lenString - lenPrefix) то
		возврат ложь;
	всё;
	нцДля i := 0 до lenPrefix-1 делай
		если prefix[i] # s[toffset + i] то возврат ложь; всё;
	кц;
	возврат истина;
кон StartsWith;

(* Returns a new string that is a substring of string s *)
проц Substring2*(beginIndex : размерМЗ; конст s : массив из симв8) : String;
нач
	возврат Substring(beginIndex, Length(s), s);
кон Substring2;

(* Returns a new string that is a substring of string s *)
(* s[endIndex-1] is the last character of the new string *)
проц Substring*(beginIndex : размерМЗ; endIndex : размерМЗ; конст s : массив из симв8) : String;
перем
	lenString, lenNewString : размерМЗ;
	st : String;
нач
	утв(beginIndex >= 0);
	lenString := Length(s);
	утв(endIndex <= lenString);
	lenNewString := endIndex - beginIndex;
	утв(lenNewString >= 0);
	нов(st, lenNewString + 1);
	Copy(s, beginIndex, lenNewString, st^);
	возврат st;
кон Substring;

(* Omitts leading and trailing whitespace of string s *)
проц TrimWS*(перем s : массив из симв8);
перем
	len, start, i : размерМЗ;
нач
	len := Length(s);
	start := 0;
	нцПока (start < len) и (кодСимв8(s[start]) < 33) делай
		увел(start);
	кц;
	нцПока (start < len) и (кодСимв8(s[len-1]) < 33) делай
		умень(len);
	кц;
	если start > 0 то
		нцДля i := 0 до len - start - 1 делай
			s[i] := s[start + i];
		кц;
		s[i] := 0X;
	иначе
		s[len] := 0X;
	всё;
кон TrimWS;

(* Returns a new string that is a copy of s in upper-case letters *)
проц UpperCaseInNew*(конст s : массив из симв8) : String;
перем n : String;
нач
	n := NewString(s);
	UpperCase(n^);
	возврат n;
кон UpperCaseInNew;

нач
	DateFormat := "dd.mmm.yyyy";
	TimeFormat := "hh:nn:ss"
кон Strings.

System.Free Strings ~
