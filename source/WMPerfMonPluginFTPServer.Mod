модуль WMPerfMonPluginFTPServer; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor plugin for FTP server statistics"; *)
(**
 * History:
 *
 *	27.02.2007	First release (staubesv)
 *)

использует
	WMPerfMonPlugins, WebFTPServer, Modules;

конст
	ModuleName = "WMPerfMonPluginFTPServer";

тип

	FTPStats= окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "WebFTPServer"; p.description := "FTP server statistics";
			p.modulename := ModuleName;
			p.autoMin := ложь; p.autoMax := истина; p.minDigits := 7;

			нов(ds, 6);
			ds[0].name := "NclientsTotal";
			ds[1].name := "NclientsActive";
			ds[2].name := "NMebiBReceived"; ds[2].unit := "MiB";
			ds[3].name := "NMebiBSent"; ds[3].unit := "MiB";
			ds[4].name := "NbytesReceived"; ds[4].unit := "B";
			ds[5].name := "NbytesSent"; ds[5].unit := "B";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := WebFTPServer.NclientsTotal;
			dataset[1] := WebFTPServer.NclientsActive;
			dataset[2] := WebFTPServer.NMebiBReceived;
			dataset[3] := WebFTPServer.NMebiBSent;
			dataset[4] := WebFTPServer.NbytesReceived;
			dataset[5] := WebFTPServer.NbytesSent;
		кон UpdateDataset;

	кон FTPStats;

проц Install*; (** ~ *)
кон Install;

проц InitPlugin;
перем par : WMPerfMonPlugins.Parameter; stats : FTPStats;
нач
	нов(par); нов(stats, par);
кон InitPlugin;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	InitPlugin;
кон WMPerfMonPluginFTPServer.

WMPerfMonPluginFTPServer.Install ~   System.Free WMPerfMonPluginFTPServer ~
