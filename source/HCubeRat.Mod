(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль HCubeRat;   (** AUTHOR "fof"; PURPOSE "3D matrix object of type Real."; *)

использует НИЗКОУР, NbrInt, ArrayXdBytes, ArrayXd := ArrayXdRat, NbrRat, DataErrors, HCubeInt, DataIO;

конст
	(** The version number used when reading/writing a hypercube to file. *)
	VERSION* = 1;

тип
	Value* = ArrayXd.Value;  Index* = цел32;  Array* = ArrayXd.Array;  IntValue = ArrayXd.IntValue;  ArrayH* = ArrayXd.Array4;
	Map* = ArrayXd.Map;

	(** Type HCube is DataIO registered, instances of it can therefore be made persistent. *)

	HCube* = окласс (ArrayXd.Array)
	перем lenx-, leny-, lenz-, lent-: цел32;   (* lenx = nr.Columns, leny = nr.Rows *)
		ox-, oy-, oz-, ot-: цел32;
		Get-: проц {делегат} ( x, y, z, t: Index ): Value;

		(** override *)
		проц {перекрыта}AlikeX*( ): ArrayXdBytes.Array;
		перем copy: HCube;
		нач
			нов( copy, origin[0], len[0], origin[1], len[1], origin[2], len[2], origin[3], len[3] );  возврат copy;
		кон AlikeX;

		проц {перекрыта}NewRangeX*( neworigin, newlen: ArrayXdBytes.IndexArray;  copydata: булево );
		нач
			если длинаМассива( newlen ) # 4 то СТОП( 1001 ) всё;
			NewRangeX^( neworigin, newlen, copydata );
		кон NewRangeX;

		проц {перекрыта}ValidateCache*;
		нач
			ValidateCache^;
			если dim # 4 то СТОП( 100 ) всё;

			lenx := len[0];  leny := len[1];  lenz := len[2];  lent := len[3];  ox := origin[0];  oy := origin[1];  oz := origin[2];
			ot := origin[2];
		кон ValidateCache;

		проц {перекрыта}SetBoundaryCondition*( c: цел8 );   (* called by new, load and directly *)
		нач
			SetBoundaryCondition^( c );
			просей c из
			ArrayXd.StrictBoundaryC:
					Get := Get4;
			| ArrayXd.AbsorbingBoundaryC:
					Get := Get4BAbsorbing;
			| ArrayXd.PeriodicBoundaryC:
					Get := Get4BPeriodic;
			| ArrayXd.SymmetricOnBoundaryC:
					Get := Get4BSymmetricOnB
			| ArrayXd.SymmetricOffBoundaryC:
					Get := Get4BSymmetricOffB
			| ArrayXd.AntisymmetricOnBoundaryC:
					Get := Get4BAntisymmetricOnB
			| ArrayXd.AntisymmetricOffBoundaryC:
					Get := Get4BAntisymmetricOffB
			всё;
		кон SetBoundaryCondition;

	(** new *)
		проц & New*( ox, w, oy, h, oz, d, ot, td: цел32 );
		нач
			NewXdB( ArrayXdBytes.Array4( ox, oy, oz, ot ), ArrayXdBytes.Array4( w, h, d, td ) );
		кон New;

		проц Alike*( ): HCube;
		перем copy: ArrayXdBytes.Array;
		нач
			copy := AlikeX();  возврат copy( HCube );
		кон Alike;

		проц NewRange*( ox, w, oy, h, oz, d, ot, td: цел32;  copydata: булево );
		нач
			если (w # len[0]) или (h # len[1]) или (d # len[2]) или (td # len[3]) или (ox # origin[0]) или (oy # origin[1]) или
			    (oz # origin[2]) или (ot # origin[3]) то
				NewRangeX^( ArrayXdBytes.Array4( ox, oy, oz, ot ), ArrayXdBytes.Array4( w, h, d, td ), copydata )
			всё;
		кон NewRange;

		проц Copy*( ): HCube;
		перем res: ArrayXdBytes.Array;
		нач
			res := CopyX();  возврат res( HCube );
		кон Copy;

		проц Set*( x, y, z, t: Index;  v: Value );
		нач
			ArrayXdBytes.Set4( сам, x, y, z, t, v );
		кон Set;

	(** copy methods using the current boundary condition SELF.bc*)
		проц CopyToVec*( dest: Array;  dim: Index;  srcx, srcy, srcz, srct, destx, len: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 1) то СТОП( 1004 ) всё;
			slen := ArrayXdBytes.Index4( 1, 1, 1, 1 );  slen[dim] := len;
			CopyToArray( dest, ArrayXdBytes.Index4( srcx, srcy, srcz, srct ), slen, ArrayXdBytes.Index1( destx ),
								   ArrayXdBytes.Index1( len ) );
		кон CopyToVec;

		проц CopyToMtx*( dest: Array;  dimx, dimy: Index;  srcx, srcy, srcz, srct, destx, desty, lenx, leny: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 2) или (dimx >= dimy) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index4( 1, 1, 1, 1 );  slen[dimx] := lenx;  slen[dimy] := leny;
			CopyToArray( dest, ArrayXdBytes.Index4( srcx, srcy, srcz, srct ), slen, ArrayXdBytes.Index2( destx, desty ),
								   ArrayXdBytes.Index2( lenx, leny ) );
		кон CopyToMtx;

		проц CopyToCube*( dest: Array;  dimx, dimy, dimz: Index;
												   srcx, srcy, srcz, srct, destx, desty, destz, lenx, leny, lenz: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 3) или (dimx >= dimy) или (dimy >= dimz) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index4( 1, 1, 1, 1 );  slen[dimx] := lenx;  slen[dimy] := leny;  slen[dimz] := lenz;
			CopyToArray( dest, ArrayXdBytes.Index4( srcx, srcy, srcz, srct ), slen, ArrayXdBytes.Index3( destx, desty, destz ),
								   ArrayXdBytes.Index3( lenx, leny, lenz ) );
		кон CopyToCube;

		проц CopyToHCube*( dest: Array;  srcx, srcy, srcz, srct, destx, desty, destz, destt, lenx, leny, lenz, lent: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 4) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index4( lenx, leny, lenz, lent );
			CopyToArray( dest, ArrayXdBytes.Index4( srcx, srcy, srcz, srct ), slen, ArrayXdBytes.Index4( destx, desty, destz, destt ),
								   slen );
		кон CopyToHCube;

		проц CopyTo1dArray*( перем dest: массив из Value;  sx, sy, sz, st, slenx, sleny, slenz, slent: Index;  dpos, dlen: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 1, ArrayXdBytes.Index1( 0 ), ArrayXdBytes.Index1( длинаМассива( dest ) ), размер16_от( Value ),
																			  адресОт( dest[0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index4( sx, sy, sz, st ),
																  ArrayXdBytes.Index4( slenx, sleny, slenz, slent ), ArrayXdBytes.Index1( dpos ),
																  ArrayXdBytes.Index1( dlen ) );
		кон CopyTo1dArray;

		проц CopyTo2dArray*( перем dest: массив из массив из Value;  sx, sy, sz, st, slenx, sleny, slenz, slent: Index;
													   dposx, dposy, dlenx, dleny: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 2, ArrayXdBytes.Index2( 0, 0 ), ArrayXdBytes.Index2( длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ),
																			  размер16_от( Value ), адресОт( dest[0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index4( sx, sy, sz, st ),
																  ArrayXdBytes.Index4( slenx, sleny, slenz, slent ),
																  ArrayXdBytes.Index2( dposx, dposy ), ArrayXdBytes.Index2( dlenx, dleny ) );
		кон CopyTo2dArray;

		проц CopyTo3dArray*( перем dest: массив из массив из массив из Value;  sx, sy, sz, st, slenx, sleny, slenz, slent: Index;
													   dposx, dposy, dposz, dlenx, dleny, dlenz: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 3, ArrayXdBytes.Index3( 0, 0, 0 ),
																			  ArrayXdBytes.Index3( длинаМассива( dest, 2 ), длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ), размер16_от( Value ),
																			  адресОт( dest[0, 0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index4( sx, sy, sz, st ),
																  ArrayXdBytes.Index4( slenx, sleny, slenz, slent ),
																  ArrayXdBytes.Index3( dposx, dposy, dposz ),
																  ArrayXdBytes.Index3( dlenx, dleny, dlenz ) );
		кон CopyTo3dArray;

		проц CopyTo4dArray*( перем dest: массив из массив из массив из массив из Value;
													   sx, sy, sz, st, slenx, sleny, slenz, slent: Index;
													   dposx, dposy, dposz, dpost, dlenx, dleny, dlenz, dlent: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 4, ArrayXdBytes.Index4( 0, 0, 0, 0 ),
																			  ArrayXdBytes.Index4( длинаМассива( dest, 3 ), длинаМассива( dest, 2 ), длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ), размер16_от( Value ),
																			  адресОт( dest[0, 0, 0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index4( sx, sy, sz, st ),
																  ArrayXdBytes.Index4( slenx, sleny, slenz, slent ),
																  ArrayXdBytes.Index4( dposx, dposy, dposz, dpost ),
																  ArrayXdBytes.Index4( dlenx, dleny, dlenz, dlent ) );
		кон CopyTo4dArray;

	(** copy from without boundary conditions *)
		проц CopyFrom1dArray*( перем src: массив из Value;  spos, slen: Index;
														    dx, dy, dz, dt, dlenx, dleny, dlenz, dlent: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 1, ArrayXdBytes.Index1( 0 ), ArrayXdBytes.Index1( длинаМассива( src ) ), размер16_от( Value ),
																			  адресОт( src[0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index1( spos ), ArrayXdBytes.Index1( slen ),
																			   ArrayXdBytes.Index4( dx, dy, dz, dt ),
																			   ArrayXdBytes.Index4( dlenx, dleny, dlenz, dlent ) );
		кон CopyFrom1dArray;

		проц CopyFrom2dArray*( перем src: массив из массив из Value;  sposx, spoxy, slenx, sleny: Index;
														    dx, dy, dz, dt, dlenx, dleny, dlenz, dlent: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 2, ArrayXdBytes.Index2( 0, 0 ), ArrayXdBytes.Index2( длинаМассива( src, 1 ), длинаМассива( src, 0 ) ),
																			  размер16_от( Value ), адресОт( src[0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index2( sposx, spoxy ),
																			   ArrayXdBytes.Index2( slenx, sleny ), ArrayXdBytes.Index4( dx, dy, dz, dt ),
																			   ArrayXdBytes.Index4( dlenx, dleny, dlenz, dlent ) );
		кон CopyFrom2dArray;

		проц CopyFrom3dArray*( перем src: массив из массив из массив из Value;  sposx, spoxy, sposz, slenx, sleny, slenz: Index;
														    dx, dy, dz, dt, dlenx, dleny, dlenz, dlent: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 3, ArrayXdBytes.Index3( 0, 0, 0 ),
																			  ArrayXdBytes.Index3( длинаМассива( src, 2 ), длинаМассива( src, 1 ), длинаМассива( src, 0 ) ), размер16_от( Value ),
																			  адресОт( src[0, 0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index3( sposx, spoxy, sposz ),
																			   ArrayXdBytes.Index3( slenx, sleny, slenz ),
																			   ArrayXdBytes.Index4( dx, dy, dz, dt ),
																			   ArrayXdBytes.Index4( dlenx, dleny, dlenz, dlent ) );
		кон CopyFrom3dArray;

		проц CopyFrom4dArray*( перем src: массив из массив из массив из массив из Value;
														    sposx, spoxy, sposz, spost, slenx, sleny, slenz, slent: Index;
														    dx, dy, dz, dt, dlenx, dleny, dlenz, dlent: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 4, ArrayXdBytes.Index4( 0, 0, 0, 0 ),
																			  ArrayXdBytes.Index4( длинаМассива( src, 3 ), длинаМассива( src, 2 ), длинаМассива( src, 1 ), длинаМассива( src, 0 ) ), размер16_от( Value ),
																			  адресОт( src[0, 0, 0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index4( sposx, spoxy, sposz, spost ),
																			   ArrayXdBytes.Index4( slenx, sleny, slenz, slent ),
																			   ArrayXdBytes.Index4( dx, dy, dz, dt ),
																			   ArrayXdBytes.Index4( dlenx, dleny, dlenz, dlent ) );
		кон CopyFrom4dArray;

	кон HCube;

	операция ":="*( перем l: HCube;  перем r: массив из массив из массив из массив из Value );
	нач
		(* IF r = NIL THEN l := NIL;  RETURN END;  *)
		если l = НУЛЬ то нов( l, 0, длинаМассива( r, 3 ), 0, длинаМассива( r, 2 ), 0, длинаМассива( r, 1 ), 0, длинаМассива( r, 0 ) )
		иначе l.NewRange( 0, длинаМассива( r, 3 ), 0, длинаМассива( r, 2 ), 0, длинаМассива( r, 1 ), 0, длинаМассива( r, 0 ), ложь )
		всё;
		ArrayXdBytes.CopyMemoryToArrayPart( адресОт( r[0, 0, 0, 0] ), l, длинаМассива( r, 0 ) * длинаМассива( r, 1 ) * длинаМассива( r, 2 ) * длинаМассива( r, 3 ), НУЛЬ , НУЛЬ );
	кон ":=";

	операция ":="*( перем l: HCube;  r: HCubeInt.HCube );
	перем i, last: цел32;
	нач
		если r = НУЛЬ то l := НУЛЬ иначе
			если l = НУЛЬ то нов( l, r.origin[0], r.len[0], r.origin[1], r.len[1], r.origin[2], r.len[2], r.origin[3], r.len[3] );  всё;
			last := длинаМассива( r.data ) - 1;
			нцДля i := 0 до last делай l.data[i] := r.data[i];  кц;
		всё;
	кон ":=";

	операция ":="*( перем l: HCube;  r: Value );
	нач
		если l # НУЛЬ то ArrayXd.Fill( l, r );  всё;
	кон ":=";

	операция ":="*( перем l: HCube;  r: IntValue );
	перем r1: Value;
	нач
		r1 := r;  l := r1;
	кон ":=";

	операция "+"*( l, r: HCube ): HCube;
	перем res: HCube;
	нач
		res := l.Alike();  ArrayXd.Add( l, r, res );  возврат res;
	кон "+";

	операция "-"*( l, r: HCube ): HCube;
	перем res: HCube;
	нач
		res := l.Alike();  ArrayXd.Sub( l, r, res );  возврат res;
	кон "-";

	операция "+"*( l: HCube;  r: Value ): HCube;
	перем res: HCube;
	нач
		res := l.Alike();  ArrayXd.AddAV( l, r, res );  возврат res;
	кон "+";

	операция "+"*( l: HCube;  r: IntValue ): HCube;
	перем res: HCube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.AddAV( l, r1, res );  возврат res;
	кон "+";

	операция "+"*( l: Value;  r: HCube ): HCube;
	нач
		возврат r + l
	кон "+";

	операция "+"*( l: IntValue;  r: HCube ): HCube;
	нач
		возврат r + l
	кон "+";

	операция "-"*( l: HCube;  r: Value ): HCube;
	перем res: HCube;
	нач
		res := l.Alike();  ArrayXd.SubAV( l, r, res );  возврат res;
	кон "-";

	операция "-"*( l: HCube;  r: IntValue ): HCube;
	перем res: HCube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.SubAV( l, r1, res );  возврат res;
	кон "-";

	операция "-"*( l: Value;  r: HCube ): HCube;
	перем res: HCube;
	нач
		res := r.Alike();  ArrayXd.SubVA( l, r, res );  возврат res;
	кон "-";

	операция "-"*( l: IntValue;  r: HCube ): HCube;
	перем res: HCube;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.SubVA( l1, r, res );  возврат res;
	кон "-";

	операция "-"*( l: HCube ): HCube;
	нач
		возврат 0 - l;
	кон "-";

	операция "*"*( l: HCube;  r: Value ): HCube;
	перем res: HCube;
	нач
		res := l.Alike();  ArrayXd.MulAV( l, r, res );  возврат res;
	кон "*";

	операция "*"*( l: HCube;  r: IntValue ): HCube;
	перем res: HCube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.MulAV( l, r1, res );  возврат res;
	кон "*";

	операция "*"*( l: Value;  r: HCube ): HCube;
	нач
		возврат r * l;
	кон "*";

	операция "*"*( l: IntValue;  r: HCube ): HCube;
	нач
		возврат r * l;
	кон "*";

	операция "/"*( l: HCube;  r: Value ): HCube;
	перем res: HCube;
	нач
		res := l.Alike();  ArrayXd.DivAV( l, r, res );  возврат res;
	кон "/";

	операция "/"*( l: HCube;  r: IntValue ): HCube;
	перем res: HCube;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.DivAV( l, r1, res );  возврат res;
	кон "/";

	операция "/"*( l: Value;  r: HCube ): HCube;
	перем res: HCube;
	нач
		res := r.Alike();  ArrayXd.DivVA( l, r, res );  возврат res;
	кон "/";

	операция "/"*( l: IntValue;  r: HCube ): HCube;
	перем res: HCube;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.DivVA( l1, r, res );  возврат res;
	кон "/";

(* The procedures needed to register type HCube so that its instances can be made persistent. *)
	проц LoadHCube( R: DataIO.Reader;  перем obj: окласс );
	перем a: HCube;  version: цел8;  ver: NbrInt.Integer;
	нач
		R.чЦел8_мз( version );
		если version = -1 то
			obj := НУЛЬ  (* Version tag is -1 for NIL. *)
		аесли version = VERSION то нов( a, 0, 0, 0, 0, 0, 0, 0, 0 );  a.Read( R );  obj := a
		иначе ver := version;  DataErrors.IntError( ver, "Alien version number encountered." );  СТОП( 1000 )
		всё
	кон LoadHCube;

	проц StoreHCube( W: DataIO.Writer;  obj: окласс );
	перем a: HCube;
	нач
		если obj = НУЛЬ то W.пЦел8_мз( -1 ) иначе W.пЦел8_мз( VERSION );  a := obj( HCube );  a.Write( W ) всё
	кон StoreHCube;

	проц Register;
	перем a: HCube;
	нач
		нов( a, 0, 0, 0, 0, 0, 0, 0, 0 );  DataIO.PlugIn( a, LoadHCube, StoreHCube )
	кон Register;

(** Load and Store are procedures for external use that read/write an instance of HCube from/to a file. *)
	проц Load*( R: DataIO.Reader;  перем obj: HCube );
	перем ptr: окласс;
	нач
		R.Object( ptr );  obj := ptr( HCube )
	кон Load;

	проц Store*( W: DataIO.Writer;  obj: HCube );
	нач
		W.Object( obj )
	кон Store;

нач
	Register
кон HCubeRat.
