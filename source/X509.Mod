модуль X509;
(**
	X.509 Certificates and certificate management.
	See RFC5280 standard.
	Written by Timothée Martiel, 2014, with some fragments by P. Hunziker.
*)
использует Потоки, Dates, CryptoBigNumbers, CryptoRSA, ASN1 (*, ASN1Printer*), ЛогЯдра;
конст
	Trace = ложь;

	(** Verification flags *)
	AllowNotTrusted * = 0; (** Allow valid certificates that are not linked to the trusted root certificates *)
	AllowOutdated * = 1; (** Allow certificates that are not valid anymore *)
	Bypass * = 31; (** Bypass all checks *)

тип
	(** Represent an issuer or a subject. Only mandatory fields are kept. Fields are NIL if not present. *)
	Entity * = запись
		country-, organisation-, organisationUnit-, distName-, state-, commonName-: укль на массив из симв8;
		serial-: цел32;
	кон;

	(** Base certificate type. Certificates are specialized according to the encryption algorithm they provide. *)
	Certificate * = укль на запись
		(* tbsCertificate field of X.509 *)
		version-, serial-: цел32;
		algorithm-: укль на массив из симв8;
		validity-: запись notBefore-, notAfter-: Dates.DateTime кон;
		identifier-: укль на массив из симв8;
		publicKey-: CryptoRSA.Key;
		issuer-, subject-: Entity;

		(* signatureAlgorithm field of X.509 *)
		(* signatureValue of X.509 *)

		(* Verification chain *)
		parent -, next: Certificate;
	кон;

перем
	roots: Certificate; (** Trusted certificates *)

	(** Verifies validity of certificate in the given mode. *)
	проц Verify * (chain: Certificate; flags: мнвоНаБитахМЗ): булево;
	перем
		valid: булево;

		проц CheckDate (c: Certificate): булево;
		перем now: Dates.DateTime;
		нач
			если AllowOutdated в flags то возврат истина всё;
			now := Dates.Now();
			возврат (Dates.CompareDateTime(c.validity.notBefore, now) <= 0) и (Dates.CompareDateTime(now, c.validity.notAfter) <= 0)
		кон CheckDate;

		проц CheckSignature (c: Certificate): булево;
		кон CheckSignature;

	нач
		если Bypass в flags то возврат истина всё;

		(* For now check a single certificate *)
		valid := CheckDate(chain) и CheckSignature(chain);
		возврат valid
	кон Verify;

	(** Reads a certificate from a stream *)
	проц Read * (reader: Потоки.Чтец): Certificate;
	конст
		TimeLength = 15;
	перем
		c: Certificate;
		res: булево;
		root, segment, data: ASN1.Triplet;
		length: размерМЗ;
		r: Потоки.ЧтецИзСтроки;
		writer: Потоки.Писарь;
	нач
		нов(c);
		root := ASN1.Decode(reader, 0, length);

		(* tbsCertificate *)
		segment := root.child;

		(* version *)
		data := segment.child;
		(*Check(data.tag = ASN1.Integer);*)
		если (data.next.tag = ASN1.Integer) то
			(* Explicit version specified *)
			если data.tag = ASN1.Integer то
				c.version := data.ivalue + 1
			иначе
				c.version := кодСимв8(data.svalue[0])
			всё;
			data := data.next;
		иначе
			c.version := 1
		всё;

		(* serial. *)
		(*Check(data.tag = ASN1.Integer);*)
		c.serial := data.ivalue;

		(* algorithm OID *)
		data := data.next;
		(*Check(data.child.tag = ASN1.Oid);*)
		c.algorithm := data.child.svalue;
		(* Algorithm parameters are ignored *)

		(* issuer = CA *)
		data := data.next;
		FormatEntity(data, c.issuer);

		(* validity *)
		data := data.next;
		нов(r, TimeLength);
		r.ПримиСрезСтроки8ВСтрокуДляЧтения(data.child.svalue^, 0, длинаМассива(data.child.svalue^));
		res := ReadTime(r, c.validity.notBefore);
		(*Check(res);*)
		r.ПримиСрезСтроки8ВСтрокуДляЧтения(data.child.next.svalue^, 0, длинаМассива(data.child.next.svalue^));
		r.ПерейдиКМестуВПотоке(0);
		res := ReadTime(r, c.validity.notAfter);
		(*Check(res);*)

		(* subject *)
		data := data.next;
		FormatEntity(data, c.subject);

		(* subject public key algorithm *)
		(*data := data.next;*)

		(* subject public key *)
		data := data.next;
		FormatRsaPublicKey(data, c.publicKey);

		если c.version > 2 то
			(* extensions *)
			data := data.next
		всё;

		(* Signature algorithm -- from top level again *)
		segment := segment.next;

		(* Signature value *)
		segment := segment.next;

		если Trace то
			ЛогЯдра.пСтроку8("========== Read X509 Certificate ========="); ЛогЯдра.пВК_ПС;
			Потоки.НастройПисаря(writer, ЛогЯдра.ЗапишиВПоток);
			PrintCertificate(c, writer);
			ЛогЯдра.пСтроку8("========== End X509 Certificate ========="); ЛогЯдра.пВК_ПС
		всё;
		возврат c
	кон Read;

	(** Writes a certificate to a stream *)
	проц Write * (writer: Потоки.Писарь; cert: Certificate);
	кон Write;

	(** Add certificate to trusted roots *)
	проц AddToTrusted * (certificate: Certificate);
	нач {единолично}
		certificate.next := roots;
		roots := certificate
	кон AddToTrusted;

	(* ========== Helper procedures ========== *)
	проц IsDigit(char: симв8): булево;
	нач
		если (кодСимв8(char) < кодСимв8('0')) или (кодСимв8(char) > кодСимв8('9')) то
			возврат ложь
		иначе
			возврат истина
		всё
	кон IsDigit;

	(** Read an UTCTime type object from stream and fills the date with it. Returns success. *)
	проц ReadUTCTime(reader: Потоки.Чтец; перем date: Dates.DateTime): булево;
	перем
		char: симв8;
	нач
		(* Year: 2 digits, >= 50 -> 19..; < 50 -> 20.. *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.year := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.year, кодСимв8(char) - кодСимв8('0'));
		если date.year >= 50 то
			увел(date.year, 1900)
		иначе
			увел(date.year, 2000)
		всё;

		(* Month, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.month := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.month, кодСимв8(char) - кодСимв8('0'));

		(* Day, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.day := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.day, кодСимв8(char) - кодСимв8('0'));

		(* Hours, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.hour := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.hour, кодСимв8(char) - кодСимв8('0'));

		(* Minutes, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.minute := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.minute, кодСимв8(char) - кодСимв8('0'));

		(* Seconds, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.second := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.second, кодСимв8(char) - кодСимв8('0'));

		(* Check for trailing 'Z' *)
		char := reader.чИДайСимв8();
		если char # 'Z' то
			возврат ложь
		иначе
			возврат истина
		всё
	кон ReadUTCTime;

	(** Writes and UTCTime type object from date. *)
	проц WriteUTCTime(writer: Потоки.Писарь; конст date: Dates.DateTime);
	нач
	кон WriteUTCTime;

	(** Same as ReadUTCTime, but reads a GeneralizedTime instead. Returns success. *)
	проц ReadGeneralizedTime(reader: Потоки.Чтец; перем date: Dates.DateTime): булево;
	перем
		char: симв8;
	нач
		(* Year: 4 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.year := 1000 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.year, 100 * кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.year, 10 * кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.year, кодСимв8(char) - кодСимв8('0'));

		(* Month, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.month := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.month, кодСимв8(char) - кодСимв8('0'));

		(* Day, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.day := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.day, кодСимв8(char) - кодСимв8('0'));

		(* Hours, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.hour := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.hour, кодСимв8(char) - кодСимв8('0'));

		(* Minutes, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.minute := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.minute, кодСимв8(char) - кодСимв8('0'));

		(* Seconds, 2 digits *)
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		date.second := 10 * (кодСимв8(char) - кодСимв8('0'));
		char := reader.чИДайСимв8();
		если ~IsDigit(char) то возврат ложь всё;
		увел(date.second, кодСимв8(char) - кодСимв8('0'));

		(* Check for trailing 'Z' *)
		char := reader.чИДайСимв8();
		если char # 'Z' то
			возврат ложь
		иначе
			возврат истина
		всё
	кон ReadGeneralizedTime;

	(** Same as WriteUTCTime, but writes a GeneralizedTime instead. *)
	проц WriteGeneralizedTime(writer: Потоки.Писарь; конст date: Dates.DateTime);
	нач
		writer.пЦел64(date.year, 4);
		writer.пЦел64(date.month, 2);
		writer.пЦел64(date.day, 2);
		writer.пСимв8('Z')
	кон WriteGeneralizedTime;

	(** Selects the reading type: UTCTime or GeneralizedTime *)
	проц ReadTime(reader: Потоки.Чтец; перем date: Dates.DateTime): булево;
	перем
		pos: цел32;
		res: булево;
	нач
		res := ReadUTCTime(reader, date);
		если ~res то
			res := ReadGeneralizedTime(reader, date);
			возврат res
		всё;
		возврат истина
	кон ReadTime;

	(** Formats an entity record from an ASN1 triplet *)
	проц FormatEntity(triplet: ASN1.Triplet; перем entity: Entity);
	конст
		Country = 1; Org = 2; OrgUnit = 3; DistNameQual = 4; State = 5; CommonName = 6;
		Serial = 7;
	перем
		data, key, value: ASN1.Triplet;
		currentData: цел32;
	нач
		data := triplet.child;
		нцПока data # НУЛЬ делай
			key := data.child;
			нцПока key # НУЛЬ делай
				value := key.child;
				нцПока value # НУЛЬ делай
					(* If we already registered a data key, we store the value *)
					если currentData # 0 то
						просей currentData из
							 Country: entity.country := value.svalue
							|Org: entity.organisation := value.svalue
							|OrgUnit: entity.organisationUnit := value.svalue
							|DistNameQual: entity.distName := value.svalue
							|State: entity.state := value.svalue
							|CommonName: entity.commonName := value.svalue
						всё;
						currentData := 0
					аесли (value.svalue[0] = 55X) и (value.svalue[1] = 4X) и (value.svalue[2] = 6X) то
						currentData := Country
					аесли (value.svalue[0] = 55X) и (value.svalue[1] = 4X) и (value.svalue[2] = 8X) то
						currentData := State
					аесли (value.svalue[0] = 55X) и (value.svalue[1] = 4X) и (value.svalue[2] = 0AX) то
						currentData := Org
					аесли (value.svalue[0] = 55X) и (value.svalue[1] = 4X) и (value.svalue[2] = 0BX) то
						currentData := OrgUnit
					аесли (value.svalue[0] = 55X) и (value.svalue[1] = 4X) и (value.svalue[2] = 3X) то
						currentData := CommonName
					всё;
					value := value.next
				кц;
				key := key.next;
			кц;
			data := data.next
		кц;
	кон FormatEntity;

	(** Formats a RSA public key from a ASN1 triplet. *)
	проц FormatRsaPublicKey * (publicKey: ASN1.Triplet; перем key: CryptoRSA.Key);
	перем
		algo: укль на массив из симв8;
		sequence, mod,exp: ASN1.Triplet;
		sr: Потоки.ЧтецИзСтроки;
		len0:размерМЗ;
		modulus, exponent: CryptoBigNumbers.BigNumber;
	нач
		algo := publicKey.child.child.svalue;

		нов(sr, длинаМассива(publicKey.child.next.svalue^));
		sr.ПримиСрезСтроки8ВСтрокуДляЧтения(publicKey.child.next.svalue^, 0, длинаМассива(publicKey.child.next.svalue^));
		sequence := ASN1.Decode(sr, publicKey.child.next.level + 1, len0);

		mod := sequence.child;
		CryptoBigNumbers.AssignBin(modulus, mod.svalue^, 0, длинаМассива(mod.svalue^));

		exp:=mod.next;
		если exp.ivalue # 0 то
			CryptoBigNumbers.AssignInt(exponent, exp.ivalue)
		иначе
			CryptoBigNumbers.AssignBin(exponent, exp.svalue^, 0, длинаМассива(exp.svalue^))
		всё;
		key := CryptoRSA.PubKey(exponent, modulus);
	кон FormatRsaPublicKey;

	(** Print a date in human readable form *)
	проц PrintDate(date: Dates.DateTime; writer: Потоки.Писарь);
	нач
		(* date *)
		writer.пЦел64(date.year, 0); writer.пСтроку8('/'); writer.пЦел64(date.month, 0); writer.пСтроку8('/'); writer.пЦел64(date.day, 0);

		(* time *)
		writer.пСтроку8(" "); writer.пЦел64(date.hour, 0); writer.пСтроку8(":"); writer.пЦел64(date.minute, 0); writer.пСтроку8(":"); writer.пЦел64(date.second, 0)
	кон PrintDate;

	проц PrintEntity(entity: Entity; writer: Потоки.Писарь);
	нач
		если entity.country # НУЛЬ то
			writer.пСтроку8("	country:	"); writer.пБайты(entity.country^, 0, длинаМассива(entity.country)); writer.пВК_ПС
		всё;
		если entity.organisation # НУЛЬ то
			writer.пСтроку8("	organisation:	"); writer.пБайты(entity.organisation^, 0, длинаМассива(entity.organisation)); writer.пВК_ПС
		всё;
		если entity.organisationUnit # НУЛЬ то
			writer.пСтроку8("	organisation unit:	"); writer.пБайты(entity.organisationUnit^, 0, длинаМассива(entity.organisationUnit)); writer.пВК_ПС
		всё;
		если entity.distName # НУЛЬ то
			writer.пСтроку8("	distinguished name:	"); writer.пБайты(entity.distName^, 0, длинаМассива(entity.distName)); writer.пВК_ПС
		всё;
		если entity.state # НУЛЬ то
			writer.пСтроку8("	state:	"); writer.пБайты(entity.state^, 0, длинаМассива(entity.state)); writer.пВК_ПС
		всё;
		если entity.commonName # НУЛЬ то
			writer.пСтроку8("	common name:	"); writer.пБайты(entity.commonName^, 0, длинаМассива(entity.commonName)); writer.пВК_ПС
		всё;
	кон PrintEntity;

	(** Prints a certificate in human readable form *)
	проц PrintCertificate * (certificate: Certificate; writer: Потоки.Писарь);
	перем
		count: размерМЗ;
		mod: массив 16 * 4096 из симв8;
	нач
		writer.пСтроку8("== Begin of Certificate =="); writer.пВК_ПС;

		writer.пСтроку8("version:	"); writer.пЦел64(certificate.version, 0); writer.пВК_ПС;

		writer.пСтроку8("serial:	"); writer.пЦел64(certificate.serial, 0); writer.пВК_ПС;

		writer.пСтроку8("signature:"); writer.пВК_ПС;
		writer.пСтроку8("	algorithm:	");
		нцДля count := 0 до длинаМассива(certificate.algorithm) - 1 делай
			writer.п16ричное(кодСимв8(certificate.algorithm[count]), -2); writer.пСтроку8(" ");
		кц;
		writer.пВК_ПС;

		writer.пСтроку8("Issuer:"); writer.пВК_ПС;
		PrintEntity(certificate.issuer, writer);

		writer.пСтроку8("validity:"); writer.пВК_ПС;
		writer.пСтроку8("	not before:	"); PrintDate(certificate.validity.notBefore, writer); writer.пВК_ПС;
		writer.пСтроку8("	not after:	"); PrintDate(certificate.validity.notAfter, writer); writer.пВК_ПС;

		writer.пСтроку8("Subject:"); writer.пВК_ПС;
		PrintEntity(certificate.subject, writer);

		writer.пСтроку8("RSA public key:"); writer.пВК_ПС;
		writer.пСтроку8("	modulus:	"); CryptoBigNumbers.TextWrite(writer, certificate.publicKey.modulus); writer.пВК_ПС;
		(*writer.Update;
		CryptoBigNumbers.GetBinaryValue(certificate.publicKey.modulus, mod, 0);
		writer.String("	modulus:	"); FOR count := 0 TO LEN(mod) - 1 DO writer.Hex(ORD(mod[count]), -2); writer.String(' '); writer.Update END; writer.Ln;*)
		writer.пСтроку8("	exponent:	"); CryptoBigNumbers.TextWrite(writer, certificate.publicKey.exponent); writer.пВК_ПС;

		writer.пСтроку8("== End of Certificate =="); writer.пВК_ПС
	кон PrintCertificate;

	(* ========== Error reporting ========== *)
	проц Check(b: булево);
	нач
		утв(b)
	кон Check;
кон X509.
