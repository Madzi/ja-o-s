(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль ZipTool; (** AUTHOR "Stefan Walthert"; PURPOSE "Command line  front-end for Zip  **)

использует
	Потоки, Commands, Options, Files, Strings, Zip;

конст
	EXTRACT = 1;
	OPEN = 2;

	Tab = 9X;

(* Get the suffix of str. The suffix is started by the last sepchar in str. If sepchar does not occur in str, str is returned *)
проц GetSuffix(конст str : массив из симв8; перем suf : массив из симв8; sepchar: симв8);
перем i, j, len, sep: размерМЗ;
нач
	i := 0; sep := -1;
	нцПока str[i] # 0X делай
		если str[i] = sepchar то
			sep := i
		всё;
		увел(i)
	кц;
	j := 0;
	len := длинаМассива(suf) - 1; i := sep + 1;
	нцПока (j < len) и (str[i] # 0X) делай
		suf[j] := str[i]; увел(j); увел(i)
	кц;
	suf[j] := 0X
кон GetSuffix;

(* Append this to to *)
проц Append(перем to: массив из симв8; конст this: массив из симв8);
перем i, j, l: размерМЗ;
нач
	i := 0;
	нцПока to[i] # 0X делай
		увел(i)
	кц;
	l := длинаМассива(to)-1; j := 0;
	нцПока (i < l) и (this[j] # 0X) делай
		to[i] := this[j]; увел(i); увел(j)
	кц;
	to[i] := 0X
кон Append;

проц OpenArchive(конст archiveName : массив из симв8; errorLog : Потоки.Писарь) : Zip.Archive;
перем archive : Zip.Archive; res : целМЗ;
нач
	archive := Zip.OpenArchive(archiveName, res);
	если (res # Zip.Ok) то
		archive := НУЛЬ;
		errorLog.пСтроку8("Could not open archive '"); errorLog.пСтроку8(archiveName); errorLog.пСтроку8("': ");
		Zip.ShowError(res, errorLog); errorLog.пВК_ПС; errorLog.ПротолкниБуферВПоток;
	всё;
	возврат archive;
кон OpenArchive;


(** Writes the directory of an archive. **)
проц WriteDirectory*(out, error : Потоки.Писарь; конст archiveName: массив из симв8; details: булево; перем res: целМЗ);
перем
	archive: Zip.Archive;
	entry: Zip.Entry;
	ratio : цел32;
нач
	утв(out # НУЛЬ);
	archive := OpenArchive(archiveName, error);
	если (archive # НУЛЬ) то
		если details то
			out.пСтроку8("Name"); out.пСимв8(Tab);  out.пСимв8(Tab); out.пСтроку8("Date"); out.пСимв8(Tab); out.пСимв8(Tab);
			out.пСтроку8("Size"); out.пСимв8(Tab); out.пСтроку8("Ratio"); out.пСимв8(Tab);
			out.пСтроку8("Compressed"); out.пВК_ПС; out.пВК_ПС;
		всё;
		entry := Zip.FirstEntry(archive);
		нцПока (entry # НУЛЬ) делай
			out.пСтроку8(entry.name);
			если details то
				out.пСимв8(Tab); out.пСимв8(Tab); out.пДатуОберонаISO(entry.time, entry.date);
				out.пСимв8(Tab); out.пСимв8(Tab); out.пЦел64(entry.uncompSize, 0);
				ratio := округлиВниз(((1 - entry.compSize / entry.uncompSize) * 100) + 0.5);
				если ratio < 0 то ratio := 0 всё;	(* ratio can not be less than zero *)
				out.пСимв8(Tab); out.пЦел64(ratio, 0); out.пСтроку8("%");
				out.пСимв8(Tab); out.пЦел64(entry.compSize, 0);
			всё;
			out.пВК_ПС;
			entry := Zip.NextEntry(entry)
		кц;
		out.пВК_ПС;
		out.пЦел64(archive.nofEntries, 0);
		если (archive.nofEntries = 1) то out.пСтроку8(" entry");
		иначе out.пСтроку8(" entries");
		всё;
		out.пВК_ПС;
	всё
кон WriteDirectory;

(** Shows the content of the selected zip-archive in a new viewer.
	ZipTool.Directory [-d ] ZipFile ~
	Options:
	--details: If set, details of entries of selected zip-archive are shown  *)
проц Directory*(context : Commands.Context);
перем
	archiveName : Files.FileName;
	options : Options.Options;
	res: целМЗ;
нач
	нов(options);
	options.Add("d", "details", Options.Flag);

	если options.Parse(context.arg, context.error) то
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(archiveName);
		WriteDirectory(context.out, context.error, archiveName, options.GetFlag("details"), res);
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон Directory;

проц DoExtract(
	action: цел32;
	archive: Zip.Archive; entry: Zip.Entry; name: массив из симв8; перем tempfile: Files.File;
	path, overwrite, show: булево; out, error : Потоки.Писарь; перем res: целМЗ);
перем
	f, of: Files.File; r: Files.Rider;
	bakname, temp: массив 256 из симв8; res2: целМЗ;
	suf: массив 32 из симв8;
нач
	если action = EXTRACT то
		если ~path то
	(*		GetSuffix(name, name, ':'); *)
			GetSuffix(name, name, '/')
		всё;
		f := Files.New(name);
		если (f # НУЛЬ) то
			если (out # НУЛЬ) то
				out.пСтроку8("Extracting "); out.пСтроку8(entry.name);
				если (entry.name # name) то
					out.пСтроку8(" -> "); out.пСтроку8(name);
				всё;
				out.пСтроку8(" ... ");
			всё;
		иначе
			если (error # НУЛЬ) то error.пСтроку8("Could not create file "); error.пСтроку8(name); всё;
			res := -1;
			возврат;
		всё;
	иначе
		temp := "Temp.Zip.";
		GetSuffix(name,suf,'.');
		Append(temp,suf);
		f := Files.New(temp);
		если (f = НУЛЬ) то
			если (error # НУЛЬ) то error.пСтроку8("Could not create temporary file Temp.Zip"); всё;
			res := -1;
			возврат;
		всё;
	всё;
	tempfile := f;
	f.Set(r, 0);
	Zip.ExtractEntry(archive, entry, r, res);
	если (res = Zip.Ok) то
		если action = EXTRACT то
			of := Files.Old(name);
			если (of # НУЛЬ) то (* file exists on this volume or another volume in search path *)
				если ~overwrite то
					копируйСтрокуДо0(name, bakname); Append(bakname, ".Bak"); (* assume enough space for .Bak *)
					Files.Rename(name, bakname, res2);
					если (res2 = Files.Ok) то
						если (out # НУЛЬ) то out.пСтроку8(" done (backup in "); out.пСтроку8(bakname); out.пСтроку8(")."); всё;
					иначе (* assume old file was in another place in the search path *)
						of.GetName(bakname);
						если (out # НУЛЬ) то out.пСтроку8(" done (masks "); out.пСтроку8(bakname); out.пСтроку8(")."); всё;
					всё
				иначе
					если (out # НУЛЬ) то out.пСтроку8("done (overwritten)."); всё;
				всё;
			иначе
				если (out # НУЛЬ) то out.пСтроку8("done."); всё;
			всё;
			f.SetDate(entry.time, entry.date);
		всё;
		Files.Register(f);
		tempfile := f;
	иначе
		если (out # НУЛЬ) то Zip.ShowError(res, out); всё;
	всё;
	если (out # НУЛЬ) то out.пВК_ПС; out.ПротолкниБуферВПоток; всё;
кон DoExtract;

(** Extracts the entry ent from the zip-archive ent and stores as under the filename name. Some log-output is generated.
	If path is set, the file is stored in the directory according to the relative path in name.
	If overwrite is set, files with the same name are overwritten, otherwise they are renamed to name.Bak.
	Possible results: cf. Zip.ExtractEntry **)
проц ExtractFile*(arc: Zip.Archive; ent: Zip.Entry; конст name: массив из симв8; path, overwrite: булево; log, error : Потоки.Писарь; перем res: целМЗ);
перем temp: Files.File;
нач
	DoExtract(EXTRACT, arc, ent, name, temp, path, overwrite, ложь, log, error, res);
кон ExtractFile;

(** Extracts the entry ent from the zip-archive ent and stores as under the filename name. Some log-output is generated.
	If path is set, the file is stored in the directory according to the relative path in name.
	If overwrite is set, files with the same name are overwritten, otherwise they are renamed to name.Bak.
	Possible results: cf. Zip.ExtractEntry **)
проц OpenFile*(
	arc: Zip.Archive; ent: Zip.Entry; конст name: массив из симв8; перем tempfile: Files.File;
	path, overwrite, show: булево; log, error : Потоки.Писарь; перем res: целМЗ);
нач
	DoExtract(OPEN, arc, ent, name, tempfile, path, overwrite, show, log, error, res);
кон OpenFile;


(** Extracts the selected entries of the selected zip-Archive. The relative path in the file name of the entry
	is ignored (c.f. option \d).
	ZipTool.Extract [-d] [-o] ZipFile {Entry [=> NewName]}
	Options:
	--directory: If set, the file is stored in the directory according to the relative path in the file name of the entry
	--overwrite: If set, files with the same name are overwritten, otherwise they are renamed to filename.Bak
	--ignore: Continue in case of errors
	--prefix: Add prefix to extracted files **)
проц Extract*(context : Commands.Context);
перем
	archivename, entryname, filename, prefix: Files.FileName;
	options : Options.Options;
	archive: Zip.Archive; entry: Zip.Entry;
	path, overwrite, stopOnError: булево;
	nofExtracted, nofErrors: цел32; res: целМЗ;
нач
	нов(options);
	options.Add("d", "directory", Options.Flag);
	options.Add("o", "overwrite", Options.Flag);
	options.Add("i", "ignore", Options.Flag);
	options.Add("p", "prefix", Options.String);

	если options.Parse(context.arg, context.error) то

		path := options.GetFlag("directory");
		overwrite := options.GetFlag("overwrite");
		stopOnError := ~options.GetFlag("ignore");
		если ~options.GetString("prefix", prefix) то prefix := ""; всё;

		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(archivename);
		archive := OpenArchive(archivename, context.error);
		если (archive # НУЛЬ) то
			nofExtracted := 0; nofErrors := 0;
			нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(entryname) и ((nofErrors = 0) или ~stopOnError) делай
				entry := Zip.GetEntry(archive, entryname, res);
				если (res = Zip.Ok) то
					если (prefix # "") то
						копируйСтрокуДо0(prefix, filename); Append(filename, entry.name);
					иначе
						копируйСтрокуДо0(entry.name, filename);
					всё;
					ExtractFile(archive, entry, filename, path, overwrite, context.out, context.error, res);
					если (res = Zip.Ok) то
						увел(nofExtracted);
					иначе
						увел(nofErrors);
					всё;
				иначе
					увел(nofErrors);
					context.out.пСтроку8("Extracting "); context.out.пСтроку8(entryname);
					context.out.пСтроку8(" ... "); Zip.ShowError(res, context.out); context.out.пВК_ПС;
				всё;
			кц;
			context.out.пЦел64(nofExtracted, 0);
			если (nofExtracted = 1) то context.out.пСтроку8(" entry extracted"); иначе context.out.пСтроку8(" entries extracted"); всё;
			если (nofErrors > 0) то
				context.out.пСтроку8(" ("); context.out.пЦел64(nofErrors, 0);
				если (nofErrors = 1) то context.out.пСтроку8(" error)");
				иначе context.out.пСтроку8(" errors)");
				всё;
				context.result := Commands.CommandError;
			всё;
			context.out.пВК_ПС;
		всё;
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон Extract;

(** Extracts all entries of the selected zip-archives. The relative path in the file name of the entry
	is ignored (c.f. option \d).
	ZipTool.ExtractAll [-d] [-o] [-p=DstPrefix] [-sourcePath=SrcPrefix] [-s] {ZipFile} ~
	Options:
	--directory: If set, the file is stored in the directory according to the relative path in the file name of the entry
	--overwrite: If set, files with the same name are overwritten, otherwise they are renamed to filename.Bak
	--ignore: If set, continue extraction in case of an error, otherwise abort
	--prefix: If set, DstPrefix is prefixed to all file names of the entries in the zip-archives
	--sourcePath: If set, SrcPrefix is prefixed to all archive names *)
проц ExtractAll*(context : Commands.Context);
перем
	fullArchiveName, archiveName, filename,  prefix : Files.FileName;
	options : Options.Options;
	path, overwrite, stopOnError, silent: булево;
	archive: Zip.Archive;
	entry: Zip.Entry;
	nofExtracted, nofErrors: цел32; res: целМЗ;
нач
	нов(options);
	options.Add("d", "directory", Options.Flag);
	options.Add("o", "overwrite", Options.Flag);
	options.Add("i", "ignore", Options.Flag);
	options.Add("p", "prefix", Options.String);
	options.Add(0X, "sourcePath", Options.String);
	options.Add("s", "silent", Options.Flag);

	если options.Parse(context.arg, context.error) то

		path := options.GetFlag("directory");
		overwrite := options.GetFlag("overwrite");
		stopOnError := options.GetFlag("ignore");
		silent := options.GetFlag("silent");
		если ~options.GetString("prefix", prefix) то prefix := ""; всё;

		нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(archiveName) делай

			если ~options.GetString("sourcePath", fullArchiveName) то fullArchiveName := ""; всё;
			Strings.Append(fullArchiveName, archiveName);

			archive := OpenArchive(fullArchiveName, context.error);
			если (archive # НУЛЬ) то
				context.out.пСтроку8("Extracting "); context.out.пСтроку8(fullArchiveName); context.out.пСтроку8(" ... ");
				если ~silent то context.out.пВК_ПС; всё;
				context.out.ПротолкниБуферВПоток;
				nofExtracted := 0; nofErrors := 0;
				entry := Zip.FirstEntry(archive);
				нцПока (entry # НУЛЬ) и ((nofErrors = 0) или ~stopOnError) делай
					если (prefix # "") то
						копируйСтрокуДо0(prefix, filename); Append(filename, entry.name);
					иначе
						копируйСтрокуДо0(entry.name, filename);
					всё;
					если silent то
						ExtractFile(archive, entry, filename, path, overwrite, НУЛЬ, НУЛЬ, res);
					иначе
						ExtractFile(archive, entry, filename, path, overwrite, context.out, context.error, res);
					всё;
					если (res = Zip.Ok) то
						увел(nofExtracted);
					иначе
						увел(nofErrors);
					всё;
					entry := Zip.NextEntry(entry);
				кц;
				если (nofExtracted > 1) то
					context.out.пЦел64(nofExtracted, 0);	context.out.пСтроку8(" entries extracted");
				всё;
				если (nofErrors > 0) то
					context.out.пСтроку8(" (");
					если (nofErrors = 1) то context.out.пСтроку8("1 error)");
					иначе context.out.пЦел64(nofErrors, 0); context.out.пСтроку8(" errors)");
					всё;
					context.result := Commands.CommandError;
				всё;
				если (nofExtracted > 1) или (nofErrors > 0) то context.out.пВК_ПС; всё;
			всё;
		кц;
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон ExtractAll;

(** Adds a file to the selected zip-archive.
	level: specifies the compression level (0: no compression, 9: best compression)
	strategy: specifies the compression strategy (from 0 - 2)
	res = Zip.Ok, Zip.BadName, Zip.EntryAlreadyExists, Zip.DataError **)
проц AddFile*(arc: Zip.Archive; конст srcname : массив из симв8; конст dstname: массив из симв8; level, strategy: цел32; перем res: целМЗ);
перем f: Files.File; r: Files.Rider;
нач
	f := Files.Old(srcname);
	если f = НУЛЬ то
		res := Zip.BadName
	иначе
		f.Set(r, 0);
		Zip.AddEntry(arc, dstname, r, f.Length()(цел32), устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(level)), устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(strategy)), res);
	всё;
кон AddFile;

проц GetFileName(конст fullname : массив из симв8; перем filename : массив из симв8);
перем prefix : Files.Prefix; pathname, path : Files.FileName;
нач
	Files.SplitName(fullname, prefix, pathname);
	Files.SplitPath(pathname, path, filename);
кон GetFileName;

проц GetName(конст fullname : массив из симв8; перем name : массив из симв8);
перем prefix : Files.Prefix;
нач
	Files.SplitName(fullname, prefix, name);
	если (name[0] = Files.PathDelimiter) то Strings.Delete(name, 0, 1); всё;
кон GetName;

(** Adds the selected files to the selected zip-archive.
	ZipTool.Add [--level=<int> [--strategy=<int>]] [--nopath] ZipFile  {Entry} ~
	Options:
	--level=<integer>: specifies the compression level (0: no compression, 9: best compression)
		if not set, default level (-1) is used
	--strategy=<integer>: specifies the compression strategy (from 0 - 2)
	--nopath: remove prefix & path from filename
	--removePrefix: remove prefix from filename (but keep path)
	--ignore: continue in case of errors
	--silent: Only error output *)
проц Add*(context : Commands.Context);
перем
	archiveName, entryName : Files.FileName;
	options : Options.Options;
	archive: Zip.Archive;
	strategy, level: цел32; stopOnError : булево;
	oldname, newname: массив 256 из симв8;
	nofAdded, nofErrors: цел32; res: целМЗ;

	проц ShowFile(конст oldname, newname : массив из симв8; out : Потоки.Писарь);
	нач
		context.out.пСтроку8("Adding "); context.out.пСтроку8(oldname);
		если (oldname # newname) то context.out.пСтроку8(" -> "); context.out.пСтроку8(newname); всё;
		context.out.пСтроку8(" ... ");
	кон ShowFile;

нач
	нов(options);
	options.Add("l", "level", Options.Integer);
	options.Add("s", "strategy", Options.Integer);
	options.Add("n", "nopath", Options.Flag);
	options.Add("i", "ignore", Options.Flag);
	options.Add("r", "removePrefix", Options.Flag);
	options.Add(0X, "silent", Options.Flag);

	если options.Parse(context.arg, context.error) то

		если ~options.GetInteger("level", level) то level := Zip.DefaultCompression; всё;
		если ~options.GetInteger("strategy", strategy) то strategy := Zip.DefaultStrategy; всё;
		stopOnError := ~options.GetFlag("ignore");

		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(archiveName);

		archive := Zip.CreateArchive(archiveName, res);
		если (res = Zip.Ok) то
			nofAdded := 0; nofErrors := 0;
			нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(entryName) и ((nofErrors = 0) или ~stopOnError) делай

				копируйСтрокуДо0(entryName, oldname);
				если options.GetFlag("nopath") то
					GetFileName(entryName, newname);
				иначе
					если options.GetFlag("removePrefix") то
						GetName(entryName, newname);
					иначе
						копируйСтрокуДо0(entryName, newname);
					всё;
				всё;

				если ~options.GetFlag("silent") то
					ShowFile(oldname, newname, context.out);
				всё;
				AddFile(archive, oldname, newname, level, strategy, res);
				если (res = Zip.Ok) то
					увел(nofAdded);
					если ~options.GetFlag("silent") то
						context.out.пСтроку8("done."); context.out.пВК_ПС;
					всё;
				иначе
					увел(nofErrors);
					если options.GetFlag("silent") то
						ShowFile(oldname, newname, context.out);
					всё;
					Zip.ShowError(res, context.out); context.out.пВК_ПС;
				всё;
			кц;
			если (nofAdded > 1) то
				context.out.пЦел64(nofAdded, 0); context.out.пСтроку8(" entries added to archive "); context.out.пСтроку8(archiveName);
			всё;
			если (nofErrors > 0) то
				context.out.пСтроку8(" ("); context.out.пЦел64(nofErrors, 0);
				если (nofErrors = 1) то context.out.пСтроку8(" error)"); иначе context.out.пСтроку8(" errors)"); всё;
				context.result := Commands.CommandError;
			всё;
			если (nofAdded > 1) или (nofErrors > 0) то context.out.пВК_ПС; всё;
		иначе
			context.error.пСтроку8("Could not create archive '"); context.error.пСтроку8(archiveName); context.error.пСтроку8("': ");
			Zip.ShowError(res, context.error); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон Add;

(** Deletes the selected entries from the selected zip-archive.
	ZipTool.Delete [--ignore] ZipFile {Entry} ~ **)
проц Delete*(context : Commands.Context);
перем
	archiveName, entryName : Files.FileName;
	options : Options.Options;
	archive: Zip.Archive;
	entry: Zip.Entry;
	stopOnError : булево;
	nofDeleted, nofErrors: цел32; res: целМЗ;
нач
	нов(options);
	options.Add("i", "ignore", Options.Flag);

	если options.Parse(context.arg, context.error) то

		stopOnError := ~options.GetFlag("ignore");

		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(archiveName);
		archive := OpenArchive(archiveName, context.error);
		если (archive # НУЛЬ) то
			nofDeleted := 0; nofErrors := 0;
			нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(entryName) и ((nofErrors = 0) или ~stopOnError) делай
				entry := Zip.GetEntry(archive, entryName, res);
				context.out.пСтроку8("Deleting entry "); context.out.пСтроку8(entryName); context.out.пСтроку8(" ... ");
				если (res = Zip.Ok) то
					Zip.DeleteEntry(archive, entry, res);
					если (res = Zip.Ok) то
						увел(nofDeleted);
						context.out.пСтроку8("done.");
					всё;
				всё;
				если (res # Zip.Ok) то
					увел(nofErrors);
					Zip.ShowError(res, context.out);
				всё;
				context.out.пВК_ПС;
			кц;
			если (nofDeleted > 1) то
				context.out.пЦел64(nofDeleted, 0);
				если (nofDeleted = 1) то context.out.пСтроку8(" entry deleted");
				иначе context.out.пСтроку8(" entries deleted");
				всё;
			всё;
			если (nofErrors > 0) то
				context.out.пСтроку8(" (");
				context.out.пЦел64(nofErrors, 0);
				если (nofErrors = 1) то context.out.пСтроку8("error)");
				иначе context.out.пСтроку8(" errors)");
				всё;
				context.result := Commands.CommandError;
			всё;
			если (nofDeleted > 1) или (nofErrors > 0) то context.out.пВК_ПС; всё;
		всё;
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон Delete;

кон ZipTool.

System.Free ZipTool ~

ZipTool.Directory ZeroSkin.zip ~
ZipTool.Directory --details ZeroSkin.zip ~

ZipTool.Extract ZeroSkin.zip arrow.png ~

ZipTool.ExtractAll ZeroSkin.zip ~
