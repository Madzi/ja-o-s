(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Example1;	(* pjm *)

(*
Single resource monitor.
Ref: C.A.R. Hoare, "Monitors: An Operating System Structuring Concept", CACM 17(10), 1974
*)

тип
	Semaphore* = окласс
		перем busy: булево;

		проц Acquire*;
		нач {единолично}
			дождись(~busy);
			busy := истина
		кон Acquire;

		проц Release*;
		нач {единолично}
			busy := ложь
		кон Release;

		проц &Init*;
		нач
			busy := ложь
		кон Init;

	кон Semaphore;

кон Example1.
