(* Raspberry Pi environment *)
(* Copyright (C) Florian Negele *)

модуль Environment;

использует НИЗКОУР, Activities, CPU, HeapManager, Interrupts, Трассировка, Processors, Timer;

конст Running* = 0; ShuttingDown* = 1; Rebooting* = 2;

перем memory: размерМЗ;
перем heap: HeapManager.Heap;
перем clock := 0: цел32;
перем frequency: Timer.Counter;
перем status* := Running: целМЗ;
перем uartInterruptInstalled := 0: размерМЗ;
перем timerInterruptInstalled := 0: размерМЗ;
перем uartInterrupt: Interrupts.Interrupt;
перем previousTimerHandler := НУЛЬ: CPU.InterruptHandler;
перем atags {неОтслСборщиком}: укль {опасныйДоступКПамяти} на массив 1000H из размерМЗ;

проц {NORETURN} Abort-;
нач {безКооперации, безОбычныхДинПроверок}
	если НИЗКОУР.GetActivity () # НУЛЬ то Activities.TerminateCurrentActivity всё;
	Activities.TerminateCurrentActivity;
кон Abort;

проц Allocate- (size: размерМЗ): адресВПамяти;
перем result, address: адресВПамяти;
нач {безКооперации, безОбычныхДинПроверок}
	result := HeapManager.Allocate (size, heap);
	если result = НУЛЬ то возврат НУЛЬ всё;
	нцДля address := result до result + size - 1 делай НИЗКОУР.запиши8битПоАдресу (address, 0) кц;
	возврат result;
кон Allocate;

проц Deallocate- (address: адресВПамяти);
нач {безКооперации, безОбычныхДинПроверок}
	HeapManager.Deallocate (address, heap);
кон Deallocate;

проц Write- (character: симв8);
нач {безКооперации, безОбычныхДинПроверок}
	нцПока CPU.TXFF в CPU.ReadMask (CPU.UART_FR) делай кц;
	CPU.WriteWord (CPU.UART_DR, кодСимв8 (character));
кон Write;

проц Read- (перем character: симв8): булево;
нач {безКооперации, безОбычныхДинПроверок}
	нцПока CPU.RXFE в CPU.ReadMask (CPU.UART_FR) делай
		если сравнениеСОбменом (uartInterruptInstalled, 0, 1) = 0 то
			Interrupts.Install (uartInterrupt, CPU.IRQ);
		всё;
		CPU.WriteMask (CPU.IRQEnable2, {25});
		Interrupts.Await (uartInterrupt);
		если status # Running то возврат ложь всё;
	кц;
	character := симв8ИзКода (CPU.ReadWord (CPU.UART_DR));
	Write (character); возврат истина;
кон Read;

проц Flush-;
нач {безКооперации, безОбычныхДинПроверок}
	нцДо кцПри CPU.TXFE в CPU.ReadMask (CPU.UART_FR);
кон Flush;

проц GetString- (конст name: массив из симв8; перем result: массив из симв8);
нач {безКооперации, безОбычныхДинПроверок}
	result[0] := 0X
кон GetString;

проц Clock- (): цел32;
нач возврат Timer.GetCounter () DIV frequency;
кон Clock;

проц Sleep- (milliseconds: цел32);
перем interrupt: Interrupts.Interrupt;
нач {безКооперации, безОбычныхДинПроверок}
	утв (milliseconds >= 0);
	если сравнениеСОбменом (timerInterruptInstalled, 0, 1) = 0 то
		previousTimerHandler := CPU.InstallInterrupt (HandleTimer, CPU.IRQ);
		CPU.WriteWord (CPU.STC1, CPU.ReadWord (CPU.STCLO) + 1000);
		CPU.WriteMask (CPU.IRQEnable1, {1});
	всё;
	Interrupts.Install (interrupt, CPU.IRQ); увел (milliseconds, clock);
	нцПока clock - milliseconds < 0 делай Interrupts.Await (interrupt) кц;
кон Sleep;

проц HandleTimer (index: размерМЗ);
нач {безКооперации, безОбычныхДинПроверок}
	если previousTimerHandler # НУЛЬ то previousTimerHandler (index) всё;
	если 1 в CPU.ReadMask (CPU.STCS) то
		CPU.WriteWord (CPU.STC1, CPU.ReadWord (CPU.STCLO) + 1000);
		CPU.WriteMask (CPU.STCS, {1}); увел (clock);
		CPU.WriteMask (CPU.IRQEnable1, {1});
	всё;
кон HandleTimer;

проц LED- (led: цел32; status: булево);
нач {безКооперации, безОбычныхДинПроверок}
	если led = 0 то
	CPU.MaskIn (CPU.GPFSEL4, {21..23}, {21});
	если status то CPU.WriteMask (CPU.GPSET1, {15}) иначе CPU.WriteMask (CPU.GPCLR1, {15}) всё;
	иначе (* power led *)
		если status то
			CPU.MaskIn (CPU.GPFSEL3, {15..17}, {17});
		иначе
			CPU.MaskIn (CPU.GPFSEL3, {15..17}, {15});
		всё;
	всё;
кон LED;



проц Shutdown*;
нач {безКооперации, безОбычныхДинПроверок}
	если сравнениеСОбменом (status, Running, ShuttingDown) # Running то возврат всё;
	Трассировка.StringLn ("system: shutting down...");
	если uartInterruptInstalled # 0 то CPU.WriteMask (CPU.IRQDisable2, {25}); Interrupts.Cancel (uartInterrupt) всё;
	если timerInterruptInstalled # 0 то CPU.WriteMask (CPU.IRQDisable1, {1}) всё;
кон Shutdown;

проц Reboot*;
нач {безКооперации, безОбычныхДинПроверок}
	Shutdown;
	утв (сравнениеСОбменом (status, ShuttingDown, Rebooting) = ShuttingDown);
кон Reboot;

проц {NORETURN} Exit- (status: целМЗ);
нач {безКооперации, безОбычныхДинПроверок}
	Трассировка.пСтроку8 ("system: ");
	если status = Rebooting то Трассировка.StringLn ("rebooting..."); CPU.Reset всё;
	Трассировка.StringLn ("ready for power off or restart"); Flush; CPU.Halt;
кон Exit;

проц InitTrace;
конст BaudRate = 115200;
нач {безКооперации, безОбычныхДинПроверок}
	CPU.WriteMask (CPU.UART_CR, {CPU.UARTEN});
	CPU.Unmask (CPU.GPPUD, {CPU.PUD}); CPU.Delay (150);
	CPU.Mask (CPU.GPPUDCLK0, {14, 15}); CPU.Delay (150);
	CPU.WriteMask (CPU.GPPUDCLK0, {});
	CPU.WriteMask (CPU.UART_ICR, {1, 4..10});
	CPU.WriteWord (CPU.UART_IBRD, CPU.FUARTCLK DIV (16 * BaudRate));
	CPU.WriteWord (CPU.UART_FBRD, (CPU.FUARTCLK остОтДеленияНа (16 * BaudRate)) * 64 DIV (16 * BaudRate));
	CPU.WriteMask (CPU.UART_LCRH, CPU.WLEN8);
	CPU.WriteMask (CPU.UART_IMSC, {CPU.RXIM});
	CPU.WriteMask (CPU.UART_CR, {CPU.UARTEN, CPU.TXE, CPU.RXE});
	Трассировка.Настройся; Трассировка.пСимв8 := Write;
кон InitTrace;

проц InitMemory;
конст ATAG_MEM = 054410002H;
перем atag := 0: размерМЗ; memTag {неОтслСборщиком}: укль {опасныйДоступКПамяти} на запись size: размерМЗ; start: адресВПамяти кон;
нач {безКооперации, безОбычныхДинПроверок}
	нцПока atags[atag + 1] # ATAG_MEM делай увел (atag, atags[atag]) кц;
	memTag := операцияАдресОт atags[atag + 2];
	CPU.IdentityMapMemory (memTag.size); CPU.EnableMemoryManagementUnit;
	HeapManager.Initialize (heap, операцияАдресОт KernelEnd, memTag.start + memTag.size);
	memory := memTag.start + memTag.size - операцияАдресОт KernelEnd;
кон InitMemory;

проц Blink-(num, speed: цел32);
перем i,j: цел32;
нач {безКооперации, безОбычныхДинПроверок}
	нцДля  j:= 1 до num делай
		LED (0, истина); LED(1, ложь);
		нцДля i := 0 до speed*1000000 делай кц;
		LED(0, ложь); LED(1, истина);
		нцДля i := 0 до speed*1000000 делай кц;
	кц;
кон Blink;

проц StoreActivity-;
нач {безКооперации, безОбычныхДинПроверок}
кон StoreActivity;

проц RestoreActivity-;
нач {безКооперации, безОбычныхДинПроверок}
кон RestoreActivity;

проц Initialize-;
перем i: цел32;
нач {безКооперации, безОбычныхДинПроверок}
	НИЗКОУР.SetActivity (НУЛЬ);
	CPU.Initialize; InitTrace; InitMemory;
	frequency := Timer.GetFrequency () DIV 1000;
кон Initialize;

проц Terminate-;
нач {безКооперации, безОбычныхДинПроверок}
	Interrupts.Terminate;
	LED (0, ложь);
кон Terminate;

проц {NOPAF, INITIAL, FIXED(8000H)} KernelBegin;
машКод
	MOV	SP, #8000H
	LDR	R0, [PC, #tags-$-8]
	STR	R2, [R0, #0]
	B		skip
tags:
	d32	atags
skip:
кон KernelBegin;

проц {NOPAF, неРасширяемая, выровнять(32)} KernelEnd;
машКод
кон KernelEnd;

нач {безОбычныхДинПроверок}
	Трассировка.пСтроку8 ("Version "); Трассировка.пСтроку8 (НИЗКОУР.Date); Трассировка.пСтроку8 (" (");
	Трассировка.пЦел64 (memory DIV (1024 * 1024), 0); Трассировка.пСтроку8 (" MB RAM, GC, ");
	Трассировка.пЦел64 (Processors.count, 0); Трассировка.пСтроку8 (" CPU");
	если Processors.count > 1 то Трассировка.пСимв8 ('s') всё; Трассировка.пСимв8 (')'); Трассировка.пВК_ПС;
кон Environment.
