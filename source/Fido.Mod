(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Fido; (** AUTHOR "pjm"; PURPOSE "Watchdog debugging"; *)

использует ЛогЯдра, Modules, Objects, Kernel, Потоки, ProcessInfo;

тип
	Watchdog* = окласс
		перем
			timer: Kernel.Timer; delay: цел32; time: Kernel.MilliTimer; alive, done: булево;
			name: массив 32 из симв8;

		проц Cleanup;
		нач {единолично}
			timer.Wakeup;
			alive := ложь;
			дождись(done)
		кон Cleanup;

		проц Done;
		нач {единолично}
			done := истина
		кон Done;

		проц Reset*;
		нач
			Kernel.SetTimer(time, delay)
		кон Reset;

		проц &Init*(конст name: массив из симв8; delay: цел32);
		нач
			копируйСтрокуДо0(name, сам.name); сам.delay := delay;
			alive := истина; done := ложь;
			нов(timer)
		кон Init;

	нач {активное}
		нц
			timer.Sleep(delay);
			если ~alive то прервиЦикл всё;
			если Kernel.Expired(time) то
				ЛогЯдра.пСтроку8("Watchdog "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС;
				Action
			всё
		кц;
		Done
	кон Watchdog;

перем
	watchdog: Watchdog;

проц Action*;
перем
	processes : массив ProcessInfo.MaxNofProcesses из Objects.Process;
	nofProcesses, i : размерМЗ;
	writer: Потоки.Писарь;
нач
	Потоки.НастройПисаря(writer, ЛогЯдра.ЗапишиВПоток);
	ProcessInfo.GetProcesses(processes, nofProcesses);
	ProcessInfo.Sort(processes, nofProcesses, ProcessInfo.SortByID);
	нцДля i := 0 до nofProcesses - 1 делай
		ProcessInfo.ShowProcess(processes[i], writer);
	кц;
кон Action;

проц Allocate;
нач {единолично}
	если watchdog = НУЛЬ то нов(watchdog, "Default", 5000) всё
кон Allocate;

проц Reset*;
нач
	если watchdog = НУЛЬ то Allocate всё;
	watchdog.Reset
кон Reset;

проц Cleanup;
нач
	если watchdog # НУЛЬ то watchdog.Cleanup всё
кон Cleanup;

нач
	watchdog := НУЛЬ;
	Modules.InstallTermHandler(Cleanup)
кон Fido.

Fido.Reset

System.Free Fido ~
