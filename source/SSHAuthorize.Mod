модуль SSHAuthorize; 	(* g.f.	2001.12.12 *)

использует T := SSHTransport, U := CryptoUtils, B := CryptoBigNumbers, RSA := CryptoRSA, G := SSHGlobals,
	SHA1 := CryptoSHA1, Strings, Out := ЛогЯдра, Files, WMDialogs, Beep;

тип
	Connection* = T.Connection;

конст
	Closed* = T.Closed;  Connected* = T.Connected;

	ServiceRequest = 5X;  ServiceAccept = 6X;

	UserauthRequest = 32X;  UserauthFailure = 33X;
	UserauthSuccess = 34X;  UserauthBanner = 35X;
	UserauthPkOk = 3CX;

тип
	Password = укль на запись
		next: Password;
		host, user, pw: массив 64 из симв8;
	кон;

перем
	passwords: Password;

	privKey, pubKey: RSA.Key;

	проц GetPW( конст host, user: массив из симв8; перем pw: массив из симв8 );
	перем n: Password;
	нач
		n := passwords;  pw := "";
		нцПока n # НУЛЬ делай
			если (n.host = host) и (n.user = user) то копируйСтрокуДо0( n.pw, pw );  возврат  всё;
			n := n.next
		кц
	кон GetPW;


	проц AddPW( конст host, user, pw: массив из симв8 );
	перем n, p: Password;
	нач
		n := passwords;
		нцПока n # НУЛЬ делай
			если (n.host = host) и (n.user = user) то
				(* replace pw *)
				копируйСтрокуДо0( pw, n.pw );  возврат
			всё;
			p := n;  n := n.next
		кц;
		если p = НУЛЬ то  нов( passwords );  p := passwords
		иначе  нов( p.next );  p := p.next
		всё;
		p.next := НУЛЬ;
		копируйСтрокуДо0( host, p.host );
		копируйСтрокуДо0( user, p.user );
		копируйСтрокуДо0( pw, p.pw )
	кон AddPW;




	проц RequestService( перем ssh: Connection;  конст service: массив из симв8 ): булево;
	перем p: T.Packet;  d: размерМЗ;  buf: массив 256 из симв8;
	нач
		нов( p, ServiceRequest, 256 );
		p.AppString( service );
		ssh.SendPacket( p );

		если ssh.ReceivePacket( buf, d ) = ServiceAccept то  возврат истина
		иначе  ssh.Disconnect( 11, "" );  возврат ложь
		всё
	кон RequestService;


	проц RequestAuthorizeNone( ssh: Connection;  конст user: массив из симв8 ): булево;
	перем p: T.Packet;  msg: массив 512 из симв8;  len: размерМЗ;
	нач
		нов( p, UserauthRequest, 256 );
		p.AppString( user );
		p.AppString( "ssh-connection" );
		p.AppString( "none" );

		ssh.SendPacket( p );
		возврат ssh.ReceivePacket( msg, len ) = UserauthSuccess
	кон RequestAuthorizeNone;


	проц RequestConnPW( ssh: Connection;  конст user, host: массив из симв8; try: цел32 );
	перем p: T.Packet;  headline, pw: массив 64 из симв8;
	нач
		headline := "SSH: Enter Password for ";
		Strings.Append( headline, user );
		Strings.Append( headline, "@" );
		Strings.Append( headline, host );

		нов( p, UserauthRequest, 1024 );
		p.AppString( user );
		p.AppString( "ssh-connection" );
		p.AppString( "password" );
		p.AppChar( 0X );
		если try = 1 то  GetPW( host, user, pw ) иначе  pw := ""  всё;
		если pw = "" то
			Beep.Beep( 1000 );
			неважно WMDialogs.QueryPassword( headline, pw);
			AddPW( host, user, pw );
		всё;
		p.AppString( pw );

		ssh.SendPacket( p )
	кон RequestConnPW;


	проц AuthorizePasswd( ssh: Connection; конст host, user: массив из симв8 ): булево;
	перем
		msg: массив 2048 из симв8;
		len: размерМЗ;  try: цел16;
	нач
		try := 1;
		RequestConnPW( ssh, user, host, try );
		нц
			просей ssh.ReceivePacket( msg, len ) из
			| UserauthBanner:
					U.PrintBufferString( msg, 1 ); Out.пВК_ПС;
			| UserauthSuccess:
					Out.пСтроку8( "password authentication succeeded" );  Out.пВК_ПС;
					возврат истина
			| UserauthFailure:
					если try > 1 то
						Out.пСтроку8( "password authentication failed" );  Out.пВК_ПС;
						возврат ложь
					иначе
						увел( try );
						RequestConnPW( ssh, user, host, try )
					всё
			иначе
				Out.пСтроку8( "SSHAuthorization.AuthorizePasswd: protocol error: got " );  Out.пЦел64( кодСимв8( msg[0] ), 3 );  Out.пВК_ПС;
				возврат ложь
			всё
		кц
	кон AuthorizePasswd;


	проц MakePubKeyBlob( перем buf: массив из симв8;  перем len: размерМЗ );
	нач
		len := 0;
		U.PutString( buf, len, "ssh-rsa" );
		U.PutBigNumber( buf, len, pubKey.exponent );
		U.PutBigNumber( buf, len, pubKey.modulus );
	кон MakePubKeyBlob;



	проц CheckAuthorizeKey( ssh: Connection;  конст user: массив из симв8 ): булево;
	перем p: T.Packet;  buf: массив 512 из симв8; len :размерМЗ;
	нач
		MakePubKeyBlob( buf, len );

		нов( p, UserauthRequest, 1024 );
		p.AppString( user );
		p.AppString( "ssh-connection" );
		p.AppString( "publickey" );
		p.AppChar( 0X );  (* false *)
		p.AppString( "ssh-rsa" );
		p.AppArray( buf, 0, len );

		ssh.SendPacket( p );
		если ssh.ReceivePacket( buf, len ) # UserauthPkOk то
			U.PrintBufferString( buf, 1 );  Out.пВК_ПС;
			возврат ложь
		всё;
		возврат истина
	кон CheckAuthorizeKey;


	проц RequestAuthorizeKey( ssh: Connection;  конст user: массив из симв8 ): булево;
	конст
		Asn1DerSha1 = "3021300906052B0E03021A05000414";
		HashLen = 20;
		MsgLen = 15 + HashLen;
		EmSize = 256;
		PadLen = EmSize - MsgLen;
	перем
		p: T.Packet;
		blob, sig, buf: массив 512 из симв8; pos, blen, len: размерМЗ;
		signature: B.BigNumber;
		sha1: SHA1.Hash;
		em: массив EmSize из симв8;
		i: цел32;
	нач
		утв( privKey.size = 2048 );
		MakePubKeyBlob( blob, blen );
		нов( sha1 );  sha1.Initialize;

		pos := 0;
		U.PutArray( buf, pos, ssh.sessionId, 0, 20 );
		U.PutChar( buf, pos, UserauthRequest );
		U.PutString( buf, pos, user );
		U.PutString( buf, pos, "ssh-connection" );
		U.PutString( buf, pos, "publickey" );
		U.PutChar( buf, pos, 1X );
		U.PutString( buf, pos, "ssh-rsa" );
		U.PutArray( buf, pos, blob, 0, blen );
		sha1.Update( buf, 0, pos );

		(* padding PKCS1 type 1 *)
		em[0] := 0X;  em[1] := 1X;
		нцДля i := 2 до PadLen - 2 делай em[i] := 0FFX  кц;
		em[PadLen - 1] := 0X;

		U.Hex2Bin( Asn1DerSha1, 0, em, EmSize - MsgLen, 15 );
		sha1.GetHash( em, EmSize - HashLen );

		signature := privKey.Sign( em, EmSize );
		pos := 0;
		U.PutString( sig, pos, "ssh-rsa" );
		U.PutBigNumber( sig, pos, signature );

		нов( p, UserauthRequest, 1024 );
		p.AppString( user );
		p.AppString( "ssh-connection" );
		p.AppString( "publickey" );
		p.AppChar( 1X );  (* true *)
		p.AppString( "ssh-rsa" );
		p.AppArray( blob, 0, blen );
		p.AppArray( sig, 0, pos );

		ssh.SendPacket( p );
		если ssh.ReceivePacket( buf, len ) # UserauthSuccess то
			U.PrintBufferString( buf, 1 );  Out.пВК_ПС;
			Out.пСтроку8( "public key authentication failed" );  Out.пВК_ПС;
			возврат ложь
		всё;
		Out.пСтроку8( "public key authentication succeeded" );  Out.пВК_ПС;
		возврат истина
	кон RequestAuthorizeKey;

	проц AuthorizeKey( ssh: Connection; конст user: массив из симв8 ): булево;
	конст
		headline = "enter passphrase for opening your private key";
	перем
		f: Files.File; r: Files.Reader; i: цел32;
		pw: массив 64 из симв8;
	нач
		если privKey = НУЛЬ то
			f := Files.Old( G.PrivateKeyFile );
			если f = НУЛЬ то
				Out.пСтроку8( "private key '" ); Out.пСтроку8( G.PrivateKeyFile );
				Out.пСтроку8( "' not found" ); Out.пВК_ПС;
				возврат ложь
			всё;
			Files.OpenReader( r, f, 0 );  i := 0;
			нцДо
				Beep.Beep( 1000 );
				неважно WMDialogs.QueryPassword( headline, pw );
				r.ПерейдиКМестуВПотоке( 0 );
				privKey := RSA.LoadPrivateKey( r, pw );
				увел( i )
			кцПри (privKey # НУЛЬ) или (i = 2);
			если privKey = НУЛЬ то
				Out.пСтроку8( "### error: wrong passphrase" ); Out.пВК_ПС;  возврат ложь
			всё;
		всё;
		f := Files.Old( G.PublicKeyFile );
		если f = НУЛЬ то
			Out.пСтроку8( "### error: public key not found" );  Out.пВК_ПС;
			возврат ложь
		всё;
		Files.OpenReader( r, f, 0 );
		pubKey := RSA.LoadPublicKey( r );

		если CheckAuthorizeKey( ssh, user ) то
			возврат RequestAuthorizeKey( ssh, user )
		всё;
		возврат ложь
	кон AuthorizeKey;


	(** Open an outhorized SSH connection, returns NIL on failure *)
	проц OpenConnection*( конст host, user: массив из симв8 ): Connection;
	перем
		conn: Connection; authorized: булево;
	нач
		нов( conn, host );
		если conn.state = Connected то
			если RequestService( conn, "ssh-userauth" ) то
				authorized := RequestAuthorizeNone( conn, user );
				если ~authorized то
					Out.пСтроку8( "trying public key authentication" ); Out.пВК_ПС;
					authorized := AuthorizeKey( conn, user );
				всё;
				если ~authorized то
					Out.пСтроку8( "trying password authentication" ); Out.пВК_ПС;
					authorized := AuthorizePasswd( conn, host, user )
				всё;
				если authorized то  возврат conn  всё
			всё;
			conn.Disconnect( 11, "" );  возврат НУЛЬ
		иначе
			возврат НУЛЬ
		всё
	кон OpenConnection;

кон SSHAuthorize.

