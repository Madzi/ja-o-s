модуль CryptoHMAC;  	(** AUTHOR "G.F."; PURPOSE "RFC 2104 HMAC"; *)

использует Hashes := CryptoHashes, BIT;

тип
	HMac* = окласс
		перем
			size-: размерМЗ;		(** mac size in bytes *)
			name-: массив 64 из симв8;
			ih, oh: Hashes.Hash;

		проц & Init*( конст hashmod: массив из симв8 );
		перем i, l: размерМЗ; c: симв8;
		нач
			ih := Hashes.NewHash( hashmod );
			oh := Hashes.NewHash( hashmod );
			size := ih.size;
			name := "hmac-";  l := 5; i := 0;
			нцДо  c := ih.name[i];  name[l] := c;  увел( l );  увел( i )  кцПри c = 0X;
			если (size < ih.size) и (size = 12) то
				name[l - 1] := '-'; name[l] := '9';  name[l+1] := '6'; name[l+2] := 0X
			всё;
			сам.size := size
		кон Init;

		(** Set a key, recommended key-length is the hash-size of the underlying hash-function.
			This method has to be invoked for EACH mac to be calculated *)
		проц Initialize*( конст key: массив из симв8; keyLen: размерМЗ );
		перем
			buf, buf2: массив 64 из симв8;
			i: размерМЗ;
		нач
			нцДля i := 0 до keyLen-1 делай  buf[i] := key[i]  кц;
			нцДля i := keyLen до 63 делай  buf[i] := 0X  кц;

			нцДля i := 0 до 63 делай  buf2[i] := BIT.CXOR( 36X, buf[i] )  кц;
			ih.Initialize;
			ih.Update( buf2, 0, 64 );

			нцДля i := 0 до 63 делай  buf2[i] := BIT.CXOR( 5CX, buf[i] )  кц;
			oh.Initialize;
			oh.Update( buf2, 0, 64 )
		кон Initialize;

		(** set string from which a mac will be calculated. strings can be concatenated by
			invoking Update several times without invoking Initialize *)
		проц Update*( конст data: массив из симв8;  pos, len: размерМЗ );
		нач
			ih.Update( data, pos, len )
		кон Update;

		(** Load the generated mac of size SELF.size into buf, starting at position pos *)
		проц GetMac*( перем buf: массив из симв8;  pos: размерМЗ );
		перем
			tmp: массив 64 из симв8;
			i: размерМЗ;
		нач
			ih.GetHash( tmp, 0 );
			oh.Update( tmp, 0, ih.size );
			oh.GetHash( tmp, 0 );
			нцДля i := 0 до size - 1 делай  buf[pos + i] := tmp[i]  кц
		кон GetMac;

	кон HMac;

кон CryptoHMAC.


System.Free CryptoHMAC ~
