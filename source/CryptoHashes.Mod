модуль CryptoHashes;	(** AUTHOR "G.F."; PURPOSE "Empty Hash"; *)

тип
	Hash* = окласс
		перем
			name-: массив 64 из симв8;
			size-: цел32;
			initialized*: булево;

		проц &Init*;
		нач
			initialized := ложь
		кон Init;

		проц Initialize*;
		нач
			СТОП(301) (* force overwriting *)
		кон Initialize;

		(** this method is invoked by subclasses *)
		проц SetNameAndSize*( конст name: массив из симв8; size: цел32 );
		нач
			копируйСтрокуДо0( name, сам.name );
			сам.size := size
		кон SetNameAndSize;

		(** data: value to be hashed; when Update is invoked several times before GetHash or Initialize
			is invoked, the concatenation of all data-parameters is hashed *)
		проц Update*( конст data: массив из симв8; ofs, len: размерМЗ );
		нач
			СТОП(301) (* force overwriting *)
		кон Update;

		(** get the hashvalue of length SELF.size *)
		проц GetHash*( перем buffer: массив из симв8; position: размерМЗ );
		нач
			СТОП(301) (* force overwriting *)
		кон GetHash;

	кон Hash;

	HashFactory = проц( ): Hash;

	(** get a new hash from module 'modname' *)
	проц NewHash*( конст modname: массив из симв8 ): Hash;
	перем hash : Hash; factory : HashFactory;
	нач
		утв( длинаМассива( modname ) < 57 );
		hash := НУЛЬ;
		дайПроцПоИмени( modname, "NewHash", factory );
		если factory # НУЛЬ то
			hash := factory();
		всё;
		возврат hash;
	кон NewHash;

кон CryptoHashes.


System.Free CryptoHashes~
