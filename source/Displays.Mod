(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Displays; (** AUTHOR "pjm"; PURPOSE "Abstract display device driver"; *)

использует НИЗКОУР, Plugins;

конст
		(** formats for Transfer.  value = bytes per pixel. *)
	index8* = 1; color565* = 2; color888* = 3; color8888* = 4;

		(** operations for Transfer. *)
	get* = 0; set* = 1;

		(** color components. *)
	red* = 00FF0000H; green* = 0000FF00H; blue* = 000000FFH;

	trans* = цел32(80000000H);	(** transparency for Mask. *)
	invert* = 40000000H;	(** inverting. *)
	(*alpha = 0C0000000H;	(** alpha blending. *)*)

	BufSize = 65536;
перем
	reverse*:булево;

тип
	Display* = окласс (Plugins.Plugin)
		перем
			width*, height*: цел32;	(** dimensions of visible display. *)
			offscreen*: цел32;	(** number of non-visible lines at the bottom of the display. *)
			format*: цел32;	(** format for Transfer. *)
			unit*: цел32;	(** approximate square pixel size = unit/36000 mm. *)
			fbadr-: адресВПамяти; fbsize-, fbstride-: цел32; (** frame buffer address, size and stride *)

		(** Transfer a block of pixels in "raw" display format to (op = set) or from (op = get) the display.  Pixels in the rectangular area are transferred from left to right and top to bottom.  The pixels are transferred to or from "buf", starting at "ofs".  The line byte increment is "stride", which may be positive, negative or zero. *)
		проц Transfer*(перем buf: массив из симв8; ofs, stride, x, y, w, h: размерМЗ; op: целМЗ);
		перем bufadr, buflow, bufhigh, dispadr,w0,b,d: адресВПамяти;
		нач
			если w > 0 то
				утв(fbadr # 0);
				bufadr := адресОт(buf[ofs]);
				dispadr := fbadr + y * fbstride + x * format;
				если reverse то
				dispadr := fbadr + (height-y-1) * fbstride + (width-x-1) * format;
				всё;

				утв((dispadr >= fbadr) и ((y+h-1)*fbstride + (x+w-1)*format <=  fbsize));	(* display index check *)
				w := w * format;	(* convert to bytes *)
				просей op из
					set:
						если reverse то
						нцПока h > 0 делай
							w0 := w DIV format; b:= bufadr; d := dispadr;
							нцПока w0 > 0 делай
								НИЗКОУР.копируйПамять(b, d, format);
								увел(b,format);
								умень(d, format);
								умень(w0);
							кц;
							увел(bufadr, stride); умень(dispadr, fbstride);
							умень(h)
						кц
						иначе
						нцПока h > 0 делай
							НИЗКОУР.копируйПамять(bufadr, dispadr, w);
							увел(bufadr, stride); увел(dispadr, fbstride);
							умень(h)
						кц
						всё;
					|get:
						если reverse то
						buflow := адресОт(buf[0]); bufhigh := buflow + длинаМассива(buf);
						нцПока h > 0 делай
							утв((bufadr >= buflow) и (bufadr+w <= bufhigh));	(* index check *)
							w0 := w DIV format; b:= bufadr; d := dispadr;
							нцПока w0 > 0 делай
								НИЗКОУР.копируйПамять(d, b, format);
								увел(b,format);
								умень(d, format);
								умень(w0);
							кц;
							увел(bufadr, stride); умень(dispadr, fbstride);
							умень(h)
						кц;
						иначе
						buflow := адресОт(buf[0]); bufhigh := buflow + длинаМассива(buf);
						нцПока h > 0 делай
							утв((bufadr >= buflow) и (bufadr+w <= bufhigh));	(* index check *)
							НИЗКОУР.копируйПамять(dispadr, bufadr, w);
							увел(bufadr, stride); увел(dispadr, fbstride);
							умень(h)
						кц;
						всё;
					иначе (* skip *)
				всё
			всё
		кон Transfer;

		(** Fill a rectangle in color "col". *)
		проц Fill*(col, x, y, w, h: цел32);
		нач
			Fill0(сам, col, x, y, w, h)
		кон Fill;

		(** Equivalent to Fill(col, x, y, 1, 1). *)
		проц Dot*(col, x, y: цел32);
		нач
			Fill(col, x, y, 1, 1)
		кон Dot;

		(** Transfer a block of pixels from a 1-bit mask to the display.  Pixels in the rectangular area are transferred from left to right and top to bottom.  The pixels are transferred from "buf", starting at bit offset "bitofs".  The line byte increment is "stride", which may be positive, negative or zero. "fg" and "bg" specify the colors for value 1 and 0 pixels respectively. *)
		проц Mask*(перем buf: массив из симв8; bitof, stride, fg, bg, x, y, w, h: цел32);
		конст SetSize = матМаксимум (мнвоНаБитахМЗ) + 1;
		перем p: адресВПамяти; i, bitofs: цел32; s: мнвоНаБитахМЗ;
		нач
			если (w > 0) и (h > 0) то
				i := цел32(адресОт(buf[0]) остОтДеленияНа размер16_от(мнвоНаБитахМЗ));
				bitofs := bitof + i * 8;
				p := адресОт(buf[0])-i + bitofs DIV SetSize * размер16_от (мнвоНаБитахМЗ);	(* p always aligned to 32-bit boundary *)
				bitofs := bitofs остОтДеленияНа SetSize; stride := stride*8;
				нц
					НИЗКОУР.прочтиОбъектПоАдресу(p, s); i := bitofs;
					нц
						если (i остОтДеленияНа SetSize) в s то
							если fg >= 0 то Dot(fg, устарПреобразуйКБолееУзкомуЦел(x+i-bitofs), y) всё
						иначе
							если bg >= 0 то Dot(bg, устарПреобразуйКБолееУзкомуЦел (x+i-bitofs), y) всё
						всё;
						увел(i);
						если i-bitofs = w то прервиЦикл всё;
						если i остОтДеленияНа SetSize = 0 то НИЗКОУР.прочтиОбъектПоАдресу(p+i DIV 8, s) всё
					кц;
					умень(h);
					если h = 0 то прервиЦикл всё;
					увел(y); увел(bitofs, stride);
					если (bitofs >= SetSize) или (bitofs < 0) то	(* moved outside s *)
						увел(p, bitofs DIV SetSize * размер16_от (мнвоНаБитахМЗ)); bitofs := bitofs остОтДеленияНа SetSize
					всё
				кц
			всё
		кон Mask;

		(** Copy source block sx, sy, w, h to destination dx, dy.  Overlap is allowed. *)
		проц Copy*(sx, sy, w, h, dx, dy: цел32);
		нач
			Copy0(сам, sx, sy, w, h, dx, dy)
		кон Copy;

		(** Update the visible display (if caching is used). *)
		проц Update*;
		кон Update;

		(** Map a color value to an 8-bit CLUT index.  Only used if format = index8. *)
		проц ColorToIndex*(col: цел32): цел32;
		нач
				(* default implementation is not very useful and should be overridden. *)
			возврат цел32(
					мнвоНаБитах32(арифмСдвиг(col, 7-23)) * {5..7} +
					мнвоНаБитах32(арифмСдвиг(col, 4-15)) * {2..4} +
					мнвоНаБитах32(арифмСдвиг(col, 1-7)) * {0..1})
		кон ColorToIndex;

		(** Map an 8-bit CLUT index to a color value.  Only used if format = index8. *)
		проц IndexToColor*(index: цел32): цел32;
		нач
				(* default implementation is not very useful and should be overridden. *)
			возврат
					арифмСдвиг(цел32(мнвоНаБитах32(index) * {5..7}), 23-7) +
					арифмСдвиг(цел32(мнвоНаБитах32(index) * {2..4}), 15-4) +
					арифмСдвиг(цел32(мнвоНаБитах32(index) * {0..1}), 7-1)
		кон IndexToColor;

		(** Initialize a linear frame buffer for Transfer. *)
		проц InitFrameBuffer*(adr: адресВПамяти; size, stride: цел32);
		нач
			утв((height+offscreen)*stride <= size);
			fbadr := adr; fbsize := size; fbstride := stride;
		кон InitFrameBuffer;

(*
		(** Draw a line. *)
		PROCEDURE Line*(col, x1, y1, x2, y2: SIGNED32);	(* error term, major, minor? *)
		BEGIN
			HALT(99)
		END Line;

		(* Like Mask, but replicate the mask in the specified rectangular area. *)
		PROCEDURE ReplMask*(VAR buf: ARRAY OF CHAR; bitofs, stride, fg, bg, px, py, pw, ph, x, y, w, h: SIGNED32);
		BEGIN
			HALT(99)
		END ReplMask;
*)

		(** Finalize the display.  Further calls to display methods are not allowed, and may cause exceptions. *)
		проц Finalize*;
		нач	(* should really be exclusive with Transfer, but we assume the caller keeps to the rules above *)
			fbadr := 0; fbsize := 0
		кон Finalize;

	кон Display;

перем
	registry*: Plugins.Registry;
	buf: укль на массив BufSize из симв8;

проц Fill0(d: Display; col, x, y, w, h: цел32);
перем j, w0, h0, s: цел32; p: адресВПамяти; t, c: мнвоНаБитах32; invert: булево;
нач {единолично}
	если (w > 0) и (h > 0) и (col >= 0) то	(* opaque or invert *)
		invert := арифмСдвиг(col, 1) < 0;
		если buf = НУЛЬ то нов(buf) всё;
		просей d.format из
			index8:
				s := 4; col := d.ColorToIndex(col);
				c := мнвоНаБитах32(арифмСдвиг(col, 24) + арифмСдвиг(col, 16) + арифмСдвиг(col, 8) + col)
			|color565:
				s := 4;
				col := цел32( мнвоНаБитах32(арифмСдвиг(col, 15-23)) * {11..15} +
						мнвоНаБитах32(арифмСдвиг(col, 10-15)) * {5..10} +
						мнвоНаБитах32(арифмСдвиг(col, 4-7)) * {0..4});
				c := мнвоНаБитах32(арифмСдвиг(col остОтДеленияНа 10000H, 16) + col остОтДеленияНа 10000H)
			|color888:
				s := 3; c := мнвоНаБитах32(col остОтДеленияНа 1000000H)
			|color8888:
				s := 4; c := мнвоНаБитах32(col остОтДеленияНа 1000000H)
		всё;
		w0 := w*d.format; h0 := (длинаМассива(buf^)-3) DIV w0;	(* -3 for 32-bit loops below *)
		утв(h0 > 0);
		если h < h0 то h0 := h всё;
		если ~invert то
			p := адресОт(buf[0]);
			нцДля j := 0 до (w0*h0-1) DIV s делай НИЗКОУР.запиши32битаПоАдресу(p, c); увел(p, s) кц
		иначе
			если c = {} то c := {0..31} всё
		всё;
		нц
			если invert то
				d.Transfer(buf^, 0, w0, x, y, w, h0, get);
				p := адресОт(buf[0]);
				нцДля j := 0 до (w0*h0-1) DIV s делай
					t := мнвоНаБитах32(НИЗКОУР.прочти32битаПоАдресу(p)); НИЗКОУР.запиши32битаПоАдресу(p, t / c); увел(p, s)
				кц
			всё;
			d.Transfer(buf^, 0, w0, x, y, w, h0, set);
			умень(h, h0);
			если h <= 0 то прервиЦикл всё;
			увел(y, h0);
			если h < h0 то h0 := h всё
		кц
	всё
кон Fill0;

проц Copy0(d: Display; sx, sy, w, h, dx, dy: цел32);
перем w0, h0, s: цел32;
нач {единолично}
	если (w > 0) и (h > 0) то
		если buf = НУЛЬ то нов(buf) всё;
		w0 := w*d.format; h0 := длинаМассива(buf^) DIV w0;
		утв(h0 > 0);
		если (sy >= dy) или (h <= h0) то
			s := 1
		иначе
			s := -1; увел(sy, h-h0); увел(dy, h-h0)
		всё;
		нц
			если h < h0 то
				если s = -1 то увел(sy, h0-h); увел(dy, h0-h) всё;
				h0 := h
			всё;
			d.Transfer(buf^, 0, w0, sx, sy, w, h0, get);
			d.Transfer(buf^, 0, w0, dx, dy, w, h0, set);
			умень(h, h0);
			если h <= 0 то прервиЦикл всё;
			увел(sy, s*h0); увел(dy, s*h0)
		кц
	всё
кон Copy0;

проц Reverse*;
нач
	reverse := ~reverse;
кон Reverse;

нач
	нов(registry, "Displays", "Display drivers");
	buf := НУЛЬ;
	reverse := ложь;
кон Displays.

(**
o The display origin (0,0) is at the top left.
o The display is "width" pixels wide and "height" pixels high.
o The offscreen area is a possibly empty extension below the visible display.  Its height is "offscreen" pixels.
o Rectangles are specified with the top left corner as pinpoint.
o No clipping is performed.
o The offset and stride parameters must always specify values inside the supplied buffer (otherwise results undefined).
o Accessing coordinates outside the display space (including offscreen) is undefined.
o "Undefined" in this case means a trap could occur, or garbage can be displayed, but memory will never be corrupted.
o Colors are 888 truecolor values represented in RGB order with B in the least significant byte.  The top 2 bits of a 32-bit color value are used for flags.  The other bits are reserved.
o The "invert" flag means the destination color is inverted with the given color.  The effect is implementation-defined, but must be reversible with the same color.  Usually an XOR operation is performed.
o The "trans" flag means the color is transparent and drawing in this color has no effect.  It is defined for Mask only.
o The transfer "format" should be chosen close to the native framebuffer format for efficiency.
o Transfer uses raw framebuffer values, and does not support color flags.
o A concrete Display must implement at least the Transfer function, or initialize a linear frame buffer and call the InitFrameBuffer method.
o An optimized Display driver should override all the primitives with accellerated versions.
o An "index8" display uses a fixed palette and map a truecolor value to an equivalent color in the palette.
o The palette can be chosen freely by a concrete 8-bit Display, which should override the ColorToIndex and IndexToColor methods.  These methods are not defined for other formats.
o The default ColorToIndex method assumes a direct-mapped palette with 3 bits each for red and green, and 2 bits for blue.
o Palette animation is not supported.
*)
