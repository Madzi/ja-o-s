модуль WMPieMenu; (** AUTHOR "TF"; PURPOSE "Pie Menu"; *)

использует
	Strings, WMMessages, WMEvents, WMWindowManager, WMComponents, WMGraphics, WMProperties;

конст
	MenuPoints = 8;
	InnerRadius = 15; OuterRadius = 84;

тип
	String = Strings.String;

	PieMenu = окласс(WMComponents.VisualComponent)
	перем
		lastX, lastY : размерМЗ;
		dir : массив 4 * MenuPoints из запись x, y : цел32 кон;
		clDefault, clHover, clShadow, clLine : WMProperties.ColorProperty;
		useBgBitmap : WMProperties.BooleanProperty;
		bgBitmapName : WMProperties.StringProperty;
		bgBitmap : WMGraphics.Image;
		shadow: WMProperties.Int32Property;
		dx, dy : размерМЗ;
		dirNr : цел32;
		on0, on1, on2, on3, closeIt : WMEvents.EventSource;
		images : массив 4 из WMGraphics.Image;
		texts : массив 4 из Strings.String;
		enabled, hover : мнвоНаБитахМЗ;
		sent : булево;

		проц &{перекрыта}Init*;
		нач
			Init^;

			нов(on0, сам, НУЛЬ, НУЛЬ, НУЛЬ);
			нов(on1, сам, НУЛЬ, НУЛЬ, НУЛЬ);
			нов(on2, сам, НУЛЬ, НУЛЬ, НУЛЬ);
			нов(on3, сам, НУЛЬ, НУЛЬ, НУЛЬ);
			нов(closeIt, сам, НУЛЬ, НУЛЬ, НУЛЬ);
			dir[0].x := 181; dir[0].y := -182;
			dir[1].x := 212; dir[1].y := -143;
			dir[2].x := 236; dir[2].y := -98;
			dir[3].x := 251; dir[3].y := -50;
			dir[4].x := 256; dir[4].y := 0;
			dir[5].x := 251; dir[5].y := 49;
			dir[6].x := 236; dir[6].y := 97;
			dir[7].x := 212; dir[7].y := 142;
			dir[8].x := 181; dir[8].y := 181;
			dir[9].x := 142; dir[9].y := 212;
			dir[10].x := 97; dir[10].y := 236;
			dir[11].x := 49; dir[11].y := 251;
			dir[12].x := -1; dir[12].y := 255;
			dir[13].x := -50; dir[13].y := 251;
			dir[14].x := -98; dir[14].y := 236;
			dir[15].x := -143; dir[15].y := 212;
			dir[16].x := -182; dir[16].y := 181;
			dir[17].x := -213; dir[17].y := 142;
			dir[18].x := -237; dir[18].y := 97;
			dir[19].x := -252; dir[19].y := 49;
			dir[20].x := -256; dir[20].y := -1;
			dir[21].x := -252; dir[21].y := -50;
			dir[22].x := -237; dir[22].y := -98;
			dir[23].x := -213; dir[23].y := -143;
			dir[24].x := -182; dir[24].y := -182;
			dir[25].x := -143; dir[25].y := -213;
			dir[26].x := -98; dir[26].y := -237;
			dir[27].x := -50; dir[27].y := -252;
			dir[28].x := 0; dir[28].y := -256;
			dir[29].x := 49; dir[29].y := -252;
			dir[30].x := 97; dir[30].y := -237;
			dir[31].x := 142; dir[31].y := -213;

			нов(clDefault, ProtoPmClDefault, НУЛЬ, НУЛЬ); properties.Add(clDefault);
			нов(clHover, ProtoPmClHover, НУЛЬ, НУЛЬ); properties.Add(clHover);
			нов(clShadow, ProtoPmClShadow, НУЛЬ, НУЛЬ); properties.Add(clShadow);
			нов(clLine, ProtoPmClLine, НУЛЬ, НУЛЬ); properties.Add(clLine);
			нов(useBgBitmap, ProtoPmUseBgBitmap, НУЛЬ, НУЛЬ); properties.Add(useBgBitmap);
			нов(bgBitmapName, ProtoPmBgBitmapName, НУЛЬ, НУЛЬ); properties.Add(bgBitmapName);
			нов(shadow, ProtoPmShadow, НУЛЬ, НУЛЬ); properties.Add(shadow);
			takesFocus.Set(истина);
			enabled := {0..3};
			SetNameAsString(StrPieMenu);
		кон Init;

		проц {перекрыта}RecacheProperties*;
		перем s : String;
		нач
			если useBgBitmap.Get() то
				s := bgBitmapName.Get(); если s # НУЛЬ то bgBitmap := WMGraphics.LoadImage(s^, истина) всё
			всё;
		кон RecacheProperties;

		проц {перекрыта}PropertyChanged*(sender, prop : динамическиТипизированныйУкль);
		нач
			если prop = bgBitmapName то
				RecacheProperties
			иначе
				PropertyChanged^(sender, prop)
			всё
		кон PropertyChanged;

		проц GetSector(nr : размерМЗ; перем s : массив из WMGraphics.Point2d);
		перем i, j : размерМЗ; перем x, y : размерМЗ;
		нач
			x := bounds.GetWidth() DIV 2 - 4;
			y := bounds.GetHeight() DIV 2 - 4;
			nr := nr * MenuPoints;
			s[i].x := x + (InnerRadius * dir[nr остОтДеленияНа (4*MenuPoints)].x DIV 100H);
			s[i].y := y - (InnerRadius * dir[nr остОтДеленияНа (4*MenuPoints)].y DIV 100H);
			увел(i);
			s[i].x := x + (OuterRadius * dir[nr остОтДеленияНа (4*MenuPoints)].x DIV 100H);
			s[i].y := y - (OuterRadius * dir[nr остОтДеленияНа (4*MenuPoints)].y DIV 100H);
			увел(i);
			нцДля j := 1 до MenuPoints - 1 делай
				s[i].x := x + (OuterRadius * dir[(nr + j) остОтДеленияНа (4*MenuPoints)].x DIV 100H);
				s[i].y := y - (OuterRadius * dir[(nr + j) остОтДеленияНа (4*MenuPoints)].y DIV 100H);
				увел(i)
			кц;
			s[i].x := x + (OuterRadius * dir[(nr + MenuPoints)остОтДеленияНа (4*MenuPoints)].x DIV 100H);
			s[i].y := y - (OuterRadius * dir[(nr + MenuPoints) остОтДеленияНа (4*MenuPoints)].y DIV 100H);
			увел(i);
			s[i].x := x + (InnerRadius * dir[(nr + MenuPoints) остОтДеленияНа (4*MenuPoints)].x DIV 100H);
			s[i].y := y - (InnerRadius * dir[(nr + MenuPoints) остОтДеленияНа (4*MenuPoints)].y DIV 100H);
			увел(i);
			нцДля j := MenuPoints-1  до 1 шаг -1 делай
				s[i].x := x + (InnerRadius * dir[(nr + j) остОтДеленияНа (4*MenuPoints)].x DIV 100H);
				s[i].y := y - (InnerRadius * dir[(nr + j) остОтДеленияНа (4*MenuPoints)].y DIV 100H);
				увел(i)
			кц
		кон GetSector;

		проц SetImage(nr : цел32; image : WMGraphics.Image);
		нач
			images[nr] := image;
			Invalidate
		кон SetImage;

		проц SetText(nr: цел32; text: Strings.String);
		нач
			texts[nr] := text;
			Invalidate
		кон SetText;

		проц SetEnabled(s : мнвоНаБитахМЗ);
		нач
			Acquire;
			enabled := s;
			Release;
			Invalidate
		кон SetEnabled;

		проц SetHover(s : мнвоНаБитахМЗ);
		нач
			Acquire;
			hover := s;
			Release;
			Invalidate
		кон SetHover;

		проц {перекрыта}PointerMove*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		перем tdist, tx, ty : размерМЗ;
		нач
			dx := x - bounds.GetWidth() DIV 2 + 4;
			dy := y - bounds.GetHeight() DIV 2 + 4;
			tx := (dx * dx);
			ty := (dy * dy);
			tdist := tx + ty;

			если tdist > InnerRadius * InnerRadius то
				если (dx * dx) > (dy * dy) то
					если dx > 0 то SetHover({0})
					иначе SetHover({2}) всё
				иначе
					если dy > 0 то SetHover({3})
					иначе SetHover({1}) всё
				всё
			иначе SetHover({}) всё
		кон PointerMove;

		проц {перекрыта}PointerUp*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		перем tdist, tx, ty : размерМЗ;
		нач
			если sent то возврат всё;
			lastX := x; lastY := y;
			dx := x - bounds.GetWidth() DIV 2 + 4;
			dy := y - bounds.GetHeight() DIV 2 + 4;
			tx := (dx * dx);
			ty := (dy * dy);
			tdist := tx + ty;

			если tdist > InnerRadius * InnerRadius то
				если (dx * dx) > (dy * dy) то
					если dx > 0 то
						если 0 в enabled то on0.Call(НУЛЬ) всё; dirNr := 0
					иначе если 2 в enabled то on2.Call(НУЛЬ) всё; dirNr := 2
					всё
				иначе
					если dy > 0 то если 3 в enabled то on3.Call(НУЛЬ) всё; dirNr := 3
					иначе если 1 в enabled то on1.Call(НУЛЬ) всё; dirNr := 1 всё
				всё
			всё;
			closeIt.Call(НУЛЬ); dirNr := -1
		кон PointerUp;

		проц {перекрыта}KeyEvent*(ucs :размерМЗ; flags : мнвоНаБитахМЗ; перем keySym : размерМЗ);
		нач
			если keySym = 0FF51H то (* Cursor Left *)
				SetHover({2}); on2.Call(НУЛЬ); dirNr := 2; closeIt.Call(НУЛЬ)
			аесли keySym = 0FF53H то (* Cursor Right *)
				SetHover({0}); on0.Call(НУЛЬ); dirNr := 0; closeIt.Call(НУЛЬ)
			аесли keySym = 0FF54H то (* Cursor Down *)
				SetHover({3}); on3.Call(НУЛЬ); dirNr := 3; closeIt.Call(НУЛЬ)
			аесли keySym = 0FF52H то (* Cursor Up *)
				SetHover({1}); on1.Call(НУЛЬ); dirNr := 1; closeIt.Call(НУЛЬ)
			аесли keySym = 0FF1BH то (* ESC *)
				SetHover({}); closeIt.Call(НУЛЬ); dirNr := -1
			всё;
		кон KeyEvent;

		проц {перекрыта}Draw*(canvas : WMGraphics.Canvas);
		перем sector, shadow : массив 2 * MenuPoints + 2 из WMGraphics.Point2d;
			i, j: размерМЗ; shadowEffect : цел32;
			x, y, dx, dy : размерМЗ;
			font : WMGraphics.Font;
		нач
			x := bounds.GetWidth() DIV 2;
			y := bounds.GetHeight() DIV 2;

			(* shadow *)
			shadowEffect := сам.shadow.Get();
			если shadowEffect > 0 то
				нцДля i := 0 до 3 делай
					GetSector(i, sector);
					нцДля j := 0 до 2 * MenuPoints +2 - 1 делай 	shadow[j].x := sector[j].x + shadowEffect; shadow[j].y := sector[j].y + shadowEffect кц;
					canvas.FillPolygonFlat(shadow, 2 * MenuPoints + 2, clShadow.Get(), WMGraphics.ModeCopy);
					нцДля j := 0 до 2 * MenuPoints +2 - 2 делай canvas.Line(shadow[j].x, shadow[j].y, shadow[j + 1].x, shadow[j + 1].y,  clShadow.Get(), WMGraphics.ModeCopy) кц;
					canvas.Line(shadow[2 * MenuPoints + 2- 1].x, shadow[2 * MenuPoints + 2 -1].y, shadow[0].x, shadow[0].y, clShadow.Get(), WMGraphics.ModeCopy)
				кц
			всё;
			(* pie *)
			нцДля i := 0 до 3 делай
				GetSector(i, sector);
				если i в enabled то
					если i в hover то
						canvas.FillPolygonFlat(sector, 2 * MenuPoints + 2, clHover.Get(), WMGraphics.ModeCopy);
					аесли ~useBgBitmap.Get() то
						canvas.FillPolygonFlat(sector, 2 * MenuPoints + 2, clDefault.Get(), WMGraphics.ModeCopy)
					всё;
				иначе
					canvas.FillPolygonFlat(sector, 2 * MenuPoints + 2, цел32(0CCCC0030H), WMGraphics.ModeCopy)
				всё;
				если ~useBgBitmap.Get() то
					нцДля j := 0 до 2 * MenuPoints +2 - 2 делай canvas.Line(sector[j].x, sector[j].y, sector[j + 1].x, sector[j + 1].y,  clLine.Get(), WMGraphics.ModeCopy) кц;
					canvas.Line(sector[2 * MenuPoints + 2- 1].x, sector[2 * MenuPoints + 2 -1].y, sector[0].x, sector[0].y, clLine.Get(), WMGraphics.ModeCopy)
				всё
			кц;
			(* background image *)
			если useBgBitmap.Get() и (bgBitmap # НУЛЬ) то
				canvas.DrawImage(26, 26, bgBitmap, WMGraphics.ModeSrcOverDst)
			всё;
			(* caption *)
			font := WMGraphics.GetFont("Oberon", 14, {0}); canvas.SetColor(0FFH); canvas.SetFont(font);
			если images[0] # НУЛЬ то
				canvas.DrawImage(x + (InnerRadius + OuterRadius) DIV 2 - images[0].width DIV 2, y - images[0].height DIV 2, images[0], WMGraphics.ModeSrcOverDst)
			аесли texts[0] # НУЛЬ то
				font.GetStringSize(texts[0]^, dx, dy);
				canvas.DrawString(x-4 + (InnerRadius + OuterRadius) DIV 2 - dx DIV 2, y+11 - dy DIV 2, texts[0]^)
			всё;
			если images[1] # НУЛЬ то
				canvas.DrawImage(x - images[1].width DIV 2, y - (InnerRadius + OuterRadius) DIV 2 - images[1].height DIV 2, images[1], WMGraphics.ModeSrcOverDst)
			аесли texts[1] # НУЛЬ то
				font.GetStringSize(texts[1]^, dx, dy);
				canvas.DrawString(x-4 - dx DIV 2, y+11 - (InnerRadius + OuterRadius) DIV 2 - dy DIV 2, texts[1]^)
			всё;
			если images[2] # НУЛЬ то
				canvas.DrawImage(x - (InnerRadius + OuterRadius) DIV 2 - images[2].width DIV 2, y - images[2].height DIV 2, images[2], WMGraphics.ModeSrcOverDst)
			аесли texts[2] # НУЛЬ то
				font.GetStringSize(texts[2]^, dx, dy);
				canvas.DrawString(x-4 - (InnerRadius + OuterRadius) DIV 2 - dx DIV 2, y+11 - dy DIV 2, texts[2]^)
			всё;
			если images[3] # НУЛЬ то
				canvas.DrawImage(x - images[3].width DIV 2, y + (InnerRadius + OuterRadius) DIV 2 - images[3].height DIV 2, images[3],WMGraphics.ModeSrcOverDst)
			аесли texts[3] # НУЛЬ то
				font.GetStringSize(texts[3]^, dx, dy);
				canvas.DrawString(x-4 - dx DIV 2, y+11 + (InnerRadius + OuterRadius) DIV 2 - dy DIV 2, texts[3]^)
			всё;
		кон Draw;
	кон PieMenu;

	Menu* = окласс (WMComponents.FormWindow)
	перем piemenu: PieMenu;
		on0-, on1-, on2-, on3- , onClose-: WMEvents.EventSource;
		lastX*, lastY* : размерМЗ;
		caller : WMWindowManager.Window;
		pointerReturned, bt* : булево;
		userData* : динамическиТипизированныйУкль;
		shown : булево;

		проц CreateForm(): WMComponents.VisualComponent;
		нач
			нов(piemenu); piemenu.bounds.SetExtents(230, 230);
			нов(onClose, сам, НУЛЬ, НУЛЬ, НУЛЬ);
			on0 := piemenu.on0;
			on1 := piemenu.on1;
			on2 := piemenu.on2;
			on3 := piemenu.on3;
			onClose := piemenu.closeIt;
			piemenu.fillColor.Set(цел32(0FFFFFF80H)); piemenu.takesFocus.Set(истина);
			onClose.Add(CloseIt);
			возврат piemenu;
		кон CreateForm;

		проц {перекрыта}FocusLost*;
		нач
			FocusLost^;
			Close;
		кон FocusLost;

		проц CloseIt(sender, data : динамическиТипизированныйУкль);
		нач
			Close
		кон CloseIt;

		проц &New*;
		перем vc : WMComponents.VisualComponent;
		нач
			(* To create a multi language app, try loading the respective XML instead of CreateForm()
			if the XML was not found or does not contain all needed elements, use CreateForm as fallback *)
			vc := CreateForm();

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), истина);
			SetContent(vc);

			pointerThreshold := 100
		кон New;

		проц SetImage*(nr : цел32; image : WMGraphics.Image);
		нач
			piemenu.SetImage(nr, image);
		кон SetImage;

		проц SetText*(nr: цел32; text: Strings.String);
		нач
			piemenu.SetText(nr, text);
		кон SetText;

		проц SetEnabled*(enabled : мнвоНаБитахМЗ);
		нач
			piemenu.SetEnabled(enabled)
		кон SetEnabled;

		проц Show*(caller : WMWindowManager.Window; x, y : размерМЗ; bt : булево);
		перем nm : WMMessages.Message;
		нач
			если ~shown то shown := истина;
				сам.bt := bt;
				lastX := x - 50;
				lastY :=  y - 50;
				сам.caller := caller;
				pointerReturned := ложь;
				manager := WMWindowManager.GetDefaultManager();
				(* simulate a pointer movement so the component system is not "surprised" when suddenly
					a pressed mouse move message occurs. The component system builds up its internal owner
					chain and will forward following drag messages to the component hit with this message *)
				nm.msgType := WMMessages.MsgPointer;
				nm.msgSubType := WMMessages.MsgSubPointerMove;
				nm.x := 100; nm.y := 100;
				Handle(nm);
				manager.Add(x - piemenu.bounds.GetWidth() DIV 2, y - piemenu.bounds.GetWidth() DIV 2, сам, {WMWindowManager.FlagHidden, WMWindowManager.FlagStayOnTop});
				если manager.TransferPointer(сам) то всё;
				manager.SetFocus(сам)
			всё;
		кон Show;

		проц {перекрыта}Close*;
		нач
			Close^;
			shown := ложь;
		кон Close;

	кон Menu;

перем
	ColorPrototype, ProtoPmClDefault, ProtoPmClHover, ProtoPmClShadow, ProtoPmClLine : WMProperties.ColorProperty;
	Int32Prototype, ProtoPmShadow : WMProperties.Int32Property;
	BooleanPrototype, ProtoPmUseBgBitmap : WMProperties.BooleanProperty;
	StringPrototype, ProtoPmBgBitmapName : WMProperties.StringProperty;

	StrPieMenu : Strings.String;

проц InitStrings;
нач
	StrPieMenu := Strings.NewString("PieMenu");
кон InitStrings;

проц InitPrototypes;
перем plPieMenu : WMProperties.PropertyList;
нач
	нов(plPieMenu); WMComponents.propertyListList.Add("PieMenu", plPieMenu);
	(* use background bitmap *)
	нов(BooleanPrototype, НУЛЬ, Strings.NewString("UseBgBitmap"), Strings.NewString("Use background bitmap")); BooleanPrototype.Set(ложь);
	нов(ProtoPmUseBgBitmap, BooleanPrototype, НУЛЬ, НУЛЬ); plPieMenu.Add(ProtoPmUseBgBitmap);
	(* background bitmap *)
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgBitmap"), Strings.NewString("Name of the background bitmap")); StringPrototype.Set(Strings.NewString(""));
	нов(ProtoPmBgBitmapName, StringPrototype, НУЛЬ, НУЛЬ); plPieMenu.Add(ProtoPmBgBitmapName);
	(* colors *)
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClDefault"), Strings.NewString("Default color")); ColorPrototype.Set(цел32(0CCCC0080H));
	нов(ProtoPmClDefault, ColorPrototype, НУЛЬ, НУЛЬ); plPieMenu.Add(ProtoPmClDefault);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClHover"), Strings.NewString("Mouseover color")); ColorPrototype.Set(цел32(0CC880080H));
	нов(ProtoPmClHover , ColorPrototype, НУЛЬ, НУЛЬ); plPieMenu.Add(ProtoPmClHover);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClShadow"), Strings.NewString("Shadow color")); ColorPrototype.Set(80H);
	нов(ProtoPmClShadow, ColorPrototype, НУЛЬ, НУЛЬ); plPieMenu.Add(ProtoPmClShadow);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClLine"), Strings.NewString("Line color")); ColorPrototype.Set(80H);
	нов(ProtoPmClLine, ColorPrototype, НУЛЬ, НУЛЬ); plPieMenu.Add(ProtoPmClLine);
	(* shadow *)
	нов(Int32Prototype, НУЛЬ, Strings.NewString("UseShadow"), Strings.NewString("Draw a shadow or not")); Int32Prototype.Set(0);
	нов(ProtoPmShadow, Int32Prototype, НУЛЬ, НУЛЬ); plPieMenu.Add(ProtoPmShadow);
кон InitPrototypes;

нач
	InitStrings;
	InitPrototypes;
кон WMPieMenu.

