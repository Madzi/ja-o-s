модуль WMPerfMonPluginVNCServer; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor plugin for VNC server"; *)
(**
 * History:
 *
 *	27.02.2007	First release (staubesv)
 *)

использует
	WMPerfMonPlugins, Modules, VNCServer;

конст
	ModuleName = "WMPerfMonPluginVNCServer";

тип

	VncStats= окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "VNCServer"; p.description := "Performance Monitor plugin for VNC server";
			p.modulename := ModuleName;
			p.autoMin := ложь; p.autoMax := истина; p.minDigits := 7;

			нов(ds, 7);
			ds[0].name := "NnofAuthenticate";
			ds[1].name := "NnofAuthNone";
			ds[2].name := "NnofAuthVNC";
			ds[3].name := "NnofAuthOk";
			ds[4].name := "NnofAuthFailed";
			ds[5].name := "NnofEnteredServe";
			ds[6].name := "NnofLeftServer";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := VNCServer.NnofAuthenticate;
			dataset[1] := VNCServer.NnofAuthNone;
			dataset[2] := VNCServer.NnofAuthVNC;
			dataset[3] := VNCServer.NnofAuthOk;
			dataset[4] := VNCServer.NnofAuthFailed;
			dataset[5] := VNCServer.NnofEnteredServe;
			dataset[6] := VNCServer.NnofLeftServer;
		кон UpdateDataset;

	кон VncStats;

проц Install*;
кон Install;

проц InitPlugin;
перем par : WMPerfMonPlugins.Parameter; plugin : VncStats;
нач
	нов(par); нов(plugin, par);
кон InitPlugin;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	InitPlugin;
кон WMPerfMonPluginVNCServer.

WMPerfMonPluginVNCServer.Install ~	System.Free WMPerfMonPluginVNCServer ~
