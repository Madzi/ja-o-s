модуль WMTrapWriter; (** AUTHOR "fof"; PURPOSE "trap window writer"; *)

использует
	Modules, Потоки, TrapWriters, WMGraphics, WMUtilities;

перем w: WMUtilities.WindowWriter;

проц TrapWriterFactory*(): Потоки.Писарь;
нач
	если (w = НУЛЬ) или (~w.IsVisible()) то
		нов(w, "TRAPS", 860, 480, ложь);
		w.SetWindowBgColor(WMGraphics.DarkRed);
		w.SetWindowIcon(WMGraphics.LoadImage("WMIcons.tar://WMTrapWriter.png", истина));
		w.SetFontName("VeraMo");
		w.SetFontSize(12);
		w.SetFontColor(WMGraphics.Gold);
		w.window.editor.SetWordWrap(истина);
	всё;
	возврат w;
кон TrapWriterFactory;

проц Install*;
нач
	TrapWriters.InstallTrapWriterFactory (TrapWriterFactory)
кон Install;

проц Uninstall*;
нач
	TrapWriters.UninstallTrapWriterFactory (TrapWriterFactory)
кон Uninstall;

проц HaltTest*;
нач
	СТОП(100);
кон HaltTest;

проц СотриТекстИзОкна*;
нач
  если w # НУЛЬ то
  		w.window.editor.Clear() всё кон СотриТекстИзОкна;

проц Cleanup;
нач
	Uninstall;
	если (w # НУЛЬ) то w.Close; всё;
кон Cleanup;

нач
	w := НУЛЬ;
	Modules.InstallTermHandler(Cleanup);
кон WMTrapWriter.

System.Free WMTrapWriter WMUtilities ~

WMTrapWriter.Install ~
WMTrapWriter.Uninstall ~
WMTrapWriter.HaltTest ~
WMTrapWriter.СотриТекстИзОкна ~
 
