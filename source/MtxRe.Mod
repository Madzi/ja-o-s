(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль MtxRe;   (** AUTHOR "fof"; PURPOSE "Matrix objects of type Real."; *)
(** caution:
since common indexing order in linear algebra
is row then column, the x coordinate corresponds to row
and y coordinate corresponds to column in this module.
Do not assume x to be horizontal and y to be vertical for
matrices when you use it for linear algebraic computation.
*)


использует НИЗКОУР, NbrInt, ArrayXdBytes, ArrayXd := ArrayXdRe, Array1d := Array1dRe, DataErrors, Vec := VecRe, NbrRat, MtxInt, MtxRat, DataIO, NbrRe;

конст
	(** The version number used when reading/writing a matrix to file. *)
	VERSION* = 1;

тип
	Value* = ArrayXd.Value;  Index* = цел32;  IntValue = ArrayXd.IntValue;  RatValue = NbrRat.Rational;
	Array* = ArrayXd.Array2;  Map* = ArrayXd.Map;

	Matrix* = окласс (ArrayXd.Array)
	перем lenx-, leny-: цел32;
		rows-, cols-: цел32;   (* lenx = nr.Rows, leny = nr.Columns *)
	перем ox-, oy-: цел32;
		Get-: проц {делегат} ( x, y: Index ): Value;

		(* override *)
		проц {перекрыта}AlikeX*( ): ArrayXdBytes.Array;
		перем copy: Matrix;
		нач
			нов( copy, origin[0], len[0], origin[1], len[1] );  возврат copy;
		кон AlikeX;

		проц {перекрыта}NewRangeX*( neworigin, newlen: ArrayXdBytes.IndexArray;  copydata: булево );
		нач
			если длинаМассива( newlen ) # 2 то СТОП( 1001 ) всё;
			NewRangeX^( neworigin, newlen, copydata );
		кон NewRangeX;

		проц {перекрыта}ValidateCache*;
		нач
			ValidateCache^;
			если dim # 2 то СТОП( 100 ) всё;
			lenx := len[0];  leny := len[1];  ox := origin[0];  oy := origin[1];  rows := lenx;  cols := leny;
		кон ValidateCache;

		проц {перекрыта}SetBoundaryCondition*( c: цел8 );   (* called by new, load and directly *)
		нач
			SetBoundaryCondition^( c );
			просей c из
			ArrayXd.StrictBoundaryC:
					Get := Get2;
			| ArrayXd.AbsorbingBoundaryC:
					Get := Get2BAbsorbing;
			| ArrayXd.PeriodicBoundaryC:
					Get := Get2BPeriodic;
			| ArrayXd.SymmetricOnBoundaryC:
					Get := Get2BSymmetricOnB
			| ArrayXd.SymmetricOffBoundaryC:
					Get := Get2BSymmetricOffB
			| ArrayXd.AntisymmetricOnBoundaryC:
					Get := Get2BAntisymmetricOnB
			| ArrayXd.AntisymmetricOffBoundaryC:
					Get := Get2BAntisymmetricOffB
			всё;
		кон SetBoundaryCondition;

	(** new*)
		проц & New*( ox, rowsORw, oy, colsORh: цел32 );
		нач
			NewXdB( ArrayXdBytes.Array2( ox, oy ), ArrayXdBytes.Array2( rowsORw, colsORh ) );
		кон New;

		проц Alike*( ): Matrix;
		перем copy: ArrayXdBytes.Array;
		нач
			copy := AlikeX();  возврат copy( Matrix );
		кон Alike;

		проц NewRange*( ox, rowsORw, oy, colsORh: цел32;  copydata: булево );
		нач
			если (rowsORw # len[0]) или (colsORh # len[1]) или (ox # origin[0]) или (oy # origin[1]) то
				NewRangeX^( ArrayXdBytes.Array2( ox, oy ), ArrayXdBytes.Array2( rowsORw, colsORh ), copydata )
			всё;
		кон NewRange;

		проц Copy*( ): Matrix;
		перем res: ArrayXdBytes.Array;
		нач
			res := CopyX();  возврат res( Matrix );
		кон Copy;

		проц Set*( rowORx (**= row or x coordinate*) , colORy (**= column or y coordinate *) : Index;  v: Value );
		нач
			ArrayXdBytes.Set2( сам, rowORx, colORy, v );
		кон Set;

	(** Exchanges values held by  mtx[row1, k]  and  mtx[row2, k],  0 # k < cols. *)
		проц SwapRows*( row1, row2: Index );
		нач
			ToggleElements( 0, row1, row2 )
		кон SwapRows;

	(** Exchanges values held by  mtx[k, col1]  and  mtx[k, col2],  0 # k < rows. *)
		проц SwapColumns*( col1, col2: Index );
		нач
			ToggleElements( 1, col1, col2 )
		кон SwapColumns;

		проц Transpose*;
		нач
			PermuteDimensions( ArrayXdBytes.Array2( 1, 0 ), истина );
		кон Transpose;


	(** Stores mtx := mtx * x  which requires  x  to be square and to have dimension  cols X cols *)

		проц Dot*( x: Matrix );
		перем res: Matrix;
		нач
			если x # НУЛЬ то
				если x.len[0] = x.len[1] то
					если len[0] # len[1] то
						если len[0] = x.len[0] то
							res := сам * x;  ArrayXdBytes.CopyArrayPartToArrayPart( res, сам, res.origin, res.len, origin, len );
						иначе DataErrors.Error( "The two matrices were not compatible" )
						всё;
					иначе DataErrors.Error( "This  matrix in not square." )
					всё
				иначе DataErrors.Error( "The supplied matrix was not square." )
				всё
			иначе DataErrors.Error( "The supplied matrix to be doted was NIL." )
			всё
		кон Dot;

	(** Stores mtx := mtxT * x  which requires both matrices to be square and have equal dimensions  *)
		проц LeftDot*( x: Matrix );
		нач
			Transpose;  Dot( x );
		кон LeftDot;

	(** Stores mtx := mtx * xT  which requires both matrices to be square and have equal dimensions  *)
		проц RightDot*( x: Matrix );
		нач
			x.Transpose;  Dot( x );  x.Transpose;
		кон RightDot;

		проц Row*( row: Index ): Vec.Vector;
		перем v: Vec.Vector;
		нач
			нов( v, 0, len[1] );  ArrayXd.CopyMtxToVec( сам, v, 1, row, 0, 0, len[1] );  возврат v;
		кон Row;

		проц InsertRow*( at: Index );
		нач
			InsertElements( 0, at, 1 ) (* insert elements in each  col *)
		кон InsertRow;

		проц DeleteRow*( x: Index );
		нач
			DeleteElements( 0, x, 1 );   (* delete elements from each  col *)
		кон DeleteRow;

		проц Col*( col: Index ): Vec.Vector;
		перем v: Vec.Vector;
		нач
			нов( v, 0, len[0] );  ArrayXd.CopyMtxToVec( сам, v, 0, 0, col, 0, len[0] );  возврат v;
		кон Col;

		проц InsertCol*( at: Index );
		нач
			InsertElements( 1, at, 1 )
		кон InsertCol;

		проц DeleteCol*( x: Index );
		нач
			DeleteElements( 1, x, 1 );
		кон DeleteCol;

	(** copy methods using the current boundary condition SELF.bc*)
		проц CopyToVec*( dest: ArrayXd.Array;  dim: Index;  srcx, srcy, destx, len: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 1) то СТОП( 1002 ) всё;
			slen := ArrayXdBytes.Index2( 1, 1 );  slen[dim] := len;
			CopyToArray( dest, ArrayXdBytes.Index2( srcx, srcy ), slen, ArrayXdBytes.Index1( destx ), ArrayXdBytes.Index1( len ) );
		кон CopyToVec;

		проц CopyToMtx*( dest: ArrayXd.Array;  srcx, srcy, destx, desty, lenx, leny: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 2) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index2( lenx, leny );
			CopyToArray( dest, ArrayXdBytes.Index2( srcx, srcy ), slen, ArrayXdBytes.Index2( destx, desty ), slen );
		кон CopyToMtx;

		проц CopyToCube*( dest: ArrayXd.Array;  dimx, dimy: Index;  srcx, srcy, destx, desty, destz, lenx, leny: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 3) или (dimx >= dimy) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index3( 1, 1, 1 );  slen[dimx] := lenx;  slen[dimy] := leny;
			CopyToArray( dest, ArrayXdBytes.Index2( srcx, srcy ), ArrayXdBytes.Index2( lenx, leny ),
								   ArrayXdBytes.Index3( destx, desty, destz ), slen );
		кон CopyToCube;

		проц CopyToHCube*( dest: ArrayXd.Array;  dimx, dimy: Index;
													  srcx, srcy, destx, desty, destz, destt, lenx, leny: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 4) или (dimx >= dimy) то СТОП( 1005 ) всё;
			slen := ArrayXdBytes.Index4( 1, 1, 1, 1 );  slen[dimx] := lenx;  slen[dimy] := leny;
			CopyToArray( dest, ArrayXdBytes.Index2( srcx, srcy ), ArrayXdBytes.Index2( lenx, leny ),
								   ArrayXdBytes.Index4( destx, desty, destz, destt ), slen );
		кон CopyToHCube;

		проц CopyTo1dArray*( перем dest: массив из Value;  sx, sy, slenx, sleny: Index;  dpos, dlen: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 1, ArrayXdBytes.Index1( 0 ), ArrayXdBytes.Index1( длинаМассива( dest ) ), размер16_от( Value ),
																			  адресОт( dest[0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index2( sx, sy ), ArrayXdBytes.Index2( slenx, sleny ),
																  ArrayXdBytes.Index1( dpos ), ArrayXdBytes.Index1( dlen ) );
		кон CopyTo1dArray;

		проц CopyTo2dArray*( перем dest: массив из массив из Value;  sx, sy, slenx, sleny: Index;  dposx, dposy, dlenx, dleny: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 2, ArrayXdBytes.Index2( 0, 0 ), ArrayXdBytes.Index2( длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ),
																			  размер16_от( Value ), адресОт( dest[0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index2( sx, sy ), ArrayXdBytes.Index2( slenx, sleny ),
																  ArrayXdBytes.Index2( dposx, dposy ), ArrayXdBytes.Index2( dlenx, dleny ) );
		кон CopyTo2dArray;

		проц CopyTo3dArray*( перем dest: массив из массив из массив из Value;  sx, sy, slenx, sleny: Index;
													   dposx, dposy, dposz, dlenx, dleny, dlenz: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 3, ArrayXdBytes.Index3( 0, 0, 0 ),
																			  ArrayXdBytes.Index3( длинаМассива( dest, 2 ), длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ), размер16_от( Value ),
																			  адресОт( dest[0, 0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index2( sx, sy ), ArrayXdBytes.Index2( slenx, sleny ),
																  ArrayXdBytes.Index3( dposx, dposy, dposz ),
																  ArrayXdBytes.Index3( dlenx, dleny, dlenz ) );
		кон CopyTo3dArray;

		проц CopyTo4dArray*( перем dest: массив из массив из массив из массив из Value;  sx, sy, slenx, sleny: Index;
													   dposx, dposy, dposz, dpost, dlenx, dleny, dlenz, dlent: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 4, ArrayXdBytes.Index4( 0, 0, 0, 0 ),
																			  ArrayXdBytes.Index4( длинаМассива( dest, 3 ), длинаМассива( dest, 2 ), длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ), размер16_от( Value ),
																			  адресОт( dest[0, 0, 0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index2( sx, sy ), ArrayXdBytes.Index2( slenx, sleny ),
																  ArrayXdBytes.Index4( dposx, dposy, dposz, dpost ),
																  ArrayXdBytes.Index4( dlenx, dleny, dlenz, dlent ) );
		кон CopyTo4dArray;

	(** copy from without boundary conditions *)
		проц CopyFrom1dArray*( перем src: массив из Value;  spos, slen: Index;  dx, dy, dlenx, dleny: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 1, ArrayXdBytes.Index1( 0 ), ArrayXdBytes.Index1( длинаМассива( src ) ), размер16_от( Value ),
																			  адресОт( src[0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index1( spos ), ArrayXdBytes.Index1( slen ),
																			   ArrayXdBytes.Index2( dx, dy ), ArrayXdBytes.Index2( dlenx, dleny ) );
		кон CopyFrom1dArray;

		проц CopyFrom2dArray*( перем src: массив из массив из Value;  sposx, spoxy, slenx, sleny: Index;
														    dx, dy, dlenx, dleny: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 2, ArrayXdBytes.Index2( 0, 0 ), ArrayXdBytes.Index2( длинаМассива( src, 1 ), длинаМассива( src, 0 ) ),
																			  размер16_от( Value ), адресОт( src[0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index2( sposx, spoxy ),
																			   ArrayXdBytes.Index2( slenx, sleny ), ArrayXdBytes.Index2( dx, dy ),
																			   ArrayXdBytes.Index2( dlenx, dleny ) );
		кон CopyFrom2dArray;

		проц CopyFrom3dArray*( перем src: массив из массив из массив из Value;  sposx, spoxy, sposz, slenx, sleny, slenz: Index;
														    dx, dy, dlenx, dleny: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 3, ArrayXdBytes.Index3( 0, 0, 0 ),
																			  ArrayXdBytes.Index3( длинаМассива( src, 2 ), длинаМассива( src, 1 ), длинаМассива( src, 0 ) ), размер16_от( Value ),
																			  адресОт( src[0, 0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index3( sposx, spoxy, sposz ),
																			   ArrayXdBytes.Index3( slenx, sleny, slenz ), ArrayXdBytes.Index2( dx, dy ),
																			   ArrayXdBytes.Index2( dlenx, dleny ) );
		кон CopyFrom3dArray;

		проц CopyFrom4dArray*( перем src: массив из массив из массив из массив из Value;
														    sposx, spoxy, sposz, spost, slenx, sleny, slenz, slent: Index;  dx, dy, dlenx, dleny: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 4, ArrayXdBytes.Index4( 0, 0, 0, 0 ),
																			  ArrayXdBytes.Index4( длинаМассива( src, 3 ), длинаМассива( src, 2 ), длинаМассива( src, 1 ), длинаМассива( src, 0 ) ), размер16_от( Value ),
																			  адресОт( src[0, 0, 0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index4( sposx, spoxy, sposz, spost ),
																			   ArrayXdBytes.Index4( slenx, sleny, slenz, slent ),
																			   ArrayXdBytes.Index2( dx, dy ), ArrayXdBytes.Index2( dlenx, dleny ) );
		кон CopyFrom4dArray;

	кон Matrix;

	проц FrobeniusNorm*( m: Matrix ): NbrRe.Real;
	нач
		возврат Array1d.L2Norm( m.data^, 0, длинаМассива( m.data ) );
	кон FrobeniusNorm;


	проц Transpose*( u: Matrix ): Matrix;
	перем res: Matrix;
	нач
		res := u.Copy();  res.Transpose;  возврат res;
	кон Transpose;

	операция ":="*( перем l: Matrix;  перем r: массив из массив из Value );
	нач
		(*		IF r = NIL THEN l := NIL;  RETURN END;  *)
		если l = НУЛЬ то нов( l, 0, длинаМассива( r, 1 ), 0, длинаМассива( r, 0 ) ) иначе l.NewRange( 0, длинаМассива( r, 1 ), 0, длинаМассива( r, 0 ), ложь );  всё;

		ArrayXdBytes.CopyMemoryToArrayPart( адресОт( r[0, 0] ), l, длинаМассива( r, 1 ) * длинаМассива( r, 0 ), НУЛЬ , НУЛЬ );
	кон ":=";

	операция ":="*( перем l: Matrix;  r: Vec.Vector );
	нач
		(*		IF r = NIL THEN l := NIL;  RETURN END;  *)
		если l = НУЛЬ то нов( l, 0, 1, 0, r.len[0] ) иначе l.NewRange( 0, 1, 0, r.len[0], ложь );  всё;
		ArrayXdBytes.CopyDataRaw( r, l );
	кон ":=";

	операция ":="*( перем l: Matrix;  r: MtxInt.Matrix );
	перем i, last: цел32;
	нач
		если r = НУЛЬ то l := НУЛЬ иначе
			если l = НУЛЬ то нов( l, r.origin[0], r.len[0], r.origin[1], r.len[1] );  всё;
			last := длинаМассива( r.data ) - 1;
			нцДля i := 0 до last делай l.data[i] := r.data[i];  кц;
		всё;
	кон ":=";

	операция ":="*( перем l: Matrix;  r: MtxRat.Matrix );
	перем i, last: цел32;
	нач
		если r = НУЛЬ то l := НУЛЬ иначе
			если l = НУЛЬ то нов( l, r.origin[0], r.len[0], r.origin[1], r.len[1] );  всё;
			last := длинаМассива( r.data ) - 1;
			нцДля i := 0 до last делай l.data[i] := r.data[i];  кц;
		всё;
	кон ":=";

	операция ":="*( перем l: Matrix;  r: Value );
	нач
		если l # НУЛЬ то ArrayXd.Fill( l, r );  всё;
	кон ":=";

	операция ":="*( перем l: Matrix;  r: RatValue );
	перем r1: Value;
	нач
		r1 := r;  l := r1;
	кон ":=";

	операция ":="*( перем l: Matrix;  r: IntValue );
	перем r1: Value;
	нач
		r1 := r;  l := r1;
	кон ":=";

	операция "+"*( l, r: Matrix ): Matrix;
	перем res: Matrix;
	нач
		res := l.Alike();  ArrayXd.Add( l, r, res );  возврат res;
	кон "+";

	операция "-"*( l, r: Matrix ): Matrix;
	перем res: Matrix;
	нач
		res := l.Alike();  ArrayXd.Sub( l, r, res );  возврат res;
	кон "-";

	операция "+"*( l: Matrix;  r: Value ): Matrix;
	перем res: Matrix;
	нач
		res := l.Alike();  ArrayXd.AddAV( l, r, res );  возврат res;
	кон "+";

	операция "+"*( l: Matrix;  r: RatValue ): Matrix;
	перем res: Matrix;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.AddAV( l, r1, res );  возврат res;
	кон "+";

	операция "+"*( l: Matrix;  r: IntValue ): Matrix;
	перем res: Matrix;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.AddAV( l, r1, res );  возврат res;
	кон "+";

	операция "+"*( l: Value;  r: Matrix ): Matrix;
	нач
		возврат r + l
	кон "+";

	операция "+"*( l: RatValue;  r: Matrix ): Matrix;
	нач
		возврат r + l
	кон "+";

	операция "+"*( l: IntValue;  r: Matrix ): Matrix;
	нач
		возврат r + l
	кон "+";

	операция "-"*( l: Matrix;  r: Value ): Matrix;
	перем res: Matrix;
	нач
		res := l.Alike();  ArrayXd.SubAV( l, r, res );  возврат res;
	кон "-";

	операция "-"*( l: Matrix;  r: RatValue ): Matrix;
	перем res: Matrix;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.SubAV( l, r1, res );  возврат res;
	кон "-";

	операция "-"*( l: Matrix;  r: IntValue ): Matrix;
	перем res: Matrix;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.SubAV( l, r1, res );  возврат res;
	кон "-";

	операция "-"*( l: Value;  r: Matrix ): Matrix;
	перем res: Matrix;
	нач
		res := r.Alike();  ArrayXd.SubVA( l, r, res );  возврат res;
	кон "-";

	операция "-"*( l: RatValue;  r: Matrix ): Matrix;
	перем res: Matrix;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.SubVA( l1, r, res );  возврат res;
	кон "-";

	операция "-"*( l: IntValue;  r: Matrix ): Matrix;
	перем res: Matrix;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.SubVA( l1, r, res );  возврат res;
	кон "-";

	операция "-"*( l: Matrix ): Matrix;
	нач
		возврат 0 - l;
	кон "-";

	операция "*"*( l: Matrix;  r: Value ): Matrix;
	перем res: Matrix;
	нач
		res := l.Alike();  ArrayXd.MulAV( l, r, res );  возврат res;
	кон "*";

	операция "*"*( l: Matrix;  r: RatValue ): Matrix;
	перем res: Matrix;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.MulAV( l, r1, res );  возврат res;
	кон "*";

	операция "*"*( l: Matrix;  r: IntValue ): Matrix;
	перем res: Matrix;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.MulAV( l, r1, res );  возврат res;
	кон "*";

	операция "*"*( l: Value;  r: Matrix ): Matrix;
	нач
		возврат r * l;
	кон "*";

	операция "*"*( l: RatValue;  r: Matrix ): Matrix;
	нач
		возврат r * l;
	кон "*";

	операция "*"*( l: IntValue;  r: Matrix ): Matrix;
	нач
		возврат r * l;
	кон "*";

	операция "/"*( l: Matrix;  r: Value ): Matrix;
	перем res: Matrix;
	нач
		res := l.Alike();  ArrayXd.DivAV( l, r, res );  возврат res;
	кон "/";

	операция "/"*( l: Matrix;  r: RatValue ): Matrix;
	перем res: Matrix;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.DivAV( l, r1, res );  возврат res;
	кон "/";

	операция "/"*( l: Matrix;  r: IntValue ): Matrix;
	перем res: Matrix;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.DivAV( l, r1, res );  возврат res;
	кон "/";

	операция "/"*( l: Value;  r: Matrix ): Matrix;
	перем res: Matrix;
	нач
		res := r.Alike();  ArrayXd.DivVA( l, r, res );  возврат res;
	кон "/";

	операция "/"*( l: RatValue;  r: Matrix ): Matrix;
	перем res: Matrix;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.DivVA( l1, r, res );  возврат res;
	кон "/";

	операция "/"*( l: IntValue;  r: Matrix ): Matrix;
	перем res: Matrix;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.DivVA( l1, r, res );  возврат res;
	кон "/";


(*
	OPERATOR "MOD"*( l: Matrix;  r: Value ): Matrix;
	VAR res: Matrix;
	BEGIN
		res := l.Alike();  ArrayXd.ModAV( l, r, res );  RETURN res;
	END "MOD";

	OPERATOR "MOD"*( l: Value;  r: Matrix ): Matrix;
	VAR res: Matrix;
	BEGIN
		res := r.Alike();  ArrayXd.ModVA( l, r, res );  RETURN res;
	END "MOD";
*)

	операция "*"*( l: Vec.Vector;  r: Matrix ): Vec.Vector;
	перем res: Vec.Vector;  rc, rr, lc, i, j: цел32;  sum: Value;
	нач
		rc := r.cols;  rr := r.rows;  lc := l.lenx;

		если lc # rr то DataErrors.Error( "The supplied matrix / vector were incompatible." );  СТОП( 100 );
		иначе
			нов( res, 0, rc );
			нцДля i := 0 до rc - 1 делай  (* right columns *)
				sum := 0;
				нцДля j := 0 до lc - 1 делай sum := sum + l.Get( j ) * r.Get( j, i );  кц;
				res.Set( i, sum );
			кц;
		всё;
		возврат res;
	кон "*";

	операция "*"*( l: Matrix;  r: Vec.Vector ): Vec.Vector;
	перем res: Vec.Vector;  lr, lc, rr, i, j: цел32;  sum: Value;
	нач
		lr := l.rows;  lc := l.cols;  rr := r.lenx;
		если lc # rr то DataErrors.Error( "The supplied matrix / vector were incompatible." );  СТОП( 100 );
		иначе
			нов( res, 0, lr );
			нцДля i := 0 до lr - 1 делай
				sum := 0;
				нцДля j := 0 до lc - 1 делай sum := sum + l.Get( i, j ) * r.Get( j );  кц;
				res.Set( i, sum );
			кц;
		всё;
		возврат res;
	кон "*";

	операция "*"*( l, r: Matrix ): Matrix;
	перем res: Matrix;  rr, rc, lr, lc, i, j, k: цел32;  sum: Value;
		(**! far from opimal: use internal routines of ArrayXdBytes *)
	нач
		rc := r.cols;   (* columns of right matrix *)
		rr := r.rows;   (* rows of right matrix *)
		lr := l.rows;  lc := l.cols;
		если lc # rr то DataErrors.Error( "The supplied matrices were incompatible." );  СТОП( 100 )
		иначе
			нов( res, 0, lr, 0, rc );

			нцДля i := 0 до lr - 1 делай  (* left rows*)
				нцДля j := 0 до rc - 1 делай  (* right columns *)
					sum := 0;
					нцДля k := 0 до lc - 1 (* = rr -1 *) делай sum := sum + l.Get( i, k ) * r.Get( k, j );  кц;
					res.Set( i, j, sum );
				кц;
			кц;
		всё;
		возврат res;
	кон "*";

(* The procedures needed to register type Matrix so that its instances can be made persistent. *)
	проц LoadMatrix( R: DataIO.Reader;  перем obj: окласс );
	перем a: Matrix;  version: цел8;  ver: NbrInt.Integer;
	нач
		R.чЦел8_мз( version );
		если version = -1 то
			obj := НУЛЬ  (* Version tag is -1 for NIL. *)
		аесли version = VERSION то нов( a, 0, 0, 0, 0 );  a.Read( R );  obj := a
		иначе  (* Encountered an unknown version number. *)
			ver := version;  DataErrors.IntError( ver, "Alien version number encountered." );  СТОП( 1000 )
		всё
	кон LoadMatrix;

	проц StoreMatrix( W: DataIO.Writer;  obj: окласс );
	перем a: Matrix;
	нач
		если obj = НУЛЬ то W.пЦел8_мз( -1 ) иначе W.пЦел8_мз( VERSION );  a := obj( Matrix );  a.Write( W ) всё
	кон StoreMatrix;

	проц Register;
	перем a: Matrix;
	нач
		нов( a, 0, 0, 0, 0 );  DataIO.PlugIn( a, LoadMatrix, StoreMatrix )
	кон Register;

(** Load and Store are procedures for external use that read/write an instance of Matrix from/to a file. *)
	проц Load*( R: DataIO.Reader;  перем obj: Matrix );
	перем ptr: окласс;
	нач
		R.Object( ptr );  obj := ptr( Matrix )
	кон Load;

	проц Store*( W: DataIO.Writer;  obj: Matrix );
	нач
		W.Object( obj )
	кон Store;

нач
	Register
кон MtxRe.
