модуль UsbMouse;  (** AUTHOR "staubesv"; PURPOSE "USB Mouse Driver"; *)
(**
 * Bluebottle USB Mouse Driver (HID boot protocol)
 *
 * Usage:
 *	UsbMouse.Install ~ loads this driver
 *	System.Free UsbMouse ~ unloads this driver
 *
 *	The HID boot protocol for USB mice supports 3 buttons and 2 axes.
 *
 * References:
 *	Device Class Definition for Human Interface Devices (HID), Version 1.11, 27.06.2001, www.usb.org
 *
 * History:
 *	01.12.2005	Added MouseSpeed & MouseAcceleration (staubesv)
 *	10.10.2006	Adapted to UsbHid (staubesv)
 *	28.02.2007	Removed mouse wheel hack since new HID driver can correctly handle it (staubesv)
 *)

использует НИЗКОУР, ЛогЯдра, Modules, Inputs, Usbdi, UsbHid;

конст

	Name = "UsbMouse";
	Description = "HID boot protocol mouse driver";

	(* Mouse Configuration *)
	MouseSpeed = 50;
	MouseAcceleration = 0;

	Debug = истина;

тип

	MouseDriver= окласс (UsbHid.HidDriver);
	перем
		buffer : Usbdi.BufferPtr;
		pipe : Usbdi.Pipe;

		lastDx, lastDy : цел32;
		accelX, accelY : вещ32;

		проц HandleEvent(status : Usbdi.Status; actLen : размерМЗ);
		перем mm : Inputs.MouseMsg; dx, dy : цел32;
		нач
			если (status = Usbdi.Ok) или ((status = Usbdi.ShortPacket) и (actLen >= 3))  то
				dx := НИЗКОУР.подмениТипЗначения(цел8, buffer[1]);
				dy := НИЗКОУР.подмениТипЗначения(цел8, buffer[2]);

				accelX := 1.0 + матМодуль(dx - lastDx) / 128 * MouseAcceleration;
				accelY := 1.0 + матМодуль(dy - lastDy) / 128 * MouseAcceleration;

				lastDx := dx;
				lastDy := dy;

				mm.dx :=округлиВниз(MouseSpeed / 50.0 *  dx * accelX);
				mm.dy := округлиВниз(MouseSpeed / 50.0 * dy * accelY);

				если (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, buffer[0]) * {0}) # {} то mm.keys := mm.keys + {0}; всё;
				если (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, buffer[0]) * {1}) # {} то mm.keys := mm.keys + {2}; всё;
				если (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, buffer[0]) * {2}) # {} то mm.keys := mm.keys + {1}; всё;

				Inputs.mouse.Handle(mm);
				status := pipe.Transfer(pipe.maxPacketSize, 0, buffer^);
			иначе
				если status = Usbdi.Stalled то
					если pipe.ClearHalt() то
						если Debug то ЛогЯдра.пСтроку8("UsbMouse: Stall on Interrupt Pipe cleared."); ЛогЯдра.пВК_ПС; всё;
						status := pipe.Transfer(pipe.maxPacketSize, 0, buffer^); (* ignore status *)
					иначе
						если Debug то ЛогЯдра.пСтроку8("UsbMouse: Couldn't clear stall on interrupt pipe. Abort."); ЛогЯдра.пВК_ПС; всё;
						device.FreePipe(pipe);
					всё;
				всё;
			всё;
		кон HandleEvent;

		проц {перекрыта}Connect() : булево;
		перем endpoint, i : цел32; status : Usbdi.Status;
		нач
			(* Set the HID boot protocol *)
			если SetProtocol(UsbHid.BootProtocol) = ложь то
				если Debug то ЛогЯдра.пСтроку8("UsbMouse: Error: Cannot set boot protocol."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь
			всё;

			(* Look for the first interrupt IN endpoint of this device *)
			нц
				если i >= длинаМассива(interface.endpoints) то прервиЦикл; всё;
				если interface.endpoints[i].type = Usbdi.InterruptIn то
					endpoint := interface.endpoints[i].bEndpointAddress;
					прервиЦикл;
				всё;
				увел(i);
			кц;

			если endpoint = 0 то
				если Debug то ЛогЯдра.пСтроку8("UsbMouse: No interrupt IN endpoint found."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			pipe := device.GetPipe(endpoint);
			если pipe = НУЛЬ то возврат ложь всё;

			нов(buffer, pipe.maxPacketSize);

			pipe.SetTimeout(0);
			pipe.SetCompletionHandler(HandleEvent);

			status := pipe.Transfer(pipe.maxPacketSize, 0, buffer^); (* ignore res *)
			возврат истина;
		кон Connect;

		проц {перекрыта}Disconnect;
		нач
			ЛогЯдра.пСтроку8("USB mouse disconnected."); ЛогЯдра.пВК_ПС;
		кон Disconnect;

	кон MouseDriver;

проц Probe(dev : Usbdi.UsbDevice; id : Usbdi.InterfaceDescriptor) : Usbdi.Driver;
перем driver : MouseDriver;
нач
	если id.bInterfaceClass # 3 то возврат НУЛЬ всё; (* HID class *)
	если id.bInterfaceSubClass # 1 то возврат НУЛЬ всё; (* Boot protocol subclass *)
	если id.bInterfaceProtocol # 2 то возврат НУЛЬ всё; (* Mouse *)
	нов(driver); возврат driver;
кон Probe;

проц Install*;
кон Install;

проц Cleanup;
нач
	Usbdi.drivers.Remove(Name);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	Usbdi.drivers.Add(Probe, Name, Description, 10)
кон UsbMouse.

UsbMouse.Install ~  System.Free UsbMouse ~
