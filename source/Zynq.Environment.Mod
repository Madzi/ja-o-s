(* Zynq environment *)
(* Copyright (C) Florian Negele *)

модуль Environment;

использует НИЗКОУР, Activities, CPU, HeapManager, Interrupts, Трассировка, Processors, Timer;

конст Running* = 0; ShuttingDown* = 1; Rebooting* = 2;

перем memory: размерМЗ;
перем heap: HeapManager.Heap;
перем frequency: Timer.Counter;
перем status* := Running: целМЗ;

проц {NORETURN} Abort-;
нач {безКооперации, безОбычныхДинПроверок}
	если НИЗКОУР.GetActivity () # НУЛЬ то Activities.TerminateCurrentActivity всё;
	Activities.TerminateCurrentActivity;
кон Abort;

проц Allocate- (size: размерМЗ): адресВПамяти;
перем result, address: адресВПамяти;
нач {безКооперации, безОбычныхДинПроверок}
	result := HeapManager.Allocate (size, heap);
	если result = НУЛЬ то возврат НУЛЬ всё;
	нцДля address := result до result + size - 1 делай НИЗКОУР.запиши8битПоАдресу (address, 0) кц;
	возврат result;
кон Allocate;

проц Deallocate- (address: адресВПамяти);
нач {безКооперации, безОбычныхДинПроверок}
	HeapManager.Deallocate (address, heap);
кон Deallocate;

проц Write- (character: симв8);
нач {безКооперации, безОбычныхДинПроверок}
	нцПока CPU.TXFULL в CPU.ReadMask (CPU.UART1 + CPU.Channel_sts_reg0) делай кц;
	CPU.WriteWord (CPU.UART1 + CPU.TX_RX_FIFO0, логСдвиг (кодСимв8 (character), CPU.FIFO));
кон Write;

проц Flush-;
нач {безКооперации, безОбычныхДинПроверок}
	нцДо кцПри CPU.TXEMPTY в CPU.ReadMask (CPU.UART1 + CPU.Channel_sts_reg0);
кон Flush;

проц GetString- (конст name: массив из симв8; перем result: массив из симв8);
нач {безКооперации, безОбычныхДинПроверок}
	result[0] := 0X
кон GetString;

проц Clock- (): цел32;
нач возврат Timer.GetCounter () DIV frequency;
кон Clock;

проц Sleep- (milliseconds: цел32);
перем clock: Timer.Counter;
нач {безКооперации, безОбычныхДинПроверок}
	утв (milliseconds >= 0);
	clock := Timer.GetCounter () + milliseconds * frequency;
	нцПока Timer.GetCounter () - clock < 0 делай Activities.Switch кц;
кон Sleep;

проц Shutdown*;
нач {безКооперации, безОбычныхДинПроверок}
	если сравнениеСОбменом (status, Running, ShuttingDown) # Running то возврат всё;
	Трассировка.StringLn ("system: shutting down...");
кон Shutdown;

проц Reboot*;
нач {безКооперации, безОбычныхДинПроверок}
	Shutdown;
	утв (сравнениеСОбменом (status, ShuttingDown, Rebooting) = ShuttingDown);
кон Reboot;

проц {NORETURN} Exit- (status: целМЗ);
нач {безКооперации, безОбычныхДинПроверок}
	Трассировка.пСтроку8 ("system: ");
	если status = Rebooting то Трассировка.StringLn ("rebooting..."); CPU.Reset всё;
	Трассировка.StringLn ("ready for power off or restart"); Flush; CPU.Halt;
кон Exit;

проц InitTrace;
конст BaudRate = 115200;
конст BDIV = 6; CD = CPU.UART_REF_CLK DIV BaudRate DIV (BDIV + 1);
нач {безКооперации, безОбычныхДинПроверок}
	CPU.WriteMask (CPU.UART_RST_CTRL, {CPU.UART1_REF_RST, CPU.UART1_CPU1X_RST});
	CPU.WriteMask (CPU.UART1 + CPU.mode_reg0, {CPU.PAR + 2});
	CPU.WriteMask (CPU.UART1 + CPU.Intrpt_dis_reg0, {0..12});
(*	commented out to reuse UART settings from bootloader
	CPU.WriteMask (CPU.UART1 + CPU.Control_reg0, {CPU.RXDIS, CPU.TXDIS});
	CPU.WriteWord (CPU.UART1 + CPU.Baud_rate_gen_reg0, LSH (CD, CPU.CD));
	CPU.WriteWord (CPU.UART1 + CPU.Baud_rate_divider_reg0, LSH (BDIV, CPU.BDIV));
	CPU.WriteMask (CPU.UART1 + CPU.Control_reg0, {CPU.RXRST, CPU.TXRST});
	CPU.WriteMask (CPU.UART1 + CPU.Control_reg0, {CPU.RXEN, CPU.TXEN});
*)
	Трассировка.Настройся; Трассировка.пСимв8 := Write;
кон InitTrace;

проц InitMemory;
конст MemorySize = 512 * 1024 * 1024;
нач {безКооперации, безОбычныхДинПроверок}
	HeapManager.Initialize (heap, операцияАдресОт KernelEnd, MemorySize);
	memory := MemorySize - операцияАдресОт KernelEnd;
кон InitMemory;

проц StoreActivity-;
нач {безКооперации, безОбычныхДинПроверок}
кон StoreActivity;

проц RestoreActivity-;
нач {безКооперации, безОбычныхДинПроверок}
кон RestoreActivity;

проц Initialize-;
нач {безКооперации, безОбычныхДинПроверок}
	CPU.Initialize; InitTrace; InitMemory;
	frequency := Timer.GetFrequency () DIV 1000;
кон Initialize;

проц Terminate-;
нач {безКооперации, безОбычныхДинПроверок}
	Interrupts.Terminate;
кон Terminate;

проц {NOPAF, INITIAL, FIXED(100000H)} KernelBegin;
машКод
	; initialize SP
	MOV	SP, #0x8000
	MRC	P15, 0, R0, C0, C0, 5
	AND	R0, R0, #0x1
	SUB	SP, SP, R0, LSL #13

	; filter CPU
	CMP	R0, #0
	BEQ	skip
	WFE
	B		@Processors.Boot
skip:
кон KernelBegin;

проц {NOPAF, неРасширяемая, выровнять(32)} KernelEnd;
машКод
кон KernelEnd;

нач {безОбычныхДинПроверок}
	Трассировка.пСтроку8 ("Version "); Трассировка.пСтроку8 (НИЗКОУР.Date); Трассировка.пСтроку8 (" (");
	Трассировка.пЦел64 (memory DIV (1024 * 1024), 0); Трассировка.пСтроку8 (" MB RAM, GC, ");
	Трассировка.пЦел64 (Processors.count, 0); Трассировка.пСтроку8 (" CPU");
	если Processors.count > 1 то Трассировка.пСимв8 ('s') всё; Трассировка.пСимв8 (')'); Трассировка.пВК_ПС;
кон Environment.
