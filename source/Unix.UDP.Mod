(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль UDP;   (** AUTHOR "pjm, mvt, G.F."; PURPOSE "UDP protocol"; *)

использует IP, Sockets, Unix;

конст
	(** Error codes *)
	Ok* = 0;  PortInUse* = 3501;  Timeout* = 3502;  BufferOverflow* = 3503;  NoInterface* = 3504;
	Closed* = 3505;  Error* = 9999;

	NilPort* = 0;  anyport = 0;

	UDPHdrLen = 8;
	MaxUDPDataLen = 10000H - UDPHdrLen;
перем
	anyIP: IP.Adr;

тип
	(** Socket. Stores the state of a UDP communication endpoint. *)
	Socket* = окласс
			перем
				socket: цел32;
				lport: цел32;   (* local port *)
				open: булево;


				(** Constructor *)
				проц & Open*( lport: цел32;  перем res: целМЗ );
				перем laddr: Sockets.SocketAdr;
				нач
					утв( (lport >= 0) и (lport < 10000H) );
					сам.lport := lport;  res := Error;
					socket := Sockets.Socket( Unix.AFINET, Unix.SockDGram, Unix.IpProtoUDP );
					если socket # 0 то
						если lport # anyport то
							(* server *)
							laddr := Sockets.NewSocketAdr( anyIP, lport );
							если Sockets.Bind( socket, laddr ) то
								res := Ok;  open := истина
							иначе
								Sockets.Close( socket )
							всё
						иначе
							(* client *)
							res := Ok;  open := истина
						всё
					всё
				кон Open;


				(** Send a UDP datagram to the foreign address specified by "fip" and "fport".
					   The data is in "data[ofs..ofs+len-1]".  In case of concurrent sends the datagrams are serialized.
					*)
				проц Send*( fip: IP.Adr;
								   fport: цел32;
								   конст data: массив из симв8;  ofs, len: размерМЗ;
								   перем res: целМЗ );
				перем addr: Sockets.SocketAdr;
				нач {единолично}
					утв( (fport >= 0) и (fport < 10000H) );
					утв( (len >= 0) и (len <= MaxUDPDataLen) );
					addr := Sockets.NewSocketAdr( fip, fport );
					если Sockets.SendTo( socket, addr, data, ofs, len ) то  res := Ok  иначе  res := Error  всё
				кон Send;


				(** Send a broadcast UDP datagram via interface "int" to port "lport". Normally only used by DHCP.
					   The data is in "data[ofs..ofs+len-1]".  In case of concurrent sends the datagrams are serialized.
					*)
				проц SendBroadcast*( int: IP.Interface;  fport: цел32;
										      конст data: массив из симв8;  ofs, len: размерМЗ );
				нач (*{EXCLUSIVE}*)
					утв( (fport >= 0) и (fport < 10000H) );  утв( (len >= 0) и (len <= MaxUDPDataLen) );
					СТОП( 99 ) (* not implemented yet *)
				кон SendBroadcast;


				(** Receive a UDP datagram.  If none is available, wait up to the specified timeout for one to arrive.
					"data[ofs..ofs+size-1]" is the data buffer to hold the returned datagram.
					"ms" is a wait timeout value in milliseconds, 0 means "don't wait", -1 means "infinite wait".
					On return, "fip" and "fport" hold the foreign address and port.
					"len" returns the actual datagram size and "data[ofs..ofs+len-1]" returns the data.
					"res" returns "Timeout" in case of a timeout and "BufferOverflow" if the received datagram was
					too big.
				*)
				проц Receive*( перем data: массив из симв8;  ofs, size: размерМЗ; ms: цел32;
									 перем fip: IP.Adr;  перем fport: цел32; перем len: размерМЗ; перем res: целМЗ );
				перем
					addr: Sockets.SocketAdr; i: размерМЗ;
				нач {единолично}
					если ~open то  res := Closed;  возврат   всё;
					если (ms >= 0) и ~Sockets.AwaitPacket( socket, ms ) то
						res := Timeout;  возврат
					всё;
					если Sockets.RecvFrom( socket, addr, data, ofs, len ) то
						fport := Sockets.GetPortNumber( addr );
						если addr суть Sockets.SocketAdrV4 то
							fip.usedProtocol := IP.IPv4;
							fip.ipv4Adr := addr(Sockets.SocketAdrV4).v4Adr;
						иначе
							fip.usedProtocol := IP.IPv6;
							нцДля i := 0 до 15 делай
								fip.ipv6Adr[i] := addr(Sockets.SocketAdrV6).v6Adr[i]
							кц
						всё;
						res := Ok;
					иначе
						res := Error
					всё
				кон Receive;



				(** Close the Socket, freeing its address for re-use. *)
				проц Close*;
				нач
					Sockets.Close( socket );  open := ложь
				кон Close;

			кон Socket;

нач
	anyIP := IP.NilAdr;
кон UDP.



