модуль WMDocumentEditor; (** AUTHOR "staubesv"; PURPOSE "Document editor component"; *)

использует
	ЛогЯдра, Потоки, Files, Inputs, Strings, XML, XMLObjects, Configuration, Texts, TextUtilities, Codecs,
	WMMacros, WMGraphics, WMRectangles, WMMessages, WMComponents, WMStandardComponents,
	WMPopups, WMTextView, WMEditors, WMSearchComponents, WMDialogs, WMRestorable;

конст
	LoadButton* = {0};
	StoreButton* = {1};
	FormatButton*= {2};
	SearchButton* = {3};
	WrapButton* = {4};
	ClearButton* = {5};

	All* = {0..31};

	DefaultTextEncoder = "BBT";

тип

	CaptionObject = окласс
	перем
		caption : массив 100 из симв8;

		проц &New*(конст caption : массив из симв8);
		нач
			копируйСтрокуДо0(caption, сам.caption);
		кон New;

	кон CaptionObject;

тип

	CodecFormatName = массив 100 из симв8;
	
	Editor* = окласс(WMComponents.VisualComponent)
	перем
		editor- : WMEditors.Editor;

		toolbar : WMStandardComponents.Panel;

		filenamePanel : WMStandardComponents.Panel;
		filenameEdit : WMEditors.Editor;
		resizer : WMStandardComponents.Resizer;
		loadBtn, storeBtn, formatBtn : WMStandardComponents.Button;

		searchBtn : WMStandardComponents.Button;
		searchPanel : WMSearchComponents.SearchPanel;

		wrapBtn, clearBtn : WMStandardComponents.Button;

		popup : WMPopups.Popup;

		lastFilename : Files.FileName;
		codecFormat : CodecFormatName;
		autoCodecFormat : CodecFormatName;

		wordWrap, modified : булево;

		buttons : мнвоНаБитахМЗ;

		проц FilenameEscapeHandler(sender, data : динамическиТипизированныйУкль);
		нач
			filenameEdit.SetAsString(lastFilename);
		кон FilenameEscapeHandler;

		проц LoadHandler(sender, data : динамическиТипизированныйУкль);
		перем filename : Files.FileName;
		нач
			filenameEdit.GetAsString(filename);
			Strings.TrimWS(filename);
			если (filename # "") то
				Load(filename, codecFormat);
			всё;
		кон LoadHandler;

		проц Load*(конст filename,  format : массив из симв8);
		перем
			text : Texts.Text;
			msg : массив 512 из симв8; res : целМЗ;
			fullname : Files.FileName;
			decoder : Codecs.TextDecoder;
			in : Потоки.Чтец;
			file : Files.File;
		нач
			res := -1;
			копируйСтрокуДо0(filename, fullname);

			(* Check whether file exists and get its canonical name *)
			file := Files.Old(filename);
			если (file # НУЛЬ) то
				file.GetName(fullname);
			иначе
				file := Files.New(filename); (* to get path *)
				если (file # НУЛЬ) то
					file.GetName(fullname);
					file := НУЛЬ;
				всё;
			всё;

			если (filenameEdit # НУЛЬ) то
				filenameEdit.SetAsString(fullname);
				lastFilename := fullname;
			всё;

			text := editor.text;
			text.AcquireWrite;
			modified := истина; (* avoid the ! on the store button while loading *)
			text.Delete(0, text.GetLength());
			editor.tv.firstLine.Set(0);
			text.ReleaseWrite;

			если (file # НУЛЬ) то
				если (format = "AUTO") или (format = "АВТО") то
					decoder := TextUtilities.DecodeAuto(fullname, autoCodecFormat);
				иначе
					decoder := Codecs.GetTextDecoder(format);
				всё;

				если decoder # НУЛЬ то
					копируйСтрокуДо0(format, codecFormat);
					in := Codecs.OpenInputStream(fullname);
					если in # НУЛЬ то
						decoder.Open(in, res);
						если res = 0 то
							editor.text.onTextChanged.Remove(TextChanged);
							SetText(decoder.GetText());
							editor.text.onTextChanged.Add(TextChanged);
						всё;
					иначе
						msg := "Не могу открыть поток чтения для файла "; Strings.Append(msg, fullname);
						WMDialogs.Error("Error", msg);
					всё;
				иначе
					msg := "No decoder for file "; Strings.Append(msg, fullname);
					Strings.Append(msg, " (Format: "); Strings.Append(msg, format); Strings.Append(msg, ")");
					WMDialogs.Error("Error", msg);
				всё;
			всё;

			SetFormatCaption(format);
			editor.tv.firstLine.Set(0);
			editor.tv.cursor.SetPosition(0);
			editor.tv.SetFocus;
			modified := ложь;
			если (buttons * StoreButton # {}) то storeBtn.caption.SetAOC("Store") всё;
		кон Load;

		проц StoreHandler(sender, data : динамическиТипизированныйУкль);
		перем filename : Files.FileName;
		нач
			filenameEdit.GetAsString(filename);
			Strings.TrimWS(filename);
			если filename # "" то
				Store(filename, codecFormat);
			иначе
				WMDialogs.Error("Error", "Filename invalid"); (* ignore res *)
				filenameEdit.SetAsString(filename);
			всё;
		кон StoreHandler;

		проц Store*(конст filename,format  : массив из симв8);
		перем
			text : Texts.Text;
			fullname : Files.FileName;
			msg : массив 512 из симв8; res : целМЗ;
			backName : массив 128 из симв8;
			encoder : Codecs.TextEncoder;
			w : Files.Writer;
			f : Files.File;
		нач
			если (filenameEdit # НУЛЬ) то filenameEdit.SetAsString(filename); всё;
			f := Files.Old(filename);
			если (f # НУЛЬ) то
				если (Files.ReadOnly в f.flags) то
					msg := "File is read-only: "; Strings.Append(msg, filename);
					WMDialogs.Error("Error", msg);
					возврат;
				всё;
				f := НУЛЬ;
			всё;
			(* create backup *)
			Strings.Concat(filename, ".Bak", backName);
			Files.Rename(filename, backName, res);
			если res = 0 то ЛогЯдра.пСтроку8("Backup created  in "); ЛогЯдра.пСтроку8(backName); ЛогЯдра.пВК_ПС всё;
			text := editor.text;
			text.AcquireWrite;

			если (format = "AUTO") или (format = "АВТО") то
				если (autoCodecFormat = "") то
					encoder := Codecs.GetTextEncoder(DefaultTextEncoder);
				иначе
					encoder := Codecs.GetTextEncoder(autoCodecFormat);
				всё;
			иначе encoder := Codecs.GetTextEncoder(format);
			всё;

			копируйСтрокуДо0(filename, fullname);
			если (encoder # НУЛЬ) то
				f := Files.New(filename);
				если (f = НУЛЬ) то
					msg := "Could not create file "; Strings.Append(msg, filename);
					WMDialogs.Error("Error", msg);
					возврат;
				всё;

				f.GetName(fullname);
				Files.OpenWriter(w, f, 0);

				encoder.Open(w);
				encoder.WriteText(text, res);
				если res = 0 то
					Files.Register(f); f.Update;
				иначе
					msg := "Could not encode file "; Strings.Append(msg, fullname);
					WMDialogs.Error("Error", msg);
				всё;
			иначе
				msg := "Could not store file "; Strings.Append(msg, fullname); Strings.Append(msg, " (No encoder found)");
				WMDialogs.Error("Error", msg);
			всё;

			text.ReleaseWrite;
			modified := ложь;
			если (filenameEdit # НУЛЬ) то
				filenameEdit.SetAsString(fullname);
				lastFilename := fullname;
			всё;
			если (buttons * StoreButton # {}) то storeBtn.caption.SetAOC("Store") всё;
		кон Store;

		проц FormatHandler(x, y : размерМЗ; keys : мнвоНаБитахМЗ; перем handled : булево);
		перем rectangle : WMRectangles.Rectangle; left, top : размерМЗ;
		нач
			если (formatBtn = НУЛЬ) то возврат всё;
			handled := истина;
			rectangle := formatBtn.bounds.Get();
			ToWMCoordinates(rectangle.l, rectangle.t, left, top);
			popup.Popup(left, top + formatBtn.bounds.GetHeight());
		кон FormatHandler;

		проц SetFormatCaption(конст format : массив из симв8);
		перем caption : массив 128 из симв8;
		нач
			если (formatBtn = НУЛЬ) то возврат всё;
			caption := "Формат : ";
			Strings.Append(caption, format);
			если (format = "AUTO") или (format = "АВТО") то Strings.Append(caption, " "); Strings.Append(caption, autoCodecFormat); всё;
			formatBtn.caption.SetAOC(caption);
			formatBtn.Invalidate;
		кон SetFormatCaption;

		проц SetCodecFormat*(конст newCodecFormat : массив из симв8);
		нач{единолично}
			копируйСтрокуДо0(newCodecFormat, codecFormat);
			SetFormatCaption(codecFormat);
		кон SetCodecFormat;

		проц FormatPopupHandler(sender, data : динамическиТипизированныйУкль);
		нач
			если (popup # НУЛЬ) и (data # НУЛЬ) и (data суть CaptionObject) то
				popup.Close;
				SetCodecFormat(data(CaptionObject).caption);
			всё;
		кон FormatPopupHandler;

		проц SearchHandler(sender, data : динамическиТипизированныйУкль);
		перем searchString : WMSearchComponents.SearchString;
		нач
			EnsureSearchPanel;
			searchPanel.visible.Set(истина);
			searchPanel.SetToLastSelection;
			searchPanel.searchEdit.GetAsString(searchString);
			если (searchString # "") то
				searchPanel.SearchHandler(НУЛЬ, НУЛЬ);
			иначе
				searchPanel.searchEdit.SetFocus;
			всё;
		кон SearchHandler;

		проц WrapHandler(sender, data : динамическиТипизированныйУкль);
		нач
			SetWordWrap(~wordWrap);
		кон WrapHandler;

		проц SetWordWrap*(wordWrap : булево);
		нач
			сам.wordWrap := wordWrap;
			если wordWrap то
				editor.tv.wrapMode.Set(WMTextView.WrapWord);
			иначе
				editor.tv.wrapMode.Set(WMTextView.NoWrap);
			всё;
			если (wrapBtn # НУЛЬ) то wrapBtn.SetPressed(wordWrap); всё;
		кон SetWordWrap;

		проц ClearHandler(sender, data : динамическиТипизированныйУкль);
		нач
			Clear;
		кон ClearHandler;

		проц Clear*;
		нач
			editor.text.AcquireWrite;
			editor.text.Delete(0, editor.text.GetLength());
			editor.tv.firstLine.Set(0); editor.tv.cursor.SetPosition(0);
			editor.text.ReleaseWrite;
		кон Clear;

		проц TextChanged(sender, data : динамическиТипизированныйУкль);
		нач
			если (buttons * StoreButton # {}) и ~modified то
				storeBtn.caption.SetAOC("Store !");
				modified := истина
			всё
		кон TextChanged;

		проц SetText*(text : Texts.Text);
		нач
			если (editor.text # НУЛЬ) то
				editor.text.onTextChanged.Remove(TextChanged);
			всё;
			text.onTextChanged.Add(TextChanged);
			editor.SetText(text);
			если (searchPanel # НУЛЬ) то searchPanel.SetText(text); всё;
		кон SetText;

		проц SetToolbar*(buttons : мнвоНаБитахМЗ);
		нач
			сам.buttons := buttons;
			если (buttons * LoadButton # {}) или (buttons * StoreButton # {}) то
				нов(filenamePanel);
				filenamePanel.alignment.Set(WMComponents.AlignLeft);
				filenamePanel.bounds.SetWidth(200);
				toolbar.AddInternalComponent(filenamePanel);

				нов(resizer);
				resizer.alignment.Set(WMComponents.AlignRight);
				resizer.bounds.SetWidth(5);
				filenamePanel.AddInternalComponent(resizer);

				нов(filenameEdit);
				filenameEdit.alignment.Set(WMComponents.AlignClient);
				filenameEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);
				filenameEdit.multiLine.Set(ложь);
				filenameEdit.tv.showBorder.Set(истина);
				filenameEdit.tv.borders.Set(WMRectangles.MakeRect(3,3,1,1));
				filenamePanel.AddInternalComponent(filenameEdit); filenameEdit.fillColor.Set(цел32(0FFFFFFFFH));
				filenameEdit.onEscape.Add(FilenameEscapeHandler);

				если (buttons * LoadButton # {}) то
					filenameEdit.onEnter.Add(LoadHandler);
					нов(loadBtn);
					loadBtn.caption.SetAOC("Открой"); loadBtn.alignment.Set(WMComponents.AlignLeft);
					loadBtn.onClick.Add(LoadHandler);
					toolbar.AddInternalComponent(loadBtn);
				всё;

				если (buttons * StoreButton # {}) то
					нов(storeBtn);
					storeBtn.caption.SetAOC("Сохрани"); storeBtn.alignment.Set(WMComponents.AlignLeft);
					storeBtn.onClick.Add(StoreHandler);
					toolbar.AddInternalComponent(storeBtn);
				всё;

				если (buttons * FormatButton # {}) то
					нов(formatBtn);
					formatBtn.caption.SetAOC("Формат"); formatBtn.alignment.Set(WMComponents.AlignLeft);
					formatBtn.SetExtPointerDownHandler(FormatHandler);
					formatBtn.bounds.SetWidth(3 * formatBtn.bounds.GetWidth());
					toolbar.AddInternalComponent(formatBtn);

					SetFormatCaption("АВТО");
				всё;
			всё;

			если (buttons * WrapButton # {}) то
				нов(wrapBtn);
				wrapBtn.caption.SetAOC("Word Wrap"); wrapBtn.alignment.Set(WMComponents.AlignLeft);
				wrapBtn.isToggle.Set(истина); wrapBtn.SetPressed(wordWrap);
				wrapBtn.onClick.Add(WrapHandler); wrapBtn.bounds.SetWidth(100);
				toolbar.AddInternalComponent(wrapBtn);
			всё;

			если (buttons * ClearButton # {}) то
				нов(clearBtn);
				clearBtn.caption.SetAOC("Clear"); clearBtn.alignment.Set(WMComponents.AlignRight);
				clearBtn.onClick.Add(ClearHandler);
				toolbar.AddInternalComponent(clearBtn);
			всё;

			если (buttons * SearchButton # {}) то
				нов(searchBtn);
				searchBtn.alignment.Set(WMComponents.AlignRight);
				searchBtn.SetCaption("Search");
				searchBtn.onClick.Add(SearchHandler);
				toolbar.AddInternalComponent(searchBtn);
			всё;

			toolbar.visible.Set(истина);
		кон SetToolbar;

		проц HandleShortcut*(ucs : размерМЗ; flags : мнвоНаБитахМЗ; keysym : размерМЗ) : булево;
		нач
			если (buttons * StoreButton # {}) и (keysym = 13H) и ControlKeyDown(flags) то (* CTRL-S *)
				StoreHandler(НУЛЬ, НУЛЬ);
			аесли (buttons * LoadButton # {}) и (keysym = 0FH) и ControlKeyDown(flags) то (* CTRL-O *)
				filenameEdit.SetAsString("");
				filenameEdit.SetFocus;
			аесли (buttons * SearchButton # {}) и (keysym = 06H) и ControlKeyDown(flags)то (* CTRL-F *)
				EnsureSearchPanel; searchPanel.ToggleVisibility;
			аесли (buttons * SearchButton # {}) и (keysym= 0EH) и ControlKeyDown(flags) и (searchPanel # НУЛЬ) то (* CTRL-N *)
				searchPanel.HandlePreviousNext(истина);
			аесли (buttons * SearchButton # {}) и (keysym = 10H) и ControlKeyDown(flags) и (searchPanel # НУЛЬ) то (* CTRL-P *)
				searchPanel.HandlePreviousNext(ложь);
			аесли (buttons * SearchButton # {}) и (keysym = Inputs.KsTab) и (flags = {}) то (* TAB *)
				возврат (searchPanel # НУЛЬ) и searchPanel.HandleTab();
			иначе
				возврат ложь; (* Key not handled *)
			всё;
			возврат истина;
		кон HandleShortcut;

		проц ToXml*(config : XML.Element);
		перем filename : Files.FileName;
		нач
			filenameEdit.GetAsString(filename);
			WMRestorable.StoreString(config, "file", filename);
			WMRestorable.StoreString(config, "codecFormat", codecFormat);
			WMRestorable.StoreSize(config, "firstLine", editor.tv.firstLine.Get());
			WMRestorable.StoreSize(config, "cursorPos", editor.tv.cursor.GetPosition());
			WMRestorable.StoreBoolean(config, "wordWrap", wordWrap);
		кон ToXml;

		проц FromXml*(config : XML.Element);
		перем filename : Files.FileName; firstLine, cursorPos : размерМЗ; wordWrap : булево;
		нач
			утв(config # НУЛЬ);
			WMRestorable.LoadString(config, "file", filename);
			WMRestorable.LoadString(config, "codecFormat", codecFormat);
			WMRestorable.LoadSize(config, "firstLine", firstLine);
			WMRestorable.LoadSize(config, "cursorPos", cursorPos);
			WMRestorable.LoadBoolean(config, "wordWrap", wordWrap);
			Load(filename, codecFormat);
			editor.tv.firstLine.Set(firstLine);
			editor.tv.cursor.SetPosition(cursorPos);
			SetWordWrap(wordWrap);
		кон FromXml;

		проц {перекрыта}Handle*(перем m: WMMessages.Message);
		нач
			если m.msgType = WMMessages.MsgKey то
				если ~HandleShortcut(m.x, m.flags, m.y) то
					Handle^(m);
				всё;
			иначе Handle^(m)
			всё
		кон Handle;

		проц InitCodecs;
		перем caption : CaptionObject;
			elem : XML.Element; enum : XMLObjects.Enumerator; ptr : динамическиТипизированныйУкль; str : Strings.String;
		нач
			нов(popup);
			(* retrieve available Text-Codecs *)
			если Configuration.config # НУЛЬ то
				elem := Configuration.config.GetRoot();
				если elem # НУЛЬ то
					enum := elem.GetContents(); enum.Reset();
					нцПока enum.HasMoreElements() делай
						ptr := enum.GetNext();
						если ptr суть XML.Element то
							str := ptr(XML.Element).GetAttributeValue("name");
							если (str # НУЛЬ) и (str^ = "Codecs") то
								enum := ptr(XML.Element).GetContents(); enum.Reset();
								нцПока enum.HasMoreElements() делай
									ptr := enum.GetNext();
									если ptr суть XML.Element то
										str := ptr(XML.Element).GetAttributeValue("name");
										если (str # НУЛЬ) и (str^ = "Decoder") то
											enum := ptr(XML.Element).GetContents(); enum.Reset();
											нцПока enum.HasMoreElements() делай
												ptr := enum.GetNext();
												если ptr суть XML.Element то
													str := ptr(XML.Element).GetAttributeValue("name");
													если (str # НУЛЬ) и (str^ = "Text") то
														enum := ptr(XML.Element).GetContents(); enum.Reset();
														нцПока enum.HasMoreElements() делай
															ptr := enum.GetNext();
															если ptr суть XML.Element то
																str := ptr(XML.Element).GetAttributeValue("name");
																нов(caption, str^);
																popup.AddParButton(str^, FormatPopupHandler, caption);
															всё;
														кц;
													всё;
												всё;
											кц;
										всё;
									всё;
								кц;
							всё;
						всё;
					кц;
				всё;
			всё;
			нов(caption, "АВТО");
			popup.AddParButton("АВТО", FormatPopupHandler, caption);
		кон InitCodecs;

		проц EnsureSearchPanel;
		нач
			если (searchPanel = НУЛЬ) то
				RemoveContent(editor);
				нов(searchPanel);
				searchPanel.alignment.Set(WMComponents.AlignBottom);
				searchPanel.bounds.SetHeight(40);
				searchPanel.SetText(editor.text);
				searchPanel.SetTextView(editor.tv);
				searchPanel.visible.Set(ложь);
				AddInternalComponent(searchPanel);
				AddInternalComponent(editor);
				Reset(НУЛЬ, НУЛЬ);
			всё;
		кон EnsureSearchPanel;

		проц {перекрыта}Finalize*;
		нач
			Finalize^;
			если (editor.text # НУЛЬ) то
				editor.text.onTextChanged.Remove(TextChanged);
			всё;
		кон Finalize;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrDocumentEditor);
			wordWrap := ложь;
			InitCodecs;
			lastFilename := "";
			codecFormat := "АВТО";
			autoCodecFormat := "";
			modified := ложь;

			fillColor.Set(WMGraphics.White);

			нов(toolbar);
			toolbar.alignment.Set(WMComponents.AlignTop);
			toolbar.bounds.SetHeight(20);
			toolbar.visible.Set(ложь);
			AddInternalComponent(toolbar);

			searchPanel := НУЛЬ;

			нов(editor);
			editor.alignment.Set(WMComponents.AlignClient); editor.tv.showBorder.Set(истина);
			editor.macros.Add(WMMacros.Handle);
			editor.multiLine.Set(истина);
			editor.tv.wrapMode.Set(WMTextView.NoWrap);
			editor.text.onTextChanged.Add(TextChanged);
			AddInternalComponent(editor);

			SetFormatCaption("АВТО");
			SetWordWrap(wordWrap);
		кон Init;

	кон Editor;

перем
	StrDocumentEditor : Strings.String;

проц InitStrings;
нач
	StrDocumentEditor := Strings.NewString("DocumentEditor");
кон InitStrings;

проц ControlKeyDown(flags : мнвоНаБитахМЗ) : булево;
нач
	возврат (flags * Inputs.Ctrl # {}) и (flags - Inputs.Ctrl = {});
кон ControlKeyDown;

нач
	InitStrings;
кон WMDocumentEditor.

System.FreeDownTo WMDocumentEditor ~
