# Постройка образа ЯОС, с которого можно загзурить компьютер IBM PC (проверено на ASUS G771JW)
# https://forum.oberoncore.ru/viewtopic.php?f=22&t=63867
# Порядок выполнения:
#   - создать пустую директорию ЯОС:Win32/NewBios32USB 
#   - выполнить две команды - появится образ JA-O-S-USB.img
#   - записать образ на флешку, например, с помощью Win32 disk imager
#   - выключить питание компьютера
#   - загрузиться с флешки (для ASUS G771JW - del, boot overrides, выбрать флешку


# Сборка
СборщикВыпускаЯОС.Скомпилируй --лис --путьКОбъектнымФайлам="../NewBios32USB/" --zip --скомпилируй --xml Bios32 ~

# Подготовка образа для загрузки с USB (JA-O-S-USB.img)

System.DoCommands

System.Timer start ~

PCAAMD64.Assemble OBLUnreal.Asm ~
PartitionsLib.SetBootLoaderFile OBLUnreal.Bin ~
PCAAMD64.Assemble BootManager.Asm ~
BootManager.Split BootManager.Bin ~

FSTools.DeleteFiles -i ../NewBios32USB/JA-O-S-USB.img ~

VirtualDisks.Create ../NewBios32USB/JA-O-S-USB.img 320000 512 ~
VirtualDisks.Install -b=512 VDISK0 ../NewBios32USB/JA-O-S-USB.img ~

Linker.Link --path=../NewBios32USB/ --displacement=100000H --fileName=../NewBios32USB/USB.Bin
	Kernel Traps
	UsbHubDriver UsbEhci UsbEhciPCI UsbStorageBoot
	DiskVolumes DiskFS Loader BootConsole ~

Partitions.WriteMBR VDISK0#0 OBEMBR.BIN ~
Partitions.InstallBootManager VDISK0#0 BootManagerMBR.Bin BootManagerTail.Bin ~
Partitions.Create VDISK0#1 76 150 ~

Partitions.Format VDISK0#1 AosFS -1 ../NewBios32USB/USB.Bin ~ (* -1 makes sure that actual boot file size is taken as offset for AosFS *)
FSTools.Mount TEMP AosFS VDISK0#1 ~

ZipTool.ExtractAll --prefix=TEMP: --sourcePath=../NewBios32USB/ --overwrite --silent
	Kernel.zip System.zip Drivers.zip ApplicationsMini.zip Applications.zip Compiler.zip CompilerSrc.zip
	GuiApplicationsMini.zip GuiApplications.zip Fun.zip Contributions.zip Build.zip EFI.zip
	Oberon.zip OberonGadgets.zip OberonApplications.zip OberonDocumentation.zip
	KernelSrc.zip SystemSrc.zip DriversSrc.zip ApplicationsMiniSrc.zip ApplicationsSrc.zip GuiApplicationsMiniSrc.zip GuiApplicationsSrc.zip FunSrc.zip BuildSrc.zip
	ScreenFonts.zip CjkFonts.zip TrueTypeFonts.zip ~

FSTools.Watch TEMP ~
FSTools.Unmount TEMP ~

Partitions.SetConfig VDISK0#1
	TraceMode="1" TracePort="1" TraceBPS="115200"
	BootVol1="AOS AosFS USB0#1"
	AosFS="DiskVolumes.New DiskFS.NewFS"
	CacheSize="1000"
	ExtMemSize="512"
	MaxProcs="-1"
	ATADetect="legacy"
	Init="117"
	Boot="DisplayLinear.Install"
	Boot1="Keyboard.Install;MousePS2.Install"
	Boot2="DriverDatabase.Enable;UsbHubDriver.Install;UsbEhciPCI.Install;UsbUhci.Install;UsbOhci.Install"
	Boot3="WindowManager.Install"
	Boot4="ЗагрузиПереводыЭлементовКода.ИзВсехФайлов"
	Boot5="Autostart.Run"
~
VirtualDisks.Uninstall VDISK0 ~

System.Show USB image build time: ~ System.Timer elapsed ~

FSTools.CloseFiles ../NewBios32USB/JA-O-S-USB.img ~

~
