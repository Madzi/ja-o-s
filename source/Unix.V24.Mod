модуль V24;		(** AUTHOR "F.Necati"; PURPOSE "V24 serial lines"; *)

использует Unix, Log := ЛогЯдра, Serials, Objects, Commands, Strings;

конст
	(* modem lines  bits *)
	TIOCM_LE = 0;  TIOCM_DTR = 1; TIOCM_RTS = 2; TIOCM_ST = 3;
	TIOCM_SR = 4;  TIOCM_CTS = 5; TIOCM_CAR = 6; TIOCM_RNG = 7;
	TIOCM_DSR	= 8;  TIOCM_CD = TIOCM_CAR; TIOCM_RI = TIOCM_RNG;

	(* tio.iflag bits *)
	IGNBRK  = {0};
	BRKINT  = {1};
	IGNPAR  = {2};

	(* tcsetattr actions *)
	TCSANOW    = 0;
	TCSADRAIN  = 1;
	TCSAFLUSH  = 2;

	NumberOfPorts = 4;


перем
	tcgetattr	: проц {C} ( fd: цел32;  перем tio: Unix.Termios ): цел32;
	tcsetattr	: проц {C} ( fd: цел32; action: цел32;  перем tio: Unix.Termios ): цел32;
	cfsetispeed	: проц {C} ( перем tio: Unix.Termios; speed: цел32 ): цел32;
	cfsetospeed	: проц {C} ( перем tio: Unix.Termios; speed: цел32 ): цел32;
	cfgetispeed	: проц {C} ( перем tio: Unix.Termios ): цел32;
	cfgetospeed	: проц {C} ( перем tio: Unix.Termios ): цел32;




тип
	Port* = окласс (Serials.Port)
	перем
		 fd: цел32;
		 portname: массив 128 из симв8;

		проц & Init*( port: цел32; конст name: массив из симв8 );
		нач
			копируйСтрокуДо0(name, portname);
			fd := 0;
		кон Init;

		(* adapted from LNO.V24.Mod *)
		проц {перекрыта}Open*( bps, data, parity, stop: цел32;  перем res: целМЗ );
		перем
			err, speed: цел32;
			tio: Unix.Termios;
		нач {единолично}
			если fd > 0 то res := Serials.PortInUse; возврат всё;

			если (data < 5) или ( data > 8) или ( parity = Serials.ParMark) или
			    ( parity = Serials.ParSpace) или (stop = Serials.Stop1dot5) то
				res := Serials.WrongData;  возврат;
			всё;

			fd := Unix.open( адресОт(portname), Unix.rdwr, 0 );
			если fd = -1 то
				fd := 0;  res := Serials.NoSuchPort;  возврат
			всё;

			err := tcgetattr( fd, tio );

			tio.iflags:= IGNBRK + IGNPAR;
			tio.oflags:={};
			tio.cflags:= Unix.CREAD + Unix.HUPCL + Unix.CLOCAL;
			tio.lflags:={};

			если bps < 50 то speed := Unix.B0
			аесли bps < 75 то speed := Unix.B50
			аесли bps < 110 то speed := Unix.B75
			аесли bps < 134 то speed := Unix.B110
			аесли bps < 150 то speed := Unix.B134
			аесли bps < 200 то speed := Unix.B150
			аесли bps < 300 то speed := Unix.B200
			аесли bps < 600 то speed := Unix.B300
			аесли bps < 1200 то speed := Unix.B600
			аесли bps < 1800 то speed := Unix.B1200
			аесли bps < 2400 то speed := Unix.B1800
			аесли bps < 4800 то speed := Unix.B2400
			аесли bps < 9600 то speed := Unix.B4800
			аесли bps < 19200 то speed := Unix.B9600
			аесли bps < 38400 то speed := Unix.B19200
			аесли bps < 57600 то speed := Unix.B38400
			аесли bps < 115200 то speed := Unix.B57600
			аесли bps < 230400 то speed := Unix.B115200
			иначе speed := Unix.B230400
			всё;
			err := cfsetispeed( tio, speed );
			err := cfsetospeed( tio, speed );

			tio.cflags := tio.cflags - Unix.CSIZE;
			если data= 5 то tio.cflags:= tio.cflags + Unix.CS5;
			аесли data= 6 то tio.cflags:= tio.cflags + Unix.CS6;
			аесли data= 7 то tio.cflags:= tio.cflags + Unix.CS7;
			аесли data= 8 то tio.cflags:= tio.cflags + Unix.CS8;
			всё;

			если parity # Serials.ParNo то
				tio.cflags := tio.cflags + Unix.PARENB;
				если parity= Serials.ParOdd то  tio.cflags := tio.cflags + Unix.PARODD   всё;
			всё;
			если stop= Serials.Stop2 то tio.cflags := tio.cflags + Unix.CSTOPB  всё;

			err := tcsetattr( fd, TCSAFLUSH, tio);
			если err # -1 то
				Log.пСтроку8( "V24 opened the port: " ); Log.пСтроку8( portname ); Log.пВК_ПС;
				res := Serials.Ok;
			иначе
				Закрой;
				res := Serials.NoSuchPort;
			всё;
		кон Open;

		проц {перекрыта}Закрой*;
		перем err: цел32;
		нач
			если fd > 0  то
				err:= Unix.close( fd );
				fd := 0
			всё
		кон Закрой;

		проц {перекрыта}Available*(): размерМЗ;
		перем
			num, err: целМЗ;
		нач
		 	err := Unix.ioctl( fd, Unix.FIONREAD, адресОт(num) );
			возврат num;
		кон Available;

		проц {перекрыта}ReceiveChar*( перем ch: симв8;  перем res: целМЗ );
		перем r: размерМЗ;
		нач
			r := Unix.read( fd, адресОт(ch), 1);  res := цел32(r);
			если res = 1 то
				увел( charactersReceived );
				res := Serials.Ok;
			иначе
				res := Serials.TransportError;
			всё;
		кон ReceiveChar;

		(** Send - Send a byte to the specified port.  Waits until buffer space is available.  res = 0 iff ok. *)
		проц {перекрыта}SendChar*( ch: симв8;  перем res: целМЗ );
		перем r: размерМЗ;
		нач
			r := Unix.write( fd, адресОт(ch), 1);  res := цел32(r);
			если res = 1 то
				увел( charactersSent );
				res := Serials.Ok;
			иначе
				res := Serials.TransportError;
			всё;
		кон SendChar;

		проц {перекрыта}ЗапишиВПоток*( конст buf: массив из симв8;  ofs, len: размерМЗ; propagate: булево; перем res: целМЗ );
		перем r: размерМЗ;
		нач
			утв ( длинаМассива( buf ) >= ofs + len );   (* array bound check not implemented in Kernel32.WriteFile *)
			r := Unix.write( fd, адресОт(buf[ofs]), len );
			если r < 0 то
				res := Serials.TransportError;
			иначе
				увел( charactersSent, r );
				res := Serials.Ok;
			всё;
		кон ЗапишиВПоток;

		проц {перекрыта}ПрочтиИзПотока*( перем buf: массив из симв8;  ofs, size, min: размерМЗ;  перем len: размерМЗ; перем res: целМЗ );
		перем i, l: размерМЗ;  read: размерМЗ;
		нач
			утв ( длинаМассива( buf ) >= ofs + size );
			утв ( длинаМассива( buf ) >= ofs + min );   (* array bound check not implemented in Kernel32.ReadFile *)
			res := Serials.Ok; len := 0;
			i := ofs;  l := Available();
			нцПока (res = Serials.Ok) и ( (min > 0) или ((l > 0) и (size > 0)) ) делай  (* fof 060804 *)
				если l > size то l := size всё;
				если  l > 0 то
					read := Unix.read(fd, адресОт(buf[i]), l );
					если  read = l  то
						charactersReceived := charactersReceived + цел32(read);
						умень( min, l );  умень( size, l );  увел( len, l );  увел( i, l );
					иначе
						(* If we've already received <min> bytes, <res> will become Serials.Ok later *)
						res := Serials.TransportError;
					всё;
				всё;
				l := Available();
				если (res = Serials.Ok) и ( (min > 0) или ((l > 0) и (size > 0)) ) то
					Objects.Sleep(1);
				всё;
			кц;
			если min <= 0 то  res := Serials.Ok  всё;
		кон ПрочтиИзПотока;

		(** Get the port state: state (open, closed), speed in bps, no. of data bits, parity, stop bit length. *)
		проц {перекрыта}GetPortState*( перем openstat : булево;  перем bps, data, parity, stop : цел32 );
		перем
			err, br: цел32;  t: мнвоНаБитахМЗ;  tio: Unix.Termios;
		нач
			если fd > 0 то
				err := tcgetattr( fd, tio );
				br := cfgetispeed( tio );
				если br = Unix.B0 то bps := 0
				аесли br = Unix.B50 то bps := 50
				аесли br = Unix.B75 то bps := 75
				аесли br = Unix.B110 то bps := 110
				аесли br = Unix.B134 то bps := 134
				аесли br = Unix.B150 то bps := 150
				аесли br = Unix.B200 то bps := 200
				аесли br = Unix.B300 то bps := 300
				аесли br = Unix.B600 то bps := 600
				аесли br = Unix.B1200 то bps := 1200
				аесли br = Unix.B1800 то bps := 1800
				аесли br = Unix.B2400 то bps := 2400
				аесли br = Unix.B4800 то bps := 4800
				аесли br = Unix.B9600 то bps := 9600
				аесли br = Unix.B19200 то bps := 19200
				аесли br = Unix.B38400 то bps := 38400
				аесли br = Unix.B57600 то bps := 57600
				аесли br = Unix.B115200 то bps := 115200
				аесли br = Unix.B230400 то bps := 230400
				иначе bps := -1
				всё;
				t := tio.cflags*Unix.CSIZE;
				если t = Unix.CS8 то data := 8
				аесли t = Unix.CS7 то data := 7
				аесли t = Unix.CS6 то data := 6
				иначе data := 5
				всё;
				если tio.cflags*Unix.PARENB = {} то parity := Serials.ParNo
				аесли tio.cflags*Unix.PARODD = Unix.PARODD то parity := Serials.ParOdd
				иначе parity := Serials.ParEven
				всё;
				если tio.cflags*Unix.CSTOPB = {} то  stop := Serials.Stop1  иначе  stop := Serials.Stop2  всё
			иначе
				openstat := ложь
			всё
		кон GetPortState;

		(** Clear the specified modem control lines.  s may contain DTR, RTS & Break. *)
		проц {перекрыта}ClearMC*( s: мнвоНаБитахМЗ );
		перем
			err : цел32;  stat: мнвоНаБитахМЗ;
		нач
			err := Unix.ioctl( fd, Unix.TIOCMGET , адресОт(stat) );
			если Serials.DTR в s то  исключиИзМнваНаБитах( stat, TIOCM_DTR )  всё;
			если Serials.RTS в s то  исключиИзМнваНаБитах( stat, TIOCM_RTS )  всё;
			err := Unix.ioctl( fd, Unix.TIOCMSET , адресОт(stat) );
		кон ClearMC;

		(** Set the specified modem control lines.  s may contain DTR, RTS & Break. *)
		проц {перекрыта}SetMC*( s: мнвоНаБитахМЗ );
		перем
			err : цел32;  stat: мнвоНаБитахМЗ;
		нач
			err := Unix.ioctl( fd, Unix.TIOCMGET , адресОт(stat) );
			если Serials.DTR в s то  включиВоМнвоНаБитах( stat, TIOCM_DTR )  иначе  исключиИзМнваНаБитах( stat, TIOCM_DTR )  всё;
			если Serials.RTS в s то  включиВоМнвоНаБитах( stat, TIOCM_RTS )  иначе  исключиИзМнваНаБитах( stat, TIOCM_RTS )  всё;
			err := Unix.ioctl( fd, Unix.TIOCMSET , адресОт(stat) );
		кон SetMC;

		(** Return the state of the specified modem control lines. s contains
			the current state of DSR, CTS, RI, DCD & Break Interrupt. *)
		проц {перекрыта}GetMC*( перем s: мнвоНаБитахМЗ );
		перем
			err : цел32;  stat: мнвоНаБитахМЗ;
		нач
			err := Unix.ioctl( fd, Unix.TIOCMGET , адресОт(stat) );
			если TIOCM_DTR в stat то  включиВоМнвоНаБитах( s, Serials.DTR )  всё;
			если TIOCM_RTS в stat то  включиВоМнвоНаБитах( s, Serials.RTS )  всё;
			(* IF TIOCM_LE IN stat THEN INCL(s, Serials.Break) END;  *) (* ?? *)
			если TIOCM_DSR в stat то  включиВоМнвоНаБитах( s, Serials.DSR )  всё;
			если TIOCM_CTS в stat то  включиВоМнвоНаБитах( s, Serials.CTS )  всё;
			если TIOCM_RI в stat то  включиВоМнвоНаБитах( s, Serials.RI )  всё;
			если TIOCM_CAR в stat то  включиВоМнвоНаБитах( s, Serials.DCD )  всё;
		кон GetMC;


	кон Port;

	проц Install*();
	перем i: цел32;
		port: Port;
		name, ttyname: массив 128 из симв8;
	нач
		нцДля i := 0 до NumberOfPorts - 1 делай
			name:="COM ";
			name[3]:= симв8ИзКода( i + кодСимв8("1") );
			ttyname:="/dev/ttySx";
			ttyname[ 9]:= симв8ИзКода( i + кодСимв8("0") );
			нов( port, i , ttyname);
			Serials.RegisterOnboardPort( i+1 , port, name, ttyname);
		кц;
	кон Install;

	проц Map*(context: Commands.Context);
	перем	number: цел32; name, ttyname: массив 128 из симв8; port: Port;
	нач
		если context.arg.ПропустиБелоеПолеИЧитайЦел32(number, ложь) и context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(ttyname) то
			name := "COM";
			Strings.AppendInt(name, number);
			нов(port, number, ttyname);
			Serials.RegisterOnboardPort( number, port, name, ttyname);
		всё;
	кон Map;


	проц Initialize;
	нач
		Unix.Dlsym( Unix.libc, "tcgetattr",		адресОт( tcgetattr ) );
		Unix.Dlsym( Unix.libc, "tcsetattr",		адресОт( tcsetattr ) );
		Unix.Dlsym( Unix.libc, "cfgetispeed",		адресОт( cfgetispeed ) );
		Unix.Dlsym( Unix.libc, "cfgetospeed",	адресОт( cfgetospeed ) );
		Unix.Dlsym( Unix.libc, "cfsetispeed",		адресОт( cfsetispeed ) );
		Unix.Dlsym( Unix.libc, "cfsetospeed",		адресОт( cfsetospeed ) );
	кон Initialize

нач
	Initialize;
(*	Install;*)
кон V24.

V24.Install ~
V24.Map 5 ttyUSB0 ~

Serials.Show ~

System.Free V24~





	install /dev/ttySx as :

	COM1 -> /dev/ttyS0
	COM2 -> /dev/ttyS1
	COM3 -> /dev/ttyS2
	COM4 -> /dev/ttyS3

	On Solaris and Darwin hosts the tty lines have different names.
	To make them useable for Aos create symbolic links

	Solaris:	ln -s  /dev/ttya  /dev/ttyS0


	------------------------------------------

	you must have access rights to serial port:

	first method:
	must give username exclusive access to the device file, the first serial port on linux:

	# chown username /dev/ttyS0
	# chmod 0600 /dev/ttyS0

	#  ls -ls  /dev/ttyS0

	second method: allow anybody to use serial port 0 (be carefull for security)

	# chmod a+rw  /dev/ttyS0

	---------------------
	permanent setting:

	# groups

	see which groups you belong
	if you dont belong to "dialout" group, add yourself to this group by

	# sudo usermode -a -G dialout your_username
	or
	# sudo adduser your_username group
	 logout and login to system. that is all.


	  removing:
	# sudo deluser your_username group

	---------------------
	------------------------------------------

