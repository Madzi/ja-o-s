модуль SVNArgument; (** AUTHOR "rstoll"; PURPOSE "parse context and provide query methods"; *)

(*
	TODO
	 - strings/arrays dynamisch machen
*)

использует
	Commands, Потоки;

конст
	ResOK* = 0;
	ResKEYNOTFOUND* = -1;
	ResEXPECTEDPARAMCOUNTFAILED* = -2;


	MaxArgumentValues = 32;

тип

	Argument *= окласс
	перем
		next, error*, last, unkeyed : цел16;
		msg* : массив 256 из симв8;

		arguments : массив 256 из запись
			key : массив 32 из симв8;
			value : массив MaxArgumentValues,256 из симв8;
			expParams : цел16;
			isset : булево;
		кон;

		arguments2 : массив MaxArgumentValues, 256 из симв8; (* dynamisch machen *)


		проц &Init*;
		нач
			next := 0;
			error := ResOK;
		кон Init;


		проц Push* ( конст key : массив из симв8; expParams : цел16 );
		нач
			утв ( next < MaxArgumentValues );

			копируйСтрокуДо0 ( key, arguments[next].key );
			arguments[next].expParams := expParams;
			arguments[next].isset := ложь;

			увел ( next );
		кон Push;


		проц GetKeyedArgument* ( конст key : массив из симв8; перем value : массив из симв8; index : цел16 );
		нач
			если IsSet ( key ) и (index < next) то
				копируйСтрокуДо0 ( arguments[last].value[index], value );
			всё;
		кон GetKeyedArgument;


		проц GetUnkeyedArgument* ( перем value : массив из симв8; index : цел16 );
		нач
			если index < unkeyed то (* muss man array bounds testen? :) *)
				копируйСтрокуДо0 ( arguments2[index], value );
			всё;
		кон GetUnkeyedArgument;


		проц CountUnkeyedArguments* (): цел16;
		нач
			возврат unkeyed;
		кон CountUnkeyedArguments;


		проц IsSet* ( конст key : массив из симв8 ) : булево;
		перем
			idx : цел16;
			found : булево;

		нач
			FindKey ( key, idx, found );

			если ~found то возврат ложь всё;
			last := idx;

			возврат arguments[idx].isset;
		кон IsSet;


		проц Read* ( context : Commands.Context );
		перем
			ch : симв8;
			arg : Потоки.Чтец;
			s : массив 256 из симв8;
			a, j : цел16;
			found : булево;

		нач
			unkeyed := 0;
			arg := context.arg;

			нцПока( arg.кодВозвратаПоследнейОперации = 0 ) делай
				arg.ПропустиБелоеПоле();

				если arg.ПодглядиСимв8() = '\' то
					arg.чСимв8( ch );
					arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках ( s );

					FindKey ( s, a, found );

					если ~found то
						копируйСтрокуДо0 ( s, msg );
						error := ResKEYNOTFOUND;
						возврат;
					всё;

					arguments[a].isset := истина;

					j := 0;
					нцПока( (arg.кодВозвратаПоследнейОперации = 0) и (j < arguments[a].expParams) ) делай
						утв ( j < MaxArgumentValues );

						arg.ПропустиБелоеПоле();
						arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках ( arguments[a].value[j] );

						увел ( j );
					кц;

					если j # arguments[a].expParams то
						копируйСтрокуДо0 ( s, msg );
						error := ResEXPECTEDPARAMCOUNTFAILED;
						возврат;
					всё;
				иначе
					утв ( unkeyed < MaxArgumentValues );

					arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках ( arguments2[unkeyed] );

					если arguments2[unkeyed] # "" то
						увел ( unkeyed );
					всё;
				всё;
			кц;
		кон Read;


		проц FindKey ( конст s : массив из симв8; перем index : цел16; перем found : булево );
		нач
			found := ложь;
			index := 0;

			нцПока( (index < next) и ~found ) делай
				если arguments[index].key = s то
					found := истина;
				иначе
					увел ( index );
				всё;
			кц;

			утв ( (index = next) или found);
		кон FindKey;


	нач
	кон Argument;

кон SVNArgument.


System.Free Argument ~
PC.Compile \s \W SVNArgument.Mod ~
