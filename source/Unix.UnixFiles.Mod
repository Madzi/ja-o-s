(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль UnixFiles;   (** AUTHOR "gf"; PURPOSE "Unix file systems" *)

(*  derived fron (SPARCOberon) Files.Mod by J. Templ 1.12. 89/14.05.93 *)

использует S := НИЗКОУР, Unix, Kernel, Modules, Log := ЛогЯдра, Files, Commands;


конст
	NBufs = 4;  Bufsize = 4096;  NoDesc = -1;

	Open = 0;  Create = 1;  Closed = 2;	(* file states *)

	NoKey = -1;
	CreateFlags = Unix.rdwr + Unix.creat + Unix.trunc;

	TraceCollection = 0;
	Trace = {};
	Separator = 0AX;
перем
	tempno: цел16;
	openfiles: цел32;

	searchPath: массив 1024 из симв8;
	cwd: массив 256 из симв8;

	unixFS: UnixFileSystem; (* must be unique *)
	collection: Collection; (* must be unique *)

тип
	Filename = массив 256 из симв8;

	NameSet = окласс
			перем
				name: массив 64 из симв8;
				left, right: NameSet;

				проц Add( конст filename: массив из симв8 ): булево;
					(* add filename if it not already exists. else return false *)
				нач
					если filename = name то  возврат ложь  всё;
					если filename < name то
						если left = НУЛЬ то  нов( left, filename );  возврат истина
						иначе  возврат left.Add( filename )
						всё
					иначе
						если right = НУЛЬ то  нов( right, filename );  возврат истина
						иначе  возврат right.Add( filename )
						всё
					всё
				кон Add;

				проц & Init( конст filename: массив из симв8 );
				нач
					копируйСтрокуДо0( filename, name );
					left := НУЛЬ; right := НУЛЬ
				кон Init;

			кон NameSet;

	AliasFileSystem = окласс (Files.FileSystem)
		перем
			fs: UnixFileSystem;

			проц & Init*( realFS: UnixFileSystem);
			нач
				сам.fs := realFS;
			кон Init;

			проц {перекрыта}New0(конст name: массив из симв8 ): Files.File;
			перем f: Files.File;
			нач
				f := fs.New0( name );
				если f # НУЛЬ то  f.fs := сам  всё;
				возврат f;
			кон New0;

			проц {перекрыта}Old0(конст name: массив из симв8 ): Files.File;
			перем f: Files.File;
			нач
				f :=  fs.Old0( name );
				если f # НУЛЬ то  f.fs := сам  всё;
				возврат f;
			кон Old0;

			проц {перекрыта}Delete0(конст name: массив из симв8;  перем key: цел32; перем res: целМЗ );
			нач
				fs.Delete0( name, key, res );
			кон Delete0;

			проц {перекрыта}Rename0(конст old, new: массив из симв8;  fold: Files.File;  перем res: целМЗ );
			нач
				fs.Rename0( old, new, fold, res );
			кон Rename0;

			проц {перекрыта}Enumerate0(конст mask: массив из симв8;  flags: мнвоНаБитахМЗ;  enum: Files.Enumerator );
			нач
				fs.Enumerate0( mask, flags, enum );
			кон Enumerate0;

			проц {перекрыта}FileKey(конст name: массив из симв8 ): цел32;
			перем
			нач
				возврат fs.FileKey( name );
			кон FileKey;

			проц {перекрыта}CreateDirectory0(конст name: массив из симв8;  перем res: целМЗ );
			нач
				fs.CreateDirectory0( name, res );
			кон CreateDirectory0;

			проц {перекрыта}RemoveDirectory0(конст name: массив из симв8;  force: булево;  перем key: цел32; перем res: целМЗ );
			нач
				fs.RemoveDirectory0( name, force, key, res );
			кон RemoveDirectory0;

			проц {перекрыта}Has(конст name: массив из симв8; перем fullName: массив из симв8; перем flags: мнвоНаБитахМЗ): булево;
			нач
				возврат fs.Has(name, fullName, flags);
			кон Has;

	кон AliasFileSystem;

	SearchByFstat = окласс
	перем
		found: File;
		stat: Unix.Status;

		проц Init( s: Unix.Status );
		нач
			found := НУЛЬ;
			stat := s;
		кон Init;

		проц EnumFile( f: динамическиТипизированныйУкль;  перем cont: булево );
		нач
			просейТип f: File делай
				если (stat.ino = f.ino) и (stat.dev = f.dev) то
					(* possible different name but same file! *)
					ResetBuffers( f, stat );
					found := f; cont := ложь;
				всё;
			всё;
		кон EnumFile;

	кон SearchByFstat;




	Collection = окласс  (* methods in Collection shared by objects Filesystem and File *)
	перем oldFiles, newFiles: Kernel.FinalizedCollection;
		ssearch: SearchByFstat;

		проц & Init*;
		нач
			нов( oldFiles );  нов( newFiles );   нов( ssearch );
		кон Init;

		проц AddNew( F: File );
		нач {единолично}
			если TraceCollection в Trace то Log.пСтроку8( "Collections.AddNew: " );  Log.пСтроку8( F.workName );  Log.пВК_ПС;  всё;
			newFiles.Add( F, FinalizeFile );
		кон AddNew;

		проц AddOld( F: File );
		нач {единолично}
			если TraceCollection в Trace то Log.пСтроку8( "Collections.AddOld: " );  Log.пСтроку8( F.workName );  Log.пВК_ПС;  всё;
			oldFiles.Add( F, FinalizeFile );
		кон AddOld;

		проц ByStat(конст stat: Unix.Status): File;
		нач{единолично}
			ssearch.Init(stat);
			oldFiles.EnumerateN(ssearch.EnumFile);
			если ssearch.found = НУЛЬ то
				newFiles.EnumerateN(ssearch.EnumFile)
			всё;

			если TraceCollection в Trace то
				Log.пСтроку8( "Collections.ByStatus: " );  Log.пВК_ПС;
				если ssearch.found = НУЛЬ то Log.пСтроку8("not found") иначе Log.пСтроку8("found") всё;
			всё;

			возврат ssearch.found;
		кон ByStat;

		проц Finalize;
		нач {единолично}
			если TraceCollection в Trace то Log.пСтроку8( "Collections.Finalize " );  Log.пВК_ПС;  всё;
			newFiles.Enumerate( EnumFinalize );  newFiles.Clear();
			oldFiles.Enumerate( EnumFinalize );  oldFiles.Clear();
		кон Finalize;

		проц Collect;
		нач{единолично}
			Kernel.GC; Kernel.GC; (* make sure that no file enumeration takes place during GC --> limit the chance to revive files with concurrent GC *)
		кон Collect;

		проц FinalizeFile( obj: динамическиТипизированныйУкль );
		перем F: File;
		нач
			F := obj( File );
			если TraceCollection в Trace то Log.пСтроку8( "Collections.FinalizeFile " );  Log.пСтроку8( F.workName );  Log.пВК_ПС;  всё;
			F.Finalize()
		кон FinalizeFile;

	кон Collection;


	UnixFileSystem* = окласс (Files.FileSystem)

				проц & Init;
				нач
					prefix := "";  vol := НУЛЬ;  desc := "UnixFS"
				кон Init;


				проц {перекрыта}New0*(конст name: массив из симв8 ): Files.File;
				перем f: File; stat: Unix.Status;
					  res, err: цел32;
					  path, nameonly: массив 512 из симв8;
				нач {единолично}
					(*first check if the path actually exits first using fstat. fstat returns -1 and sets erno to ENOENT when a component of the path doesn't exist or the entire path is empty*)
					Files.SplitPath( name, path, nameonly );
					res:=Unix.stat( адресОт( path ), stat ) ;
					err:=Unix.errno();
					если (name="") или (path="") или (res>=0) или (err#Unix.ENOENT) то
						нов( f, сам );
						f.workName := "";  копируйСтрокуДо0( name, f.registerName );
						f.fd := NoDesc;  f.state := Create;  f.fsize := 0;  f.fpos := 0;
						f.swapper := -1;   (*all f.buf[i] = NIL*)
						f.key := NoKey;  f.fs := сам;
						f.tempFile := name = "";
						возврат f;
					иначе
						Log.пСтроку8( "UnixFileSystem.New0: file allocation failed. Probably a nonexistent path." );  Log.пВК_ПС;
						возврат НУЛЬ;
					всё;
				кон New0;


				проц IsDirectory( перем stat: Unix.Status ): булево;
				перем mode: цел32;
				нач
					mode := stat.mode;
					возврат нечётноеЛи¿( mode DIV 4000H )
				кон IsDirectory;


				проц {перекрыта}Old0*(конст name: массив из симв8 ): Files.File;
				перем f: File;  stat: Unix.Status;  fd, r, pos: цел32;
					oflags: мнвоНаБитах32;  nextdir, path: Filename;
				нач  {единолично}
					если name = "" то  возврат НУЛЬ  всё;

					если IsFullName( name ) то
						копируйСтрокуДо0( name, path );  nextdir := "";
					иначе
						pos := 0;  ScanPath( pos, nextdir );  MakePath( nextdir, name, path );
						ScanPath( pos, nextdir )
					всё;

					нц
						f := НУЛЬ;
						r := Unix.access( адресОт( path ), Unix.R_OK );
						если r >= 0 то
							r := Unix.access( адресОт( path ), Unix.W_OK );
							если r < 0 то  oflags := Unix.rdonly  иначе  oflags := Unix.rdwr  всё;
							fd := UnixOpen( адресОт( path ), oflags, 0 );
							если fd >= 0 то
								r := Unix.fstat( fd, stat );
								f := collection.ByStat(stat);
								если f # НУЛЬ то
									(* use the file already cached *)  r := Unix.close( fd );  прервиЦикл
								иначе
									нов( f, сам );
									f.fd := fd;  f.dev := stat.dev;  f.ino := stat.ino;
									f.mtime := stat.mtime.sec;  f.fsize := stat.size;  f.fpos := 0;
									f.state := Open;  f.swapper := -1;   (*all f.buf[i] = NIL*)
									копируйСтрокуДо0( path, f.workName );  f.registerName := "";
									f.tempFile := ложь;
									если IsDirectory( stat ) то
										f.flags := {Files.Directory, Files.ReadOnly}
									аесли oflags = Unix.rdonly то
										f.flags := {Files.ReadOnly}
									всё;
									f.key := NoKey;  f.fs := сам;
									IncOpenFiles();
									collection.AddOld(f);
									прервиЦикл
								всё
							иначе
								(* file exists but open failed *)  f := НУЛЬ;  прервиЦикл
							всё
						аесли nextdir # "" то
							MakePath( nextdir, name, path );  ScanPath( pos, nextdir );
						иначе
							f := НУЛЬ;  прервиЦикл
						всё;
					кц; (* loop *)
					утв( (f = НУЛЬ) или (f.fd >= 0) );
					возврат f
				кон Old0;

				(** Return the unique non-zero key of the named file, if it exists. *)
				проц {перекрыта}FileKey*(конст name: массив из симв8 ): цел32;
				(* 	Can not be used for Unix files as SIGNED32 is too small.
					In the Unix filesystem a file is identified by
					- dev	(64 bit (Linux), 32 bit (Solaris, Darwin))	+
					- ino	(32 bit)
				*)
				нач
					возврат 0
				кон FileKey;

				проц {перекрыта}Delete0*(конст name: массив из симв8;  перем key: цел32; перем res: целМЗ );
				перем r: цел32;
				нач  {единолично}
					r := Unix.unlink( адресОт( name ) );
					если r = 0 то  res := Files.Ok
					иначе  res := Unix.errno( )
					всё;
					key := 0;
				кон Delete0;


				(* return remaining old file, if any *)
				проц TryRename*( old, new: массив из симв8;  f: Files.File;  перем res: целМЗ ): цел32;
				конст Bufsize = 4096;
				перем fdold, fdnew, fo, r: цел32;  n, n2: размерМЗ;  ostat, nstat: Unix.Status;
					buf: массив Bufsize из симв8;
				нач {единолично}
					fo := NoDesc;
					r:= Unix.stat( адресОт( old ), ostat );

					если r >= 0 то
						r := Unix.stat( адресОт( new ), nstat );
						если (r >= 0) и ((ostat.dev # nstat.dev) или (ostat.ino # nstat.ino)) то
							 r := Unix.unlink( адресОт( new ) )  (* work around stale nfs handles *);
						всё;
						r := Unix.rename( адресОт( old ), адресОт( new ) );
						если r < 0 то (* could not rename, try copy *)
							res := Unix.errno( );
							если (res = Unix.EXDEV) или (res = Unix.ETXTBSY) то  (* cross device link, move the file / file busy frequently happens in VirtualBox *)
								fdold := UnixOpen( адресОт( old ), Unix.rdonly, 0 );
								fo := fdold;
								если fdold < 0 то
									res := Unix.errno( );
									возврат NoDesc;
								всё;
								fdnew := UnixOpen( адресОт( new ), Unix.rdwr + Unix.creat + Unix.trunc, Unix.rwrwr );
								если fdnew < 0 то
									res := Unix.errno( );
									возврат NoDesc;
								всё;
								нцДо
									n := UnixRead( fdold, адресОт( buf ), Bufsize );
									если n > 0 то
										n2 := UnixWrite( fdnew, адресОт( buf ), n );
										если n2 < 0 то
											r := Unix.close( fdold );
											r := Unix.close( fdnew );
											возврат NoDesc;
										всё;
									всё
								кцПри n = 0;
								r := Unix.unlink( адресОт( old ) );
								r := Unix.close( fdold );
								r := Unix.close( fdnew );
								res := Files.Ok
							иначе
								возврат NoDesc (* res is Unix.rename return code *)
							всё
						всё;
						res := Files.Ok
					иначе
						res := Unix.errno();
					всё;
					возврат fo;
				кон TryRename;

				проц {перекрыта}Rename0*(конст old, new: массив из симв8;  f: Files.File;  перем res: целМЗ );
				перем of: цел32;
				нач
					of := TryRename(old, new, f, res);
				кон Rename0;


				проц {перекрыта}CreateDirectory0*(конст path: массив из симв8;  перем res: целМЗ );
				перем r: цел32;
				нач {единолично}
					r := Unix.mkdir( адресОт( path ), Unix.rwxrwxrwx );
					если r = 0 то  res := Files.Ok
					иначе res := Unix.errno( )
					всё
				кон CreateDirectory0;


				проц {перекрыта}RemoveDirectory0*(конст path: массив из симв8;  force: булево;  перем key: цел32; перем res: целМЗ );
				перем r: цел32;
				нач {единолично}
					r := Unix.rmdir( адресОт( path ) );
					если r = 0 то  res := Files.Ok
					иначе  res := Unix.errno( )
					всё
				кон RemoveDirectory0;


				проц {перекрыта}Enumerate0*(конст mask: массив из симв8;  flags: мнвоНаБитахМЗ;  enum: Files.Enumerator );
				перем
					path, filemask: Filename;
					isPath: булево;
					i, j: цел16;  dirName, fileName, fullName, xName: Filename;
					checkSet: NameSet;  ent: Unix.Dirent;

					проц GetEntryName;
					перем i: цел16;  adr: адресВПамяти;
					нач
						i := -1;  adr := адресОт( ent.name );
						нцДо  увел( i );  S.прочтиОбъектПоАдресу( adr, fileName[i] );  увел( adr )  кцПри fileName[i] = 0X
					кон GetEntryName;

					проц EnumDir( конст dirName: массив из симв8 );
					перем
						dir: адресВПамяти;
						tm: Unix.TmPtr;  date, time: цел32;
						stat: Unix.Status; r: цел32; p: адресВПамяти;
					нач
						dir := Unix.opendir( адресОт( dirName ) );
						если dir # 0 то
							ent := Unix.readdir( dir );
							нцПока ent # НУЛЬ делай
								копируйСтрокуДо0( dirName, fullName );

								GetEntryName;  AppendName( fullName, fileName );
								если (fileName[0] # '.')  и Match( fileName, filemask, 0, 0 ) то
									если checkSet.Add( fileName ) то  (* not a covered name *)
										r := Unix.stat( адресОт( fullName ), stat );
										tm := Unix.localtime( stat.mtime );
										date := tm.year*200H + (tm.mon + 1)*20H + tm.mday;
										time := tm.hour*1000H + tm.min*40H + tm.sec;
										flags := {};
										если IsDirectory( stat ) то
											flags := {Files.ReadOnly, Files.Directory}
										иначе
											r := Unix.access( адресОт( fullName ), Unix.W_OK );
											если r < 0 то  flags := {Files.ReadOnly}  всё
										всё;
										p := Unix.realpath(адресОт(fullName), адресОт(xName));
										если (p # 0) то копируйСтрокуДо0(xName, fullName) всё;
										enum.PutEntry( fullName, flags, time, date, цел32 (stat.size) );
									всё
								всё;
								ent := Unix.readdir( dir );
							кц;
							Unix.closedir( dir )
						всё;
					кон EnumDir;


				нач {единолично}
					Files.SplitName( mask, prefix, fullName );
					Files.SplitPath( fullName, path, filemask );
					нов( checkSet, "M###N" );
					isPath:= path#"";

					если isPath то
						CleanPath(path); (*get rid of xxx/../xxx and  xxx/./xxx in the path string*)
					всё;
					если isPath и (path[0] = '/') то (*check for absolute path*)
						EnumDir( path);
					иначе (*no path or relative path*)
						i := 0;  j := 0;
						нц (*go through the search paths, every time a complete search path has been traversed, look for the element there*)
							если (searchPath[i] = Separator) или (searchPath[i] = 0X) то
								dirName[j] := 0X;
								если isPath то (*if relative path: add relative path to the current search path*)
									Files.JoinPath(dirName, path, dirName);
								всё;
								EnumDir( dirName );
								если searchPath[i] = 0X то  прервиЦикл
								иначе  увел( i );  j := 0
								всё
							иначе
								dirName[j] := searchPath[i];  увел( j );  увел( i )
							всё
						кц
					всё;
					checkSet := НУЛЬ;
				кон Enumerate0;

				проц {перекрыта}Has(конст name: массив из симв8; перем fullName: массив из симв8; перем flags: мнвоНаБитахМЗ): булево;
				перем r: цел32; p: адресВПамяти; stat: Unix.Status;
				нач
					r := Unix.stat( адресОт( name ), stat );
					если r  # 0 то возврат ложь всё;
					flags := {};
					если IsDirectory( stat ) то
						flags := {Files.ReadOnly, Files.Directory}
					иначе
						r := Unix.access( адресОт( name ), Unix.W_OK );
						если r < 0 то  flags := {Files.ReadOnly}  всё
					всё;
					p := Unix.realpath(адресОт(name), адресОт(fullName));
					если (p = 0) то копируйСтрокуДо0(name, fullName) всё; (* no success *)
					возврат истина;
				кон Has;

	кон UnixFileSystem;


	Buffer =	укль на запись (Files.Hint)
					chg: булево;
					org, size: размерМЗ;
					data: массив Bufsize из симв8;
				кон;

	File* = окласс (Files.File)
			перем
				fd: цел32;
				workName, registerName: Filename;
				tempFile: булево;
				dev: Unix.dev_t;
				ino: Unix.ino_t;
				mtime: цел64;
				fsize, fpos: размерМЗ;
				bufs: массив NBufs из Buffer;
				swapper: размерМЗ; state: цел32;


				проц & Init( fs: Files.FileSystem );
				нач
					сам.fs := fs;  flags := {};
				кон Init;

				проц CreateUnixFile;
				перем
					stat: Unix.Status;  r: цел32;
				нач
					если state = Create то
						GetTempName( registerName, workName )
					аесли state = Closed то
						если registerName # "" то
							(* shortcut renaming in Register0 *)
							workName := registerName;  registerName := ""
						иначе
							(* file has been finally closed *)  возврат
						всё
					всё;
					r := Unix.unlink( адресОт( workName ) );
					(*unlink first to avoid stale NFS handles and to avoid reuse of inodes*)

					fd := UnixOpen( адресОт( workName ), CreateFlags, Unix.rwrwr );
					если fd >= 0 то
						r := Unix.fstat( fd, stat );
						dev := stat.dev;  ino := stat.ino;  mtime := stat.mtime.sec;
						state := Open;  fpos := 0;
						IncOpenFiles( );
						collection.AddNew( сам );
					иначе
						Halt( сам, истина, "UnixFiles.File.CreateUnixFile: open failed" );
					всё
				кон CreateUnixFile;


				проц Flush( buf: Buffer );
				перем n: размерМЗ;  res: целМЗ;  stat: Unix.Status;
				нач
					если buf.chg то
						если fd = NoDesc то  CreateUnixFile  всё;
						если buf.org # fpos то
							если Unix.lseek( fd, buf.org, 0 ) = -1 то
								Halt( сам, истина, "UnixFiles.File.Flush: lseek failed" )
							всё
						всё;
						n := UnixWrite( fd, адресОт( buf.data ), buf.size );
						если n < 0 то  Halt( сам, истина, "UnixFiles.File.Flush: write failed" )  всё;
						fpos := buf.org + buf.size;  buf.chg := ложь;
						res := Unix.fstat( fd, stat );  mtime := stat.mtime.sec
					всё
				кон Flush;


				проц {перекрыта}Set*( перем r: Files.Rider;  pos: Files.Position );
				нач {единолично}
					SetX( r, pos )
				кон Set;

				проц SetX( перем r: Files.Rider;  p: Files.Position );
				перем  org, offset, i, n: размерМЗ;  buf: Buffer;
				нач
					r.file := сам;  r.fs := fs;
					если p > fsize то  p := цел32(fsize)
					аесли p < 0 то  p := 0
					всё;
					offset := размерМЗ(p) остОтДеленияНа Bufsize;  org := размерМЗ(p) - offset;
					i := 0;
					нцПока (i < NBufs) и (bufs[i] # НУЛЬ) и (org # bufs[i].org) делай  увел( i )  кц;
					если i < NBufs то
						если bufs[i] = НУЛЬ то
							нов( buf );  buf.chg := ложь;  buf.org := -1;
							bufs[i] := buf
						иначе
							swapper := i;
							buf := bufs[swapper];  Flush( buf )
						всё
					иначе
						swapper := (swapper + 1) остОтДеленияНа NBufs;
						buf := bufs[swapper];  Flush( buf )
					всё;
					если buf.org # org то
						если org = fsize то
							buf.size := 0
						иначе
							если fd = NoDesc то  CreateUnixFile  всё;
							если fpos # org то
								если Unix.lseek( fd, org, 0 ) = -1 то
									Halt( сам, истина, "UnixFiles.File.Set: lseek failed" )
								всё
							всё;
							n := UnixRead( fd, адресОт( buf.data ), Bufsize );
							если n < 0 то
								если p < fsize то  Halt( сам, истина, "UnixFiles.File.Set: read failed" )
								иначе n := 0
								всё
							всё;
							fpos := org + n;  buf.size := n
						всё;
						buf.org := org;  buf.chg := ложь
					иначе
						org := цел32(buf.org)
					всё;

					r.hint := buf;  r.apos := org;  r.bpos := offset;
					r.res := 0;  r.eof := ложь;
				кон SetX;


				проц {перекрыта}Pos*( перем r: Files.Rider ): Files.Position;
				нач
					возврат r.apos + r.bpos
				кон Pos;


				проц {перекрыта}Read*( перем r: Files.Rider;  перем x: симв8 );
				перем offset: размерМЗ;  buf: Buffer;
				нач  {единолично}
					buf := r.hint(Buffer);  offset := r.bpos;
					если r.apos # buf.org то
						SetX( r, r.apos + offset );
						buf := r.hint(Buffer);  offset := r.bpos
					всё;
					если (offset < buf.size) то
						x := buf.data[offset];  r.bpos := offset + 1
					аесли r.apos + offset < fsize то
						SetX( r, r.apos + offset );
						x := r.hint(Buffer).data[0];  r.bpos := 1
					иначе
						x := 0X;  r.eof := истина
					всё
				кон Read;

				проц {перекрыта}ReadBytes*( перем r: Files.Rider;  перем x: массив из симв8;  ofs, len: размерМЗ );
				перем xpos, offset, restInBuf, min: размерМЗ;  buf: Buffer;
				нач  {единолично}
					x[ofs] := 0X;  xpos := ofs;
					buf := r.hint(Buffer);  offset := r.bpos;
					нцПока len > 0 делай
						если (r.apos # buf.org) или (offset >= Bufsize) то
							SetX( r, r.apos + цел32(offset) );
							buf := r.hint(Buffer);  offset := r.bpos
						всё;
						restInBuf := buf.size - offset;
						если restInBuf = 0 то  r.res := len;  r.eof := истина;  возврат
						аесли len > restInBuf то  min := цел32(restInBuf)
						иначе  min := len
						всё;
						S.копируйПамять( адресОт( buf.data ) + offset, адресОт( x ) + xpos, min );
						увел( offset, min );  r.bpos := цел32(offset);
						увел( xpos, min );  умень( len, min )
					кц;
					r.res := 0;  r.eof := ложь;
				кон ReadBytes;


				проц {перекрыта}Write*( перем r: Files.Rider;  x: симв8 );
				перем buf: Buffer;  offset: размерМЗ;
				нач  {единолично}
					buf := r.hint(Buffer);  offset := r.bpos;
					если (r.apos # buf.org) или (offset >= Bufsize) то
						SetX( r, r.apos + offset );
						buf := r.hint(Buffer);  offset := r.bpos
					всё;
					buf.data[offset] := x;  buf.chg := истина;
					если offset = buf.size то  увел( buf.size );  увел( fsize )  всё;
					r.bpos := offset + 1;  r.res := Files.Ok
				кон Write;

				проц {перекрыта}WriteBytes*( перем r: Files.Rider;  конст x: массив из симв8;  ofs, len: размерМЗ );
				перем xpos, min, restInBuf, offset: размерМЗ;  buf: Buffer;
				нач  {единолично}
					xpos := ofs;  buf := r.hint(Buffer);  offset := r.bpos;
					нцПока len > 0 делай
						если (r.apos # buf.org) или (offset >= Bufsize) то
							SetX( r, r.apos + offset );
							buf := r.hint(Buffer);  offset := r.bpos
						всё;
						restInBuf := Bufsize - offset;
						если len > restInBuf то  min := restInBuf  иначе  min := len  всё;
						S.копируйПамять( адресОт( x ) + xpos, адресОт( buf.data ) + offset, min );
						увел( offset, min );  r.bpos := offset;
						если offset > buf.size то
							увел( fsize, offset - buf.size );  buf.size := offset
						всё;
						увел( xpos, min );  умень( len, min );  buf.chg := истина
					кц;
					r.res := Files.Ok
				кон WriteBytes;


				проц {перекрыта}Length*( ): Files.Size;
				нач
					возврат fsize
				кон Length;


				проц {перекрыта}GetDate*( перем t, d: цел32 );
				перем stat: Unix.Status;   r: цел32;  time: Unix.TmPtr;
				нач {единолично}
					если fd = NoDesc то  CreateUnixFile  всё;
					r := Unix.fstat( fd, stat );
					time := Unix.localtime( stat.mtime );
					t := time.sec + арифмСдвиг( time.min, 6 ) + арифмСдвиг( time.hour, 12 );
					d := time.mday + арифмСдвиг( time.mon + 1, 5 ) + арифмСдвиг( time.year, 9 );
				кон GetDate;


				проц {перекрыта}SetDate*( t, d: цел32 );
				тип
					Time = запись actime, modtime: цел32 кон;
				перем
					tm: Unix.Tm;  buf: Time;  r: цел32;  path: Filename;
				нач {единолично}
					если registerName # "" то  копируйСтрокуДо0( registerName, path )
					иначе  копируйСтрокуДо0( workName, path )
					всё;
					(* get year and timezone *)
					(* fill in new date *)
					tm.isdst := -1;  tm.sec := t остОтДеленияНа 64;  tm.min := t DIV 64 остОтДеленияНа 64;
					tm.hour := t DIV 4096 остОтДеленияНа 32;
					tm.mday := d остОтДеленияНа 32;  tm.mon := d DIV 32 остОтДеленияНа 16 - 1;  tm.year := d DIV 512;
					tm.wday := 0;  tm.yday := 0;
					buf.actime := Unix.mktime( tm );  buf.modtime := buf.actime;
					r := Unix.utime( адресОт( path ), адресОт( buf ) );
				кон SetDate;


				проц {перекрыта}GetAttributes*( ): мнвоНаБитахМЗ;
				нач {единолично}
					возврат flags
				кон GetAttributes;

				проц {перекрыта}SetAttributes*( attr: мнвоНаБитахМЗ );
				нач {единолично}
					(* flags := attr	*)
				кон SetAttributes;


				проц {перекрыта}Register0*( перем res: целМЗ );
				перем fo, r: цел32;
				нач {единолично}
					если (state = Create) и (registerName # "") то
						state := Closed (* shortcut renaming *)   ;
					всё;

					FlushBuffers;

					если registerName # "" то
						fo := unixFS.TryRename( workName, registerName, сам, res );
						если res # Files.Ok то
							Halt( сам, ложь, "UnixFiles.File.Register: rename failed" )
						всё;

						если fo # NoDesc то (* SELF still refers to old file *)
							r := Unix.close( fd );  (* VirtualBox ! Can only delete file when closed. *)
							r := Unix.unlink( адресОт( workName ) );
							fd := UnixOpen( адресОт( registerName ), Unix.rdwr, Unix.rwrwr );
						всё;

						workName := registerName;  registerName := ""
					всё;
				кон Register0;


				проц {перекрыта}Update*;
				нач {единолично}
					FlushBuffers
				кон Update;


				проц FlushBuffers;
				перем i: цел32;
				нач
					если fd = NoDesc то  CreateUnixFile  всё;
					нцДля i := 0 до NBufs - 1 делай
						если bufs[i] # НУЛЬ то  Flush( bufs[i] )  всё
					кц;
				кон FlushBuffers;


				проц Finalize*;
				перем r: цел32;
				нач {единолично}
					если tempFile то
						если fd # NoDesc то
							r := Unix.close( fd );
							fd := NoDesc
						всё;
						r := Unix.unlink( адресОт( workName ) );
					иначе
						FlushBuffers;
						если fd # NoDesc то
							r := Unix.close( fd );
							fd := NoDesc;
						всё;
					всё;
					state := Closed;
					DecOpenFiles()
				кон Finalize;

				проц {перекрыта}Close;
				нач
					Finalize;
					collection.newFiles.Remove( сам );
					collection.oldFiles.Remove( сам );
				кон Close;

				проц {перекрыта}GetName*( перем name: массив из симв8 );
				нач {единолично}
					если registerName = "" то  копируйСтрокуДо0( workName, name ) ;
					иначе  копируйСтрокуДо0( registerName, name )
					всё;
					CleanPath( name )
				кон GetName;

			кон File;

	проц EnumFinalize( f: динамическиТипизированныйУкль;  перем cont: булево );
	перем F: File;
	нач
		F := f( File );  F.Finalize();  cont := истина;
	кон EnumFinalize;

(*===================================================================*)

	(** Get the current directory. *)
	проц GetWorkingDirectory*( перем path: массив из симв8 );
	нач
		копируйСтрокуДо0( cwd, path )
	кон GetWorkingDirectory;

	(** Change to directory path. *)
	проц ChangeDirectory*( конст path: массив из симв8;  перем done: булево );
	перем r: цел32;  newdir: Filename;
	нач
		если path[0] # '/' то
			копируйСтрокуДо0( cwd, newdir );  AppendName( newdir, path );
			CleanPath( newdir )
		иначе
			копируйСтрокуДо0( path, newdir );
		всё;
		r := Unix.chdir( адресОт( newdir ) );
		если r = 0 то  копируйСтрокуДо0( newdir, cwd );  done := истина   иначе  done := ложь   всё
	кон ChangeDirectory;

(*===================================================================*)

	проц StripPath*( конст path: массив из симв8;  перем name: массив из симв8 );
	перем i, p: цел16;  c: симв8;
	нач
		i := 0;  p := 0;
		нцДо
			если path[i] = '/' то  p := i + 1  всё;
			увел( i )
		кцПри path[i] = 0X;
		i := 0;
		нцДо  c := path[p];  name[i] := c;  увел( i );  увел( p )  кцПри c = 0X
	кон StripPath;


	проц CleanPath*( перем path: массив из симв8 );
	(*
		/aaa/../bbb/./ccc/../ddd/.  ==>   /bbb/ddd
		../aaa  ==>  CWD/../aaa  ==>  . . .
	*)
	перем
		i, prevNameStart, nameStart: цел16;
		c1, c2, c3: симв8;

		проц prependCWD;
		перем tmp: массив 256 из симв8;
		нач
			копируйСтрокуДо0( cwd, tmp ); AppendName( tmp, path );  копируйСтрокуДо0( tmp, path )
		кон prependCWD;

		проц restart;
		нач
			если path[0] = '/' то  nameStart := 1  иначе  nameStart := 0  всё;
			i := -1;  prevNameStart := -1;
		кон restart;

		проц shift( p0, p1: цел16 );
		перем c: симв8;
		нач
			нцДо  c := path[p1];  path[p0] := c;  увел( p0 );  увел( p1 )  кцПри c = 0X;
			если p0 > 1 то  restart  иначе  i := 0  всё
		кон shift;

	нач
		restart;
		нцДо
			увел( i );
			если i = nameStart то
				c1 := path[i];  c2 := path[i + 1];  c3 := path[i + 2];
				если c1 = '/' то  shift( i, i + 1 ) (* // *)
				аесли c1 = '.' то
					если c2 = 0X то
						если i > 1 то  умень( i )  всё;
						path[i] := 0X
					аесли c2 = '/' то  shift( i, i + 2 );   (* ./ *)
					аесли (c2 = '.') и ((c3 = 0X) или (c3 = '/')) то  (* .. *)
						если i = 0 то  prependCWD;  restart
						аесли c3 = 0X то умень( i ); path[i] := 0X
						аесли c3 = '/' то  (* ../ *)
							если prevNameStart >= 0 то  shift( prevNameStart, i + 3 )  всё
						всё
					всё
				всё
			аесли path[i] = '/' то
				если i > 0 то  prevNameStart := nameStart  всё;
				nameStart := i + 1
			всё;
		кцПри (i >= 0) и (path[i] = 0X);
		если (i > 1) и (path[i - 1] = '/') то  path[i - 1] := 0X  всё;
		если path = "" то  копируйСтрокуДо0( cwd, path )  всё;
	кон CleanPath;


	проц Match( конст name, pat: массив из симв8;  i, j: цел16 ): булево;
	нач
		если (name[i] = 0X) и (pat[j] = 0X) то  возврат истина
		аесли pat[j] # "*" то  возврат (name[i] = pat[j]) и Match( name, pat, i + 1, j + 1 )
		иначе  (* pat[j] = "*", name[i] may be 0X *)
			возврат Match( name, pat, i, j + 1 ) или ((name[i] # 0X) и Match( name, pat, i + 1, j ))
		всё
	кон Match;


	проц Append( перем a: Filename;  конст this: массив из симв8 );
	перем i, j: цел32;
	нач
		i := 0;  j := 0;
		нцПока a[i] # 0X делай  увел( i )  кц;
		нцПока (i < длинаМассива( a ) - 1) и (this[j] # 0X) делай  a[i] := this[j];  увел( i );  увел( j )  кц;
		a[i] := 0X
	кон Append;

	проц AppendName( перем path: Filename;  конст filename: массив из симв8 );
	перем i, j, max: цел32;
	нач
		i := 0;  j := 0;  max := длинаМассива( path ) - 1;
		нцПока path[i] # 0X делай  увел( i )  кц;
		если (i > 0) и (path[i - 1] # "/") то  path[i] := "/";  увел( i );  path[i] := 0X  всё;
		Append( path, filename );
	кон AppendName;


	проц AppendInt( перем str: Filename; n: цел32 );
	перем i: цел32;
	нач
		i := 0;
		нцПока str[i] # 0X делай  увел(i)  кц;
		нцПока n > 0 делай  str[i] := симв8ИзКода( n остОтДеленияНа 10 + кодСимв8('0') );  n := n DIV 10;  увел(i)  кц;
		str[i] := 0X
	кон AppendInt;


	проц IsFullName( конст name: массив из симв8 ): булево;
	перем i: цел16;  ch: симв8;
	нач
		i := 0;  ch := name[0];
		нцПока (ch # 0X) и (ch # "/") делай  увел( i );  ch := name[i]  кц;
		возврат ch = "/"
	кон IsFullName;

	проц Halt( f: File;  unixError: булево;  конст msg: массив из симв8 );
	перем fd, errno, state: цел32;
		workName, registerName: Filename;
	нач
		если f = НУЛЬ то
			workName := "???";  registerName := "???"
		иначе
			workName := f.workName;  registerName := f.registerName;
			fd := f.fd; state := f.state
		всё;
		если unixError то  errno := Unix.errno( );  Unix.Perror( msg )  всё;
		СТОП( 99 )
	кон Halt;


	проц ResetBuffers( f: File;  перем stat: Unix.Status );
	перем i: цел16;
	нач
		f.fsize := stat.size;
		если (f.mtime # stat.mtime.sec) то
			нцДля i := 0 до NBufs - 1 делай
				если f.bufs[i] # НУЛЬ то  f.bufs[i].org := -1;  f.bufs[i] := НУЛЬ  всё;
			кц;
			f.swapper := -1;  f.mtime := stat.mtime.sec
		всё
	кон ResetBuffers;


	проц MakePath( конст dir, name: массив из симв8;  перем dest: массив из симв8 );
	перем i, j: цел16;
	нач
		i := 0;  j := 0;
		нцПока dir[i] # 0X делай  dest[i] := dir[i];  увел( i )  кц;
		если (i>0) и (dest[i - 1] # "/") то  dest[i] := "/";  увел( i )  всё;
		нцПока name[j] # 0X делай  dest[i] := name[j];  увел( i );  увел( j )  кц;
		dest[i] := 0X
	кон MakePath;


	проц ScanPath( перем pos: цел32;  перем dir: массив из симв8 );
	перем i: цел32;  ch: симв8;
	нач
		i := 0;  ch := searchPath[pos];
		нцПока ch = Separator делай  увел( pos );  ch := searchPath[pos]  кц;
		нцПока (ch >= " ") и (ch # Separator) делай  dir[i] := ch;  увел( i );  увел( pos );  ch := searchPath[pos]  кц;
		dir[i] := 0X
	кон ScanPath;


	проц GetTempName( конст finalName: массив из симв8;  перем tempName: Filename );
	перем n, i, j, pe, pid: цел32;
	нач
		увел(tempno);  n := tempno;  i := 0;  j := 0; pe := 1;
		нцПока finalName[j] = ' ' делай  увел(j)  кц;   (* skip leading spaces *)
		если finalName[j] # "/" то  (* relative pathname *)
			нцПока cwd[i] # 0X делай  tempName[i] := cwd[i];  увел(i)  кц;
			если tempName[i - 1] # '/' то  tempName[i] := '/';  увел(i)  всё;
			pe := i - 1
		всё;
		нцПока finalName[j] # 0X делай  tempName[i] := finalName[j];  увел(i);  увел(j)  кц;
		нцПока (i > pe) и (tempName[i-1] # '/') делай  умень(i)  кц;  (* remove filename *)
		tempName[i] := 0X;
		Append( tempName, ".tmp." );
		AppendInt( tempName, n );  Append( tempName, "." );
		pid := Unix.getpid();
		AppendInt( tempName, pid )
	кон GetTempName;

	проц Install;
	перем aliasFS: AliasFileSystem;
	нач
		нов(collection);
		нов( unixFS );  (*  Files.Add( unixFS, "" );	*)
		нов( aliasFS, unixFS );  Files.Add( aliasFS, "searcher" )
	кон Install;



	проц Initialize;
	перем a: адресВПамяти;  i: цел16;  ch: симв8;
	нач
		(* get current working directory *)
		a := Unix.getenv( адресОт( "PWD" ) );
		если a > 0 то
			i := 0;
			нцДо  S.прочтиОбъектПоАдресу( a, ch );  увел( a );  cwd[i] := ch;  увел( i )  кцПри ch = 0X;
		иначе
			(* $PWD not set *)
			a := Unix.getcwd( адресОт( cwd ), длинаМассива( cwd ) )
		всё;
		i := 0;
		нцПока cwd[i] # 0X делай  увел( i )  кц;
		умень( i );
		если (i > 0) и (cwd[i] = '/') то  cwd[i] := 0X  всё;

		(* get search pathes *)
		a := Unix.getenv( адресОт( "AOSPATH" ) );  i := 0;
		если a = 0 то
		(*	Log.String( "UnixFiles.Initialize: environment variable AOSPATH not defined" );  Log.Ln; *)
		иначе
			нцДо
				S.прочтиОбъектПоАдресу( a, ch );  увел( a );
				если ch = ":" то  ch := Separator  всё;
				searchPath[i] := ch;  увел( i )
			кцПри ch = 0X;
		всё;
		tempno := 1;  openfiles := 0;
		Modules.InstallTermHandler( Finalization )
	кон Initialize;


	(*!	The system calls open, read and write return -1 when they get interrupted
		by receiving a signal. Possibly through Objects.SuspendActivities() (GC).
	*)
	проц UnixOpen( path: адресВПамяти; flags: мнвоНаБитах32; permissions: Unix.mode_t ): цел32;
	перем
		fd, fo, errno: цел32;  failure: булево;
	нач
		failure := ложь;
		нцДо
			fd := Unix.open( path, flags, permissions );
			если fd < 0 то
				errno := Unix.errno();
				если errno в  {Unix.ENFILE, Unix.EMFILE} то
					fo := openfiles;
					collection.Collect;
					WaitClose( fo )
				аесли (errno = Unix.EINVAL) и (openfiles > 1000) то
					(* in Solaris open fails with EINVAL ??? *)
					fo := openfiles;
					collection.Collect;
					WaitClose( fo )
				аесли errno # Unix.EINTR то
					failure := истина
				всё
			всё
		кцПри (fd >= 0) или failure;
		возврат fd
	кон UnixOpen;

	проц UnixRead( fd: цел32; buf: адресВПамяти; len: размерМЗ ): размерМЗ;
	перем n: размерМЗ;
	нач
		нцДо
			n := Unix.read( fd, buf, len )
		кцПри (n >= 0) или (Unix.errno() # Unix.EINTR);
		возврат n
	кон UnixRead;

	проц UnixWrite( fd: цел32; buf: адресВПамяти; len: размерМЗ ): размерМЗ;
	перем n: размерМЗ;
	нач
		нцДо
			n := Unix.write( fd, buf, len )
		кцПри (n >= 0) или (Unix.errno() # Unix.EINTR);
		возврат n
	кон UnixWrite;


	проц AddSearchPath*( context: Commands.Context );
	перем name: Files.FileName;  i, j: цел32;  ch : симв8;
	нач
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( name ) то
			CleanPath( name );
			i := 0;  j := 0;
			нцПока searchPath[i] # 0X делай  увел( i )  кц;
			searchPath[i] := Separator;  увел( i );
			нцДо
				ch := name[j];  searchPath[i] := ch;
				увел( j );  увел( i );
			кцПри ch = 0X;
		всё;
	кон AddSearchPath;

	проц SetWorkPath*( context: Commands.Context );
	перем name: Files.FileName; done: булево;
	нач
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( name ) то
			ChangeDirectory( name, done );
			если ~done то
				context.error.пСтроку8( "could not change directory to " );  context.error.пСтроку8( name );  context.error.пВК_ПС
			всё
		всё
	кон SetWorkPath;

	проц Finalization;
	перем ft: Files.FileSystemTable;  i: размерМЗ;
	нач
		Files.GetList( ft );
		если ft # НУЛЬ то
			нцДля i := 0 до длинаМассива( ft^ ) - 1 делай
				если ft[i] суть AliasFileSystem то  Files.Remove( ft[i] )  всё
			кц
		всё;
		collection.Finalize;
		unixFS.Finalize;
	кон Finalization;

	проц DecOpenFiles;
	нач{единолично}
		умень(openfiles);
	кон DecOpenFiles;

	проц IncOpenFiles;
	нач{единолично}
		увел(openfiles);
	кон IncOpenFiles;


	проц WaitClose(no: цел32);
	нач{единолично}
		дождись(openfiles < no);
	кон WaitClose;


нач
	Initialize;
	Install
кон UnixFiles.
