модуль TuringCoatWnd;	(* Soren Renner / TF *)

использует
	Raster, Random, Objects, Kernel, WMRectangles, WMGraphics, Modules, Strings,
	WM := WMWindowManager, WMMessages, Commands, Options;

конст
	m = 50;
	size = 4;

	WindowWidth = m * size; WindowHeight = m * size;

тип

	KillerMsg = окласс
	кон KillerMsg;

	TCW* =  окласс(WM.BufferWindow)
	перем
		mesh1, mesh2, n1 : массив m, m из вещ32;
		random : Random.Generator;
		alive, dead, alpha, delay : булево;
		t0 : цел32;
		timer : Kernel.Timer;

		проц &New*(alpha, delay : булево);
		перем i, j : цел32;
		нач
			Init(WindowWidth, WindowHeight, alpha);
			сам.alpha :=alpha;
			сам.delay := delay;
			manager := WM.GetDefaultManager();
			manager.Add(100, 100, сам, {WM.FlagFrame, WM.FlagClose, WM.FlagNoResizing});

			SetTitle(Strings.NewString("Turing"));
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://TuringCoatWnd.png", истина));

			нов(random);

			нов( timer );  t0 := Kernel.GetTicks( );

			нцДля i := 0 до m - 1 делай
				нцДля j := 0 до m - 1 делай
					mesh1[i, j] := 0;
					mesh2[i, j] := 0;
					n1[i, j] := 0
				кц
			кц;
			нцДля i :=  1 до m - 2 делай
				нцДля j := 1 до m - 2 делай
					если random.Dice(100) > 90 то mesh1[i, j] := random.Dice(1000)/1000 всё
				кц
			кц;
			IncCount;
		кон New;

		проц {перекрыта}Handle*(перем m: WMMessages.Message);
		нач
			если (m.msgType = WMMessages.MsgExt) и (m.ext # НУЛЬ) и (m.ext суть KillerMsg) то
				Close;
			иначе Handle^(m)
			всё
		кон Handle;

		проц {перекрыта}Draw*(canvas : WMGraphics.Canvas; w, h : размерМЗ; q : цел32);
		нач
			Draw^(canvas, w, h, 0)
		кон Draw;

		проц {перекрыта}Close*;
		нач
			alive := ложь;
			нач {единолично} дождись(dead); кон;
			Close^;
			DecCount;
		кон Close;

		проц Delay;
		перем t: цел32;
		нач
			t := Kernel.GetTicks( ) - t0;
			если t < 8 то
				если t >= 0 то  timer.Sleep( 8 - t )  всё;
			всё;
			t0 := Kernel.GetTicks( )
		кон Delay;

		проц Generation;
		перем i, j : цел32;
		нач
			нцДля i := 1 до m - 2 делай
				n1[i, 0] := mesh1[i - 1, 0] + mesh1[i + 1, 0] + mesh1[i, m - 1] + mesh1[i, 1]
				 	+ mesh1[i - 1, m - 1] +  mesh1[i + 1, 1] + mesh1[i + 1, m - 1] + mesh1[i - 1,  1];
				n1[i, m - 1] := mesh1[i - 1, m - 1] + mesh1[i + 1, m - 1] + mesh1[i, m - 2] + mesh1[i, 0]
					+ mesh1[i - 1, m - 2] +  mesh1[i + 1, 0] + mesh1[i + 1, m - 2] + mesh1[i - 1, 0];
				кц;
			нцДля j := 1 до m - 2 делай
				n1[0, j] := mesh1[m - 1, j] + mesh1[1, j] + mesh1[0, j - 1] + mesh1[0, j + 1]
					+ mesh1[m - 1, j - 1] +  mesh1[1, j + 1] + mesh1[1, j - 1] + mesh1[m - 1, j + 1];
				n1[m - 1, j] := mesh1[m - 2, j] + mesh1[0, j] + mesh1[m - 1, j - 1] + mesh1[m - 1, j + 1]
					+ mesh1[m - 2, j - 1] +  mesh1[0, j + 1] + mesh1[0, j - 1] + mesh1[m - 2, j + 1]
			кц;

			нцДля i := 1 до m - 2 делай
				нцДля j := 1 до m - 2 делай
					n1[i, j] := mesh1[i - 1, j] + mesh1[i + 1, j] + mesh1[i, j - 1] + mesh1[i, j + 1]
						+ mesh1[i - 1, j - 1] +  mesh1[i + 1, j + 1] + mesh1[i + 1, j - 1] + mesh1[i - 1, j + 1]
				кц
			кц;
			нцДля i := 1 до m - 2 делай
				нцДля j := 1 до m - 2 делай
					(*  HERE ARE THE DIFFERENCE RULES! *)
					mesh1[i, j] := mesh1[i, j] + n1[i, j] / 80- (mesh2[i, j] * mesh2[i, j])  ;
					mesh2[i, j] := mesh2[i, j] +  mesh1[i, j] / 20 - 0.03 ;
					если mesh1[i, j] < 0 то mesh1[i, j] := 0 всё;
					если mesh2[i, j] < 0 то mesh2[i, j] := 0 всё;
					если mesh1[i, j] > 1 то mesh1[i, j] := 1 всё;
					если mesh2[i, j] > 1 то mesh2[i, j] := 1 всё;
				кц;
			кц;
		кон Generation;

		проц DrawIt;
		перем i, j, ix, jy : цел32;
			pix : Raster.Pixel;
			mode : Raster.Mode;
		нач
			Raster.InitMode(mode, Raster.srcCopy);
			нцДля i := 0 до m - 1 делай
				ix := i * size ;
				нцДля j := 0 до m - 1 делай
					jy := j * size;
					если alpha то
						Raster.SetRGBA(pix, устарПреобразуйКБолееУзкомуЦел((255-округлиВниз(mesh1[i, j] * 255)) ), устарПреобразуйКБолееУзкомуЦел((255-округлиВниз(mesh2[i, j] * 255)) ), 0,
						устарПреобразуйКБолееУзкомуЦел( (255-округлиВниз(mesh2[i, j] * 255))+округлиВниз(mesh1[i, j] * 255)) остОтДеленияНа 128+127 )
					иначе
						Raster.SetRGB(pix, устарПреобразуйКБолееУзкомуЦел((255-округлиВниз(mesh1[i, j] * 255)) ), устарПреобразуйКБолееУзкомуЦел((255-округлиВниз(mesh2[i, j] * 255)) ), 0)
					всё;
					Raster.Fill(img, ix, jy, ix+size, jy+size, pix, mode)
				кц
			кц;
			Invalidate(WMRectangles.MakeRect(0, 0, GetWidth(), GetHeight()))
		кон DrawIt;

	нач {активное}
		alive := истина;
		Objects.SetPriority(Objects.Low);
		нцПока alive делай
			Generation;
			если delay то Delay всё;	(* GF, make animation speed independent of CPU speed *)
			DrawIt;
		кц;
		нач {единолично} dead := истина; кон;
	кон TCW;

перем
	nofWindows : цел32;

проц Open*(context: Commands.Context);
перем window : TCW; options: Options.Options;
нач
	нов(options);
	options.Add("d", "delay", Options.Flag);
	options.Add("a", "alpha", Options.Flag);
	если options.Parse(context.arg, context.error) то
		нов(window, options.GetFlag("alpha"), options.GetFlag("delay"));
	всё;
кон Open;

проц IncCount;
нач {единолично}
	увел(nofWindows);
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows);
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die); msg.ext := die; msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон TuringCoatWnd.

System.Free TuringCoatWnd ~
TuringCoatWnd.Open ~
TuringCoatWnd.Open --alpha~
TuringCoatWnd.Open --delay ~

