модуль CompilerInterface; (** AUTHOR "staubesv"; PURPOSE "Generic compiler interface"; *)
(**
 * The idea of this module is to make it possible for client applications to use multiple different compilers.
 * Compiler can be retrieve by name, file extension of filename.
 *)

использует
	ЛогЯдра, Потоки, Commands, Strings, Texts, Diagnostics;

конст

	ModuleName = "CompilerInterface";

тип

	Name* = массив 16 из симв8;
	Description* = массив 128 из симв8;
	FileExtension* = массив 16 из симв8;

	CompileTextProc* = проц {делегат} (t : Texts.Text; конст source: массив из симв8; pos: цел32; конст pc,opt: массив из симв8;
		log: Потоки.Писарь; diagnostics : Diagnostics.Diagnostics; перем error: булево);

тип

	Compiler* = окласс
	перем
		name- : Name;
		description- : Description;
		fileExtension- : FileExtension;

		compileText : CompileTextProc;

		next : Compiler;

		проц CompileText*(t : Texts.Text; конст source: массив из симв8; pos: цел32; конст pc,opt: массив из симв8;
			log: Потоки.Писарь; diagnostics : Diagnostics.Diagnostics; перем error: булево);
		перем
			trap : булево;
		нач
			trap := ложь;
			если (compileText # НУЛЬ) то
				compileText(t, source, pos, pc, opt, log, diagnostics, error);
			аесли (diagnostics # НУЛЬ) то
				diagnostics.Error(source, Потоки.НевернаяПозицияВПотоке, "Text compile procedure not set");
			всё;
		выходя
			если trap то (* trap will be set in case a trap occurs in the block above *)
				error := истина;
				diagnostics.Error(source, Потоки.НевернаяПозицияВПотоке, "COMPILER TRAPPED");
				log.пСтроку8("COMPILER TRAPPED!!!"); log.ПротолкниБуферВПоток;
			всё;
		кон CompileText;

		проц Show(out : Потоки.Писарь);
		нач
			out.пСтроку8(name);
			out.пСтроку8(" ("); out.пСтроку8(description); out.пСтроку8(") ");
			out.пСтроку8("File Extension: "); out.пСтроку8(fileExtension);
			out.пВК_ПС;
		кон Show;

		проц &Init*(
			конст name : Name;
			конст description : Description;
			конст fileExtension : FileExtension;
			compileText : CompileTextProc
		);
		нач
			сам.name := name; сам.description := description; сам.fileExtension := fileExtension;
			сам.compileText := compileText;
		кон Init;

	кон Compiler;

перем
	compilers : Compiler;

проц FindCompilerByName(конст name : массив из симв8) : Compiler;
перем c : Compiler;
нач
	c := compilers;
	нцПока (c # НУЛЬ) и (c.name # name) делай c := c.next; кц;
	возврат c;
кон FindCompilerByName;

(** Get compiler object for a specific file extension. Returns NIL if no appropriate compiler found. *)
проц GetCompiler*(fileExtension : FileExtension) : Compiler;
перем c : Compiler;
нач {единолично}
	Strings.UpperCase(fileExtension);
	c := compilers;
	нцПока (c # НУЛЬ) и (c.fileExtension # fileExtension) делай c := c.next; кц;
	возврат c;
кон GetCompiler;

проц GetCompilerByName*(конст name : массив из симв8) : Compiler;
нач {единолично}
	возврат FindCompilerByName(name);
кон GetCompilerByName;

(** Get compiler object for a filename. A compiler is appropriate for a given file name
	if the file extension the compiler requires is part of the filename, e.g. Module.Mod.Bak for the file extension .Mod
	Returns NIL if no appropriate compiler found *)
проц GetCompilerByFilename*(filename : массив из симв8) : Compiler;
перем c : Compiler; pos : размерМЗ;
нач {единолично}
	Strings.UpperCase(filename);
	c := compilers;
	нц
		если (c = НУЛЬ) то прервиЦикл; всё;
		pos := Strings.Pos(c.fileExtension, filename);
		если (pos > 0) и (filename[pos-1] = ".") то
			прервиЦикл;
		всё;
		c := c.next;
	кц;
	возврат c;
кон GetCompilerByFilename;

(** Show all registered compilers *)
проц Show*(context : Commands.Context);
перем c : Compiler;
нач {единолично}
	если (compilers = НУЛЬ) то
		context.out.пСтроку8("No compilers registered."); context.out.пВК_ПС;
	иначе
		c := compilers;
		нцПока (c # НУЛЬ) делай c.Show(context.out); c := c.next; кц;
	всё;
кон Show;

(** Register a compiler. The name of the compiler must be unique. *)
проц Register*(
	конст name : Name;
	конст description : Description;
	fileExtension : FileExtension;
	compileText : CompileTextProc);
перем
	c : Compiler;
нач {единолично}
	утв(compileText # НУЛЬ);
	c := FindCompilerByName(name);
	если (c = НУЛЬ) то
		Strings.UpperCase(fileExtension);
		нов(c, name, description, fileExtension, compileText);
		c.next := compilers; compilers := c;
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование;
		ЛогЯдра.пСтроку8(ModuleName); ЛогЯдра.пСтроку8(": Cannot register compiler '");
		ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8("': Name is already in use.");
		ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	всё;
кон Register;

(** Unregister a compiler *)
проц Unregister*(конст name : Name);
перем prev : Compiler;
нач {единолично}
	если (compilers = НУЛЬ) то возврат; всё;
	если (compilers.name = name) то
		compilers := compilers.next;
	иначе
		prev := compilers;
		нцПока(prev.next # НУЛЬ) и (prev.next.name # name) делай prev := prev.next; кц;
		если (prev.next # НУЛЬ) то
			prev.next := prev.next.next;
		всё;
	всё;
кон Unregister;

нач
	compilers := НУЛЬ;
кон CompilerInterface.

CompilerInterface.Show ~

System.Free CompilerInterface ~
