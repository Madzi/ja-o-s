модуль WMClock; (** AUTHOR "TF/staubesv"; PURPOSE "Clock components & clock application"; *)

использует
	Modules, Kernel, Math, Dates, Strings, Locks, XML, Raster, WMRasterScale, WMRectangles, WMGraphics, WMGraphicUtilities,
	WMWindowManager, WMPopups, WMRestorable, WMMessages, WMComponents, WMProperties;

конст
	(** Clock.viewMode property *)
	ViewModeStandard* = 0;
	ViewModeDateTime* = 1;
	ViewModeDayOfWeek* = 2;
	ViewModeAnalog* = 3;
	ViewModeFormatted*= 4;

	WindowWidth = 150; WindowHeight = 50;

тип

	ContextMenuPar = окласс
	перем
		mode : цел32;

		проц &New*(m : цел32);
		нач
			mode := m;
		кон New;

	кон ContextMenuPar;

тип

	KillerMsg = окласс
	кон KillerMsg;

	Window = окласс(WMComponents.FormWindow);
	перем
		clock : Clock;
		imageNameAnalog : Strings.String;
		contextMenu : WMPopups.Popup;
		dragging, resizing : булево;
		lastX, lastY : размерМЗ;

		проц &New*(context : WMRestorable.Context; flags : мнвоНаБитахМЗ);
		перем configuration : WMRestorable.XmlElement; viewMode, color : цел32;
		нач
			IncCount;
			если (context # НУЛЬ) то
				Init(context.r - context.l, context.b - context.t, истина);
			иначе
				Init(WindowWidth, WindowHeight, истина);
			всё;

			нов(clock);
			clock.alignment.Set(WMComponents.AlignClient);
			imageNameAnalog := clock.imageName.Get();
			если (clock.viewMode.Get() # ViewModeAnalog) то clock.imageName.Set(НУЛЬ); всё;
			SetContent(clock);
			SetTitle(Strings.NewString("Clock"));

			если (context # НУЛЬ) то
				configuration := WMRestorable.GetElement(context, "Configuration");
				если (configuration # НУЛЬ) то
					WMRestorable.LoadLongint(configuration, "color", color); clock.color.Set(color);
					WMRestorable.LoadLongint(configuration, "viewMode", viewMode); clock.viewMode.Set(viewMode);
				всё;
				WMRestorable.AddByContext(сам, context);
			иначе
				если (WMWindowManager.FlagNavigation в flags) то
					WMWindowManager.ExtAddViewBoundWindow(сам, 50, 50, НУЛЬ, flags);
				иначе
					WMWindowManager.ExtAddWindow(сам, 50, 50, flags)
				всё;
			всё;

		кон New;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount;
		кон Close;

		проц HandleClose(sender, par: динамическиТипизированныйУкль);
		перем manager : WMWindowManager.WindowManager;
		нач
			manager := WMWindowManager.GetDefaultManager();
			manager.SetFocus(сам);
			Close;
		кон HandleClose;

		проц HandleToggleColor(sender, data: динамическиТипизированныйУкль);
		нач
			если (clock.color.Get() = 0FFH) то clock.color.Set(цел32(0FFFFFFFFH)) иначе clock.color.Set(0FFH) всё;
		кон HandleToggleColor;

		проц HandleToggleView(sender, par: динамическиТипизированныйУкль);
		перем manager : WMWindowManager.WindowManager; viewMode : цел32;
		нач
			manager := WMWindowManager.GetDefaultManager();
			manager.SetFocus(сам);
			если (par # НУЛЬ) и (par суть ContextMenuPar) то
				viewMode := par(ContextMenuPar).mode;
				если (clock.viewMode.Get() # viewMode) то
					если (par(ContextMenuPar).mode = ViewModeAnalog) то
						clock.imageName.Set(imageNameAnalog);
					иначе
						clock.imageName.Set(НУЛЬ);
					всё;
					clock.viewMode.Set(par(ContextMenuPar).mode);
				всё;
			иначе
				clock.viewMode.Set(ViewModeStandard);
				clock.imageName.Set(НУЛЬ);
			всё
		кон HandleToggleView;

		проц {перекрыта}PointerDown*(x, y:размерМЗ; keys:мнвоНаБитахМЗ);
		нач
			lastX := bounds.l + x; lastY:=bounds.t + y;
			если keys = {0} то
				dragging := истина
			аесли keys = {1,2} то
				dragging := ложь;
				resizing := истина;
			аесли keys = {2} то
				нов(contextMenu);
				contextMenu.Add("::WMClock:ClockMenu:SClose", HandleClose);
				contextMenu.AddParButton("::WMClock:ClockMenu:STime", HandleToggleView, contextMenuParStandard);
				contextMenu.AddParButton("::WMClock:ClockMenu:SDate", HandleToggleView, contextMenuParDateTime);
				contextMenu.AddParButton("::WMClock:ClockMenu:SDayOfWeek", HandleToggleView, contextMenuParDayOfWeek);
				contextMenu.AddParButton("::WMClock:ClockMenu:SAnalog", HandleToggleView, contextMenuParAnalog);
				contextMenu.AddParButton("::WMClock:ClockMenu:SToggleColor", HandleToggleColor, НУЛЬ);
				contextMenu.Popup(bounds.l + x, bounds.t + y)
			всё
		кон PointerDown;

		проц {перекрыта}PointerMove*(x,y:размерМЗ; keys:мнвоНаБитахМЗ);
		перем dx, dy, width, height : размерМЗ;
		нач
			если dragging или resizing то
				x := bounds.l + x; y := bounds.t + y; dx := x - lastX; dy := y - lastY;
				lastX := lastX + dx; lastY := lastY + dy;
				если (dx # 0) или (dy # 0) то
					если dragging то
						manager.SetWindowPos(сам, bounds.l + dx, bounds.t + dy);
					иначе
						width := GetWidth();
						height := GetHeight();
						width := матМаксимум(10, width + dx);
						height := матМаксимум(10, height + dy);
						manager.SetWindowSize(сам, width, height);
					всё;
				всё;
			всё;
		кон PointerMove;

		проц {перекрыта}PointerUp*(x, y:размерМЗ; keys:мнвоНаБитахМЗ);
		нач
			dragging := ложь;
			если (keys # {1,2}) то
				если resizing то
					resizing := ложь;
					Resized(GetWidth(), GetHeight());
				всё;
			всё;
		кон PointerUp;

		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		перем configuration : WMRestorable.XmlElement;
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть KillerMsg) то
					Close;
				аесли (x.ext суть WMRestorable.Storage) то
					нов(configuration); configuration.SetName("Configuration");
					WMRestorable.StoreBoolean(configuration, "stayOnTop", WMWindowManager.FlagStayOnTop в flags);
					WMRestorable.StoreBoolean(configuration, "navigation", WMWindowManager.FlagNavigation в flags);
					WMRestorable.StoreLongint(configuration, "color", clock.color.Get());
					WMRestorable.StoreLongint(configuration, "viewMode", clock.viewMode.Get());
					x.ext(WMRestorable.Storage).Add("WMClock", "WMClock.Restore", сам, configuration);
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

	кон Window;

тип

	Clock* = окласс(WMComponents.VisualComponent)
	перем
		viewMode- : WMProperties.Int32Property;
		color- : WMProperties.ColorProperty;

		(** background image filename *)
		imageName- : WMProperties.StringProperty;

		(** time offset in hours *)
		timeOffset- : WMProperties.Int32Property;

		(** hand lengths in percent of component width/height *)
		secondHandLength-, minuteHandLength-, hourHandLength- : WMProperties.Int32Property;

		(** colors of hands *)
		secondHandColor-, minuteHandColor-, hourHandColor- : WMProperties.ColorProperty;

		(* format *)
		format-: WMProperties.StringProperty;

		currentTime : Dates.DateTime;
		lock : Locks.Lock; (* protects currentTime *)

		str : Strings.String;
		centerX, centerY : цел32;

		image : WMGraphics.Image;
		updateInterval : цел32;

		alive, dead : булево;
		timer : Kernel.Timer;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrClock);
			SetGenerator("WMClock.GenClock");
			нов(imageName, PrototypeImageName, НУЛЬ, НУЛЬ); properties.Add(imageName);
			нов(timeOffset, PrototypeTimeOffset, НУЛЬ, НУЛЬ); properties.Add(timeOffset);
			нов(viewMode, PrototypeViewMode, НУЛЬ, НУЛЬ); properties.Add(viewMode);
			нов(color, PrototypeColor, НУЛЬ, НУЛЬ); properties.Add(color);
			нов(secondHandLength, PrototypeSecondHandLength, НУЛЬ, НУЛЬ); properties.Add(secondHandLength);
			нов(minuteHandLength, PrototypeMinuteHandLength, НУЛЬ, НУЛЬ); properties.Add(minuteHandLength);
			нов(hourHandLength, PrototypeHourHandLength, НУЛЬ, НУЛЬ); properties.Add(hourHandLength);
			нов(secondHandColor, PrototypeSecondHandColor, НУЛЬ, НУЛЬ); properties.Add(secondHandColor);
			нов(minuteHandColor, PrototypeMinuteHandColor, НУЛЬ, НУЛЬ); properties.Add(minuteHandColor);
			нов(hourHandColor, PrototypeHourHandColor, НУЛЬ, НУЛЬ); properties.Add(hourHandColor);
			нов(format, PrototypeFormat, НУЛЬ, НУЛЬ); properties.Add(format);
			нов(lock);
			нов(str, 32);
			image := НУЛЬ;
			updateInterval := 500;
			alive := истина; dead := ложь;
			нов(timer);
			SetFont(WMGraphics.GetFont("Oberon", 24, {WMGraphics.FontBold}));
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		перем vmValue : цел32;
		нач
			если (property = viewMode) то
				vmValue := viewMode.Get();
				если vmValue = ViewModeStandard то
					format.SetAOC("hh:nn:ss");
				аесли vmValue = ViewModeDateTime то
					format.SetAOC("dd.mm.yy");
				аесли vmValue = ViewModeDayOfWeek то
					format.SetAOC("www dd.");
				всё;
				timer.Wakeup;
			аесли (property = color) то
				timer.Wakeup;
			аесли (property = imageName) то
				RecacheProperties;
				timer.Wakeup;
			аесли (property = bounds) то
				PropertyChanged^(sender, property);
				RecacheProperties;
				timer.Wakeup;
			аесли (property = timeOffset) то
				timer.Wakeup;
			аесли (property = secondHandLength) или (property = minuteHandLength) или (property = hourHandLength) или
				(property = secondHandColor) или (property = minuteHandColor) или (property = hourHandColor) то
				timer.Wakeup;
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц {перекрыта}RecacheProperties*;
		перем string : Strings.String; newImage, resizedImage : WMGraphics.Image; vmValue : цел32;
		нач
			RecacheProperties^;
			vmValue := viewMode.Get();
			если vmValue = ViewModeStandard то
				format.SetAOC("hh:nn:ss");
			аесли vmValue = ViewModeDateTime то
				format.SetAOC("dd.mm.yy");
			аесли vmValue = ViewModeDayOfWeek то
				format.SetAOC("www dd.");
			всё;
			newImage := НУЛЬ;
			string := imageName.Get();
			если (string # НУЛЬ) то
				newImage := WMGraphics.LoadImage(string^, истина);
				если (newImage # НУЛЬ) то
					если (bounds.GetWidth() # newImage.width) или (bounds.GetHeight() # newImage.height) то
						нов(resizedImage);
						Raster.Create(resizedImage, bounds.GetWidth(), bounds.GetHeight(), Raster.BGRA8888);
						WMRasterScale.Scale(
							newImage, WMRectangles.MakeRect(0, 0, newImage.width, newImage.height),
							resizedImage, WMRectangles.MakeRect(0, 0, resizedImage.width, resizedImage.height),
							WMRectangles.MakeRect(0, 0, resizedImage.width, resizedImage.height),
							WMRasterScale.ModeCopy, WMRasterScale.ScaleBilinear);
						newImage := resizedImage;
					всё;
				всё;
			всё;
			image := newImage;
			centerX := округлиВниз(bounds.GetWidth() / 2 + 0.5);
			centerY := округлиВниз(bounds.GetHeight() / 2 + 0.5);
		кон RecacheProperties;

		проц DrawHands(canvas : WMGraphics.Canvas; time : Dates.DateTime);

			проц DrawLine(handLengthInPercent : цел32; color : WMGraphics.Color; angle : вещ32);
			перем deltaX, deltaY : цел32; radiants : вещ32; lengthX, lengthY : размерМЗ;
			нач
				lengthX := handLengthInPercent * bounds.GetWidth() DIV 2 DIV 100;
				lengthY := handLengthInPercent * bounds.GetHeight() DIV 2 DIV 100;
				radiants := (angle / 360) * 2*Math.pi;
				deltaX := округлиВниз(lengthX * Math.sin(radiants) + 0.5);
				deltaY := округлиВниз(lengthY * Math.cos(radiants) + 0.5);
				canvas.Line(centerX, centerY, centerX + deltaX, centerY - deltaY, color, WMGraphics.ModeSrcOverDst);
			кон DrawLine;

		нач
			если (hourHandLength.Get() > 0) то
				time.hour := time.hour остОтДеленияНа 12;
				DrawLine(hourHandLength.Get(), hourHandColor.Get(), (time.hour + time.minute/60) * (360 DIV 12));
			всё;
			если (minuteHandLength.Get() > 0) то
				DrawLine(minuteHandLength.Get(), minuteHandColor.Get(), (time.minute + time.second/60) * (360 DIV 60));
			всё;
			если (secondHandLength.Get() > 0) то
				DrawLine(secondHandLength.Get(), secondHandColor.Get(), time.second  * (360 DIV 60));
			всё;
		кон DrawHands;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем time : Dates.DateTime; formatString: Strings.String;
		нач
			DrawBackground^(canvas);
			lock.Acquire;
			time := currentTime;
			lock.Release;
			если image # НУЛЬ то
				canvas.DrawImage(0, 0, image, WMGraphics.ModeSrcOverDst);
			всё;
			если (viewMode.Get() = ViewModeAnalog) то
				DrawHands(canvas, time);
			иначе
				formatString := format.Get();
				Strings.FormatDateTime(formatString^, time, str^);
				canvas.SetColor(color.Get());
				если (image = НУЛЬ) то
					(*WMGraphicUtilities.DrawRect(canvas, GetClientRect(), color.Get(), WMGraphics.ModeCopy);*)
				всё;
				WMGraphics.DrawStringInRect(canvas, GetClientRect(), ложь, WMGraphics.AlignCenter, WMGraphics.AlignCenter, str^)
			всё;
		кон DrawBackground;

		проц {перекрыта}Finalize*;
		нач
			Finalize^;
			alive := ложь;
			timer.Wakeup;
			нач {единолично} дождись(dead); кон;
		кон Finalize;

	нач {активное}
		нцПока alive делай
			lock.Acquire;
			currentTime := Dates.Now();
			currentTime.hour := (currentTime.hour + timeOffset.Get());
			lock.Release;
			Invalidate;
			timer.Sleep(updateInterval);
		кц;
		нач {единолично} dead := истина; кон;
	кон Clock;

перем
	nofWindows : цел32;

	StrClock : Strings.String;

	PrototypeViewMode : WMProperties.Int32Property;
	PrototypeColor : WMProperties.ColorProperty;

	PrototypeImageName : WMProperties.StringProperty;
	PrototypeSecondHandLength, PrototypeMinuteHandLength, PrototypeHourHandLength : WMProperties.Int32Property;
	PrototypeSecondHandColor, PrototypeMinuteHandColor, PrototypeHourHandColor : WMProperties.ColorProperty;
	PrototypeTimeOffset, PrototypeUpdateInterval : WMProperties.Int32Property;
	PrototypeFormat: WMProperties.StringProperty;

	contextMenuParStandard, contextMenuParDateTime, contextMenuParDayOfWeek, contextMenuParAnalog : ContextMenuPar;

проц Open*;
перем window : Window;
нач
	нов(window, НУЛЬ, {WMWindowManager.FlagStayOnTop, WMWindowManager.FlagNavigation, WMWindowManager.FlagHidden});
кон Open;

проц Restore*(context : WMRestorable.Context);
перем window : Window;
нач
	нов(window, context, {});
кон Restore;

проц GenClock*() : XML.Element;
перем clock : Clock;
нач
	нов(clock); возврат clock;
кон GenClock;

проц InitStrings;
нач
	StrClock := Strings.NewString("Clock");
кон InitStrings;

проц InitPrototypes;
нач
	(* DigitalClock *)
	нов(PrototypeColor, НУЛЬ, Strings.NewString("Color"), Strings.NewString("toggle clock color"));
	PrototypeColor.Set(0FFH);
	нов(PrototypeViewMode, НУЛЬ, Strings.NewString("ViewMode"),	Strings.NewString("select view mode: time=0, date=1, dayOfWeek=2, analog=3, formatted=4"));
	PrototypeViewMode.Set(ViewModeStandard);

	(* AnalogClock *)
	нов(PrototypeImageName, НУЛЬ, Strings.NewString("ImageName"), Strings.NewString("Clock face image name"));
	PrototypeImageName.SetAOC("WMClock.rep://images/roman_numeral_wall_clock.png");
	нов(PrototypeTimeOffset, НУЛЬ, Strings.NewString("TimeOffset"), Strings.NewString("Time offset in hours"));
	PrototypeTimeOffset.Set(0);
	нов(PrototypeSecondHandLength, НУЛЬ, Strings.NewString("SecondHandLength"), Strings.NewString("Length of second hand in percent of radius"));
	PrototypeSecondHandLength.Set(90);
	нов(PrototypeMinuteHandLength, НУЛЬ, Strings.NewString("MinuteHandLength"), Strings.NewString("Length of minute hand in percent of radius"));
	PrototypeMinuteHandLength.Set(80);
	нов(PrototypeHourHandLength, НУЛЬ, Strings.NewString("HourHandLength"), Strings.NewString("Length of hour hand in percent of radius"));
	PrototypeHourHandLength.Set(60);
	нов(PrototypeSecondHandColor, НУЛЬ, Strings.NewString("SecondHandColor"), Strings.NewString("Color of second hand"));
	PrototypeSecondHandColor.Set(WMGraphics.Red);
	нов(PrototypeMinuteHandColor, НУЛЬ, Strings.NewString("MinuteHandColor"), Strings.NewString("Color of minute hand"));
	PrototypeMinuteHandColor.Set(WMGraphics.Black);
	нов(PrototypeHourHandColor, НУЛЬ, Strings.NewString("HourHandColor"), Strings.NewString("Color of hour hand"));
	PrototypeHourHandColor.Set(WMGraphics.Black);
	нов(PrototypeUpdateInterval, НУЛЬ, Strings.NewString("UpdateInterval"), Strings.NewString("Redraw rate"));
	PrototypeUpdateInterval.Set(500);
	нов(PrototypeFormat, НУЛЬ, Strings.NewString("Format"), Strings.NewString("Textual Format (yy, mm, dd, www, hh, nn, ss)"));
	PrototypeFormat.Set(Strings.NewString("hh:nn:ss"));
кон InitPrototypes;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	nofWindows := 0;
	InitStrings;
	InitPrototypes;
	Modules.InstallTermHandler(Cleanup);
	нов(contextMenuParStandard, ViewModeStandard);
	нов(contextMenuParDateTime, ViewModeDateTime);
	нов(contextMenuParDayOfWeek, ViewModeDayOfWeek);
	нов(contextMenuParAnalog, ViewModeAnalog);
кон WMClock.

System.Free WMClock~

WMClock.Open ~
