модуль TestLocks;	(* pjm *)

(* Test module for Locks. *)

использует НИЗКОУР, Machine, ЛогЯдра;

(* Display locking state. *)

проц ShowState(msg: массив из симв8);
перем id: цел32; ints: булево;
нач
	ints := Machine.InterruptsEnabled();
	ЛогЯдра.пСтроку8(msg); ЛогЯдра.пСтроку8(": ");
	id := Machine.ID();
	ЛогЯдра.пСтроку8("Processor "); ЛогЯдра.пЦел64(id, 1);
	ЛогЯдра.пСтроку8(", interrupts are ");
	если ints то ЛогЯдра.пСтроку8("on") иначе ЛогЯдра.пСтроку8("off") всё;
	ЛогЯдра.пВК_ПС
кон ShowState;

(** Acquire and release a single lock. *)

проц Single*;
нач
	Machine.Acquire(Machine.KernelLog);
	ShowState("Single");
	Machine.Release(Machine.KernelLog);
кон Single;

(** Same as Single, but switch on interrupts. *)

проц SingleInt*;
нач
	Machine.Acquire(Machine.KernelLog);
	Machine.Sti();	(* nested lock in write will switch off interrupts again *)
	ShowState("SingleInt1");
	ShowState("SingleInt2");
	Machine.Release(Machine.KernelLog);
кон SingleInt;

(** Hold a lock for a long time, to test interrupt interaction. *)

проц Long*;
перем i: цел32;
нач
	Machine.Acquire(Machine.KernelLog);
	нцДля i := 0 до 100000000 делай кц;
	ShowState("Long");
	Machine.Release(Machine.KernelLog);
кон Long;

(** Same as Long, but switch on interrupts. *)

проц LongInt*;
перем i: цел32;
нач
	Machine.Acquire(Machine.KernelLog);
	Machine.Sti;
	нцДля i := 0 до 100000000 делай кц;
	Machine.Cli;
	ShowState("LongInt");
	Machine.Release(Machine.KernelLog);
кон LongInt;

(** Acquire and release all locks. *)

проц All*;
нач
	Machine.AcquireAll;
	(*ShowState("All");*)
	Machine.ReleaseAll
кон All;

(** Attempt to acquire a lock recursively (allowed in old model). *)

проц Recursive*;
нач
	Machine.Acquire(Machine.KernelLog);
	Machine.Acquire(Machine.KernelLog);
	ShowState("Recursive");
	Machine.Release(Machine.KernelLog);
	Machine.Release(Machine.KernelLog);
кон Recursive;

(** Acquire a lock out of order (not allowed). *)

проц OutOfOrder*;
нач
	Machine.Acquire(Machine.TraceOutput);
	Machine.Acquire(Machine.KernelLog);
	ShowState("OutOfOrder");
	Machine.Release(Machine.KernelLog);
	Machine.Release(Machine.TraceOutput)
кон OutOfOrder;

(*
(** Acquire special lock. *)

PROCEDURE Special*;
BEGIN
	Machine.Cli();
	ShowState("Special1");
	Machine.AcquireSpecial(Machine.KernelLog);
	ShowState("Special2");
	Machine.Release(Machine.KernelLog);
	ShowState("Special3");
	Machine.Sti
END Special;
*)

кон TestLocks.

System.OpenKernelLog

TestLocks.Single
TestLocks.SingleInt	(should trap if StrongChecks are on)
TestLocks.Long	(runs for a few seconds)
TestLocks.LongInt
TestLocks.All
TestLocks.Recursive	(should trap or hang)
TestLocks.OutOfOrder	(should trap or hang)
TestLocks.Special
