модуль NetworkMii; (** AUTHOR "staubesv"; PURPOSE "Media Independent Interface (MII) management interface support"; *)
(**
 * The Media Independent Interface (MII) is an interface between the MAC Layer and the PHY. The MII Management Interface's purpose is
 * to control the PHY and gather status from the PHY. This module defines constants and implements some basic functionality for both
 * the MII management interface and its superset GMII.
 *
 * References:
 *
 *	[1] IEEE Std 802.3-2005 Edition, www.ieee.org
 *
 * History:
 *
 *	03.11.2006	First release (staubesv)
 *
 * Status:
 *
 *	BETA
 *		- 1GB auto-negotiation not yet supported
 *		- Handling of devices that don't support auto-negotiation not supported
 *)

использует
	НИЗКОУР, ЛогЯдра, Objects, Kernel, Network;

конст

	Ok* = 0;
	Unsupported* = 1;
	ParametersInvalid* = 2;
	NotAvailable* = 3;
	ErrorRead* = 5;
	ErrorWrite* = 6;
	Timeout* = 7;

	AutoNegotiationTimeout = 1000; (* ms *)

	TraceAutoNegotiation = {0};
	TraceCommands = {1};

	Trace = {};

	Debug = истина;

	ModuleName = "NetworkMii";

	(** MII register offsets *)

	(** Basic Registers *)
	BMCR* = 0H;		(** Basic Mode Control Register *)
	BMSR* = 1H;		(** Basic Mode Status Register *)

	BMESR* = 15H;		(** Basic Mode Extended Status Register (GMII only) *)

	(** Extended Registers *)
	PHYIDR1* = 2H; 		(** PHY Identifier Register 1 *)
	PHYIDR2* = 3H;		(** PHY Identifier Register 2 *)
	ANAR* = 4H;		(** Auto-Negotiation Advertisement Register *)
	ANLPAR* = 5H;		(** Auto-Negotiation Link Partner Ability Register *)
	ANER* = 6H;		(** Auto-Negotiation Expansion Register *)
	ANNPTR* = 7H;		(** Auto-Negotiation Next Page Transmit Register *)
	ANLPRNPR* = 8H;	(** Auto-Negotiation Link Partner Received Next Page Register *)
	MSCR* = 9H;		(** Master-Slave Control Register *)
	MSSR* = 10H;		(** Master-Slave Status Register *)
	PSECR* = 11H;		(** PSE Control Register *)
	PSESR* = 12H;		(** PSE Status Register *)
	MMDACR* = 13H;	(** MMD Access Control Register *)
	MMDAADR* = 14H;	(** MMD Access Address Data Register *)

	(* 16H-31H: Vendor-Specific extended registers *)

	(* Basic Mode Control Register (BMCR) *)
	BMCR_Reset* = {15}; (* self-clearing, default: 0 *)
	BMCR_Loopback* = {14}; (* default: 0 *)
	BMCR_SpeedSelectionLsb* = {13};
	BMCR_AutoNegotiationEnable* = {12}; (* default: 1 *)
	BMCR_PowerDown* = {11}; (* default: 0 *)
	BMCR_Isolate* = {10}; (* default: 0 *)
	BMCR_RestartAutoNegotiation* = {9}; (* self-clearing *)
	BMCR_DuplexMode* = {8};
	BMCR_CollisionTest* = {7}; (* default: 0 *)
	BMCR_SpeedSelectionMsb* = {6};
	BMCR_UnidirectionalEnable* = {5};
	BMCR_Reserved* = {0..4}; (* write as zero *)

	(* Basic Mode Status Register (BMSR), all fields are read-only *)
	BMSR_100BaseT4* = {15};
	BMSR_100BaseTXFullDuplex* = {14};
	BMSR_100BaseTXHalfDuplex* = {13};
	BMSR_10BaseTFullDuplex* = {12};
	BMSR_10BaseTHalfDuplex* = {11};
	BMSR_100BaseT2FullDuplex* = {10};
	BMSR_100BaseT2HalfDuplex* = {9};
	BMSR_ExtendedStatus* = {8}; (* Extended status information in register 15 (BMESR) *)
	BMSR_UnidirectionalAbility* = {7};
	BMSR_MfPreambleSuppression* = {6};
	BMSR_AutoNegotiationComplete* = {5};
	BMSR_RemoteFault* = {4};
	BMSR_AutoNegotiationAbility* = {3};
	BMSR_LinkStatus* = {2};
	BMSR_JabberDetect* = {1};
	BMSR_ExtendedCapability* = {0}; (* Extended register available *)

	(* Basic Mode Extended Status Register (BMESR), all fields are read-only *)
	BMESR_1000BaseXFullDuplex* = {15};
	BMESR_1000BaseXHalfDuplex* = {14};
	BMESR_1000BaseTFullDuplex* = {13};
	BMESR_1000BaseTHalfDuplex* = {12};
	BMESR_Reserved* = {0..11};

	(* PHY Identifier Register 1 (PHYIDR1) *)
	PHYIDR1_OuiMSB* = {0..15};

	(* PHY Identifier Register 2 (PHYIDR2) *)
	PHYIDR2_OuiLSB* = {10..15};
	PHYIDR2_VendorModel* = {4..9};
	PHYIDR2_ModelRevision* = {0..3};

	(* Auto Negotiation Advertisement Register (ANAR) *)
	(* Auto Negotiation Link Partner Ability Register (ANLPAR) *)
	ANAR_NextPageIndication* = {15};
	ANAR_Ackknowlegdement* = {14};
	ANAR_RemoteFault* = {13};
	ANAR_Reserved* = {11..12}; (* write as zero *)
	ANAR_AsymmetricPause* = {11};
	ANAR_Pause* = {10};
	ANAR_100BaseT4Support* = {9};
	ANAR_100BaseTXFullDuplex* = {8};
	ANAR_100BaseTXHalfDuplex* = {7};
	ANAR_10BaseTFullDuplex* = {6};
	ANAR_10BaseTHalfDuplex* = {5};
	ANAR_Selector* = {0..4};

	(* ANAR/ANLPAR Selector field *)
	Selector_IEEE_STD802_3* = 1;
	Selector_IEEE_STD802_9ISLAN16T* = 2;

	(* Auto Negotiation Expansion Register (ANER) *)
	ANER_Reserved* = {5..15}; (* write as zero *)
	ANER_ParallelDetectionFault* = {4};
	ANER_LinkPartnerNextPageEnable* = {3};
	ANER_PhyNextPageEnable* = {2};
	ANER_NewPageReception* = {1};
	ANER_LinkPartnerAnEnable* = {0};

тип

	Identifier* = запись
		oui- : цел32; (* Organizationally Unique Identifier *)
		model- : цел32;
		revision- : цел32;
	кон;

тип

	MII* = окласс
	перем
		phyId- : цел32;

		lockedBy : динамическиТипизированныйУкль;
		timer : Kernel.Timer;

		(** Abstract interface to be implemented by the implementor *)

		(** Acqurie PHY ownership *)
		проц AcquirePhyOwnership*() : булево; (* abstract *)
		нач СТОП(301); кон AcquirePhyOwnership;

		(** Release PHY ownership *)
		проц ReleasePhyOwnership*() : булево; (* abstract *)
		нач СТОП(301); кон ReleasePhyOwnership;

		проц HasPhyOwnership() : булево; (* abstract *)
		нач СТОП(301); кон HasPhyOwnership;

		(** Read a MII/GMII register *)
		проц ReadRegister16*(register: цел32; перем value : мнвоНаБитахМЗ; перем res : целМЗ); (* abstract *)
		нач СТОП(301); кон ReadRegister16;

		(** Write a MII/GMII register *)
		проц WriteRegister16*(register : цел32; value : мнвоНаБитахМЗ; перем res : целМЗ); (* abstract *)
		нач СТОП(301); кон WriteRegister16;

		(** MII Management Interface *)

		проц Acquire*;
		перем me : динамическиТипизированныйУкль;
		нач {единолично}
			me := Objects.ActiveObject();
			утв(lockedBy # me); (* no recursive locking *)
			дождись(lockedBy = НУЛЬ);
			если ~AcquirePhyOwnership() то
				Show("Serious error: Software could not acquire PHY ownership."); ЛогЯдра.пВК_ПС;
			всё;
			lockedBy := me;
		кон Acquire;

		проц Release*;
		нач {единолично}
			утв(lockedBy = Objects.ActiveObject());
			если ~ReleasePhyOwnership() то
				Show("Fatal error: Software could not release PHY ownership."); ЛогЯдра.пВК_ПС;
			всё;
			lockedBy := НУЛЬ;
		кон Release;

		(** Enable/disable auto-negotiation *)
		проц EnableAutoNegotiation*(enable : булево; перем res : целМЗ);
		перем anar, bmcr, bmsr : мнвоНаБитахМЗ;
		нач
			утв(lockedBy = Objects.ActiveObject());
			(* Check wether PHY supports auto-negotiation *)
			ReadRegister16(BMSR, bmsr, res);
			если res # Ok то возврат; всё;

			если bmsr * BMSR_AutoNegotiationAbility = {} то
				если Trace * TraceCommands # {} то Show("Auto-Negotiation not supported."); ЛогЯдра.пВК_ПС; всё;
				res := Unsupported;
				возврат;
			всё;

			если bmsr * BMSR_ExtendedCapability = {} то
				если Trace * TraceCommands # {} то Show("Extended registers not available."); ЛогЯдра.пВК_ПС; всё;
				res := Unsupported;
				возврат;
			всё;

			(* Set abilities to be advertised *)
			anar := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Selector_IEEE_STD802_3) * ANAR_Selector;

			если bmsr * BMSR_100BaseT4 # {} то anar := anar + ANAR_100BaseT4Support; всё;
			если bmsr * BMSR_100BaseTXFullDuplex # {} то anar := anar + ANAR_100BaseTXFullDuplex; всё;
			если bmsr * BMSR_100BaseTXHalfDuplex # {} то anar := anar + ANAR_100BaseTXHalfDuplex; всё;
			если bmsr * BMSR_10BaseTFullDuplex # {} то anar := anar + ANAR_10BaseTFullDuplex; всё;
			если bmsr * BMSR_10BaseTHalfDuplex # {} то anar := anar + ANAR_10BaseTHalfDuplex; всё;
			если bmsr * BMSR_100BaseT2FullDuplex # {} то (* TODO *) всё;
			если bmsr * BMSR_100BaseT2HalfDuplex # {} то (* TODO *) всё;
			(* TODO: 1GB *)

			anar := anar + ANAR_Pause;

			WriteRegister16(ANAR, anar, res);
			если res # Ok то возврат всё;

			ReadRegister16(BMCR, bmcr, res);
			если res # Ok то возврат; всё;

			если enable то bmcr := bmcr + BMCR_AutoNegotiationEnable; иначе bmcr := bmcr - BMCR_AutoNegotiationEnable; всё;

			WriteRegister16(BMCR, bmcr, res);
			если res # Ok то возврат; всё;

			если enable то
				WriteRegister16(BMCR, bmcr + BMCR_RestartAutoNegotiation, res);
			всё;
		кон EnableAutoNegotiation;

		(** Get link configuration negotiated by auto-negotiation *)
		проц GetAutoNegotiationResult*(перем linkspeed : цел32; перем fullDuplex : булево; перем res : целМЗ);
		перем bmsr, anar, aner, anlpar, supported : мнвоНаБитахМЗ; timeout : цел32; string : массив 64 из симв8;
		нач
			утв(lockedBy = Objects.ActiveObject());
			ReadRegister16(BMSR, bmsr, res);
			если res # Ok то возврат; всё;

			если bmsr * BMSR_AutoNegotiationAbility = {} то
				если Trace * TraceAutoNegotiation # {} то Show("Auto-Negotiation not supported."); ЛогЯдра.пВК_ПС; всё;
				res := Unsupported;
				возврат;
			всё;

			если bmsr * BMSR_ExtendedCapability = {} то
				если Trace * TraceAutoNegotiation# {} то Show("Extended registers not available."); ЛогЯдра.пВК_ПС; всё;
				res := Unsupported;
				возврат;
			всё;

			ReadRegister16(ANER, aner, res);
			если res # Ok то возврат; всё;

			если aner * ANER_LinkPartnerAnEnable = {} то
				если Trace * TraceAutoNegotiation # {} то Show("Link partner has not enabled auto-negotiation."); ЛогЯдра.пВК_ПС; всё;
				res := NotAvailable;
				возврат;
			всё;

			timeout := AutoNegotiationTimeout;
			нцДо
				ReadRegister16(BMSR, bmsr, res);
				если res # Ok то возврат всё;
				timer.Sleep(20);
				умень(timeout, 20);
			кцПри (bmsr * BMSR_AutoNegotiationComplete # {}) или (timeout <= 0);

			если bmsr * BMSR_AutoNegotiationComplete = {} то
				если Trace * TraceAutoNegotiation # {} то Show("Auto-Negotiation not complete."); ЛогЯдра.пВК_ПС; всё;
				res := NotAvailable;
				возврат;
			всё;

			ReadRegister16(ANAR, anar, res);
			если res # Ok то возврат; всё;

			ReadRegister16(ANLPAR, anlpar, res);
			если res # Ok то возврат; всё;

			если НИЗКОУР.подмениТипЗначения(цел32, anlpar * ANAR_Selector) # Selector_IEEE_STD802_3 то
				Show("Link partner doesn't use IEEE 802.3 Auto-Negotiation _ Other types not supported."); ЛогЯдра.пВК_ПС;
				res := Unsupported;
				возврат;
			всё;

			supported := anar * anlpar;

			(* Priority Resolution according [1], Annex 28B *)
			если supported * ANAR_100BaseTXFullDuplex # {} то
				linkspeed := 100; fullDuplex := истина; string := "100BASETX Full Duplex";
			аесли supported * ANAR_100BaseTXHalfDuplex # {} то
				linkspeed := 100; fullDuplex := ложь; string := "100BASETX Half Duplex";
			аесли supported * ANAR_100BaseT4Support # {} то
				linkspeed := 100; fullDuplex := истина; string := "100BASET4";
			аесли supported * ANAR_10BaseTFullDuplex # {} то
				linkspeed := 10; fullDuplex := истина; string := "10BASET Full Duplex";
			аесли supported * ANAR_10BaseTHalfDuplex # {} то
				linkspeed := 10; fullDuplex := ложь; string := "10BASET Half Duplex";
			иначе
			всё;

			если Trace * TraceAutoNegotiation # {} то Show("Detected "); ЛогЯдра.пСтроку8(string); ЛогЯдра.пВК_ПС; всё;
		кон GetAutoNegotiationResult;

		(** Get the current link status *)
		проц GetLinkStatus*(перем linkStatus: цел32; перем res : целМЗ);
		перем value : мнвоНаБитахМЗ;
		нач
			утв(lockedBy = Objects.ActiveObject());
			ReadRegister16(BMSR, value, res);
			если res = Ok то
				если value * BMSR_LinkStatus # {} то linkStatus := Network.LinkLinked;
				иначе linkStatus := Network.LinkNotLinked;
				всё;
			всё;
		кон GetLinkStatus;

		(** Manually set the link speed and operating mode. This disables auto-negotiation as side-effect. *)
		проц ConfigureLink*(speed : цел32; fullDuplex : булево; перем res : целМЗ);
		перем value : мнвоНаБитахМЗ;
		нач
			утв(lockedBy = Objects.ActiveObject());
			утв((speed = 10) или (speed = 100) или (speed = 1000));
			(* determine whether PHY supports the desired link configuration *)
			ReadRegister16(BMSR, value, res);
			если res # Ok то возврат; всё;
			(* BMSR_100BaseT4 ???? *)
			если (speed = 10) и ((fullDuplex и (value * BMSR_10BaseTFullDuplex = {})) или (~fullDuplex и (value * BMSR_10BaseTHalfDuplex = {}))) то
				res := ParametersInvalid; возврат;
			аесли (speed = 100) и ((fullDuplex и (value * (BMSR_100BaseTXFullDuplex + BMSR_100BaseT2FullDuplex) = {})) или
					(~fullDuplex и (value * (BMSR_100BaseTXHalfDuplex + BMSR_100BaseT2HalfDuplex) = {}))) то
				res := ParametersInvalid; возврат;
			аесли (speed = 1000) то
				если value * BMSR_ExtendedStatus = {} то res := ParametersInvalid; возврат; всё;
				ReadRegister16(BMESR, value, res);
				если res # Ok то возврат; всё;
				если (fullDuplex и (value * (BMESR_1000BaseXFullDuplex + BMESR_1000BaseTFullDuplex) = {})) или
					(~fullDuplex и (value * (BMESR_1000BaseXHalfDuplex + BMESR_1000BaseTHalfDuplex) = {})) то
					res := ParametersInvalid; возврат;
				всё;
			всё;
			(* disable auto-negotiation if it's enabled *)
			ReadRegister16(BMCR, value, res);
			если res # Ok то возврат; всё;

			если value * BMCR_AutoNegotiationEnable # {} то
				WriteRegister16(BMCR, value - BMCR_AutoNegotiationEnable, res);
				если res # Ok то возврат; всё;
			всё;

			(* set link speed and duplex mode *)
			если fullDuplex то value := value + BMCR_DuplexMode; иначе value := value - BMCR_DuplexMode; всё;
			если speed = 10 то
				value := value - BMCR_SpeedSelectionMsb - BMCR_SpeedSelectionLsb;
			аесли speed = 100 то
				value := value - BMCR_SpeedSelectionMsb + BMCR_SpeedSelectionLsb;
			иначе
				value := value + BMCR_SpeedSelectionMsb - BMCR_SpeedSelectionLsb;
			всё;

			WriteRegister16(BMCR, value, res);
		кон ConfigureLink;

		проц EnableLoopback*(enable : булево; перем res : целМЗ);
		перем value : мнвоНаБитахМЗ;
		нач
			утв(lockedBy = Objects.ActiveObject());
			если Trace * TraceCommands # {} то
				если enable то Show("Enable "); иначе Show("Disable "); всё; ЛогЯдра.пСтроку8("loopback."); ЛогЯдра.пВК_ПС;
			всё;
			ReadRegister16(BMCR, value, res);
			если res # Ok то возврат; всё;
			если enable то value := value + BMCR_Loopback; иначе value := value - BMCR_Loopback; всё;
			WriteRegister16(BMCR, value + BMCR_Loopback, res);
		кон EnableLoopback;

		(** Reset the PHY. Sets the Control and the Status registers to their default values. *)
		проц Reset*(перем res : целМЗ);
		перем timeout, interval : цел32; value : мнвоНаБитахМЗ;
		нач (*  {EXCLUSIVE} *)
			утв(lockedBy = Objects.ActiveObject());
			если Trace * TraceCommands # {} то Show("Reset PHY... "); всё;
			WriteRegister16(BMCR, BMCR_Reset, res);
			если res # Ok то возврат; всё;
			(* 0.5s timeout according to [1], page 512 *)
			timeout := 500; interval := 10;
			нц
				timer.Sleep(interval);
				ReadRegister16(BMCR, value, res);
				если (value * BMCR_Reset = {}) или (res # Ok) то прервиЦикл; всё;
				умень(timeout, interval);
				если timeout <= 0 то res := Timeout; прервиЦикл; всё
			кц;
			если Trace * TraceCommands # {} то ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС; всё;
		кон Reset;

		проц GetIdentifier*(перем identifier : Identifier; перем res : целМЗ);
		перем phyIdr1, phyIdr2 : мнвоНаБитахМЗ;
		нач
			утв(lockedBy = Objects.ActiveObject());
			ReadRegister16(PHYIDR1, phyIdr1, res);
			если res # Ok то
				если Debug то Show("Could not read PHYIDR1 register."); ЛогЯдра.пВК_ПС; всё;
				возврат;
			всё;
			ReadRegister16(PHYIDR2, phyIdr2, res);
			если res # Ok то
				если Debug то Show("Could not read PHYIDR2 register."); ЛогЯдра.пВК_ПС; всё;
				возврат;
			всё;
			identifier.oui := НИЗКОУР.подмениТипЗначения(цел32, логСдвиг(phyIdr1 * PHYIDR1_OuiMSB, 3) + логСдвиг(phyIdr2 * PHYIDR2_OuiLSB, 9));
			identifier.model := НИЗКОУР.подмениТипЗначения(цел32, логСдвиг(phyIdr2 * PHYIDR2_VendorModel, -4));
			identifier.revision := НИЗКОУР.подмениТипЗначения(цел32, phyIdr2 * PHYIDR2_ModelRevision);
		кон GetIdentifier;

		проц Diag*;
		перем identifier : Identifier; res : целМЗ; bmsr, anar, anlpar, register : мнвоНаБитахМЗ;
		нач
			утв(lockedBy = Objects.ActiveObject());
			ЛогЯдра.пСтроку8("MII information:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("   PHY ID: "); ЛогЯдра.п16ричное(phyId, 0); ЛогЯдра.пВК_ПС;
			GetIdentifier(identifier, res);
			если res = Ok то
				ЛогЯдра.пСтроку8("   OUI: "); ЛогЯдра.п16ричное(identifier.oui, 0); ЛогЯдра.пСтроку8(", Vendor model: "); ЛогЯдра.пЦел64(identifier.model, 0);
				ЛогЯдра.пСтроку8(", Revision: "); ЛогЯдра.пЦел64(identifier.revision, 0);
			иначе
				ЛогЯдра.пСтроку8("Error: Could not read PHY Identification registers."); ЛогЯдра.пВК_ПС;
			всё;
			ЛогЯдра.пВК_ПС;
			ReadRegister16(BMCR, register, res);
			если res = Ok то ShowControlRegister(register);
			иначе ЛогЯдра.пСтроку8("Error: Could not read BMCR register."); ЛогЯдра.пВК_ПС;
			всё;
			ReadRegister16(BMSR, bmsr, res);
			если res = Ok то ShowStatusRegister(bmsr);
			иначе ЛогЯдра.пСтроку8("Error: Could not read BMSR register."); ЛогЯдра.пВК_ПС;
			всё;
			ЛогЯдра.пСтроку8("Extended registers: "); ЛогЯдра.пВК_ПС;
			если bmsr * BMSR_ExtendedCapability # {} то
				ReadRegister16(ANAR, anar, res);
				если res = Ok то
					ReadRegister16(ANLPAR, anlpar, res);
					если res = Ok то
						ShowAdvertisementRegisters(anar, anlpar);
					иначе ЛогЯдра.пСтроку8("Error: Could not read ANLPAR register."); ЛогЯдра.пВК_ПС;
					всё;
				иначе ЛогЯдра.пСтроку8("Error: Could not read ANAR register."); ЛогЯдра.пВК_ПС;
				всё;
				ReadRegister16(ANER, register, res);
				если res = Ok то
					ShowAnerRegister(register);
				иначе ЛогЯдра.пСтроку8("Error: Could not read ANER register."); ЛогЯдра.пВК_ПС;
				всё;
			иначе
				ЛогЯдра.пСтроку8("Extended registers not available."); ЛогЯдра.пВК_ПС;
			всё;
			если bmsr * BMSR_ExtendedStatus # {} то
				ReadRegister16(BMESR, register, res);
				если res = Ok то ShowExtendedStatusRegister(register);
				иначе ЛогЯдра.пСтроку8("Error: Could not read BMESR register."); ЛогЯдра.пВК_ПС;
				всё;
			всё;
			если bmsr * BMSR_ExtendedCapability # {} то
			всё;
		кон Diag;

		проц &Init*(phyId : цел32);
		нач
			сам.phyId := phyId;
			нов(timer);
			lockedBy := НУЛЬ;
		кон Init;

	кон MII;

проц ShowControlRegister(bmcr : мнвоНаБитахМЗ);
нач
	ЛогЯдра.пСтроку8("Basic Mode Control Register (BMCR):"); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Reset PHY: "); ЛогЯдра.пБулево(bmcr * BMCR_Reset # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Loopback: "); ЛогЯдра.пБулево(bmcr * BMCR_Loopback # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Auto-Negotiation Enable: "); ЛогЯдра.пБулево(bmcr * BMCR_AutoNegotiationEnable # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Power Down: "); ЛогЯдра.пБулево(bmcr * BMCR_PowerDown # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Isolate: "); ЛогЯдра.пБулево(bmcr * BMCR_Isolate # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Restart Auto-Negotiation: "); ЛогЯдра.пБулево(bmcr * BMCR_RestartAutoNegotiation # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Collision Test: "); ЛогЯдра.пБулево(bmcr * BMCR_CollisionTest # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Link Speed: ");
	если bmcr * BMCR_AutoNegotiationEnable = {} то
		если bmcr * BMCR_SpeedSelectionMsb = {} то
			если bmcr * BMCR_SpeedSelectionLsb # {} то ЛогЯдра.пСтроку8("100 Mbps");
			иначе ЛогЯдра.пСтроку8("10 Mbps");
			всё;
		иначе
			если bmcr * BMCR_SpeedSelectionMsb # {} то ЛогЯдра.пСтроку8("1000 Mbps");
			иначе ЛогЯдра.пСтроку8("Invalid setting");
			всё;
		всё;
		если bmcr * BMCR_DuplexMode # {} то ЛогЯдра.пСтроку8(" (Full Duplex)"); иначе ЛогЯдра.пСтроку8(" (Half Duplex)"); всё;
	иначе ЛогЯдра.пСтроку8("Reported in BMSR since Auto-negotiation is enabled");
	всё;
	ЛогЯдра.пВК_ПС;
кон ShowControlRegister;

проц ShowStatusRegister(bmsr : мнвоНаБитахМЗ);
нач
	ЛогЯдра.пСтроку8("Basic Mode Status Register (BMSR):"); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Link Capabilities:"); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      100BaseT4: "); ЛогЯдра.пБулево(bmsr * BMSR_100BaseT4 # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      100BaseX Full Duplex: "); ЛогЯдра.пБулево(bmsr * BMSR_100BaseTXFullDuplex # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      100BaseX Half Duplex: "); ЛогЯдра.пБулево(bmsr * BMSR_100BaseTXHalfDuplex # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      10Mb/s Full Duplex: "); ЛогЯдра.пБулево(bmsr * BMSR_10BaseTFullDuplex # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      10Mb/s Half Fuplex: "); ЛогЯдра.пБулево(bmsr * BMSR_10BaseTHalfDuplex # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      100BaseT2FullDuplex: "); ЛогЯдра.пБулево(bmsr * BMSR_100BaseT2FullDuplex # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      100BaseT2HalfDuplex: "); ЛогЯдра.пБулево(bmsr * BMSR_100BaseT2HalfDuplex  # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Extended Status Register available: "); ЛогЯдра.пБулево(bmsr * BMSR_ExtendedStatus # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   MF Preamble Suppression: "); ЛогЯдра.пБулево(bmsr * BMSR_MfPreambleSuppression # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Auto-Negotiation complete: "); ЛогЯдра.пБулево(bmsr * BMSR_AutoNegotiationComplete # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Remote Fault: "); ЛогЯдра.пБулево(bmsr * BMSR_RemoteFault # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Auto-Negotiation Ability: "); ЛогЯдра.пБулево(bmsr * BMSR_AutoNegotiationAbility # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Link status: ");
	если bmsr * BMSR_LinkStatus # {} то ЛогЯдра.пСтроку8("UP"); иначе ЛогЯдра.пСтроку8("DOWN"); всё; ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Jabber Detected: "); ЛогЯдра.пБулево(bmsr * BMSR_JabberDetect # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Extended Capability: "); ЛогЯдра.пБулево(bmsr * BMSR_ExtendedCapability # {}); ЛогЯдра.пВК_ПС;
кон ShowStatusRegister;

проц ShowAdvertisementRegisters(anar, anlpar : мнвоНаБитахМЗ);

	проц ShowBit(конст title : массив из симв8; bit : мнвоНаБитахМЗ);
	нач
		ЛогЯдра.пСтроку8(title); ЛогЯдра.пБулево(anar * bit # {});
		ЛогЯдра.пСтроку8(", Link partner: "); ЛогЯдра.пБулево(anlpar * bit # {}); ЛогЯдра.пВК_ПС;
	кон ShowBit;

нач
	ЛогЯдра.пСтроку8("Auto-Negotiation Advertisement Registers (ANAR/ANLPAR):"); ЛогЯдра.пВК_ПС;
	ShowBit("   Next page indication: ", ANAR_NextPageIndication);
	ShowBit("   Ackknowlegdement: ", ANAR_Ackknowlegdement);
	ShowBit("   Remote Fault: ", ANAR_RemoteFault);
	ShowBit("   Asymmetric Pause: ", ANAR_AsymmetricPause);
	ShowBit("   Pause: ", ANAR_Pause);
	ShowBit("   100BaseT4 support: ", ANAR_100BaseT4Support);
	ShowBit("   100BaseTX Full Duplex support: ", ANAR_100BaseTXFullDuplex);
	ShowBit("   100BaseTX Half Duplex support: ", ANAR_100BaseTXHalfDuplex);
	ShowBit("   10BaseT Full Duplex support: ", ANAR_10BaseTFullDuplex);
	ShowBit("   10BaseT Half Duplex support: ", ANAR_10BaseTHalfDuplex);
	ЛогЯдра.пСтроку8("   Selector: "); ЛогЯдра.пЦел64(НИЗКОУР.подмениТипЗначения(цел32, anar * ANAR_Selector), 0);
	ЛогЯдра.пСтроку8(", Link partner: "); ЛогЯдра.пЦел64(НИЗКОУР.подмениТипЗначения(цел32, anlpar * ANAR_Selector), 0);
	ЛогЯдра.пВК_ПС;
кон ShowAdvertisementRegisters;

проц ShowAnerRegister(aner : мнвоНаБитахМЗ);
нач
	ЛогЯдра.пСтроку8("Auto-Negotiation Extension Register (ANER):"); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Parallel Detection Fault: "); ЛогЯдра.пБулево(aner * ANER_ParallelDetectionFault # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Link Partner Next Page Enable: "); ЛогЯдра.пБулево(aner * ANER_LinkPartnerNextPageEnable # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   PHY Next Page Enable: "); ЛогЯдра.пБулево(aner * ANER_PhyNextPageEnable # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   New Page Reception: "); ЛогЯдра.пБулево(aner * ANER_NewPageReception # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Link Partner Auto-Negotiation Enable: "); ЛогЯдра.пБулево(aner * ANER_LinkPartnerAnEnable # {}); ЛогЯдра.пВК_ПС;
кон ShowAnerRegister;

проц ShowExtendedStatusRegister(bmesr : мнвоНаБитахМЗ);
нач
	ЛогЯдра.пСтроку8("Base Mode Extended Status Register (BMESR):"); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("   Link Capabilities:"); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      1000BaseX Full Duplex: "); ЛогЯдра.пБулево(bmesr * BMESR_1000BaseXFullDuplex # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      1000BaseX Half Duplex: "); ЛогЯдра.пБулево(bmesr * BMESR_1000BaseXHalfDuplex # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      1000BaseT Full Duplex: "); ЛогЯдра.пБулево(bmesr * BMESR_1000BaseTFullDuplex # {}); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8("      1000BaseT Half Duplex: "); ЛогЯдра.пБулево(bmesr * BMESR_1000BaseTHalfDuplex # {}); ЛогЯдра.пВК_ПС;
кон ShowExtendedStatusRegister;

проц Show(конст string : массив из симв8);
нач
	ЛогЯдра.пСтроку8(ModuleName); ЛогЯдра.пСтроку8(": "); ЛогЯдра.пСтроку8(string);
кон Show;

кон NetworkMii.
