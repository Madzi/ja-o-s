модуль TestSuite; (** AUTHOR "negelef"; PURPOSE "Simple testing framework"; *)

использует Потоки, Files, Commands, Strings, TextUtilities, Diagnostics;

конст
	PositiveTest = 0;
	NegativeTest = 1;

	Positive* = 0;
	Negative* = 1;
	Failure* = 2;

тип
	TestType* = целМЗ;
	TestName = массив 100 из симв8;

	TestResult* = укль на запись
		type-: TestType;
		name-: TestName;
		succeeded-, new-: булево;
		next: TestResult
	кон;

	TestResultList = запись
		first, last: TestResult;
	кон;

	Report* = окласс

	перем tests-, succeeded-, succeededThisTime-, failed-, failedThisTime-: размерМЗ;

	проц Open*;
	кон Open;

	проц Handle* (result: TestResult);
	кон Handle;

	проц Close*;
	кон Close;

	кон Report;

	Tester* = окласс

	перем
		tests, results: TestResultList;
		diagnostics-: Diagnostics.Diagnostics;

	проц &Init* (diagnostics: Diagnostics.Diagnostics);
	нач сам.diagnostics := diagnostics;
	кон Init;

	проц Process* (r: Потоки.Чтец): булево;
	перем type: TestType; name: TestName; line: массив 200 из симв8;
		code: Strings.Buffer; writer : Потоки.Писарь;
		string : Strings.String; reader: Потоки.ЧтецИзСтроки;
	нач
		нов (code, 1000); writer := code.GetWriter ();
		ClearList (tests);
		нцПока SkipComment (r) делай
			если ~ReadType (r, type) или ~SkipWhitespace (r) или ~ReadText (r, name) то
				diagnostics.Error (name, r.МестоВПотоке(), "parse error"); возврат ложь;
			всё;
			если FindResult (tests, name) # НУЛЬ то
				diagnostics.Error (name, Потоки.НевернаяПозицияВПотоке, "duplicated test"); возврат ложь;
			всё;
			code.Clear; writer.Сбрось;
			нцПока SkipLn (r) и Tabulator (r) и ReadText (r, line) делай writer.пСимв8 (09X); writer.пСтроку8 (line); writer.пСимв8 (0AX); кц;
			string := code.GetString ();
			нов (reader, code.GetLength ());
			reader.ПримиСтроку8ДляЧтения (string^);
			AddResult (tests, type, name, Handle (reader, r.МестоВПотоке () - writer.МестоВПотоке () - 1, name, type) = type);
		кц;
		возврат истина;
	кон Process;

	проц Handle* (r: Потоки.Чтец; pos: Потоки.ТипМестоВПотоке; конст name: массив из симв8; type: TestType): целМЗ;
	кон Handle;

	проц Print* (report: Report);
	перем test, result: TestResult;
	нач
		report.tests := 0; report.succeeded := 0; report.succeededThisTime := 0; report.failed := 0; report.failedThisTime := 0;
		report.Open;
		test := tests.first;
		нцПока test # НУЛЬ делай
			увел (report.tests); если test.succeeded то увел (report.succeeded) иначе увел (report.failed) всё;
			result := FindResult (results, test.name);
			test.new := (result = НУЛЬ) или (test.succeeded # result.succeeded);
			если test.new то если test.succeeded то увел (report.succeededThisTime) иначе увел (report.failedThisTime) всё всё;
			если (~test.succeeded) или (test.new) то report.Handle (test) всё;
			test := test.next;
		кц;
		report.Close;
	кон Print;

	кон Tester;

	StreamReport* = окласс (Report)

	перем w: Потоки.Писарь; tw: TextUtilities.TextWriter;

		проц &InitStreamReport *(w: Потоки.Писарь);
		нач сам.w := w; если w суть TextUtilities.TextWriter то tw := w(TextUtilities.TextWriter) иначе tw := НУЛЬ всё;
		кон InitStreamReport;

		проц {перекрыта}Open*;
		нач w.пВК_ПС; Bold; w.пСтроку8 ("Test results:"); Default; w.пВК_ПС
		кон Open;

		проц Green;
		нач если tw # НУЛЬ то tw.SetFontColor (000C000FFH); tw.SetFontStyle ({0}) всё;
		кон Green;

		проц Red;
		нач если tw # НУЛЬ то tw.SetFontColor (цел32(0FF0000FFH)); tw.SetFontStyle ({0}) всё;
		кон Red;

		проц Orange;
		нач если tw # НУЛЬ то tw.SetFontColor (цел32 (0FFC000FFH)); tw.SetFontStyle ({0}) всё;
		кон Orange;

		проц Default;
		нач если tw # НУЛЬ то tw.SetFontColor (0000000FFH); tw.SetFontStyle ({}) всё;
		кон Default;

		проц Bold;
		нач если tw # НУЛЬ то tw.SetFontStyle ({0}) всё;
		кон Bold;

		проц {перекрыта}Handle* (test: TestResult);
		нач
			если test.type = PositiveTest то w.пСтроку8 ("positive: ");
			аесли test.type = NegativeTest то w.пСтроку8 ("negative: ") всё;
			w.пСтроку8 (test.name); w.пСтроку8 (": ");
			если test.succeeded то
				Green;
				w.пСтроку8 ("succeeded")
			иначе
				если test.new то Orange иначе Red всё;
				w.пСтроку8 ("failed")
			всё;
			Default; w.пВК_ПС
		кон Handle;

		проц {перекрыта}Close*;
		нач w.пВК_ПС; Bold; w.пСтроку8 ("Summary:"); Default; w.пВК_ПС;
			w.пСтроку8 ("number of tests:"); w.пСимв8 (9X); w.пЦел64 (tests, 0); w.пВК_ПС;
			w.пСтроку8 ("successful tests:"); w.пСимв8 (9X); если succeeded = tests то Green иначе Red всё; w.пЦел64 (succeeded, 0); Default;
			если succeededThisTime > 0 то w.пСимв8 (9X); w.пСимв8 ('('); w.пСимв8 ('+'); w.пЦел64 (succeededThisTime, 0); w.пСимв8 (')'); всё; w.пВК_ПС;
			w.пСтроку8 ("failed tests:"); w.пСимв8 (9X); w.пСимв8 (9X); если failed = 0 то Green иначе Red всё; w.пЦел64 (failed, 0); Default;
			если failedThisTime > 0 то w.пСимв8 (9X); w.пСимв8 ('('); w.пСимв8 ('+'); w.пЦел64 (failedThisTime, 0); w.пСимв8 (')'); всё; w.пВК_ПС;
		кон Close;

	кон StreamReport;

(* helper procedures for parsing *)

проц SkipComment (r: Потоки.Чтец): булево;
перем char: симв8;
нач char := r.ПодглядиСимв8 (); нцПока (char = '#') или  (char = 0AX) или (char = 0DX) делай r.ПропустиДоКонцаСтрокиТекстаВключительно; char := r.ПодглядиСимв8 (); кц; возврат (r.кодВозвратаПоследнейОперации = Потоки.Успех) и (char # 0X);
кон SkipComment;

проц SkipWhitespace (r: Потоки.Чтец): булево;
нач нцПока r.ПодглядиСимв8 () = ' ' делай r.ПропустиБайты (1) кц; возврат r.кодВозвратаПоследнейОперации = Потоки.Успех
кон SkipWhitespace;

проц SkipLn (r: Потоки.Чтец): булево;
нач нцПока (r.ПодглядиСимв8 () = 0AX) или (r.ПодглядиСимв8 () = 0DX) делай r.ПропустиБайты (1) кц; возврат r.кодВозвратаПоследнейОперации = Потоки.Успех
кон SkipLn;

проц ReadType (r: Потоки.Чтец; перем type: TestType): булево;
перем c: симв8; string: массив 10 из симв8; i: размерМЗ;
нач
	i := 0; r.чСимв8 (c);
	нцПока (c # ':') и (i # длинаМассива (string)) делай string[i] := c; увел (i); r.чСимв8 (c) кц;
	если i = длинаМассива (string) то возврат ложь всё;
	string[i] := 0X;
	если string = "positive" то type := PositiveTest; возврат истина
	аесли string = "negative" то type := NegativeTest; возврат истина
	иначе возврат ложь всё
кон ReadType;

проц ReadText (r: Потоки.Чтец; перем text: массив из симв8): булево;
нач r.чСтроку8ДоКонцаСтрокиТекстаВключительно (text); возврат r.кодВозвратаПоследнейОперации = Потоки.Успех
кон ReadText;

проц Tabulator (r: Потоки.Чтец): булево;
нач возврат (r.ПодглядиСимв8 () = 09X) и (r.чИДайСимв8 () = 09X)
кон Tabulator;

проц ReadBoolean (r: Потоки.Чтец; перем boolean: булево): булево;
перем value: целМЗ;
нач r.чЦел32 (value, ложь); boolean := value = 1; возврат r.кодВозвратаПоследнейОперации = Потоки.Успех
кон ReadBoolean;

проц ReadResults (r: Потоки.Чтец; перем list: TestResultList);
перем succeeded: булево; name: TestName;
нач нцПока ReadBoolean (r, succeeded) и SkipWhitespace (r) и ReadText (r, name) делай AddResult (list, 0, name, succeeded) кц
кон ReadResults;

проц WriteResults (w: Потоки.Писарь; конст list: TestResultList);
перем result: TestResult;
нач result := list.first;
	нцПока result # НУЛЬ делай
		если result.succeeded то w.пСимв8 ('1') иначе w.пСимв8 ('0') всё;
		w.пСимв8 (' '); w.пСтроку8 (result.name); w.пВК_ПС;
		result := result.next
	кц
кон WriteResults;

(* test results management *)

проц ClearList (перем list: TestResultList);
нач list.first := НУЛЬ; list.last := НУЛЬ
кон ClearList;

проц AddResult (перем list: TestResultList; type: TestType;  конст name: массив из симв8; succeeded: булево);
перем result: TestResult;
нач нов (result); копируйСтрокуДо0 (name, result.name); result.succeeded := succeeded; result.new := ложь; result.next := НУЛЬ; result.type := type;
	если list.first = НУЛЬ то list.first := result иначе list.last.next := result всё; list.last := result;
кон AddResult;

проц FindResult (конст list: TestResultList; конст name: массив из симв8): TestResult;
перем result: TestResult;
нач result := list.first; нцПока (result # НУЛЬ) и (result.name # name) делай result := result.next кц; возврат result
кон FindResult;

(* public interface helper *)
проц DriveByReader* (reader: Потоки.Чтец; error: Потоки.Писарь; конст resultname: массив из симв8; tester: Tester): булево;
перем resreader: Files.Reader;result: Files.File; writer: Files.Writer;
нач
	если reader = НУЛЬ то
		возврат истина;
	всё;
	ClearList (tester.results);
	если resultname # "" то
		result := Files.Old (resultname);
		если result # НУЛЬ то
			нов (resreader, result, 0); ReadResults (resreader, tester.results)
		всё
	всё;
	если ~tester.Process (reader) то возврат ложь всё;
	если resultname # "" то
		result := Files.New (resultname);
		если result = НУЛЬ то
			error.пСтроку8 ("Failed to open result file "); error.пСтроку8 (resultname); error.пВК_ПС;
			возврат ложь;
		иначе
			нов (writer, result, 0); WriteResults (writer, tester.tests); writer.ПротолкниБуферВПоток; Files.Register (result);
		всё
	всё;
	возврат истина;
кон DriveByReader;

(* public interface helper *)
проц Drive* (context: Commands.Context; tester: Tester);
перем testname, resultname: Files.FileName; test: Files.File; reader: Files.Reader;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках (testname) то
		test := Files.Old (testname);
		если test = НУЛЬ то
			context.error.пСтроку8 ("Failed to open test file "); context.error.пСтроку8 (testname); context.error.пВК_ПС;
			context.result := Commands.CommandError;
			возврат;
		всё;
	иначе
		context.result := Commands.CommandParseError;
	всё;
	нов (reader, test, 0);
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках (resultname) то
		resultname := "";
	всё;
	если DriveByReader(reader, context.error, resultname, tester) то
		context.result := Commands.CommandError;
	всё;
кон Drive;

кон TestSuite.
