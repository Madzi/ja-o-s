модуль WMTabComponents; (** AUTHOR "?/staubesv"; PURPOSE "Tab component" *)

использует
	ЛогЯдра, Strings, XML, WMEvents, WMProperties,
	WMStandardComponents, WMRectangles, WMComponents, WMGraphics;

тип

	Tab* = окласс
	перем
		caption- : Strings.String;
		w : размерМЗ;
		width : размерМЗ; (* if 0, automatically determine width based on caption string size *)
		color- : WMGraphics.Color;
		data- : динамическиТипизированныйУкль;
		inserted, attention* : булево;
		next- : Tab;

		проц &Init*;
		нач
			caption := НУЛЬ;
			w := 0; width := 0; color := 0;
			data := НУЛЬ;
			inserted := ложь; attention := ложь;
			next := НУЛЬ;
		кон Init;

	кон Tab;

	Tabs* = окласс(WMComponents.VisualComponent)
	перем
		left, right : WMStandardComponents.Button;
		leftOfs, totalWidth, border, lines : размерМЗ;
		lineHeight-: WMProperties.SizeProperty;
		tabs-, hover, selected- : Tab;
		canvasState : WMGraphics.CanvasState;

		onSelectTab- : WMEvents.EventSource;
		(* general look *)
		useBgBitmaps- : WMProperties.BooleanProperty;
		borderWidth- : WMProperties.Int32Property;
		(* colors *)
		clDefault-, clHover-, clSelected-, clAttention-, clSelectedAttention-,
		clTextDefault-, clTextHover-, clTextSelected-, clTextAttention, clTextSelectedAttention- : WMProperties.ColorProperty;
		(* background bitmaps *)
		bgLeftDefault-, bgMiddleDefault-, bgRightDefault-,
		bgLeftHover-, bgMiddleHover-, bgRightHover-,
		bgLeftSelected-, bgMiddleSelected-, bgRightSelected-,
		bgLeftAttention-, bgMiddleAttention-, bgRightAttention- : WMProperties.StringProperty;

		imgLeftDefault, imgMiddleDefault, imgRightDefault,
		imgLeftHover, imgMiddleHover, imgRightHover,
		imgLeftSelected, imgMiddleSelected, imgRightSelected,
		imgLeftAttention, imgMiddleAttention, imgRightAttention : WMGraphics.Image;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrTabs);
			SetGenerator("WMTabComponents.GenTabControl");
			lines := 1;
			нов(left);
			left.alignment.Set(WMComponents.AlignLeft);
			left.bounds.SetWidth(10);
			left.isRepeating.Set(истина);
			left.onClick.Add(MoveLeft);
			left.visible.Set(ложь);

			нов(right);
			right.alignment.Set(WMComponents.AlignRight);
			right.bounds.SetWidth(10);
			right.isRepeating.Set(истина);
			right.onClick.Add(MoveRight);
			right.visible.Set(ложь);

			AddInternalComponent(left); AddInternalComponent(right);

			нов(onSelectTab, сам, Strings.NewString("onSelectTab"), Strings.NewString("if tab clicked"), сам.StringToCompCommand);
			(* general look *)
			нов(borderWidth, ProtoTcBorderWidth, НУЛЬ, НУЛЬ); properties.Add(borderWidth);
			нов(useBgBitmaps, ProtoTcUseBgBitmaps, НУЛЬ, НУЛЬ); properties.Add(useBgBitmaps);
			(* background colors *)
			fillColor.SetPrototype(ProtoTcDefault);
			нов(clDefault, ProtoTcDefault, НУЛЬ, НУЛЬ); properties.Add(clDefault);
			нов(clHover, ProtoTcHover, НУЛЬ, НУЛЬ); properties.Add(clHover);
			нов(clSelected, ProtoTcSelected, НУЛЬ, НУЛЬ); properties.Add(clSelected);
			нов(clAttention, ProtoTcAttention, НУЛЬ, НУЛЬ); properties.Add(clAttention);
			нов(clSelectedAttention, ProtoTcSelectedAttention, НУЛЬ, НУЛЬ); properties.Add(clSelectedAttention);
			(* text colors *)
			нов(clTextDefault, ProtoTcTextDefault, НУЛЬ, НУЛЬ); properties.Add(clTextDefault);
			нов(clTextHover, ProtoTcTextHover, НУЛЬ, НУЛЬ); properties.Add(clTextHover);
			нов(clTextSelected, ProtoTcTextSelected,  НУЛЬ, НУЛЬ); properties.Add(clTextSelected);
			нов(clTextAttention, ProtoTcTextAttention,  НУЛЬ, НУЛЬ); properties.Add(clTextAttention);
			нов(clTextSelectedAttention, ProtoTcTextSelectedAttention, НУЛЬ, НУЛЬ); properties.Add(clTextSelectedAttention);
			(* background bitmaps *)
			нов(bgLeftDefault, ProtoTcBgLeftDefault, НУЛЬ, НУЛЬ); properties.Add(bgLeftDefault);
			нов(bgMiddleDefault, ProtoTcBgMiddleDefault, НУЛЬ, НУЛЬ); properties.Add(bgMiddleDefault);
			нов(bgRightDefault, ProtoTcBgRightDefault, НУЛЬ, НУЛЬ); properties.Add(bgRightDefault);

			нов(bgLeftHover, ProtoTcBgLeftHover, НУЛЬ, НУЛЬ); properties.Add(bgLeftHover);
			нов(bgMiddleHover, ProtoTcBgMiddleHover, НУЛЬ, НУЛЬ); properties.Add(bgMiddleHover);
			нов(bgRightHover, ProtoTcBgRightHover, НУЛЬ, НУЛЬ); properties.Add(bgRightHover);

			нов(bgLeftSelected, ProtoTcBgLeftSelected, НУЛЬ, НУЛЬ); properties.Add(bgLeftSelected);
			нов(bgMiddleSelected, ProtoTcBgMiddleSelected, НУЛЬ, НУЛЬ); properties.Add(bgMiddleSelected);
			нов(bgRightSelected, ProtoTcBgRightSelected, НУЛЬ, НУЛЬ); properties.Add(bgRightSelected);

			нов(bgLeftAttention, ProtoTcBgLeftAttention, НУЛЬ, НУЛЬ); properties.Add(bgLeftAttention);
			нов(bgMiddleAttention, ProtoTcBgMiddleAttention, НУЛЬ, НУЛЬ); properties.Add(bgMiddleAttention);
			нов(bgRightAttention, ProtoTcBgRightAttention, НУЛЬ, НУЛЬ); properties.Add(bgRightAttention);

			нов(lineHeight, ProtoLineHeight, НУЛЬ, НУЛЬ); properties.Add(lineHeight);
		кон Init;

		проц {перекрыта}Initialize*;
		нач
			Initialize^;
			CheckLeftRightButtons;
		кон Initialize;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если 	(property = useBgBitmaps) или
				(property = bgLeftDefault) или (property = bgMiddleDefault) или (property = bgRightDefault) или
				(property = bgLeftHover) или (property = bgMiddleHover) или (property = bgRightHover) или
				(property = bgLeftSelected) или (property = bgMiddleSelected) или (property = bgRightSelected) или
				(property = bgLeftAttention) или (property = bgMiddleAttention) или (property = bgRightAttention)
			то
				RecacheProperties; Invalidate;
			аесли (property = borderWidth) или
				(property = clDefault) или (property = clHover) или (property = clSelected) или (property = clAttention) или
				(property = clSelectedAttention) или (property = clTextDefault) или (property = clTextHover) или
				(property = clTextSelected) или (property = clTextAttention) или (property = clTextSelectedAttention)
			то
				Invalidate;
			аесли (property = bounds) то
				CalcSize; PropertyChanged^(sender, property);
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц {перекрыта}RecacheProperties*;
		перем s : Strings.String;
		нач
			RecacheProperties^;
			если useBgBitmaps.Get() то
				s := bgLeftDefault.Get();	если s # НУЛЬ то imgLeftDefault := WMGraphics.LoadImage(s^, истина) всё;
				s := bgMiddleDefault.Get();	если s # НУЛЬ то imgMiddleDefault := WMGraphics.LoadImage(s^, истина) всё;
				s := bgRightDefault.Get();	если s # НУЛЬ то imgRightDefault := WMGraphics.LoadImage(s^, истина) всё;

				s := bgLeftHover.Get();	если s # НУЛЬ то imgLeftHover := WMGraphics.LoadImage(s^, истина) всё;
				s := bgMiddleHover.Get();	если s # НУЛЬ то imgMiddleHover := WMGraphics.LoadImage(s^, истина) всё;
				s := bgRightHover.Get();	если s # НУЛЬ то imgRightHover := WMGraphics.LoadImage(s^, истина) всё;

				s := bgLeftSelected.Get();	если s # НУЛЬ то imgLeftSelected := WMGraphics.LoadImage(s^, истина) всё;
				s := bgMiddleSelected.Get();	если s # НУЛЬ то imgMiddleSelected := WMGraphics.LoadImage(s^, истина) всё;
				s := bgRightSelected.Get();	если s # НУЛЬ то imgRightSelected := WMGraphics.LoadImage(s^, истина) всё;

				s := bgLeftAttention.Get();	если s # НУЛЬ то imgLeftAttention := WMGraphics.LoadImage(s^, истина) всё;
				s := bgMiddleAttention.Get();	если s # НУЛЬ то imgMiddleAttention := WMGraphics.LoadImage(s^, истина) всё;
				s := bgRightAttention.Get();	если s # НУЛЬ то imgRightAttention := WMGraphics.LoadImage(s^, истина) всё;
			иначе
				imgLeftDefault := НУЛЬ; imgMiddleDefault := НУЛЬ; imgRightDefault := НУЛЬ;
				imgLeftHover := НУЛЬ; imgMiddleHover := НУЛЬ; imgRightHover := НУЛЬ;
				imgLeftSelected := НУЛЬ; imgMiddleSelected := НУЛЬ; imgRightSelected := НУЛЬ;
				imgLeftAttention := НУЛЬ; imgMiddleAttention := НУЛЬ; imgRightAttention := НУЛЬ;
			всё;
			(*Invalidate*)
		кон RecacheProperties;

		проц FindTabFromPos(x,y: размерМЗ) : Tab;
		перем cur : Tab; pos,posy, dl, w,h: размерМЗ;
		нач
			если left.visible.Get() то dl := left.bounds.GetWidth() иначе dl := 0 всё;
			h := lineHeight.Get();
			если h = 0 то h := bounds.GetHeight() всё;
			pos := - leftOfs + dl; posy := 0;
			cur := tabs;
			нцПока cur # НУЛЬ делай
				w := cur.w;
				если pos + w > bounds.GetWidth() то увел(posy, h); pos := dl; (*RETURN NIL*) всё;
				pos := pos + w;
				если (x < pos) и (y < posy+h) то возврат cur всё;
				cur := cur.next
			кц;
			возврат НУЛЬ
		кон FindTabFromPos;

		проц {перекрыта}PointerDown*(x, y: размерМЗ; keys: мнвоНаБитахМЗ); (** PROTECTED *)
		перем new : Tab;
		нач
			если 0 в keys то
				new := FindTabFromPos(x,y);
				если (selected # new) и (new # НУЛЬ) то
					selected := new;
					onSelectTab.Call(selected);
					Invalidate
				всё
			всё
		кон PointerDown;

		проц Select*(new : Tab);
		нач
			Acquire;
			если selected # new то
				selected := new;
				Invalidate
			всё;
			Release
		кон Select;

		проц SelectByName*(конст name : массив из симв8);
		перем tab : Tab; found : булево;
		нач
			found := ложь;
			Acquire;
			tab := tabs;
			нцПока ~found и (tab # НУЛЬ) делай
				если tab.inserted и (tab.caption # НУЛЬ) то
					found := tab.caption^ = name;
				всё;
				если ~found то tab := tab.next; всё;
			кц;
			если found и (selected # tab) то selected := tab; Invalidate; всё;
			Release;
		кон SelectByName;

		проц {перекрыта}PointerMove*(x, y: размерМЗ; keys: мнвоНаБитахМЗ); (** PROTECTED *)
		перем  new : Tab;
		нач
			new := FindTabFromPos(x,y);
			если hover # new то
				hover := new;
				Invalidate
			всё
		кон PointerMove;

		проц {перекрыта}PointerLeave*;
		нач
			hover := НУЛЬ;
			Invalidate
		кон PointerLeave;


		проц GetLeftTabs(перем w: размерМЗ; inner: булево): булево;
		перем cur : Tab; font : WMGraphics.Font; dx, dy, lh,dl : размерМЗ; width, pos: размерМЗ;
		нач
			если left.visible.Get() то dl := left.bounds.GetWidth() иначе dl := 0 всё;
			font := GetFont();
			cur := tabs;
			pos := -leftOfs+dl;
			нцПока cur # НУЛЬ делай
				если cur.width # 0 то
					w := cur.width
				аесли cur.caption # НУЛЬ то
					font.GetStringSize(cur.caption^, dx, dy);
					w := dx + 2 * border
				иначе
					w := 2 * border
				всё;
				если (pos >= 0) и inner то возврат истина всё;
				pos := pos + w;
				если (pos >= 0) и ~inner то возврат истина всё;
				cur := cur.next;
			кц;
			возврат ложь
		кон GetLeftTabs;

		проц MoveLeft(sender, data : динамическиТипизированныйУкль);
		перем w: размерМЗ;
		нач
			если GetLeftTabs(w, ложь) то
				умень(leftOfs, w)
			всё;
			(*DEC(leftOfs, 10);*)
			если leftOfs < 0 то leftOfs := 0 всё;
			CalcSize;
			Invalidate;
		кон MoveLeft;

		проц MoveRight(sender, data : динамическиТипизированныйУкль);
		перем w: размерМЗ;
		нач
			если GetLeftTabs(w, истина) то
				увел(leftOfs, w)
			всё;
			(*
			INC(leftOfs, 10);
			*)
			если leftOfs > totalWidth - 10 то leftOfs := totalWidth - 10 всё;
			CalcSize;
			Invalidate;
		кон MoveRight;

		проц AddTab*(tab : Tab);
		перем cur : Tab;
		нач
			Acquire;
			tab.next := НУЛЬ; tab.inserted := истина;
			если tabs = НУЛЬ то tabs := tab; selected := tab;
			иначе
				cur := tabs;
				нцПока cur.next # НУЛЬ делай cur := cur.next кц;
				cur.next := tab
			всё;
			CalcSize;
			Release;
			Invalidate
		кон AddTab;

		проц RemoveTab*(tab : Tab);
		перем cur : Tab;
		нач
			если (tabs = НУЛЬ) или (tab = НУЛЬ)  то возврат всё;
			Acquire;
			если tabs = tab то tabs := tabs.next
			иначе
				cur := tabs;
				нцПока (cur # НУЛЬ) и (cur.next # tab) делай cur := cur.next кц;
				если cur # НУЛЬ то cur.next := cur.next.next всё
			всё;
			CalcSize;
			tab.inserted := ложь;
			Release;
			Invalidate
		кон RemoveTab;

		проц RemoveAllTabs*;
		нач
			Acquire;
			tabs := НУЛЬ;
			CalcSize;
			Release;
			Invalidate
		кон RemoveAllTabs;

		проц CheckLeftRightButtons;
		нач
			если (totalWidth >= bounds.GetWidth()) или (lines > 1)  то
				right.visible.Set(истина);
				left.visible.Set(истина)
			иначе
				leftOfs := 0;
				right.visible.Set(ложь);
				left.visible.Set(ложь)
			всё
		кон CheckLeftRightButtons;

		проц {перекрыта}Resized*;
		нач
			Resized^;
			CheckLeftRightButtons
		кон Resized;

		проц CalcSize;
		перем cur : Tab; font : WMGraphics.Font; dx, dy, lh : размерМЗ; width, w, pos: размерМЗ;
		нач
			font := GetFont();
			totalWidth := 0; width := 0; lines := 1; lh := lineHeight.Get();
			cur := tabs;
			pos := -leftOfs;

			нцПока cur # НУЛЬ делай
				если cur.width # 0 то
					w := cur.width
				аесли cur.caption # НУЛЬ то
					font.GetStringSize(cur.caption^, dx, dy);
					w := dx + 2 * border
				иначе
					w := 2 * border
				всё;
				если (pos + w > bounds.GetWidth()) и (lh # 0) то
					width := 0; pos := 0; увел(lines);
				всё;
				width := width + w; pos := pos + w;
				cur := cur.next;
				если width > totalWidth то totalWidth := width всё;
			кц;
			если lh # 0 то bounds.SetHeight(lines * lh) всё;
			CheckLeftRightButtons
		кон CalcSize;

		проц SetTabCaption*(tab : Tab; caption : Strings.String);
		нач
			Acquire;
			tab.caption := caption;
			CalcSize;
			Release;
			если tab.inserted то Invalidate всё
		кон SetTabCaption;

		проц SetTabColor*(tab : Tab; color : WMGraphics.Color);
		нач
			Acquire;
			tab.color := color;
			Release;
			если tab.inserted то Invalidate всё
		кон SetTabColor;

		(* Set fixed width for the specified tab. If width = 0, the width of the tab is detemined by the width of the caption String *)
		проц SetTabWidth*(tab : Tab; width : цел32);
		нач
			Acquire;
			tab.width := width;
			CalcSize;
			Release;
			если tab.inserted то Invalidate; всё;
		кон SetTabWidth;

		проц SetTabData*(tab : Tab; data : динамическиТипизированныйУкль);
		нач
			Acquire;
			tab.data := data;
			Release;
			если tab.inserted то Invalidate всё
		кон SetTabData;

		проц NewTab*() : Tab;
		перем tab : Tab;
		нач
			нов(tab); возврат tab
		кон NewTab;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем r : WMRectangles.Rectangle;
			w, h, lh, dl, dr, wLeft, wRight : размерМЗ;
			pos,ypos : размерМЗ; dx, dy, dc : размерМЗ;
			cur : Tab; font : WMGraphics.Font;
			imgLeft, imgMiddle, imgRight : WMGraphics.Image;
		нач
			border := borderWidth.Get();
			font := GetFont();
			dc := font.descent;
(*			DrawBackground^(canvas); *)
			lh := lineHeight.Get();
			если lh # 0 то h := lh иначе h:= bounds.GetHeight() всё;
			w := bounds.GetWidth();

			если left.visible.Get() то dl := left.bounds.GetWidth() иначе dl := 0 всё;
			если right.visible.Get() то dr := right.bounds.GetWidth() иначе dr := 0 всё;
			canvas.SaveState(canvasState);
			canvas.SetClipRect(WMRectangles.MakeRect(dl, 0, w - dr, bounds.GetHeight()));
			canvas.ClipRectAsNewLimits(dl, 0);

			pos := - leftOfs;
			cur := tabs;
			нцПока cur # НУЛЬ делай
				если cur.width # 0 то
					w := cur.width;
				аесли cur.caption # НУЛЬ то
					font.GetStringSize(cur.caption^, dx, dy); w := dx + 2 * border;
				иначе
					w := 2 * border
				всё;
				cur.w := w;
				если pos + w >  bounds.GetWidth() то
					pos := 0;
					ypos := ypos + h;
				всё;

				r := WMRectangles.MakeRect(pos, ypos, pos + w, ypos+h);

				если useBgBitmaps.Get() то
					если cur = hover то
						imgLeft := imgLeftHover;
						imgMiddle := imgMiddleHover;
						imgRight := imgRightHover;
					аесли cur = selected то
						imgLeft := imgLeftSelected;
						imgMiddle := imgMiddleSelected;
						imgRight := imgRightSelected;
					аесли cur.attention то
						imgLeft := imgLeftAttention;
						imgMiddle := imgMiddleAttention;
						imgRight := imgRightAttention;
					иначе
						imgLeft := imgLeftDefault;
						imgMiddle := imgMiddleDefault;
						imgRight := imgRightDefault
					всё;
					(* left *)
			 		если imgLeft # НУЛЬ то
						wLeft := imgLeft.width;
						canvas.ScaleImage(	imgLeft,
						 	WMRectangles.MakeRect(0, 0, imgLeft.width, imgLeft.height),
							WMRectangles.MakeRect(pos, 0, pos+wLeft, bounds.GetHeight()),
							WMGraphics.ModeSrcOverDst, 10)
					иначе
						wLeft := 0
					всё;
					(* right *)
			 		если imgRight # НУЛЬ то
						wRight := imgRight.width;
						canvas.ScaleImage(	imgRight,
						 	WMRectangles.MakeRect(0, 0, imgRight.width, imgRight.height),
							WMRectangles.MakeRect(pos+w-wRight, 0, pos+w, bounds.GetHeight()),
							WMGraphics.ModeSrcOverDst, 10)
					иначе
						wRight := 0
					всё;
					(* middle *)
					если imgMiddle # НУЛЬ то
						canvas.ScaleImage(	imgMiddle,
								 	WMRectangles.MakeRect(0, 0, imgMiddle.width, imgMiddle.height),
									WMRectangles.MakeRect(pos+wLeft, 0, pos+w-wRight, bounds.GetHeight()), WMGraphics.ModeSrcOverDst, 10)
					всё
				иначе (* no bitmaps are used to decorate the background *)
					если cur = hover то
						canvas.Fill(r, clHover.Get(), WMGraphics.ModeSrcOverDst)
					аесли cur = selected то
						если (cur.attention) то
							canvas.Fill(r, clSelectedAttention.Get(), WMGraphics.ModeSrcOverDst);
						иначе
							canvas.Fill(r, clSelected.Get(), WMGraphics.ModeSrcOverDst)
						всё;
					аесли cur.attention то
						canvas.Fill(r, clAttention.Get(), WMGraphics.ModeSrcOverDst)
					иначе
						если cur.color # 0 то canvas.Fill(r, cur.color, WMGraphics.ModeSrcOverDst)
						иначе canvas.Fill(r, clDefault.Get(), WMGraphics.ModeSrcOverDst)
						всё
					всё;
					RectGlassShade(canvas, r, {2}, 2, cur = selected)
				всё;
				(* caption *)
				если cur = hover то
					canvas.SetColor(clTextHover.Get());
				аесли cur = selected то
					если (cur.attention) то
						canvas.SetColor(clTextSelectedAttention.Get());
					иначе
						canvas.SetColor(clTextSelected.Get());
					всё;
				аесли cur.attention то
					canvas.SetColor(clTextAttention.Get());
				иначе
					canvas.SetColor(clTextDefault.Get());
				всё;
				если cur.caption # НУЛЬ то canvas.DrawString(r.l + border , r.b - dc - 1, cur.caption^) всё;
				pos := pos + w;
				cur := cur.next
			кц;
			canvas.RestoreState(canvasState)
		кон DrawBackground;

	кон Tabs;

тип

	TabEntry* = окласс(WMStandardComponents.Panel)
	перем
		caption- : WMProperties.StringProperty;
		color- : WMProperties.ColorProperty;
		tabWidth- : WMProperties.Int32Property;

		tab : Tab;
		tabs : Tabs;

		next : TabEntry;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = caption) то
				если (tabs # НУЛЬ) и (tab # НУЛЬ) то
					tabs.SetTabCaption(tab, caption.Get());
				всё;
			аесли (property = color) то
				если (tabs # НУЛЬ) и (tab # НУЛЬ) то
					tabs.SetTabColor(tab, color.Get());
				всё;
			аесли (property = tabWidth) то
				если (tabs # НУЛЬ) и (tab # НУЛЬ) то
					tabs.SetTabWidth(tab, tabWidth.Get());
				всё;
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrTab);
			SetGenerator("WMTabComponents.GenTab");
			нов(caption, НУЛЬ, StrCaption, StrCaptionDescription); properties.Add(caption);
			caption.Set(StrNoCaption);
			нов(color, НУЛЬ, StrColor, StrColorDescription); properties.Add(color);
			color.Set(0);
			нов(tabWidth, НУЛЬ, StrTabWidth, StrTabWidthDescription); properties.Add(tabWidth);
			tabWidth.Set(0);
			tab := НУЛЬ;
			tabs := НУЛЬ;
			next := НУЛЬ;
		кон Init;

	кон TabEntry;

тип

	TabPanel* = окласс(WMStandardComponents.Panel)
	перем
		selection- : WMProperties.StringProperty;
		entries : TabEntry;
		first : булево;
		tabs : Tabs;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrTabPanel);
			SetGenerator("WMTabComponents.GenTabPanel");
			нов(selection, НУЛЬ, StrSelection, StrSelectionDescription); properties.Add(selection);
			selection.Set(StrNoSelection);
			first := истина;
			tabs := НУЛЬ;
			entries := НУЛЬ;
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		перем string : Strings.String;
		нач
			если (property = selection) то
				string := selection.Get();
				если (tabs # НУЛЬ) и (string # НУЛЬ) то
					tabs.SelectByName(string^);
				всё;
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц TabSelected(sender, data : динамическиТипизированныйУкль);
		перем e : TabEntry; tab : Tab;
		нач
			если (data # НУЛЬ) и (data суть Tab) то
				tab := data(Tab);
				DisableUpdate;
				нач {единолично}
					e := entries;
					нцПока (e # НУЛЬ) делай
						если (tab.data # НУЛЬ) и (tab.data суть TabEntry) то
							e.visible.Set(tab.data = e);
						всё;
						e := e.next;
					кц;
				кон;
				EnableUpdate;
				Invalidate;
				selection.Set(Strings.NewString(data(Tab).caption^));
			всё;
		кон TabSelected;

		проц {перекрыта}AddContent*(c : XML.Content);
		перем entry : TabEntry;  tab : Tab; string : Strings.String; select : булево;
		нач
			если (c суть Tabs) то
				если (tabs = НУЛЬ) то
					tabs := c(Tabs);
					tabs.onSelectTab.Add(TabSelected);
				иначе
					ЛогЯдра.пСтроку8("WMTabComponents: Warning: Cannot add TabControl component (already set)");
					ЛогЯдра.пВК_ПС;
				всё;
			аесли (c суть TabEntry) то
				entry := c(TabEntry);
				если (tabs # НУЛЬ) то
					tab := tabs.NewTab();
					tab.data := entry;
					tab.caption := entry.caption.Get();
					tab.color := entry.color.Get();
					tab.width := entry.tabWidth.Get();
					если (tab.caption = НУЛЬ) то tab.caption := Strings.NewString("NoCaption"); всё;
					entry.alignment.Set(WMComponents.AlignClient);
					string := selection.Get();
					если (string = StrNoSelection) и first то
						entry.visible.Set(истина);
						first := ложь;
						select := истина;
					аесли (string # НУЛЬ) и (string^ = tab.caption^) то
						entry.visible.Set(истина);
						select := истина;
					иначе
						entry.visible.Set(ложь);
						select := ложь;
					всё;
					нач {единолично}
						если (entries = НУЛЬ) то
							entries := entry;
						иначе
							entry.next := entries;
							entries := entry;
						всё;
					кон;
					entry.tabs := tabs; entry.tab := tab;
					tabs.AddTab(tab);
					если select то tabs.Select(tab); всё;
				иначе
					ЛогЯдра.пСтроку8("WMTabComponents: Warning: Cannot add tab (missing TabControl component)");
					ЛогЯдра.пВК_ПС;
				всё;
			всё;
			AddContent^(c);
		кон AddContent;

	кон TabPanel;

перем
	ColorPrototype, ProtoTcDefault*, ProtoTcHover*, ProtoTcSelected*, ProtoTcAttention*, ProtoTcSelectedAttention*,
	ProtoTcTextDefault*, ProtoTcTextHover*, ProtoTcTextSelected*, ProtoTcTextAttention, ProtoTcTextSelectedAttention* : WMProperties.ColorProperty;
	Int32Prototype, ProtoTcBorderWidth* : WMProperties.Int32Property;
	StringPrototype, ProtoTcBgLeftDefault, ProtoTcBgMiddleDefault, ProtoTcBgRightDefault,
	ProtoTcBgLeftHover, ProtoTcBgMiddleHover, ProtoTcBgRightHover,
	ProtoTcBgLeftSelected, ProtoTcBgMiddleSelected, ProtoTcBgRightSelected,
	ProtoTcBgLeftAttention, ProtoTcBgMiddleAttention, ProtoTcBgRightAttention : WMProperties.StringProperty;
	BooleanPrototype, ProtoTcUseBgBitmaps : WMProperties.BooleanProperty;
	ProtoLineHeight : WMProperties.SizeProperty;

	StrTabs, StrTabPanel, StrTab,
	StrCaption, StrCaptionDescription, StrNoCaption,
	StrColor, StrColorDescription,
	StrTabWidth, StrTabWidthDescription,
	StrSelection, StrSelectionDescription, StrNoSelection : Strings.String;

проц GenTabPanel*() : XML.Element;
перем p : TabPanel;
нач
	нов(p); возврат p;
кон GenTabPanel;

проц GenTabControl*() : XML.Element;
перем c : Tabs;
нач
	нов(c); возврат c;
кон GenTabControl;

проц GenTab*() : XML.Element;
перем t : TabEntry;
нач
	нов(t); возврат t;
кон GenTab;

проц RectGlassShade*(canvas : WMGraphics.Canvas; rect : WMRectangles.Rectangle; openSides : мнвоНаБитахМЗ; borderWidth : цел32; down : булево);
перем i, ul, dr, da, w, a, b, c, d : цел32;
нач
	если down то ul := 090H; dr := цел32(0FFFFFF90H)
	иначе dr := 090H; ul := цел32(0FFFFFF90H)
	всё;
	da := 90H DIV borderWidth;
	нцДля i := 0 до borderWidth - 1 делай
		если  (0 в openSides) то a := 0 иначе a := i всё;
		если  (1 в openSides) то b := 0 иначе b := i + 1 всё;
		если  (2 в openSides) то c := 0 иначе c := i всё;
		если  (3 в openSides) то d := 0 иначе d := i + 1 всё;
		(* top *)
		если ~(0 в openSides) то canvas.Fill(WMRectangles.MakeRect(rect.l + b , rect.t + i, rect.r - d, rect.t + i + 1), ul, WMGraphics.ModeSrcOverDst) всё;
		(* left *)
		если ~(1 в openSides) то canvas.Fill(WMRectangles.MakeRect(rect.l + i, rect.t + a, rect.l + i + 1, rect.b - c), ul, WMGraphics.ModeSrcOverDst) всё;
		(* bottom *)
		если ~(2 в openSides) то canvas.Fill(WMRectangles.MakeRect(rect.l + b, rect.b - 1 - i, rect.r - d, rect.b - i), dr, WMGraphics.ModeSrcOverDst) всё;
		(* right *)
		если ~(3 в openSides) то canvas.Fill(WMRectangles.MakeRect(rect.r - 1 - i, rect.t + a, rect.r - i, rect.b - c), dr, WMGraphics.ModeSrcOverDst) всё;
		умень(ul, da); умень(dr, da)
	кц;
	i := 3; ul := цел32(0FFFFFF40H); w := 5;
	canvas.Fill(WMRectangles.MakeRect(rect.l + i , rect.t + i, rect.l + i + w, rect.t + i + 2), ul, WMGraphics.ModeSrcOverDst);
	canvas.Fill(WMRectangles.MakeRect(rect.l + i, rect.t + i, rect.l + i + 2, rect.t + i + w), ul, WMGraphics.ModeSrcOverDst);
кон RectGlassShade;

проц InitStrings;
нач
	StrTabs := Strings.NewString("Tabs");
	StrTabPanel := Strings.NewString("TabPanel");
	StrTab := Strings.NewString("Tab");
	StrCaption := Strings.NewString("Caption");
	StrNoCaption := Strings.NewString("NoCaption");
	StrCaptionDescription := Strings.NewString("Caption of tab");
	StrColor := Strings.NewString("Color");
	StrColorDescription := Strings.NewString("Color of tab (0: use tab default color)");
	StrTabWidth := Strings.NewString("TabWidth");
	StrTabWidthDescription := Strings.NewString("Width of the tab button");
	StrSelection := Strings.NewString("Selection");
	StrSelectionDescription := Strings.NewString("SelectionDescription");
	StrNoSelection := Strings.NewString("");
кон InitStrings;

проц InitPrototypes;
перем plTabs: WMProperties.PropertyList;
нач
	нов(plTabs); WMComponents.propertyListList.Add("Tab", plTabs);
	(* tab background *)
	нов(BooleanPrototype, НУЛЬ, Strings.NewString("UseBgBitmaps"), Strings.NewString("Will the background be decorated with bitmaps?"));
	BooleanPrototype.Set(ложь);
	нов(ProtoTcUseBgBitmaps, BooleanPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcUseBgBitmaps);
	(* background colors *)
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClDefault"), Strings.NewString("color of the tab item"));
	ColorPrototype.Set(0000FF88H);
	нов(ProtoTcDefault, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcDefault);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClHover"), Strings.NewString("color of the tab item, if the mouse is over it"));
	ColorPrototype.Set(цел32(0FFFF00FFH));
	нов(ProtoTcHover, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcHover);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClSelected"), Strings.NewString("color of the the tab item, if it is selected"));
	ColorPrototype.Set(цел32(0FFFF00FFH));
	нов(ProtoTcSelected, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcSelected);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClAttention"), Strings.NewString("color of the the tab item, if attention is required"));
	ColorPrototype.Set(цел32(0FF8040FFH));
	нов(ProtoTcAttention, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcAttention);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClSelectedAttention"), Strings.NewString("color of the tab item, if it is selected and requires attention"));
	ColorPrototype.Set(цел32(0FF9020FFH));
	нов(ProtoTcSelectedAttention, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcSelectedAttention);
	(* background bitmaps *)
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgLeftDefault"), Strings.NewString("Left default background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgLeftDefault, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgLeftDefault);
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgMiddleDefault"), Strings.NewString("Middle default background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgMiddleDefault, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgMiddleDefault);
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgRightDefault"), Strings.NewString("Right default background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgRightDefault, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgRightDefault);

	нов(StringPrototype, НУЛЬ, Strings.NewString("BgLeftHover"), Strings.NewString("Left mouseover background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgLeftHover, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgLeftHover);
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgMiddleHover"), Strings.NewString("Middle mouseover background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgMiddleHover, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgMiddleHover);
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgRightHover"), Strings.NewString("Right mouseover background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgRightHover, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgRightHover);

	нов(StringPrototype, НУЛЬ, Strings.NewString("BgLeftSelected"), Strings.NewString("Left selected background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgLeftSelected, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgLeftSelected);
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgMiddleSelected"), Strings.NewString("Middle selected background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgMiddleSelected, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgMiddleSelected);
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgRightSelected"), Strings.NewString("Right selected background bitmap"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgRightSelected, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgRightSelected);

	нов(StringPrototype, НУЛЬ, Strings.NewString("BgLeftAttention"), Strings.NewString("Left background bitmap when attention is required"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgLeftAttention, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgLeftAttention);
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgMiddleAttention"), Strings.NewString("Middle background bitmap when attention is required"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgMiddleAttention, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgMiddleAttention);
	нов(StringPrototype, НУЛЬ, Strings.NewString("BgRightAttention"), Strings.NewString("Right background bitmap when attention is required"));
	StringPrototype.Set(НУЛЬ); нов(ProtoTcBgRightAttention, StringPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcBgRightAttention);
	(* text colors *)
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClTextDefault"), Strings.NewString("default text color of the tab  item")); ColorPrototype.Set(WMGraphics.Yellow);
	нов(ProtoTcTextDefault, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcTextDefault);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClTextHover"), Strings.NewString("text color of the tab item, if the mouse is over it")); ColorPrototype.Set(00000FFFFH);
	нов(ProtoTcTextHover, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcTextHover);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClTextSelected"), Strings.NewString("text color of the tab item, when selected")); ColorPrototype.Set(0000FFFFH);
	нов(ProtoTcTextSelected, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcTextSelected);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClTextAttention"), Strings.NewString("text color of the tab item, when attention is required")); ColorPrototype.Set(0000FFFFH);
	нов(ProtoTcTextAttention, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcTextAttention);
	нов(ColorPrototype, НУЛЬ, Strings.NewString("ClTextSelectedAttention"),
		Strings.NewString("text color of the tab item, when selected and attention is required")); ColorPrototype.Set(0000FFFFH);
	нов(ProtoTcTextSelectedAttention, ColorPrototype, НУЛЬ, НУЛЬ); plTabs.Add(ProtoTcTextSelectedAttention);
	(* border width *)
	нов(Int32Prototype, НУЛЬ, Strings.NewString("BorderWidth"), Strings.NewString("Width of the border of the tabs")); Int32Prototype.Set(3);
	нов(ProtoTcBorderWidth, Int32Prototype, НУЛЬ, НУЛЬ);	plTabs.Add(ProtoTcBorderWidth);

	нов(ProtoLineHeight, НУЛЬ, Strings.NewString("LineHeight"), Strings.NewString("height of a single line. If zero then no multiline support")); ProtoLineHeight.Set(0);

	WMComponents.propertyListList.UpdateStyle;
кон InitPrototypes;

нач
	InitStrings;
	InitPrototypes;
кон WMTabComponents.

System.Free WMTabComponents ~
WMTabComponents.Open ~
