(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль BenchObjCount;	(* pjm *)

(* Test how many active objects can be created. *)

использует НИЗКОУР, ЛогЯдра, Machine, Kernel;

тип
	Tester = окласс
		перем next: Tester; stop: булево;

		проц Stop;
		нач {единолично}
			stop := истина
		кон Stop;

		проц &Init*;
		нач
			stop := ложь
		кон Init;

	нач {активное, единолично}
		дождись(stop)
	кон Tester;

проц Report(конст msg: массив из симв8; n, time: цел32);
нач
	ЛогЯдра.ЗахватВЕдиноличноеПользование;
	ЛогЯдра.пЦел64(n, 1); ЛогЯдра.пСтроку8(" threads "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пСтроку8(", ");
	ЛогЯдра.пЦел64(time, 1); ЛогЯдра.пСтроку8("ms");
	если n # 0 то
		ЛогЯдра.пСтроку8(", "); ЛогЯдра.пЦел64(округлиВниз(time/n*1000), 1); ЛогЯдра.пСтроку8("us/thread")
	всё;
	ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
кон Report;

проц Max*;
перем n, time: цел32; total, low, high: размерМЗ; root, t: Tester;
нач
	n := Kernel.GetTicks();
	нцДо time := Kernel.GetTicks() кцПри time # n;

	n := 0; root := НУЛЬ;
	нц
		Machine.GetFreeK(total, low, high);
		если low+high < 1024 то прервиЦикл всё;
		нов(t); t.next := root; root := t;
		увел(n)
	кц;
	time := округлиВниз((Kernel.GetTicks() - time)/Kernel.second*1000);
	Report("created", n, time);

	time := Kernel.GetTicks();
	нцПока root # НУЛЬ делай root.Stop; t := root; root := root.next; t.next := НУЛЬ кц;
	time := округлиВниз((Kernel.GetTicks() - time)/Kernel.second*1000);
	Report("stopped", n, time);

	time := Kernel.GetTicks();
	Kernel.GC;
	time := округлиВниз((Kernel.GetTicks() - time)/Kernel.second*1000);
	Report("collected", n, time);	(* not accurate, because interrupts currently disabled during GC *)

	time := Kernel.GetTicks();
	(*Heaps.CallFinalizers;*)	(* safe, if we are in an Oberon command *)
	time := округлиВниз((Kernel.GetTicks() - time)/Kernel.second*1000);
	Report("finalized", n, time)
кон Max;

кон BenchObjCount.

System.Free BenchObjCount ~

System.State Machine ~

System.OpenKernelLog

Configuration.DoCommands
BenchObjCount.Max
System.Time start
System.Collect
System.Time lap
System.Watch
~
