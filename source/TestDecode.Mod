MODULE TestDecode; (** AUTHOR "BohdanT"; PURPOSE "Visual part of the decoder"; *)

IMPORT
	KernelLog, Modules, Strings, Streams,
	(* visual part *)
	WMComponents, WMStandardComponents,
	WMGraphics,
	WMStringGrids, WMGrids,WM := WMWindowManager,
	(*user part*)
	BtDecoder;

TYPE
	Window* = OBJECT (WMComponents.FormWindow)
	VAR 
		codelist : WMStringGrids.StringGrid;
		spacings : WMGrids.Spacings;

		PROCEDURE CreateForm() : WMComponents.VisualComponent;
		VAR
			panel : WMStandardComponents.Panel;
			i:SIGNED32;
		BEGIN
			NEW(panel);
			panel.bounds.SetExtents(800, 700);
			panel.fillColor.Set(SIGNED32(0FFFFFFFFH));

			NEW(codelist); codelist.alignment.Set(WMComponents.AlignClient); 
			
			NEW(spacings, 4); spacings[0] := 80;spacings[1] := 60;spacings[2] := 140;spacings[3] := 160;

			codelist.Acquire;
			codelist.defaultRowHeight.Set(14);
			codelist.cellDist.Set(0);
			codelist.clCell.Set(SIGNED32(0FFFFFFA0H));
			codelist.SetColSpacings(spacings);
			codelist.SetFont(WMGraphics.GetFont("Courier", 10, {}));
			codelist.SetSelectionMode(WMGrids.GridSelectSingleRow);
			codelist.model.Acquire;
			codelist.model.SetNofCols(4);
			codelist.model.SetNofRows(1);
			codelist.model.SetCellText(0, 0, Strings.NewString("Adr"));
			codelist.model.SetCellText(1, 0, Strings.NewString("Instr"));
			codelist.model.SetCellText(2, 0, Strings.NewString("Arg"));
			codelist.model.SetCellText(3, 0, Strings.NewString("vars"));
			codelist.SetTopPosition(0, 0, TRUE);
			codelist.model.Release;
			codelist.Release;
			panel.AddContent(codelist);

			RETURN panel
		END CreateForm;
		
		PROCEDURE Test;
		VAR
			mi:BtDecoder.ModuleInfo;
			mdi:BtDecoder.ModuleInfoObjectFile;
			opcodes,p : BtDecoder.Opcode;
			i,n:SIGNED32;

			value : ARRAY 255 OF CHAR;
			st : Streams.StringWriter;
			s:Strings.String;
		BEGIN
			NEW(mdi,"WMDebugger.Obw");
			mi:=mdi;
			opcodes:=mi.GetOpcodes(mi.GetProcedureByIndex(2));
			n:=0;
			p:=opcodes;
			WHILE p # NIL DO
				INC(n);
				p := p.next;
			END;
			codelist.model.Acquire;
			codelist.model.SetNofRows(n+1);

			FOR i:=1 TO n DO
				NEW(s,10);
				Strings.IntToHexStr(opcodes.offset,8,s^);
				codelist.model.SetCellText(0, i, s);
				NEW(st, 20); opcodes.PrintInstruction(st);st.Get(value);
				codelist.model.SetCellText(1, i, Strings.NewString(value));

				NEW(st, 20); opcodes.PrintArguments(st);st.Get(value);
				codelist.model.SetCellText(2, i, Strings.NewString(value));

				NEW(st, 30); opcodes.PrintVariables(st);st.Get(value);
				codelist.model.SetCellText(3, i, Strings.NewString(value));
				opcodes := opcodes.next;
			END;
			codelist.model.Release;
		END Test;
		
		
		PROCEDURE &New*;
		VAR vc : WMComponents.VisualComponent;
		BEGIN
			vc := CreateForm ();
			Init (vc.bounds.GetWidth (), vc.bounds.GetHeight (), FALSE);
			SetContent (vc);
			WM.DefaultAddWindow (SELF);
			SetTitle (Strings.NewString ("Test Decoder"));
		END New;


	END Window;


VAR 
	winstance : Window;

PROCEDURE Open*;
BEGIN
	IF winstance = NIL THEN NEW (winstance); END;	(* Only one window may be instantiated. *)
END Open;

PROCEDURE Test*;
BEGIN
	IF winstance # NIL THEN 
		winstance.Test;
	END;	(* Only one window may be instantiated. *)
END Test;

PROCEDURE Cleanup;
BEGIN
	IF 	winstance # NIL THEN
		winstance.Close ();
		winstance := NIL
	END;
END Cleanup;

BEGIN
	Modules.InstallTermHandler(Cleanup)	

END TestDecode.Open~

TestDecode.Test~