(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль ЛогЯдра; (** AUTHOR "pjm"; PURPOSE "Trace output for booting and debugging"; *)

(* AFI 12.03.2003 - procedure Init modified to obtain trace port info from Aos.Par i.o. being hardcoded. 

*)

использует НИЗКОУР, Трассировка, Machine, Objects, UCS32;

конст
	BufSize = 8000;	(* default trace buffer size (usually overriden by System.StartLog or LogWindow.Open *)

перем
	traceBufDef: массив BufSize из симв8;	(* default trace buffer *)
	traceBufAdr: адресВПамяти; traceBufSize: размерМЗ;	(* current trace buffer virtual addresses *)
	traceHead, traceTail: адресВПамяти;

(** Send the specified characters to the trace output (cf. Streams.Sender). *)

проц ЗапишиВПоток*(конст буфер: массив из симв8; начальноеСмещениеВБуфере˛байт, сколькоБайтЗаписать: размерМЗ; propagate: булево; перем кодВозврата: целМЗ);
перем next: адресВПамяти; c: симв8;
нач
	увел(сколькоБайтЗаписать, начальноеСмещениеВБуфере˛байт);	(* len is now end position *)
	Machine.Acquire(Machine.TraceOutput);
	нц
		если начальноеСмещениеВБуфере˛байт >= сколькоБайтЗаписать то прервиЦикл всё;
		c := буфер[начальноеСмещениеВБуфере˛байт];
		если c = 0X то прервиЦикл всё;
		next := (traceTail+1) остОтДеленияНа traceBufSize;
		если next # traceHead то
			НИЗКОУР.запиши8битПоАдресу(traceBufAdr+traceTail, c);
			traceTail := next
		иначе	(* overwrite previous character with overflow signal *)
			НИЗКОУР.запиши8битПоАдресу(traceBufAdr + (traceTail-1) остОтДеленияНа traceBufSize, 3X)
		всё;
		Трассировка.пСимв8 (c);
		увел(начальноеСмещениеВБуфере˛байт)
	кц;
	Machine.Release(Machine.TraceOutput)
кон ЗапишиВПоток;

(** Write a string to the trace output. *)

проц пСтроку8*(конст s: массив из симв8);
перем len, n: размерМЗ; res: целМЗ;
нач
	len := 0; n := длинаМассива(s);
	нцПока (len # n) и (s[len] # 0X) делай увел(len) кц;
	ЗапишиВПоток(s, 0, len, ложь, res)
кон пСтроку8;


		(** Encode one UCS-32 value in ucs into one well-formed UTF-8 character. Sm takzhe UTF8Strings.EncodeChar *)
проц пСимв32˛представленныйТипомЦел32˛вЮ8*(ucs: бцел32);
перем buf: массив 7 из симв8; byte, i, mask, max: цел16;
нач
	если (ucs <= 7FH) то
		пСимв8(симв8ИзКода(устарПреобразуйКБолееУзкомуЦел(ucs)));
		возврат 
	иначе
		byte := 5; mask := 7F80H; max := 3FH;
		нцПока (ucs > max) делай
			buf[byte] := симв8ИзКода(80H + устарПреобразуйКБолееУзкомуЦел(ucs остОтДеленияНа 40H)); увел(byte, -1); (* CHR(80H + SHORT(AND(ucs, 3FH))) *)
			ucs := ucs DIV 64; (* LSH(ucs, -6) *)
			mask := mask DIV 2; (* 80H + LSH(mask, -1). Left-most bit remains set after DIV (mask is negative) *)
			max := max DIV 2 (* LSH(max, -1) *)
		кц;
		buf[byte] := симв8ИзКода(mask + устарПреобразуйКБолееУзкомуЦел(ucs)); 
		нцДля i := 0 до 5 - byte делай
			buf[i] := buf[i + byte] кц;	
		buf[6 - byte]	:= 0X;
		пСтроку8(buf) всё кон пСимв32˛представленныйТипомЦел32˛вЮ8;


		(** Encode one UCS-32 value in ucs into one well-formed UTF-8 character. Sm takzhe UTF8Strings.EncodeChar *)
проц пСимв32_вЮ8*(ucs: UCS32.CharJQ);
нач
	пСимв32˛представленныйТипомЦел32˛вЮ8(ucs.UCS32CharCode) кон пСимв32_вЮ8;

(* НАЧАЛО Этот кусок есть в Streams, KernelLog и Trace, но в Trace отличаются флаги! *)

(** 
   Напечатай заканчивающуюся нулём строку UCS32 в формате UTF8.
	rezhim: 0 - просто выводит буквы
	1 - выводит буквы и завершает их нулём (как RawString)
	2 - выводит буквы в таких кавычках „“, закавычивая закрывающую кавычку, LF выводит как есть, Tab - как \t, прочие 
		управляющие буквы и буквы вне диапазона по кириллицу - в виде \NNN;
	3 - выводит буквы в таких кавычках \" "\, кавычку закавычивает, LF выводит как есть, Tab - как \t, 
		прочие управляющие буквы и буквы вне диапазона по кириллицу - в виде \NNN;  *)
проц пСтроку32*(конст з : UCS32.StringJQ; режим : бцел8);
	перем i: размерМЗ; kod : бцел32; 
нач
	i := 0;
	если режим = 2 то пСимв32˛представленныйТипомЦел32˛вЮ8(UCS32.DoubleLowDash9QuotationMark) 
	аесли режим = 3 то пСтроку8('\"') 
	всё;
	kod := з[i].UCS32CharCode;
	нц
		если i = длинаМассива(з) то
			пСимв32˛представленныйТипомЦел32˛вЮ8(UCS32.ReplacementChar); прервиЦикл всё;
		kod := з[i].UCS32CharCode;
		если kod = 0 то
			если режим = 1 то
				пСимв8(0X)
			аесли режим = 2 то
				пСимв32˛представленныйТипомЦел32˛вЮ8(UCS32.LeftDoubleQuotationMark)
			аесли режим = 3 то
				пСтроку8('"\') всё;
			возврат;
		аесли (режим = 2) и (kod = UCS32.LeftDoubleQuotationMark) то
			пСимв8("\"); пСимв32˛представленныйТипомЦел32˛вЮ8(kod)
		аесли (режим = 3) и (kod = кодСимв8('"')) то
			пСимв8("\"); пСимв32˛представленныйТипомЦел32˛вЮ8(kod)
		аесли (режим >= 2) и ((kod < кодСимв8(" ")) или (kod >= UCS32.DiapazonJunikodaSKirillicejj)) то
			если kod = UCS32.KodTAB то
				пСтроку8("\t")
			аесли (kod = UCS32.KodLF) 
				или (kod = UCS32.LeftDoubleQuotationMark)
				или (kod = UCS32.DoubleLowDash9QuotationMark) то
				пСимв32˛представленныйТипомЦел32˛вЮ8(kod)
			иначе
				пСимв8("\");
				п16ричное(kod, 0);
				пСимв8(";") всё
		иначе
			пСимв32˛представленныйТипомЦел32˛вЮ8(kod) всё;
		увел(i) кц кон пСтроку32;
	
проц пСтроку32БуквальльноС0*(конст з : UCS32.StringJQ);
нач пСтроку32(з, 1) кон пСтроку32БуквальльноС0;

проц пСтроку32_вЮ8*(конст з : UCS32.StringJQ);
нач пСтроку32(з, 0) кон пСтроку32_вЮ8;


(* КОНЕЦ Этот кусок есть в Streams, KernelLog и Trace *)


(** Skip to the next line on trace output. *)

проц пВК_ПС*;
нач пСимв8 (0DX); пСимв8 (0AX);
кон пВК_ПС;

(** Write a character. *)

проц пСимв8*(c: симв8);
тип Str = массив 1 из симв8;
нач
	пСтроку8(НИЗКОУР.подмениТипЗначения(Str, c))
кон пСимв8;

(** Write "x" as a decimal number. "w" is the field width. *)

проц пЦел64*(x: цел64; w: целМЗ);
перем i: размерМЗ; x0: цел64; a: массив 21 из симв8;
нач
	если x < 0 то
		если x = матМинимум(цел64) то
			умень(w, 20);
			нцПока w > 0 делай пСимв8(" "); умень(w) кц;
			пСтроку8 ("-9223372036854775808");
			возврат
		иначе
			умень(w); x0 := -x
		всё
	иначе
		x0 := x
	всё;
	i := 0;
	нцДо
		a[i] := симв8ИзКода(x0 остОтДеленияНа 10 + 30H); x0 := x0 DIV 10; увел(i)
	кцПри x0 = 0;
	нцПока w > i делай пСимв8(" "); умень(w) кц;
	если x < 0 то пСимв8("-") всё;
	нцДо умень(i); пСимв8(a[i]) кцПри i = 0
кон пЦел64;

проц пБулево*(x : булево);
нач
	если x то пСтроку8("TRUE") иначе пСтроку8("FALSE") всё
кон пБулево;

(** Write "x" as a decimal number with a power-of-two multiplier (K, M or G), followed by "suffix". "w" is the field width, excluding "suffix". *)

проц пОбъёмПамятиСРазмерностью*(x: цел64; w: целМЗ; конст suffix: массив из симв8);
конст K = 1024; M = K*K; G = K*M;
перем mult: симв8;
нач
	если x остОтДеленияНа K # 0 то
		пЦел64(x, w)
	иначе
		если x остОтДеленияНа M # 0 то mult := "K"; x := x DIV K
		аесли x остОтДеленияНа G # 0 то mult := "M"; x := x DIV M
		иначе mult := "G"; x := x DIV G
		всё;
		пЦел64(x, w-1); пСимв8(mult)
	всё;
	пСтроку8(suffix)
кон пОбъёмПамятиСРазмерностью;
(*
(** Write "x" as a hexadecimal number. The absolute value of "w" is the field width. If "w" is negative, two hex digits are printed (x MOD 100H), otherwise 8 digits are printed. *)

PROCEDURE Hex*(x: SIGNED64; w: INTEGER);
VAR i, j: SIZE; buf: ARRAY 10 OF CHAR;
BEGIN
	IF w >= 0 THEN j := 8 ELSE j := 2; w := -w END;
	FOR i := j+1 TO w DO Char(" ") END;
	FOR i := j-1 TO 0 BY -1 DO
		buf[i] := CHR(x MOD 10H + 48);
		IF buf[i] > "9" THEN
			buf[i] := CHR(ORD(buf[i]) - 48 + 65 - 10)
		END;
		x := x DIV 10H
	END;
	buf[j] := 0X;
	String(buf)
END Hex;
*)

(**	Write an integer in hexadecimal right-justified in a field of at least ABS(w) characters.
	If w < 0, the w least significant hex digits of x are written (potentially including leading zeros)
*)
проц п16ричное*( x: цел64;  w: целМЗ );
перем filler: симв8; i, maxi, y: целМЗ;  buf: массив 20 из симв8;
нач
	если w < 0 то  filler := '0';  w := -w;  maxi := w  иначе  filler := ' ';  maxi := 16  всё;
	i := 0;
	нцДо
		y := устарПреобразуйКБолееУзкомуЦел( x остОтДеленияНа 10H );
		если y < 10 то  buf[i] := симв8ИзКода( y + кодСимв8('0') )  иначе  buf[i] := симв8ИзКода( y - 10 + кодСимв8('A') )  всё;
		x := x DIV 10H;
		увел( i );
	кцПри (x = 0) или (i = maxi);
	нцПока w > i делай  пСимв8( filler );  умень( w )  кц;
	нцДо  умень( i ); пСимв8( buf[i] )  кцПри i = 0
кон п16ричное;

(** Write "x" as a hexadecimal address. *)

проц пАдресВПамяти* (x: адресВПамяти);
нач
	п16ричное(x, -размер16_от(адресВПамяти)*2)
кон пАдресВПамяти;

(** Write "x" as a size *)

проц пРазмерМЗ* (x: размерМЗ);
нач {безКооперации, безОбычныхДинПроверок}
	пЦел64(x,0);
кон пРазмерМЗ;

(** Write "x" as a hexadecimal number.  "w" is the field width.  Always prints 16 digits. *)

проц п16ричноеДубликат*(x: цел64; w: целМЗ);
нач
	п16ричное( x, w )
кон п16ричноеДубликат;

(** Write a block of memory in hex. *)

проц пБлокПамяти16рично*(вхАдр: адресВПамяти; вхРазм˛байт: размерМЗ);
перем i, j: адресВПамяти; ch: симв8;
нач
	пСимв8(0EX);	(* "fixed font" *)
	вхРазм˛байт := вхАдр+вхРазм˛байт-1;
	нцДля i := вхАдр до вхРазм˛байт шаг 16 делай
		пАдресВПамяти (i); пСимв8 (' ');
		нцДля j := i до i+15 делай
			если j <= вхРазм˛байт то
				НИЗКОУР.прочтиОбъектПоАдресу(j, ch);
				п16ричное(кодСимв8(ch), -2)
			иначе
				пСтроку8("   ")
			всё
		кц;
		пСтроку8(" ");
		нцДля j := i до i+15 делай
			если j <= вхРазм˛байт то
				НИЗКОУР.прочтиОбъектПоАдресу(j, ch);
				если (ch < " ") или (ch >= симв8ИзКода(127)) то ch := "." всё;
				пСимв8(ch)
			всё
		кц;
		пВК_ПС
	кц;
	пСимв8(0FX)	(* "proportional font" *)
кон пБлокПамяти16рично;

(** Write a buffer in hex. *)

проц пСрезБуфера16рично*(конст буф: массив из симв8; начСмещение, сколькоБайтЗаписать: размерМЗ);
нач
	пБлокПамяти16рично(адресОт(буф[начСмещение]), сколькоБайтЗаписать)
кон пСрезБуфера16рично;

(** Write bits (ofs..ofs+n-1) of x in binary. *)

проц пМнвоНаБитахМЗКакБиты*(x: мнвоНаБитахМЗ; ofs, n: целМЗ);
нач
	нцДо
		умень(n);
		если (ofs+n) в x то пСимв8("1") иначе пСимв8("0") всё
	кцПри n = 0
кон пМнвоНаБитахМЗКакБиты;

(** write a set as set *)
проц пМнвоНаБитахМЗКакЧисла*(x: мнвоНаБитахМЗ);
перем first: булево; i: целМЗ;
нач
	first := истина;
	пСимв8("{");
	нцДля i := 0 до матМаксимум(мнвоНаБитахМЗ) делай
		если i в x то
			если ~first то пСимв8(",") иначе first := ложь всё;
			пЦел64(i,1);
		всё;
	кц;
	пСимв8("}");
кон пМнвоНаБитахМЗКакЧисла;

(** Enter mutually exclusive region for writing, using a fine-grained lock.  This region should be kept as short as possible, and only procedures from KernelLog should be called inside it. *)

проц ЗахватВЕдиноличноеПользование*;
нач
	Machine.Acquire(Machine.KernelLog);
	пСтроку8("{P cpuid= "); пЦел64(Machine.ID(), 0); пСтроку8 (", pid= "); пЦел64 (Objects.GetProcessID (), 0); пСимв8 (' ');
кон ЗахватВЕдиноличноеПользование;

(** Exit mutually exclusive region for writing. *)

проц ОсвобождениеИзЕдиноличногоПользования*;
нач
	пСимв8("}"); пВК_ПС;
	Machine.Release(Machine.KernelLog)
кон ОсвобождениеИзЕдиноличногоПользования;

(* Switch to a new tracing buffer, copying the existing data. *)

проц SwitchToBuffer(adr: адресВПамяти; size: размерМЗ);
перем tail: адресВПамяти; c: симв8;
нач
	tail := 0; утв(size > 0);
	нцПока (traceHead # traceTail) и (tail+1 # size) делай	(* source not empty, destination not full *)
		НИЗКОУР.прочтиОбъектПоАдресу (traceBufAdr + traceHead, c);
		НИЗКОУР.запишиОбъектПоАдресу (adr + tail, c);
		traceHead := (traceHead+1) остОтДеленияНа traceBufSize;
		увел(tail)
	кц;
	traceBufAdr := adr; traceBufSize := size;
	traceHead := 0; traceTail := tail
кон SwitchToBuffer;

(** Assign a new trace buffer.  Used by a display process. *)

проц ВпредьПишиВАльтернативныйБуферТрассировки*(вхАдресАльтернативногоБуфера: адресВПамяти; вхРазмерАльтернативногоБуфера: размерМЗ): булево;
перем ok: булево;
нач
	Machine.Acquire(Machine.TraceOutput);
	если traceBufAdr = адресОт(traceBufDef[0]) то
		SwitchToBuffer(вхАдресАльтернативногоБуфера, вхРазмерАльтернативногоБуфера); ok := истина
	иначе
		ok := ложь
	всё;
	Machine.Release(Machine.TraceOutput);
	возврат ok
кон ВпредьПишиВАльтернативныйБуферТрассировки;

(** Return output buffer contents.  Used by a display process. *)

проц ДайСодержимоеБуфераТрассировки*(перем val: массив из симв8);
перем i, m: размерМЗ;
нач
	i := 0; m := длинаМассива(val)-1;
	Machine.Acquire(Machine.TraceOutput);
	нцПока (i < m) и (traceHead # traceTail) делай
		val[i] := симв8ИзКода(НИЗКОУР.прочти8битПоАдресу(traceBufAdr + traceHead));
		traceHead := (traceHead+1) остОтДеленияНа traceBufSize;
		увел(i)
	кц;
	Machine.Release(Machine.TraceOutput);
	val[i] := 0X
кон ДайСодержимоеБуфераТрассировки;

(** Close the trace buffer and revert to the default.  Used by a display process. *)

проц ВпредьПишиВОсновнойБуферТрассировки*;
нач
	Machine.Acquire(Machine.TraceOutput);
	если traceBufAdr # адресОт(traceBufDef[0]) то
		SwitchToBuffer(адресОт(traceBufDef[0]), длинаМассива(traceBufDef))
	всё;
	Machine.Release(Machine.TraceOutput)
кон ВпредьПишиВОсновнойБуферТрассировки;

нач
	traceBufAdr := адресОт(traceBufDef[0]);
	traceBufSize := длинаМассива(traceBufDef);
	traceHead := 0; traceTail := 0;
кон ЛогЯдра.

(**
Notes

This module provides low-level output facilities for Aos.  It is similar to the Out module of Oberon, but it can be called from anywhere, even from active object bodies and interrupt handlers.  It can write to the text display (when not using a graphics mode), a serial port, a memory buffer, or all of the above.  This is controlled by the TraceMode and related config strings (see Aos.Par).

Typically, a memory buffer is used.  The buffer is installed by the LogWindow.Open, or with the System.StartLog command when using Oberon.  The latter is recommended, as it also interprets traps specially and opens a new viewer for them.  The displaying of the buffer is done off-line by the LogWindow or Oberon threads, thereby allowing the procedures here to be called from anywhere.

Control characters:
0X	end of string (can not be printed)
1X	start of trap (if System.StartLog active then trap viewer will be opened and output redirected)
2X	end of trap (if System.StartLog active then it will revert output to the kernel log text)
3X	signal log overflow
9X	TAB (or single space)
0DX	CR (or NL and LF ignored)
0AX	LF (ignored if CR is NL)
0EX	set fixed-width font
0FX	set proportial font (default)
*)

(*
TraceMode:
0	1	Screen
2	4	V24
*)

(*
03.03.1998	pjm	First version
16.06.2000	pjm	Cleaned up
29.11.2000	pjm	buffering
12.06.2001	pjm	moved Flags to Traps, moved SegDesc and TSS to AosFragments
*)
