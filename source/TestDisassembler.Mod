MODULE TestDisassembler; (** AUTHOR ""; PURPOSE ""; *)

IMPORT Disassembler:=FoxDisassembler, Commands, Files;

	PROCEDURE Disassemble*(context: Commands.Context);
	TYPE
		Disasm = OBJECT (Disassembler.Disassembler)

			PROCEDURE DisassembleInstruction(bitSet: BitSets.BitSet; VAR adr: SIGNED32; maxInstructionSize: SIGNED32; w:Streams.Writer);
			VAR instruction: Instruction; value: SIGNED32; mnemonic: SIGNED32;
			BEGIN
				(* maxInstructionSize can be ignored here *)
				value := bitSet.GetBits(adr*8,32);
				IF Decode(value, instruction) THEN
					DumpInstruction(w, instruction);
					mnemonic := instructionFormats[instruction.format].mnemonic;
					IF (mnemonic = opBL) OR (mnemonic = opB) THEN
						WriteReference(instruction.operands[0].immediate+adr+8, TRUE (* points to code section *), w);

					ELSIF (mnemonic = opLDR) OR (mnemonic = opSTR) THEN
						(* LDR? ..., [PC, #...] or STR? ..., [PC, #...] *)
						ASSERT(instruction.operands[1].mode = modeMemory);
						IF (instruction.operands[1].register = PC) & (instruction.operands[1].offsetRegister = None) THEN
							value := instruction.operands[1].offsetImmediate + adr + 8;
							WriteReference(value, TRUE, w);
							IF value * 8 + 32 < bitSet.GetSize() THEN
								WriteReference(bitSet.GetBits(value * 8, 32) - codeDisplacement, TRUE, w); (* note that data references cannot be resolved like this *)
							END
						END
					END;
				ELSE
					w.String("*** COULD NOT DECODE ***");
				END;
				INC(adr,4);
			END DisassembleInstruction;

		END Disasm;

	VAR disassembler: Disasm; codeFileName, dataFileName, logFileName: Files.FileName; codeFile, logFile: Files.File;code: BitSets.BitSet; options: Options.Options;
	BEGIN
		IF context.arg.GetString(codeFileName) THEN
			codeFile := Files.Old(codeFileName);
			IF codeFile = NIL THEN context.out.String("file not found "); context.out.String(codeFileName); RETURN END;
			NEW(disassembler, context.out);
			code := ReadCode(codeFile);
			IF options.GetString("logFile",logFileName) THEN
				logFile := Files.Old(logFileName);
			ELSE
				logFile := disassembler.GetLogFile(codeFileName)
			END;
			disassembler.Disassemble(code, code, 8,8 , logFile);
		END;
	END Disassemble;


END TestDisassembler.
