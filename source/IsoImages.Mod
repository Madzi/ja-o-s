модуль IsoImages; (** AUTHOR "Roger Keller"; PURPOSE "Create bootable ISO image"; *)

использует НИЗКОУР, Commands, Потоки, Files, Dates, Strings;

конст
	Ok* = 0;
	FileNotFound* = 1;			(* Input disk image file not found *)
	CouldNotCreateFile* = 2;	(* Creation of ISO image file failed *)

	MaxPathLen = 256;
	ISO9660Id = "CD001";
	CDSectorSize = 2048; (* size of a sector of data on a CD *)
	NumSystemSectors = 16; (* number of sectors at begin of CD which are unused *)

	ElToritoSysId = "EL TORITO SPECIFICATION"; (* boot system id for the el torito standard *)
	Platform80x86 = 0X;
	PlatformPowerPC = 0X;
	PlatformMac = 0X;

	Bootable = 88X;
	NotBootable = 00X;

	EmulationNone = 0X;
	Emulation12Floppy = 1X;
	Emulation144Floppy = 2X;
	Emulation288Floppy = 3X;
	EmulationHDD = 4X;

	BBVolumeId = "BLUEBOTTLE";
	BBPublisher = "ETH_ZURICH";

тип
	BootCatalogEntry = массив 32 из симв8;
	BCValidationEntry = запись
		HeaderId: симв8;
		PlatformId: симв8;
		Reserved: цел16;
		IdString: массив 24 из симв8;
		Checksum: цел16;
		KeyBytes: массив 2 из симв8;
	кон;
	BCInitialDefaultEntry = запись
		BootIndicator: симв8;
		BootMediaType: симв8;
		LoadSegment: цел16;
		SystemType: симв8;
		Unused1: симв8;
		SectorCount: цел16;
		LoadRBA: цел32;
		Unused2: массив 20 из симв8;
	кон;

проц WriteImage(w: Потоки.Писарь; r: Потоки.Чтец; imageSize: цел32);
перем read, padLen: размерМЗ; buf: массив CDSectorSize из симв8;
нач
	padLen := CDSectorSize - (imageSize остОтДеленияНа CDSectorSize);

	r.чБайты(buf, 0, CDSectorSize, read);
	нцПока (read > 0) делай
		w.пБайты(buf, 0, read);
		r.чБайты(buf, 0, CDSectorSize, read);
	кц;
	WriteByteRep(w, 0X, padLen);
кон WriteImage;

проц WriteElToritoDescriptor(w: Потоки.Писарь);
нач
	w.пСимв8(0X); (* boot record indicator *)
	w.пСтроку8(ISO9660Id); (* standard identifier for ISO 9660 *)
	w.пСимв8(1X); (* descriptor version; 1 for el torito 1.0 specification from january 25, 1995 *)
	WriteStringWithPadding(w, ElToritoSysId, 0X, 32); (* boot system id *)
	WriteByteRep(w, 0X, 32); (* unused *)
	w.пЦел32_мз(NumSystemSectors + 1 + 1 + 1); (* absolute pointer to sector (LBA) of boot catalog *)
	WriteByteRep(w, 0X, 1973); (* unused *)
кон WriteElToritoDescriptor;

проц WriteBootCatalog(w: Потоки.Писарь);
перем entry: BCValidationEntry; entry2: BCInitialDefaultEntry; len: цел32;
нач
	len := 0;

	(* validation entry *)
	entry.HeaderId := 1X; (* header id *)
	entry.PlatformId := Platform80x86; (* platform id *)
	entry.Reserved := 0; (* reserved *)
	entry.IdString := BBVolumeId;
	entry.Checksum := 0;(* init checksum to zero *)
	entry.KeyBytes[0] := 55X; entry.KeyBytes[1] := 0AAX; (* key bytes *)
	entry.Checksum := CalcChecksum16(НИЗКОУР.подмениТипЗначения(BootCatalogEntry, entry)); (* update the checksum *)
	w.пБайты(НИЗКОУР.подмениТипЗначения(BootCatalogEntry, entry), 0, 32);
	увел(len, 32);

	(* initial / default entry *)
	entry2.BootIndicator := Bootable;
	entry2.BootMediaType := Emulation144Floppy;
	entry2.LoadSegment := 0; (* use default load segment which is 7C0H *)
	entry2.SystemType := 0X;
	entry2.Unused1 := 0X;
	entry2.SectorCount := 1;
	entry2.LoadRBA := NumSystemSectors + 7;
	w.пБайты(НИЗКОУР.подмениТипЗначения(BootCatalogEntry, entry2), 0, 32);
	увел(len, 32);

	(* pad rest of sector with zeros *)
	WriteByteRep(w, 0X, CDSectorSize - len);
кон WriteBootCatalog;

проц WriteIsoFSData(w: Потоки.Писарь);
перем now: Dates.DateTime;
нач
	now := Dates.Now();

	w.пСимв8(22X); (* length of directory record *)
	w.пСимв8(0X); (* extended attribute record length *)
	w.пЦел32_мз(NumSystemSectors + 4); w.пЦел32_сп(NumSystemSectors + 4); (* location of extent *)
	w.пЦел32_мз(CDSectorSize); w.пЦел32_сп(CDSectorSize); (* data length *)
	WriteByteRep(w, 0X, 7);
	(*w.RawSInt(SHORT(SHORT(now.Year - 1900)));
	w.RawSInt(SHORT(SHORT(now.Month + 1)));
	w.RawSInt(SHORT(SHORT(now.Day)));
	w.RawSInt(SHORT(SHORT(now.Hour)));
	w.RawSInt(SHORT(SHORT(now.Minute)));
	w.RawSInt(SHORT(SHORT(now.Second)));
	w.Char(0X);*)
	w.пСимв8(2X); (* file flags: this is a directory *)
	w.пСимв8(0X); (* file unit size *)
	w.пСимв8(0X); (* interleave gap size *)
	w.пЦел16_мз(1); w.п2МладшихБайта_сп(1); (* volume sequence number *)
	w.пСимв8(1X); (* length of file identifier *)
	w.пСимв8(0X); (* file id: 0X indicates first entry of directory *)

	w.пСимв8(22X); (* length of directory record *)
	w.пСимв8(0X); (* extended attribute record length *)
	w.пЦел32_мз(NumSystemSectors + 4); w.пЦел32_сп(NumSystemSectors + 4); (* location of extent *)
	w.пЦел32_мз(CDSectorSize); w.пЦел32_сп(CDSectorSize); (* data length *)
	WriteByteRep(w, 0X, 7);
	w.пСимв8(2X); (* file flags: this is a directory *)
	w.пСимв8(0X); (* file unit size *)
	w.пСимв8(0X); (* interleave gap size *)
	w.пЦел16_мз(1); w.п2МладшихБайта_сп(1); (* volume sequence number *)
	w.пСимв8(1X); (* length of file identifier *)
	w.пСимв8(1X); (* file id: 0X indicates first entry of directory *)

	WriteByteRep(w, 0X, CDSectorSize - (2 * 22H));

	WriteTypeLPathTable(w);
	WriteTypeMPathTable(w);
кон WriteIsoFSData;

проц WriteTypeLPathTable(w: Потоки.Писарь);
нач
	w.пСимв8(1X); (* length of directory identifier *)
	w.пСимв8(0X); (* extended attribute record length *)
	w.пЦел32_мз(NumSystemSectors + 4); (* location of extent *)
	w.пЦел16_мз(1); (* parent directory number *)
	w.пСимв8(0X); (* directory identifier *)

	WriteByteRep(w, 0X, CDSectorSize - 9);
кон WriteTypeLPathTable;

проц WriteTypeMPathTable(w: Потоки.Писарь);
нач
	w.пСимв8(1X); (* length of directory identifier *)
	w.пСимв8(0X); (* extended attribute record length *)
	w.пЦел32_сп(NumSystemSectors + 4); (* location of extent *)
	w.п2МладшихБайта_сп(1); (* parent directory number *)
	w.пСимв8(0X); (* directory identifier *)

	WriteByteRep(w, 0X, CDSectorSize - 9);
кон WriteTypeMPathTable;

проц WritePrimaryVolumeDescriptor(w: Потоки.Писарь; isoImageSectorCount: цел32);
перем now: Dates.DateTime; dtBuf: массив 20 из симв8;
нач
	now := Dates.Now();
	Strings.FormatDateTime("yyyymmddhhnnss00", now, dtBuf);

	w.пСимв8(1X); (* descriptor type *)
	w.пСтроку8(ISO9660Id); (* standard identifier *)
	w.пСимв8(1X); (* volume descriptor version *)
	w.пСимв8(0X); (* unused *)
	WriteByteRep(w, ' ', 32); (* system identifier *)
	WriteStringWithPadding(w, BBVolumeId, ' ', 32); (* volume identifier *)
	WriteByteRep(w, 0X, 8); (* unused *)
	WriteBothByteOrder32(w, isoImageSectorCount); (* volume space size *)
	WriteByteRep(w, 0X, 32); (* unused *)
	WriteBothByteOrder16(w, 1); (* volume set size *)
	WriteBothByteOrder16(w, 1); (* volume sequence number *)
	WriteBothByteOrder16(w, CDSectorSize); (* logical block size *)
	WriteBothByteOrder32(w, 10); (* path table size *)
	w.пЦел32_мз(NumSystemSectors + 1 + 1 + 1 + 1 + 1); (* location (LBA) of occurrence of type L path table *)
	w.пЦел32_мз(0); (* location (LBA) of optional occurrence of type L path table *)
	w.пЦел32_сп(NumSystemSectors + 1 + 1 + 1 + 1 + 1 + 1); (* location (LBA) of occurrence of type M path table *)
	w.пЦел32_мз(0); (* location (LBA) of optional occurrence of type M path table *)
	WriteDirectoryRecord(w); (* directory record for root directory *)
	WriteByteRep(w, ' ', 128); (* volume set id *)
	WriteStringWithPadding(w, BBPublisher, ' ', 128);
	WriteByteRep(w, ' ', 128 + 128); (* data preparer id, application id *)
	WriteByteRep(w, ' ', 37 + 37 + 37); (* copyright file id, abstract file id, bibliography file id *)
	w.пСтроку8(dtBuf); w.пСимв8(0X); (* volume creation date / time; time offset is set to zero *)
	w.пСтроку8(dtBuf); w.пСимв8(0X); (* volume modification date / time; time offset is set to zero *)
	dtBuf := "0000000000000000";
	w.пСтроку8(dtBuf); w.пСимв8(0X); (* volume expiration date / time; time offset is set to zero *)
	w.пСтроку8(dtBuf); w.пСимв8(0X); (* volume effective date / time; time offset is set to zero *)
	w.пСимв8(1X); (* file structure version: 1 stands for ISO 9660 *)
	w.пСимв8(0X); (* reserved *)
	WriteByteRep(w, 0X, 512 + 653); (* application use (512 bytes) and reserved (653 bytes) *)
кон WritePrimaryVolumeDescriptor;

проц WriteSetTerminatorDescriptor(w: Потоки.Писарь);
нач
	w.пСимв8(0FFX); (* descriptor type: set terminator *)
	w.пСтроку8(ISO9660Id); (* standard identifier *)
	w.пСимв8(1X); (* volume descriptor version *)
	WriteByteRep(w, 0X, 2041); (* reserved *)
кон WriteSetTerminatorDescriptor;

проц WriteDirectoryRecord(w: Потоки.Писарь);
перем now: Dates.DateTime;
нач
	now := Dates.Now();

	w.пЦел8_мз(22H); (* length of this directory record *)
	w.пСимв8(0X); (* extended attribute record length *)
	WriteBothByteOrder32(w, NumSystemSectors + 1 + 1 + 1 + 1); (* location (LBA) of extent *)
	WriteBothByteOrder32(w, CDSectorSize); (* data length *)

	(* recording date and time, one byte per field; year, month, day, hour, minute, second, time zone offset *)
	w.пЦел8_мз(устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(now.year - 1900)));
	w.пЦел8_мз(устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(now.month + 1)));
	w.пЦел8_мз(устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(now.day)));
	w.пЦел8_мз(устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(now.hour)));
	w.пЦел8_мз(устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(now.minute)));
	w.пЦел8_мз(устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(now.second)));
	w.пСимв8(0X); (* use zero time offset *)
	w.пСимв8(2X); (* file flags: this is a directory *)
	w.пСимв8(0X); (* file unit size *)
	w.пСимв8(0X); (* interleave gap size *)
	WriteBothByteOrder16(w, 1); (* volume sequence number *)
	w.пСимв8(1X); (* length of file identifier *)
	w.пСимв8(0X); (* self indicator *)
кон WriteDirectoryRecord;

проц CalcIsoImageSectorCount(inputImageSize: цел32): цел32;
перем imageSectors: цел32;
нач
	imageSectors := inputImageSize DIV CDSectorSize;
	если (inputImageSize остОтДеленияНа CDSectorSize # 0) то
		увел(imageSectors);
	всё;

	возврат NumSystemSectors +
		1 + (* primary volume descriptor *)
		1 + (* el torito boot volume descriptor *)
		1 + (* volume descriptor set terminator *)
		1 + (* boot catalog *)
		3 + (* root directory descriptor, type L path table, type M path table *)
		imageSectors;
кон CalcIsoImageSectorCount;

проц WriteBothByteOrder32(w: Потоки.Писарь; x: цел32);
нач
	w.пЦел32_мз(x);
	w.пЦел32_сп(x);
кон WriteBothByteOrder32;

проц WriteBothByteOrder16(w: Потоки.Писарь; x:цел16);
нач
	w.пЦел16_мз(x);
	w.п2МладшихБайта_сп(x);
кон WriteBothByteOrder16;

проц WriteByteRep(w: Потоки.Писарь; b: симв8; n: размерМЗ);
перем i: размерМЗ;
нач
	нцДля i := 1 до n делай
		w.пСимв8(b);
	кц;
кон WriteByteRep;

проц WriteStringWithPadding(w: Потоки.Писарь; конст str: массив из симв8; padChar: симв8; len: цел32);
перем strLen: размерМЗ;
нач
	strLen := длинаМассива(str) - 1; (* we don't write the terminating 0X *)
	w.пСтроку8(str);
	WriteByteRep(w, padChar, len - strLen);
кон WriteStringWithPadding;

проц WriteEmptySectors(w: Потоки.Писарь; n: цел32);
перем i, s, nLongs: цел32;
нач
	nLongs := CDSectorSize DIV 4; (* number of 32bits per sector *)
	нцДля s := 1 до n делай
		нцДля i := 1 до nLongs делай
			w.пЦел32_мз(0);
		кц;
	кц;
кон WriteEmptySectors;

(*
PROCEDURE WriteStringWithPaddingToBuffer(buf: ARRAY OF CHAR; offset: SIGNED32; str: ARRAY OF CHAR;
	padChar: CHAR; len: SIZE);
VAR i, strLen: SIZE;
BEGIN
	strLen := LEN(str) - 1; (* we don't write the terminating 0X *)
	SYSTEM.MOVE(ADDRESSOF(str), ADDRESSOF(buf) + offset, strLen);
	FOR i := 0 TO len - strLen - 1 DO
		buf[offset + i] := 0X;
	END;
END WriteStringWithPaddingToBuffer;
*)

проц CalcChecksum16(конст buf: массив из симв8): цел16;
перем checksum: цел32; i, numWords: размерМЗ;
нач
	checksum := 0;
	numWords := длинаМассива(buf) DIV 2;
	нцДля i := 0 до numWords - 1 делай
		checksum := (checksum + кодСимв8(buf[i * 2])) остОтДеленияНа 10000H;
	кц;
	возврат устарПреобразуйКБолееУзкомуЦел(10000H - checksum);
кон CalcChecksum16;

проц MakeImage*(конст input, output: массив из симв8; перем imageSize , res : цел32);
перем fOut, fIn: Files.File; out: Files.Writer; in: Files.Reader; numSectors: цел32;
нач
	res := Ok;

	fIn := Files.Old(input);
	если (fIn = НУЛЬ) то
		res := FileNotFound;
		возврат;
	всё;

	fOut := Files.New(output);
	если (fOut = НУЛЬ) то
		res := CouldNotCreateFile;
		возврат;
	всё;

	numSectors := CalcIsoImageSectorCount(fIn.Length()(цел32));

	Files.Register(fOut);
	Files.OpenWriter(out, fOut, 0);

	WriteEmptySectors(out, NumSystemSectors);
	WritePrimaryVolumeDescriptor(out, numSectors);
	WriteElToritoDescriptor(out);
	WriteSetTerminatorDescriptor(out);
	WriteBootCatalog(out);
	WriteIsoFSData(out);
	Files.OpenReader(in, fIn, 0); (* open a reader on the raw input image *)
	WriteImage(out, in, fIn.Length()(цел32)); (* write the raw image *)

	out.ПротолкниБуферВПоток;
	fOut.Update;
	imageSize := fOut.Length()(цел32);
кон MakeImage;

проц Make*(context : Commands.Context);
перем
	imageSource, isoDest: массив MaxPathLen из симв8;
	imageSize, res : цел32;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(isoDest);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(imageSource);

	context.out.пСтроку8("Making ISO-9660 Bootable Image"); context.out.пВК_ПС;

	context.out.пСтроку8("Input image is ");
	context.out.пСтроку8(imageSource); context.out.пВК_ПС;
	context.out.пСтроку8("Writing Bootable ISO Image to ");
	context.out.пСтроку8(isoDest); context.out.пСтроку8(" ... ");

	MakeImage(imageSource, isoDest, imageSize, res);

	если (res = Ok) то
		context.out.пСтроку8("done."); context.out.пВК_ПС;
		context.out.пСтроку8("Bootable ISO Image successfully written (Size: ");
		context.out.пЦел64(imageSize DIV 1024, 0); context.out.пСтроку8(" KB)"); context.out.пВК_ПС;
	аесли (res = FileNotFound) то
		context.error.пСтроку8("Disk image file "); context.error.пСтроку8(imageSource);
		context.error.пСтроку8(" not found."); context.error.пВК_ПС;
	аесли (res = CouldNotCreateFile) то
		context.error.пСтроку8("Could not create image file "); context.error.пСтроку8(isoDest);
		context.error.пСтроку8("."); context.error.пВК_ПС;
	иначе
		context.error.пСтроку8("Error, res: "); context.error.пЦел64(res, 0); context.error.пВК_ПС;
	всё;
кон Make;

кон IsoImages.


(*
IsoImages.Make AosCDPrivate.iso AosCDPrivate.Dsk ~
System.Free IsoImages ~
*)
