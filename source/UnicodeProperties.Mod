модуль UnicodeProperties; (** AUTHOR "gubsermi"; PURPOSE "Reading the Unicode.txt file and interpreting the properties"; *)

использует
	Texts, Codecs, Files, Потоки, ЛогЯдра, Strings;

конст
	NUL*	= 00H;
	EOT*	= 04H;
	LF*		= 0AH;
	CR*		= 0DH;
	SP*		= 20H;

	CacheDebugging = ложь;


перем
	error- : булево;



тип
	(* Caches a result from the property files. Can be used for a string or a character value, but not both! *)
	CacheElement = окласс
	перем
		next : CacheElement;
		key : Texts.Char32;
		sValue : массив 256 из симв8;
		cValue : Texts.Char32;

		(* Initializes a CacheElement with a key and a value *)
		проц &Init*(key : Texts.Char32; конст sValue : массив из симв8; cValue : Texts.Char32);
		нач
			сам.key := key;
			если sValue[0] # симв8ИзКода(0H) то
				Strings.Copy(sValue,0,длинаМассива(sValue),сам.sValue);
				сам.cValue := -1;
			иначе
				сам.sValue[0] := симв8ИзКода(0H);
				сам.cValue := cValue;
			всё;
		кон Init;

	кон CacheElement;


	(* The Property Cache uses a Hashmap of a specific size to cache either string or character properties. *)
	CharacterPropertyCache = окласс
	перем
		internalCache : укль на массив из CacheElement;
		cacheSize : размерМЗ;

		(* Initializes the hashmap with a specific size *)
		проц &Init*(size : размерМЗ);
		нач
			cacheSize := size;
			нов(internalCache,cacheSize);
		кон Init;

		(* Searches the cache for a specific key and returns the corresponding string entry *)
		проц SLookup(char : Texts.Char32; перем res : массив из симв8);
		перем
			bucket : размерМЗ;
			currentElement : CacheElement;
		нач
			(* get the bucket where the element resides if available *)
			bucket := char остОтДеленияНа cacheSize;
			currentElement := internalCache[bucket];

			(* search the linked list for the entry *)
			нцПока currentElement # НУЛЬ делай
				если currentElement.key = char то
					если CacheDebugging то
						ЛогЯдра.пСтроку8("found: "); ЛогЯдра.п16ричное(currentElement.key,4);
						ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пСтроку8(currentElement.sValue);
						ЛогЯдра.пСтроку8(")"); ЛогЯдра.пВК_ПС;
					всё;
					Strings.Copy(currentElement.sValue,0,длинаМассива(res),res);
					возврат;
				иначе
					currentElement := currentElement.next;
				всё;
			кц;

			(* clear the result if nothing was found. *)
			res := "";
		кон SLookup;

		(* Searches the cache for a specific key and returns the corresponding character entry *)
		проц CLookup(char : Texts.Char32) : Texts.Char32;
		перем
			bucket : размерМЗ;
			currentElement : CacheElement;
		нач
			(* get the bucket where the element resides if available *)
			bucket := char остОтДеленияНа cacheSize;
			currentElement := internalCache[bucket];

			(* search the linked list for the entry *)
			нцПока currentElement # НУЛЬ делай
				если currentElement.key = char то
					если CacheDebugging то
						ЛогЯдра.пСтроку8("found: "); ЛогЯдра.п16ричное(currentElement.key,4);
						ЛогЯдра.пСтроку8(" ("); ЛогЯдра.п16ричное(currentElement.cValue,4);
						ЛогЯдра.пСтроку8(")"); ЛогЯдра.пВК_ПС;
					всё;
					возврат currentElement.cValue;
				иначе
					currentElement := currentElement.next;
				всё;
			кц;

			(* return a 'fault code' if nothing was found *)
			возврат -1
		кон CLookup;


		(* Inserts a new string entry for a given key. *)
		проц SInsert(char : Texts.Char32; конст value : массив из симв8);
		перем
			newElement : CacheElement;
			bucket : размерМЗ;
		нач
			нов(newElement,char,value,-1);

			(* insert the new entry at the first position of the correct bucket *)
			bucket := char остОтДеленияНа cacheSize;
			newElement.next := internalCache[bucket];
			internalCache[bucket] := newElement;

			если CacheDebugging то
				ЛогЯдра.пСтроку8("inserted: "); ЛогЯдра.п16ричное(char,4);
				ЛогЯдра.пСтроку8(" (");
				ЛогЯдра.пСтроку8(value);
				ЛогЯдра.пСтроку8(")"); ЛогЯдра.пВК_ПС;
			всё;
		кон SInsert;

		(* Inserts a new character entry for a given key. *)
		проц CInsert(char : Texts.Char32; value : Texts.Char32);
		перем
			newElement: CacheElement;
			bucket : размерМЗ;
			dummy : массив 1 из симв8;
		нач
			dummy[0] := симв8ИзКода(0H);
			нов(newElement,char,dummy,value);

			(* insert the new entry at the first position of the correct bucket *)
			bucket := char остОтДеленияНа cacheSize;
			newElement.next := internalCache[bucket];
			internalCache[bucket] := newElement;

			если CacheDebugging то
				ЛогЯдра.пСтроку8("inserted: "); ЛогЯдра.п16ричное(char,4);
				ЛогЯдра.пСтроку8(" ("); ЛогЯдра.п16ричное(value,4);
				ЛогЯдра.пСтроку8(")"); ЛогЯдра.пВК_ПС;
			всё;
		кон CInsert;

		(* Prints the whole cache to the console *)
		проц Print;
		перем
			i : размерМЗ;
			thisElement : CacheElement;
		нач
			нцДля i := 0 до cacheSize - 1 делай
				thisElement := internalCache[i];
				ЛогЯдра.пЦел64(i,3); ЛогЯдра.пСтроку8(": ");
				нцПока thisElement # НУЛЬ делай
					ЛогЯдра.пЦел64(thisElement.key,4); ЛогЯдра.пСтроку8(" (");
					если thisElement.cValue = -1 то ЛогЯдра.пСтроку8(thisElement.sValue) всё;
					ЛогЯдра.пСтроку8(") -> ");
					thisElement := thisElement.next;
				кц;
				ЛогЯдра.пВК_ПС;
			кц;
		кон Print;

	кон CharacterPropertyCache;

	(* A handy implementation for text file reading and analyzation. Basic functionality is provided. TxtReaders that
	    handle a specific text layout, should inherit this class and (re-)implement necessary procedures. *)
	TxtReader = окласс
	перем
		filename : массив 256 из симв8;
		text : Texts.Text;
		textReader : Texts.TextReader;
		startPos : размерМЗ;
		decoder : Codecs.TextDecoder;
		msg : массив 512 из симв8;
		fullname : массив 256 из симв8;
		file : Files.File;
		in: Потоки.Чтец;
		decoderRes : целМЗ;

		(* loads a file into a local Text and creates an associated TextReader *)
		проц LoadTxtFile;
		нач
			error := ложь;
			копируйСтрокуДо0(filename, fullname);

			(* Check whether file exists and get its canonical name *)
			file := Files.Old(filename);
			если (file # НУЛЬ) то
				file.GetName(fullname);
			иначе
				file := Files.New(filename); (* to get path *)
				если (file # НУЛЬ) то
					file.GetName(fullname);
					file := НУЛЬ;
				всё;
			всё;

			если (file # НУЛЬ) то
				decoder := Codecs.GetTextDecoder("ISO8859-1");

				если (decoder # НУЛЬ) то
					in := Codecs.OpenInputStream(fullname);
					если in # НУЛЬ то
						decoder.Open(in, decoderRes);
						если decoderRes = 0 то
							text := decoder.GetText();
							нов(textReader,text);
						всё;
					иначе
						msg := "Can't open input stream on file "; Strings.Append(msg, fullname);
						ЛогЯдра.пСтроку8(msg);
						error := истина;
					всё;
				иначе
					msg := "No decoder for file "; Strings.Append(msg, fullname);
					Strings.Append(msg, " (Format: "); Strings.Append(msg, "ISO8859-1"); Strings.Append(msg, ")");
					ЛогЯдра.пСтроку8(msg);
					error := истина;
				всё;
			иначе
				msg := "file '"; Strings.Append(msg, fullname); Strings.Append(msg,"' not found.");
				ЛогЯдра.пСтроку8(msg);
				error := истина;
			всё;
			FindStartPos;
		кон LoadTxtFile;

		(* Abstract procedure to be overwritten by the children of TxtReader *)
		проц FindStartPos;
		нач
			СТОП (999);
		кон FindStartPos;

		(* Skips a whole line of the file *)
		проц NextLine;
		перем
			thisChar : Texts.Char32;
		нач
			если textReader = НУЛЬ то возврат всё;

			(* read the characters until the end of the line is reached *)
			нцДо
				textReader.ReadCh(thisChar);
			кцПри ((thisChar = LF) или (thisChar = CR));
		кон NextLine;

	кон TxtReader;


тип

	(* TxtReader to read the UnicodeData.txt file. So far there's direct support for the bidi character type and the
	'mirrored' property. More explicit lookups can easily be added later on. *)
	UnicodeTxtReader*=окласс(TxtReader)
	перем
		(* For each property that is explicitly needed, a cache is used. Whenever a new property is needed often,
		feel free to add another cache. *)
		charTypeCache, mirrorPropCache : CharacterPropertyCache;

		(* Loads the UnicodeData.txt into memory and creates the caches. *)
		проц &Init*;
		нач
			filename := "UnicodeData.txt";
			LoadTxtFile;
			нов(charTypeCache,256);
			нов(mirrorPropCache,256);
		кон Init;

		(* The property file has no leading comments. Therefore there are no lines to be skipped *)
		проц {перекрыта}FindStartPos;
		нач
			startPos := 0;
		кон FindStartPos;


		(* Returns the bidirectional character type for a specific character *)
		проц GetBidiCharacterType*(char : Texts.Char32; перем res : Strings.String);
		перем
			tempRes : массив 16 из симв8;
		нач

			(* firstly, the appropriate cache is searched for an entry of this character *)
			charTypeCache.SLookup(char,tempRes);

			(* if nothing was found the file is read and the result is added to the cache. *)
			если tempRes = "" то
				GetProperty(char,4,res^);
				если res^ = "" то
					res^ := "L";
					ЛогЯдра.пСтроку8("no character type has been found. Using 'L'"); ЛогЯдра.пВК_ПС;
				всё;
				charTypeCache.SInsert(char,res^);
			иначе
				Strings.Copy(tempRes,0,длинаМассива(tempRes),res^);
			всё;
		кон GetBidiCharacterType;

		(* Checks if a specific character has its 'mirrored' property set to 'yes' *)
		проц IsMirroredChar*(char : Texts.Char32) : булево;
		перем
			res : массив 16 из симв8;
		нач

			(* firstly, the appropriate cache is searched for an entry of this character *)
			mirrorPropCache.SLookup(char,res);

			(* if nothing was found the file is read and the result is added to the cache. *)
			если res = "" то
				GetProperty(char,9,res);
				mirrorPropCache.SInsert(char,res);
			всё;

			возврат res = "Y";
		кон IsMirroredChar;

		(* Checks if the character type of a specific character is 'WS' *)
		проц IsWhiteSpaceChar*(char : Texts.Char32) : булево;
		перем
			res : массив 16 из симв8;
		нач

			(* firstly, the appropriate cache is searched for an entry of this character *)
			charTypeCache.SLookup(char,res);

			(* if nothing was found the file is read and the result is added to the cache. *)
			если res = "" то
				GetProperty(char,4,res);
				charTypeCache.SInsert(char,res);
			всё;

			возврат res = "WS";
		кон IsWhiteSpaceChar;

		(* Gets the character's property at a certain position (0 being the character itself). *)
		проц GetProperty*(char : Texts.Char32; pos : размерМЗ; перем res : массив из симв8);
		перем
			thisChar, thisInt : Texts.Char32;
			i,j : цел16;
			dummyVal : целМЗ;
		нач
			text.AcquireRead;
			textReader.SetPosition(startPos);

			(* iterate through characters *)
			нц
				i := 0;
				(* iterate through properties *)
				нц
					j := 0;
					(* read the current property *)
					нцДо
						textReader.ReadCh(thisChar);

						(* is end of file reached? *)
						если (j = 0) и ((thisChar = EOT) или (thisChar = NUL)) то
							res[j] := симв8ИзКода(0H);
							text.ReleaseRead;
							возврат;
						всё;

						(* store the string if its the character's coded or the wanted property *)
						если (i = pos) или (i = 0) то
							res[j] := симв8ИзКода(thisChar);
						всё;
						увел(j);
					кцПри (thisChar = кодСимв8(';')) или (thisChar = CR) или (thisChar = LF);

					(* the property has been found *)
					если (i = pos) то
						res[j-1] := симв8ИзКода(0H);
						text.ReleaseRead;
						возврат;
					(* the character's code has been found *)
					аесли (i = 0) то
						res[j-1] := симв8ИзКода(0H);
						Strings.HexStrToInt(res,thisInt, dummyVal);

						(* carry on if the this was not the wanted character yet *)
						если (thisInt < char) то
							прервиЦикл;
						(* return if the wanted character has already been passed *)
						аесли (thisInt > char) то
							res[0] := симв8ИзКода(0H);
							text.ReleaseRead;
							возврат;
						всё;
					(* return if the wanted property has already been passed *)
					аесли (i > pos) то
						res[0] := симв8ИзКода(0H);
						text.ReleaseRead;
						возврат;
					всё;
					(* carry on if this was the last property of the line *)
					если (thisChar = CR) или (thisChar = LF) то
						прервиЦикл;
					всё;
					увел(i);
				кц;
				NextLine;
			кц;

			text.ReleaseRead;
		кон GetProperty;

		(* Exported procedure to print the character type cache *)
		проц PrintCharTypeCache*;
		нач
			charTypeCache.Print;
		кон PrintCharTypeCache;

	кон UnicodeTxtReader;


тип

	(* TxtReader to read the BidiMirroring.txt file. *)
	BidiMirroringTxtReader*=окласс(TxtReader)
	перем
		mirrorCache : CharacterPropertyCache;

		(* Loads the BidiMirroring.txt into memory *)
		проц &Init*;
		нач
			filename := "BidiMirroring.txt";
			LoadTxtFile;
			нов(mirrorCache,256);
		кон Init;


		(* Finds the start position of the relevant data. The mirroring file has a large comment at the beginning,
		so the scanner needs to be set to the first line of interest. *)
		проц {перекрыта}FindStartPos;
		перем
			thisChar : цел32;
		нач
			thisChar := 0;
			text.AcquireRead;

			(* read the line's first character and skip the line if it's a '#' *)
			textReader.ReadCh(thisChar);
	 		нцПока (thisChar = кодСимв8('#')) делай
	 			NextLine;
	 			textReader.ReadCh(thisChar);
	 		кц;

			(* store the start position *)
	 		startPos := textReader.GetPosition();
	 		text.ReleaseRead;
		кон FindStartPos;



		(* Reads the next source character. The procedure assumes the scanner to be at the beginning	of the line. *)
		проц GetSourceChar() : Texts.Char32;
		перем
			sourceString : массив 7 из симв8;
			sourceInt, tempChar : Texts.Char32;
			i : цел16;
			res : целМЗ;
		нач
			sourceInt := -1;
			i := -1;

			(* read the characters that form the code for the source character *)
			нцДо
				увел(i);
				textReader.ReadCh(tempChar);
				sourceString[i] := симв8ИзКода(tempChar);
			кцПри (tempChar = EOT) или (tempChar = кодСимв8('#')) или (tempChar = кодСимв8(';'));

			(* if the character was terminated by a ';' it is assumed to be valid and is converted into an integer *)
			если (tempChar = кодСимв8(';')) то
				sourceString[i] := симв8ИзКода(0H);
				Strings.HexStrToInt(sourceString,sourceInt,res);
			всё;

			возврат sourceInt;
		кон GetSourceChar;


		(* Reads the next target character. The procedure assumes the scanner to have already read the source character
		and to be now at the beginning of the target character's code. Additionally it assumes the text to be locked. *)
		проц GetTargetChar() : Texts.Char32;
		перем
			targetString : массив 7 из симв8;
			targetInt, tempChar : Texts.Char32;
			i : цел16;
			res : целМЗ;
		нач
			targetInt := -1;
			i := -1;

			(* read the whitespace *)
			textReader.ReadCh(tempChar);

			(* read the characters that form the code for the target character *)
			нцДо
				увел(i);
				textReader.ReadCh(tempChar);
				targetString[i] := симв8ИзКода(tempChar);
			кцПри (tempChar = EOT) или (tempChar = кодСимв8('#')) или (tempChar = SP);

			(* terminate the result string and convert it into an integer *)
			targetString[i] := симв8ИзКода(0H);
			Strings.HexStrToInt(targetString,targetInt,res);

			возврат targetInt;
		кон GetTargetChar;



		(* Searches the mirror file for a given character and returns its counterpart if found. *)
		проц GetMirroredChar*(char : Texts.Char32) : Texts.Char32;
		перем
			sChar : Texts.Char32;
		нач

			(* look in the cache first *)
			sChar := mirrorCache.CLookup(char);
			если sChar = -1 то
				text.AcquireRead;

				(* search the right source character *)
				textReader.SetPosition(startPos);
				нцДо
					sChar := GetSourceChar();
					если (sChar # char) то
						NextLine;
					всё;
				кцПри (sChar = char) или (sChar = -1);	(* if the char is found or if the end of chars is reached, jump out of the loop *)

				(* return Null if the source character could not be found *)
				если (sChar = -1) то
					text.ReleaseRead;
					возврат 0;
				иначе
					(* get the target character, store it in the cache and return it *)
					sChar := GetTargetChar();
					mirrorCache.CInsert(char,sChar);
					text.ReleaseRead;
					возврат sChar;
				всё;
			иначе
				возврат sChar;
			всё;
		кон GetMirroredChar;

	кон BidiMirroringTxtReader;

кон UnicodeProperties.



System.Free UnicodeProperties ~


UnicodeProperties.TestIsMirroredChar 00000028H ~


PC0.Compile UnicodeProperties.Mod ~
