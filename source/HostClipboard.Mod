модуль HostClipboard; (** AUTHOR "staubesv"; PURPOSE "Interface to host system clipboard"; *)
(**
 * This module provides an interface for accessing the host system clipboard (e.g. Windows, Virtual Machine, ...)
 *)

использует
	Texts;

конст
	Ok* = 0;
	ClipboardNotAvailable* = 1;

тип
	ClipboardHandler = проц {делегат} (text : Texts.Text);

перем
	getFromClipboard, putToClipboard : ClipboardHandler;

проц Get*(text : Texts.Text; перем res : целМЗ);
нач {единолично}
	утв((text # НУЛЬ) и (text.HasWriteLock()));
	если (getFromClipboard # НУЛЬ) то
		getFromClipboard(text);
		res := Ok;
	иначе
		res := ClipboardNotAvailable;
	всё;
кон Get;

проц Put*(text : Texts.Text; перем res : целМЗ);
нач {единолично}
	утв((text # НУЛЬ) и (text.HasReadLock()));
	если (putToClipboard # НУЛЬ) то
		putToClipboard(text);
		res := Ok;
	иначе
		res := ClipboardNotAvailable;
	всё;
кон Put;

проц SetHandlers*(get, put : ClipboardHandler);
нач {единолично}
	getFromClipboard := get; putToClipboard := put;
кон SetHandlers;

кон HostClipboard.
