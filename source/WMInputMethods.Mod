модуль WMInputMethods;	(** AUTHOR "TF"; PURPOSE "Abstract input method editor"; *)

использует
	Потоки, Strings, Configuration, Texts, TextUtilities, Commands, Modules;

конст
	CR = 0DX; LF = 0AX;
	keySymПереключенияМетодаВвода* = 0FFC2H (* F5 *);

тип
	IMEInterface* = запись
		AcquireText*, ReleaseText* : проц {делегат};
		InsertUCS32* : проц {делегат} (atPosition : размерМЗ; конст string : Texts.UCS32String);
		GetCursorPosition* : проц {делегат} () : размерМЗ;
		GetCursorScreenPosition* : проц {делегат} (перем x, y : размерМЗ);
		SetCursorInfo* : проц {делегат} (position : размерМЗ);
	кон;


	IME* = окласс
	перем
		interface : IMEInterface;
		valid : булево; (* if TRUE, all delegates in <interface> are not NIL *)

		проц &Init*;
		нач
			interface := None;
			valid := ложь;
		кон Init;

		проц GetName*() : Strings.String;
		нач
			возврат НУЛЬ
		кон GetName;

		проц SetInterface*(i : IMEInterface);
		нач {единолично}
			утв(
				 ((i.AcquireText # НУЛЬ) и (i.ReleaseText # НУЛЬ) и (i.InsertUCS32 # НУЛЬ) и
					(i.GetCursorPosition # НУЛЬ) и (i.GetCursorScreenPosition # НУЛЬ) и (i.SetCursorInfo # НУЛЬ))
				или
				 ((i.AcquireText = НУЛЬ) и (i.ReleaseText = НУЛЬ) и (i.InsertUCS32 = НУЛЬ) и
					(i.GetCursorPosition = НУЛЬ) и (i.GetCursorScreenPosition = НУЛЬ) и (i.SetCursorInfo = НУЛЬ))
			);
			сам.interface := i;
			valid := interface.AcquireText # НУЛЬ;
		кон SetInterface;

		проц InsertChar*(ucs : размерМЗ);
		перем buf : массив 2 из Texts.Char32;
		нач {единолично}
			если valid то
				interface.AcquireText;
				interface.SetCursorInfo(1);
				buf[0] := ucs(Texts.Char32); buf[1] := 0;
				interface.InsertUCS32(interface.GetCursorPosition(), buf);
				interface.ReleaseText;
			всё;
		кон InsertChar;

		проц InsertMultiChar*(конст ucs : массив из цел32);
		перем buf : массив 2 из Texts.Char32; pos, i : размерМЗ;
		нач {единолично}
			если valid то
				interface.AcquireText;
				pos := interface.GetCursorPosition();
				interface.SetCursorInfo(длинаМассива(ucs));
				buf[1] := 0;
				нцДля i := 0 до длинаМассива(ucs) - 1 делай
					buf[0] := ucs[i];
					interface.InsertUCS32(pos, buf);
				кц;
				interface.ReleaseText;
			всё;
		кон InsertMultiChar;

		проц InsertUTF8String*(конст string : массив из симв8);
		перем r : Потоки.ЧтецИзСтроки;
			pos, i, m: размерМЗ;
			tempUCS32 : массив 1024 из Texts.Char32;
			ch, last : Texts.Char32;
		нач {единолично}
			если valid то
				interface.AcquireText;
				pos := interface.GetCursorPosition();
				нов(r, длинаМассива(string));
				m := длинаМассива(tempUCS32) - 1;
				r.ПримиСтроку8ДляЧтения(string);
				i := 0;
				нцДо
					если TextUtilities.GetUTF8Char(r, ch) то
						если i = m то tempUCS32[i] := 0; interface.InsertUCS32(pos, tempUCS32); увел(pos, m); i := 0 всё;
						если (last # кодСимв8(CR)) или (ch # кодСимв8(LF)) то
							если ch = кодСимв8(CR) то tempUCS32[i] := кодСимв8(LF)
							иначе tempUCS32[i] := ch
							всё;
							увел(i)
						всё;
						last := ch
					всё
				кцПри (r.кодВозвратаПоследнейОперации # Потоки.Успех);
				tempUCS32[i] := 0; interface.InsertUCS32(pos, tempUCS32);
				interface.ReleaseText;
			всё;
		кон InsertUTF8String;

		проц GetCursorScreenPosition*(перем x, y : размерМЗ);
		нач {единолично}
			если valid то
				interface.GetCursorScreenPosition(x, y);
			иначе
				x := 0; y := 0;
			всё;
		кон GetCursorScreenPosition;

		(* Только при нажатии кнопки *)
		проц KeyEvent*(ucs : размерМЗ; flags : мнвоНаБитахМЗ; keysym : размерМЗ);
		кон KeyEvent;
		
		(* возвращает-истину,-если-текст-был-вставлен-*)
		проц ОбработайСобытиеОтжатияКнопки*(ucs : размерМЗ; flags : мнвоНаБитахМЗ; keysym : размерМЗ) : булево;
		кон ОбработайСобытиеОтжатияКнопки;


		проц Hide*;
		кон Hide;

		проц Finalize*;
		нач
			SetInterface(None);
		кон Finalize;

	кон IME;

	(* the installer will register the IME *)
	IMEInstaller*  = проц;

	(* the switch is called to let the owner of the switch know, that the IME has been switched *)
	IMToolSwitchCallback* = проц;
	
	(* Пр-ра из WMTextView, к-рая будет перерисовывать текущий курсор *)
	WMTextViewSwitchCallback* = проц;

перем
	defaultIME- : IME;
	activeIME- : IME;
	toolSwitch* : IMToolSwitchCallback;
	aVMTextViewSwitchCallback* : WMTextViewSwitchCallback;
	None- : IMEInterface;

(* Returns the installer procedure of the desired IME *)
проц GetIME*(конст name : массив из симв8; перем res : целМЗ) : IMEInstaller;
перем
	config, installerName, msg : массив 128 из симв8;
	moduleName, procedureName : Modules.Name;
	installer : IMEInstaller;
нач
	res := -1;

	(* look in the config file for the appropriate installer's name *)
	config := "IME."; Strings.Append(config,name);
	Configuration.Get(config,installerName,res);
	(* retrieve the procedure *)
	если (res = 0) то
		Commands.Split(installerName, moduleName, procedureName, res, msg);
	всё;
	если (res = 0) то
		дайПроцПоИмени(moduleName,procedureName,installer);
	всё;
	возврат installer;
кон GetIME;

(* switches the on/off-state of the default IME *)
проц SwitchIME*() : IME;
нач
	(* set the active IME to the default IME or to NIL, respectively
	FIXME здесь должно быть Exclusive? *)
	если activeIME = НУЛЬ то
		activeIME := defaultIME;
	иначе
		activeIME := НУЛЬ;
	всё;

	(* If an external tool registered a switch procedure, it is called now *)
	если toolSwitch # НУЛЬ то
		toolSwitch;
	всё;

	если aVMTextViewSwitchCallback # НУЛЬ то
		aVMTextViewSwitchCallback;
	всё;

	(* return the currently active IME *)
	возврат activeIME
кон SwitchIME;

(* sets a new default IME and sets it active if the old IME was active *)
проц InstallIME*(newIME : IME);
нач
	defaultIME := newIME;
	если (activeIME # НУЛЬ) то
		activeIME := newIME;
	всё;
кон InstallIME;

нач
	None.AcquireText := НУЛЬ; None.ReleaseText := НУЛЬ; None.InsertUCS32 := НУЛЬ;
	None.GetCursorPosition := НУЛЬ; None.GetCursorScreenPosition := НУЛЬ;
кон WMInputMethods.
