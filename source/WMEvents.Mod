модуль WMEvents; (** AUTHOR "TF"; PURPOSE "Events"; *)
(* PH added rate limited Call procedure*)

использует
	Kernel, Objects, Strings, ЛогЯдра;

тип
	String = Strings.String;

	(** Generic Event Listener - процедура, которую можно назначить на наступление события.
	
	 	Данная тип является делегатом, т.е. переменной такого типа можно назначить не процедуру, а метод.
	 	В момент назначения метод связывается с тем объектом, который назначал. 
	 	При вызове будет вызван этот метод от этого объекта. Пример делегата: "разбуди меня, когда захочешь есть".
	 	Здесь есть действие (разбудить), источник причины для этого действия (ты), сама причина (захочешь есть)
	 	и указание, над кем действие должно быть произведено (я). Таким образом, объект ложащийся спать 
	 	передаёт свой слушатель события объекту, который является источником события. 
	 	
	 	Передача делегата для вызова оповещения требует, чтобы в принимающем объекте 
	 	(который будет генерировать события, т.е., в нашем примере, будить) 
	 	существовал WMEvents.EventSource, который должен не просто быть полем, 
	 	а должен ещё быть инициализирован с помощью последовательности магических действий. 
	 	
	 	См. например, вхождения слова onLinkClicked в WMTextView.Mod
	 	
	 	Сама передача состоит в вызове метода Add для источника события (см. применение onLinkClicked в TFPET). 	
	 	
	 	После того, как передача делегата состоялась и оба объекта приведены в работающее состояние (понятие
	 	работающего состояния несколько туманно, но им должен быть назначен sequencer (очередь сообщений) и 
	 	вызван Initialize),	оповещение работает. Т.е., когда в коде источника событий происходит 
	 	вызов Call(поле-типа-EventSource),	в очередь событий слушателя (которого нужно разбудить) кладётся
	 	некая	 	штука, которая в своё время выполнит делегата, причём выполнение будет из sequencer. 
	 	Последнее важно, т.к. многие процедуры проверяют, что они вызваны из очереди сообщений своего 
	 	объекта. Данная защита предназначена для того, чтобы каждый объект сам контролировал своё состояние. 
	 	
	 	Упрощённый вариант этой схемы, позволяющий избежать механизм подписки, можно назвать шаблоном проектирования 
	 	"Спасибо, ваш заказ принят". 	Объект представляет метод, который может вызвать кто угодно, но если этот метод 
	 	вызван не через очередь событий объекта, то объект кладёт вызов в свою собственную очередь сообщений:
	 	
````
		PROCEDURE ShowPublicHandler(sender, data : ANY);
		BEGIN
			IF ~IsCallFromSequencer() THEN
				sequencer.ScheduleEvent(SELF.ShowPublicHandler, sender, data);
				RETURN
			END;
			showPublicOnly := ~showPublicOnly;
			publicBtn.SetPressed(showPublicOnly);
			tree.Acquire;
			SetNodeVisibilities(tree.GetRoot(), showPublicOnly);
			tree.Release;
		END ShowPublicHandler;
````

		Так происходит отвязка вызовающего (управление вернётся сразу же) от вызываемого (который выполнит задание позже).
	 	*)

	(* EventListener будет вызван через очередь сообщений для объекта, подписавшегося на событие. *)	
	EventListener* = проц  { делегат } (sender, par : динамическиТипизированныйУкль);
	(** EventListenerFinder searches an EventListener by string in its context and returns the EventListener or NIL *)
	CompCommandFinder* = проц { делегат } (str : String) : EventListener;

	(* element of list of EventListeners *)
	EventListenerLink = укль на запись
		event : EventListener;
		string : String;
		next : EventListenerLink;
	кон;

	(** Event info class. CompCommand can be registered and unregistered to/from this class. 
	EventSource. Поле такого типа нужно добавить в объект, чтобы он мог отправлять другим объектам
	сообщения о событиях, которые у него произошли. Другие объекты вызывают метод Add этого поля, 
	чтобы подписаться на сообщения. А сам объект вызывает метод Call, чтобы осуществить рассылку *)
	EventSource* = окласс
	перем
		listeners : запись
			event : EventListener;
			string : String;
			next : EventListenerLink;
		кон;
		name, info : String;
		owner : динамическиТипизированныйУкль;
		finder : CompCommandFinder;
		next : EventSource;

		minimalDelay: цел32;
		updateTimer:Kernel.MilliTimer;
		lastFrameTimer: Objects.Timer;
		lastPar: динамическиТипизированныйУкль;

		(** create an EventInfo for a component owner. Report name as the name of this event *)
		проц &New*(owner : динамическиТипизированныйУкль; name, info : String; finder : CompCommandFinder);
		нач
			сам.owner := owner; сам.name := name; сам.info := info; сам.finder := finder;
			listeners.event := НУЛЬ; listeners.string := НУЛЬ; listeners.next := НУЛЬ;
			next := НУЛЬ;
			minimalDelay:=0;
		кон New;

		проц SetMinimalDelay*(ms:цел32);
		нач
			minimalDelay:=матМаксимум(0,ms);
			если minimalDelay>0 то
				Kernel.SetTimer(updateTimer,0);
				нов(lastFrameTimer);
			всё;
		кон SetMinimalDelay;

		проц GetName*() : String;
		нач
			возврат name
		кон GetName;

		проц GetInfo*() : String;
		нач
			возврат info
		кон GetInfo;

		(** Add a command to this event. Observers can be added more then once. *)
		проц Add*(observer : EventListener);
		перем new : EventListenerLink;
		нач {единолично}
			если (listeners.event = НУЛЬ) и (listeners.string = НУЛЬ) то
				listeners.event := observer;
			иначе
				нов(new); new.event := observer; new.next := listeners.next; listeners.next := new
			всё;
		кон Add;

		(** Add an listener to this event. The listener is found by findig the component referenced in the string and then
			querying the component for the listener. Listeners can be added more then once.
			The dereferencing is done on demand at the first call. If the EventListener can not be found, the call to the
			respective listener is ignored. On each call, the EventListener is searched again *)
		проц AddByString*(link : String);
		перем new : EventListenerLink;
		нач {единолично}
			если (listeners.event = НУЛЬ) и (listeners.string = НУЛЬ) то
				listeners.string := link;
			иначе
				нов(new); new.string := link; new.next := listeners.next; listeners.next := new
			всё;
		кон AddByString;

		(** Remove the first found entry of event *)
		проц Remove*(observer : EventListener);
		перем cur : EventListenerLink;
		нач {единолично}
			если (listeners.event = observer) то
				если (listeners.next = НУЛЬ) то
					listeners.event := НУЛЬ;
				иначе
					listeners.event := listeners.next.event;
					listeners.string := listeners.next.string;
					listeners.next := listeners.next.next;
				всё;
			аесли (listeners.next # НУЛЬ) то
				если (listeners.next.event = observer) то listeners.next := listeners.next.next;
				иначе
					cur := listeners.next;
					нцПока cur.next # НУЛЬ делай
						если cur.next.event = observer то cur.next := cur.next.next; возврат всё;
						cur := cur.next
					кц
				всё;
			всё;
		кон Remove;

		(** Remove the first found entry of event, specified as a string *)
		проц RemoveByString*(string : String);
		перем cur : EventListenerLink;
		нач {единолично}
			если (listeners.string # НУЛЬ) и (listeners.string^ = string^) то
				если (listeners.next = НУЛЬ) то
					listeners.string := НУЛЬ;
				иначе
					listeners.event := listeners.next.event;
					listeners.string := listeners.next.string;
					listeners.next := listeners.next.next;
				всё;
			аесли (listeners.next # НУЛЬ) то
				если (listeners.next.string # НУЛЬ) и (listeners.next.string^ = string^) то listeners.next := listeners.next.next;
				иначе
					cur := listeners.next;
					нцПока cur.next # НУЛЬ делай
						если (cur.next.string # НУЛЬ) и (cur.next.string^ = string^) то cur.next := cur.next.next; возврат всё;
						cur := cur.next
					кц
				всё;
			всё;
		кон RemoveByString;

		проц CallWithSender*(sender, par: динамическиТипизированныйУкль);
		перем cur : EventListenerLink;
		нач
			если (listeners.event # НУЛЬ) или (listeners.string # НУЛЬ) то
				если listeners.event # НУЛЬ то listeners.event(sender, par);
				аесли (listeners.string # НУЛЬ) и (finder # НУЛЬ) то
					listeners.event := finder(listeners.string);
					если listeners.event = НУЛЬ то ЛогЯдра.пСтроку8("Fixup failed"); ЛогЯдра.пСтроку8(listeners.string^) всё;
					если listeners.event # НУЛЬ то listeners.event(sender, par) всё
				всё;
				cur := listeners.next;
				нцПока cur # НУЛЬ делай
					если cur.event # НУЛЬ то cur.event(sender, par)
					иначе
						если (cur.string # НУЛЬ) и (finder # НУЛЬ) то
							cur.event := finder(cur.string);
							если cur.event = НУЛЬ то ЛогЯдра.пСтроку8("Fixup failed"); ЛогЯдра.пСтроку8(cur.string^) всё;
							если cur.event # НУЛЬ то cur.event(sender, par) всё
						всё
					всё;
					cur := cur.next
				кц;
			всё;
		кон CallWithSender;

		(** Call the event with parameter par.
		The owner of the EventInfo class will be added in the event's sender parameter.
		The rate of triggered events can be limited by setting minimalDelay>0. *)
		проц Call*(par: динамическиТипизированныйУкль);
		нач
			если minimalDelay=0 то
				CallWithSender(owner, par);
			иначе
				Objects.CancelTimeout(lastFrameTimer);
				если Kernel.Expired(updateTimer) то  (* limit rate of triggered events *)
					CallWithSender(owner, par);
					Kernel.SetTimer(updateTimer,minimalDelay);
				иначе
					lastPar:=par;
					Objects.SetTimeout(lastFrameTimer, Call0, minimalDelay); (* guarantee that the last call leads to a message*)
				всё;
			всё;
		кон Call;

		проц Call0;
		нач
			CallWithSender(owner, lastPar);
		кон Call0;

		(** return true if listeners are installed; Can be used to avoid calculating parameters, if there
		are no listeners *)
		проц HasListeners*() : булево;
		нач {единолично}
			возврат (listeners.event # НУЛЬ) или (listeners.string # НУЛЬ);
		кон HasListeners;
	кон EventSource;

тип

	EventSourceArray* = укль на массив из EventSource;

	EventSourceList* = окласс
	перем
		head : EventSource;
		nofEventSources : цел32;

		проц &New *;
		нач
			head := НУЛЬ; nofEventSources := 0;
		кон New;

		проц Add*(x : EventSource);
		перем e : EventSource;
		нач {единолично}
			утв((x # НУЛЬ) и (x.next = НУЛЬ));
			если (head = НУЛЬ) то
				head := x;
			иначе
				e := head;
				нцПока (e.next # НУЛЬ) делай e := e.next; кц;
				e.next := x;
			всё;
			увел(nofEventSources)
		кон Add;

		проц Remove*(x : EventSource);
		перем e : EventSource;
		нач {единолично}
			утв(x # НУЛЬ);
			если (head = x) то
				head := head.next; x.next := НУЛЬ;
				умень(nofEventSources);
			аесли (head # НУЛЬ) то
				e := head;
				нцПока (e.next # x) делай e := e.next; кц;
				если (e.next # НУЛЬ) то
					e.next := e.next.next; x.next := НУЛЬ;
					умень(nofEventSources);
				всё;
			всё;
		кон Remove;

		проц Enumerate*() : EventSourceArray;
		перем current : EventSourceArray; e : EventSource; i : цел32;
		нач {единолично}
			нов(current, nofEventSources);
			e := head; i := 0;
			нцПока (e # НУЛЬ) делай
				current[i] := e; увел(i);
				e := e.next;
			кц;
			возврат current
		кон Enumerate;

		проц GetEventSourceByName*(name : String) : EventSource;
		перем e : EventSource; n : String;
		нач {единолично}
			e := head;
			нцПока (e # НУЛЬ) делай
				n := e.GetName();
				если (n # НУЛЬ) и (n^ = name^) то возврат e; всё;
				e := e.next;
			кц;
			возврат НУЛЬ;
		кон GetEventSourceByName;

	кон EventSourceList;

тип

	EventListenerInfo* = окласс
	перем
		name, info : String;
		eventListener : EventListener;
		next : EventListenerInfo;

		проц &Init*(name, info : String; handler : EventListener);
		нач
			сам.name := name; сам.info := info; сам.eventListener := handler; next := НУЛЬ;
		кон Init;

		проц GetName*() : String;
		нач
			возврат name
		кон GetName;

		проц GetInfo*() : String;
		нач
			возврат info
		кон GetInfo;

		проц GetHandler*() : EventListener;
		нач
			возврат eventListener
		кон GetHandler;

	кон EventListenerInfo;

тип

	EventListenerArray* = укль на массив из EventListenerInfo;

	EventListenerList* = окласс
	перем
		head : EventListenerInfo;
		nofEventListeners : цел32;

		проц &New *;
		нач
			head := НУЛЬ; nofEventListeners := 0;
		кон New;

		проц Add*(x : EventListenerInfo);
		перем e : EventListenerInfo;
		нач {единолично}
			утв((x # НУЛЬ) и (x.next = НУЛЬ));
			если (head = НУЛЬ) то
				head := x;
			иначе
				e := head;
				нцПока (e.next # НУЛЬ) делай e := e.next; кц;
				e.next := x;
			всё;
			увел(nofEventListeners);
		кон Add;

		проц Remove*(x : EventListenerInfo);
		перем e : EventListenerInfo;
		нач {единолично}
			утв(x # НУЛЬ);
			если (head = x) то
				head := head.next; x.next := НУЛЬ;
				умень(nofEventListeners);
			аесли (head # НУЛЬ) то
				e := head;
				нцПока (e.next # x) делай e := e.next; кц;
				если (e.next # НУЛЬ) то
					e.next := e.next.next; x.next := НУЛЬ;
					умень(nofEventListeners);
				всё;
			всё;
		кон Remove;

		проц Enumerate*() : EventListenerArray;
		перем current : EventListenerArray; e : EventListenerInfo; i : цел32;
		нач {единолично}
			нов(current, nofEventListeners);
			e := head; i := 0;
			нцПока (e # НУЛЬ) делай
				current[i] := e; увел(i);
				e := e.next;
			кц;
			возврат current
		кон Enumerate;

		проц GetHandlerByName*(name : String) : EventListener;
		перем e : EventListenerInfo; n : String;
		нач {единолично}
			e := head;
			нцПока (e # НУЛЬ) делай
				n := e.GetName();
				если (n # НУЛЬ) и (n^ = name^) то
					возврат e.GetHandler()
				всё;
				e := e.next;
			кц;
			возврат НУЛЬ;
		кон GetHandlerByName;

	кон EventListenerList;

кон WMEvents.

