модуль ExampleTextWriter;	(** AUTHOR "TF"; PURPOSE "Template/Example  for component windows"; *)

(** This program shows the implementation of a multi instance component containing window *)

использует
	Strings, WMGraphics, WMComponents, WMWindowManager,
	WMEditors, TextUtilities, Math;

тип
	Window* = окласс (WMComponents.FormWindow)
	перем editor : WMEditors.Editor;

		проц &New*;
		нач
			нов(editor);  editor.bounds.SetExtents(800, 700);
			editor.fillColor.Set(WMGraphics.White);

			Init(editor.bounds.GetWidth(), editor.bounds.GetHeight(), ложь);
			SetContent(editor);

			 WMWindowManager.DefaultAddWindow(сам);
			SetTitle(Strings.NewString("TextWriter Example"));

			WriteToEditor;
		кон New;

		проц WriteToEditor;
		перем
			tw : TextUtilities.TextWriter;
			i : цел32;
			buffer : массив 256 из симв8;
		нач
			нов(tw, editor.text);

			tw.SetFontSize(20);
			tw.пСтроку8("This is a simple text. Count from 0 to 10 : "); tw.пВК_ПС;
			нцДля i := 0 до 10 делай tw.пЦел64(i, 5) кц; tw.пВК_ПС;

			tw.SetFontStyle({WMGraphics.FontBold});
			tw.пСтроку8("This is bold. ");

			tw.SetFontStyle({WMGraphics.FontItalic});
			tw.пСтроку8("This is italic.");

			tw.SetFontStyle({WMGraphics.FontBold});
			tw.SetFontColor(WMGraphics.Red);
			tw.пСтроку8("This is bold red."); tw.пВК_ПС;

			tw.SetBgColor(WMGraphics.Black);
			tw.SetFontColor(WMGraphics.White);
			tw.пСтроку8("This is bold white on black");
			tw.пВК_ПС;

			tw.SetFontStyle({});
			tw.SetBgColor(WMGraphics.White);
			tw.SetFontColor(WMGraphics.Black);

			buffer := "This is a bit fancy! It modulates the vertical offset with a cosine function and fades out.";
			i := 0;
			tw.SetFontStyle({WMGraphics.FontBold});
			нцПока buffer[i] # 0X делай
				tw.SetFontColor(WMGraphics.RGBAToColor(i * 2, i * 2, i * 2, 0FFH));
				tw.SetVerticalOffset(округлиВниз(15 * Math.cos(i/4)));
				tw.пСимв8(buffer[i]);
				увел(i)
			кц;
			tw.ПротолкниБуферВПоток;
		кон WriteToEditor;

	кон Window;

проц Open*;
перем inst, i2 : Window;
нач
	нов(inst);
	нов(i2);
	i2.editor.SetText(inst.editor.text);
кон Open;

кон ExampleTextWriter.

System.Free ExampleTextWriter ~
ExampleTextWriter.Open
