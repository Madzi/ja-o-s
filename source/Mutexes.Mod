(* Runtime support for mutexes *)
(* Copyright (C) Florian Negele *)

(** The Mutexes module provides synchronization objects called mutexes. *)
модуль Mutexes;

использует Activities, Queues;

(** Represents a synchronization object which acquires mutually exclusive access to shared resources. *)
тип Mutex* = запись
	blockedQueue: Queues.Queue; (* maintains a queue of all activities that failed to acquire the mutex *)
	owner {неОтслСборщиком} := НУЛЬ: Activities.Activity; (* stores the activity that has currently acquired the mutex, or NIL if it is not acquired *)
кон;

(** Acquires mutual exclusive access to shared resources that are associated with a mutex. *)
(** If the mutex is currently acquired by some other activity, this procedure waits until it gets exclusive access to it. *)
(** A call to this procedure guarantees mutual exclusive access which has to be released by a subsequent call to the Mutexes.Release procedure. *)
проц Acquire- (перем mutex: Mutex);
перем currentActivity {неОтслСборщиком}, nextActivity: Activities.Activity;
нач {безКооперации, безОбычныхДинПроверок}
	currentActivity := Activities.GetCurrentActivity ();

	(* repeated tries of atomicly changing the owner from NIL to the current activity guarantees mutual exclusion *)
	нцПока сравнениеСОбменом (mutex.owner, НУЛЬ, currentActivity) # НУЛЬ делай

		(* suspend and enqueue the current activity on behalf of its successor *)
		если Activities.Select (nextActivity, Activities.IdlePriority) то
			Activities.SwitchTo (nextActivity, Enqueue, операцияАдресОт mutex);
			Activities.FinalizeSwitch;
		всё;
	кц;
кон Acquire;

(* This procedure is a switch finalizer and is executed by a different activity *)
проц Enqueue (previous {неОтслСборщиком}: Activities.Activity; mutex {неОтслСборщиком}: укль {опасныйДоступКПамяти} на Mutex);
перем item: Queues.Item;
нач {безКооперации, безОбычныхДинПроверок}
	(* the mutex could have already been released at this point, *)
	(* and enqueuing the previous activity might cause it to remain suspended forever *)
	(* the only safe way of inspecting the owner of the mutex is after enqueueing, *)
	(* since the mutex can potentially be released at any time during that operation *)
	Queues.Enqueue (previous, mutex.blockedQueue);

	(* if the mutex was released before enqueueing, the owner of the mutex must have changed to NIL *)
	(* if the owner is still NIL at this point and the releasing activity did not resume any waiting activity, we do that on behalf of it now *)
	(* otherwise, there is either an activity that has acquired the mutex or there is a resumed activity that will try to aquire the mutex *)
	(* in all of these cases, it is guaranteed that there is a running activity that will eventually resume the previous one *)
	если сравнениеСОбменом (mutex.owner, НУЛЬ, НУЛЬ) = НУЛЬ то
		если Queues.Dequeue (item, mutex.blockedQueue) то Activities.Resume (item(Activities.Activity)) всё;
	всё;
кон Enqueue;

(** Releases the mutual exclusive access to shared resources that are associated with a mutex. *)
(** This procedure must be called once after each corresponding call to the Mutexes.Acquire procedure. *)
(** A mutex may not be released if it was not acquired by the same activity beforehand. *)
проц Release- (перем mutex: Mutex);
перем currentActivity {неОтслСборщиком}: Activities.Activity; item: Queues.Item;
нач {безКооперации, безОбычныхДинПроверок}
	currentActivity := Activities.GetCurrentActivity ();

	(* reset the owner and check for consistency *)
	утв (сравнениеСОбменом (mutex.owner, currentActivity, НУЛЬ) = currentActivity);

	(* the mutex could have already been acquired again at this point, *)
	(* but resuming does not hurt since any resumed activity will wait again while acquiring *)
	если Queues.Dequeue (item, mutex.blockedQueue) то Activities.Resume (item(Activities.Activity)) всё;
кон Release;

кон Mutexes.
