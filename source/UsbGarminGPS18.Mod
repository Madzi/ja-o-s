модуль UsbGarminGPS18;  (** AUTHOR "staubesv"; PURPOSE "Garmin GPS 18 USB Driver"; *)
(**
 * Status:
 *
 *	14.12.2005: Device works, delivered data is valid.  No interface for clients implemented so far.
 *
 * Usage:
 *
 *	UsbGarminGPS18.Install ~ loads this driver
 *	System.Free UsbGarminGPS18 ~ unloads it
 *
 * Note: The GPS needs several minutes until it receives PVT (position, velocity, time) data.
 *
 * References:
 *
 *	GARMIN GPS Interface Specification USB Addendum (Rev. 3)
 *
 * History:
 *
 *	01.12.2005 	History started (staubesv)
 *	14.12.2005 	Fixed PVT data parsing, some cleanup (staubesv)
 *	05.07.2006	Adapted to Usbdi (staubesv)
 *)

использует НИЗКОУР, ЛогЯдра, Modules, Kernel, Strings, Usbdi;

конст

	Name = "UsbGps";
	Description = "Garmin GPS 18 USB";

	Debug = истина;

	TraceEvents = {1};
	TracePackets = {2};
	TraceDeviceInfo = {3};
	TraceData = {4};
	TraceSatelliteInfo = {5};
	TracePVTData = {6};
	TraceAll = {0..31};
	TraceNone = {};

	Trace = TracePVTData + TraceSatelliteInfo;

	(* Packet format: 														*)
	(*																		*)
	(* Byte 0 : 		PacketType (Transport Layer = 0, Application Layer = 20)	*)
	(* Byte 1-3 : 	Reserved (must be 0)									*)
	(* Byte 4-5 : 	PacketID												*)
	(* Byte 6-7 : 	Reserved (must be 0)									*)
	(* Byte 4-11: 	DataSize												*)
	(* Byte 12+  :	Data													*)
	PacketTransport = 0;
	PacketApplication = 20;

	(* Transport layer packets *)
	TlPidDataAvailable = 2;
	TlPidStartSession = 5; 	(* host-to-device: start session *)
	TlPidSessionStarted = 6; 	(* device-to-host: session started *)

	(* Application layer packets *)
	AlPidAck = 6;
	AlPidNak = 21;
	AlPidSatelliteData = 114;
	AlPidProtocolArray = 253;
	AlPidProductRequest = 254;
	AlPidProductData = 255;

	GPS18UnitID = 18;

	(* Link Protocol L001 *)
	PidL1CommandData = 10;
	PidL1XferCmplt = 12;
	PidL1DateTimeData = 14;
	PidL1PositionData = 17;
	PidL1PrxWptData = 19;
	PidL1Records = 27;
	PidL1RteHdr = 29;
	PidL1RteWptData = 30;
	PidL1AlamancData = 31;
	PidL1TrkData = 34;
	PidL1WptData = 35;
	PidL1PvtData = 51;
	PidL1RteLinkData = 98;
	PidL1TrkHdr = 99;
	PidL1FlightbookRecord = 134; 		(* packet with Flightbook data *)
	PidL1Lap = 149; 						(* part of Forerunner data *)

	(* Device Command Protocol A10 *)
	CmdA10AbortTransfer = 0; 			(* abort current transfer *)
	CmdA10TransferAlm = 1; 			(* transfer almanac *)
	CmdA10TransferPosn = 2; 			(* transfer position *)
	CmdA10TransferPrx = 3; 				(* transfer proximity waypoints *)
	CmdA10TransferRte = 4; 				(* transfer routes *)
	CmdA10TransferTime =5; 			(* transfer time *)
	CmdA10TransferTrk = 6; 				(* transfer track log *)
	CmdA10TransferWpt = 7; 			(* transfer waypoints *)
	CmdA10TurnOffPwr = 8; 				(* turn off power *)
	CmdA10StartPvtData = 49; 			(* start transmitting PVT data *)
	CmdA10StopPvtData = 50; 			(* stop transmitting PVT data *)
	CmdA10FlightbookTransfer = 92;		(* start transferring flight records *)
	CmdA10TransferLaps = 117; 			(* transfer laps *)

	(* GPS18-specific commands *)
	CmdGps18MeasurementOn = 110;	(* Receiver Measurement Record On *)
	CmdGps18MeasurementOff = 111; 	(* Receiver Measurement Record On *)

	(* Coding of D800PVT.fix field *)
	FixUnusable = 0;
	FixInvalid = 1;
	Fix2D = 2;
	Fix3D = 3;
	Fix2DDiff = 4;
	Fix3DDiff = 5;

	pi = 3.14159265358979323846E0;

тип

	ProtocolDataType = запись
		tag : симв8;
		data : цел32;
	кон;

	SatelliteData = запись;
		svid : цел32; 		(* space vehicle identification (1-32 and 33-64 for WAAS); (uint8) *)
		snr : цел32; 		(* signal-to-noise ratio (uint16) 						*)
		elev : цел32; 	(* satellite elevation in degrees (uint8) 				*)
		azmuth : цел32; 	(* satellite azmuth in degrees (uint16) 				*)
		(* status bit field:														*)
		(* Bit 1: The unit has ephemeris data for the specified satellite			*)
		(* Bit 2: The unit has a differential correction for the specified satellite		*)
		(* Bit 3: The unit is using this satellite in the solution 						*)
		status : мнвоНаБитахМЗ;
	кон;

	SatelliteInfo = окласс
	перем
		info : массив 12 из SatelliteData;
		valid : булево; (* did we already receive the information ?? *)

		проц Parse(data : массив из симв8);
		перем temp, i : цел32;
		нач {единолично}
			если длинаМассива(data) # 84 то возврат; всё;
			нцДля i := 0 до 11 делай
				info[i].svid := кодСимв8(data[i*7]);
				info[i].snr := кодСимв8(data[i*7+1]) + 256*кодСимв8(data[i*7+2]);
				info[i].elev := кодСимв8(data[i*7+3]);
				info[i].azmuth := кодСимв8(data[i*7+4]) + 256*кодСимв8(data[i*7+5]);
				temp := кодСимв8(data[i*7+6]); (* QBIC broken SYSTEM.VAL() workaround *)
				info[i].status := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, temp);
			кц;
			valid := истина;
		кон Parse;

		проц Show(details : булево);
		конст SnrThreshold = 100;
		перем nbrOfSatellites, i : цел32;
		нач {единолично}
			ЛогЯдра.пСтроку8("Satellite Data: "); ЛогЯдра.пВК_ПС;
			если valid то
				если details то
					нцДля i := 0 до 11 делай
						ЛогЯдра.пСтроку8("Info Rec "); ЛогЯдра.пЦел64(i, 4);
						ЛогЯдра.пСтроку8(": Vehicle identification: "); ЛогЯдра.пЦел64(info[i].svid, 5);
						ЛогЯдра.пСтроку8(" SNR: "); ЛогЯдра.пЦел64(info[i].snr, 5);
						ЛогЯдра.пСтроку8(" elevation: "); ЛогЯдра.пЦел64(info[i].elev, 5);
						ЛогЯдра.пСтроку8(" azmuth : "); ЛогЯдра.пЦел64(info[i].azmuth, 5);
						ЛогЯдра.пСтроку8(" Status: ");
						если 0 в info[i].status то ЛогЯдра.пСтроку8("[ephemeris]"); всё;
						если 1 в info[i].status то ЛогЯдра.пСтроку8("[differential correction]"); всё;
						если 2 в info[i].status то ЛогЯдра.пСтроку8("[inUse]"); всё;
						ЛогЯдра.пВК_ПС;
					кц;
				иначе
					nbrOfSatellites := 0;
					нцДля i := 0 до 11 делай если info[i].snr >= SnrThreshold то увел(nbrOfSatellites); всё; кц;
					ЛогЯдра.пСтроку8(" Satellites: ");  ЛогЯдра.пЦел64(nbrOfSatellites, 0); ЛогЯдра.пВК_ПС;
				всё;
			иначе
				ЛогЯдра.пСтроку8("Information not yet received."); ЛогЯдра.пВК_ПС;
			всё;
		кон Show;

		проц &Init*;
		нач
			valid := ложь;
		кон Init;

	кон SatelliteInfo;

тип

	RadianType* = запись
		lat-, long- : вещ64;
	кон;

	D800PVT* = запись;
		alt- : вещ32; 				(* altitude above WGS 84 ellipsoid (meters) *)
		epe- : вещ32; 				(* estimated position error, 2 sigma (meters) *)
		eph-, epv- : вещ32; 		(* epe, but horizontal rsp. vertical only (meters) *)
		fix- : цел16; 			(* type of position fix: 0 = no fix; 1 = no fix; 2 = 2D; 3 = 3D; 4 = 2D differential, 5 = 3D differential *)
		tow- : вещ64; 		(* time of week (seconds) *)
		posn- : RadianType;  		(* latitude and longitude (radians) *)
		east-, north-, up- : вещ32; 	(* velocity east, north & up (meters per second) *)
		mslHeight- : вещ32; 		(* height of WGS 84 ellipsoid above MSL (meters) *)
		leapSeconds- : цел16; 	(* difference between GPS and UTC *)
		wnDays- : цел32; 		(* week number days *)
	кон;

	(* Note: Consider the D800PVT.fix field for information about which fields are valid *)
	PvtInfo = окласс
	перем
		pvtData : D800PVT;
		valid : булево;

		проц Parse(data : массив из симв8);
		нач {единолично}
			если длинаМассива(data) # 64 то возврат всё;
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[0]), pvtData.alt);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[4]), pvtData.epe);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[8]), pvtData.eph);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[12]), pvtData.epv);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[16]), pvtData.fix);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[18]), pvtData.tow);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[26]), pvtData.posn.lat);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[34]), pvtData.posn.long);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[42]), pvtData.east);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[46]), pvtData.north);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[50]), pvtData.up);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[54]), pvtData.mslHeight);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[59]), pvtData.leapSeconds);
			НИЗКОУР.прочтиОбъектПоАдресу(адресОт(data[60]), pvtData.wnDays);
			valid := истина;
		кон Parse;

		проц GetPVTdata() : D800PVT;
		нач {единолично}
			возврат pvtData;
		кон GetPVTdata;

		проц Show(details : булево);
		перем string : массив 32 из симв8; temp : вещ32;
		нач {единолично}
			ЛогЯдра.пСтроку8("GPS18: Position Info: "); ЛогЯдра.пВК_ПС;
			если valid то
				ЛогЯдра.пСтроку8("Position: ");
				ЛогЯдра.пСтроку8("Latitude: ");
				string := ""; Strings.FloatToStr(RadianToDegree(pvtData.posn.lat), 5, 4, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("N degree, ");
				ЛогЯдра.пСтроку8("Longitude: ");
				string := ""; Strings.FloatToStr(RadianToDegree(pvtData.posn.long), 5, 4, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("E degree");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("Altitude: ");
				temp := pvtData.alt + pvtData.mslHeight;
				string := ""; Strings.FloatToStr(temp, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m (MSL)");
				ЛогЯдра.пСтроку8(", alt: "); string := ""; Strings.FloatToStr(pvtData.alt, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m, ");
				ЛогЯдра.пСтроку8(", msl: "); string := ""; Strings.FloatToStr(pvtData.mslHeight, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m, ");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("Estimated Position Error: ");
				string := ""; Strings.FloatToStr(pvtData.epe, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m (");
				ЛогЯдра.пСтроку8("Horizontal: "); string := ""; Strings.FloatToStr(pvtData.eph, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m, ");
				ЛогЯдра.пСтроку8("Vertical: "); string := ""; Strings.FloatToStr(pvtData.epv, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m)");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("Velocity: ");
				ЛогЯдра.пСтроку8("E: "); string := ""; Strings.FloatToStr(pvtData.east, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m/s, ");
				ЛогЯдра.пСтроку8("N: ");  string := ""; Strings.FloatToStr(pvtData.north, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m/s, ");
				ЛогЯдра.пСтроку8("Up: ");  string := ""; Strings.FloatToStr(pvtData.up, 5, 2, 0, string); ЛогЯдра.пСтроку8(string); ЛогЯдра.пСтроку8("m/s, ");
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("Fix: "); ShowFix(pvtData.fix); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("WeekNbrDays: "); ЛогЯдра.пЦел64(pvtData.wnDays, 0); ЛогЯдра.пВК_ПС;
			иначе
				ЛогЯдра.пСтроку8("No position info available");
			всё;
			ЛогЯдра.пВК_ПС;
		кон Show;

		проц ShowFix(fix : цел32);
		нач
			просей fix из
				FixUnusable: ЛогЯдра.пСтроку8("unusable");
				|FixInvalid:  ЛогЯдра.пСтроку8("invalid");
				|Fix2D: ЛогЯдра.пСтроку8("2D");
				|Fix3D: ЛогЯдра.пСтроку8("3D");
				|Fix2DDiff: ЛогЯдра.пСтроку8("2D differential");
				|Fix3DDiff: ЛогЯдра.пСтроку8("3D differential");
			иначе
				ЛогЯдра.пСтроку8("unknown("); ЛогЯдра.пЦел64(fix, 0); ЛогЯдра.пСтроку8(")");
			всё;
		кон ShowFix;

		проц &Init*;
		нач
			valid := ложь;
		кон Init;

	кон PvtInfo;

тип

	GarminGPS18= окласс (Usbdi.Driver)
	перем
		satelliteInfo : SatelliteInfo;
		pvt : PvtInfo;
		interruptIn, bulkIn, bulkOut : Usbdi.Pipe;
		interruptInBuffer : Usbdi.BufferPtr;
		started, received : булево;
		epMaxSize : цел32;
		byteCounter : размерМЗ;	(* how many bytes have been received *)

		(* information about the GPS device and its capabilities *)
		unitID : цел32;
		productString : Strings.String;
		productId, softwareVersion : цел32;
		protocolDataType : укль на массив из ProtocolDataType;

		(* packets *)
		startSession, productDataRequest : Usbdi.BufferPtr;

		(* used by Handleevent *)
		data : укль на массив из симв8;
		fragLen, fragOfs: размерМЗ;
		fragType, fragId : цел32;

		(** Start gathering PVT data *)
		проц StartPvtData*() : булево;
		перем command : Usbdi.BufferPtr;
		нач
			нов(command, 2); command[0] := симв8ИзКода(CmdA10StartPvtData); command[1] := симв8ИзКода(0);
			возврат SendPacket(BuildPacket(PacketApplication, PidL1CommandData, command));
		кон StartPvtData;

		(** Stop gathering PVT data *)
		проц StopPvtData*() : булево;
		перем command : Usbdi.BufferPtr;
		нач
			нов(command, 2); command[0] := симв8ИзКода(CmdA10StopPvtData); command[1] := симв8ИзКода(0);
			возврат SendPacket(BuildPacket(PacketApplication, PidL1CommandData, command));
		кон StopPvtData;

		(** Get most recent PVT data. Consider the field fix for validity of data *)
		проц GetPvtData*() : D800PVT;
		нач
			возврат pvt.GetPVTdata();
		кон GetPvtData;

		проц HandlePacket(type, id : цел32; data : массив из симв8);
		перем i : размерМЗ;
		нач
			если Trace * TracePackets # {} то ЛогЯдра.пСтроку8("GPS18: packet received: "); ShowPacket(type, id); ЛогЯдра.пВК_ПС; всё;
			если  type = PacketTransport то

				если id =TlPidSessionStarted то
						если длинаМассива(data) = 4 то
							unitID := Get4(data, 0);
							если Trace * TraceDeviceInfo # {} то ЛогЯдра.пСтроку8("GPS18: Unit ID received: "); ЛогЯдра.п16ричное(unitID, 0); ЛогЯдра.пВК_ПС; всё;
						всё;
						started := истина;
				аесли id = TlPidDataAvailable то
				всё;

			аесли type = PacketApplication то

				если id = AlPidProductData то
					утв(длинаМассива(data) > 4);
					нов(productString, длинаМассива(data)-4);
					productId := кодСимв8(data[0]) + 256*кодСимв8(data[1]);
					softwareVersion := кодСимв8(data[2]) + 256*кодСимв8(data[3]);
					нцДля i :=4 до длинаМассива(data)-1 делай
						productString[i-4] := data[i];
					кц;
					если Trace * TraceDeviceInfo # {} то
						ЛогЯдра.пСтроку8("GPS18: Product ID: "); ЛогЯдра.пЦел64(productId, 0);
						ЛогЯдра.пСтроку8(" Software Version: "); ЛогЯдра.пЦел64(softwareVersion, 0);
						ЛогЯдра.пСтроку8(" Product description: "); ЛогЯдра.пСтроку8(productString^);
						ЛогЯдра.пВК_ПС;
					всё;

				аесли id = AlPidProtocolArray то
					утв(длинаМассива(data)  остОтДеленияНа  3 = 0);
					нов(protocolDataType, длинаМассива(data) DIV 3);
					если Trace * TraceDeviceInfo # {} то ЛогЯдра.пСтроку8("GPS18: Supported protocols: "); всё;
					нцДля i := 0 до (длинаМассива(data) DIV 3) -1 делай
						protocolDataType[i].tag := data[i*3];
						protocolDataType[i].data := кодСимв8(data[i*3+1]) + 256*кодСимв8(data[i*3+2]);
						если Trace * TraceDeviceInfo # {} то ЛогЯдра.пСимв8(protocolDataType[i].tag); ЛогЯдра.пЦел64(protocolDataType[i].data, 0); ЛогЯдра.пСимв8(" "); всё;
					кц;
					если Trace  * TraceDeviceInfo # {}то ЛогЯдра.пВК_ПС; всё;
					received := истина;

				аесли id = AlPidSatelliteData то
					satelliteInfo.Parse(data);
					если Trace * TraceSatelliteInfo # {} то satelliteInfo.Show(истина); всё;

				аесли id = PidL1PvtData то
					pvt.Parse(data);
					если Trace * TracePVTData # {} то pvt.Show(ложь); всё;
				всё;

			иначе
				если Debug то ЛогЯдра.пСтроку8("GPS 18: Unknown packet type. Discarding packet."); ЛогЯдра.пВК_ПС; всё;
			всё;
		кон HandlePacket;

		(* re-assembles fragmented packets and sends them to HandlePacket *)
		проц HandleEvent(status : Usbdi.Status; actLen : размерМЗ);
		перем
			packetType, packetId : цел32;
			dataLen, len, i : размерМЗ;
			error : булево;
		нач
			error := ложь;
			если actLen > 0 то byteCounter := byteCounter + actLen; всё;
			если (status = Usbdi.Ok) или (status = Usbdi.ShortPacket) то
				если fragLen > 0 то (* we expect a fragment of the lenght fragLen *)
					если Trace * TraceEvents # {} то
						ЛогЯдра.пСтроку8("GPS18: Handle fragment: "); ShowPacket(fragType, fragId);
						ЛогЯдра.пСтроку8(" fragLen: "); ЛогЯдра.пЦел64(fragLen, 0); ЛогЯдра.пСтроку8(" fragOfs: "); ЛогЯдра.пЦел64(fragOfs, 0);
						ЛогЯдра.пСтроку8(" actLen: "); ЛогЯдра.пЦел64(actLen, 0); ЛогЯдра.пВК_ПС;
					всё;
					если fragLen > epMaxSize то len := epMaxSize; иначе len := fragLen; всё;
					утв(len <= actLen);

					нцДля i := 0 до len -1 делай data[fragOfs + i] := interruptInBuffer[i]; кц;

					если fragLen > epMaxSize то
						утв(actLen = epMaxSize);
						fragLen := fragLen - epMaxSize;
						fragOfs := fragOfs + epMaxSize;
					иначе
						fragLen := 0; fragOfs := 0;
						HandlePacket(fragType, fragId, data^);
					всё;

				аесли actLen >= 11 то (* beginning of new packet *)
					утв((interruptInBuffer[1]=симв8ИзКода(0)) и (interruptInBuffer[2]=симв8ИзКода(0)) и (interruptInBuffer[3]=симв8ИзКода(0)));
					packetType := кодСимв8(interruptInBuffer[0]);
					packetId := кодСимв8(interruptInBuffer[4]) + 256*кодСимв8(interruptInBuffer[5]);
					утв((interruptInBuffer[6]=симв8ИзКода(0)) и (interruptInBuffer[7]=симв8ИзКода(0)));
					dataLen := Get4(interruptInBuffer^, 8);
					если Trace * TraceEvents # {} то
						ЛогЯдра.пСтроку8("GPS18: Handle packet: "); ShowPacket(packetType, packetId);
						ЛогЯдра.пСтроку8(" dataLen: "); ЛогЯдра.пЦел64(Get4(interruptInBuffer^,8) ,0); ЛогЯдра.пСтроку8(" actLen: "); ЛогЯдра.пЦел64(actLen, 0);
						если Trace * TraceData # {} то ЛогЯдра.пСтроку8(" Data: "); нцДля i := 12 до actLen-1 делай ЛогЯдра.пСимв8(interruptInBuffer[i]); кц; всё;
						ЛогЯдра.пВК_ПС;
					всё;
					нов(data, dataLen);
					если dataLen + 12 > epMaxSize то (* packet fragmented *)
						fragLen := dataLen + 12 - epMaxSize; fragOfs := epMaxSize - 12;
						fragType := packetType; fragId := packetId;
					иначе
						fragLen := 0; fragOfs := 0;
					всё;

					нцДля i := 0 до actLen - 12-1 делай
						data[i] := interruptInBuffer[12 + i];
					кц;

					если fragLen = 0 то HandlePacket(packetType, packetId, data^); всё;

				иначе (* probably the device has been disconneted *)
					если Debug то ЛогЯдра.пСтроку8("GPS18: Serious error encountered..."); ЛогЯдра.пВК_ПС; всё;
					error := истина;
				всё;

				если ~error то status := interruptIn.Transfer(epMaxSize, 0, interruptInBuffer^); (* ignore res *) иначе device.FreePipe(interruptIn); всё;

			иначе
				если Debug то interruptIn.Show(истина); ЛогЯдра.пВК_ПС; всё;
				если status = Usbdi.Stalled то
					если interruptIn.ClearHalt() то
						если Debug то ЛогЯдра.пСтроку8("GPS18: Stall on Interrupt Pipe cleared."); ЛогЯдра.пВК_ПС; всё;
						status := interruptIn.Transfer(epMaxSize, 0, interruptInBuffer^); (* ignore res *)
					иначе
						если Debug то ЛогЯдра.пСтроку8("GPS18: Couldn't clear stall on interrupt pipe. Abort."); ЛогЯдра.пВК_ПС; всё;
						device.FreePipe(interruptIn);
					всё;
				всё;
			всё;
		кон HandleEvent;

		проц SendPacket(packet : Usbdi.BufferPtr) : булево;
		перем status : Usbdi.Status;
		нач {единолично}
			утв(длинаМассива(packet) >= 12);
			status := bulkOut.Transfer(длинаМассива(packet), 0, packet^);
			возврат status = Usbdi.Ok;
		кон SendPacket;

		проц BuildPacket(type, id : цел32; data : Usbdi.BufferPtr) : Usbdi.BufferPtr;
		перем packet : Usbdi.BufferPtr; i : размерМЗ;
		нач
			если data=НУЛЬ то
				нов(packet, 12);
			иначе
				нов(packet, 12 + длинаМассива(data));
				нцДля i := 0 до длинаМассива(data)-1 делай packet[i + 12] := data[i]; кц;
			всё;
			нцДля i := 0 до 11 делай packet[i] := симв8ИзКода(0); кц;
			если data # НУЛЬ то Put4(packet, 8, длинаМассива(data)(цел32)); всё;
			packet[0] := симв8ИзКода(type);
			packet[4] := симв8ИзКода(id);
			возврат packet;
		кон BuildPacket;

		проц {перекрыта}Connect() : булево;
		конст
			MaxWaits = 5000; (* maximum time we wait for the TlSessionStarted packet [ms] *)
		перем
			epInterruptIn, epBulkOut, epBulkIn, epFound : цел32;
			status : Usbdi.Status;
			timer : Kernel.Timer;
			waits : цел32;
			i : цел32;
		нач
			(* Expected endpoints: 1 InterruptIn, 1 BulkIn, 1 Bulkout*)
			нц
				если ((i >= длинаМассива(interface.endpoints)) или (epFound = 3)) то прервиЦикл; всё;
				если interface.endpoints[i].type = Usbdi.InterruptIn то
					epInterruptIn := interface.endpoints[i].bEndpointAddress; epMaxSize := interface.endpoints[i].wMaxPacketSize;
					увел(epFound);
				аесли interface.endpoints[i].type = Usbdi.BulkIn то
					epBulkIn := interface.endpoints[i].bEndpointAddress;
					увел(epFound);
				аесли  interface.endpoints[i].type = Usbdi.BulkOut то
					epBulkOut := interface.endpoints[i].bEndpointAddress;
					увел(epFound);
				всё;
				увел(i);
			кц;

			если ~((epFound = 3) и (epInterruptIn # 0) и (epBulkIn # 0) и (epBulkOut # 0)) то
				если Debug то ЛогЯдра.пСтроку8("GPS18: Error: Expected endpoints not found."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			interruptIn := device.GetPipe(epInterruptIn);
			если interruptIn = НУЛЬ то
				если Debug то ЛогЯдра.пСтроку8("GPS18: Error: Couldn't get interrupt in pipe."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			bulkIn := device.GetPipe(epBulkIn); bulkOut := device.GetPipe(epBulkOut);
			если (bulkIn = НУЛЬ) или (bulkOut = НУЛЬ) то
				если Debug то ЛогЯдра.пСтроку8("GPS18: Error: Couldn't get bulk in/out pipes."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			нов(interruptInBuffer, interruptIn.maxPacketSize);
			interruptIn.SetTimeout(0); (* non-blocking *)
			interruptIn.SetCompletionHandler(HandleEvent);
			status := interruptIn.Transfer(interruptIn.maxPacketSize, 0, interruptInBuffer^); (* ignore res *)

		(*	NEW(buffer, 64);
			bulkIn.SetTimeout(0);
			bulkIn.SetInterruptHandler(HandleEvent);
			status := bulkIn.Send(64, 0, buffer);*)

			(* start session *)
			started := ложь;
			если ~SendPacket(BuildPacket(PacketTransport, TlPidStartSession, НУЛЬ)) то
				если Debug то ЛогЯдра.пСтроку8("GPS18: Couldn't start session"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			(* the start session packet should be acknowlegde by a SessionStarted packet... wait for it *)
			нов(timer); waits := 0;
			нц
				если started или (waits >= MaxWaits) то прервиЦикл; всё;
				timer.Sleep(1);
			кц;

			если ~started то
				если Debug то ЛогЯдра.пСтроку8("GPS18: Timeout: SesssionStarted packet not received"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			(* A001 Protocol capability protocol - get supported protocols *)
			если ~SendPacket(BuildPacket(PacketApplication, AlPidProductRequest, НУЛЬ)) то
				если Debug то ЛогЯдра.пСтроку8("GPS18: Couldn't get product data"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			received := ложь; waits := 0;
			нц
				если received или (waits >= MaxWaits) то прервиЦикл; всё;
				timer.Sleep(1);
			кц;

			если ~received то
				если Debug то ЛогЯдра.пСтроку8("GPS18: Timeout: Product Data packet not received"); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			если StartPvtData() то
				ЛогЯдра.пСтроку8("GPS18: PVT data started"); ЛогЯдра.пВК_ПС;
			иначе
				ЛогЯдра.пСтроку8("GPS18: PVT start command failed."); ЛогЯдра.пВК_ПС;
			всё;

			ЛогЯдра.пСтроку8("Garmin GPS18 connected."); ЛогЯдра.пВК_ПС;
			возврат истина;
		кон Connect;

		проц {перекрыта}Disconnect;
		нач
			ЛогЯдра.пСтроку8("Garmin GPS 18 USB disconnected."); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("GPS18: Received bytes: "); ЛогЯдра.пЦел64(byteCounter, 0); ЛогЯдра.пВК_ПС;
		кон Disconnect;

		проц &Init*;
		нач
			fragLen := 0; нов(satelliteInfo); нов(pvt);
			byteCounter := 0;
		кон Init;

	кон GarminGPS18;

проц RadianToDegree(radian : вещ64) : вещ64;
нач
	возврат radian * (180 / pi);
кон RadianToDegree;

проц Get4(конст buf : массив из симв8; ofs : цел32): цел32;
нач
	утв(ofs + 3 < длинаМассива(buf));
	возврат кодСимв8(buf[ofs]) + логСдвиг(кодСимв8(buf[ofs+1]), 8) + логСдвиг(кодСимв8(buf[ofs+2]), 16) + логСдвиг(кодСимв8(buf[ofs+3]), 24);
кон Get4;

проц Put4(buf : Usbdi.BufferPtr; ofs, value : цел32);
нач
	утв(ofs + 3 < длинаМассива(buf));
	buf[ofs] := симв8ИзКода(НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, value) * {0..7}));
	buf[ofs+1] := симв8ИзКода(НИЗКОУР.подмениТипЗначения(цел32, логСдвиг(НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, value) * {8..15}, -8)));
	buf[ofs+2] := симв8ИзКода(НИЗКОУР.подмениТипЗначения(цел32, логСдвиг(НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, value) * {16..23}, -16)));
	buf[ofs+3] := симв8ИзКода(НИЗКОУР.подмениТипЗначения(цел32, логСдвиг(НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, value) * {24..31}, -24)));
кон Put4;

проц ShowPacket(type, id : цел32);
нач
	ЛогЯдра.пСтроку8("Type: ");
	если type = PacketTransport то ЛогЯдра.пСтроку8("Transport");
	аесли type = PacketApplication то ЛогЯдра.пСтроку8("Application");
	иначе ЛогЯдра.пСтроку8("Unknown("); ЛогЯдра.пЦел64(type, 0); ЛогЯдра.пСтроку8(")");
	всё;
	ЛогЯдра.пСтроку8(" id: ");
	просей id из
		TlPidDataAvailable: ЛогЯдра.пСтроку8("Data Available");
		|TlPidStartSession: ЛогЯдра.пСтроку8("Start Session");
		|AlPidAck:  если type = PacketTransport то ЛогЯдра.пСтроку8("Session Started"); иначе ЛогЯдра.пСтроку8("Ack"); всё;
		|AlPidNak: ЛогЯдра.пСтроку8("Nak");
		|AlPidProtocolArray: ЛогЯдра.пСтроку8("Protocol array");
		|AlPidProductRequest: ЛогЯдра.пСтроку8("Product request");
		|AlPidProductData: ЛогЯдра.пСтроку8("Product Data");
		|AlPidSatelliteData : ЛогЯдра.пСтроку8("Satellite Data Record");
	иначе
		ЛогЯдра.пСтроку8("Unkown("); ЛогЯдра.пЦел64(id, 0); ЛогЯдра.пСтроку8(")");
	всё;
кон ShowPacket;

проц Probe(dev : Usbdi.UsbDevice; id : Usbdi.InterfaceDescriptor) : Usbdi.Driver;
перем driver : GarminGPS18;
нач
	(* check whether the probed device is a Garmin GPS 18 USB*)
	если (dev.descriptor.idVendor # 91EH) или (dev.descriptor.idProduct # 03H) то возврат НУЛЬ; всё;
	если Trace * TraceDeviceInfo # {}  то ЛогЯдра.пСтроку8("Garmin GPS 18 USB found."); ЛогЯдра.пВК_ПС; всё;
	нов(driver);
	возврат driver;
кон Probe;

проц Cleanup;
нач
	Usbdi.drivers.Remove(Name);
кон Cleanup;

проц Install*;
кон Install;

нач
	Modules.InstallTermHandler(Cleanup);
	Usbdi.drivers.Add(Probe, Name, Description, 9)
кон UsbGarminGPS18.

UsbGarminGPS18.Install ~  System.Free UsbGarminGPS18 ~
