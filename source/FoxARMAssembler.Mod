модуль FoxARMAssembler; (** AUTHOR ""; PURPOSE ""; *)

использует InstructionSet := FoxARMInstructionSet, FoxAssembler,  (*D := Debugging,*) Scanner := FoxScanner, Diagnostics, Strings, Basic := FoxBasic;

конст Trace = FoxAssembler.Trace;

тип
	Assembler* = окласс(FoxAssembler.Assembler)
	перем
		проц & Init2*(diagnostics: Diagnostics.Diagnostics);
		нач Init(diagnostics)
		кон Init2;

		(** parse a register name **)
		проц GetRegister*(перем registerNumber: цел32): булево;
		перем
			result: булево;
		нач
			registerNumber := InstructionSet.None;
			result := ложь;
			если token.видЛексемы = Scanner.Идентификатор то
				registerNumber := InstructionSet.RegisterNumberFromName(token.строкаИдентификатора);
				если registerNumber # InstructionSet.None то
					result := истина;
					NextToken
				всё
			всё;
			возврат result
		кон GetRegister;

		проц GetRegisterList(перем registerList: мнвоНаБитахМЗ): булево;
		перем num: цел32;
		нач
			registerList := {};
			если token.видЛексемы = Scanner.ОткрывающаяФигурнаяСкобка то
				нцДо
					NextToken;
					если GetRegister(num) то
						если (num > 16) то
							Error(errorPosition, "invalid register in list (not yet implemented)")
						всё;
						включиВоМнвоНаБитах(registerList, num);
					всё;
				кцПри token.видЛексемы # Scanner.Запятая;
				если token.видЛексемы # Scanner.ЗакрываяющаяФигурнаяСкобка то
					Error(errorPosition, "'}' expected.")
				иначе
					NextToken;
					возврат истина
				всё;
			всё;
			возврат ложь;
		кон GetRegisterList;

		(** parse a special register name, along with fields **)
		проц GetSpecialRegisterWithFields(перем registerNumber: цел32; перем fields: мнвоНаБитахМЗ): булево;
		перем
			result: булево;
			i: размерМЗ;
			strings: Strings.StringArray;
		нач
			result := ложь;
			registerNumber := InstructionSet.None;
			fields := {};

			если token.видЛексемы = Scanner.Идентификатор то
				strings := Strings.Split(token.строкаИдентификатора, '_'); (* split the identifier at the underscore symbol *)
				если длинаМассива(strings) = 2 то
					если (strings[0]^ = "CPSR") или (strings[0]^ = "SPSR") то
						если strings[0]^ = "CPSR" то registerNumber := InstructionSet.CPSR
						иначе registerNumber := InstructionSet.SPSR
						всё;
						если strings[1]^ # "" то
							нцДля i := 0 до длинаМассива(strings[1]) - 1 делай
								просей strings[1][i] из
								| 'f': включиВоМнвоНаБитах(fields, InstructionSet.fieldF)
								| 's': включиВоМнвоНаБитах(fields, InstructionSet.fieldS)
								| 'x': включиВоМнвоНаБитах(fields, InstructionSet.fieldX)
								| 'c': включиВоМнвоНаБитах(fields, InstructionSet.fieldC)
								иначе
								всё
							кц;
							result := истина;
							NextToken
						всё
					всё
				всё
			всё;
			возврат result
		кон GetSpecialRegisterWithFields;

		(** parse a shift mode name **)
		проц GetShiftMode*(перем shiftModeNumber: цел32): булево;
		перем
			result: булево;
		нач
			shiftModeNumber := InstructionSet.None;
			result := ложь;
			если token.видЛексемы = Scanner.Идентификатор то
				shiftModeNumber := InstructionSet.ShiftModeNumberFromName(token.строкаИдентификатора);
				если shiftModeNumber # InstructionSet.None то
					result := истина;
					NextToken
				всё
			всё;
			возврат result
		кон GetShiftMode;

		(** parse a coprocessor name **)
		проц GetCoprocessor*(перем coprocessorNumber: цел32): булево;
		перем
			result: булево;
		нач
			coprocessorNumber := InstructionSet.None;
			result := ложь;

			если token.видЛексемы = Scanner.Идентификатор то
				coprocessorNumber := InstructionSet.CoprocessorNumberFromName(token.строкаИдентификатора);
				если coprocessorNumber # InstructionSet.None то
					result := истина;
					NextToken
				всё
			всё;

			возврат result
		кон GetCoprocessor;

		(* parse coprocessor opcode *)
		проц GetCoprocessorOpcode*(перем coprocessorOpcode: цел32): булево;
		перем
			result: булево;
		нач
			если (token.видЛексемы = Scanner.Число) и (token.numberType = Scanner.Integer) и (token.integer >= 0) и (token.integer <= 7) то
				coprocessorOpcode := цел32(token.integer);
				result := истина;
				NextToken
			иначе
				coprocessorOpcode := InstructionSet.None;
				result := ложь
			всё;
			возврат result
		кон GetCoprocessorOpcode;

		(** parse any expression that evaluates to a constant value **)
		проц GetPlainValue*(перем value: цел32): булево;
		перем
			assemblerResult: FoxAssembler.Result;
			result: булево;
		нач
			если Expression(assemblerResult, ложь) и ((assemblerResult.type = FoxAssembler.ConstantInteger) или (assemblerResult.type = FoxAssembler.Offset)) то
				value := цел32(assemblerResult.value);
				result := истина
			иначе
				value := 0;
				result := ложь
			всё;
			возврат result
		кон GetPlainValue;

		(** parse an ARM immediate value
			i.e., the '#'-sign followed by any expression that evaluates to a constant value
		**)
		проц GetImmediateValue*(перем immediateValue: цел32): булево;
		нач возврат ThisSymbol(Scanner.Unequal) и GetPlainValue(immediateValue)
		кон GetImmediateValue;

		проц {перекрыта}Instruction*(конст mnemonic: массив из симв8);
		перем
			instruction: InstructionSet.Instruction;
			operands: массив InstructionSet.MaxOperands из InstructionSet.Operand;
			position: Basic.Position;
			opCode, condition, i, operandNumber: цел32;
			flags: мнвоНаБитахМЗ;
			newOperandExpected: булево;
			result: FoxAssembler.Result;

			(** parse an operand
				- note that a subsequent comma is consumed as well
				- 'newOperandExpected' indicates if any more operands are expected
			**)
			проц ParseOperand;
			перем
				operand: InstructionSet.Operand;
				indexingMode, fields: мнвоНаБитахМЗ;
				registerNumber, offsetRegisterNumber, shiftModeNumber, shiftRegisterNumber, shiftImmediateValue, offsetImmediateValue, value: цел32;
				l0position: Basic.Position;
				isImmediateOffset, bracketIsOpen: булево;
				registerList: мнвоНаБитахМЗ;
			нач
				newOperandExpected := ложь;
				l0position := errorPosition;
				если operandNumber >= InstructionSet.MaxOperands то
					Error(l0position, "too many operands")
				иначе
					InstructionSet.InitOperand(operand);
					если ThisSymbol(Scanner.ОткрывающаяКвадратнаяСкобка) то
						bracketIsOpen := истина;
						(* memory operand *)
						indexingMode := {};
						если GetRegister(registerNumber) то
							если ThisSymbol(Scanner.ЗакрывающаяКвадратнаяСкобка) то
								bracketIsOpen := ложь;
								(* post indexing *)
								включиВоМнвоНаБитах(indexingMode, InstructionSet.PostIndexed)
							всё;
							если ExpectSymbol(Scanner.Запятая) то
								если GetImmediateValue(offsetImmediateValue) то
									(* immediate offset memory operand *)
									isImmediateOffset := истина;
									если матМодуль(offsetImmediateValue) < InstructionSet.Bits12 то
										если offsetImmediateValue >= 0 то
											включиВоМнвоНаБитах(indexingMode, InstructionSet.Increment)
										иначе
											включиВоМнвоНаБитах(indexingMode, InstructionSet.Decrement)
										всё;
										offsetImmediateValue := матМодуль(offsetImmediateValue)
									иначе
										Error(errorPosition, "immediate offset is out of range")
									всё
								иначе
									(* register offset memory operand *)
									isImmediateOffset := ложь;

									(* parse sign *)
									если ThisSymbol(Scanner.ЗнакПлюс) то
										включиВоМнвоНаБитах(indexingMode, InstructionSet.Increment)
									аесли ThisSymbol(Scanner.ЗнакМинус) то
										включиВоМнвоНаБитах(indexingMode, InstructionSet.Decrement)
									иначе
										Error(errorPosition, "plus or minus sign expected")
									всё;

									если ~error то
										(* parse offset register *)
										если GetRegister(offsetRegisterNumber) то
											shiftModeNumber := InstructionSet.None;
											shiftImmediateValue := 0;
											(* parse optional shift *)
											если GetShiftMode(shiftModeNumber) то
												если GetImmediateValue(shiftImmediateValue) то
													если shiftImmediateValue >= InstructionSet.Bits5 то
														Error(errorPosition, "immediate shift amount is out of range")
													всё
												иначе
													Error(errorPosition, "immediate shift amount expected")
												всё
											всё
										иначе
											Error(errorPosition, "register expected")
										всё
									всё
								всё
							всё;

							если bracketIsOpen то
								если ExpectSymbol(Scanner.ЗакрывающаяКвадратнаяСкобка) то
									если ThisSymbol(Scanner.ВосклицательныйЗнак) то
										(* preindexing *)
										включиВоМнвоНаБитах(indexingMode, InstructionSet.PreIndexed)
									всё
								всё
							всё
						аесли GetPlainValue(offsetImmediateValue) то
							(* pc label of the form [labelName], translated to [PC, #labelName - $ - 8] *)
							registerNumber := InstructionSet.PC;
							isImmediateOffset := истина;
							умень(offsetImmediateValue, 8);
							умень(offsetImmediateValue, code.pc(цел32));
							если матМодуль(offsetImmediateValue) < InstructionSet.Bits12 то
								если offsetImmediateValue >= 0 то
									включиВоМнвоНаБитах(indexingMode, InstructionSet.Increment)
								иначе
									включиВоМнвоНаБитах(indexingMode, InstructionSet.Decrement)
								всё;
								offsetImmediateValue := матМодуль(offsetImmediateValue)
							иначе
								Error(errorPosition, "immediate offset is out of range")
							всё;
							если ExpectSymbol(Scanner.ЗакрывающаяКвадратнаяСкобка) то
							всё;
						иначе
							Error(errorPosition, "register expected")
						всё;

						если ~error то
							если isImmediateOffset то
								InstructionSet.InitImmediateOffsetMemory(operand, registerNumber, offsetImmediateValue, indexingMode)
							иначе
								InstructionSet.InitRegisterOffsetMemory(operand, registerNumber, offsetRegisterNumber, shiftModeNumber, shiftImmediateValue, indexingMode);
							всё
						всё

					аесли GetSpecialRegisterWithFields(registerNumber, fields) то
						утв((registerNumber = InstructionSet.CPSR) или (registerNumber = InstructionSet.SPSR));
						InstructionSet.InitRegisterWithFields(operand, registerNumber, fields);

					аесли GetRegister(registerNumber) то
						(* register *)
						shiftModeNumber := InstructionSet.None; (* defaults *)
						shiftRegisterNumber := InstructionSet.None;
						shiftImmediateValue := 0;

						если ThisSymbol(Scanner.ВосклицательныйЗнак) то
							включиВоМнвоНаБитах(flags, InstructionSet.flagBaseRegisterUpdate);
						всё;
						если ThisSymbol(Scanner.Запятая) то
							(* parse shift mode *)
							если GetShiftMode(shiftModeNumber) то
								если shiftModeNumber # InstructionSet.shiftRRX то (* RRX shift amount is always 1 *)
									(* parse shift amount *)
									если ~GetRegister(shiftRegisterNumber) и ~GetImmediateValue(shiftImmediateValue) то
										Error(l0position, "invalid shift amount")
									всё
								всё
							иначе
								newOperandExpected := истина
							всё
						всё;
						если ~error то
							InstructionSet.InitRegister(operand, registerNumber, shiftModeNumber, shiftRegisterNumber, shiftImmediateValue)
						всё
					аесли GetRegisterList(registerList) то
						InstructionSet.InitRegisterList(operand, InstructionSet.R0, registerList);
						если ThisSymbol(Scanner.Arrow) то
							включиВоМнвоНаБитах(flags, InstructionSet.flagUserMode);
						всё;

					аесли GetCoprocessor(value) то
						(* coprocessor name *)
						InstructionSet.InitCoprocessor(operand, value)

					аесли GetCoprocessorOpcode(value) то (* integer constant in the range 0 .. 7 *)
						(* coprocessor opcode *)
						InstructionSet.InitOpcode(operand, value)

					аесли GetImmediateValue(value) то (* expression that evaluates to constant value starting with '#' *)
						(* ARM immediate value *)
						InstructionSet.InitImmediate(operand, value)

					аесли GetNonConstant(errorPosition,token.строкаИдентификатора, result) то
						InstructionSet.InitImmediate(operand,цел32(result.value));
						если result.fixup # НУЛЬ то
							InstructionSet.AddFixup(operand,result.fixup);
						всё;
						NextToken;
					аесли GetPlainValue(value) то (* expression that evaluates to constant value *)
						(* resolved label name *)
						InstructionSet.InitImmediate(operand, value)

					иначе
						Error(l0position, "invalid operand")
					всё;

					если ThisSymbol(Scanner.ВосклицательныйЗнак) то
						включиВоМнвоНаБитах(flags, InstructionSet.flagBaseRegisterUpdate);
					всё;

					если ~newOperandExpected то newOperandExpected := ThisSymbol(Scanner.Запятая) всё; (* a comma means that there is one more operand *)

					operands[operandNumber] := operand;

				всё
			кон ParseOperand;

		нач
			(*
			IF Trace THEN D.String("Instruction: "); D.String(mnemonic);  D.String(" "); D.Ln END;
			*)

			position := errorPosition;
			если InstructionSet.FindMnemonic(mnemonic, opCode, condition, flags) то

				(*IF Trace THEN
					D.String("    opCode="); D.Int(opCode, 0); D.Ln;
					D.String("    condition="); D.Int(condition, 0); D.Ln;
					D.String("    flags="); D.Set(flags); D.Ln;
				END;*)

				нцДля i := 0 до InstructionSet.MaxOperands - 1 делай
					InstructionSet.InitOperand(operands[i])
				кц;

				operandNumber := 0;
				если token.видЛексемы # Scanner.Ln то
					нцДо
						ParseOperand;

						увел(operandNumber);
					кцПри error или ~newOperandExpected;
				всё;

				если ~error то
					если ~InstructionSet.MakeInstruction(instruction, opCode, condition, flags, operands) то
						ErrorSS(position, "wrong instruction format: ", mnemonic);
					иначе
						если pass < FoxAssembler.MaxPasses то
							(* not last pass: only increment the current PC by 4 units *)
							section.resolved.SetPC(section.resolved.pc + 4)
						иначе
							(* last pass: emit the instruction *)
							если ~InstructionSet.EmitInstruction(instruction, section.resolved) то
								ErrorSS(position, "wrong instruction format (encoding failed): ", mnemonic);
							всё;
						всё
					всё
				всё
			иначе
				ErrorSS(position, "unknown mnemonic: ", mnemonic)
			всё
		кон Instruction;

	кон Assembler;

кон FoxARMAssembler.

System.FreeDownTo  FoxARMInstructionSet ~
Alwazs
