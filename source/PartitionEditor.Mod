модуль PartitionEditor; (** AUTHOR "staubesv"; PURPOSE "Partition Table Editor Application"; *)

использует
	Modules, Потоки, Commands, Strings, PartitionTable := PartitionEditorTable, PartitionEditorComponents,
	WMGraphics, WMWindowManager,  WMMessages, WMRestorable,
	WMComponents, WMStandardComponents, WMEditors, WMDialogs;

конст
	WindowTitle = "Partition Table Editor";
	WindowWidth = 680; WindowHeight = 140;

	Grey = цел32(0C0C0C0FFH);

тип

	KillerMsg = окласс
	кон KillerMsg;

	Window* = окласс (WMComponents.FormWindow)
	перем
		blockNumberEditor, deviceNameEditor : WMEditors.Editor;
		editor : PartitionEditorComponents.PartitionTableEditor;
		loadBtn, storeBtn, clearBtn : WMStandardComponents.Button;
		statusLabel : WMStandardComponents.Label;

		reader : Потоки.ЧтецИзСтроки;

		(**	Retrieve the blockNumber currently displayed in the blockNumberEditor. Returns FALSE
			if the string is no valid number *)
		проц GetBlockNumber(перем blockNumber : цел32) : булево;
		перем nbr : массив 16 из симв8;
		нач
			blockNumberEditor.GetAsString(nbr);
			reader.ОчистьСостояниеБуфера;
			reader.ПропустиБелоеПоле;
			reader.ПримиСтроку8ДляЧтения(nbr);
			reader.чЦел32(blockNumber, ложь);
			возврат (reader.кодВозвратаПоследнейОперации = Потоки.Успех) или (reader.кодВозвратаПоследнейОперации = Потоки.КонецФайла);
		кон GetBlockNumber;

		проц HandleButtons(sender, data : динамическиТипизированныйУкль);
		перем
			deviceName : массив 32 из симв8;
			pt : PartitionTable.PartitionTable; blockNumber : цел32; res : целМЗ;
			message: массив 256 из симв8;
		нач
			если (sender = storeBtn) то
				deviceNameEditor.GetAsString(deviceName);
				Strings.TrimWS(deviceName);
				если (deviceName # "") то
					pt := editor.Get();
					res := WMDialogs.Confirmation("Confirmation", "Do you really want to store the partition table?");
					если (res = WMDialogs.ResYes) то
						если GetBlockNumber(blockNumber) то
							PartitionTable.StorePartitionTable(deviceName, blockNumber, pt, res);
							если (res # PartitionTable.Ok) то
								GetErrorMessage("Could not write block! ", deviceName, res, message);
								 WMDialogs.Error(WindowTitle, message);
							всё;
						иначе
							WMDialogs.Error(WindowTitle, "Block number invalid!");
						всё;
					всё;
				иначе
					 WMDialogs.Error(WindowTitle, "No device name specified");
				всё;
			аесли (sender = loadBtn) то
				deviceNameEditor.GetAsString(deviceName);
				если (deviceName # "") то
					если GetBlockNumber(blockNumber) то
						Load(deviceName, blockNumber);
					иначе
						WMDialogs.Error(WindowTitle, "Block number invalid!");
					всё;
				иначе
					WMDialogs.Error(WindowTitle, "No device name specified");
				всё;
			аесли (sender = clearBtn) то
				PartitionTable.Clear(pt);
				editor.Set(pt);
			всё;
		кон HandleButtons;

		проц CreateForm() : WMComponents.VisualComponent;
		перем panel, toolbar : WMStandardComponents.Panel; label : WMStandardComponents.Label;
		нач
			нов(panel); panel.alignment.Set(WMComponents.AlignClient);

			нов(toolbar); toolbar.alignment.Set(WMComponents.AlignTop);
			toolbar.bounds.SetHeight(20);
			toolbar.fillColor.Set(Grey);
			panel.AddContent(toolbar);

			нов(label); label.alignment.Set(WMComponents.AlignLeft);
			label.bounds.SetWidth(60);
			label.caption.SetAOC(" Device:");
			toolbar.AddContent(label);

			нов(deviceNameEditor); deviceNameEditor.alignment.Set(WMComponents.AlignLeft);
			deviceNameEditor.bounds.SetWidth(100);
			deviceNameEditor.tv.showBorder.Set(истина);
			deviceNameEditor.multiLine.Set(ложь);
			deviceNameEditor.tv.textAlignV.Set(WMGraphics.AlignCenter);
			deviceNameEditor.fillColor.Set(WMGraphics.White);
			toolbar.AddContent(deviceNameEditor);

			нов(label); label.alignment.Set(WMComponents.AlignLeft);
			label.bounds.SetWidth(80);
			label.caption.SetAOC(" Block (LBA):");
			toolbar.AddContent(label);

			нов(blockNumberEditor); blockNumberEditor.alignment.Set(WMComponents.AlignLeft);
			blockNumberEditor.bounds.SetWidth(100);
			blockNumberEditor.tv.showBorder.Set(истина);
			blockNumberEditor.multiLine.Set(ложь);
			blockNumberEditor.tv.textAlignV.Set(WMGraphics.AlignCenter);
			blockNumberEditor.fillColor.Set(WMGraphics.White);
			toolbar.AddContent(blockNumberEditor);

			нов(loadBtn); loadBtn.alignment.Set(WMComponents.AlignLeft);
			loadBtn.caption.SetAOC("Load");
			loadBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(loadBtn);

			нов(storeBtn); storeBtn.alignment.Set(WMComponents.AlignLeft);
			storeBtn.caption.SetAOC("Store");
			storeBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(storeBtn);

			нов(clearBtn); clearBtn.alignment.Set(WMComponents.AlignRight);
			clearBtn.caption.SetAOC("Clear");
			clearBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(clearBtn);

			нов(statusLabel); statusLabel.alignment.Set(WMComponents.AlignBottom);
			statusLabel.bounds.SetHeight(20);
			statusLabel.fillColor.Set(Grey);

			нов(editor); editor.alignment.Set(WMComponents.AlignBottom);
			editor.bounds.SetHeight(120);
			editor.changeHandler := HandleChange;
			panel.AddContent(editor);

			возврат panel;
		кон CreateForm;

		проц &New*(context : WMRestorable.Context);
		нач
			IncCount;
			нов(reader, 256);

			Init(WindowWidth, WindowHeight, ложь);

			SetTitle(Strings.NewString(WindowTitle));
			SetContent(CreateForm());

			если (context # НУЛЬ) то
				WMRestorable.AddByContext(сам, context);
			иначе
				WMWindowManager.DefaultAddWindow(сам)
			всё;
		кон New;

		проц Load(deviceName : массив из симв8; block : цел32);
		перем pt : PartitionTable.PartitionTable; blockStr : массив 16 из симв8; message: массив 256 из симв8; res : целМЗ;
		нач
			Strings.TrimWS(deviceName);
			deviceNameEditor.SetAsString(deviceName);
			Strings.IntToStr(block, blockStr);
			blockNumberEditor.SetAsString(blockStr);
			если (deviceName # "") то
				если GetBlockNumber(block) то
					pt := PartitionTable.LoadPartitionTable(deviceName, block, res);
					если (res = PartitionTable.Ok) или (res = PartitionTable.NoSignature) то
						editor.Set(pt);
					иначе
						GetErrorMessage("Could not load block!", deviceName, res, message);
						WMDialogs.Error(WindowTitle, message);
					всё;
				иначе
					WMDialogs.Error(WindowTitle, "Block number invalid!");
				всё;
			иначе
				WMDialogs.Error(WindowTitle, "No device name specified");
			всё;
		кон Load;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount
		кон Close;

		(* Since the content of this window is not scalable, we don't allow window resizing *)
		проц {перекрыта}Resizing*(перем width, height : размерМЗ);
		нач
			width := WindowWidth; height := WindowHeight;
		кон Resizing;

		проц HandleChange(changeType : цел32; перем partition : PartitionTable.Partition);
		перем deviceName : массив 32 из симв8; res : целМЗ;
		нач
			deviceNameEditor.GetAsString(deviceName);
			PartitionTable.Changed(changeType, partition, deviceName, res);
			если (res # PartitionTable.Ok) то
				WMDialogs.Warning(WindowTitle, "Field adjustion failed");
			всё;
		кон HandleChange;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть KillerMsg) то Close
				аесли (x.ext суть WMRestorable.Storage) то
					x.ext(WMRestorable.Storage).Add("Partition Table Editor", "PartitionEditor.Restore", сам, НУЛЬ);
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

	кон Window;

перем
	nofWindows : цел32;

проц GetErrorMessage(конст string1, devicename : массив из симв8; res : целМЗ; перем message: массив из симв8);
перем nbr : массив 8 из симв8;
нач
	копируйСтрокуДо0(string1, message);
	просей res из
		|PartitionTable.DeviceNotFound:
			Strings.Append(message, " (Device '"); Strings.Append(message, devicename); Strings.Append(message, "' not found)");
		|PartitionTable.BlocksizeNotSupported:
			Strings.Append(message, " (Block size not supported)");
	иначе
		Strings.Append(message, " (res: ");
		Strings.IntToStr(res, nbr); Strings.Append(message, nbr); Strings.Append(message, ")");
	всё;
кон GetErrorMessage;

(* open an editor window *)
проц Open*(context : Commands.Context); (** DeviceName [Block] ~ *)
перем window : Window; deviceName : массив 128 из симв8; block : цел32;
нач
	deviceName := ""; block := 0;
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(deviceName);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(block, ложь);
	нов(window, НУЛЬ);
	если (deviceName # "") то
		window.Load(deviceName, block);
	всё;
кон Open;

(* if system context is stored via menu system -> SaveDesktop, then restore the window like it was at next bootup *)
проц Restore*(context : WMRestorable.Context);
перем window : Window;
нач
	нов(window, context)
кон Restore;

(* increase the number of open windows, for window book kepping and correct cleanup *)
проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

(* decrease the number of open windows, for window book kepping and correct cleanup *)
проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

(* Cleanup: close all open windows when this module gets unloaded or when the system goes down *)
проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон PartitionEditor.

PC.Compile \s PartitionEditorTable.Mod PartitionEditorComponents.Mod PartitionEditor.Mod ~

System.Free PartitionEditor PartitionEditorComponents PartitionEditorTable ~

PartitionEditor.Open ~

VirtualDisks.Create Test.Dsk 2880 512 80 2 18 ~  1.44MB floppy disk :)

VirtualDisks.Install Test Test.Dsk  512 80 2 18 ~

VirtualDisks.Uninstall Test ~

ZipTool.Add PartitionEditor.zip
	PartitionEditorTable.Mod
	PartitionEditorComponents.Mod
	PartitionEditor.Mod
~
