модуль BootShell; (** AUTHOR "staubesv"; PURPOSE "Simple VGA text mode shell"; *)

использует
	НИЗКОУР, ЛогЯдра, Machine, Modules, Потоки, Commands, Inputs, Strings, Locks;

конст
	Version = "A2 Bootshell v1.0";

	LineWidth = 80; TraceHeight = 25;
	TraceBase = 0B8000H; (* default screen buffer *)

	BufferHeight = 2048; (* lines *)

	BufferSize = BufferHeight * LineWidth; (* characters *)

	TAB = 09X;
	CR = 0DX;
	LF = 0AX;
	SPACE = " ";

	Mode_Insert = 0;
	Mode_Overwrite = 1;

	Black = 0;
	Blue = 1;
	Green = 2;
	Cyan = 3;
	Red = 4;
	Magenta = 5;
	Brown = 6;
	White = 7;
	Gray = 8;
	LightBlue = 9;
	LightGreen = 10;
	LightCyan = 11;
	LightRed = 12;
	LightMagenta = 13;
	Yellow = 14;
	BrightWhite = 15;

тип

	(* Copied from Shell.mod *)
	CommandsString = укль на запись
		prev, next: CommandsString;
		string: Strings.String;
	кон;

	CommandHistoryObject = окласс
	перем
		first, current: CommandsString;

		проц GetNextCommand() : Strings.String;
		перем string : Strings.String;
		нач
			если first # НУЛЬ то
				если current = НУЛЬ то current := first иначе current := current.next всё;
				string := current.string;
			иначе
				string := НУЛЬ;
			всё;
			возврат string;
		кон GetNextCommand;

		проц GetPreviousCommand() : Strings.String;
		перем string : Strings.String;
		нач
			если first # НУЛЬ то
				если current = НУЛЬ то current := first.prev иначе current := current.prev всё;
				string := current.string;
			иначе
				string := НУЛЬ;
			всё;
			возврат string;
		кон GetPreviousCommand;

		проц AddCommand(string : Strings.String);
		перем command: CommandsString;
		нач
			утв((string # НУЛЬ) и (string^ # ""));
			command := first;
			если command # НУЛЬ то
				нцПока (command.string^ # string^) и (command.next # first) делай command := command.next кц;
				если command.string^ # string^ то command := НУЛЬ всё
			всё;
			если command # НУЛЬ то
				если first = command то first := command.next всё;
				command.prev.next := command.next;
				command.next.prev := command.prev;
			иначе
				нов (command);
				command.string := string;
			всё;
			если first = НУЛЬ то
				first := command; first.next := first; first.prev := first
			иначе
				command.prev := first.prev; command.next := first;
				first.prev.next := command; first.prev := command;
			всё;
			current := НУЛЬ;
		кон AddCommand;

		проц &Init*;
		нач first := НУЛЬ; current := НУЛЬ;
		кон Init;

	кон CommandHistoryObject;

тип

	Character = запись
		ch : симв8;
		color : цел8;
	кон;

	Line = массив LineWidth из Character;

	TextBuffer = окласс
	перем
		defaultColor : цел8;
		currentColor : цел8;

		(* ring buffer of lines *)
		lines : массив BufferHeight из Line;

		(* index of first line in ring buffer *)
		firstLine, lastLine : цел32;

		(* index of line currently shown on top of the display *)
		firstLineShown : цел32;

		(* start and end of currently edited text *)
		editStartPosition, editEndPosition : цел32;

		(* character position of cursor *)
		cursorPosition : цел32;

		mode : цел32;

		lock : Locks.RecursiveLock;

		проц &Init*;
		нач
			mode := Mode_Insert;
			нов(lock);
			lock.Acquire;
			Clear;
			lock.Release;
		кон Init;

		проц Clear;
		перем i : цел32;
		нач
			утв(lock.HasLock());
			firstLine := 0; lastLine := 0;
			firstLineShown := 0;
			cursorPosition := 0;
			editStartPosition := 0; editEndPosition := 0;
			SetColor(White, Black);
			defaultColor := White + 10H * Black;
			нцДля i := 0 до длинаМассива(lines)-1 делай
				ClearLine(lines[i], 0, LineWidth-1, defaultColor);
			кц;
			Invalidate(сам);
		кон Clear;

		проц SetColor(foreground, background : цел8);
		нач
			currentColor := foreground + 10H * background;
		кон SetColor;

		проц SetEditStart;
		нач
			editStartPosition := cursorPosition;
			editEndPosition := cursorPosition;
		кон SetEditStart;

		проц Send( конст data: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем i : размерМЗ;
		нач
			lock.Acquire;
			нцДля i := ofs до ofs + len - 1 делай
				CharInternal(data[i]);
			кц;
			CheckVisibility;
			Invalidate(сам);
			lock.Release;
			res := Потоки.Успех;
		кон Send;

		проц String(конст string : массив из симв8);
		перем i : цел32;
		нач
			lock.Acquire;
			i := 0;
			нцПока (i < длинаМассива(string)) и (string[i] # 0X) делай
				CharInternal(string[i]);
				увел(i);
			кц;
			CheckVisibility;
			Invalidate(сам);
			lock.Release;
		кон String;

		проц Char(ch : симв8);
		нач
			lock.Acquire;
			CharInternal(ch);
			CheckVisibility;
			Invalidate(сам);
			lock.Release;
		кон Char;

		проц CheckVisibility;
		нач
			утв(lock.HasLock());
			если (Difference(lastLine, firstLineShown, длинаМассива(lines)) > TraceHeight - 1) то
				firstLineShown := Subtract(lastLine, TraceHeight - 1, длинаМассива(lines));
				Invalidate(сам);
			всё;
		кон CheckVisibility;

		проц NextLine;
		нач
			утв(lock.HasLock());
			lastLine := Add(lastLine, 1, BufferHeight);
			ClearLine(lines[lastLine], 0, LineWidth-1, defaultColor);
			если (lastLine = firstLine) то
				firstLine := Add(firstLine, 1, BufferHeight);
				если (firstLineShown = lastLine) то
					firstLineShown := firstLine;
				всё;
			всё;
		кон NextLine;

		проц MoveCharactersToRight;
		перем current, previous : цел32;
		нач
			утв(editStartPosition # editEndPosition);
			если (editEndPosition = LineWidth-1) то (* reserve new line in advance *)
				NextLine;
			всё;
			editEndPosition := Add(editEndPosition, 1, BufferSize);
			current := editEndPosition;
			нцПока (current # cursorPosition) делай
				previous := Subtract(current, 1, BufferSize);
				lines[current DIV LineWidth][current остОтДеленияНа LineWidth] := lines[previous DIV LineWidth][previous остОтДеленияНа LineWidth];
				current := previous;
			кц;
		кон MoveCharactersToRight;

		проц MoveCharactersToLeft;
		перем current, next : цел32;
		нач
			утв(editStartPosition # editEndPosition);
			если (editEndPosition = 0) то (* line will be removed *)
				lastLine := Subtract(lastLine, 1, длинаМассива(lines));
			всё;
			current := cursorPosition;
			нцДо
				next := Add(current, 1, BufferSize);
				lines[current DIV LineWidth][current остОтДеленияНа LineWidth] := lines[next DIV LineWidth][next остОтДеленияНа LineWidth];
				current := next;
			кцПри (next = editEndPosition);
			editEndPosition := Subtract(editEndPosition, 1, BufferSize);
		кон MoveCharactersToLeft;

		проц CharInternal(ch : симв8);
		перем index : цел32;
		нач
			утв(lock.HasLock());
			если (ch = CR) то (* ignore *)
			аесли (ch = LF) то
				ClearLine(lines[cursorPosition DIV LineWidth], cursorPosition остОтДеленияНа LineWidth, LineWidth-1, currentColor);
				NextLine;
				cursorPosition := Add(cursorPosition, LineWidth - (cursorPosition остОтДеленияНа LineWidth), BufferSize);
				editEndPosition := cursorPosition;
			аесли (SPACE <= ch) и (кодСимв8(ch) < 128) то
				index := cursorPosition DIV LineWidth;
				если (cursorPosition = editEndPosition) то (* append *)
					утв(index = lastLine);
					lines[index][cursorPosition остОтДеленияНа LineWidth].ch := ch;
					lines[index][cursorPosition остОтДеленияНа LineWidth].color := currentColor;
					cursorPosition := Add(cursorPosition, 1, BufferSize);
					editEndPosition := cursorPosition;
					если (cursorPosition DIV LineWidth # index) то
						NextLine;
					всё;
				иначе
					если (mode # Mode_Overwrite) то
						MoveCharactersToRight;
					всё;
					lines[index][cursorPosition остОтДеленияНа LineWidth].ch := ch;
					lines[index][cursorPosition остОтДеленияНа LineWidth].color := currentColor;
					cursorPosition := Add(cursorPosition, 1, BufferSize);
				всё;
			всё;
		кон CharInternal;

		проц DeleteCurrentLine;
		перем i : цел32;
		нач
			lock.Acquire;
			i := editStartPosition;
			нц
				lines[i DIV LineWidth][i остОтДеленияНа LineWidth].ch := SPACE;
				если (i = editEndPosition) то прервиЦикл; всё;
				увел(i);
			кц;
			cursorPosition := editStartPosition;
			editEndPosition := editStartPosition;
			lastLine := editStartPosition DIV LineWidth;
			lock.Release;
		кон DeleteCurrentLine;

		проц GetCurrentLine() : Strings.String;
		перем string : Strings.String; i, length : цел32;
		нач
			lock.Acquire;
			length := Difference(editEndPosition, editStartPosition, BufferSize);
			нов(string, length + 1);
			i := 0;
			нцПока (i < length - 1) делай
				string[i] := lines[(editStartPosition + i) DIV LineWidth][(editStartPosition + i) остОтДеленияНа LineWidth].ch;
				увел(i);
			кц;
			string[length-1] := 0X;
			lock.Release;
			возврат string;
		кон GetCurrentLine;

		проц Home;
		нач
			lock.Acquire;
			если (cursorPosition # editStartPosition) то
				cursorPosition := editStartPosition;
				Invalidate(сам);
			всё;
			lock.Release;
		кон Home;

		проц End;
		нач
			lock.Acquire;
			если (cursorPosition # editEndPosition) то
				cursorPosition := editEndPosition;
				Invalidate(сам);
			всё;
			lock.Release;
		кон End;

		проц Backspace;
		нач
			lock.Acquire;
			если (cursorPosition # editStartPosition) то
				cursorPosition := Subtract(cursorPosition, 1, BufferSize);
				MoveCharactersToLeft;
				Invalidate(сам);
			всё;
			lock.Release;
		кон Backspace;

		проц Delete;
		нач
			lock.Acquire;
			если (cursorPosition # editEndPosition) то
				MoveCharactersToLeft;
				Invalidate(сам);
			всё;
			lock.Release;
		кон Delete;

		проц ScrollUp(nofLines : цел32);
		перем d : цел32;
		нач
			lock.Acquire;
			d := Difference(firstLineShown, firstLine, длинаМассива(lines));
			nofLines := матМинимум(nofLines, d - 1);
			если (nofLines > 0) то
				firstLineShown := Subtract(firstLineShown, nofLines, длинаМассива(lines));
			всё;
			Invalidate(сам);
			lock.Release;
		кон ScrollUp;

		проц ScrollDown(nofLines : цел32);
		перем d : цел32;
		нач
			lock.Acquire;
			d := Difference(lastLine, firstLineShown, длинаМассива(lines));
			nofLines := матМинимум(nofLines, d - 1);
			если (nofLines > 0) то
				firstLineShown := Add(firstLineShown, nofLines, длинаМассива(lines));
			всё;
			Invalidate(сам);
			lock.Release;
		кон ScrollDown;

		проц CursorLeft;
		перем oldCursorPosition : цел32;
		нач
			lock.Acquire;
			если (cursorPosition # editStartPosition) то
				oldCursorPosition := cursorPosition;
				cursorPosition := Subtract(cursorPosition, 1, BufferSize);
				Invalidate(сам);
			всё;
			lock.Release;
		кон CursorLeft;

		проц CursorRight;
		перем oldCursorPosition : цел32;
		нач
			lock.Acquire;
			если (cursorPosition # editEndPosition) то
				oldCursorPosition := cursorPosition;
				cursorPosition := Add(cursorPosition, 1, BufferSize);
				Invalidate(сам);
			всё;
			lock.Release;
		кон CursorRight;

		проц Dump(out : Потоки.Писарь);
		перем i, j : цел32;
		нач
			утв(out # НУЛЬ);
			lock.Acquire;
			out.пСтроку8("firstLine = "); out.пЦел64(firstLine, 0); out.пСтроку8(", lastLine = "); out.пЦел64(lastLine, 0); out.пВК_ПС;
			out.пСтроку8("firstLineShown = "); out.пЦел64(firstLineShown, 0); out.пВК_ПС;
			out.пСтроку8("cursorPosition = "); out.пЦел64(cursorPosition, 0); out.пВК_ПС;
			out.пСтроку8("editStartPosition = "); out.пЦел64(editStartPosition, 0); out.пСтроку8(", editEndPosition = "); out.пЦел64(editEndPosition, 0); out.пВК_ПС;
			i := firstLine;
			нц
				нцДля j := 0 до LineWidth-1 делай
					out.пСимв8(lines[i остОтДеленияНа длинаМассива(lines)][j].ch);
				кц;
				out.пВК_ПС;
				если (i = lastLine) то прервиЦикл; всё;
				увел(i);
			кц;
			out.пВК_ПС;
			lock.Release;
		кон Dump;

	кон TextBuffer;

тип

	Shell = окласс(Inputs.Sink)
	перем
		textBuffer : TextBuffer;
		history : CommandHistoryObject;

		проц &Init;
		нач
			нов(textBuffer);
			textBuffer.lock.Acquire;
			textBuffer.SetColor(Yellow, Black);
			textBuffer.String(Version);
			textBuffer.Char(LF);
			Prompt;
			textBuffer.SetEditStart;
			textBuffer.lock.Release;
			нов(history);
			Inputs.keyboard.Register(сам);
		кон Init;

		проц {перекрыта}Handle*(перем msg: Inputs.Message);
		нач
			если (msg суть Inputs.KeyboardMsg) и (msg(Inputs.KeyboardMsg).flags * {Inputs.Release} = {}) то
				просейТип msg:Inputs.KeyboardMsg делай
					если (msg.keysym = Inputs.KsPageUp) то
						если (msg.flags * Inputs.Shift # {}) то textBuffer.ScrollUp(1); иначе textBuffer.ScrollUp(TraceHeight); всё;
					аесли (msg.keysym = Inputs.KsPageDown) то
						если (msg.flags * Inputs.Shift # {}) то textBuffer.ScrollDown(1); иначе textBuffer.ScrollDown(TraceHeight); всё;
					аесли (msg.keysym = Inputs.KsLeft) то
						textBuffer.CursorLeft;
					аесли (msg.keysym = Inputs.KsRight) то
						textBuffer.CursorRight;
					аесли (msg.keysym = Inputs.KsUp) то
						CommandHistory(ложь);
					аесли (msg.keysym = Inputs.KsDown) то
						CommandHistory(истина);
					аесли (msg.keysym = Inputs.KsHome) то
						textBuffer.Home;
					аесли (msg.keysym = Inputs.KsEnd) то
						textBuffer.End;
					аесли (msg.keysym = Inputs.KsDelete) то
						textBuffer.Delete;
					аесли (msg.keysym = Inputs.KsBackSpace) то
						textBuffer.Backspace;
					аесли (msg.keysym = Inputs.KsReturn) то
						textBuffer.lock.Acquire;
						textBuffer.cursorPosition := textBuffer.editEndPosition;
						textBuffer.Char(LF);
						textBuffer.lock.Release;
						Execute;
						textBuffer.lock.Acquire;
						textBuffer.Char(LF);
						Prompt;
						textBuffer.SetEditStart;
						textBuffer.lock.Release;
					аесли (msg.ch = LF) или ((SPACE <= msg.ch) и (кодСимв8(msg.ch) < 128)) то
						textBuffer.Char(msg.ch);
					всё;
				всё;
			всё;
		кон Handle;

		проц CommandHistory(next : булево);
		перем string : Strings.String;
		нач
			textBuffer.lock.Acquire;
			если next то
				string := history.GetNextCommand();
			иначе
				string := history.GetPreviousCommand();
			всё;
			если (string # НУЛЬ) то
				textBuffer.DeleteCurrentLine;
				textBuffer.String(string^);
			всё;
			textBuffer.lock.Release;
		кон CommandHistory;

		проц Prompt;
		нач
			textBuffer.SetColor(LightBlue, Black);
			textBuffer.String("A2>");
			textBuffer.SetColor(White, Black);
		кон Prompt;

		проц Execute;
		перем
			context : Commands.Context; writer : Потоки.Писарь; arg : Потоки.ЧтецИзСтроки;
			commandLine : Strings.String;
			nbr : массив 8 из симв8;
			msg, command : массив 128 из симв8;
			i, length : размерМЗ; res : целМЗ;
		нач
			commandLine := textBuffer.GetCurrentLine();
			Strings.TrimWS(commandLine^);

			если (commandLine^ # "") то
				history.AddCommand(commandLine);
			всё;

			length := Strings.Length(commandLine^);

			i := 0;
			нцПока (i < length) и ~IsWhitespace(commandLine[i]) и (i < длинаМассива(command) - 1) делай
				command[i] := commandLine[i];
				увел(i);
			кц;
			command[i] := 0X;

			если (command = "exit") то
				Close;
			аесли (command = "clear") то
				textBuffer.lock.Acquire;
				textBuffer.Clear;
				textBuffer.lock.Release;
			аесли (command = "version") то
				textBuffer.lock.Acquire;
				textBuffer.String(Version);
				textBuffer.lock.Release;
			аесли (command = "") то
				(* ignore *)
			иначе
				если (i < length) то
					нов(arg, length - i);
					arg.ПримиСрезСтроки8ВСтрокуДляЧтения(commandLine^, i, length - i);
				иначе
					нов(arg, 1); arg.ПримиСтроку8ДляЧтения("");
				всё;

				нов(writer, textBuffer.Send, 256);
				нов(context, НУЛЬ, arg, writer, writer, сам);

				Commands.Activate(command, context, {Commands.Wait}, res, msg);

				context.out.ПротолкниБуферВПоток; context.error.ПротолкниБуферВПоток;

				если (res # Commands.Ok) то
					textBuffer.lock.Acquire;
					textBuffer.SetColor(Red, Black);
					textBuffer.String("Command execution error, res = ");
					Strings.IntToStr(res, nbr);
					textBuffer.String(nbr);
					textBuffer.String(" ("); textBuffer.String(msg); textBuffer.String(")");
					textBuffer.Char(LF);
					textBuffer.SetColor(White, Black);
					textBuffer.lock.Release;
				всё;
			всё;
		кон Execute;

		проц Quit;
		нач
			Inputs.keyboard.Unregister(сам);
		кон Quit;

	кон Shell;

перем
	shell : Shell;

проц Subtract(position, value, bufferSize : цел32) : цел32;
перем result : цел32;
нач
	утв((0 <= position) и (position < bufferSize));
	value := value остОтДеленияНа bufferSize;
	если (position - value >= 0) то result := position - value;
	иначе result := bufferSize - 1 - (value - position);
	всё;
	утв((0 <= result) и (result < bufferSize));
	возврат result;
кон Subtract;

проц Add(position, value, bufferSize : цел32) : цел32;
перем result : цел32;
нач
	утв((0 <= position) и (position < bufferSize));
	result := (position + value) остОтДеленияНа bufferSize;
	утв((0 <= result) и (result < bufferSize));
	возврат result;
кон Add;

проц Difference(end, start, bufferSize : цел32) : цел32;
перем result : цел32;
нач
	если (end >= start) то
		result := end - start + 1;
	иначе
		result := (end + 1) + (bufferSize - start + 1);
	всё;
	возврат result;
кон Difference;

проц ClearLine(перем line : Line; from, to : цел32; color : цел8);
перем i : цел32;
нач
	утв((0 <= from) и (from < LineWidth));
	утв((0 <= to) и (to < LineWidth));
	нцДля i := from до to делай
		line[i].ch := SPACE;
		line[i].color := color;
	кц;
кон ClearLine;

проц IsWhitespace(ch : симв8) : булево;
нач
	возврат (ch = SPACE) или (ch = TAB) или (ch = CR) или (ch = LF);
кон IsWhitespace;

проц Invalidate(textBuffer : TextBuffer);
перем offset, index, i, nofLines : цел32; line : Line; character : Character; ch : симв8;
нач
	утв(textBuffer # НУЛЬ);
	утв(textBuffer.lock.HasLock());
	offset := 0;
	nofLines := 1;
	index := textBuffer.firstLineShown;
	нц
		line := textBuffer.lines[index остОтДеленияНа длинаМассива(textBuffer.lines)];
		нцДля i := 0 до LineWidth-1 делай
			character := line[i];
			если (character.ch = TAB) то ch := SPACE; иначе ch := character.ch; всё;
			НИЗКОУР.запиши16битПоАдресу(TraceBase + offset, кодСимв8(ch) + 100H * character.color);
			увел(offset, 2);
		кц;
		если (index = textBuffer.lastLine) или (nofLines = TraceHeight) то прервиЦикл; всё;
		увел(index);
		увел(nofLines);
	кц;
	нцПока (nofLines < TraceHeight) делай
		нцДля i := 0 до LineWidth-1 делай
			НИЗКОУР.запиши16битПоАдресу(TraceBase + offset, кодСимв8(SPACE));
			увел(offset, 2);
		кц;
		увел(nofLines);
	кц;
	UpdateCursor(textBuffer);
кон Invalidate;

проц Open*;
нач {единолично}
	если (shell = НУЛЬ) то
		ЛогЯдра.пСтроку8("BootShell: Starting shell..."); ЛогЯдра.пВК_ПС;
		нов(shell);
	всё;
кон Open;

проц Close*;
нач {единолично}
	если (shell # НУЛЬ) то
		shell.Quit;
		shell := НУЛЬ;
	всё;
кон Close;

проц Dump*(context : Commands.Context);
нач {единолично}
	если (shell # НУЛЬ) то
		shell.textBuffer.Dump(context.out);
	иначе
		context.out.пСтроку8("BootShell not started."); context.out.пВК_ПС;
	всё;
кон Dump;

проц UpdateCursor(textBuffer : TextBuffer);
перем cursorLocation : цел32;
нач
	утв(textBuffer # НУЛЬ);
	утв(textBuffer.lock.HasLock());
	cursorLocation := Subtract(textBuffer.cursorPosition, textBuffer.firstLineShown * LineWidth, BufferSize);
	Machine.Portout8(3D4H, 0EX); (* Select cursor location high register *)
	Machine.Portout8(3D5H, симв8ИзКода(cursorLocation DIV 100H));
	Machine.Portout8(3D4H, 0FX); (* Select cursor location low register *)
	Machine.Portout8(3D5H, симв8ИзКода(cursorLocation остОтДеленияНа 100H));
кон UpdateCursor;

проц Cleanup;
нач
	Close;
кон Cleanup;

проц Init;
перем value : массив 32 из симв8;
нач
	Machine.GetConfig("Diagnosis", value);
	Strings.TrimWS(value);
	если (value = "1") то
		Open;
		нач {единолично} дождись(shell = НУЛЬ); кон;
	всё;
кон Init;

нач
	Modules.InstallTermHandler(Cleanup);
	Init;
кон BootShell.

System.DoCommands

	Linker.Link \P../Test/ \.Obx ../Test/IDE.Bin 0100000H 1000H Kernel Traps
		ATADisks DiskVolumes DiskFS Keyboard BootShell BootConsole ~

	VirtualDisks.Install VM0 E:/Private/A2/WinAos/VM/Old-f001.vmdk ~

	Partitions.UpdateBootFile VM0#1 ../Test/IDE.Bin ~

	VirtualDisks.Uninstall VM0 ~
~~~

System.DoCommands

	FSTools.DeleteFiles BootShell.img ~

	VirtualDisks.Create BootShell.img 2048 512 ~
	VirtualDisks.Install -c=80 -h=2 -s=18 -b=512 VDISK0 BootShell.img ~

	Linker.Link \P../Test/ \.Obx ../Test/CD.Bin 0100000H 1000H Kernel Traps ProcessInfo System Keyboard BootShell BootConsole ~

	Partitions.Format VDISK0#0 AosFS 640 ../Test/CD.Bin ~

	Partitions.SetConfig VDISK0#0
		TraceMode="4" TracePort="1" TraceBPS="115200"
		ExtMemSize="64"
		MaxProcs="-1"
		Diagnosis="1"
	~
	VirtualDisks.Uninstall VDISK0 ~

	IsoImages.Make A2Diagnosis.iso BootShell.img ~

~~
