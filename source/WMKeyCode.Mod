модуль WMKeyCode;	(** AUTHOR "TF"; PURPOSE "Display key code of a pressed key"; *)

(* WMKeyCode.Open ~*)
использует
	Commands, WMRestorable, WMMessages, WMGraphics, Inputs, ЛогЯдра,
	Modules, WMRectangles, Strings,
	WM := WMWindowManager;

тип
	Window = окласс(WM.BufferWindow)

		проц &New*(c : WMRestorable.Context);
		нач
			Init(300, 32, ложь);
			canvas.Fill(WMRectangles.MakeRect(0, 0, GetWidth(), GetHeight()), WMGraphics.White, WMGraphics.ModeCopy);
			Invalidate(WMRectangles.MakeRect(0, 0, GetWidth(), GetHeight()));
			canvas.SetColor(WMGraphics.Black);
			SetTitle(Strings.NewString("См. коды клавиш в логе ядра"));
			если c # НУЛЬ то
				WMRestorable.AddByContext(сам, c)
			иначе
				WM.DefaultAddWindow(сам)
			всё;
		кон New;

		проц {перекрыта}KeyEvent*(ucs : размерМЗ; flags1 : мнвоНаБитахМЗ; keysym : размерМЗ);
		перем t : массив 128 из симв8;
			font : WMGraphics.Font;
			x, y : размерМЗ;

		нач
			font := canvas.GetFont();
			x := 3; y := 14;
			canvas.Fill(WMRectangles.MakeRect(0, 0, GetWidth(), GetHeight()), WMGraphics.White, WMGraphics.ModeCopy);
			если keysym # 0 то
				ЛогЯдра.пСтроку8("Символ: ");
				GetFlagsString(flags1, t); ЛогЯдра.пСтроку8(t);
				ЛогЯдра.пСтроку8(" '"); t[0] := симв8ИзКода(ucs); t[1] := 0X; ЛогЯдра.пСтроку8(t); ЛогЯдра.пСтроку8("' (");
				Strings.IntToHexStr(ucs, 0, t); ЛогЯдра.пСтроку8(t); ЛогЯдра.пСтроку8(")");

				x := 3; y := 14 + 14;
				ЛогЯдра.пСтроку8("; логическийКодКлавишиЯОС: ");
				Strings.IntToHexStr(keysym, 0, t);
				ЛогЯдра.пСтроку8(t);
				ЛогЯдра.пСтроку8(" ("); GetKeysymString(keysym, t); ЛогЯдра.пСтроку8(t); ЛогЯдра.пСтроку8(")");
				ЛогЯдра.пВК_ПС;
			всё;
			Invalidate(WMRectangles.MakeRect(0, 0, GetWidth(), GetHeight()));
		кон KeyEvent;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть WMRestorable.Storage) то
					x.ext(WMRestorable.Storage).Add("WMKeyCode", "WMKeyCode.Restore", сам, НУЛЬ)
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

		проц {перекрыта}Close*;
		нач
			Close^;
			winstance := НУЛЬ
		кон Close;

	кон Window;

перем
	winstance : Window;
	manager : WM.WindowManager;

проц GetKeysymString(keysym : размерМЗ; перем string: массив из симв8);
нач
	если keysym = Inputs.KsBackSpace то string := "BackSpace";
	аесли keysym = Inputs.KsTab то string := "Tab";
	аесли keysym = Inputs.KsReturn  то string := "Return";
	аесли keysym = Inputs.KsPause то string := "Pause";
	аесли keysym = Inputs.KsScrollLock то string := "ScrollLock";
	аесли keysym = Inputs.KsSysReq то string := "SysReq";
	аесли keysym = Inputs.KsEscape то string := "Escape";
	аесли keysym = Inputs.KsDelete то string := "Delete";
	аесли keysym = Inputs.KsHome то string := "Home";
	аесли keysym = Inputs.KsLeft то string := "Left";
	аесли keysym = Inputs.KsUp то string := "Up";
	аесли keysym = Inputs.KsRight то string := "Right";
	аесли keysym = Inputs.KsDown то string := "Down";
	аесли keysym = Inputs.KsPageUp то string := "PageUp";
	аесли keysym = Inputs.KsPageDown то string := "PageDown";
	аесли keysym = Inputs.KsEnd то string := "End";
	аесли keysym = Inputs.KsPrint то string := "Print";
	аесли keysym = Inputs.KsInsert то string := "Insert";
	аесли keysym = Inputs.KsMenu то string := "Menu";
	аесли keysym = Inputs.KsBreak то string := "Break";
	аесли keysym = Inputs.KsNumLock то string := "NumLock";
	аесли keysym = Inputs.KsKPEnter то string := "KPEnter";
	аесли keysym = Inputs.KsKPMultiply то string := "KPMultiply";
	аесли keysym = Inputs.KsKPAdd то string := "KPAdd";
	аесли keysym = Inputs.KsKPSubtract то string := "KPSubtract";
	аесли keysym = Inputs.KsKPDecimal то string := "KPDecimal";
	аесли keysym = Inputs.KsKPDivide то string := "KPDivide";
	аесли keysym = Inputs.KsF1 то string := "F1";
	аесли keysym = Inputs.KsF2 то string := "F2";
	аесли keysym = Inputs.KsF3 то string := "F3";
	аесли keysym = Inputs.KsF4 то string := "F4";
	аесли keysym = Inputs.KsF5 то string := "F5";
	аесли keysym = Inputs.KsF6 то string := "F6";
	аесли keysym = Inputs.KsF7 то string := "F7";
	аесли keysym = Inputs.KsF8 то string := "F8";
	аесли keysym = Inputs.KsF9 то string := "F9";
	аесли keysym = Inputs.KsF10 то string := "F10";
	аесли keysym = Inputs.KsF11 то string := "F11";
	аесли keysym = Inputs.KsF12 то string := "F12";
	аесли keysym = Inputs.KsShiftL то string := "ShiftL";
	аесли keysym = Inputs.KsShiftR то string := "ShiftR";
	аесли keysym = Inputs.KsControlL то string := "ControlL";
	аесли keysym = Inputs.KsControlR то string := "ControlR";
	аесли keysym = Inputs.KsCapsLock то string := "CapsLock";
	аесли keysym = Inputs.KsMetaL то string := "MetaL";
	аесли keysym = Inputs.KsMetaR то string := "MetaR";
	аесли keysym = Inputs.KsAltL то string := "AltL";
	аесли keysym = Inputs.KsAltR то string := "AltR";
	аесли keysym = Inputs.KsNil то string := "No Key";
	иначе
		string := "Нет такого ЛККЯ";
	всё;
кон GetKeysymString;

проц GetFlagsString(flags : мнвоНаБитахМЗ; перем string: массив из симв8);
нач
	string := "";
	если Inputs.LeftCtrl в flags то Strings.Append(string, "[LCTRL]"); всё;
	если Inputs.RightCtrl в flags то Strings.Append(string, "[RCTRL]"); всё;
	если Inputs.LeftShift в flags то Strings.Append(string, "[LSHIFT]"); всё;
	если Inputs.RightShift в flags то Strings.Append(string, "[RSHIFT]"); всё;
	если Inputs.LeftAlt в flags то Strings.Append(string, "[LALT]"); всё;
	если Inputs.RightAlt в flags то Strings.Append(string, "[RALT]"); всё;
	если Inputs.LeftMeta в flags то Strings.Append(string, "[LMETA]"); всё;
	если Inputs.RightMeta в flags то Strings.Append(string, "[RMETA]"); всё;
	если Inputs.Release в flags то Strings.Append(string, "[RELEASE]"); всё;
кон GetFlagsString;

(* Message preview handler for window manager. MUST NOT TRAP!!! *)
проц ПредобработчикСобытийДляWindowManager(перем msg : WMMessages.Message; перем discard : булево);
перем str: массив 128 из симв8;
нач
	discard := ложь;
	если msg.msgType = WMMessages.MsgKey то
		ЛогЯдра.пСтроку8("Key: UCS="); ЛогЯдра.п16ричное(msg.x, 8);
		ЛогЯдра.пСтроку8(", KeySym="); ЛогЯдра.п16ричное(msg.y, 8);
		ЛогЯдра.пСтроку8(" ("); GetKeysymString(msg.y, str); ЛогЯдра.пСтроку8(str); ЛогЯдра.пСтроку8(")");
		ЛогЯдра.пСтроку8(", Key: '");
		если (31 < msg.x) и (msg.x < 128) то ЛогЯдра.пСимв8(симв8ИзКода(msg.x)); всё;
		ЛогЯдра.пСтроку8("', Flags="); GetFlagsString(msg.flags, str); ЛогЯдра.пСтроку8(str);
		ЛогЯдра.пВК_ПС;
	всё;
кон ПредобработчикСобытийДляWindowManager;

(** Start logging key messages to kernel log *)
проц StartLog*(context : Commands.Context); (** ~ *)
нач {единолично}
	если manager = НУЛЬ то
		manager := WM.GetDefaultManager();
		если manager # НУЛЬ то
			context.out.пСтроку8("WMKeyCode: Log started."); context.out.пВК_ПС;
			manager.InstallMessagePreview(ПредобработчикСобытийДляWindowManager);
		иначе
			context.out.пСтроку8("WMKeyCode: Could not retrieve default window manager."); context.out.пВК_ПС;
		всё;
	иначе
		context.out.пСтроку8("WMKeyCode: Log is already started."); context.out.пВК_ПС;
	всё;
кон StartLog;

(** Stop logging key messages to kernel log *)
проц StopLog*(context : Commands.Context);
нач {единолично}
	если manager # НУЛЬ то
		manager.RemoveMessagePreview(ПредобработчикСобытийДляWindowManager);
		manager := НУЛЬ;
		context.out.пСтроку8("WMKeyCode: Log stoppped."); context.out.пВК_ПС;
	иначе
		context.out.пСтроку8("WMKeyCode: Log is not running."); context.out.пВК_ПС;
	всё;
кон StopLog;

проц Open*;
нач {единолично}
	если winstance= НУЛЬ то нов(winstance, НУЛЬ) всё;
кон Open;

проц Restore* (context : WMRestorable.Context);
нач {единолично}
	если (winstance = НУЛЬ) то
		нов (winstance, context)
	всё;
кон Restore;

проц Cleanup;
нач {единолично}
	если winstance # НУЛЬ то winstance.Close всё;
	если manager # НУЛЬ то manager.RemoveMessagePreview(ПредобработчикСобытийДляWindowManager); всё;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон WMKeyCode.

WMKeyCode.Open ~

WMKeyCode.StartLog ~
WMKeyCode.StopLog ~

System.Free WMKeyCode ~
