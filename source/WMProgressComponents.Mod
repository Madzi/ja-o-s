модуль WMProgressComponents; (** AUTHOR "staubesv"; PURPOSE "Progress indication components and windows"; *)

использует
	ЛогЯдра, Strings, XML, Types, Models, WMGraphicUtilities, WMComponents, WMProperties, WMRectangles, WMGraphics;

тип

	ProgressBar* = окласс(WMComponents.VisualComponent)
	перем
		min-, max- : WMProperties.IntegerProperty;

		current : цел64;

		model- : WMProperties.ReferenceProperty;
		modelI : Models.Model;

		isVertical- : WMProperties.BooleanProperty;
		color-: WMProperties.ColorProperty;
		borderColor-: WMProperties.ColorProperty;
		textColor- : WMProperties.ColorProperty;

		showPercents- : WMProperties.BooleanProperty; (* Default: TRUE *)

		проц &{перекрыта}Init*;
		нач
			Init^;
			takesFocus.Set(ложь);
			нов(isVertical, PrototypePbIsVertical, НУЛЬ, НУЛЬ); properties.Add(isVertical);
			нов(color, PrototypePbColor, НУЛЬ, НУЛЬ); properties.Add(color);
			нов(borderColor, PrototypePbBorderColor, НУЛЬ, НУЛЬ); properties.Add(borderColor);
			нов(min, PrototypePbMin, НУЛЬ, НУЛЬ); properties.Add(min);
			нов(max, PrototypePbMax, НУЛЬ, НУЛЬ); properties.Add(max);
			current := 0;
			нов(model, PrototypePbModel, НУЛЬ, НУЛЬ); properties.Add(model);
			modelI := НУЛЬ;
			нов(showPercents, PrototypePbShowPercents, НУЛЬ, НУЛЬ); properties.Add(showPercents);
			нов(textColor, PrototypePbTextColor, НУЛЬ, НУЛЬ); properties.Add(textColor);
			SetNameAsString(StrProgressBar);
			SetGenerator("WMProgressComponents.GenProgressBar");
		кон Init;

		проц SetRange*(min, max : цел64);
		нач
			Acquire;
			сам.min.Set(min);
			сам.max.Set(max);
			Release;
			Invalidate;
		кон SetRange;

		проц SetCurrent*(current : цел64);
		нач
			SetInternal(current, истина);
		кон SetCurrent;

		проц SetInternal(current : цел64; updateModel : булево);
		перем changed : булево; min, max : цел64;
		нач
			min := сам.min.Get(); max := сам.max.Get();
			если (current < min) то current := min; аесли (current > max) то current := max; всё;
			Acquire;
			если (current # сам.current) то
				сам.current := current; changed := истина;
			всё;
			Release;
			если changed то
				если updateModel то UpdateModel(current); всё;
				Invalidate;
			всё;
		кон SetInternal;

		проц IncPos*;
		перем max : цел64;
		нач
			max := сам.max.Get();
			Acquire;
			увел(current); если current > max то current := max; всё;
			Release;
			Invalidate;
		кон IncPos;

		проц {перекрыта}RecacheProperties*;
		перем any : динамическиТипизированныйУкль;
		нач
			RecacheProperties^;
			any := model.Get();
			если (any # НУЛЬ) и (any суть Models.Model) и (any # modelI) то
				modelI := any (Models.Model);
				modelI.onChanged.Add(ModelChanged);
				ModelChanged(НУЛЬ, НУЛЬ);
			аесли (modelI # НУЛЬ) то
				modelI.onChanged.Remove(ModelChanged);
				modelI := НУЛЬ;
			всё;
		кон RecacheProperties;

		проц UpdateModel(value : цел64);
		перем integer : Types.Hugeint; res : целМЗ; model : Models.Model;
		нач
			model := modelI;
			если (model # НУЛЬ) то
				integer.value := value;
				model.SetGeneric(integer, res); (* ignore res *)
			всё;
		кон UpdateModel;

		проц ModelChanged(sender, data : динамическиТипизированныйУкль);
		перем integer : Types.Hugeint; res : целМЗ;
		нач
			Acquire;
			modelI.GetGeneric(integer, res);
			если (res = Models.Ok) то
				SetInternal(integer.value, истина);
			иначе
				ЛогЯдра.пСтроку8("WMProgressComponents.ModelChanged.res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
			всё;
			Release;
		кон ModelChanged;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = min) или (property = max) то
				Invalidate;
			аесли (property = color) или (property = borderColor) или (property = textColor) или
				 	(property = showPercents) или (property = isVertical) то
				Invalidate;
			аесли (property = model) то
				RecacheProperties;
			иначе PropertyChanged^(sender, property)
			всё;
		кон PropertyChanged;

		проц {перекрыта}DrawBackground*(canvas: WMGraphics.Canvas);
		перем
			rect: WMRectangles.Rectangle;
			width: цел32;
			string : массив 32 из симв8;
			min, max, cur: цел64; percent : целМЗ; color: WMGraphics.Color;
			isVertical : булево;
		нач
			min := сам.min.Get();
			max := сам.max.Get();
			cur := current;
			если (cur > max) то cur := max; аесли (cur < min) то cur := min; всё;
			isVertical := сам.isVertical.Get();

			DrawBackground^(canvas);
			если max > min то
				если ~isVertical то
					width := округлиВниз((cur - min) / (max - min) * bounds.GetWidth());
					rect := WMRectangles.MakeRect(0, 0, width, bounds.GetHeight());
				иначе
					width := округлиВниз((cur - min) / (max - min) * bounds.GetHeight());
					rect := WMRectangles.MakeRect(0, bounds.GetHeight() - width, bounds.GetWidth(), bounds.GetHeight());
				всё;
				canvas.Fill(rect, сам.color.Get(), WMGraphics.ModeSrcOverDst);
			всё;

			rect := GetClientRect();

			если ~isVertical то
				rect.l := width;
			иначе
				rect.t := rect.b + width;
			всё;

			canvas.Fill(rect, fillColor.Get(), WMGraphics.ModeSrcOverDst);

			color := borderColor.Get();
			если (color # 0) то
				WMGraphicUtilities.DrawRect(canvas, GetClientRect(), color, WMGraphics.ModeSrcOverDst);
			всё;

			если showPercents.Get() и (max - min > 0) то
				percent := округлиВниз(100 * (cur - min) / (max - min)); Strings.IntToStr(percent, string); Strings.Append(string, "%");
				canvas.SetColor(textColor.Get());
				WMGraphics.DrawStringInRect(canvas, GetClientRect(), ложь, WMGraphics.AlignCenter, WMGraphics.AlignCenter, string)
			всё;
		кон DrawBackground;

	кон ProgressBar;

перем

	(** ProgressBar property prototypes *)
	PrototypePbMin*, PrototypePbMax*: WMProperties.IntegerProperty;
	PrototypePbModel* : WMProperties.ReferenceProperty;
	PrototypePbIsVertical* : WMProperties.BooleanProperty;
	PrototypePbColor*, PrototypePbBorderColor* : WMProperties.ColorProperty;
	PrototypePbShowPercents* : WMProperties.BooleanProperty;
	PrototypePbTextColor* : WMProperties.ColorProperty;

	StrProgressBar : Strings.String;

проц GenProgressBar*() : XML.Element;
перем pb : ProgressBar;
нач нов(pb); возврат pb
кон GenProgressBar;

проц InitPrototypes;
перем plProgressBar : WMProperties.PropertyList;
нач
	(* ProgressBar properties *)
	нов(plProgressBar); WMComponents.propertyListList.Add("ProgressBar", plProgressBar);
	(* colors *)
	нов(PrototypePbIsVertical, НУЛЬ, Strings.NewString("IsVertical"), Strings.NewString("Vertical progress bar?"));
	PrototypePbIsVertical.Set(ложь);
	нов(PrototypePbBorderColor, НУЛЬ, Strings.NewString("BorderColor"), Strings.NewString("Progressbar border color")); plProgressBar.Add(PrototypePbBorderColor);
	PrototypePbBorderColor.Set(WMGraphics.White);
	нов(PrototypePbColor, НУЛЬ, Strings.NewString("Color"), Strings.NewString("Progressbar color")); plProgressBar.Add(PrototypePbColor);
	PrototypePbColor.Set(WMGraphics.Blue);
	нов(PrototypePbTextColor, НУЛЬ, Strings.NewString("TextColor"), Strings.NewString("Progressbar text color")); plProgressBar.Add(PrototypePbTextColor);
	PrototypePbTextColor.Set(WMGraphics.White);
	(* position *)
	нов(PrototypePbMin, НУЛЬ, Strings.NewString("Min"), Strings.NewString("Minimum position"));
	PrototypePbMin.Set(0);
	нов(PrototypePbMax, НУЛЬ, Strings.NewString("Max"), Strings.NewString("Maximum position"));
	PrototypePbMax.Set(100);
	нов(PrototypePbModel, НУЛЬ, Strings.NewString("Model"), Strings.NewString("Model")); PrototypePbModel.Set(НУЛЬ);
	(* other *)
	нов(PrototypePbShowPercents, НУЛЬ, Strings.NewString("ShowPercents"), Strings.NewString("Should the progress also be displayed as text")); plProgressBar.Add(PrototypePbShowPercents);
	PrototypePbShowPercents.Set(истина);
кон InitPrototypes;

нач
	StrProgressBar := Strings.NewString("ProgressBar");
	InitPrototypes;
кон WMProgressComponents.
