модуль AFM; (** AUTHOR "TF"; PURPOSE "AFN - Adobe Font Metrics (minimal support and only unicode < 256)"; *)

использует
	Files, ЛогЯдра;

тип
	CharInfo = запись
		w : цел32;
	кон;

	FontMetrics* = окласс
	перем chars : массив 256 из CharInfo;
		fontName : массив 64 из симв8;

		проц AddCharInfo(ucs : цел32; ci : CharInfo);
		нач
			если (ucs >= 0) и (ucs < 256) то
				chars[ucs] := ci
			всё
		кон AddCharInfo;

		проц InternalGetWidth(ucs : цел32) : цел32;
		нач
			если (ucs >= 0) и (ucs < 256) то возврат chars[ucs].w
			иначе возврат 0
			всё
		кон InternalGetWidth;

		проц GetWidth*(size : вещ32; ucs : цел32) : вещ32;
		нач
			если (ucs >= 0) и (ucs < 256) то возврат chars[ucs].w * size / 1000
			иначе возврат 0
			всё
		кон GetWidth;

		проц Kerning(ucs0, ucs1 : цел32) : цел32;
		нач
			возврат 0
		кон Kerning;

		проц GetStringWidth*(size : вещ32; str : массив из симв8) : вещ32;
		перем i, w : цел32;
		нач
			если str[0] = 0X то возврат 0.0 всё;
			w := InternalGetWidth(кодСимв8(str[0])); i := 1;
			нцПока str[i] # 0X делай
				w := w + InternalGetWidth(кодСимв8(str[i])) + Kerning(кодСимв8(str[i - 1]), кодСимв8(str[i]));
				увел(i)
			кц;
			возврат (w * size) / 1000
		кон GetStringWidth;

		проц LoadAFM(filename : массив из симв8; перем res : целМЗ);
		перем f : Files.File; r : Files.Reader;
			t  : массив 32 из симв8;
			hasName, hasCharMetrics, isC : булево;
			char : цел32;
			ci : CharInfo;
		нач
			f := Files.Old(filename);
			если f # НУЛЬ то
				Files.OpenReader(r, f, 0);
				нцПока (r.кодВозвратаПоследнейОперации = 0) и (~hasName) делай
					r.чЦепочкуСимв8ДоБелогоПоля(t);
					если t = "FontName" то hasName := истина
					иначе r.ПропустиДоКонцаСтрокиТекстаВключительно
					всё
				кц;
				если ~hasName то res := 1; возврат всё;
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fontName); r.ПропустиДоКонцаСтрокиТекстаВключительно;

				нцПока (r.кодВозвратаПоследнейОперации = 0) и (~hasCharMetrics) делай
					r.чЦепочкуСимв8ДоБелогоПоля(t);
					если t = "StartCharMetrics" то hasCharMetrics := истина
					иначе r.ПропустиДоКонцаСтрокиТекстаВключительно
					всё
				кц;
				r.ПропустиДоКонцаСтрокиТекстаВключительно;
				если ~hasCharMetrics то res := 2; возврат всё;

				isC := истина;
				нцПока (r.кодВозвратаПоследнейОперации = 0) и (isC) делай
					r.чЦепочкуСимв8ДоБелогоПоля(t);
					isC := t = "C";
					если isC то
						r.ПропустиБелоеПоле;
						r.чЦел32(char, ложь);
						r.ПропустиБелоеПоле;
						если r.чИДайСимв8() # ";" то res := 3; возврат всё;
						r.ПропустиБелоеПоле;
						r.чЦепочкуСимв8ДоБелогоПоля(t);
						если t # "WX" то res := 3; возврат всё;
						r.ПропустиБелоеПоле;
						r.чЦел32(ci.w, ложь);
						r.ПропустиДоКонцаСтрокиТекстаВключительно;
						AddCharInfo(char, ci)
					всё
				кц
			всё
		кон LoadAFM;

	кон FontMetrics;

перем times*, helvetica*, helveticaBold*  : FontMetrics;
	res : целМЗ;
нач
	нов(times);
	times.LoadAFM("TIR.AFM", res);
	ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;

	нов(helvetica);
	helvetica.LoadAFM("HV.AFM", res);
	ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;

	нов(helveticaBold);
	helveticaBold.LoadAFM("HVB.AFM", res);
	ЛогЯдра.пСтроку8("res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
кон AFM.X

System.Free AFM ~
