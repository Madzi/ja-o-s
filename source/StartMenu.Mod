модуль StartMenu;

использует
	Strings, XML, ЛогЯдра, Modules, Inputs, UTF8Strings, XMLObjects,
	MainMenu, WM := WMWindowManager, WMComponents, WMStandardComponents, WMProperties, WMMessages, WMEvents,
	WMRectangles;

конст
	DefaultPlugin = "BlockStartMenu";
	FancyMenuDesc = "Dummy.XML";
	MaxMenus = 16;
	MaxMenuButtons = 10;

тип
	String = Strings.String;
	EventListenerInfo = WMEvents.EventListenerInfo;
	Message = WMMessages.Message;

	Popup = окласс (WMComponents.FormWindow)

		проц {перекрыта}FocusLost*;
		нач manager.Remove(сам) кон FocusLost;

		проц {перекрыта}FocusGot*;
		нач manager.SetFocus(сам) кон FocusGot;

		проц {перекрыта}Handle*(перем msg : Message);
		нач
			Handle^(msg);
			если (msg.msgType = WMMessages.MsgExt) и (msg.ext = closePopupMsg) то FocusLost всё;
		кон Handle;

	кон Popup;

	ClosePopupMsg = окласс
	кон ClosePopupMsg;

	PluginManager = окласс
	перем
		plugin : Plugin;
		currentPluginName : Strings.String; (* { currentPluginName # NIL } *)
		factory : PluginFactory;
		startIndex : цел32;

		проц & New*;
		нач
			нов(factory);
			currentPluginName := НУЛЬ;
			SetPlugin(Strings.NewString(DefaultPlugin));
			startIndex := 0;
		кон New;

		проц Refresh;
		нач {единолично}
			если manager = НУЛЬ то manager := WM.GetDefaultManager() всё;
			SetPlugin(pluginName.Get());
		кон Refresh;

		проц SetPlugin(name : String);
		нач
			если plugin # НУЛЬ то
				plugin.Close;
			всё;
			если (name # НУЛЬ) и ((currentPluginName=НУЛЬ) или (name^ # currentPluginName^)) то
				currentPluginName := name;
				plugin := factory.Get(name);
				startIndex := 0;
			всё;
			если plugin # НУЛЬ то
				plugin.Open
			всё
		кон SetPlugin;

		проц Close;
		нач
			если plugin # НУЛЬ то plugin.Close всё
		кон Close;

		проц ShiftMenuItems(upwards : булево);
		нач
			plugin.ReopenMenuItemsShifted(upwards)
		кон ShiftMenuItems;

	кон PluginManager;

	PluginFactory = окласс

		проц Get(type : String) : Plugin;
		перем a : динамическиТипизированныйУкль;
		нач
			если type^ = "FancyStartMenu" то
				a := GenFancyStartMenu(); возврат a(Plugin)
			аесли type^ = "BlockStartMenu" то
				a := GenBlockStartMenu(); возврат a(Plugin)
			иначе (* return default *)
				a := GenBlockStartMenu(); возврат a(Plugin)
			всё
		кон Get;

	кон PluginFactory;

	Plugin = окласс

		проц Open;
		нач СТОП(311) кон Open;

		проц Close;
		нач СТОП(311) кон Close;

		проц ReopenMenuItemsShifted(upwards : булево);
		нач СТОП(311) кон ReopenMenuItemsShifted;

	кон Plugin;

	SubMenuOpener* = окласс(WMComponents.Component)
	перем filename : WMProperties.StringProperty;
		eRun* : EventListenerInfo;

		проц &{перекрыта}Init*;
		нач
			Init^;
			нов(filename, НУЛЬ, Strings.NewString("Filename"), Strings.NewString("")); properties.Add(filename);
			нов(eRun, Strings.NewString("Run"), Strings.NewString(""), сам.Run); eventListeners.Add(eRun);
		кон Init;

		проц Run*(sender, par : динамическиТипизированныйУкль); (** Eventhandler *)
		перем x, y : размерМЗ; filename : String;
			rect : WMRectangles.Rectangle;
		нач
			(* synchronize if not synchronized *)
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Run, sender, par)
			иначе
				(* actual business logic *)
				filename := сам.filename.Get();
				если filename # НУЛЬ то
					rect := sender(WMComponents.VisualComponent).bounds.Get();
					sender(WMComponents.VisualComponent).ToWMCoordinates(0, 0, x, y);
					OpenPopup(x, y, filename^);
				 всё
			всё
		кон Run;
	кон SubMenuOpener;

	MenuButtons =  массив MaxMenuButtons  из WMStandardComponents.Button;
	SMOArr  =  массив MaxMenuButtons  из SubMenuOpener;

	FancyStartMenu = окласс(Plugin)
	перем startMenu : WMComponents.FormWindow;
		nofLayouts, nofButtons, nofSMOs : цел32;
		menuButtons : MenuButtons;
		smos : SMOArr;

		проц CountLayouts() : цел32;
		перем i, n : цел32; done : булево; s : Strings.String;
		нач
			n := 0; done := ложь;
			нцПока (i < длинаМассива(layoutNames) - 1) и ~done делай
				s := layoutNames[i].Get();
				если UTF8Strings.Compare(s^, FancyMenuDesc) # UTF8Strings.CmpEqual то
					увел(n)
				иначе
					done := истина
				всё;
				увел(i)
			кц;
			возврат n
		кон CountLayouts;

		проц FindSMO(c : XML.Content; перем smo : SubMenuOpener);
		перем enum : XMLObjects.Enumerator; found : булево; ptr : динамическиТипизированныйУкль;
		нач
			smo := НУЛЬ;
			если c суть WMComponents.VisualComponent то
				enum := c(WMComponents.VisualComponent).GetContents();
				found := ложь;
				нцПока enum.HasMoreElements() и ~found делай
					ptr := enum.GetNext();
					если ptr суть SubMenuOpener то
						smo := ptr(SubMenuOpener);
						found := истина
					всё
				кц
			всё;
		кон FindSMO;

		проц FindMenuButtonsSMO(c : XML.Content; перем menuButtons : MenuButtons; перем smos: SMOArr; перем n, m : цел32);
		перем enum : XMLObjects.Enumerator; i, j : цел32; ptr : динамическиТипизированныйУкль; s : Strings.String;
			b : WMStandardComponents.Button; smo : SubMenuOpener;
		нач
			если c суть WMComponents.VisualComponent то
				enum := c(WMComponents.VisualComponent).GetContents();
				i := 0; j := 0;
				нцПока enum.HasMoreElements() делай
					ptr := enum.GetNext();
					если ptr суть WMStandardComponents.Button то
						b := ptr(WMStandardComponents.Button);
						s := b.caption.Get();
						если (s # НУЛЬ) и (UTF8Strings.Compare(s^, "") # UTF8Strings.CmpEqual) то
							menuButtons[i] := b; увел(i);
							FindSMO(b, smo);
							если smo # НУЛЬ то smos[j] := smo; увел(j) всё;
						всё;
					всё;
				кц;
				n := i; m := j
			иначе
				n := 0; m := 0;
			всё;
		кон FindMenuButtonsSMO;

		проц {перекрыта}Open;
		перем c : XML.Content; width, height : размерМЗ; view : WM.ViewPort; s : String;
		нач
			nofLayouts := CountLayouts();
			s := layoutNames[pm.startIndex].Get();
			ЛогЯдра.пСтроку8("loading "); ЛогЯдра.пСтроку8(s^); ЛогЯдра.пВК_ПС;
			c := WMComponents.Load(s^);
			если (c # НУЛЬ) и (c суть WMComponents.VisualComponent) то
				FindMenuButtonsSMO(c, menuButtons, smos, nofButtons, nofSMOs);
				width := c(WMComponents.VisualComponent).bounds.GetWidth();
				height := c(WMComponents.VisualComponent).bounds.GetHeight();
				нов(startMenu, width, height, истина);
				startMenu.SetTitle(Strings.NewString("StartMenu"));
				startMenu.pointerThreshold := 10;
				startMenu.DisableUpdate;
				startMenu.SetContent(c);
				startMenu.EnableUpdate;
				startMenu.Invalidate(startMenu.bounds);
				view := WM.GetDefaultView();
				manager := WM.GetDefaultManager();
				manager.Add(округлиВниз(view.range.l), округлиВниз(view.range.b) - height + 1, startMenu, {WM.FlagHidden});
			иначе
				ЛогЯдра.пСтроку8("XML-file not correctly loaded"); ЛогЯдра.пВК_ПС
			всё
		кон Open;

		проц {перекрыта}Close;
		нач {единолично}
			если startMenu # НУЛЬ то startMenu.Close всё
		кон Close;

		проц ReplaceMenuButtonsSMO(конст mb, newmb : MenuButtons; конст smos, newsmos : SMOArr);
		перем i : цел32; s : Strings.String;
		нач
			нцДля i := 0 до nofButtons - 1 делай
				s := newmb[i].caption.Get();
				mb[i].caption.Set(s);
				s := newsmos[i].filename.Get();
				smos[i].filename.Set(s);
			кц;
		кон ReplaceMenuButtonsSMO;

		проц {перекрыта}ReopenMenuItemsShifted(upwards : булево);
		перем old, i : цел32; s : String; c : XML.Content;
			newMButtons : MenuButtons;
			newsmos : SMOArr;
			n, m : цел32;
		нач
			old := pm.startIndex;
			если upwards то
				pm.startIndex := (pm.startIndex + 1) остОтДеленияНа nofLayouts
			иначе
				pm.startIndex := (pm.startIndex - 1) остОтДеленияНа nofLayouts
			всё;
			если old # pm.startIndex то
				s := layoutNames[pm.startIndex].Get();
				c := WMComponents.Load(s^);
				если (c # НУЛЬ) и (c суть WMComponents.VisualComponent) то
					FindMenuButtonsSMO(c, newMButtons, newsmos, n, m);
					если (nofButtons = nofSMOs) и (nofButtons = n) и (nofSMOs = m) то
						ReplaceMenuButtonsSMO(menuButtons, newMButtons, smos, newsmos);
						нцДля i := 0 до nofButtons - 1 делай
							menuButtons[i].Invalidate;
						кц;
					иначе
						ЛогЯдра.пСтроку8("layout "); ЛогЯдра.пСтроку8(s^); ЛогЯдра.пСтроку8(" does not match."); ЛогЯдра.пВК_ПС
					всё
				иначе
					ЛогЯдра.пСтроку8("XML-file not correctly loaded"); ЛогЯдра.пВК_ПС
				всё
			всё
		кон ReopenMenuItemsShifted;

	кон FancyStartMenu;

	BlockStartMenu = окласс(Plugin)
	перем startMenu : MainMenu.Window;

		проц {перекрыта}Open;
		нач
			нов(startMenu);
			startMenu.SetOriginator(НУЛЬ); (* includes page loading *)
		кон Open;

		проц {перекрыта}Close;
		нач
			если startMenu # НУЛЬ то startMenu.Close всё
		кон Close;

		проц {перекрыта}ReopenMenuItemsShifted(upwards : булево);
		кон ReopenMenuItemsShifted;

	кон BlockStartMenu;

	(* the starter decouples the sensitive callback from the WindowManager. *)
	Starter = окласс
	перем originator : динамическиТипизированныйУкль;

		проц &Init*(o : динамическиТипизированныйУкль);
		нач
			originator := o
		кон Init;

	нач {активное}
		pm.Refresh
	кон Starter;


перем
	stringPrototype, pluginName : WMProperties.StringProperty;
	layoutNames : массив MaxMenus из WMProperties.StringProperty;
	manager : WM.WindowManager;
	pm : PluginManager;
	p : Popup;
	closePopupMsg : ClosePopupMsg;

	проц OpenPopup*(x, y : размерМЗ; конст filename : массив из симв8);
	перем m : WM.WindowManager;
		 c : XML.Content;
		width, height : размерМЗ;
	нач
		c := WMComponents.Load(filename);
		если (c # НУЛЬ) и (c суть WMComponents.VisualComponent) то
			width := c(WMComponents.VisualComponent).bounds.GetWidth();
			height := c(WMComponents.VisualComponent).bounds.GetHeight();
			если width <= 0 то width := 10 всё; если height <= 0 то height := 10 всё;
			нов(p, width, height, истина);
			p.SetContent(c);
			m := WM.GetDefaultManager(); m.Add(x, y-height, p, {WM.FlagHidden}); m.SetFocus(p)
		иначе
			ЛогЯдра.пСтроку8(filename); ЛогЯдра.пСтроку8(" not correctly loaded"); ЛогЯдра.пВК_ПС
		всё
	кон OpenPopup;

	проц ClosePopup*;
	перем msg : WMMessages.Message; manager : WM.WindowManager;
	нач
		msg.msgType := WMMessages.MsgExt; msg.ext := closePopupMsg;
		manager := WM.GetDefaultManager();
		manager.Broadcast(msg);
	кон ClosePopup;

	проц GenSubMenuOpener*() : XML.Element;
	перем smo : SubMenuOpener;
	нач
		нов(smo); возврат smo
	кон GenSubMenuOpener;

	проц Open*;
	нач
		pm.SetPlugin(pluginName.Get());
	кон Open;

	(* load start menu with buttons shifted to the right *)
	проц ShiftMenuItemsRight*;
	нач
		pm.ShiftMenuItems(истина);
	кон ShiftMenuItemsRight;

	(* load start menu with buttons shifted to the left *)
	проц ShiftMenuItemsLeft*;
	нач
		pm.ShiftMenuItems(ложь);
	кон ShiftMenuItemsLeft;

	(* This procedure is directly called by the window manager. It must be safe. *)
	проц MessagePreview(перем m : WMMessages.Message; перем discard : булево);
	перем starter : Starter;
	нач
		если m.msgType = WMMessages.MsgKey то
			если (m.y = 0FF1BH) и ((m.flags * Inputs.Ctrl # {}) или (m.flags * Inputs.Meta # {})) то
				нов(starter, m.originator); discard := истина
			всё;
		аесли (m.msgType = WMMessages.MsgExt) и (m.ext = WMComponents.componentStyleMsg) то
			нов(starter, m.originator);
		всё
	кон MessagePreview;

	проц GenFancyStartMenu() : Plugin;
	перем menu : FancyStartMenu;
	нач нов(menu); возврат menu
	кон GenFancyStartMenu;

	проц GenBlockStartMenu() : Plugin;
	перем menu : BlockStartMenu;
	нач нов(menu); возврат menu
	кон GenBlockStartMenu;

	проц InitPrototypes;
	перем plStartMenu : WMProperties.PropertyList;
		s0, s1 : массив 128 из симв8;
		i : цел32;
	нач
		нов(plStartMenu); 	WMComponents.propertyListList.Add("StartMenu", plStartMenu);

		нов(stringPrototype, НУЛЬ, Strings.NewString("Plugin"), Strings.NewString("Plug-in-object that creates start-menu and determines its properties"));
		stringPrototype.Set(Strings.NewString(DefaultPlugin));
		нов(pluginName, stringPrototype, НУЛЬ, НУЛЬ); plStartMenu.Add(pluginName);

		нцДля i := 0 до длинаМассива(layoutNames) - 1 делай
			Strings.IntToStr(i, s0);
			копируйСтрокуДо0("Layout", s1);
			Strings.Append(s1, s0);
			нов(stringPrototype, НУЛЬ, Strings.NewString(s1), Strings.NewString("XML-file that determins content and layout of the fancy start-menu"));
			stringPrototype.Set(Strings.NewString(FancyMenuDesc));
			нов(layoutNames[i], stringPrototype, НУЛЬ, НУЛЬ); plStartMenu.Add(layoutNames[i]);
		кц;
	кон InitPrototypes;

	проц Cleanup;
	нач
		если pm # НУЛЬ то pm.Close всё;
		manager.RemoveMessagePreview(MessagePreview)
	кон Cleanup;

	проц Fancy*;
	нач
		pluginName.Set(Strings.NewString("FancyStartMenu"));
	кон Fancy;

	проц Block*;
	нач
		pluginName.Set(Strings.NewString("BlockStartMenu"));
	кон Block;

нач
	нов(pm);
	нов(closePopupMsg);
	InitPrototypes;
	Modules.InstallTermHandler(Cleanup);
	manager := WM.GetDefaultManager();
	manager.InstallMessagePreview(MessagePreview);
кон StartMenu.
