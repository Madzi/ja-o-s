(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль NbrRe64;   (** AUTHOR "adf"; PURPOSE "Alias for type FLOAT64."; *)

использует Потоки, MathL, NbrInt8, NbrInt16, NbrInt32, NbrInt64, NbrRe32;

конст
	E* = MathL.e;  Pi* = MathL.pi;

тип
	Real* = вещ64;

перем
	MinNbr-, MaxNbr-,
	(** Machine epsilon, i.e., e. *)
	reExp, Epsilon-: Real;  minExp, maxExp: NbrInt16.Integer;
	(** Machine radix, or base number. *)
	Radix-: NbrInt8.Integer;

(** Basic functions *)
	проц Abs*( x: Real ): Real;
	нач
		возврат матМодуль( x )
	кон Abs;

	проц Entier*( x: Real ): NbrInt32.Integer;
	нач
		возврат округлиВниз( x )
	кон Entier;

	проц LEntier*( x: Real ): NbrInt64.Integer;
	нач
		возврат округлиВниз64(x);
	кон LEntier;

	проц Long*( x: NbrRe32.Real ): Real;
	нач
		возврат устарПреобразуйКБолееШирокомуЦел( x )
	кон Long;

	проц IsRe32*( x: Real ): булево;
	нач
		если (матМодуль( x ) <= NbrRe32.MaxNbr) и (x = устарПреобразуйКБолееУзкомуЦел( x )) то возврат истина иначе возврат ложь всё
	кон IsRe32;

	проц Short*( x: Real ): NbrRe32.Real;
	нач
		возврат устарПреобразуйКБолееУзкомуЦел( x )
	кон Short;

	проц Max*( x1, x2: Real ): Real;
	нач
		если x1 > x2 то возврат x1 иначе возврат x2 всё
	кон Max;

	проц Min*( x1, x2: Real ): Real;
	нач
		если x1 < x2 то возврат x1 иначе возврат x2 всё
	кон Min;

	проц Sign*( x: Real ): NbrInt8.Integer;
	перем sign: NbrInt8.Integer;
	нач
		если x < 0 то sign := -1
		аесли x = 0 то sign := 0
		иначе sign := 1
		всё;
		возврат sign
	кон Sign;

	проц Int*( x: Real ): NbrInt32.Integer;
	нач
		возврат Entier( x )
	кон Int;

	проц Frac*( x: Real ): Real;
	перем y: Real;
	нач
		y := x - LEntier( x );  возврат y
	кон Frac;

	проц Round*( x: Real ): NbrInt32.Integer;
	перем int: NbrInt32.Integer;
	нач
		int := Entier( x );
		если x > (0.5 + int) то NbrInt32.Inc( int ) всё;
		возврат int
	кон Round;

	проц Floor*( x: Real ): NbrInt32.Integer;
	нач
		возврат Entier( x )
	кон Floor;

	проц Ceiling*( x: Real ): NbrInt32.Integer;
	перем int: NbrInt32.Integer;
	нач
		int := Entier( x );
		если x > int то NbrInt32.Inc( int ) всё;
		возврат int
	кон Ceiling;

(** Functions based on:  real = mantissa * (radix ^ exponent) *)
	проц Mantissa*( x: Real ): Real;
	перем abs: Real;
	нач
		abs := Abs( x );
		нцПока abs >= Radix делай abs := abs / Radix кц;
		нцПока abs < 1 делай abs := Radix * abs кц;
		возврат Sign( x ) * abs
	кон Mantissa;

	проц Exponent*( x: Real ): NbrInt16.Integer;
	перем exponent: NbrInt16.Integer;  abs: Real;
	нач
		abs := Abs( x );
		если abs > 0 то
			exponent := 0;
			нцПока abs >= Radix делай abs := abs / Radix;  NbrInt16.Inc( exponent ) кц;
			нцПока abs < 1 делай abs := Radix * abs;  NbrInt16.Dec( exponent ) кц
		иначе exponent := 1
		всё;
		возврат exponent
	кон Exponent;

	проц MantissaExponent( y: Real;  перем man: Real;  перем exp: NbrInt16.Integer );
	перем abs: Real;
	нач
		abs := Abs( y );
		если abs > 0 то
			exp := 0;
			нцПока abs >= Radix делай abs := abs / Radix;  NbrInt16.Inc( exp ) кц;
			нцПока abs < 1 делай abs := Radix * abs;  NbrInt16.Dec( exp ) кц;
			man := Sign( y ) * abs
		иначе man := 0;  exp := 1
		всё
	кон MantissaExponent;

	проц Re*( mantissa: Real;  exponent: NbrInt16.Integer ): Real;
	перем exp, n: NbrInt16.Integer;  coef, power, x: Real;
	нач
		если mantissa = 0 то x := 0;  возврат x всё;
		(* Obtain a corrected mantissa. *)
		MantissaExponent( mantissa, coef, exp );
		(* Compute power = radix ^ (exp + exponent) *)
		n := exp + exponent;
		если n < 0 то x := 1;  x := x / Radix;  n := -n иначе x := Radix всё;
		power := 1;
		нцПока n > 0 делай
			нцПока ~NbrInt16.Odd( n ) делай n := n DIV 2;  x := x * x кц;
			NbrInt16.Dec( n );  power := power * x
		кц;
		(* Return real = coef * radix ^ (exp + exponent) *)
		возврат coef * power
	кон Re;

(** Provide aliases for the Math functions called in NbrCplx. *)

	проц Sqrt*( x: Real ): Real;
	нач
		возврат MathL.sqrt( x )
	кон Sqrt;

	проц Sin*( x: Real ): Real;
	нач
		возврат MathL.sin( x )
	кон Sin;

	проц Cos*( x: Real ): Real;
	нач
		возврат MathL.cos( x )
	кон Cos;

	проц ArcTan*( x: Real ): Real;
	нач
		возврат MathL.arctan( x )
	кон ArcTan;

	проц Exp*( x: Real ): Real;
	нач
		возврат MathL.exp( x )
	кон Exp;

	проц Ln*( x: Real ): Real;
	нач
		возврат MathL.ln( x )
	кон Ln;


	(** String conversions. *)
(** Admissible characters include: {" ", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "D", "E", ",", "."}. *)
	проц StdForm( перем y: Real;  перем exponent: NbrInt16.Integer );
	нач
		(* Requires y >= 0, but doesn't check for this. *)
		если y > 0 то
			exponent := 0;
			нцПока y >= 10 делай y := y / 10;  NbrInt16.Inc( exponent ) кц;
			нцПока y < 1 делай y := 10 * y;  NbrInt16.Dec( exponent ) кц
		иначе y := 0;  exponent := 1
		всё
	кон StdForm;

	проц StringToRe*( string: массив из симв8;  перем x: Real );
	перем i, expSign, sign: NbrInt8.Integer;  coefExp, exp: NbrInt16.Integer;  base, coef, divisor, power: Real;
	нач
		i := 0;
		(* Pass over any leading white space. *)
		нцПока string[i] = симв8ИзКода( 20H ) делай NbrInt8.Inc( i ) кц;
		(* Determine the sign. *)
		если string[i] = симв8ИзКода( 2DH ) то sign := -1;  NbrInt8.Inc( i ) иначе sign := 1 всё;
		coef := 0;
		(* Read in that part that is to the left of the decimal point. *)
		нцПока ((string[i] # ".") и (string[i] # "D")) и ((string[i] # "E") и (string[i] # 0X)) делай
			если (симв8ИзКода( 30H ) <= string[i]) и (string[i] <= симв8ИзКода( 39H )) то coef := 10 * coef + устарПреобразуйКБолееШирокомуЦел( кодСимв8( string[i] ) - 30H )
			иначе
				(* Inadmissible character - it is skipped. *)
			всё;
			NbrInt8.Inc( i )
		кц;
		если string[i] = "." то
			NbrInt8.Inc( i );  divisor := 1;
			(* Read in that part that is to the right of the decimal point. *)
			нцПока (string[i] # "D") и (string[i] # "E") и (string[i] # 0X) делай
				если (симв8ИзКода( 30H ) <= string[i]) и (string[i] <= симв8ИзКода( 39H )) то
					divisor := 10 * divisor;  coef := coef + устарПреобразуйКБолееШирокомуЦел( кодСимв8( string[i] ) - 30H ) / divisor
				иначе
					(* Inadmissible character - it is skipped. *)
				всё;
				NbrInt8.Inc( i )
			кц
		всё;
		(* Put this coefficient into standard format. *)
		StdForm( coef, coefExp );
		(* Assign the correct sign to the coefficient. *)
		coef := sign * coef;
		(* Read in the exponent. *)
		если (string[i] = "D") или (string[i] = "E") то NbrInt8.Inc( i );
			(* Pass over any leading white space. *)
			нцПока string[i] = симв8ИзКода( 20H ) делай NbrInt8.Inc( i ) кц;
			(* Determine the sign. *)
			если string[i] = симв8ИзКода( 2DH ) то expSign := -1;  NbrInt8.Inc( i ) иначе expSign := 1 всё;
			(* Read in the string and convert it to an integer. *)
			exp := 0;
			нцПока string[i] # 0X делай
				если (симв8ИзКода( 30H ) <= string[i]) и (string[i] <= симв8ИзКода( 39H )) то exp := 10 * exp + (кодСимв8( string[i] ) - 30H)
				иначе
					(* Inadmissible character - it is skipped. *)
				всё;
				NbrInt8.Inc( i )
			кц;
			exp := expSign * exp
		иначе
			(* There is no E part. *)
			exp := 0
		всё;
		exp := coefExp + exp;
		(* Compute the integer power. *)
		если exp > maxExp то x := sign * MaxNbr
		аесли exp < minExp то x := 0
		иначе  (* Compute the power to a base of 10. *)
			если exp > 0 то base := 10
			аесли exp < 0 то base := 0.1;  exp := -exp
			иначе  (* exp = 0 *)
			всё;
			power := 1;
			нцПока exp > 0 делай
				нцПока ~NbrInt16.Odd( exp ) и (exp > 0) делай base := base * base;  exp := exp DIV 2 кц;
				power := base * power;  NbrInt16.Dec( exp )
			кц;
			x := coef * power
		всё
	кон StringToRe;

(** LEN(string >= significantFigures + 7 *)
	проц ReToString*( x: Real;  significantFigures: NbrInt8.Integer;  перем string: массив из симв8 );
	перем sign: симв8;  i: NbrInt8.Integer;  exponent: NbrInt16.Integer;  coef, factor: NbrInt64.Integer;  abs: Real;
	нач
		если significantFigures < 1 то significantFigures := 1 всё;
		если significantFigures > 16 то significantFigures := 16 всё;
		если x < 0 то abs := -x;  string[0] := "-";  иначе abs := x;  string[0] := симв8ИзКода( 30H ) всё;
		string[1] := ".";  exponent := 0;
		если abs > 0 то
			нцПока abs < 0.1 делай abs := 10 * abs;  NbrInt16.Dec( exponent ) кц;
			нцПока abs >= 1.0 делай abs := abs / 10;  NbrInt16.Inc( exponent ) кц;
			factor := 1;
			нцДля i := 1 до significantFigures делай factor := 10 * factor кц;
			coef := LEntier( factor * abs );  i := 2;
			нцДо
				factor := factor DIV 10;
				(* The following IF statement is a hack to handle the special case when x = 1.0. *)
				если (coef DIV factor) > 9 то factor := 10 * factor;  NbrInt16.Inc( exponent ) всё;
				string[i] := симв8ИзКода( NbrInt32.Short( NbrInt64.Short( coef DIV factor ) ) + 30H );  coef := coef остОтДеленияНа factor;  NbrInt8.Inc( i )
			кцПри factor = 1
		иначе
			нцДля i := 2 до significantFigures + 1 делай string[i] := симв8ИзКода( 30H ) кц
		всё;
		если exponent < 0 то sign := "-";  exponent := -exponent иначе sign := "+" всё;
		если exponent = 0 то string[significantFigures + 2] := 0X
		иначе
			string[significantFigures + 2] := "E";  string[significantFigures + 3] := sign;
			если (exponent DIV 100) # 0 то
				string[significantFigures + 4] := симв8ИзКода( (exponent DIV 100) + 30H );  exponent := exponent остОтДеленияНа 100;
				string[significantFigures + 5] := симв8ИзКода( (exponent DIV 10) + 30H );
				string[significantFigures + 6] := симв8ИзКода( (exponent остОтДеленияНа 10) + 30H );  string[significantFigures + 7] := 0X
			иначе
				exponent := exponent остОтДеленияНа 100;
				если (exponent DIV 10) # 0 то
					string[significantFigures + 4] := симв8ИзКода( (exponent DIV 10) + 30H );
					string[significantFigures + 5] := симв8ИзКода( (exponent остОтДеленияНа 10) + 30H );  string[significantFigures + 6] := 0X
				иначе string[significantFigures + 4] := симв8ИзКода( (exponent остОтДеленияНа 10) + 30H );  string[significantFigures + 5] := 0X
				всё
			всё
		всё
	кон ReToString;

(** Persistence: file IO *)
	проц Load*( R: Потоки.Чтец;  перем x: Real );
	перем char: симв8;  sInt: NbrInt8.Integer;  int: NbrInt16.Integer;  lInt: NbrInt32.Integer;  hInt: NbrInt64.Integer;
		re: NbrRe32.Real;
	нач
		R.чСимв8( char );
		просей char из
		"S":       R.чЦел8_мз( sInt );  x := NbrInt32.Long( NbrInt16.Long( sInt ) )
		| "I":     R.чЦел16_мз( int );  x := устарПреобразуйКБолееШирокомуЦел( int )
		| "L":     R.чЦел32_мз( lInt );  x := lInt
		| "H":     NbrInt64.Load( R, hInt );  x := hInt;
		| "E":     R.чВещ32_мз( re );  x := Long( re )
		иначе  (* char = "D" *)
			R.чВещ64_мз( x )
		всё
	кон Load;

	проц Store*( W: Потоки.Писарь;  x: Real );
	перем sInt: NbrInt8.Integer;  int: NbrInt16.Integer;  lInt: NbrInt32.Integer;  hInt: NbrInt64.Integer;  re: NbrRe32.Real;
		y: Real;
	нач
		hInt := LEntier( x );  y := x - hInt;
		если y = 0 то
			если NbrInt64.IsInt32( hInt ) то
				lInt := NbrInt64.Short( hInt );
				если NbrInt32.IsInt16( lInt ) то
					int := NbrInt32.Short( lInt );
					если NbrInt16.IsInt8( int ) то sInt := NbrInt16.Short( int );  W.пСимв8( "S" );  W.пЦел8_мз( sInt )
					иначе W.пСимв8( "I" );  W.пЦел16_мз( int )
					всё
				иначе W.пСимв8( "L" );  W.пЦел32_мз( lInt )
				всё
			иначе W.пСимв8( "H" );  NbrInt64.Store( W, hInt )
			всё
		иначе
			если IsRe32( x ) то re := Short( x );  W.пСимв8( "E" );  W.пВещ32_мз( re ) иначе W.пСимв8( "D" );  W.пВещ64_мз( x ) всё
		всё
	кон Store;

(* A local procedure called when the module is loaded to compute machine epsilon for export. *)
	проц EvalEpsilon;
	перем rounding: булево;  i, digits: NbrInt16.Integer;  a, b, c, epsilon, recipRadix: Real;

		проц AddProc( x, y: Real ): Real;
		(* Forces  x  and  y  to be put into memory before adding,
			overcoming optimizers that might hold one or the other in a register. *)
		нач
			возврат x + y
		кон AddProc;

		проц EvalRadix;
		перем x, y, z: Real;
		нач
			(* Compute  x = 2 ** m  with smallest  m  such that  x + 1 = x. *)
			x := 1;  z := 1;
			нцПока z = 1 делай x := 2 * x;  z := AddProc( x, 1 );  z := AddProc( z, -x ) кц;
			(* Now compute  y = 2 ** m  with smallest  m  such that  x + y > x. *)
			y := 1;  z := AddProc( x, y );
			нцПока z = x делай y := 2 * y;  z := AddProc( x, y ) кц;
			(* Reals  x  and  z  are neighboring floating point numbers whose difference is the radix. *)
			y := AddProc( z, -x );
			(* The one-quarter ensures that  y  is truncated to  radix  and not  radix - 1. *)
			Radix := NbrInt16.Short( NbrInt32.Short( Entier( y + 0.25 ) ) )
		кон EvalRadix;

		проц EvalDigits;
		перем x, y: Real;
		нач
			(* Seek smallest positive integer for which  radix ** (digits + 1) = 1. *)
			digits := 0;  x := 1;  y := 1;
			нцПока y = 1 делай NbrInt16.Inc( digits );  x := Radix * x;  y := AddProc( x, 1 );  y := AddProc( y, -x ) кц
		кон EvalDigits;

	нач
		EvalRadix;  EvalDigits;
		(* Compute epsilon. *)
		recipRadix := 1 / Radix;  epsilon := Radix;
		нцДля i := 1 до digits делай epsilon := AddProc( epsilon * recipRadix, 0 ) кц;
		(* Determine if rounding or chopping takes place during arithmetic operations. *)
		a := 1;  b := 1;
		(* Find  a = 2 ** m  with smallest  m  such that  a + 1 = a. *)
		нцПока b = 1 делай a := 2 * a;  b := AddProc( a, 1 );  b := AddProc( b, -a ) кц;
		(* Add a bit less than  radix / 2  to  a. *)
		b := AddProc( Radix / 2, -Radix / 100 );  c := AddProc( a, b );
		если a = c то rounding := истина иначе rounding := ложь всё;
		(* Add a bit more than  radix / 2  to  a. *)
		b := AddProc( Radix / 2, Radix / 100 );  c := AddProc( a, b );
		если rounding и (a = c) то rounding := ложь всё;
		(* Account for rounding. *)
		если rounding то epsilon := epsilon / 2 всё;
		Epsilon := epsilon
	кон EvalEpsilon;

нач
	EvalEpsilon;  MaxNbr := матМаксимум( вещ64 );  MinNbr := матМинимум( вещ64 );  reExp := MinNbr;  StdForm( reExp, minExp );  reExp := MaxNbr;
	StdForm( reExp, maxExp )
кон NbrRe64.
