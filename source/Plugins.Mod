(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Plugins; (** AUTHOR "pjm"; PURPOSE "Plugin object management"; *)

конст
	(** Result code constants *)
	Ok* = 0;
	DuplicateName* = 4101;
	AlreadyRegistered* = 4102;
	NeverRegistered* = 4103;

	(** Event constants *)
	EventAdd* = 0; (** Occurs when a plugin is added to the registry *)
	EventRemove* = 1; (** Occurs when a plugin is removed from the registry *)

тип
	Name* = массив 32 из симв8;
	Description* = массив 128 из симв8;

(** The plugin object provides the basis of extendibility. *)

	Plugin* = окласс
		перем
			name-: Name;	(** unique identifying name *)
			desc*: Description;	(** human-readable description of plugin *)
			link: Plugin;	(* next plugin in registry *)
			registry: Registry;

		(** Set the name field.  Must be called before adding the plugin to a registry. *)

		проц SetName*(конст name: массив из симв8);
		нач
			утв(registry = НУЛЬ);	(* can not be set after registering *)
			копируйСтрокуДо0(name, сам.name)
		кон SetName;

	кон Plugin;

	(** Plugin handler type for Registry.Enumerate() *)
	PluginHandler* = проц {делегат} (p: Plugin);

	(** Event handler (upcall) *)
	EventHandler* = проц {делегат} (event: целМЗ; plugin: Plugin);

	(* List item of event handler list *)
	EventHandlerList = укль на запись
		next: EventHandlerList;
		handler: EventHandler;
	кон;

	(** A table of plugins. *)
	Table* = укль на массив из Plugin;

(** Plugin registries are collections of plugins. *)
	Registry* = окласс (Plugin)
		перем
			root: Plugin;	(* list of registered plugins, in order of registration *)
			added: размерМЗ;	(* number of plugins successfully added (for synchronization) *)
			handlers: EventHandlerList;	(* list of installed event handlers *)

		(** Get a specific plugin.  If name = "", get the first existing plugin.  Return NIL if it was not found. *)

		проц Get*(конст name: массив из симв8): Plugin;
		перем p: Plugin;
		нач {единолично}
			p := root;
			если name # "" то
				нцПока (p # НУЛЬ) и (p.name # name) делай p := p.link кц
			всё;
			возврат p
		кон Get;

		(** Like Get, but wait until the plugin is available. *)

		проц Await*(конст name: массив из симв8): Plugin;
		перем p: Plugin; num: размерМЗ;
		нач {единолично}
			нц
				p := root;
				если name # "" то
					нцПока (p # НУЛЬ) и (p.name # name) делай p := p.link кц
				всё;
				если p # НУЛЬ то возврат p всё;
				num := added; дождись(added # num)	(* wait until a name is added *)
			кц
		кон Await;

		(** Call h for each plugin in this registry *)

		проц Enumerate*(h: PluginHandler);
		перем p: Plugin;
		нач (* can run concurrently with Add and Remove *)
			p := root;
			нцПока p # НУЛЬ делай
				h(p);
				p := p.link;
			кц;
		кон Enumerate;

		(** Get a table of available plugin instances in the registry, in order of registration.  If none are available, table = NIL, otherwise LEN(table^) is the number of available plugins. *)

		проц GetAll*(перем table: Table);
		перем p: Plugin; num, i: размерМЗ;
		нач {единолично}
			num := 0; p := root;	(* get number of entries *)
			нцПока p # НУЛЬ делай увел(num); p := p.link кц;
			если num # 0 то
				нов(table, num); p := root;
				нцДля i := 0 до num-1 делай table[i] := p; p := p.link кц
			иначе
				table := НУЛЬ
			всё
		кон GetAll;

		(** Register a new plugin instance.  Called by plugins to advertise their availability.  If the plugin has an empty name, a unique name is assigned, otherwise the name must be unique already.  The res parameter returns Ok if successful, or a non-zero error code otherwise. *)

		проц Add*(p: Plugin; перем res: целМЗ);
		перем c: Plugin; tail: Plugin; item: EventHandlerList;
		нач {единолично}
			утв(p # НУЛЬ);
			если p.registry = НУЛЬ то	(* assume this is initialized to NIL by environment *)
				если p.name = "" то GenName(added, сам.name, p.name) всё;
				если p.desc = "" то p.desc := "Unknown plugin" всё;
				tail := НУЛЬ; c := root;
				нцПока (c # НУЛЬ) и (c.name # p.name) делай tail := c; c := c.link кц;
				если c = НУЛЬ то	(* name is unique *)
					p.link := НУЛЬ; p.registry := сам;
					если root = НУЛЬ то root := p иначе tail.link := p всё;	(* add at end *)
					увел(added); res := Ok;
					(* Call event handlers *)
					item := handlers;
					нцПока item # НУЛЬ делай
						item^.handler(EventAdd, p);
						item := item^.next;
					кц;
				иначе
					res := DuplicateName
				всё
			иначе
				res := AlreadyRegistered
			всё
		кон Add;

		(** Unregister a plugin instance.  Called by plugins to withdraw their availability. *)

		проц Remove*(p: Plugin);
		перем c: Plugin; item: EventHandlerList;
		нач {единолично}
			утв(p # НУЛЬ);
			если p.registry # НУЛЬ то	(* was registered *)
				если p = root то
					root := root.link
				иначе
					c := root; нцПока c.link # p делай c := c.link кц;
					c.link := p.link
				всё;
				p.registry := НУЛЬ;
				(* Call event handlers *)
				item := handlers;
				нцПока item # НУЛЬ делай
					item^.handler(EventRemove, p);
					item := item^.next;
				кц;
			всё;
		кон Remove;

		(** Add an event handler *)

		проц AddEventHandler*(h: EventHandler; перем res: целМЗ);
		перем item: EventHandlerList;
		нач {единолично}
			утв(h # НУЛЬ);
			item := handlers;
			нцПока (item # НУЛЬ) и (item^.handler # h) делай
				item := item^.next;
			кц;
			если (item = НУЛЬ) то
				нов(item);
				item^.handler := h;
				item^.next := handlers;
				handlers := item;
				res := Ok;
			иначе
				res := AlreadyRegistered;
			всё;
		кон AddEventHandler;

		(** Remove an event handler *)

		проц RemoveEventHandler*(h: EventHandler; перем res: целМЗ);
		перем item: EventHandlerList;
		нач {единолично}
			утв(h # НУЛЬ);
			если handlers = НУЛЬ то
				res := NeverRegistered;
			аесли handlers^.handler = h то
				handlers := handlers^.next;
				res := Ok;
			иначе
				item := handlers;
				нцПока (item^.next # НУЛЬ) и (item^.next^.handler # h) делай
					item := item^.next;
				кц;
				если item^.next = НУЛЬ то
					res := NeverRegistered;
				иначе
					item^.next := item^.next^.next;
					res := Ok;
				всё;
			всё;
		кон RemoveEventHandler;

		(** Initialize the registry *)

		проц &Init*(конст name, desc: массив из симв8);
		перем res: целМЗ;
		нач
			root := НУЛЬ;
			added := 0;
			handlers := НУЛЬ;
			копируйСтрокуДо0(name, сам.name); копируйСтрокуДо0(desc, сам.desc);
			если (main # сам) и (main # НУЛЬ) то	(* add to global registry *)
				main.Add(сам, res);
				утв(res = Ok);
			всё;
		кон Init;

	кон Registry;

перем
	main*: Registry;	(** registry of all registries (excluding itself) *)

проц AppendInt(x: размерМЗ; перем to: массив из симв8);
перем i, m: размерМЗ;
нач
	утв(x >= 0);
	i := 0; нцПока to[i] # 0X делай увел(i) кц;
	если x # 0 то
		m := 1000000000;
		нцПока x < m делай m := m DIV 10 кц;
		нцДо
			to[i] := симв8ИзКода(48 + (x DIV m) остОтДеленияНа 10); увел(i);
			m := m DIV 10
		кцПри m = 0
	иначе
		to[i] := "0"; увел(i)
	всё;
	to[i] := 0X
кон AppendInt;

проц GenName(n: размерМЗ; перем registry, plugin: Name);
нач
	копируйСтрокуДо0(registry, plugin);
	AppendInt(n, plugin)
кон GenName;

нач
	main := НУЛЬ; нов(main, "Registry", "Registry of registries")
кон Plugins.

(*
To do (pjm):
o Open, Close?
o flags?
o Messaging?
o Unloading?
o deinitialize a registry.  stop awaiting clients.  invalidate plugins?
*)

(*
History:
06.10.2003	mvt	Added event handling for adding/removing plugins
07.10.2003	mvt	Added Registry.Enumerate()
*)
