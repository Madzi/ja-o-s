(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль MD5;	(** portable *) (* ejz  *)
	использует НИЗКОУР;

(** The MD5 Message-Digest Algorithm (RFC1321)

The algorithm takes as input a message of arbitrary length and produces
as output a 128-bit "fingerprint" or "message digest" of the input. It is
conjectured that it is computationally infeasible to produce two messages
having the same message digest, or to produce any message having a
given prespecified target message digest. The MD5 algorithm is intended
for digital signature applications, where a large file must be "compressed"
in a secure manner before being encrypted with a private (secret) key
under a public-key cryptosystem such as RSA. *)

	тип
		Context*  = укль на ContextDesc;
		ContextDesc = запись
			buf: массив 4 из цел32;
			bits: размерМЗ;
			in: массив 64 из симв8
		кон;
		Digest* = массив 16 из симв8;

(** Begin an MD5 operation, with a new context. *)
	проц New*(): Context;
		перем cont: Context;
	нач
		нов(cont);
		Init(cont);
		возврат cont
	кон New;

	проц Init * (cont: Context);
	нач
		cont.buf[0] := 067452301H;
		cont.buf[1] := цел32(0EFCDAB89H);
		cont.buf[2] := цел32(098BADCFEH);
		cont.buf[3] := 010325476H;
		cont.bits := 0;
	кон Init;

	проц ByteReverse(перем in: массив из НИЗКОУР.октет; перем out: массив из цел32; longs: цел32);
		перем
			adr: адресВПамяти; t, i: цел32;
			bytes: массив 4 из симв8;
	нач
		adr := адресОт(in[0]); i := 0;
		нцПока i < longs делай
			НИЗКОУР.копируйПамять(adr, адресОт(bytes[0]), 4);
			t := кодСимв8(bytes[3]);
			t := 256*t + кодСимв8(bytes[2]);
			t := 256*t + кодСимв8(bytes[1]);
			t := 256*t + кодСимв8(bytes[0]);
			out[i] := t;
			увел(adr, 4); увел(i)
		кц
	кон ByteReverse;

	проц F1(x, y, z: цел32): цел32;
	нач
		возврат цел32( (мнвоНаБитахМЗ( x)*мнвоНаБитахМЗ( y)) + ((-мнвоНаБитахМЗ( x))*мнвоНаБитахМЗ( z)))
	кон F1;

	проц F2(x, y, z: цел32): цел32;
	нач
		возврат цел32( (мнвоНаБитахМЗ( x)*мнвоНаБитахМЗ( z)) + (мнвоНаБитахМЗ( y)*(-мнвоНаБитахМЗ( z))))
	кон F2;

	проц F3(x, y, z: цел32): цел32;
	нач
		возврат цел32( мнвоНаБитахМЗ( x) / мнвоНаБитахМЗ( y) / мнвоНаБитахМЗ( z))
	кон F3;

	проц F4(x, y, z: цел32): цел32;
	нач
		возврат цел32( мнвоНаБитахМЗ( y) / (мнвоНаБитахМЗ( x)+(-мнвоНаБитахМЗ( z))))
	кон F4;

	проц STEP1(перем w: цел32; x, y, z, data: цел32; add: цел64;  s: цел32);
	нач
		w := w+F1(x, y, z)+data+устарПреобразуйКБолееУзкомуЦел(add);
		w := вращБит(w, s);
		увел(w, x)
	кон STEP1;

	проц STEP2(перем w: цел32; x, y, z, data: цел32; add: цел64; s: цел32);
	нач
		w := w+F2(x, y, z)+data+устарПреобразуйКБолееУзкомуЦел(add);
		w := вращБит(w, s);
		увел(w, x)
	кон STEP2;

	проц STEP3(перем w: цел32; x, y, z, data: цел32; add: цел64; s: цел32);
	нач
		w := w+F3(x, y, z)+data+устарПреобразуйКБолееУзкомуЦел(add);
		w := вращБит(w, s);
		увел(w, x)
	кон STEP3;

	проц STEP4(перем w: цел32; x, y, z, data: цел32; add: цел64; s: цел32);
	нач
		w := w+F4(x, y, z)+data+устарПреобразуйКБолееУзкомуЦел(add);
		w := вращБит(w, s);
		увел(w, x)
	кон STEP4;

	проц Transform(перем buf, in: массив из цел32);
		перем a, b, c, d: цел32;
	нач
		a := buf[0]; b := buf[1]; c := buf[2]; d := buf[3];

		STEP1(a, b, c, d, in[0],0D76AA478H, 7);
		STEP1(d, a, b, c, in[1],0E8C7B756H, 12);
		STEP1(c, d, a, b, in[2],0242070DBH, 17);
		STEP1(b, c, d, a, in[3],0C1BDCEEEH, 22);
		STEP1(a, b, c, d, in[4],0F57C0FAFH, 7);
		STEP1(d, a, b, c, in[5],04787C62AH, 12);
		STEP1(c, d, a, b, in[6],0A8304613H, 17);
		STEP1(b, c, d, a, in[7],0FD469501H, 22);
		STEP1(a, b, c, d, in[8],0698098D8H, 7);
		STEP1(d, a, b, c, in[9],08B44F7AFH, 12);
		STEP1(c, d, a, b, in[10],0FFFF5BB1H, 17);
		STEP1(b, c, d, a, in[11],0895CD7BEH, 22);
		STEP1(a, b, c, d, in[12],06B901122H, 7);
		STEP1(d, a, b, c, in[13],0FD987193H, 12);
		STEP1(c, d, a, b, in[14],0A679438EH, 17);
		STEP1(b, c, d, a, in[15],049B40821H, 22);

		STEP2(a, b, c, d, in[1],0F61E2562H, 5);
		STEP2(d, a, b, c, in[6],0C040B340H, 9);
		STEP2(c, d, a, b, in[11],0265E5A51H, 14);
		STEP2(b, c, d, a, in[0],0E9B6C7AAH, 20);
		STEP2(a, b, c, d, in[5],0D62F105DH, 5);
		STEP2(d, a, b, c, in[10],02441453H, 9);
		STEP2(c, d, a, b, in[15],0D8A1E681H, 14);
		STEP2(b, c, d, a, in[4],0E7D3FBC8H, 20);
		STEP2(a, b, c, d, in[9],021E1CDE6H, 5);
		STEP2(d, a, b, c, in[14],0C33707D6H, 9);
		STEP2(c, d, a, b, in[3],0F4D50D87H, 14);
		STEP2(b, c, d, a, in[8],0455A14EDH, 20);
		STEP2(a, b, c, d, in[13],0A9E3E905H, 5);
		STEP2(d, a, b, c, in[2],0FCEFA3F8H, 9);
		STEP2(c, d, a, b, in[7],0676F02D9H, 14);
		STEP2(b, c, d, a, in[12],08D2A4C8AH, 20);

		STEP3(a, b, c, d, in[5],0FFFA3942H, 4);
		STEP3(d, a, b, c, in[8],08771F681H, 11);
		STEP3(c, d, a, b, in[11],06D9D6122H, 16);
		STEP3(b, c, d, a, in[14],0FDE5380CH, 23);
		STEP3(a, b, c, d, in[1],0A4BEEA44H, 4);
		STEP3(d, a, b, c, in[4],04BDECFA9H, 11);
		STEP3(c, d, a, b, in[7],0F6BB4B60H, 16);
		STEP3(b, c, d, a, in[10],0BEBFBC70H, 23);
		STEP3(a, b, c, d, in[13],0289B7EC6H, 4);
		STEP3(d, a, b, c, in[0],0EAA127FAH, 11);
		STEP3(c, d, a, b, in[3],0D4EF3085H, 16);
		STEP3(b, c, d, a, in[6],04881D05H, 23);
		STEP3(a, b, c, d, in[9],0D9D4D039H, 4);
		STEP3(d, a, b, c, in[12],0E6DB99E5H, 11);
		STEP3(c, d, a, b, in[15],01FA27CF8H, 16);
		STEP3(b, c, d, a, in[2],0C4AC5665H, 23);

		STEP4(a, b, c, d, in[0],0F4292244H, 6);
		STEP4(d, a, b, c, in[7],0432AFF97H, 10);
		STEP4(c, d, a, b, in[14],0AB9423A7H, 15);
		STEP4(b, c, d, a, in[5],0FC93A039H, 21);
		STEP4(a, b, c, d, in[12],0655B59C3H, 6);
		STEP4(d, a, b, c, in[3],08F0CCC92H, 10);
		STEP4(c, d, a, b, in[10],0FFEFF47DH, 15);
		STEP4(b, c, d, a, in[1],085845DD1H, 21);
		STEP4(a, b, c, d, in[8],06FA87E4FH, 6);
		STEP4(d, a, b, c, in[15],0FE2CE6E0H, 10);
		STEP4(c, d, a, b, in[6],0A3014314H, 15);
		STEP4(b, c, d, a, in[13],04E0811A1H, 21);
		STEP4(a, b, c, d, in[4],0F7537E82H, 6);
		STEP4(d, a, b, c, in[11], 0BD3AF235H, 10);
		STEP4(c, d, a, b, in[2],02AD7D2BBH, 15);
		STEP4(b, c, d, a, in[9],0EB86D391H, 21);

		увел(buf[0], a); увел(buf[1], b);
		увел(buf[2], c); увел(buf[3], d)
	кон Transform;

(** Continues an MD5 message-digest operation, processing another
	message block, and updating the context. *)
	проц Write*(context: Context; ch: симв8);
		перем
			in: массив 16 из цел32;
			t, len: размерМЗ;
	нач
		t := context.bits; len := 1;
		context.bits := t + 8;
		t := (t DIV 8) остОтДеленияНа 64;
		если t > 0 то
			t := 64-t;
			если 1 < t то
				context.in[64-t] := ch;
				возврат
			всё;
			утв(len = 1);
			context.in[64-t] := ch;
			ByteReverse(context.in, in, 16);
			Transform(context.buf, in);
			умень(len, t)
		всё;
		если len > 0 то
			context.in[0] := ch
		всё
	кон Write;

(** Continues an MD5 message-digest operation, processing another
	message block, and updating the context. *)
	проц WriteBytes*(context: Context; конст buf: массив из симв8; len: размерМЗ);
		перем
			in: массив 16 из цел32;
			beg, t: размерМЗ;
	нач
		beg := 0; t := context.bits;
		context.bits := t + len*8;
		t := (t DIV 8) остОтДеленияНа 64;
		если t > 0 то
			t := 64-t;
			если len < t то
				НИЗКОУР.копируйПамять(адресОт(buf[beg]), адресОт(context.in[64-t]), len);
				возврат
			всё;
			НИЗКОУР.копируйПамять(адресОт(buf[beg]), адресОт(context.in[64-t]), t);
			ByteReverse(context.in, in, 16);
			Transform(context.buf, in);
			увел(beg, t); умень(len, t)
		всё;
		нцПока len >= 64 делай
			НИЗКОУР.копируйПамять(адресОт(buf[beg]), адресОт(context.in[0]), 64);
			ByteReverse(context.in, in, 16);
			Transform(context.buf, in);
			увел(beg, 64); умень(len, 64)
		кц;
		если len > 0 то
			НИЗКОУР.копируйПамять(адресОт(buf[beg]), адресОт(context.in[0]), len)
		всё
	кон WriteBytes;

(** Ends an MD5 message-digest operation, writing the message digest. *)
	проц Close*(context: Context; перем digest: Digest);
		перем
			in: массив 16 из цел32;
			beg, i, count: размерМЗ;
	нач
		count := (context.bits DIV 8) остОтДеленияНа 64;
		beg := count;
		context.in[beg] := симв8ИзКода(128); увел(beg);
		count := 64-1-count;
		если count < 8 то
			i := 0;
			нцПока i < count делай
				context.in[beg+i] := 0X; увел(i)
			кц;
			ByteReverse(context.in, in, 16);
			Transform(context.buf, in);
			i := 0;
			нцПока i < 56 делай
				context.in[i] := 0X; увел(i)
			кц
		иначе
			i := 0;
			нцПока i < (count-8) делай
				context.in[beg+i] := 0X; увел(i)
			кц
		всё;
		ByteReverse(context.in, in, 14);
		in[14] := context.bits(цел32); in[15] := 0;
		Transform(context.buf, in);
		ByteReverse(context.buf, in, 4);
		НИЗКОУР.копируйПамять(адресОт(in[0]), адресОт(digest[0]), 16)
	кон Close;

	проц HexDigit(i: цел32): симв8;
	нач
		если i < 10 то
			возврат симв8ИзКода(кодСимв8("0")+i)
		иначе
			возврат симв8ИзКода(кодСимв8("A")+i-10)
		всё
	кон HexDigit;

(** Convert the digest into an hexadecimal string. *)
	проц ToString*(digest: Digest; перем str: массив из симв8);
		перем i: цел32;
	нач
		нцДля i := 0 до 15 делай
			str[2*i] := HexDigit(кодСимв8(digest[i]) DIV 16);
			str[2*i+1] := HexDigit(кодСимв8(digest[i]) остОтДеленияНа 16)
		кц;
		str[32] := 0X
	кон ToString;

кон MD5.
