модуль BluetoothRFCOMM;

использует

	S := НИЗКОУР,
	BluetoothL2CAP,
	Bluetooth,
	Потоки,
	ЛогЯдра;

конст

	ModuleName = "[BTRFCOMM]";
	TraceBuffer = ложь;
	TraceChannel = ложь;
	TraceControlChannel = истина;
	TraceRFCOMM = ложь;

	(* Frame Types *)
	SABMFRAME 	= 	02FH;
	UAFRAME		= 	063H;
	DMFRAME		= 	00FH;
	DISCFRAME		= 	043H;
	UIHFRAME		= 	0EFH;

	(* DLC  states *)
	DISCONNECTED		= 	0;
	CONNECTING		=	1;
	NEGOTIATING		=	2;
	CONNECTED		=	3;
	DISCONNECTING	=	4;

	MAXBUFSIZE		=	1024;

тип

	Buffer = окласс

		перем
			maxBufSize : цел32;
			fifoBuffer : укль на массив из симв8;
			head, tail : цел32;
			dead : булево;

		проц &Init*(maxBufSize : цел32);
		нач
			сам.maxBufSize := maxBufSize;
			нов(fifoBuffer,maxBufSize);
			head := 0; tail := 0;
			dead := ложь;
		кон Init;

		проц Close;
		нач {единолично}
			dead := истина;
		кон Close;

		проц Get(перем ch : симв8; перем result : целМЗ);
		нач {единолично}
			если TraceBuffer то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Buffer.Get: Await ... head = "); ЛогЯдра.пЦел64(head,0);
				ЛогЯдра.пСтроку8(" tail = "); ЛогЯдра.пЦел64(tail,0);
				ЛогЯдра.пВК_ПС;
			всё;
			дождись((tail # head) или dead);
			если(tail#head) то
				ch := fifoBuffer[head];
				head := (head +1) остОтДеленияНа maxBufSize ;
				result := 0;
			иначе (* dead *)
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Buffer.Get: error.");
				ЛогЯдра.пВК_ПС;
				result := -1;
			всё;
			если TraceBuffer то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Buffer.Get: done. result = "); ЛогЯдра.п16ричное(result,-2);
				ЛогЯдра.пВК_ПС;
			всё;
		кон Get;

		проц Put(ch : симв8; перем result : цел32);
		нач {единолично}
			если((tail+1) остОтДеленияНа MAXBUFSIZE # head) то
				fifoBuffer[tail] := ch;
				tail := (tail +1) остОтДеленияНа maxBufSize ;
				result := 0;
			иначе (* overflow *)
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Buffer.Get: overflow.");
				result := -1;
			всё;
		кон Put;

		проц IsFull() : булево;
		нач {единолично}
			если((tail+1) остОтДеленияНа maxBufSize # head) то
				возврат ложь;
			иначе
				возврат истина;
			всё;
		кон IsFull;

		проц IsEmpty() : булево;
		нач {единолично}
			если(tail # head) то
				возврат ложь;
			иначе
				возврат истина;
			всё;
		кон IsEmpty;

		проц Dump;
		перем
			h,t,elements : цел32;
		нач {единолично}
			h := head остОтДеленияНа maxBufSize;
			t := tail остОтДеленияНа maxBufSize;
			если h<=t то
				elements := t-h;
			иначе
				elements := t-h+maxBufSize;
			всё;
			ЛогЯдра.пСтроку8(ModuleName);
			ЛогЯдра.пСтроку8("Buffer.Dump:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("buffer size = "); ЛогЯдра.пЦел64(maxBufSize,0);
			ЛогЯдра.пСтроку8(" head pos = "); ЛогЯдра.пЦел64(h,0);
			ЛогЯдра.пСтроку8(" tail pos = "); ЛогЯдра.пЦел64(t,0);
			ЛогЯдра.пСтроку8(" #elements = "); ЛогЯдра.пЦел64(elements,0);
			ЛогЯдра.пВК_ПС;
			нцПока(h < t) делай
				ЛогЯдра.п16ричное(кодСимв8(fifoBuffer[h]),-2); ЛогЯдра.пСтроку8(" ");
				h := (h+1) остОтДеленияНа maxBufSize;
			кц;
			ЛогЯдра.пВК_ПС;
		кон Dump;

	кон Buffer;

	(* ------------------------------------------------------------------------------ *)

	Channel = окласс

		перем

			dlci : цел32;
			state : цел32;
			rfcomm : RFCOMM;
			receiveBuffer : Buffer;

		(* --------------------------------------------- *)

		проц &Init*(rfcomm : RFCOMM; dlci : цел32);

		нач

			сам.rfcomm := rfcomm;
			сам.dlci := dlci;
			SetState(DISCONNECTED);

			нов(receiveBuffer,MAXBUFSIZE);

		кон Init;

		(* --------------------------------------------- *)

		проц Close;
		нач

			SetState(DISCONNECTED);
			receiveBuffer.Close();

		кон Close;

		(* --------------------------------------------- *)

		проц SetState(state : цел32);
		нач {единолично}

			сам.state := state;

		кон SetState;

		(* --------------------------------------------- *)

		проц W4State(state : цел32) : цел32;
		нач {единолично}

			дождись((сам.state = state) или (сам.state = DISCONNECTED));

			возврат сам.state;

		кон W4State;


		(* --------------------------------------------- *)

		проц SendSABM;
		перем
			frame : массив 4 из симв8;
		нач

			если (state =  DISCONNECTED) то

				frame[0] := симв8ИзКода(dlci*4+3);						(* address:  xxxx xx11; EA & C bit set *)
				frame[1] := симв8ИзКода(BITLOR(SABMFRAME,010H)); 	(* control; SABM Frame  P bit set *)
				frame[2] := симв8ИзКода(01H); 							(* length:    0000 0001; Length = 0, EA bit set *)
				frame[3] := CalculateFCS(frame,3);				(* FCS; calculate over address control and lenght *)

				rfcomm.SendFrame(frame,4);

				SetState(CONNECTING);

			всё;

		кон SendSABM;

		(* --------------------------------------------- *)

		проц SendDISC;
		перем
			frame : массив 4 из симв8;
		нач

			если(state # DISCONNECTED) то

				frame[0] := симв8ИзКода(dlci*4+3);						(* address:  xxxx xx11; EA & C bit set *)
				frame[1] := симв8ИзКода(BITLOR(DISCFRAME,010H)); 	(* control; DISC Frame  P bit set *)
				frame[2] := симв8ИзКода(01H); 							(* length:    0000 0001; Length = 0, EA bit set *)
				frame[3] := CalculateFCS(frame,3);				(* FCS; calculate over address control and lenght *)

				rfcomm.SendFrame(frame,4);

				SetState(DISCONNECTING);

			всё;

		кон SendDISC;

		(* --------------------------------------------- *)

		проц SendUA;
		перем
			frame : массив 4 из симв8;
		нач

			frame[0] := симв8ИзКода(dlci*4+3);						(* address:  xxxx xx11; EA & C bit set *)
			frame[1] := симв8ИзКода(BITLOR(UAFRAME,010H)); 		(* control; UA Frame  F bit set *)
			frame[2] := симв8ИзКода(01H); 							(* length:    0000 0001; Length = 0, EA bit set *)
			frame[3] := CalculateFCS(frame,3);				(* FCS; calculate over address control and lenght *)

			rfcomm.SendFrame(frame,4);

		кон SendUA;

		(* --------------------------------------------- *)

		проц SendDM;
		перем
			frame : массив 4 из симв8;
		нач

			frame[0] := симв8ИзКода(dlci*4+3);						(* address:  xxxx xx11; EA & C bit set *)
			frame[1] := симв8ИзКода(BITLOR(DMFRAME,010H)); 		(* control; DM Frame  F bit set *)
			frame[2] := симв8ИзКода(01H); 							(* length:    0000 0001; Length = 0, EA bit set *)
			frame[3] := CalculateFCS(frame,3);				(* FCS; calculate over address control and lenght *)

			rfcomm.SendFrame(frame,4);

		кон SendDM;

		(* --------------------------------------------- *)


		проц SendUIH(info : массив из симв8; infoLength : цел32);
		перем
			frame : массив 131 из симв8;
			i : цел32;
		нач

			утв(infoLength < 128);

			frame[0] := симв8ИзКода(dlci * 4 + 3);					(* address:  xxxx xx11; EA & C/R bit set *)
			frame[1] := симв8ИзКода(UIHFRAME);						(* control; UIH Frame  P bit not set *)
			frame[2] := симв8ИзКода(infoLength * 2 +1);		 		(* length; EA bit set *)

			нцДля i := 0 до infoLength-1 делай					(* information *)

				frame[3+i] := info[i];

			кц;

			frame[infoLength+3] := CalculateFCS(frame,2);	(* FCS; calculate over address and control *)

			rfcomm.SendFrame(frame,infoLength + 4);

		кон SendUIH;

		(* --------------------------------------------- *)

		проц ReceiveSABM;
		нач

			если TraceChannel то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Channel.ReceiveSABM: dlci = "); ЛогЯдра.п16ричное(dlci,-2);
				ЛогЯдра.пВК_ПС;
			всё;

			SendDM(); (* TO DO: accept connections *)

		кон ReceiveSABM;

		(* --------------------------------------------- *)

		проц ReceiveUA;
		нач

			если TraceChannel то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Channel.ReceiveUA: dlci = "); ЛогЯдра.п16ричное(dlci,-2);
				ЛогЯдра.пВК_ПС;
			всё;

			если(state = CONNECTING) то

				SetState(CONNECTED);

			аесли(state = DISCONNECTING) то

				Close();

			всё;

		кон ReceiveUA;

		(* --------------------------------------------- *)

		проц ReceiveDM;
		нач

			если TraceChannel то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Channel.ReceiveDM: dlci = "); ЛогЯдра.п16ричное(dlci,-2);
				ЛогЯдра.пВК_ПС;
			всё;

			Close();

		кон ReceiveDM;

		(* --------------------------------------------- *)

		проц ReceiveDISC;
		нач

			если TraceChannel то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Channel.ReceiveDISC: dlci = "); ЛогЯдра.п16ричное(dlci,-2);
				ЛогЯдра.пВК_ПС;
			всё;

			если(state = DISCONNECTED) то

				SendDM();

			иначе

				SendUA();

			всё;

			Close();

		кон ReceiveDISC;

		(* --------------------------------------------- *)

		проц ReceiveUIH(frame : массив из симв8; frameLength : цел32);

		перем

			infoLength : цел32;
			result : цел32;
			i : цел32;

		нач

			если TraceChannel то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Channel.ReceiveUIH: dlci = "); ЛогЯдра.п16ричное(dlci,-2);
				ЛогЯдра.пСтроку8(" infoLength = "); ЛогЯдра.пЦел64((кодСимв8(frame[2]) - 1) DIV 2,0);
				ЛогЯдра.пВК_ПС;
			всё;

			если (state = CONNECTED) то

				infoLength := (кодСимв8(frame[2]) - 1) DIV 2;

				утв(infoLength < 128);	(* TO DO: accept larger frames *)

				нцДля i:= 0 до infoLength-1 делай

					если (~receiveBuffer.IsFull()) то

						receiveBuffer.Put(frame[3+i],result);
						утв(result = 0);

					иначе

						(* TO DO: overflow *)

						ЛогЯдра.пСтроку8(ModuleName);
						ЛогЯдра.пСтроку8("Channel.ReceiveUIH: dlci = "); ЛогЯдра.п16ричное(dlci,-2);
						ЛогЯдра.пСтроку8(" buffer overflow!");
						ЛогЯдра.пВК_ПС;

					всё;


				кц;

			всё;

		кон ReceiveUIH;

		(* --------------------------------------------- *)

		проц CalculateFCS(data : массив из симв8; length : цел32) : симв8;
		перем
			fcs : симв8;
			i : цел32;
		нач
			fcs := симв8ИзКода(0FFH);
			i := 0;
			нцПока (length >  0) делай
				fcs := crcTable[кодСимв8(BITCXOR(fcs,data[i]))];
				увел(i); умень(length)
			кц;
			возврат симв8ИзКода(0FFH - кодСимв8(fcs));
		кон CalculateFCS;

		(* --------------------------------------------- *)

		проц Sender(конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);

		перем
			info : массив 127 из симв8;
			infoLength : цел32;
			i : размерМЗ;

		нач

			если TraceChannel то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Channel.Sender: dlci = "); ЛогЯдра.п16ричное(dlci,-2);
				ЛогЯдра.пВК_ПС;
			всё;

			i := 0;

			нцПока(i < len) делай

				infoLength := 0;

				нцПока((infoLength < длинаМассива(info)) и (i < len)) делай

					info[infoLength] := buf[i+ofs];
					увел(infoLength); увел(i);

				кц;

				SendUIH(info,infoLength);

			кц;

		кон Sender;

		(* --------------------------------------------- *)

		проц Receiver(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);

		нач

			если TraceChannel то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("Channel.Receiver: dlci = "); ЛогЯдра.п16ричное(dlci,-2);
				ЛогЯдра.пВК_ПС;
			всё;

			len := 0;
			res := 0;

			нцПока((len < min) и (res = 0)) делай

				receiveBuffer.Get(buf[ofs+len],res);
				увел(len);
			кц;
			нцПока((len < size) и (~receiveBuffer.IsEmpty()) и (res = 0)) делай

				receiveBuffer.Get(buf[ofs+len],res);
				увел(len);

			кц;

		кон Receiver;

		(* --------------------------------------------- *)

	кон Channel;

	(* ------------------------------------------------------------------------------ *)

	ControlChannel = окласс (Channel)

		(* TO DO *)

		проц {перекрыта}ReceiveUIH(info : массив из симв8; length : цел32);
		нач

			если TraceControlChannel то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("ControlChannel.ReceiveUIH: multiplexer control command received.");
				ЛогЯдра.пВК_ПС;
			всё;

		кон ReceiveUIH;

	кон ControlChannel;

	(* ------------------------------------------------------------------------------ *)

	(* RFCOMM API *)

	RFCOMM* = окласс

		перем
			l2capLayer : BluetoothL2CAP.L2CAP;
			l2capCID : цел32;
			l2capConfReqReceived : булево;
			l2capOpen : булево;
			rfcommOpen : булево;
			channelList : массив 62 из Channel;
			ok : булево;

		проц &Init*(l2capLayer : BluetoothL2CAP.L2CAP);
		перем
			result : целМЗ;
			controlChannel : ControlChannel;
			i : цел32;
		нач
			нов(controlChannel,сам,i);
			channelList[0] := controlChannel;
			нцДля i := 1 до 61 делай
				нов(channelList[i],сам,i);
			кц;
			сам.l2capLayer := l2capLayer;
			SetL2CAPOpen(ложь);
			SetL2CAPConfReqReceived(ложь);
			l2capLayer.EventIndication(BluetoothL2CAP.EConfigInd,L2CAPConfigIndication,result);
			утв(result = 0);
			l2capLayer.EventIndication(BluetoothL2CAP.EDisconnectInd,L2CAPDisconnectIndication,result);
			утв(result = 0);
			SetRFCOMMOpen(истина);
		кон Init;

		проц GetL2CAPLayer*() : BluetoothL2CAP.L2CAP;
		нач
			возврат l2capLayer;
		кон GetL2CAPLayer;

		проц Start*(bdAddr: Bluetooth.BDAddr; перем result : целМЗ);
		перем
			status : цел32;
			inMTU, outFlushTO : цел32;
			outFlow,linkTO : цел32;
			ok : булево;
		нач
			l2capLayer.Connect(BluetoothL2CAP.psmRFCOMM,bdAddr,l2capCID,result,status);
			если (result # 0) то
				если TraceRFCOMM то
					ЛогЯдра.пСтроку8(ModuleName);
					ЛогЯдра.пСтроку8("RFCOMM.Start: L2CAP connection failed. result = "); ЛогЯдра.п16ричное(result,-2);
					ЛогЯдра.пВК_ПС();
				всё;
			иначе
				inMTU := Bluetooth.MaxACLDataLen;
				outFlushTO := 0FFFFH;
				если TraceRFCOMM то
					ЛогЯдра.пСтроку8(ModuleName);
					ЛогЯдра.пСтроку8("RFCOMM.Start: configure L2CAP channel: MTU = "); ЛогЯдра.пЦел64(inMTU,0);
					ЛогЯдра.пСтроку8(" Flow = "); ЛогЯдра.пЦел64(outFlow,0);
					ЛогЯдра.пСтроку8(" FlushTo = "); ЛогЯдра.пЦел64(outFlushTO,0);
					ЛогЯдра.пСтроку8(" ...");
					ЛогЯдра.пВК_ПС;
				всё;
				l2capLayer.Configure(l2capCID,inMTU,outFlow,outFlushTO,linkTO,result);
				если (result #  0) то

						ЛогЯдра.пСтроку8(ModuleName);
						ЛогЯдра.пСтроку8("RFCOMM.Start: L2CAP configuration failed. result = "); ЛогЯдра.п16ричное(result,-2);
						ЛогЯдра.пВК_ПС();

				иначе
					если TraceRFCOMM то
						ЛогЯдра.пСтроку8(ModuleName);
						ЛогЯдра.пСтроку8("RFCOMM.Start: configuration done. result = "); ЛогЯдра.п16ричное(result,-2);
						ЛогЯдра.пСтроку8(" MTU = "); ЛогЯдра.пЦел64(inMTU,0);
						ЛогЯдра.пСтроку8(" Flow = "); ЛогЯдра.пЦел64(outFlow,0);
						ЛогЯдра.пСтроку8(" FlushTo = "); ЛогЯдра.пЦел64(outFlushTO,0);
						ЛогЯдра.пВК_ПС;
					всё;
					ok := W4L2CAPConfReqReceived();
					если(ok) то
						SetL2CAPOpen(истина);
						channelList[0].SendSABM();
					иначе
						result := -1;
					всё;
				всё;
			всё;
		кон Start;

		проц Close*(перем result : цел32);
		перем i : цел32;
		нач
			если TraceRFCOMM то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("RFCOMM.Close: ...");
				ЛогЯдра.пВК_ПС;
			всё;

			если ~rfcommOpen то
				если TraceRFCOMM то
					ЛогЯдра.пСтроку8(ModuleName);
					ЛогЯдра.пСтроку8("RFCOMM.Close: already close");
					ЛогЯдра.пВК_ПС;
				всё;
				возврат;
			всё;

			нцДля i:= 0 до 61 делай
				channelList[i].SendDISC();
				channelList[i].Close();
			кц;
			result := 0;
			если(l2capOpen) то
				l2capLayer.Disconnect(l2capCID,result);
				если (result #  0) то
					если TraceRFCOMM то
						ЛогЯдра.пСтроку8(ModuleName);
						ЛогЯдра.пСтроку8("RFCOMM.Close: l2capLayer.Disconnect failed. result = "); ЛогЯдра.п16ричное(result,-2);
						ЛогЯдра.пВК_ПС();
					всё;
				всё;
				SetL2CAPOpen(ложь);
				SetL2CAPConfReqReceived(ложь);
			всё;
			l2capLayer.Close;
			SetRFCOMMOpen(ложь);
			если TraceRFCOMM то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("RFCOMM.Close: done. result =  "); ЛогЯдра.пЦел64(result,0);
				ЛогЯдра.пВК_ПС;
			всё;
		кон Close;

		проц EstablishChannel*(serverChannel : цел32; перем result : цел32);
		перем
			state : цел32;
		нач
			channelList[serverChannel * 2].SendSABM();
			state := channelList[serverChannel * 2].W4State(CONNECTED);
			если state = CONNECTED то
				result := 0;
			иначе
				result := -1;
			всё;
		кон EstablishChannel;

		проц ReleaseChannel*(serverChannel : цел32; перем result : цел32);
		перем
			state : цел32;
		нач
			channelList[serverChannel*2].SendDISC();
			state := channelList[serverChannel * 2].W4State(DISCONNECTED);
			если state = DISCONNECTED то
				result := 0;
			иначе
				result := -1;
			всё;
		кон ReleaseChannel;

		проц SendInformation*(serverChannel : цел32; info : массив из симв8; infoLength : размерМЗ);
		перем
			result : целМЗ;
		нач
			channelList[serverChannel * 2].Sender(info,0,infoLength,истина,result);
		кон SendInformation;

		проц ReceiveInformation*(serverChannel : цел32; перем info : массив из симв8; перем infoLength : размерМЗ);
		перем
			result : целМЗ;
		нач
			channelList[serverChannel * 2].Receiver(info,0,длинаМассива(info),1,infoLength,result);
		кон ReceiveInformation;

		проц GetSender*(serverChannel : цел32) : Потоки.Делегат˛реализующийЗаписьВПоток;
		нач
			возврат channelList[serverChannel * 2].Sender;
		кон GetSender;

		проц GetReceiver*(serverChannel : цел32) : Потоки.Делегат˛реализующийЧтениеИзПотока;
		нач
			возврат channelList[serverChannel * 2].Receiver;
		кон GetReceiver;

		проц SetRFCOMMOpen(state : булево);
		нач {единолично}
			rfcommOpen := state;
		кон SetRFCOMMOpen;

		проц SetL2CAPConfReqReceived(state : булево);
		нач {единолично}
			l2capConfReqReceived := state;
		кон SetL2CAPConfReqReceived;

		проц W4L2CAPConfReqReceived() : булево;
		нач {единолично}
			дождись(l2capConfReqReceived или ~rfcommOpen);
			если(rfcommOpen) то
				возврат истина;
			иначе
				возврат ложь;
			всё;
		кон W4L2CAPConfReqReceived;

		проц SetL2CAPOpen(state: булево);
		нач {единолично}
			l2capOpen := state;
		кон SetL2CAPOpen;

		проц W4L2CAPOpen() : булево;
		нач {единолично}
			дождись(l2capOpen или ~rfcommOpen);
			если(rfcommOpen) то
				возврат истина;
			иначе
				возврат ложь;
			всё;
		кон W4L2CAPOpen;

		проц L2CAPConnectIndication(indication: BluetoothL2CAP.Indication);
		нач
			ЛогЯдра.пСтроку8(ModuleName);
			ЛогЯдра.пСтроку8("RFCOMM.L2CAPConnectIndication: ignore!");
			ЛогЯдра.пВК_ПС;
		кон L2CAPConnectIndication;

		проц L2CAPDisconnectIndication(indication: BluetoothL2CAP.Indication);
		перем
			result : цел32;
		нач
			ЛогЯдра.пСтроку8(ModuleName);
			ЛогЯдра.пСтроку8("RFCOMM.L2CAPDisconnectIndication:  shutdown RFCOMM");
			ЛогЯдра.пВК_ПС;
			Close(result);
		кон L2CAPDisconnectIndication;

		проц L2CAPConfigIndication(inParam : BluetoothL2CAP.Indication);
		перем
			result : целМЗ;
		нач
			просейТип inParam : BluetoothL2CAP.ConfigInd делай
				если TraceRFCOMM то
					ЛогЯдра.пСтроку8(ModuleName);
					ЛогЯдра.пСтроку8("RFCOMM.L2CAPConfigIndication: ..."); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("cid = "); ЛогЯдра.пЦел64(inParam.c.sid,0);
					ЛогЯдра.пСтроку8(" ident = "); ЛогЯдра.пЦел64(кодСимв8(inParam.ident),0);
					ЛогЯдра.пСтроку8(" MTU = "); ЛогЯдра.пЦел64(inParam.outMTU,0);
					ЛогЯдра.пСтроку8(" FlushTO = "); ЛогЯдра.пЦел64(inParam.inFlushTO,0);
					ЛогЯдра.пВК_ПС;
				всё;
				l2capLayer.ConfigurationResponse(inParam.c.sid,inParam.ident,inParam.outMTU,
													inParam.inFlushTO,result);
			всё;
			SetL2CAPConfReqReceived(истина);
		кон L2CAPConfigIndication;

		проц SendFrame(frame : массив из симв8;length : цел32);
		перем
			size : цел32;
			result : целМЗ;
			i : цел32;
		нач
			если TraceRFCOMM то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("RFCOMM.SendFrame: data size = "); ЛогЯдра.пЦел64(length,0);
				ЛогЯдра.пВК_ПС;
				нцДля i := 0 до length-1 делай
					 ЛогЯдра.п16ричное(кодСимв8(frame[i]), -2); ЛогЯдра.пСтроку8(" " );
				кц;
				ЛогЯдра.пВК_ПС;
			всё;
			если(l2capOpen = истина) то
				l2capLayer.Write(l2capCID,0,length,frame,size,result);
			всё;
			если (result # 0) то
				ЛогЯдра.пСтроку8(ModuleName);
				ЛогЯдра.пСтроку8("RFCOMM.SendFrame: l2cap.Write failed. result =  "); ЛогЯдра.пЦел64(result,0);
				ЛогЯдра.пВК_ПС;
			всё;
		кон SendFrame;

		проц ReceiveFrame;
		перем
			buffer : массив 512 из симв8;
			i : цел32;
			result,size : цел32;
			frameType : цел32;
			dlci : цел32;
		нач
			нцДо
				l2capLayer.Read(l2capCID,длинаМассива(buffer),buffer,result,size);
				если (result = 0) то
					если TraceRFCOMM то
						ЛогЯдра.пСтроку8(ModuleName);
						ЛогЯдра.пСтроку8("RFCOMM.ReceiveFrame: data size = "); ЛогЯдра.пЦел64(size,0);
						ЛогЯдра.пВК_ПС;
						нцДля i := 0 до size-1 делай
							 ЛогЯдра.п16ричное(кодСимв8(buffer[i]), -2); ЛогЯдра.пСтроку8(" " );
						кц;
						ЛогЯдра.пВК_ПС;
					всё;
					dlci := кодСимв8(buffer[0]) DIV 4;
					frameType := BITLAND(кодСимв8(buffer[1]),0EFH);
					если (frameType =	SABMFRAME) то
						channelList[dlci].ReceiveSABM();
					аесли(frameType = UAFRAME) то
						channelList[dlci].ReceiveUA();
					аесли(frameType = DMFRAME) то
						channelList[dlci].ReceiveDM();
					аесли(frameType = DISCFRAME) то
						channelList[dlci].ReceiveDISC();
					аесли(frameType = UIHFRAME) то
						channelList[dlci].ReceiveUIH(buffer,size);
					иначе
						ЛогЯдра.пСтроку8("unknown frame! "); ЛогЯдра.п16ричное(frameType,-2);
						ЛогЯдра.пСтроку8(" DLCI = "); ЛогЯдра.п16ричное(dlci,-2);
						ЛогЯдра.пВК_ПС;
					всё;
				иначе
					ЛогЯдра.пСтроку8(ModuleName);
					ЛогЯдра.пСтроку8("RFCOMM.ReceiveFrame: l2cap.Read failed. result =  "); ЛогЯдра.пЦел64(result,0);
					ЛогЯдра.пВК_ПС;
				всё;
			кцПри (result # 0);
		кон ReceiveFrame;

	нач {активное}
		если TraceRFCOMM то
			ЛогЯдра.пСтроку8(ModuleName);
			ЛогЯдра.пСтроку8("RFCOMM: {ACTIVE}  ...");
			ЛогЯдра.пВК_ПС;
		всё;
		ok := W4L2CAPOpen();
		если(ok) то
			ReceiveFrame();
		всё;
		если TraceRFCOMM то
			ЛогЯдра.пСтроку8(ModuleName);
			ЛогЯдра.пСтроку8("RFCOMM: {ACTIVE} done.");
			ЛогЯдра.пВК_ПС;
		всё;
	кон RFCOMM;

(* ------------------------------------------------------------------------ *)

перем

	crcTable: массив 256 из симв8;

(* ------------------------------------------------------------------------ *)

проц CreateCRCTable;
перем
	i,j : цел32;
	pol,data,sr : симв8;
	op1,op2,op3 : симв8;
нач
	pol := симв8ИзКода(224);
	нцДля j := 0 до 255 делай
		sr := симв8ИзКода(0);
		data :=  симв8ИзКода(j);
		нцДля i := 0 до 7 делай
			op1 := BITCAND(data,симв8ИзКода(1));
			op2 := BITCAND(sr,симв8ИзКода(1));
			op3 := BITCXOR(op1,op2);
			sr := симв8ИзКода(кодСимв8(sr) DIV 2);
			если (op3 #симв8ИзКода( 0)) то
				sr := BITCXOR(sr,pol);
			всё;
			data := симв8ИзКода(кодСимв8(data) DIV 2);
			sr := BITCAND(sr,симв8ИзКода(255));
		кц;
		crcTable[j] := sr;
	кц;
кон CreateCRCTable;

(* ------------------------------------------------------------------------ *)
	проц BITLOR(x, y: цел32): цел32;
	нач возврат S.подмениТипЗначения(цел32, S.подмениТипЗначения(мнвоНаБитахМЗ, x) + S.подмениТипЗначения(мнвоНаБитахМЗ, y))
	кон BITLOR;

	проц BITCAND(x, y: симв8): симв8;
	нач возврат симв8ИзКода(S.подмениТипЗначения(цел32, S.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(кодСимв8(x))) * S.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(кодСимв8(y)))))
	кон BITCAND;
	проц BITCXOR(x, y: симв8): симв8;
	нач возврат симв8ИзКода(S.подмениТипЗначения(цел32, S.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(кодСимв8(x))) / S.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(кодСимв8(y)))))
	кон BITCXOR;
	проц BITLAND(x, y: цел32): цел32;
	нач возврат S.подмениТипЗначения(цел32, S.подмениТипЗначения(мнвоНаБитахМЗ, x) * S.подмениТипЗначения(мнвоНаБитахМЗ, y))
	кон BITLAND;

нач
	CreateCRCTable();
кон BluetoothRFCOMM.
