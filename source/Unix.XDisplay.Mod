(* ETH Oberon, Copyright 2000 ETH Zürich Institut für Computersysteme, ETH Zentrum, CH-8092 Zürich.
Refer to the general ETH Oberon System license contract available at: http://www.oberon.ethz.ch/ *)

модуль XDisplay;   (** AUTHOR "gf"; PURPOSE  "display driver plugin for X Windows *)


использует S := НИЗКОУР, Трассировка, Unix, Machine, Files, UnixFiles, X11, Displays, Strings;

конст
	BG = 0;  FG = 15;   (* Background, foreground colors.*)


конст
	(* formats for Transfer.  value DIV 8 = bytes per pixel. *)
	unknown = 0;
	index8 = 8;  color555 = 16;  color565 = 17;  color664 = 18;
	color888 = 24;  color8888* = 32;

	(* Drawing operation modes. *)
	replace = 0;   (* destcolor := sourcecolor. *)
	paint = 1;   (* destcolor := destcolor OR sourcecolor. *)
	invert = 2;   (* destcolor := destcolor XOR sourcecolor. *)

перем
	winName, iconName: массив 128 из симв8;


тип

	RGB =	запись
				r, g, b: цел16
			кон;



	Clip* = окласс
		перем
			d: Display;  lx, ly, lw, lh: цел32;

			проц & Init( disp: Display );
			нач
				d := disp;  lx := 0; ly := 0; lw := 0; lh := 0;
				Reset
			кон Init;

			проц Set*( x, y, w, h: цел32 );
			перем rect: X11.Rectangle;
			нач
				если w < 0 то  w := 0  всё;
				если h < 0 то  h := 0  всё;
				если (x # lx) или (y # ly) или (w # lw) или (h # lh) то
					lx := x;  ly := y;  lw := w;  lh := h;
					если y  < d.height то  d.currwin := d.primary  иначе  d.currwin := d.secondary;  умень( y, d.height )  всё;
					rect.x := устарПреобразуйКБолееУзкомуЦел( x );  rect.y := устарПреобразуйКБолееУзкомуЦел( y );
					rect.w := устарПреобразуйКБолееУзкомуЦел( w );  rect.h := устарПреобразуйКБолееУзкомуЦел( h );
					Machine.Acquire( Machine.X11 );
					если (rect.x <= 0) и (rect.y <= 0) и (rect.w >= d.width) и (rect.h >= d.height) то
						X11.SetClipMask( d.xdisp, d.gc, X11.None ) (* no clipping *)
					иначе
						X11.SetClipRectangles( d.xdisp, d.gc, 0, 0, адресОт( rect ), 1, X11.YXBanded )
					всё;
					Machine.Release( Machine.X11 )
				всё;
			кон Set;

			проц Get*( перем x, y, w, h: цел32 );
			нач
				x := lx;  y := ly;  w := lw;  h := lh
			кон Get;

			проц InClip*( x, y, w, h: цел32 ): булево;
			нач
				возврат  (x >= lx) и (x + w <= lx + lw) и (y >= ly) и (y + h <= ly + lh)
			кон InClip;


			проц Reset*;
			нач
				Set( 0, 0, d.width, d.height );
			кон Reset;

			(** Intersect with current clip rectangle resulting in a new clip rectangle. *)
			проц Adjust*( x, y, w, h: цел32 );   (* intersection *)
			перем x0, y0, x1, y1: цел32;
			нач
				если x > lx то  x0 := x  иначе  x0 := lx  всё;
				если y > ly то  y0 := y  иначе  y0 := ly  всё;
				если x + w < lx + lw то  x1 := x + w  иначе  x1 := lx + lw  всё;
				если y + h < ly + lh то  y1 := y + h  иначе  y1 := ly + lh  всё;
				Set( x0, y0, x1 - x0, y1 - y0 );
			кон Adjust;

		кон Clip;



	Display* = окласс   (Displays.Display)
			перем
				xdisp-				: X11.DisplayPtr;
				primary-			: X11.Window;
				secondary-			: X11.Window;
				currwin				: X11.Window;
				wmDelete- 		: X11.Atom;
				screen				: цел32;
				visual{неОтслСборщиком}		: X11.VisualPtr;
				depth				: цел32;
				bigEndian			: булево;
				gc					: X11.GC;
				clip					: Clip;

				cmap				: X11.Colormap;
				planesMask			: цел32;
				foreground,
				background			: длинноеЦелМЗ;
				rgb, defPal			: массив 256 из RGB;	(* 8-bit pseudo color *)
				pixel				: массив 256 из цел32;   (* pixel values for Oberon colors *)

				xformat				: цел32;
				currcol, currmode	: цел32;
				xfunc				: массив 3 из цел32;



				проц SetMode( col: цел32 );
				перем mode: цел32;
				нач
					mode :=  replace;
					если (col # -1) и (30 в мнвоНаБитахМЗ(col )) то  mode := invert;  исключиИзМнваНаБитах( S.подмениТипЗначения( мнвоНаБитах32, col ), 30 )  всё;
					если mode # currmode то  X11.SetFunction( xdisp, gc, xfunc[mode] );  currmode := mode  всё;
					если col # currcol то  X11.SetForeground( xdisp, gc, ColorToPixel( col ) );  currcol := col  всё;
				кон SetMode;



				проц {перекрыта}Dot*( col, x, y: цел32 );
				нач
					если currwin = secondary то  умень( y, height )  всё;
					Machine.Acquire( Machine.X11 );
					SetMode( col );
					X11.DrawPoint( xdisp, currwin, gc, x, y );
					Machine.Release( Machine.X11 )
				кон Dot;

				проц {перекрыта}Fill*( col, x, y, w, h: цел32 );
				нач
					если (h > 0) и (w > 0) то
						если currwin = secondary то  умень( y, height )  всё;
						Machine.Acquire( Machine.X11 );
						SetMode( col );
						X11.FillRectangle( xdisp, currwin, gc, x, y, w, h );
						Machine.Release( Machine.X11 )
					всё
				кон Fill;

				(** Transfer a block of pixels in "raw" display format to (op = set) or from (op = get) the display.
					Pixels in the rectangular area are transferred from left to right and top to bottom.  The pixels
					are transferred to or from "buf", starting at "ofs".  The line byte increment is "stride", which may
					be positive, negative or zero. *)

				проц {перекрыта}Transfer*( перем buf: массив из симв8;  ofs, stride, left, top, width, height : размерМЗ; op: целМЗ );
				конст  Get = 0;  Set = 1;
				перем image: X11.Image;
					imp: X11.ImagePtr;
					bp, ip: адресВПамяти;
					line, ll: цел32;
					x, y, w, h : цел32;
				нач
					x := left(цел32); y := top(цел32); w := width(цел32); h := height(цел32);
					ll := w*(xformat DIV 8);
					если (ofs + (h - 1)*stride + ll > длинаМассива( buf )) или (ofs + (h - 1)*stride < 0) то  СТОП( 99 )  всё;

					если длинаМассива( imgBuffer ) < 4*w*h то
						нов( imgBuffer, 4*w*h );	(* create buffer outside lock to avoid deadlock *)
					всё;

					bp := адресОт( buf[ofs] );
					если op = Set то
						Machine.Acquire( Machine.X11 );
						image := X11.CreateImage( xdisp, visual, depth, X11.ZPixmap, 0, 0, w, h, 32, 0 );
						imp := S.подмениТипЗначения( X11.ImagePtr, image );
						imp.data := адресОт( imgBuffer[0] );  ip := imp.data;
						если imp.byteOrder = 0 то
							нцДля line := 0 до h - 1  делай
								PutLine( xformat, w, ip, bp );
								увел( bp, stride );  увел( ip, imp.bytesPerLine )
							кц;
						иначе
							нцДля line := 0 до h - 1  делай
								PutLineBE( xformat, w, ip, bp );
								увел( bp, stride );  увел( ip, imp.bytesPerLine )
							кц;
						всё;
						если currmode # replace то
							X11.SetFunction( xdisp, gc, xfunc[replace] );  currmode := replace
						всё;
						X11.PutImage( xdisp, primary, gc, image, 0, 0, x, y, w, h );
						X11.Free( image );  imp := НУЛЬ;
						Machine.Release( Machine.X11 )
					аесли op = Get то
						Machine.Acquire( Machine.X11 );
						image := X11.GetImage( xdisp, primary, x, y, w, h, planesMask, X11.ZPixmap );
						imp := S.подмениТипЗначения( X11.ImagePtr, image );  ip := imp.data;
						если imp.byteOrder = 0 то
							нцДля line := 0 до h - 1 делай
								GetLine( xformat, w, ip, bp );
								увел( bp, stride );  увел( ip, imp.bytesPerLine )
							кц
						иначе
							нцДля line := 0 до h - 1 делай
								GetLineBE( xformat, w, ip, bp );
								увел( bp, stride );  увел( ip, imp.bytesPerLine )
							кц
						всё;
						X11.Free( imp.data );  X11.Free( image );  imp := НУЛЬ;
						Machine.Release( Machine.X11 )
					всё;
				кон Transfer;


				(** Transfer a block of pixels from a 1-bit mask to the display.  Pixels in the rectangular area are
					transferred from left to right and top to bottom.  The pixels are transferred from "buf", starting
					at bit offset "bitofs".  The line byte increment is "stride", which may be positive, negative or zero.
					"fg" and "bg" specify the colors for value 1 and 0 pixels respectively. *)
				проц {перекрыта}Mask*( перем buf: массив из симв8;  bitofs, stride, fg, bg, x, y, w, h: цел32 );
				перем p: адресВПамяти; i: цел32;  s: мнвоНаБитахМЗ;
					image: X11.Image;
					fgpixel, bgpixel, xret: цел32;
					ix, iy, ih: цел32;
					imp: X11.ImagePtr;
				нач
					если (w > 0) и (h > 0) то
						если fg >= 0 то  fgpixel := pixel[fg остОтДеленияНа 256]  иначе  fgpixel := ColorToPixel( fg )  всё;
						если bg >= 0 то  bgpixel := pixel[bg остОтДеленияНа 256]  иначе  bgpixel := ColorToPixel( bg )  всё;

						если длинаМассива( imgBuffer ) < 4*w*h то
							нов( imgBuffer, 4*w*h );	(* create buffer outside lock to avoid deadlock *)
						всё;

						Machine.Acquire( Machine.X11 );
						image := X11.CreateImage( xdisp, visual, depth, X11.ZPixmap, 0, 0, w, h, 32, 0 );
						imp := S.подмениТипЗначения( X11.ImagePtr, image );
						imp.data := адресОт( imgBuffer[0] );
						i := цел32(адресОт( buf[0] ) остОтДеленияНа 4);  увел( bitofs, i*8 );
						p := адресОт( buf[0] ) - i + bitofs DIV 32*4;   (* p always aligned to 32-bit boundary *)
						bitofs := bitofs остОтДеленияНа 32;  stride := stride*8;
						ix := 0; iy := 0; ih := h;
						нц
							S.прочтиОбъектПоАдресу( p, s );  i := bitofs;
							нц
								если (i остОтДеленияНа 32) в s то  xret := X11.PutPixel( image, ix, iy, fgpixel );
								иначе  xret := X11.PutPixel( image, ix, iy, bgpixel );
								всё;
								увел( i );  увел( ix );
								если i - bitofs = w то  прервиЦикл   всё;
								если i остОтДеленияНа 32 = 0 то  S.прочтиОбъектПоАдресу( p + i DIV 8, s )  всё
							кц;
							умень( ih );
							если ih = 0 то  прервиЦикл   всё;
							увел( iy );  ix := 0;  увел( bitofs, stride );
							если (bitofs >= 32) или (bitofs < 0) то  (* moved outside s *)
								увел( p, bitofs DIV 32*4 );  bitofs := bitofs остОтДеленияНа 32
							всё
						кц;  (* loop *)
						если currmode # replace то  X11.SetFunction( xdisp, gc, xfunc[replace] )  всё;
						X11.PutImage( xdisp, primary, gc, image, 0, 0, x, y, w, h );
						если currmode # replace то  X11.SetFunction( xdisp, gc, xfunc[currmode] )  всё;
						X11.Free( image );
						Machine.Release( Machine.X11 );
					всё
				кон Mask;

				(** Copy source block sx, sy, w, h to destination dx, dy.  Overlap is allowed. *)
				проц {перекрыта}Copy*( sx, sy, w, h, dx, dy: цел32 );
				перем src: X11.DisplayPtr;
				нач
					если (w > 0) и (h > 0) то
						если sy  < height то  src := primary  иначе  src := secondary;  умень( sy, height )  всё;
						если currwin = secondary то  умень( sy, height )  всё;
						Machine.Acquire( Machine.X11 );
						SetMode( currcol );
						X11.CopyArea( xdisp, src, currwin, gc, sx, sy, w, h, dx, dy  );
						Machine.Release( Machine.X11 )
					всё;
				кон Copy;


				(** Update the visible display (if caching is used). *)
				проц {перекрыта}Update*;
				нач
					Machine.Acquire( Machine.X11 );
					X11.Sync( xdisp, X11.False );
					Machine.Release( Machine.X11 )
				кон Update;

				(** Map a color value to an 8-bit CLUT index.  Only used if xformat = index8. *)
				проц {перекрыта}ColorToIndex*( col: цел32 ): цел32;
				нач
					возврат ColorToIndex0( сам, col )
				кон ColorToIndex;

				(** Map an 8-bit CLUT index to a color value.  Only used if xformat = index8. *)
				проц {перекрыта}IndexToColor*( n: цел32 ): цел32;
				перем r, g, b: цел32;
				нач
					если n >= 0 то
						если n > 255 то  n := BG  всё;
						r := rgb[n].r;  g := rgb[n].g;  b := rgb[n].b;
						возврат матМинимум( цел32 ) + (r*100H + g)*100H + b
					иначе  возврат n
					всё;
				кон IndexToColor;

				проц SetColor*( col, red, green, blue: цел16 );   (* 0 <= col, red, green, blue < 256 *)
				перем xcol: X11.Color;  res: целМЗ;
				нач
					если (col < 0) или (col > 255) то  возврат   всё;
					rgb[col].r := red;  rgb[col].g := green;  rgb[col].b := blue;
					xcol.red := 256*red;  xcol.green := 256*green;  xcol.blue := 256*blue;
					Machine.Acquire( Machine.X11 );
					если depth > 8 то
						res := X11.AllocColor( xdisp, cmap, адресОт( xcol ) );
						если res # 0 то  pixel[col] := цел32 (xcol.pixel)  всё
					иначе
						xcol.flags := симв8ИзКода( X11.DoAll );  xcol.pixel := pixel[col];
						X11.StoreColor( xdisp, cmap, адресОт( xcol ) )
					всё;
					Machine.Release( Machine.X11 )
				кон SetColor;

				проц GetColor*( col: цел16;  перем red, green, blue: цел16 );
				нач
					если (0 <= col) и (col <= 255) то
						red := rgb[col].r;  green := rgb[col].g;  blue := rgb[col].b
					иначе
						red := rgb[BG].r;  green := rgb[BG].g;  blue := rgb[BG].b
					всё
				кон GetColor;

				проц ColorToPixel*( col: цел32 ): цел32;
				перем r, g, b, i, ii, x, y, z, m, min: цел32;  rc: RGB;
				нач
					r := логСдвиг( col, -16 ) остОтДеленияНа 256;  g := логСдвиг( col, -8 ) остОтДеленияНа 256;  b := col остОтДеленияНа 256;
					просей xformat из
					color8888, color888:
								если bigEndian то  возврат арифмСдвиг( b, 16 ) + арифмСдвиг( g, 8 ) + r
								иначе  возврат арифмСдвиг( r, 16 ) + арифмСдвиг( g, 8 ) + b
								всё
					| color555:
								r := 32*r DIV 256;  g := 32*g DIV 256;  b := 32*b DIV 256;
								если bigEndian то  возврат арифмСдвиг( b, 10 ) + арифмСдвиг( g, 5 ) + r
								иначе  возврат арифмСдвиг( r, 10 ) + арифмСдвиг( g, 5 ) + b
								всё
					| color565:
								r := 32*r DIV 256;  g := 64*g DIV 256;  b := 32*b DIV 256;
								если bigEndian то  возврат арифмСдвиг( b, 11 ) + арифмСдвиг( g, 5 ) + r
								иначе  возврат арифмСдвиг( r, 11 ) + арифмСдвиг( g, 5 ) + b
								всё
					| color664:
								r := 64*r DIV 256;  g := 64*g DIV 256;  b := 16*b DIV 256;
								если bigEndian то  возврат арифмСдвиг( b, 12 ) + арифмСдвиг( g, 6 ) + r
								иначе  возврат арифмСдвиг( r, 10 ) + арифмСдвиг( g, 4 ) + b
								всё
					иначе  (* index8 *)
						i := 0;  ii := 0;  min := матМаксимум( цел32 );
						нцПока (i < 256) и (min > 0) делай
							rc := rgb[i];
							x := матМодуль( r - rc.r );  y := матМодуль( g - rc.g );  z := матМодуль( b - rc.b );  m := x;
							если y > m то  m := y  всё;
							если z > m то  m := z  всё;
							m := m*m + (x*x + y*y + z*z);
							если m < min то  min := m;  ii := i  всё;
							увел( i )
						кц;
						возврат pixel[ii]
					всё
				кон ColorToPixel;



				проц  & Initialize( disp: X11.DisplayPtr; absWidth, absHeight, relWidth, relHeight: цел32 );
				перем
					event: X11.Event;  root: X11.Window;
					gRoot, gX, gY, gW, gH, gBW, gD, res: цел32;  screenw, screenh	: цел32;
				нач
					xdisp := disp;
					Machine.Acquire( Machine.X11 );
					screen := X11.DefaultScreen( xdisp );
					screenw := X11.DisplayWidth( xdisp, screen );
					screenh := X11.DisplayHeight( xdisp, screen );
					depth := X11.DefaultDepth( xdisp, screen );
					cmap := X11.DefaultColormap( xdisp, screen );
					foreground := X11.BlackPixel( xdisp, screen );
					background := X11.WhitePixel( xdisp, screen );
					root := X11.DefaultRootWindow( xdisp );
					primary := X11.CreateSimpleWindow( xdisp, root, 0, 0,
											     			screenw - 16, screenh - 32, 0,
														foreground, background );
					X11.StoreName( xdisp, primary, адресОт( winName ) );
					X11.SetIconName( xdisp, primary, адресОт( iconName ) );
					X11.SetCommand( xdisp, primary, Unix.argv, Unix.argc );
					X11.SelectInput( xdisp, primary, X11.ExposureMask );


					(* set wm_delete_events if in windowed mode *)
					wmDelete := X11.InternAtom(xdisp, "WM_DELETE_WINDOW", X11.True);
					res := X11.SetWMProtocols(xdisp, primary, адресОт(wmDelete), 1);

					X11.MapRaised( xdisp, primary );
					нцДо  X11.NextEvent( xdisp, event )
					кцПри (event.typ = X11.Expose) и (event.window = primary);
					(* adjust to physical window size *)
					X11.GetGeometry( xdisp, primary, gRoot, gX, gY, gW, gH, gBW, gD );
					если relWidth # -1 то
						gW := relWidth * gW DIV 100;
					иначе
						gW := absWidth;
					всё;
					если relHeight # -1 то
						gH := relHeight * gH DIV 100;
					иначе
						gH := absHeight;
					всё;
					если gW остОтДеленияНа 8 # 0 то  умень( gW, gW остОтДеленияНа 8 )  всё;
					X11.ResizeWindow( xdisp, primary, gW, gH );
					width := gW;  height := gH;
					offscreen := height;

					(* pixmap may not be larger than screen: *)
					если gW > screenw то  gW := screenw  всё;
					если gH > screenh то  gH := screenh  всё;
					secondary := X11.CreatePixmap( xdisp, primary, gW, gH, depth );
					Machine.Release( Machine.X11 );

					CreateColors( сам );  InitPalette( сам );  SuppressX11Cursors( сам );
					InitFormat( сам );  CreateGC( сам );  InitFunctions( сам );

					нов( clip, сам )
				кон Initialize;


				(** Finalize the display.  Further calls to display methods are not allowed, and may cause exceptions. *)
				проц {перекрыта}Finalize*;
				(*
				BEGIN  (* should really be exclusive with Transfer, but we assume the caller keeps to the rules above *)
					fbadr := 0;  fbsize := 0
				*)
				кон Finalize;


			кон Display;

перем
	dispname: массив 128 из симв8;

	imgBuffer: укль на массив из симв8;


	проц ColorToIndex0( disp: Display; col: цел32 ): цел16;
	перем idx, i: цел16;  r, g, b, min, x, y, z, d: цел32;  rc: RGB;
	нач
		r := арифмСдвиг( col, -16 ) остОтДеленияНа 100H;  g := арифмСдвиг( col, -8 ) остОтДеленияНа 100H;  b := col остОтДеленияНа 100H;
		i := 0;  idx := 0;  min := матМаксимум( цел32 );
		нцПока (i < 256) и (min > 0) делай
			rc := disp.defPal[i];  x := матМодуль( r - rc.r );  y := матМодуль( g - rc.g );  z := матМодуль( b - rc.b );  d := x;
			если y > d то  d := y  всё;
			если z > d то  d := z  всё;
			d := d*d + (x*x + y*y + z*z);
			если d < min то  min := d;  idx := i  всё;
			увел( i )
		кц;
		возврат idx
	кон ColorToIndex0;


	проц PutLine( xformat, width: цел32;  ip, bp: адресВПамяти );
	перем i: цел32;  byte: симв8;
	нач
		просей xformat из
		| index8:
				S.копируйПамять(bp, ip, width);
		| color565, color555, color664:
				S.копируйПамять(bp, ip, 2*width);
		| color888: (* x-format (destination) is 888, A ignored (?) , Aos Format (source) is 8888 *)
				S.копируйПамять(bp, ip, 4*width);
				(*
				FOR i := 1 TO width DO
					S.MOVE(bp,ip,3); INC(bp,3); INC(ip,3);
					(*
					S.GET( bp, byte );  S.PUT( ip, byte );  INC( bp );  INC( ip );   (* B *)
					S.GET( bp, byte );  S.PUT( ip, byte );  INC( bp );  INC( ip );   (* G *)
					S.GET( bp, byte );  S.PUT( ip, byte );  INC( bp );  INC( ip );   (* R *)
					*)
					byte := 0X;  S.PUT( ip, byte );  INC( ip )
				END;
				*)
		иначе  (* color8888 *)
				S.копируйПамять(bp, ip, 4*width);
		всё
	кон PutLine;



	проц GetLine( xformat, width: цел32;  ip, bp: адресВПамяти );
	перем i: цел32;  byte: симв8;
	нач
		просей xformat из
		| index8:
				S.копируйПамять(ip, bp, width);
		| color565, color555, color664:
				S.копируйПамять(ip, bp, 2*width);
		| color888:
				S.копируйПамять(ip, bp, 4*width);
				(*
				FOR i := 1 TO width DO
					S.GET( ip, byte );  S.PUT( bp, byte );  INC( ip );  INC( bp );   (* B *)
					S.GET( ip, byte );  S.PUT( bp, byte );  INC( ip );  INC( bp );   (* G *)
					S.GET( ip, byte );  S.PUT( bp, byte );  INC( ip );  INC( bp );   (* R *)
					INC( ip )
				END
				*)
		иначе  (* color8888 *)
				S.копируйПамять(ip, bp, 4*width);
		всё;
	кон GetLine;


	проц PutLineBE( xformat, width: цел32;  ip, bp: адресВПамяти );
	перем i: цел32;  byte: симв8;
	нач
		просей xformat из
		index8:
				нцДля i := 1 до width делай
					S.прочтиОбъектПоАдресу( bp, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( bp );  увел( ip )
				кц
		| color565, color555, color664:
				нцДля i := 1 до width делай
					S.прочтиОбъектПоАдресу( bp + 1, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );
					S.прочтиОбъектПоАдресу( bp + 0, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );
					увел( bp, 2 )
				кц
		| color888:
				нцДля i := 1 до width делай
					S.запиши8битПоАдресу( ip, 0X );  увел( ip );
					S.прочтиОбъектПоАдресу( bp + 2, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );   (* B *)
					S.прочтиОбъектПоАдресу( bp + 1, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );   (* G *)
					S.прочтиОбъектПоАдресу( bp + 0, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );   (* R *)
					увел( bp, 4 )
				кц
		иначе  (* color8888 *)
				нцДля i := 1 до width делай
					S.прочтиОбъектПоАдресу( bp + 3, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );   (* X *)
					S.прочтиОбъектПоАдресу( bp + 2, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );   (* B *)
					S.прочтиОбъектПоАдресу( bp + 1, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );   (* G *)
					S.прочтиОбъектПоАдресу( bp + 0, byte );  S.запишиОбъектПоАдресу( ip, byte );  увел( ip );   (* R *)
					увел( bp, 4 );
				кц;
		всё;
	кон PutLineBE;

	проц GetLineBE( xformat, width: цел32;  ip, bp: адресВПамяти );
	перем i: цел32;  byte: симв8;
	нач
		просей xformat из
		| index8:
				нцДля i := 1 до width делай
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp, byte );  увел( ip );  увел( bp )
				кц
		| color565, color555, color664:
				нцДля i := 1 до width делай
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 1, byte );  увел( ip );
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 0, byte );  увел( ip );
					увел( bp, 2 )
				кц
		| color888:
				нцДля i := 1 до width делай
					увел( ip );
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 2, byte );  увел( ip );   (* B *)
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 1, byte );  увел( ip );   (* G *)
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 0, byte );  увел( ip );   (* R *)
					увел( bp, 3 )
				кц;
		иначе  (* color8888 *)
				нцДля i := 1 до width делай
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 3, byte );  увел( ip );   (* X *)
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 2, byte );  увел( ip );   (* B *)
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 1, byte );  увел( ip );   (* G *)
					S.прочтиОбъектПоАдресу( ip, byte );  S.запишиОбъектПоАдресу( bp + 0, byte );  увел( ip );   (* R *)
					увел( bp, 4 )
				кц;
		всё;
	кон GetLineBE;



	проц NewPattern( d: Display;
							конст image: массив из мнвоНаБитахМЗ;
							width, height: цел16 ): X11.Pattern;
	перем
		pixmap: X11.Pixmap;  pat {неОтслСборщиком}: X11.PatternPtr;
		w, h, i, j, b, dest, srcw, destb, srci, desti: цел32;
		data: массив 256*32 из симв8;   (* 256*256 bits *)
	нач
		i := 0;
		нцПока i < длинаМассива( data ) делай  data[i] := 0X;  увел( i )  кц;
		w := width;  h := height;
		srcw := (width + 31) DIV 32;   (* number of words in source line *)
		destb := (w + 7) DIV 8;   (* number of bytes in dest line *)
		srci := (height - 1)*srcw;  desti := 0;
		нцПока srci >= 0 делай
			i := 0;  j := 0;  b := 0;  dest := 0;
			нц
				dest := dest DIV 2;
				если b в image[srci + j + 1] то  увел( dest, 80H )  всё;
				увел( b );
				если b остОтДеленияНа 8 = 0 то
					data[desti + i] := симв8ИзКода( dest );  увел( i );  dest := 0;
					если i >= destb то  прервиЦикл   всё
				всё;
				если b = 32 то
					b := 0;  увел( j );
					если j >= srcw то
						нцПока i < destb делай  data[desti + i] := 0X;  увел( i )  кц;
						прервиЦикл
					всё
				всё
			кц;
			увел( desti, destb );  умень( srci, srcw )
		кц;
		Machine.Acquire( Machine.X11 );
		pixmap := X11.CreateBitmapFromData( d.xdisp, d.primary, адресОт( data[0] ), w, h );
		Machine.Release( Machine.X11 );
		если pixmap = 0 то  СТОП( 99 )  всё;
		pat := S.подмениТипЗначения( X11.PatternPtr, Unix.malloc( размер16_от( X11.PatternDesc ) ) );
		pat.x := 0;  pat.y := 0;  pat.w := width;  pat.h := height;  pat.pixmap := pixmap;
		возврат S.подмениТипЗначения( X11.Pattern, pat )
	кон NewPattern;

	проц InitNames;
	перем cwd: массив 128 из симв8;  i: цел32;
	нач
		UnixFiles.GetWorkingDirectory( cwd );
		копируйСтрокуДо0( Machine.version, winName );
		Strings.Append( winName, ",  Work: " );  Strings.Append( winName, cwd );
		копируйСтрокуДо0( winName, iconName);
		i := 0;
		нцПока iconName[i] > ' ' делай  увел( i )  кц;
		iconName[i] := 0X
	кон InitNames;

	проц getDisplayName;
	перем adr: адресВПамяти;  i: цел16;  ch: симв8;
	нач
		Unix.GetArgval( "-display", dispname );
		если dispname = "" то
			adr := Unix.getenv( адресОт( "DISPLAY" ) );
			если adr # 0 то
				i := 0;
				нцДо  S.прочтиОбъектПоАдресу( adr, ch );  увел( adr );  dispname[i] := ch;  увел( i )   кцПри ch = 0X;
			иначе  dispname := ":0"
			всё
		всё
	кон getDisplayName;

	проц OpenX11Display( ): X11.DisplayPtr;
	перем xdisp: X11.DisplayPtr;  screen, depth: цел32;
	нач
		getDisplayName;
		Machine.Acquire(Machine.X11);
		xdisp := X11.OpenDisplay( dispname );
		если xdisp = 0 то
			Machine.Release(Machine.X11);
			Трассировка.пСтроку8( "Cannot open X11 display " );  Трассировка.StringLn( dispname );  Unix.exit( 1 )
		всё;
		screen := X11.DefaultScreen( xdisp );
		depth := X11.DefaultDepth( xdisp, screen );
		Machine.Release(Machine.X11);
		если depth < 8 то
			Трассировка.StringLn( "UnixAos needs a color display. sorry." );  Unix.exit( 1 )
		всё;
		Трассировка.пСтроку8( "X11 Display depth = " ); Трассировка.пЦел64( depth, 1 ); Трассировка.пВК_ПС;
		возврат xdisp
	кон OpenX11Display;


	проц CreateColors( d: Display );
	перем col: цел16;
		visualInfo: X11.VisualInfo;
	нач
		Machine.Acquire( Machine.X11 );
		col := 0;
		нцПока col < 256 делай  d.pixel[col] := col;  увел( col )  кц;
		если (d.depth > 8) и (X11.MatchVisualInfo( d.xdisp, d.screen, d.depth, X11.TrueColor, visualInfo ) = 1) то
			d.visual := visualInfo.visual;
		аесли X11.MatchVisualInfo( d.xdisp, d.screen, d.depth, X11.PseudoColor, visualInfo ) = 1 то
			d.visual := visualInfo.visual
		всё;
		d.bigEndian := ложь;
		если d.depth > 8 то
			d.bigEndian := d.visual.blueMask > d.visual.redMask
		иначе (* pseudo color *)
			d.cmap := X11.CreateColormap( d.xdisp, d.primary, d.visual, X11.AllocAll );
			X11.SetWindowColormap( d.xdisp, d.primary, d.cmap );
			d.foreground := d.pixel[FG];
			d.background := d.pixel[BG];
			X11.SetWindowBackground( d.xdisp, d.primary, d.background );
			X11.ClearWindow( d.xdisp, d.primary )
		всё;
		Machine.Release( Machine.X11 );
		d.planesMask := арифмСдвиг( 1, d.depth ) - 1
	кон CreateColors;


	проц InitPalette( d: Display );
	перем f: Files.File;  r: Files.Reader;  red, green, blue: симв8;  i, cols: цел16;
	нач
		если d.depth >= 8 то cols := 256 иначе cols := 16 всё;
		f := Files.Old( "Default.Pal" );
		если f # НУЛЬ то
			Files.OpenReader( r, f, 0 );
			нцДля i := 0 до cols - 1 делай
				r.чСимв8( red );  r.чСимв8( green );  r.чСимв8( blue );
				d.SetColor( i, кодСимв8( red ), кодСимв8( green ), кодСимв8( blue ) )
			кц
		всё;
		d.defPal := d.rgb
	кон InitPalette;


	проц SuppressX11Cursors( d: Display );
	перем
		fg, bg: X11.Color;  src {неОтслСборщиком}, msk {неОтслСборщиком}: X11.PatternPtr;
		image: массив 17 из мнвоНаБитахМЗ;  i: цел16;
		noCursor: X11.Cursor;
	нач
		fg.red := 256*d.rgb[FG].r;  fg.green := 256*d.rgb[FG].g;  fg.blue := 256*d.rgb[FG].b;
		bg.red := 256*d.rgb[BG].r;  bg.green := 256*d.rgb[BG].g;  bg.blue := 256*d.rgb[BG].b;

		нцДля i := 1 до 16 делай  image[i] := {}  кц;
		src := S.подмениТипЗначения( X11.PatternPtr, NewPattern( d, image, 16, 16 ) );
		msk := S.подмениТипЗначения( X11.PatternPtr, NewPattern( d, image, 16, 16 ) );

		Machine.Acquire( Machine.X11 );
		noCursor := X11.CreatePixmapCursor( d.xdisp, src.pixmap, msk.pixmap, fg, bg, 1, 1 );
		X11.DefineCursor( d.xdisp, d.primary, noCursor );
		Machine.Release( Machine.X11 )
	кон SuppressX11Cursors;


	проц InitFormat( d: Display );
	нач
		если d.depth = 8 то
			d.format := Displays.index8;
			d.xformat := index8
		аесли d.depth = 15 то
			d.format := Displays.color565;
			d.xformat := color555
		аесли d.depth = 16 то
			d.format := Displays.color565;
			если d.visual.blueMask = 0FH то  d.xformat := color664
			иначе  d.xformat := color565
			всё
		аесли d.depth = 24 то
			d.format := Displays.color8888;
			d.xformat := color888
		аесли d.depth = 32 то
			d.format := Displays.color8888;
			d.xformat := color8888
		иначе
			d.format := unknown
		всё;
	кон InitFormat;

	проц CreateGC( d: Display );
	нач
		Machine.Acquire( Machine.X11 );
		d.gc := X11.CreateGC( d.xdisp, d.primary, 0, 0 );
		если d.gc = 0 то  Machine.Release( Machine.X11 );  СТОП( 45 )  всё;
		X11.SetPlaneMask( d.xdisp, d.gc, d.planesMask );
		X11.SetGraphicsExposures( d.xdisp, d.gc, X11.True );
		X11.SetBackground( d.xdisp, d.gc, d.background );
		Machine.Release( Machine.X11 );
	кон CreateGC;

	проц InitFunctions( d: Display );
	нач
		d.xfunc[replace] := X11.GXcopy;
		d.xfunc[paint] := X11.GXor;   (* not used *)
		(* drawing in invert mode with BackgroundCol on BackgroundCol is a no-op: *)
		если мнвоНаБитахМЗ(d.background )* мнвоНаБитахМЗ(d.planesMask ) # {} то
			d.xfunc[invert] := X11.GXequiv
		иначе
			d.xfunc[invert] := X11.GXxor
		всё;
		d.currcol := -1;  d.currmode := -1;
	кон InitFunctions;


	(* PB - 2010-04-20
		Return:
			-1: absolute width and height according to DisplaySize config string.
			else: value from 50 to 100 as scaling factor, argument variables width and height are unspecified.

		Lower limit is either 50% as scaling factor or 640x480 as absolute size.
	*)
	проц GetDisplaySize(перем width, height: цел32): цел32;  (* % of Screen [50% ... 100%] *)
	перем buf: массив 64 из симв8; size, i: цел32; c: симв8; absolute: булево;
	нач
		Machine.GetConfig( "DisplaySize", buf );
		если buf = "" то size := 100
		иначе
			size := 0; c := buf[0];  i := 0;
			нцПока (c >= '0') и (c <= '9') делай
				size := 10*size + кодСимв8( c ) - кодСимв8( '0' );
				увел( i ); c := buf[i]
			кц;
			если c = 'x' то
				width := size;
				size := 0;
				увел( i ); c := buf[i];
			всё;
			нцПока (c >= '0') и (c <= '9') делай
				size := 10*size + кодСимв8( c ) - кодСимв8( '0' );
				увел( i ); c := buf[i]
			кц;
			если (width # 0) и (size # 0) то
				height := size;
				absolute := истина;
			аесли (width # 0) то (* failed to read two numbers -> fall back to scaling *)
				size := width;
				width := 0
			всё;
			если absolute то
				size := -1;
				если width < 640 то width := 640; всё;
				если height < 480 то height := 480; всё;
			иначе
				если size < 50 то  size := 50  всё;
				если size > 100 то  size := 100  всё
			всё;
		всё;
		возврат size
	кон GetDisplaySize;

	проц Install*;
	перем disp: Display; res: целМЗ; s, w, h: цел32; xdisp: X11.DisplayPtr;
	нач
		InitNames; xdisp := OpenX11Display( );
		s := GetDisplaySize( w, h );
		нов( disp, xdisp, w, h, s, s );
		disp.SetName( "XDisplay" );
		disp.desc := "X11 display driver";
		Displays.registry.Add( disp, res );
	кон Install;

нач
	нов( imgBuffer, 10000 )
кон XDisplay.
