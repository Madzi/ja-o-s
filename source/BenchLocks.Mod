модуль BenchLocks;	(* pjm *)

(* Test lock performance *)

использует Machine, Kernel, Commands;

конст
	Level = Machine.KernelLog;

тип

	Thread = окласс
	перем
		nofLoops, holdTime : цел32;
		i, j : цел32;

		проц &Init*(nofLoops, holdTime : цел32);
		нач
			утв((nofLoops > 0) и (holdTime >= 0));
			сам.nofLoops := nofLoops;
			сам.holdTime := holdTime;
		кон Init;

	нач {активное}
		нц
			нцДля i := 7 до 0 шаг -1 делай
				Machine.Acquire(i);
				нцДля j := 0 до holdTime делай
					(* skip *)
				кц;
				Machine.Release(i);
			кц;
			умень(nofLoops);
			если (nofLoops <= 0) то прервиЦикл; всё;
		кц;
		DecNofThreadsRunning;
	кон Thread;

перем
	nofThreadsRunning : цел32;

проц DecNofThreadsRunning;
нач {единолично}
	умень(nofThreadsRunning);
кон DecNofThreadsRunning;

проц Bench*(context : Commands.Context); (** nofThreads nofLoops [holdTime] ~*)
перем start, nofThreads, nofLoops, holdTime : цел32; i : размерМЗ; threads : укль на массив из Thread;
нач {единолично}
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(nofThreads, ложь);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(nofLoops, ложь);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(holdTime, ложь);
	если (nofThreads > 0) и (nofLoops > 0) и (holdTime >= 0) то
		context.out.пСтроку8("Starting "); context.out.пЦел64(nofThreads, 0);
		context.out.пСтроку8(" threads where each acquires each kernel lock ");
		context.out.пЦел64(nofLoops, 0); context.out.пСтроку8(" times (HoldTime: ");
		context.out.пЦел64(holdTime, 0); context.out.пСтроку8(") ..."); context.out.ПротолкниБуферВПоток;
		nofThreadsRunning := nofThreads;
		нов(threads, nofThreads);
		start := Kernel.GetTicks();
		нцДля i := 0 до длинаМассива(threads)-1 делай
			нов(threads[i], nofLoops, holdTime);
		кц;
		дождись(nofThreadsRunning = 0);
		context.out.пСтроку8("Time required: "); context.out.пЦел64(Kernel.GetTicks() - start, 0);
		context.out.пСтроку8(" ms");
	иначе
		context.out.пСтроку8("Parameter error: nofThreads & nofLoops must be > 0, holdTime must be >= 0");
	всё;
	context.out.пВК_ПС;
кон Bench;

проц TestAcquire*(context : Commands.Context);	(* num *)
перем i, n, t: цел32;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(n, ложь) и (n > 0) то
		i := Kernel.GetTicks();
		нцДо t := Kernel.GetTicks() кцПри t # i;
		нцДля i := 1 до n делай
			Machine.Acquire(Level);
			Machine.Release(Level)
		кц;
		t := Kernel.GetTicks() - t;
		context.out.пЦел64(n, 1); context.out.пСтроку8(" loops, ");
		context.out.пЦел64(t*1000 DIV Kernel.second, 1); context.out.пСтроку8(" ms");
		context.out.пВК_ПС;
	всё;
кон TestAcquire;

кон BenchLocks.

System.Repeat 3
	BenchLocks.Bench 32 100000 500 ~

BenchLocks.Bench 64 100000 500 ~

System.Free BenchLocks ~

Configuration.DoCommands
System.Time start
Aos.Call \w BenchLocks.TestAcquire 10000000 ~
System.Time lap
~

{P1 1000000 loops, 6105 ms} with Stats and nestCount
{P1 1000000 loops, 6005 ms} removed nestCount
{P1 1000000 loops, 2270 ms} disabled Stats
{P1 1000000 loops, 2201 ms} added quick acquire
