(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль BootConsole; (** AUTHOR "pjm"; PURPOSE "Boot console"; *)
(*
Config strings:
	BootVol# = prefix [hash] [cache] alias [volpar] ["|" fspar].	(* # is "1".."9" *)
	alias = gen gen .	(* defines a volume and file system generator *)
	gen = mod "." cmd .

Examples:
	BootVol1="AOS AosFS IDE0#2"
	BootVol2="RAM RamFS 2000 4096"

	AosFS="DiskVolumes.New DiskFS.NewFS"
	RamFS="RAMVolumes.New DiskFS.NewFS"
	RFS="RfsClientProxy.New RfsFS.NewFS"
*)

использует
	Machine, Трассировка, ЛогЯдра, Modules, Потоки, Objects, Files, Commands;

конст
	ModuleName = "Console";
	TraceBoot = ложь;

проц BootCommand(конст config: массив из симв8; flags: мнвоНаБитахМЗ);
перем i, j: цел32; res: целМЗ; par: массив 32 из симв8; s: массив 256 из симв8;
нач
	копируйСтрокуДо0(config, par);
	i := 0; j := 0; нцПока par[j] # 0X делай увел(j) кц;
	нц
		Machine.GetConfig(par, s);
		если s # "" то
			если TraceBoot то Трассировка.пСтроку8("Bootconsole:Commands.Call: "); Трассировка.пСтроку8(s); Трассировка.пВК_ПС всё;
			Commands.Call(s, flags, res, s);
			если (res # Commands.Ok) то ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8(s); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё
		всё;
		увел(i);
		если i = 10 то прервиЦикл всё;
		par[j] := симв8ИзКода(кодСимв8("0") + i); par[j+1] := 0X
	кц
кон BootCommand;

проц GetString(перем i: цел32; конст r : массив из симв8; перем s: массив из симв8): булево;
перем j: цел32;
нач
	нцПока r[i] = " " делай увел(i) кц;
	j := 0; нцПока r[i] > " " делай s[j] := r[i]; увел(j); увел(i) кц;
	s[j] := 0X;
	если TraceBoot то Трассировка.пСтроку8("GetString: "); Трассировка.пСтроку8(s); Трассировка.пВК_ПС всё;
	возврат j # 0
кон GetString;

проц Error(конст config, val: массив из симв8; i: цел32);
перем j: цел32; s: массив 32 из симв8;
нач
	s := "BootConsole: Bad ";
	ЛогЯдра.пСтроку8(s);
	j := 0; нцПока s[j] # 0X делай увел(j) кц; увел(i, j);
	ЛогЯдра.пСтроку8(config);
	j := 0; нцПока config[j] # 0X делай увел(j) кц; увел(i, j);
	ЛогЯдра.пСимв8("="); ЛогЯдра.пСимв8(22X); увел(i, 2);
	ЛогЯдра.пСтроку8(val); ЛогЯдра.пСимв8(22X); ЛогЯдра.пВК_ПС;
	нцПока i > 0 делай ЛогЯдра.пСимв8(" "); умень(i) кц;
	ЛогЯдра.пСимв8("^"); ЛогЯдра.пВК_ПС
кон Error;

проц Generate(конст name: массив из симв8; par: Files.Parameters): булево;
перем
	factory : Files.FileSystemFactory; res: целМЗ; msg: массив 256 из симв8;
	moduleName, procedureName : Modules.Name;
нач
	Commands.Split(name, moduleName, procedureName, res, msg);
	если (res = Commands.Ok) то
		дайПроцПоИмени(moduleName, procedureName, factory);
		если (factory # НУЛЬ) то
			factory(par);
			возврат истина;
		иначе
			ЛогЯдра.пСтроку8(ModuleName); ЛогЯдра.пСтроку8(": File system alias unknown"); ЛогЯдра.пВК_ПС;
		всё;
	иначе
		ЛогЯдра.пСтроку8(ModuleName); ЛогЯдра.пСтроку8(": ");  ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
	всё;
	возврат ложь;
кон Generate;

проц OpenVolume(конст config: массив из симв8);
перем
	i, j, k: цел32; parvol, parfs: Files.Parameters;
	volReady : булево;
	prefix, alias: Files.Prefix; gen: массив 64 из симв8; s: массив 256 из симв8;
	argVol, argFs : Потоки.ЧтецИзСтроки;
нач
	Machine.GetConfig(config, s); (* s = prefix alias [volpar] ["|" fspar] . *)
	если s = "" то возврат всё;
	i := 0;
	если ~GetString(i, s, prefix) то Error(config, s, i); возврат всё;
	если ~GetString(i, s, alias) то Error(config, s, i); возврат всё;

	(* generate volume generator parameter *)
	если s[i] = " " то увел(i) всё;
	j := 0; нцПока (s[i] # 0X) и (s[i] # "|") делай s[j] := s[i]; увел(i); увел(j) кц;
	если s[i] = "|" то увел(i) всё;
	s[j] := 0X;
	нов(argVol, j+1); argVol.ПримиСрезСтроки8ВСтрокуДляЧтения(s, 0, j+1);
	нов(parvol, НУЛЬ, argVol, НУЛЬ, НУЛЬ, НУЛЬ);

	(* generate file system generator parameter *)
	j := 0; нцПока s[i] # 0X делай s[j] := s[i]; увел(i); увел(j) кц;
	s[j] := 0X;
	нов(argFs, j+1); argFs.ПримиСрезСтроки8ВСтрокуДляЧтения(s, 0, j+1);
	нов(parfs, НУЛЬ, argFs, НУЛЬ, НУЛЬ, НУЛЬ);

	(* call volume generator *)
	Machine.GetConfig(alias, s);	(* s = gen gen . ; gen = mod "." cmd . *)
	k := 0;
	если ~GetString(k, s, gen) то Error(alias, s, k); возврат всё;

	(* call volume generator *)
	volReady := ложь;
	если gen = "NIL" то volReady := истина
	иначе
		если Generate(gen, parvol) и (parvol.vol # НУЛЬ) то
			включиВоМнвоНаБитах(parvol.vol.flags, Files.Boot); parfs.vol := parvol.vol;
			volReady := истина
		всё
	всё;
	если volReady то
		копируйСтрокуДо0(prefix, parfs.prefix);
		(* call file system generator *)
		если GetString(k, s, gen) то
			если Generate(gen, parfs) то parvol.vol := НУЛЬ всё
		иначе
			Error(alias, s, k)
		всё
	всё;
	если Files.This(prefix) = НУЛЬ то
		ЛогЯдра.пСтроку8("BootConsole: Mount failed on "); ЛогЯдра.пСтроку8(config); ЛогЯдра.пВК_ПС;
		если parvol.vol # НУЛЬ то
			parvol.vol.Finalize()	(* unmount volume *)
		всё
	всё;
	parfs.out.ПротолкниБуферВПоток; parfs.error.ПротолкниБуферВПоток;
	parvol.out.ПротолкниБуферВПоток; parvol.error.ПротолкниБуферВПоток;
кон OpenVolume;

проц OpenVolumes;
перем config: массив 16 из симв8; i: цел32;
нач
	config := "BootVol#";
	нцДля i := 1 до 9 делай
		config[7] := симв8ИзКода(кодСимв8("0") + i); config[8] := 0X;
		OpenVolume(config)
	кц
кон OpenVolumes;

нач
	OpenVolumes;
	BootCommand("Boot", {Commands.Wait});
	BootCommand("BootSystem", {});
	Objects.Terminate();
кон BootConsole.
