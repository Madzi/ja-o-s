(* Paco, Copyright 2000, Patrik Reali, ETH Zurich *)

модуль PCOARM;	(** be  **)

(** Code Generator for ARM. Not concurrent ! *)

использует НИЗКОУР, Files, PCLIR, PCM(*Trace, PCARMDecoder *);

конст
	(*Trace = FALSE; *)

	INTERNALERROR* = 100;
	UNIMPLEMENTED* = 101;
	NONORTHOGONALITYEXCEPTION* = 102;
	ErrBranchOffsetTooBig* = 110;
	ErrImmediateTooSmall* = 111;
	ErrImmediateTooBig* = 112;
	ErrRotateImmTooBig* = 113;
	ErrRotateImmOdd* = 114;
	ErrInvalidRegister* = 115;
	ErrInvalidRegisterSet* = 116;
	ErrInvalidMode* = 117;
	ErrCaseOffsetTooBig* = 118;

	MaxCodeLength* = 256*1024;

	InstructionSize* = 4; (* size of one instruction, in bytes *)

	(** Conditions - instruction is executed if CPSR satisfies the condition *)
	EQ* = { };	(** equal *)
	NE* = { 28 };	(** not equal *)
	CS* = { 29 };	(** carry set *)
	HS* = CS;	(** unsigned higher or same *)
	CC* = { 29, 28 };	(** carry clear *)
	LO* = CC;	(** unsigned lower *)
	MI* = { 30 };	(** minus/negative *)
	PL* = { 30, 28 };	(** plus/positive or zero *)
	VS* = { 30, 29 };	(** overflow *)
	VC* = { 30, 29, 28 };	(** no overflow *)
	HI* = { 31 };	(** unsigned higher *)
	LS* = { 31, 28 };	(** unsigned lower or same *)
	GE* = { 31, 29 };	(** signed greater or equal *)
	LT* = { 31, 29, 28 };	(** signed less than *)
	GT* = { 31, 30 };	(** signed greater than *)
	LE* = { 31, 30, 28 };	(** signed less than or equal *)
	AL* = { 31, 30, 29 };	(** always *)
	CondMask* = { 31, 30, 29, 28 };

	(** Registers *)
	R0* = 0; R1* = 1; R2* = 2; R3* = 3; R4* = 4; R5* = 5; R6* = 6; R7* = 7; R8* = 8; R9* = 9; R10* = 10; R11* = 11;
	FP* = 12;	(** frame pointer *)
	SP* = 13;	(** stack pointer *)
	LR* = 14;	(** return address *)
	PC* = 15;	(** program counter *)
	Registers* = {15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 }; (** valid registers *)
	CPSR* = {};	(** current program status register *)
	SPSR* = { 22 };	(** saved program status register *)
	CR0* = 0; CR1* = 1; CR2* = 2; CR3* = 3; CR4* = 4; CR5* = 5; CR6* = 6; CR7* = 7; CR8* = 8; CR9* = 9; CR10* = 10;
	CR11* = 11; CR12* = 12; CR13* = 13; CR14* = 14; CR15* = 15; (** coprocessor registers *)

	(** PSR flags *)
	PSRc* = { 16 };	(** control fields *)
	PSRx* = { 17 };	(** extension fields *)
	PSRs* = { 18 };	(** status fields *)
	PSRf* = { 19 };	(** flags fields *)

	(** useful Bit-Masks *)
	Mask24* = { 0..23 };
	MaskRd* = { 15, 14, 13, 12 };

	(** Common modifiers *)
	Sflag* = { 20 };	(** if set, the condition codes are updated *)
	Bflag* = { 22 };	(** distinguishes between a SWP and a SWPB instruction *)
	Lflag* = { 22 };	(** LDC/STC: specifies long load/store *)
	Lsl* = { };	(** logical shift left *)
	LSR* = { 5 };	(** logical shift right *)
	Asr* = { 6 };	(** arithmetic shift right *)
	Ror* = { 6, 5 };	(** rotate right *)
	RRX* = Ror;	(** rotate right with extend *)
	ShiftMask = { 6, 5 };
	Load* = { 20 };	(** load *)
	Store* = { };	(** store *)
	IdxAdd* = { 23 };	(** load/store: the index is added to the base register *)
	IdxSub* = { };	(** load/store: the index is subtracted from the base register *)
	Offset* = { 24 };	(** load/store: offset addressing *)
	PreIdxd* = { 24, 21 };	(** load/store: pre-indexed addressing *)
	PostIdxd* = { };	(** laod/store: post-indexed addressing *)

	(** Addressing Mode 1 - Data-processing operands *)
	A1Imm* = { 25 };	(** shifterOperand contains an 8-bit immediate value plus a 4-bit rotate immediate value *)
	A1Reg* =  { };	(** shifterOperand contains a register *)
	A1ShiftImm* = { };	(** shifterOperand contains a register and is shifted by an immeditae *)
	A1ShiftReg* = { 4 }; 	(** shifterOperand contains a register and is shifted by a register *)

	A1Immediate0* = {};	(** prepared immediate values: 0, 1, 2, 4 and 8 *)
	A1Immediate1* = { 0 };
	A1Immediate2* = { 1 };
	A1Immediate4* = { 2 };
	A1Immediate8* = { 3 };
	A1Immediate31* = { 4, 3, 2, 1, 0 };

	(** Addressing Mode 2 - Load and Store Words or Unsigned Byte *)
	A2Mode* = { 26 };	(** load/store word/unsigned byte *)
	A2Word* = { };	(** load/store word *)
	A2Byte* = { 22 };	(** load/store byte *)
	A2Imm* = { };	(** address contains an immediate value *)
	A2Reg* = { 25 };	(** address contains a register *)
	A2WImmOffset* = A2Word + A2Imm + Offset;
	A2WRegOffset* = A2Word + A2Reg + Offset;
	A2BImmOffset* = A2Byte + A2Imm + Offset;
	A2BRegOffset* = A2Byte + A2Reg + Offset;
	A2WImmPreIdxd* = A2Word + A2Imm + PreIdxd;
	A2WRegPreIdxd* = A2Word + A2Reg + PreIdxd;
	A2WImmPostIdxd* = A2Word + A2Imm + PostIdxd;
	A2WRegPostIdxd* = A2Word + A2Reg + PostIdxd;
	A2BImmPostIdxd* = A2Byte + A2Imm + PostIdxd;
	A2BRegPostIdxd* = A2Byte + A2Reg + PostIdxd;
	A2AddrModeMask = { 25, 24, 23, 22, 21 };

	(** Addressing Mode 3 - Miscellaneous Loads and Stores *)
	A3Mode* = { 7, 4 };	(** micellaneous load/store *)
	A3Halfword* = { 5 };	(** load/store halfword *)
	A3Byte* = { };	(** load/store byte *)
	A3Imm* = { 22 };	(** address contains an immediate value *)
	A3Reg* =  { };	(** address contains a register *)
	A3Signed* = { 6 };	(** signed halfword/byte *)
	A3Unsigned* = { };	(** unsigned halfword/byte *)
	A3SHImmOffset* = A3Halfword + A3Imm + A3Signed + Offset;
	A3UHImmOffset* = A3Halfword + A3Imm + A3Unsigned + Offset;
	A3SHRegOffset* = A3Halfword + A3Reg + A3Signed + Offset;
	A3UHRegOffset* = A3Halfword + A3Reg + A3Unsigned + Offset;
	A3SBImmOffset* = A3Byte + A3Imm + A3Signed + Offset;
	A3UBImmOffset* = A3Byte + A3Imm + A3Unsigned + Offset;
	A3SBRegOffset* = A3Byte + A3Reg + A3Signed + Offset;
	A3UBRegOffset* = A3Byte + A3Reg + A3Unsigned + Offset;
	A3AddrModeMask = { 24, 23, 22, 21, 7, 6, 5, 4 };

	(** Addressing Mode 4 - Load and Store Multiple *)
	A4IA* = { 23 };	(** increment after *)
	A4IB* = { 24, 23 };	(** increment before *)
	A4DA* = { }; 	(** decrement after *)
	A4DB* = { 24 };	(** decrement before *)
	A4W* = { 21 };	(** update address register *)
	A4User* = { 22 };	(** load/store user mode registers *)
	A4LDMMask* = { 20 };	(** if this bit is set, it's a LDM (ifffff it's a addressing mode 4 instruction, Hobbes, idiot) *)

	(** Addressing Mode 5 - Load and Store Coprocessor *)
	A5W* = { 21 };	(** update base register *)
	A5Offset* = { };	(** offset addressing *)
	A5PreIdxd* = { 21 };	(** pre-indexed addressing *)
	A5PostIdxd* = { 24, 21 };	(** post-indexed addressing *)
	A5UnIdxd* = { 24 };	(** unindexed addressing *)

	(** Miscellaneous *)
	MSRImmediate* = { 25 };	(** the operand is a 8bit immediate *)
	MSRRegister* = { };	(** the operand is a register *)


	(** Instruction Opcodes *)

	(* data-processing instructions *)
	opADC* = { 23, 21 };
	opADD* = { 23 };
	opAND* = { };
	opBIC* = { 24, 23, 22 };
	opCMN* = { 24, 22, 21, 20 };
	opCMP* = { 24, 22, 20 };
	opEOR* = { 21 };
	opMOV* = { 24, 23, 21 };
	opMVN* = { 24, 23, 22, 21 };
	opORR* = { 24, 23 };
	opRSB* = { 22, 21 };
	opRSC* = { 23, 22, 21 };
	opSBC* = { 23, 22 };
	opSUB* = { 22 };
	opTEQ* = { 24, 21, 20 };
	opTST* =  { 24, 20 };

	opMRS* = { 24, 19, 18, 17, 16 };
	opMSR* =  { 24, 21, 15, 14, 13, 12 };

	(* multiply instructions *)
	opMLA* = { 21, 7, 4 };
	opMUL* = { 7, 4 };
	opSMLAL* = { 23, 22, 21, 7, 4 };
	opSMULL* = { 23, 22, 7, 4 };
	opUMLAL* = { 23, 21, 7, 4 };
	opUMULL* = { 23, 7, 4 };

	(* branch instructions *)
	opB* = { 27, 25 };
	LinkBit* = { 24 };
	opBL* = opB + LinkBit;
	BMask* = { 27, 25, 24 };

	(* load/store instructions *)
	opLDM* = { 27, 20 };
	opLDR* = { 26, 20 };
	opLDRH* = { 20, 7, 4 };
	opSTM* = { 27 };
	opSTR* = { 26 };
	opSTRH* = { 7, 5, 4 };

	(* semaphore instructions *)
	opSWP* = { 24, 7, 4 };

	(* exception-generating instructions *)
	opSWI* = {27, 26, 25, 24 };	(* software interrupt *)
	opBKPT* = { 31, 30, 29, 24, 21, 6, 5, 4 };

	(* coprocessor instructions *)
	opCDP* = { 27, 26, 25 };
	opLDC* = { 27, 26, 20 };
	opMCR* = { 27, 26, 25, 4 };
	opMRC* = { 27, 26, 25, 20, 4 };
	opSTC* = { 27, 26 };



тип
	DCDList = укль на запись
		pc: цел32;
		next: DCDList;
	кон;

	Callback* = проц {делегат} (pc: цел32);

перем
	(*Trace W: Texts.Writer;
	t: Texts.Text; *)
	AddrMode: массив 5 из мнвоНаБитахМЗ; (* contains bitmasks for validity checks *)
	f: Files.File;
	r: Files.Rider;
	start: цел32;
	code: PCLIR.CodeArray;
	codelen: цел32;
	codeTooLong: булево;
	sourcepos*: цел32;
	dcd: булево;
	codeBarrier: цел32;
	codeBarrierCallback: Callback;
	callbackLocked: булево;

проц GetCodePos*(): цел32;
нач возврат codelen
кон GetCodePos;

проц GetInstruction*(pos: цел32): мнвоНаБитахМЗ;
перем factor, i, l: цел32;
нач утв(pos < codelen);
	factor := 1;
	нцДля i := 0 до 3 делай
		l := l + кодСимв8(code[pos+i])*factor;
		factor := factor*100H
	кц;
	если PCM.bigEndian то PCM.SwapBytes(l, 0, 4) всё;

	возврат НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, l)
кон GetInstruction;

проц GetCode*(перем codeArr: PCLIR.CodeArray; перем length, hdrLength, addressFactor: цел32);
нач
	codeArr := code; length := codelen; hdrLength := codelen DIV 4; addressFactor := 4
кон GetCode;

проц Lsh(v, s: цел32): мнвоНаБитахМЗ;
нач возврат НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, логСдвиг(v,s))
кон Lsh;

(*
PROCEDURE CheckCondition(c: SET);
BEGIN ASSERT(c - {31, 30, 29, 28} = {})
END CheckCondition;

PROCEDURE CheckAddressingMode(mode: SIGNED32; am: SET);
BEGIN ASSERT(am - AddrMode[mode-1] = {})
END CheckAddressingMode;
*)

проц CheckReg(register: цел32);
нач утв((0 <= register) и (register < 16), ErrInvalidRegister)
кон CheckReg;

(*
PROCEDURE CheckRegisterSet(registers: SET);
BEGIN ASSERT (registers - Registers = {}, ErrInvalidRegisterSet)
END CheckRegisterSet;
*)

проц CheckImm(imm, max: цел32);
нач утв((0 <= imm) и (imm < max), ErrImmediateTooBig)
кон CheckImm;

(*
PROCEDURE CheckSignedImm(imm, min, max: SIGNED32);
BEGIN ASSERT((min < imm), ErrImmediateTooSmall);
	ASSERT((imm < max), ErrImmediateTooBig)
END CheckSignedImm;
*)

проц CheckSet(set, mask: мнвоНаБитахМЗ);
нач утв(set * (-mask) = {}, ErrInvalidMode)
кон CheckSet;

(** Addressing Mode 3 Helpers *)
проц MakeA3Immediate*(перем addrMode: мнвоНаБитахМЗ; offset: цел32): мнвоНаБитахМЗ;
перем neg: булево; address: мнвоНаБитахМЗ;
нач
	neg := offset < 0; offset := матМодуль(offset);
	утв(offset < 100H);
		(* address[11:8] = offset[7:4], address[3:0] = offset[3:0] *)
	address := НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, 100H*(offset DIV 10H) + (offset остОтДеленияНа 10H));
	addrMode := addrMode - (A3Imm + IdxAdd + IdxSub);
	addrMode := addrMode + A3Imm;
	если ~neg то addrMode := addrMode + IdxAdd
	иначе addrMode := addrMode + IdxSub
	всё;
	возврат address
кон MakeA3Immediate;

проц MakeA3Register*(register: цел32): мнвоНаБитахМЗ;
нач возврат НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, register)
кон MakeA3Register;

(**----- Data Processing  Instructions (Addressing Mode 1) -------*)
проц MakeA1Immediate*(immediate: цел32; перем imm: мнвоНаБитахМЗ): булево;
перем rot: цел32;
нач
	rot := 0; imm := НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, immediate);
	нцПока (rot < 32) и (нечётноеЛи¿(rot) или (imm * { 8..31} # {})) делай
		imm := вращБит(imm, 1); увел(rot)
	кц;
	если (rot < 32) то
		imm := imm + мнвоНаБитах32(Lsh(rot DIV 2, 8));
		возврат истина
	иначе
		imm := {};
		возврат ложь
	всё
кон MakeA1Immediate;

проц MakeA1Register*(reg: цел32): мнвоНаБитахМЗ;
нач CheckReg(reg);
	возврат НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, reg)
кон MakeA1Register;

проц MakeA1RegSHIFTReg*(reg, shiftreg: цел32; mode: мнвоНаБитахМЗ): мнвоНаБитахМЗ;
нач CheckReg(reg); CheckReg(shiftreg); CheckSet(mode, ShiftMask);
	возврат Lsh(shiftreg, 8) + mode + A1ShiftReg + НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, reg)
кон MakeA1RegSHIFTReg;

проц MakeA1RegSHIFTImm*(reg, imm: цел32; mode: мнвоНаБитахМЗ): мнвоНаБитахМЗ;
нач CheckReg(reg); CheckImm(imm, 20H); CheckSet(mode, ShiftMask);
	возврат Lsh(imm, 7) + mode + A1ShiftImm + НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, reg)
кон MakeA1RegSHIFTImm;

(** ADC - add with carry (rD <- rN + shifterOperand + carry) *)
проц ADC*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opADC + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон ADC;

(** ADD -  add (rD <- rN + shifterOperand) *)
проц ADD*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opADD + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон ADD;

(** AND - bitwise and (rD <- rN AND shifterOperand) *)
проц AND*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач CheckSet(shifterOperand, {0..11});
	Code(cond + opAND + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон AND;

(** BIC - clears bits (rD <- rN AND NOT shifterOperand) *)
проц BIC*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opBIC + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон BIC;

(** CMN - compare negative (CC updated based on rN + shifterOperand) *)
проц CMN*(cond, addrMode: мнвоНаБитахМЗ; rN: цел32; shifterOperand: мнвоНаБитахМЗ);
нач Code(cond + opCMN + addrMode + Lsh(rN, 16) + shifterOperand)
кон CMN;

(** CMP - compare (CC updateed based on rN - shifterOperand) *)
проц CMP*(cond, addrMode: мнвоНаБитахМЗ; rN: цел32; shifterOperand: мнвоНаБитахМЗ);
нач Code(cond + opCMP + addrMode + Lsh(rN, 16) + shifterOperand)
кон CMP;

(** EOR - XOr (rD <- rN XOR shifterOperand) *)
проц EOR*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opEOR + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон EOR;

(** MOV - move (rD <- shifterOperand) *)
проц MOV*(cond, addrMode: мнвоНаБитахМЗ; rD: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opMOV + addrMode + Lsh(rD, 12) + shifterOperand + S)
кон MOV;

(** MVN - move negative (rD <- NOT shifterOperand) *)
проц MVN*(cond, addrMode: мнвоНаБитахМЗ; rD: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opMVN + addrMode + Lsh(rD, 12) + shifterOperand + S)
кон MVN;

(** ORR - bitwise OR (rD <- rN OR shifterOperand) *)
проц ORR*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opORR + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон ORR;

(** RSB -  reverse subtract (rD <- shifterOperand - rN) *)
проц RSB*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opRSB + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон RSB;

(** RSC -  reverse subtract with carry (rD <- shifterOperand - rN - NOT(carry)) *)
проц RSC*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opRSC + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон RSC;

(** SBC - subtract with carry (rD <- rN - shifterOperand - NOT(carry))*)
проц SBC*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opSBC + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон SBC;

(** SUB -  subtract (rD <- rN - shifterOperand) *)
проц SUB*(cond, addrMode: мнвоНаБитахМЗ; rD, rN: цел32; shifterOperand, S: мнвоНаБитахМЗ);
нач Code(cond + opSUB + addrMode + Lsh(rN, 16) + Lsh(rD, 12) + shifterOperand + S)
кон SUB;

(** TEQ -  test equivalence (CC updated based on rN XOR shifterOperand) *)
проц TEQ*(cond, addrMode: мнвоНаБитахМЗ; rN: цел32; shifterOperand: мнвоНаБитахМЗ);
нач Code(cond + opTEQ + addrMode + Lsh(rN, 16) + shifterOperand)
кон TEQ;

(** TST -  test (CC updated based on rN AND shifterOperand) *)
проц TST*(cond, addrMode: мнвоНаБитахМЗ; rN: цел32; shifterOperand: мнвоНаБитахМЗ);
нач Code(cond + opTST + addrMode + Lsh(rN, 16) + shifterOperand)
кон TST;

(**----------------------- Multiply  Instructions -----------------------*)
(** MLA - multiply accumulate (rD <- (rM * rS) + rN) *)
проц MLA*(cond: мнвоНаБитахМЗ; rD, rM, rS, rN: цел32; S: мнвоНаБитахМЗ);
нач утв(rD # rM, NONORTHOGONALITYEXCEPTION);
	Code(cond + opMLA + Lsh(rD, 16) + Lsh(rN, 12) + Lsh(rS, 8) + Lsh(rM, 0) + S)
кон MLA;

(** MUL - multiply (rD <- rM * rS) *)
проц MUL*(cond: мнвоНаБитахМЗ; rD, rM, rS: цел32; S: мнвоНаБитахМЗ);
нач утв(rD # rM, NONORTHOGONALITYEXCEPTION);
	Code(cond + opMUL + Lsh(rD, 16) + Lsh(rS, 8) + Lsh(rM, 0) + S)
кон MUL;

(** SMLAL - signed multiply accumulate long (rDLo <- (rA * rB)[31:0] + rDLo, rDHi <- (rA * rB)[63:32] + rDHi + carry) *)
проц SMLAL*(cond: мнвоНаБитахМЗ; rDHi, rDLo, rM, rS: цел32; S: мнвоНаБитахМЗ);
нач утв((rDHi # rDLo) и (rDHi # rM) и (rDLo # rM), NONORTHOGONALITYEXCEPTION);
	Code(cond + opSMLAL + Lsh(rDHi, 16) + Lsh(rDLo, 12) + Lsh(rS, 8) + Lsh(rM, 0) + S)
кон SMLAL;

(** SMULL - signed multiply long (rDLo <- (rM * rS)[31:0], rDHi <- (rM * rS)[63:32]) *)
проц SMULL*(cond: мнвоНаБитахМЗ; rDHi, rDLo, rM, rS: цел32; S: мнвоНаБитахМЗ);
нач утв((rDHi # rDLo) и (rDHi # rM) и (rDLo # rM), NONORTHOGONALITYEXCEPTION);
	Code(cond + opSMULL + Lsh(rDHi, 16) + Lsh(rDLo, 12) + Lsh(rS, 8) + Lsh(rM, 0) + S)
кон SMULL;

(** UMLAL - unsigned multiply accumulate long (rDLo <- (rM*rS)[31:0] + rDLo, rDHi <- (rM*rS)[63:32] + rDHi + carry) *)
проц UMLAL*(cond: мнвоНаБитахМЗ; rDLo, rDHi, rM, rS: цел32; S: мнвоНаБитахМЗ);
нач утв((rDHi # rDLo) и (rDHi # rM) и (rDLo # rM), NONORTHOGONALITYEXCEPTION);
	Code(cond + opUMLAL + Lsh(rDHi, 16) + Lsh(rDLo, 12) + Lsh(rS, 8) + Lsh(rM, 0) + S)
кон UMLAL;

(** UMULL - unsigned multply long (rDLo <- (rM*rS)[31:0], rDHi <- (rM*rS)[63:32]) *)
проц UMULL*(cond: мнвоНаБитахМЗ; rDLo, rDHi, rM, rS: цел32; S: мнвоНаБитахМЗ);
нач утв((rDHi # rDLo) и (rDHi # rM) и (rDLo # rM), NONORTHOGONALITYEXCEPTION);
	Code(cond + opUMULL + Lsh(rDHi, 16) + Lsh(rDLo, 12) + Lsh(rS, 8) + Lsh(rM, 0) + S)
кон UMULL;

(**------------------------- Branch  Instructions  -------------------------*)

(** B - branch to address. (PC <- PC + extS(address << 2)). Hint: PC is 2 instructions ahead *)
проц B*(cond: мнвоНаБитахМЗ; address: цел32);
нач утв((матМодуль(address) < 1000000H), ErrBranchOffsetTooBig);
	утв(cond # {31, 30, 29, 28});
	Code(cond + opB + Mask24*мнвоНаБитахМЗ(address))
кон B;

(** BL - branch to address. (LR <- address of instr. after branch instruction, PC <- PC + extS(address << 2))
	Hint: - PC is 2 instructions ahead *)
проц BL*(cond: мнвоНаБитахМЗ; address: цел32);
нач Code(cond + opBL + Mask24*мнвоНаБитахМЗ(address))
кон BL;

(**------------ Load/Store Instructions (Addressing Modes 2, 3 & 4) --------------*)

проц MakeA2Immediate*(перем addrMode: мнвоНаБитахМЗ; offset: цел32): мнвоНаБитахМЗ;
перем neg: булево; address: мнвоНаБитахМЗ;
нач
	neg := offset < 0; offset := матМодуль(offset);
	утв(offset < 1000H);
	address := НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, offset);
	addrMode := addrMode - (A2Imm + IdxAdd + IdxSub);
	addrMode := addrMode + A2Imm;
	если ~neg то addrMode := addrMode + IdxAdd
	иначе addrMode := addrMode + IdxSub
	всё;
	возврат address
кон MakeA2Immediate;

проц MakeA2Register*(register: цел32): мнвоНаБитахМЗ;
нач возврат НИЗКОУР.подмениТипЗначения(мнвоНаБитах32, register)
кон MakeA2Register;

проц MakeA2ScaledRegister*(reg: цел32; mode: мнвоНаБитахМЗ; shift: цел32): мнвоНаБитахМЗ;
нач CheckReg(reg); CheckImm(shift, 20H); CheckSet(mode, ShiftMask);
	возврат Lsh(shift, 7) + mode + Lsh(reg, 0)
кон MakeA2ScaledRegister;

(** LDM - uses Addressing Mode 4 *)
проц LDM*(cond, addrMode: мнвоНаБитахМЗ; rD: цел32; registers, W: мнвоНаБитахМЗ);
нач Code(cond + opLDM + addrMode + Lsh(rD, 16) + registers + W)
кон LDM;

(** LDR - uses Addressing Mode 2 *)
проц LDR*(cond, addrMode: мнвоНаБитахМЗ; rD, rAdr: цел32; address: мнвоНаБитахМЗ);
нач Code(cond + opLDR + addrMode + Lsh(rAdr, 16) + Lsh(rD, 12) + address)
кон LDR;

(** LDRH - uses Addressing Mode 3 *)
проц LDRH*(cond, addrMode: мнвоНаБитахМЗ; rD, rAdr: цел32; address: мнвоНаБитахМЗ);
нач Code(cond + opLDRH + addrMode + Lsh(rAdr, 16) + Lsh(rD, 12) + address)
кон LDRH;

(** STM - uses Addressing Mode 4 *)
проц STM*(cond, addrMode: мнвоНаБитахМЗ; rD: цел32; registers, W: мнвоНаБитахМЗ);
нач Code(cond + opSTM + addrMode + Lsh(rD, 16) + registers + W)
кон STM;

(** STR - uses Addressing Mode 2 *)
проц STR*(cond, addrMode: мнвоНаБитахМЗ; rAdr, rS: цел32; address: мнвоНаБитахМЗ);
нач Code(cond + opSTR + addrMode + Lsh(rAdr, 16) + Lsh(rS, 12) + address)
кон STR;

(** STRH - uses Addressing Mode 3 *)
проц STRH*(cond, addrMode: мнвоНаБитахМЗ; rAdr, rS: цел32; address: мнвоНаБитахМЗ);
нач утв(address*{6,5}={}, NONORTHOGONALITYEXCEPTION);
	Code(cond + opSTRH + addrMode + Lsh(rAdr, 16) + Lsh(rS, 12) + address)
кон STRH;

(**--------------------------- Miscellaneous -----------------------------*)

(** SWI - software interrupt *)
проц SWI*(cond: мнвоНаБитахМЗ; code: цел32);
нач CheckImm(code, 1000000H);
	Code(cond + opSWI + Mask24*Lsh(code, 0))
кон SWI;

(** DCD - puts a 32bit value into the code *)
проц DCD*(value: цел32);
нач dcd := истина; Code(мнвоНаБитахМЗ(value))
кон DCD;

(**--------------------------- Fixup Handling -------------------------------*)
(* Lock - does not allow automatic flushing of the constant pool. Not reentrant. *)
проц Lock*;
нач callbackLocked := истина
кон Lock;

(* Unlock - allows automatic flushing of the constant pool. Not reentrant *)
проц Unlock*;
нач callbackLocked := ложь; CheckCallback
кон Unlock;

(* SetConstantPoolBarrier - *)
проц SetConstantPoolBarrier*(pc: цел32);
нач
	утв((codelen < pc) и (pc < codelen + 1000H) или (pc = -1), INTERNALERROR);
	codeBarrier := pc
кон SetConstantPoolBarrier;

(* SetConstantPoolBarrierCallback - *)
проц SetConstantPoolBarrierCallback*(callback: Callback);
нач
	codeBarrierCallback := callback
кон SetConstantPoolBarrierCallback;

(* CheckCallback - calls the callback handler if necessary *)
проц CheckCallback;
нач
	если ~callbackLocked и (codeBarrier # -1) и (codeBarrierCallback # НУЛЬ) и (codelen >= codeBarrier) то
		Lock; (* lock or we'll get a stack overflow due to endless recursion *)
		codeBarrierCallback(codelen);
		Unlock
	всё
кон CheckCallback;

проц ExtractRegister(code: мнвоНаБитахМЗ; pos: цел32): цел32;
нач возврат логСдвиг(НИЗКОУР.подмениТипЗначения(цел32, мнвоНаБитах32(code)), -pos) остОтДеленияНа 10H
кон ExtractRegister;

проц FixLoad*(pc: цел32; address: цел32);
перем b, addrMode, addr: мнвоНаБитахМЗ; currPos: цел32;
нач
	b := GetInstruction(pc);
	currPos := codelen;
	codelen := pc;
	если (b * opLDR = opLDR) то
		addrMode := b * A2AddrModeMask;
		addr := MakeA2Immediate(addrMode, address);
		LDR(b*CondMask, addrMode, ExtractRegister(b, 12), ExtractRegister(b, 16), addr)
	аесли (b * opLDRH = opLDRH) то
		addrMode := b * A3AddrModeMask;
		addr := MakeA3Immediate(addrMode, address);
		LDRH(b*CondMask, addrMode, ExtractRegister(b, 12), ExtractRegister(b, 16), addr)
	иначе СТОП(INTERNALERROR)
	всё;
	codelen := currPos;
кон FixLoad;

проц FixJump*(pc: цел32; address: цел32);
перем b: мнвоНаБитахМЗ; currPos: цел32;
нач
	b := GetInstruction(pc);
	утв(b * opB = opB);
	currPos := codelen;
	codelen := pc;
	B(b*CondMask, address);
	codelen := currPos
кон FixJump;

проц FixCall*(pc: цел32; address: цел32): цел32;
перем b: мнвоНаБитахМЗ; currPos: цел32;
нач
	b := GetInstruction(pc);
	утв(b * opBL = opBL);
	currPos := codelen;
	codelen := pc;
	BL(b*CondMask, address);
	codelen := currPos;
	возврат НИЗКОУР.подмениТипЗначения(цел32, мнвоНаБитах32(b*Mask24))
кон FixCall;

проц FixCaseTable*(pc: цел32; address: цел32);
перем fixup, currPos: цел32;
нач
	утв((address >= 0) и (address < 10000H), ErrCaseOffsetTooBig);
	fixup := НИЗКОУР.подмениТипЗначения(цел32, мнвоНаБитах32(GetInstruction(pc) * { 16..31 }));
	currPos := codelen;
	codelen := pc;
	DCD(fixup + address);
	codelen := currPos
кон FixCaseTable;


(**--------------------------- Miscellaneous -------------------------------*)

проц Init*(codeFN: массив из симв8);
нач
	f := Files.New(codeFN);
	Files.Register(f);
	f.Set(r, 0);
	start := 0;
	если (code = НУЛЬ) то нов(code, MaxCodeLength) всё;
	codelen := 0; codeTooLong := ложь;
	(*Trace IF Trace THEN NEW(t); Texts.Open(t, ""); PCARMDecoder.Init END *)
кон Init;

проц Code(opcode: мнвоНаБитахМЗ);
тип Bytes= массив 4 из симв8;
перем b: Bytes; i: цел16; c32: мнвоНаБитах32; 
нач
	утв(codelen остОтДеленияНа 4 = 0); (* in case PutChar did not write 4x characters *)
	(*Trace IF Trace & dcd THEN
		dcd := FALSE;
		NEW(d); d.pc := codelen;
		IF (dcdLast = NIL) THEN dcdList := d; dcdLast := d
		ELSE dcdLast.next := d; dcdLast := d
		END
	END; *)
	если (codelen <= MaxCodeLength-4) то
		c32 := мнвоНаБитах32(opcode);
		b := НИЗКОУР.подмениТипЗначения(Bytes, c32);
		если PCM.bigEndian то PCM.SwapBytes(b, 0, 4) всё;
		нцДля i := 0 до 3 делай
			code[codelen] := b[i]; увел(codelen) (* little endian *)
		кц;
		CheckCallback
	иначе
		если ~codeTooLong то	(* report only once *)
			codeTooLong := истина;
			PCM.Error(244, sourcepos, "Code too long.")
		всё
	всё
кон Code;

проц PutChar*(c: симв8);
нач
	code[codelen] := c; увел(codelen)
кон PutChar;

проц Close*;
перем b: укль на массив из симв8;
нач
	если (codelen > MaxCodeLength) то (* code too long *)
		PCM.Error(244, PCM.InvalidPosition, "Code too long.");
	аесли (codelen-start > 0) то
		нов(b, codelen-start);
		НИЗКОУР.копируйПамять(адресОт(code[start]), адресОт(b[0]), codelen-start);
		f.WriteBytes(r, b^, 0, codelen-start);
		Files.Register(f);
	аесли (codelen - start < 0) то СТОП(матМаксимум(цел16)) (* show stack*)
	всё;
	f := НУЛЬ;
кон Close;

проц BoP*(name: массив из симв8);
(* VAR dcd: DCDList; i: SIGNED32; b: ARRAY InstructionSize OF CHAR;
BEGIN
	Trace IF Trace THEN
		IF (start # codelen) THEN
			dcd := dcdList;
			WHILE (start < codelen) DO
				FOR i := 0 TO InstructionSize-1 DO b[i] := code[start+i] END;
				PCARMDecoder.Decode(start, SYSTEM.VAL(SIGNED32, b), (dcd # NIL) & (dcd.pc = start));
				IF (dcd # NIL) & (dcd.pc = start) THEN dcd := dcd.next END;
				INC(start, InstructionSize)
			END;
			ASSERT(start = codelen);
			dcdList := NIL; dcdLast := NIL
		END;

		IF (name = "") THEN COPY("Module Body", name) END;
		Texts.WriteLn(PCARMDecoder.W);
		Texts.WriteString(PCARMDecoder.W, name); Texts.WriteLn(PCARMDecoder.W);
		Texts.Append(t, PCARMDecoder.W.buf)
	END; *)
кон BoP;

проц EoP*;
кон EoP;

проц Dump*;
(* VAR dcd: DCDList; i: SIGNED32; b: ARRAY InstructionSize OF CHAR;
BEGIN
	Trace IF Trace THEN
		IF (start # codelen) THEN
			dcd := dcdList;
			WHILE (start < codelen) DO
				FOR i := 0 TO InstructionSize-1 DO b[i] := code[start+i] END;
				PCARMDecoder.Decode(start, SYSTEM.VAL(SIGNED32, b), (dcd # NIL) & (dcd.pc = start));
				IF (dcd # NIL) & (dcd.pc = start) THEN dcd := dcd.next END;
				INC(start, InstructionSize)
			END;
			ASSERT(start = codelen);
			dcdList := NIL; dcdLast := NIL;
			Texts.WriteLn(PCARMDecoder.W);
			Texts.WriteString(PCARMDecoder.W, "TRAP"); Texts.WriteLn(PCARMDecoder.W);
			Texts.Append(t, PCARMDecoder.W.buf)
		END;
		Oberon.OpenText("ARM Code", t, 600, 400)
	END *)
кон Dump;

нач
	AddrMode[0] := { 0 .. 31 };
	AddrMode[1] := { 0 .. 31 };
	AddrMode[2] := { 0 .. 31 };
	AddrMode[3] := { 24, 23 };
	AddrMode[4] := { 0 .. 31 };
	SetConstantPoolBarrier(-1)
кон PCOARM.
