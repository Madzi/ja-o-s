модуль CryptoPrimes;

(*	2002.9.11	g.f.	based on 'bn_prime.c'

		Copyright of the original:    <-- middle click here
*)

использует B := CryptoBigNumbers, Out := ЛогЯдра;

конст
	N = 2048;

тип
	BigNumber = B.BigNumber;

перем
	one: BigNumber;
	primes: массив N из цел32;


	проц NewPrime*( bits: цел16;  safe: булево ): BigNumber;
	перем checks, i: цел16;  t, p: BigNumber;
	нач
		checks := Checks( bits ); i := 1;
		нц
			p := ProbablePrime( bits );
			Out.пСимв8( '.' );
			если IsPrime( p, checks, ложь ) то
				Out.пСимв8( 'P' );
				если ~safe то  Out.пВК_ПС;  возврат p
				иначе
					Out.пЦел64( i, 0 );  увел( i );
					(* for "safe prime" generation, check that (p-1)/2 is prime. *)
					B.Copy( p, t );  t.Shift( -1 );
					если IsPrime( t, checks, ложь ) то
						Out.пСтроку8( " (safe prime)" ); Out.пВК_ПС;
						возврат p
					всё;
					Out.пСтроку8( " (not safe)" ); Out.пВК_ПС;
				всё
			всё;
		кц
	кон NewPrime;

	проц NewDHPrime*( bits: цел16;  safe: булево;  add, rem: BigNumber ): BigNumber;
	перем checks: цел16;  t, p: BigNumber;
	нач
		checks := Checks( bits );
		нц
			если safe то  p := ProbableDHPrimeSafe( bits, add, rem )
			иначе p := ProbableDHPrime( bits, add, rem )
			всё;
			если IsPrime( p, checks, ложь ) то
				если ~safe то  возврат p
				иначе  (* for "safe prime" generation, check that (p-1)/2 is prime. *)
					B.Copy( p, t );  t.Shift( -1 );
					если IsPrime( t, checks, ложь ) то  возврат p  всё
				всё
			всё
		кц
	кон NewDHPrime;

	проц Checks( b: размерМЗ ): цел16;
	(* number of Miller-Rabin iterations for an error rate  of less than 2^-80
		for random 'b'-bit input, b >= 100 (taken from table 4.4 in the Handbook
		of Applied Cryptography [Menezes, van Oorschot, Vanstone; CRC Press 1996]
	*)
	перем t: цел16;
	нач
		утв( b >= 100 );
		если b >= 1300 то t := 2
		аесли b >= 850 то t := 3
		аесли b >= 650 то t := 4
		аесли b >= 550 то t := 5
		аесли b >= 450 то t := 6
		аесли b >= 400 то t := 7
		аесли b >= 350 то t := 8
		аесли b >= 300 то t := 9
		аесли b >= 250 то t := 12
		аесли b >= 200 то t := 15
		аесли b >= 150 то t := 18
		иначе  t := 27
		всё;
		возврат t
	кон Checks;



	проц ProbablePrime( bits: цел16 ): BigNumber;
	перем t: BigNumber;  delta, i: цел32; p: BigNumber;
		mods: массив N из бцел32;
	нач
		нц
			p := B.NewRand( bits, 1, 1 );
			нцДля i := 0 до N - 1 делай  mods[i] := B.ModWord( p, primes[i] )  кц;
			(* check that p is not a prime and also that gcd( p-1, primes) = 1 (except for 2) *)
			i := 0;  delta := 0;
			нц
				увел( i );
				если i >= N то
					B.AssignInt( t, delta );  p := B.Add( p, t );
					возврат p
				всё;
				если (mods[i] + delta) остОтДеленияНа primes[i] <= 1 то
					увел( delta, 2 );  i := 0;
					если delta < 0 то  (* overfow! try new random *)  прервиЦикл  всё;
				всё;
			кц
		кц
	кон ProbablePrime;


	проц ProbableDHPrime( bits: цел16;  add, rem: BigNumber ): BigNumber;
	перем d, r, p: BigNumber;  i: цел32;
	нач
		p := B.NewRand( bits, 0, 1 );

		(* we need (p - rem) mod add = 0 *)
		r := B.Mod( p, add );  p := B.Sub( p, r );
		если rem.IsZero( ) то  p.Inc  иначе  p := B.Add( p, rem )  всё;

		(* we now have a random number 'p' to test. *)
		i := 0;
		нц
			увел( i );
			если i >= N то  возврат p  всё;
			B.AssignInt( d, primes[i] );  r := B.Mod( p, d );
			если r.IsZero( ) то  p := B.Add( p, add );  i := 0  всё;
		кц
	кон ProbableDHPrime;

	проц ProbableDHPrimeSafe( bits: цел16; padd, rem: BigNumber ): BigNumber;
	перем d, q, r, qr, qadd, p: BigNumber;  i: цел32;
	нач
		B.Copy( padd, qadd );  qadd.Shift( -1 );
		q := B.NewRand( bits, 0, 1 );

		r := B.Mod( q, qadd );  q := B.Sub( q, r );
		если rem.IsZero( ) то  q.Inc
		иначе  B.Copy( rem, r );  r.Shift( -1 );  q := B.Add( q, r )
		всё;

		(* we now have a random number  to test. *)
		B.Copy( q, p );  p.Shift( 1 );  p.Inc;

		i := 0;
		нц
			увел( i );
			если i >= N то  возврат p  всё;
			B.AssignInt( d, primes[i] );  r := B.Mod( p, d );  qr := B.Mod( q, d );
			если r.IsZero( ) или qr.IsZero( ) то
				p := B.Add( p, padd );  q := B.Add( q, qadd );  i := 0
			всё;
		кц
	кон ProbableDHPrimeSafe;


	проц IsPrime*( a: BigNumber;  checks: цел16;  doTrialDiv: булево ): булево;
	перем i, k: цел16;  A, A1, A1odd, check: BigNumber;
	нач
		если checks = 0 то  checks := Checks( a.BitSize( ) )  всё;
		если ~нечётноеЛи¿( a.d[0] ) то  возврат ложь  всё;
		если doTrialDiv то
			нцДля i := 1 до N - 1 делай
				если B.ModWord( a, primes[i] ) = 0 то  возврат ложь  всё
			кц
		всё;
		B.Copy( a, A );
		если A.neg то  A.Negate  всё;
		B.Copy( A, A1 );  A1.Dec;
		если A1.IsZero( ) то  возврат ложь  всё;

		(* write  A1  as  A1odd * 2^k *)
		k := 1;  нцПока ~A1.BitSet( k ) делай  увел( k )  кц;
		B.Copy( A1, A1odd );  A1odd.Shift( -k );

		нцДля i := 1 до checks делай
			check := B.NewRand( A1.BitSize( ), 0, 0 );
			если check.GEQ( A1 ) то  check := B.Sub( check, A1 )  всё;
			check.Inc;
			(* now 1 <= check < A *)
			если ~witness( check, A, A1, A1odd, k ) то   возврат ложь  всё;
		кц;
		возврат истина
	кон IsPrime;

	проц witness( W, a, a1, a1odd: BigNumber;  k: цел16): булево;
	перем w: BigNumber;
	нач
		w := B.ModExp( W, a1odd, a );
		если w.EQ( one ) то  (* probably prime *)  возврат истина  всё;
		если w.EQ( a1 ) то   возврат истина  всё;
				(* w = -1 (mod a), a is probably prime *)
		нцПока k > 0 делай
			w := B.ModMul( w, w, a );  (* w = w^2 mod a *)
			если w.EQ( one ) то  возврат ложь  всё;
					(* a is composite, otherwise a previous w would  have been = -1 (mod a) *)
			если w.EQ( a1 ) то  возврат истина  всё;
					(* w = -1 (mod a), a is probably prime *)
			умень( k )
		кц;
		(* If we get here, w is the (a-1)/2-th power of the original w, *)
		(* and it is neither -1 nor +1 -- so a cannot be prime *)
		возврат ложь
	кон witness;

	проц Init;
	перем sieve: массив N из мнвоНаБитах32; i, j, p, n: цел32;
	нач
		(* compute the first N small primes *)
		нцДля i := 0 до N - 1 делай sieve[i] := {0..31} кц;
		primes[0] := 2;  n := 1;  i := 1;
		нцПока n < N делай
			если i остОтДеленияНа 32 в sieve[i DIV 32] то
				p := 2*i + 1;  primes[n] := p;  увел( n );  j := i;
				нцПока j DIV 32 < N делай  исключиИзМнваНаБитах( sieve[j DIV 32], j остОтДеленияНа 32 );  увел( j, p )  кц;
			всё;
			увел( i )
		кц;
	кон Init;

нач
	Init;  B.AssignInt( one, 1 );
кон CryptoPrimes.

