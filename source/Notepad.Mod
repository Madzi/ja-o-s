модуль Notepad; (** AUTHOR "TF"; PURPOSE "Simple Text Editor"; *)

использует
	Modules, Commands, Options, Files, Strings, WMRestorable, XML,
	WMGraphics, WMComponents, WMMessages, WMWindowManager, WMDocumentEditor;

тип
	KillerMsg = окласс
	кон KillerMsg;

	Window* = окласс (WMComponents.FormWindow)
	перем
		editor- : WMDocumentEditor.Editor;

		проц &New*(c : WMRestorable.Context);
		нач
			IncCount;
			Init(850, 700, ложь);

			нов(editor); editor.alignment.Set(WMComponents.AlignClient);
			editor.fillColor.Set(WMGraphics.White);
			editor.SetToolbar({0..31});
			editor.SetWordWrap(истина);
			SetContent(editor);
			SetTitle(Strings.NewString("Notepad"));
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://Notepad.png", истина));

			если c # НУЛЬ то
				(* restore the desktop *)
				WMRestorable.AddByContext(сам, c);
				если c.appData # НУЛЬ то
					editor.FromXml(c.appData(XML.Element));
					Resized(GetWidth(), GetHeight())
				всё
			иначе WMWindowManager.DefaultAddWindow(сам)
			всё;
		кон New;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount
		кон Close;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		перем data : XML.Element;
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть KillerMsg) то Close
				аесли (x.ext суть WMRestorable.Storage) то
					нов(data); data.SetName("NotepadData");
					editor.ToXml(data);
					x.ext(WMRestorable.Storage).Add("Notepad", "Notepad.Restore", сам, data);
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

	кон Window;

перем
	nofWindows : цел32;

(** Open document *)
проц Open*(context : Commands.Context); (** [Options] [filename] ~ *)
перем options : Options.Options; window : Window; filename : Files.FileName; format : массив 32 из симв8;
нач
	нов(options);
	options.Add("f", "format", Options.String);
	если options.Parse(context.arg, context.error) то
		если ~options.GetString("format", format) то format := "АВТО"; всё;
		нов(window, НУЛЬ);
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
			window.editor.Load(filename, format);
		всё;
	всё;
кон Open;

проц Restore*(context : WMRestorable.Context);
перем winstance : Window;
нач
	нов(winstance, context)
кон Restore;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон Notepad.

System.Free Notepad~

Notepad.Open ~
