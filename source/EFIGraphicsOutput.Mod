модуль EFIGraphicsOutput; (** AUTHOR "Matthias Frei"; PURPOSE "EFI Graphics Output Protocol"; *)

использует
	EFI, НИЗКОУР;

конст
	(* PixelFormat enum *)
	PFRGBX8Bit*= 0; (* a pixel is 32 bit. Each color is 8bit and they are ordered Red,Green,Blue,Reserved *)
	PFBGRX8Bit*= 1; (* a pixel is 32 bit. Each color is 8bit and they are ordered Blue,Green,Red,Reserved *)
	PFBitMask*= 2; (* format is defined by PixelBitmask *)
	PFBltOnly*= 3; (* this mode does not support a physical frame buffer *)
	PFMax*= 4;

перем
	GUID-: EFI.GUID;

тип GraphicsMode* = укль на GraphicsModeDescription;
тип GraphicsModeDescription* = запись
	MaxMode-: EFI.Int32;
	Mode-: EFI.Int32; (*current mode. Valid mode numbers are 0 to MaxMode-1 *)
	Info-{неОтслСборщиком}: GraphicsModeInfo;
	SizeOfInfo-: EFI.Int;
	FrameBufferBase-: EFI.PhysicalAddress;
	FrameBufferSize-: EFI.Int;
кон;

тип GraphicsModeInfo* = укль на GraphicsModeInfoDescription;
тип GraphicsModeInfoDescription* = запись
	Version-: EFI.Int32; (* = 0 for the structure specified here. If > 0, structure extended backwards compatibly *)
	HorizontalResolution-: EFI.Int32;
	VerticalResolution-: EFI.Int32;
	PixelFormat-: EFI.Int32; (* enum *)
	PixelBitmask-: запись RedMask-,GreenMask-,BlueMask-,ReservedMask-:EFI.Int32; кон;
	PixelsPerScanline-: EFI.Int32;
кон;

тип Protocol* = укль на ProtocolDescription;
тип GOQueryMode* = проц{WINAPI}(This : Protocol; ModeNumber : EFI.Int32; перем SizeOfInfo : EFI.Int; перем Info : GraphicsModeInfo) : EFI.Status;
тип GOSetMode* = проц{WINAPI}(This : Protocol; ModeNumber : EFI.Int32) : EFI.Status;
тип GODummyType* = проц{WINAPI}():EFI.Status;
тип ProtocolDescription* = запись(EFI.ProtocolDescription)
	QueryMode-: GOQueryMode;
	SetMode-: GOSetMode;
	Blt-: GODummyType; (* not implemented *)
	Mode-{неОтслСборщиком}: GraphicsMode;
кон;

нач
	GUID.Data1 := цел32(9042A9DEH);
	GUID.Data2 := 23DCH;
	GUID.Data3 := 4A38H;
	GUID.Data4[0] := цел8(96H);
	GUID.Data4[1] := цел8(0FBH);
	GUID.Data4[2] := 7AH;
	GUID.Data4[3] := цел8(0DEH);
	GUID.Data4[4] := цел8(0D0H);
	GUID.Data4[5] := цел8(80H);
	GUID.Data4[6] := 51H;
	GUID.Data4[7] := 6AH;
кон EFIGraphicsOutput.
