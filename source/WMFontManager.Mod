модуль WMFontManager;	(** AUTHOR "TF"; PURPOSE "Default implementation of a simple font manager"; *)

использует
	ЛогЯдра, Kernel, Modules, Commands, WMGraphics, WMDefaultFont, Strings, Configuration, XML, XMLObjects;

тип
	String = XML.String;

	FontInfo* = окласс
	перем
		name* : String;
		size* : размерМЗ;
		style* : мнвоНаБитахМЗ;
	кон FontInfo;

	FontFactory = проц(info : FontInfo) : WMGraphics.Font;

	LoaderInfo = укль на запись
		loader : String;
		next : LoaderInfo;
	кон;

	FontManager = окласс (WMGraphics.FontManager)
	перем
		fontCache : Kernel.FinalizedCollection;

		(* lru: last recently used font - circular buffer. This buffer is for fast access and provides a protection for a limited number of loaded fonts from being garbage collected.  *)
		lru: массив 64 из WMGraphics.Font;
		lruPosition: цел32;

		defaultFont : WMGraphics.Font;
		font : WMGraphics.Font; (* set by the enumerator *)
		searchName : массив 256 из симв8;
		searchSize : размерМЗ;
		searchStyle : мнвоНаБитахМЗ;
		found : булево;
		exactLoaders, approximateLoaders : LoaderInfo;

		(* default font settings specified in Configuration.XML, read by procedure GetConfig *)
		defaultFontName : массив 256 из симв8;
		defaultFontSize : цел32;
		defaultFontStyle : мнвоНаБитахМЗ;

		проц &Init*;
		перем t : WMGraphics.Font;
		нач
			нов(fontCache);
			defaultFontName := "Oberon"; defaultFontSize := 14; defaultFontStyle := {};
			GetConfig;
			defaultFont := WMDefaultFont.LoadDefaultFont(); (* fallback case *)
			defaultFont.size := defaultFontSize;
			t := GetFont(defaultFontName, defaultFontSize, defaultFontStyle);
			если t = defaultFont то ЛогЯдра.пСтроку8("Using embedded font"); ЛогЯдра.пВК_ПС иначе defaultFont := t всё;
			WMGraphics.InstallDefaultFont(defaultFont);
			lruPosition := 0;
		кон Init;

		проц MatchExact(obj : динамическиТипизированныйУкль; перем cont : булево);
		перем f : WMGraphics.Font;
		нач
			cont := истина;
			если obj суть WMGraphics.Font то
				f := obj(WMGraphics.Font);
				если (f.name = searchName) и (f.size = searchSize) и (f.style = searchStyle) то
					font := f; cont := ложь; found := истина;
				всё
			всё;
		кон MatchExact;

		проц MatchSimiliar(obj : динамическиТипизированныйУкль; перем cont : булево);
		перем f : WMGraphics.Font;
		нач
			cont := истина;
			если obj суть WMGraphics.Font то
				f := obj(WMGraphics.Font);
				если (f.name = searchName) и (f.size = searchSize) то
					font := f; cont := ложь; found := истина;
				всё
			всё;
		кон MatchSimiliar;

		проц AddExact(str : String);
		перем n : LoaderInfo;
		нач
			если str = НУЛЬ то возврат всё;
			нов(n); n.loader := str;
			n.next := exactLoaders; exactLoaders := n
		кон AddExact;

		проц AddApproximate(str : String);
		перем n : LoaderInfo;
		нач
			если str = НУЛЬ то возврат всё;
			нов(n); n.loader := str;
			n.next := approximateLoaders; approximateLoaders := n
		кон AddApproximate;

		проц GetConfig;
		перем
			section, e : XML.Element;
			p : динамическиТипизированныйУкль; enum: XMLObjects.Enumerator;
			string : массив 16 из симв8; res : целМЗ;

			проц Error;
			нач ЛогЯдра.пСтроку8("WindowManager.FontManager subsection missing in Configuration. Running on defaults"); ЛогЯдра.пВК_ПС
			кон Error;

		нач { единолично }
			section := Configuration.GetSection("WindowManager.FontManager.FontLoaders");
			если section # НУЛЬ то
				enum := section.GetContents();
				нцПока enum.HasMoreElements() делай
					p := enum.GetNext();
					если p суть XML.Element то
						e := Configuration.GetNamedElement(p(XML.Element), "Setting", "Exact");
						если e # НУЛЬ то AddExact(e.GetAttributeValue("value")) всё;
						e := Configuration.GetNamedElement(p(XML.Element), "Setting", "Approximate");
						если e # НУЛЬ то AddApproximate(e.GetAttributeValue("value")) всё;
					всё;
				кц;
				Configuration.Get("WindowManager.FontManager.DefaultFont.Name", defaultFontName, res);
				Configuration.Get("WindowManager.FontManager.DefaultFont.Size", string, res);
				если (res = Configuration.Ok) то Strings.StrToInt(string, defaultFontSize); всё;
			иначе Error;
			всё
		кон GetConfig;

		проц Load(ln : String; fi : FontInfo) : WMGraphics.Font;
		перем
			factory : FontFactory; font : WMGraphics.Font;
			moduleName, procedureName : Modules.Name;
			msg : массив 32 из симв8; res : целМЗ;
		нач
			если (ln = НУЛЬ) то возврат НУЛЬ всё;
			font := НУЛЬ;
			Commands.Split(ln^, moduleName, procedureName, res, msg);
			если (res = Commands.Ok) то
				дайПроцПоИмени(moduleName, procedureName, factory);
				если (factory # НУЛЬ) то
					font := factory(fi);
				всё;
			всё;
			возврат font;
		кон Load;

		проц {перекрыта}GetFont*(конст name : массив из симв8; size : размерМЗ; style : мнвоНаБитахМЗ) : WMGraphics.Font;
		перем tf,f : WMGraphics.Font; l : LoaderInfo; fi : FontInfo; i: цел32;
		нач {единолично}
			font := defaultFont;
			found := ложь;

			i := (lruPosition-1) остОтДеленияНа длинаМассива(lru);
			нцДо
				i := (i - 1) остОтДеленияНа длинаМассива(lru);
				f := lru[i];
				если f = НУЛЬ то i := lruPosition
				аесли (f.size = size) и (f.style = style) и (f.name= name)то
					font := f; found := истина;
				всё;
			кцПри (i = lruPosition) или found;

			если ~found то
				копируйСтрокуДо0(name, searchName); searchSize := size; searchStyle := style;
				fontCache.Enumerate(MatchExact);
				если ~found то
					нов(fi);
					fi.name := Strings.NewString(name);
					fi.size := size; fi.style := style;
					(* search for exact matches *)
					l := exactLoaders;
					нцПока ~found и (l # НУЛЬ) делай
						tf := Load(l.loader, fi);
						если tf # НУЛЬ то font := tf; fontCache.Add(font, НУЛЬ); found := истина всё;
						l := l.next;
					кц;
					(* search for approximate matches (not exact style) *)
					если ~found то fontCache.Enumerate(MatchSimiliar) всё;
					l := approximateLoaders;
					нцПока ~found и (l # НУЛЬ) делай
						tf := Load(l.loader, fi);
						если tf # НУЛЬ то font := tf; fontCache.Add(font, НУЛЬ); found := истина всё;
						l := l.next;
					кц
				всё;
				lru[lruPosition] := font; lruPosition := (lruPosition+1) остОтДеленияНа длинаМассива(lru);

			всё;
			возврат font
		кон GetFont;

	кон FontManager;

перем fm : FontManager;

проц Install*;
нач
	fm.GetConfig();
кон Install;

проц Load;
нач
	нов(fm);
	WMGraphics.InstallFontManager(fm)
кон Load;

проц Cleanup;
нач
	WMGraphics.InstallFontManager(НУЛЬ)
кон Cleanup;

нач
	Load;
	Modules.InstallTermHandler(Cleanup)
кон WMFontManager.
