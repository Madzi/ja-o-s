модуль A2Sequencers;	(** AUTHOR "negelef"; PURPOSE "Generic A2 Sequencer"; *)

(*
This module provides a generic sequencer base class  that allows deriving active objects to communicate sequentially over messages.
Messages are handled sequentially and provide atomic and exclusive access to the state of a sequencer. Requests are special messages
which allow the caller to block and wait for the sequencer to handle the request. This is useful to retrieve a set of states of the sequencer.
Code in procedures of a sequencer must make sure that they are called by their own sequencer object (using the SequencerCalledThis
procedure) and have to add a corresponding message otherwise. If sequencers share variables, they can also put it into property objects
which support atomic access to their values and a registration mechanism for notification handlers.
*)

использует Machine, Потоки, Objects, Kernel;

конст
	NoDelay* = 0;

	MaxHandlers = 10;

тип
	(* generic property object that provides lock-free access to its value *)
	Property* = окласс
	перем
		locks: целМЗ;
		container*: окласс;

		проц &InitProperty;
		нач locks := 0; container := НУЛЬ;
		кон InitProperty;

		проц AcquireRead;
		перем locks: целМЗ;
		нач
			нц
				locks := сам.locks;
				если (locks >= 0) и (Machine.AtomicCAS (сам.locks, locks, locks + 1) = locks) то прервиЦикл всё;
				Objects.Yield;
			кц;
		кон AcquireRead;

		проц ReleaseRead;
		нач Machine.AtomicDec (locks);
		кон ReleaseRead;

		проц AcquireWrite;
		перем locks: целМЗ;
		нач
			нц
				locks := сам.locks;
				если (locks = 0) и (Machine.AtomicCAS (сам.locks, locks, locks - 1) = locks) то прервиЦикл всё;
				Objects.Yield;
			кц;
		кон AcquireWrite;

		проц ReleaseWrite;
		нач Machine.AtomicInc (locks);
		кон ReleaseWrite;

		проц ToStream*(w : Потоки.Писарь);
		кон ToStream;	(* abstract *)

		проц FromStream*(r : Потоки.Чтец);
		кон FromStream;	(* abstract *)

	кон Property;

	Boolean* = окласс (Property)
	перем
		value: булево;
		handlers: массив MaxHandlers из BooleanHandler;

		проц &InitBoolean* (value: булево);
		нач InitProperty; сам.value := value;
		кон InitBoolean;

		проц Get* (): булево;
		перем value: булево;
		нач AcquireRead; value := сам.value; ReleaseRead; возврат value;
		кон Get;

		проц Set* (value: булево);
		перем changed: булево;
		нач AcquireWrite; changed := сам.value # value; сам.value := value; ReleaseWrite; если changed то Changed (value) всё;
		кон Set;

		проц Changed (value: булево);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай handlers[i] (сам, value); увел (i) кц;
		кон Changed;

		проц AddHandler* (handler: BooleanHandler);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай увел (i) кц; handlers[i] := handler;
		кон AddHandler;

	кон Boolean;

	Integer* = окласс (Property)
	перем
		value: целМЗ;
		handlers: массив MaxHandlers из IntegerHandler;

		проц &InitInteger* (value: целМЗ);
		нач InitProperty; сам.value := value;
		кон InitInteger;

		проц Get* (): целМЗ;
		перем value: целМЗ;
		нач AcquireRead; value := сам.value; ReleaseRead; возврат value;
		кон Get;

		проц Set* (value: целМЗ);
		перем changed: булево;
		нач AcquireWrite; changed := сам.value # value; сам.value := value; ReleaseWrite; если changed то Changed (value) всё;
		кон Set;

		проц Inc* (step: целМЗ);
		перем changed: булево;
		нач AcquireWrite; changed := step # 0; увел (value, step); ReleaseWrite; если changed то Changed (value) всё;
		кон Inc;

		проц Dec* (step: целМЗ);
		перем changed: булево;
		нач AcquireWrite; changed := step # 0; умень (value, step); ReleaseWrite; если changed то Changed (value) всё;
		кон Dec;

		проц Changed (value: целМЗ);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай handlers[i] (сам, value); увел (i) кц;
		кон Changed;

		проц AddHandler* (handler: IntegerHandler);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай увел (i) кц; handlers[i] := handler;
		кон AddHandler;

	кон Integer;

	Real* = окласс (Property)
	перем
		value: вещ32;
		handlers: массив MaxHandlers из RealHandler;

		проц &InitReal* (value: вещ32);
		нач InitProperty; сам.value := value;
		кон InitReal;

		проц Get* (): вещ32;
		перем value: вещ32;
		нач AcquireRead; value := сам.value; ReleaseRead; возврат value;
		кон Get;

		проц Set* (value: вещ32);
		перем changed: булево;
		нач AcquireWrite; changed := сам.value # value; сам.value := value; ReleaseWrite; если changed то Changed (value) всё;
		кон Set;

		проц Changed (value: вещ32);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай handlers[i] (сам, value); увел (i) кц;
		кон Changed;

		проц AddHandler* (handler: RealHandler);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай увел (i) кц; handlers[i] := handler;
		кон AddHandler;

	кон Real;

	Set* = окласс (Property)
	перем
		value: мнвоНаБитахМЗ;
		handlers: массив MaxHandlers из SetHandler;

		проц &InitSet* (value: мнвоНаБитахМЗ);
		нач InitProperty; сам.value := value;
		кон InitSet;

		проц Get* (): мнвоНаБитахМЗ;
		перем value: мнвоНаБитахМЗ;
		нач AcquireRead; value := сам.value; ReleaseRead; возврат value;
		кон Get;

		проц Set* (value: мнвоНаБитахМЗ);
		перем changed: булево;
		нач AcquireWrite; changed := сам.value # value; сам.value := value; ReleaseWrite; если changed то Changed (value) всё;
		кон Set;

		проц Incl* (element: целМЗ);
		перем changed: булево;
		нач AcquireWrite; changed := ~(element в value); включиВоМнвоНаБитах (value, element); ReleaseWrite; если changed то Changed (value) всё;
		кон Incl;

		проц Excl* (element: целМЗ);
		перем changed: булево;
		нач AcquireWrite; changed := element в value; исключиИзМнваНаБитах (value, element); ReleaseWrite; если changed то Changed (value) всё;
		кон Excl;

		проц Changed (value: мнвоНаБитахМЗ);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай handlers[i] (сам, value); увел (i) кц;
		кон Changed;

		проц AddHandler* (handler: SetHandler);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай увел (i) кц; handlers[i] := handler;
		кон AddHandler;

	кон Set;

	String* = окласс (Property)
	перем
		value: укль на массив из симв8;
		handlers: массив MaxHandlers из StringHandler;

		проц &InitString* (конст value: массив из симв8; length: размерМЗ);
		нач InitProperty; нов (сам.value, length); копируйСтрокуДо0 (value, сам.value^);
		кон InitString;

		проц Get* (перем value: массив из симв8);
		нач AcquireRead; копируйСтрокуДо0 (сам.value^, value); ReleaseRead;
		кон Get;

		проц Set* (конст value: массив из симв8);
		перем changed: булево;
		нач AcquireWrite; changed := сам.value^ # value; копируйСтрокуДо0 (value, сам.value^); ReleaseWrite; если changed то Changed (value) всё;
		кон Set;

		проц Changed (конст value: массив из симв8);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай handlers[i] (сам, value); увел (i) кц;
		кон Changed;

		проц AddHandler* (handler: StringHandler);
		перем i: размерМЗ;
		нач i := 0; нцПока handlers[i] # НУЛЬ делай увел (i) кц; handlers[i] := handler;
		кон AddHandler;

	кон String;

	(* generic message to be handled by the sequencer *)
	Message* = окласс
	перем
		next: Message; time: цел32;

		проц &InitMessage*;
		нач сам.next := НУЛЬ; time := NoDelay;
		кон InitMessage;

		проц Handle*;
		кон Handle;	(* abstract *)

	кон Message;

	тип ProcedureMessage*  = окласс (Message)
	перем
		procedure: Procedure;

		проц &InitProcedureMessage* (procedure: Procedure);
		нач InitMessage; сам.procedure := procedure;
		кон InitProcedureMessage;

		проц {перекрыта}Handle*;
		нач procedure;
		кон Handle;

	кон ProcedureMessage;

	тип BooleanMessage*  = окласс (Message)
	перем
		value: булево;
		procedure: BooleanProcedure;

		проц &InitBooleanMessage* (value: булево; procedure: BooleanProcedure);
		нач InitMessage; сам.value := value; сам.procedure := procedure;
		кон InitBooleanMessage;

		проц {перекрыта}Handle*;
		нач procedure (value);
		кон Handle;

	кон BooleanMessage;

	тип IntegerMessage*  = окласс (Message)
	перем
		value: целМЗ;
		procedure: IntegerProcedure;

		проц &InitIntegerMessage* (value: целМЗ; procedure: IntegerProcedure);
		нач InitMessage; сам.value := value; сам.procedure := procedure;
		кон InitIntegerMessage;

		проц {перекрыта}Handle*;
		нач procedure (value);
		кон Handle;

	кон IntegerMessage;

	тип IntegerIntegerMessage*  = окласс (Message)
	перем
		value0, value1: целМЗ;
		procedure: IntegerIntegerProcedure;

		проц &InitIntegerIntegerMessage* (value0, value1: целМЗ; procedure: IntegerIntegerProcedure);
		нач InitMessage; сам.value0 := value0; сам.value1 := value1; сам.procedure := procedure;
		кон InitIntegerIntegerMessage;

		проц {перекрыта}Handle*;
		нач procedure (value0, value1);
		кон Handle;

	кон IntegerIntegerMessage;

	тип RealMessage*  = окласс (Message)
	перем
		value: вещ32;
		procedure: RealProcedure;

		проц &InitRealMessage* (value: вещ32; procedure: RealProcedure);
		нач InitMessage; сам.value := value; сам.procedure := procedure;
		кон InitRealMessage;

		проц {перекрыта}Handle*;
		нач procedure (value);
		кон Handle;

	кон RealMessage;

	тип SetMessage*  = окласс (Message)
	перем
		value: мнвоНаБитахМЗ;
		procedure: SetProcedure;

		проц &InitSetMessage* (value: мнвоНаБитахМЗ; procedure: SetProcedure);
		нач InitMessage; сам.value := value; сам.procedure := procedure;
		кон InitSetMessage;

		проц {перекрыта}Handle*;
		нач procedure (value);
		кон Handle;

	кон SetMessage;

	тип StringMessage*  = окласс (Message)
	перем
		value: укль на массив из симв8;
		procedure: StringProcedure;

		проц &InitStringMessage* (конст value: массив из симв8; procedure: StringProcedure);
		перем length: размерМЗ;
		нач
			InitMessage; length := 0;
			нцПока value[length] # 0X делай увел (length); кц;
			нов (сам.value, length); копируйСтрокуДо0 (value, сам.value^); сам.procedure := procedure;
		кон InitStringMessage;

		проц {перекрыта}Handle*;
		нач procedure (value^);
		кон Handle;

	кон StringMessage;

	(* generic request that allows to wait for the message to be handled*)
	Request* = окласс (Message)
	перем
		handled: булево;

		проц &InitRequest*;
		нач InitMessage; handled := ложь;
		кон InitRequest;

		(* IMPORTANT: to be called at the end of overriding procedures *)
		проц {перекрыта}Handle*;
		нач {единолично} handled := истина
		кон Handle;

		(* awaits handling by sequencer  *)
		проц Await;
		нач {единолично} дождись (handled);
		кон Await;

	кон Request;

	IntegerRequest* = окласс (Request)
	перем
		value: целМЗ;
		procedure: IntegerProcedure;

		проц &InitIntegerRequest* (value: целМЗ; procedure: IntegerProcedure);
		нач InitRequest; сам.value := value; сам.procedure := procedure;
		кон InitIntegerRequest;

		проц {перекрыта}Handle*;
		нач procedure (value); Handle^;
		кон Handle;

	кон IntegerRequest;

	RequestBoolean* = окласс (Request)
	перем
		procedure: ProcedureBoolean;
		result-: булево;

		проц &InitRequestBoolean* (procedure: ProcedureBoolean);
		нач InitRequest; сам.procedure := procedure;
		кон InitRequestBoolean;

		проц {перекрыта}Handle*;
		нач result := procedure (); Handle^;
		кон Handle;

	кон RequestBoolean;

	RequestInteger* = окласс (Request)
	перем
		procedure: ProcedureInteger;
		result-: целМЗ;

		проц &InitRequestInteger* (procedure: ProcedureInteger);
		нач InitRequest; сам.procedure := procedure;
		кон InitRequestInteger;

		проц {перекрыта}Handle*;
		нач result := procedure (); Handle^;
		кон Handle;

	кон RequestInteger;

	RequestReal* = окласс (Request)
	перем
		procedure: ProcedureReal;
		result-: вещ32;

		проц &InitRequestReal* (procedure: ProcedureReal);
		нач InitRequest; сам.procedure := procedure;
		кон InitRequestReal;

		проц {перекрыта}Handle*;
		нач result := procedure (); Handle^;
		кон Handle;

	кон RequestReal;

	IntegerRequestBoolean* = окласс (Request)
	перем
		value: целМЗ;
		procedure: IntegerProcedureBoolean;
		result-: булево;

		проц &InitIntegerRequestBoolean* (value: целМЗ; procedure: IntegerProcedureBoolean);
		нач InitRequest; сам.value := value; сам.procedure := procedure;
		кон InitIntegerRequestBoolean;

		проц {перекрыта}Handle*;
		нач result := procedure (value); Handle^;
		кон Handle;

	кон IntegerRequestBoolean;

	RealRequestInteger* = окласс (Request)
	перем
		value: вещ32;
		procedure: RealProcedureInteger;
		result-: целМЗ;

		проц &InitRealRequestInteger* (value: вещ32; procedure: RealProcedureInteger);
		нач InitRequest; сам.value := value; сам.procedure := procedure;
		кон InitRealRequestInteger;

		проц {перекрыта}Handle*;
		нач result := procedure (value); Handle^;
		кон Handle;

	кон RealRequestInteger;

	(* generic base message sequencer class *)
	Sequencer* = окласс
	перем
		handling, woken: булево; first: Message; timer: Objects.Timer;

		проц &InitSequencer*;
		нач handling := истина; woken := ложь; first := НУЛЬ; нов (timer);
		кон InitSequencer;

		(* check wether current procedure was called by sequencer or by other active objects *)
		проц SequencerCalledThis* (): булево;
		нач возврат Objects.ActiveObject() = сам;
		кон SequencerCalledThis;

		проц HandleMessages;
		перем message: Message;
		нач {единолично}
			нцПока first # НУЛЬ делай
				если (first.time # NoDelay) и (first.time - Kernel.GetTicks () > 0) то возврат всё;
				message := first; first := message.next; message.next := НУЛЬ; message.Handle;
			кц;
		кон HandleMessages;

		проц Add*(message: Message; time: цел32);
		перем prev, next: Message;
		нач
			нач {единолично}
				утв (~SequencerCalledThis ());
				утв (message.next = НУЛЬ);
				prev := НУЛЬ; next := first;
				нцПока (next # НУЛЬ) и (next.time <= time) делай prev := next; next := next.next кц;
				если prev = НУЛЬ то first := message; woken := time # NoDelay; иначе prev.next := message всё;
				message.next := next; message.time := time;
			кон;
			если message суть Request то message(Request).Await всё;
		кон Add;

		проц AddMessage* (procedure: Procedure);
		перем message: ProcedureMessage;
		нач нов (message, procedure); Add (message, NoDelay);
		кон AddMessage;

		проц AddBooleanMessage* (value: булево; procedure: BooleanProcedure);
		перем message: BooleanMessage;
		нач нов (message, value, procedure); Add (message, NoDelay);
		кон AddBooleanMessage;

		проц AddIntegerMessage* (value: целМЗ; procedure: IntegerProcedure);
		перем message: IntegerMessage;
		нач нов (message, value, procedure); Add (message, NoDelay);
		кон AddIntegerMessage;

		проц AddRealMessage* (value: вещ32; procedure: RealProcedure);
		перем message: RealMessage;
		нач нов (message, value, procedure); Add (message, NoDelay);
		кон AddRealMessage;

		проц AddSetMessage* (value: мнвоНаБитахМЗ; procedure: SetProcedure);
		перем message: SetMessage;
		нач нов (message, value, procedure); Add (message, NoDelay);
		кон AddSetMessage;

		проц AddStringMessage* (конст value: массив из симв8; procedure: StringProcedure);
		перем message: StringMessage;
		нач нов (message, value, procedure); Add (message, NoDelay);
		кон AddStringMessage;

		проц AddIntegerIntegerMessage* (value0, value1: целМЗ; procedure: IntegerIntegerProcedure);
		перем message: IntegerIntegerMessage;
		нач нов (message, value0, value1, procedure); Add (message, NoDelay);
		кон AddIntegerIntegerMessage;

		проц AddIntegerRequest* (value: целМЗ; procedure: IntegerProcedure);
		перем request: IntegerRequest;
		нач нов (request, value, procedure); Add (request, NoDelay);
		кон AddIntegerRequest;

		проц AddRequestBoolean* (procedure: ProcedureBoolean): булево;
		перем request: RequestBoolean;
		нач нов (request, procedure); Add (request, NoDelay); возврат request.result;
		кон AddRequestBoolean;

		проц AddRequestInteger* (procedure: ProcedureInteger): целМЗ;
		перем request: RequestInteger;
		нач нов (request, procedure); Add (request, NoDelay); возврат request.result;
		кон AddRequestInteger;

		проц AddRequestReal* (procedure: ProcedureReal): вещ32;
		перем request: RequestReal;
		нач нов (request, procedure); Add (request, NoDelay); возврат request.result;
		кон AddRequestReal;

		проц AddIntegerRequestBoolean* (value: целМЗ; procedure: IntegerProcedureBoolean): булево;
		перем request: IntegerRequestBoolean;
		нач нов (request, value, procedure); Add (request, NoDelay); возврат request.result;
		кон AddIntegerRequestBoolean;

		проц AddRealRequestInteger* (value: вещ32; procedure: RealProcedureInteger): целМЗ;
		перем request: RealRequestInteger;
		нач нов (request, value, procedure); Add (request, NoDelay); возврат request.result;
		кон AddRealRequestInteger;

		проц Remove*(message: Message);
		перем prev, next: Message;
		нач
			утв (SequencerCalledThis ());
			если message = НУЛЬ то возврат всё;
			prev := НУЛЬ; next := first;
			нцПока (next # НУЛЬ) и (next # message) делай prev := next; next := next.next кц;
			если next = message то
				если prev = НУЛЬ то first := message.next; woken := истина; иначе prev.next := message.next всё;
			всё;
			message.next := НУЛЬ;
		кон Remove;

		(* this procedure is called sequentially and can be overridden in order to do contiguous work *)
		проц Handle*;
		нач
			если (first # НУЛЬ) и (first.time # NoDelay) то Objects.SetTimeoutAt (timer, Wakeup, first.time) всё;
			дождись ((first # НУЛЬ) и (first.time = NoDelay) или ~handling или woken);
			Objects.CancelTimeout (timer); woken := ложь;
		кон Handle;

		проц Wakeup;
		нач {единолично} woken := истина;
		кон Wakeup;

		проц Stop*;
		нач {единолично} handling := ложь;
		кон Stop;

	нач {активное}
		нцПока handling делай HandleMessages; нач {единолично} Handle кон кц;
	кон Sequencer;

(* helper types *)
	BooleanHandler = проц {делегат} (property: Boolean; value: булево);
	IntegerHandler = проц {делегат} (property: Integer; value: целМЗ);
	RealHandler = проц {делегат} (property: Real; value: вещ32);
	SetHandler = проц {делегат} (property: Set; value: мнвоНаБитахМЗ);
	StringHandler = проц {делегат} (property: String; конст value: массив из симв8);

	Procedure = проц {делегат};
	BooleanProcedure = проц {делегат} (value: булево);
	IntegerProcedure = проц {делегат} (value: целМЗ);
	IntegerIntegerProcedure = проц {делегат} (value0, value1: целМЗ);
	RealProcedure = проц {делегат} (value: вещ32);
	SetProcedure = проц {делегат} (value: мнвоНаБитахМЗ);
	StringProcedure = проц {делегат} (конст value: массив из симв8);

	ProcedureBoolean = проц {делегат} (): булево;
	ProcedureInteger = проц {делегат} (): целМЗ;
	ProcedureReal = проц {делегат} (): вещ32;

	IntegerProcedureBoolean = проц {делегат} (value: целМЗ): булево;
	RealProcedureInteger = проц {делегат} (value: вещ32): целМЗ;

(* helper function for delayed execution *)
проц Delay* (delay: цел32): цел32;
нач возврат delay + Kernel.GetTicks ();
кон Delay;

кон A2Sequencers.

Open issues:
- first parameter of event procedures concrete or abstract property type?
- no concrete request types implemented
- serializable interface of properties not implemented
