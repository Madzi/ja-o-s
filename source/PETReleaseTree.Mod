модуль PETReleaseTree; (** AUTHOR "staubesv"; PURPOSE "Visualize build description structure as tree"; *)

использует
	Потоки, Strings, Diagnostics, Build := СборщикВыпускаЯОС, PETTrees, WMTrees;

конст
	Title = " Пакеты";
	TitleError = " Пакеты (Ошибки)";

тип
	String = массив 128 из симв8;

	Tree* = окласс (PETTrees.Tree)

		проц & {перекрыта}Init*;
		нач
			Init^;
			SetTitle(Title);
		кон Init;

		проц {перекрыта}AddNodes*(parent : PETTrees.TreeNode; diagnostics : Diagnostics.Diagnostics; log : Потоки.Писарь);
		перем builds : Build.ДанныеОКонфигурациях; succeeded : булево;
		нач
			AddNodes^(parent, diagnostics, log);
			succeeded := Build.РазбериОписаниеКонфигурации(editor.text, "ReleaseTree", builds, log, diagnostics);
			если succeeded то
				tree.SetNodeCaption(parent, Strings.NewString("Packages"));
				tree.SetNodeState(parent, {WMTrees.NodeAlwaysExpanded});
				нов(parent.pos, editor.text);
				parent.pos.SetPosition(0);
				AddBuilds(parent, builds);
				AddPackages(parent, builds.пакеты.ДайВсе());
				SetTitle(Title);
			иначе
				SetTitle(TitleError);
			всё;
		кон AddNodes;

		проц AddBuilds(parent : WMTrees.TreeNode; builds : Build.ДанныеОКонфигурациях);
		перем node : PETTrees.TreeNode; nofBuilds, i : цел32;
		нач
			nofBuilds := 0;
			node := NewNode(parent, "Конфигурации");
			нцДля i := 0 до длинаМассива(builds.Конфигурации)-1 делай
				если (builds.Конфигурации[i] # НУЛЬ) то
					если (nofBuilds = 0) то
						SetNodeInfo(node, builds.Конфигурации[i].местоВФайле);
					всё;
					увел(nofBuilds);
					AddBuild(node, builds.Конфигурации[i]);
				всё;
			кц;
			AddNumberPostfixToCaption(node, nofBuilds);
		кон AddBuilds;

		проц AddBuild(parent : WMTrees.TreeNode; build : Build.ДанныеОб1Конфигурации);
		перем buildNode, node : PETTrees.TreeNode; nofSources, nofFiles : размерМЗ;
		нач
			утв(build # НУЛЬ);
			buildNode := NewNode(parent, build.имя);
			SetNodeInfo(buildNode, build.местоВФайле);
			build.ДайКвоИсходниковИФайлов(nofSources, nofFiles);
			node := NewNode(buildNode, GetNumberCaption("Исходников: ", nofSources));
			node := NewNode(buildNode, GetNumberCaption("Файлов: ", nofFiles));
		кон AddBuild;

		проц AddPackages(parent : WMTrees.TreeNode; packages : Build.уРядПакетов);
		перем node : PETTrees.TreeNode; nofPackages, i : размерМЗ;
		нач
			nofPackages := 0;
			node := NewNode(parent, "Пакеты");
			tree.SetNodeState(node, {WMTrees.NodeExpanded});
			если (packages # НУЛЬ) то
				нцДля i := 0 до длинаМассива(packages)-1 делай
					если (packages[i] # НУЛЬ) то
						если (nofPackages = 0) то
							SetNodeInfo(node, packages[i].местоВФайле);
						всё;
						увел(nofPackages);
						AddPackage(node, packages[i]);
					всё;
				кц;
			всё;
			AddNumberPostfixToCaption(node, nofPackages);
		кон AddPackages;

		проц AddPackage(parent : WMTrees.TreeNode; package : Build.Пакет);
		перем node, packageNode : PETTrees.TreeNode;
		нач
			утв((parent # НУЛЬ) и (package # НУЛЬ));
			packageNode := NewNode(parent, package.имя);
			SetNodeInfo(packageNode, package.местоВФайле);

			node := NewNode(packageNode, GetNumberCaption("Исходников: ", package.квоИсходников));
			node := NewNode(packageNode, GetNumberCaption("Файлов: ", package.квоФайлов));
			node := NewNode(packageNode, GetCaption("Архив: ", package.п_архив));
			node := NewNode(packageNode, GetCaption("АрхивИсходников: ", package.п_архивИсходников));
		кон AddPackage;

		проц SetNodeInfo(node : PETTrees.TreeNode; position : Потоки.ТипМестоВПотоке);
		нач
			утв(node # НУЛЬ);
			если (position >= 0) то
				нов(node.pos, editor.text);
				node.pos.SetPosition(position(размерМЗ));
			иначе
				node.pos := НУЛЬ;
			всё;
		кон SetNodeInfo;

		проц NewNode(parent: WMTrees.TreeNode; конст caption: массив из симв8): PETTrees.TreeNode;
		перем node: PETTrees.TreeNode;
		нач
			утв(parent # НУЛЬ);
			нов(node);
			tree.SetNodeCaption(node, Strings.NewString(caption));
			tree.AddChildNode(parent, node);
			утв(node # НУЛЬ);
			возврат node;
		кон NewNode;

	кон Tree;

проц GetNumberCaption(конст string : массив из симв8; number : размерМЗ) : String;
перем caption : String; nbr : массив 16 из симв8;
нач
	копируйСтрокуДо0(string, caption); Strings.IntToStr(number, nbr); Strings.Append(caption, nbr);
	возврат caption;
кон GetNumberCaption;

проц GetCaption(конст string1, string2 : массив из симв8) : String;
перем caption : String;
нач
	копируйСтрокуДо0(string1, caption); Strings.Append(caption, string2);
	возврат caption;
кон GetCaption;

проц GenBuildTree*() : PETTrees.Tree;
перем tree : Tree;
нач
	нов(tree); возврат tree;
кон GenBuildTree;

кон PETReleaseTree.

System.Free ReleaseTree ~
