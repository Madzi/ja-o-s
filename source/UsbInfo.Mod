модуль UsbInfo; (** AUTHOR "staubesv"; PURPOSE "USB topology info" *)
(**
 * This Module doesn't add any functionality to the USB system software. Its purpose is to represent the current state of
 * the USB system software and to control trace options.
 *
 * Usage:
 *
 *	(* Information *)
 *	UsbInfo.Show ~ will display the current USB topology
 *	UsbInfo.Show details ~ will display the current USB topology with all available information (descriptors, configurations,...)
 *	UsbInfo.ShowDrivers ~ will display information about registered USB device drivers and their instances
 *	UsbInfo.ShowHc ~ will display all registered USB host controllers
 *	UsbInfo.ShowHc details ~ will display diagnostic information of all registered USB host controllers
 * 	UsbInfo.ShowHc schedule ~ will display the scheduling data structures of all host controllers
 *	UsbInfo.ShowHc pipes ~ will display all allocated pipes
 *	UsbInfo.ShowHc pipemore ~ will display all allocated pipes including their QH/TDs
 *	UsbInfo.ShowHc all ~ will display both the HC diagnostic information and its scheduling data structures
 *
 *	(* Trace options *)
 *	UsbInfo.TraceAll ~ enables all trace options
 *	UsbInfo.TraceNone ~ disables all trace options
 *	UsbInfo.TraceShow ~ show state of trace options
 *
 *	(* See UsbDebug.Mod for a description of the individual trace optinos *)
 *	UsbInfo.TraceOn Dm~ 			UsbInfo.TraceOff  Dm~
 *	UsbInfo.TraceOn Parsing~ 		UsbInfo.TraceOff  Parsing~
 *	UsbInfo.TraceOn DeviceStates~	UsbInfo.TraceOff  DeviceStates~
 *	UsbInfo.TraceOn Control~ 		UsbInfo.TraceOff  Control~
 *	UsbInfo.TraceOn ControlData~ 	UsbInfo.TraceOff  ControlData~
 *	UsbInfo.TraceOn Transfers~ 		UsbInfo.TraceOff  Transfers~
 *	UsbInfo.TraceOn Failed~ 			UsbInfo.TraceOff  Failed~
 *	UsbInfo.TraceOn ShortPackets ~	UsbInfo.TraceOff  ShortPackets ~
 *	UsbInfo.TraceOn Pipes~ 			UsbInfo.TraceOff  Pipes~
 *	UsbInfo.TraceOn Copying~ 		UsbInfo.TraceOff  Copying~
 *	UsbInfo.TraceOn Ioc~ 			UsbInfo.TraceOff  Ioc~
 *	UsbInfo.TraceOn Init~ 			UsbInfo.TraceOff  Init~
 *	UsbInfo.TraceOn Interrupts~ 		UsbInfo.TraceOff  Interrupts~
 *	UsbInfo.TraceOn Queuing~ 		UsbInfo.TraceOff  Queuing~
 *	UsbInfo.TraceOn HubRequests~ 	UsbInfo.TraceOff  HubRequests~
 *	UsbInfo.TraceOn Suspend~		UsbInfo.TraceOff  Suspend~
 *	UsbInfo.TraceOn Connects~		UsbInfo.TraceOff  Connects~
 *	UsbInfo.TraceOn Info~ 			UsbInfo.TraceOff  Info~
 *	UsbInfo.TraceOn Sensing~		UsbInfo.TraceOff  Sensing~
 *	UsbInfo.TraceOn ScRequests~ 	UsbInfo.TraceOff  ScRequests~
 *	UsbInfo.TraceOn ScTransfers~ 	UsbInfo.TraceOff  ScTransfers~
 *	UsbInfo.TraceOn CSWs~ 			UsbInfo.TraceOff  CSWs~
 *	UsbInfo.TraceOn CBWs~ 			UsbInfo.TraceOff  CBWs~
 *	UsbInfo.TraceOn ScInit~		 	UsbInfo.TraceOff  ScInit~
 *	UsbInfo.TraceOn Custom~		UsbInfo.TraceOff Custom~
 *
 *	UsbInfo.TraceOn Info Sensing ScRequests ScTransfers CSWs CBWs ScInit~ turns on all mass storage device related trace options
 *	UsbInfo.TraceOff Info Sensing ScRequests ScTransfers CSWs CBWs ScInit~ turns off ...
 *
 *	UsbInfo.TraceOn Dm Parsing DeviceStates Failed Pipes Init HubRequests Connects Info ~ is interesting when connecting devices
 *
 *	System.Free UsbInfo ~
 *
 * History:
 *
 *	17.11.2005	Created (staubesv)
 *	12.12.2005	Added schedule, pipes & all parameter to ShowHc (staubesv)
 *	01.02.2006	Adapted ShowHc to UsbHcdi changes (staubesv)
 *	06.02.2006	Added trace option control (staubesv)
 *	26.02.2006	Added Custom trace option (staubesv)
 *	28.06.2006	Adapted to modified Usb.GetRootHubs procedure (staubesv)
 *	04.07.2006	Added ShowHc pipemore (staubesv)
 *	05.01.2007	Added ShortPackets trace option, call ShowConfiguration in ShowDevice (staubesv)
 *)

использует
	НИЗКОУР,
	Потоки, Commands, Plugins, Strings,
	UsbHcdi, Usb, Usbdi, UsbDebug;

проц ShowDeviceName(dev : Usb.UsbDevice; out : Потоки.Писарь);
перем descriptor : Usb.DeviceDescriptor;
нач
	descriptor := dev.descriptor (Usb.DeviceDescriptor);
	если (descriptor # НУЛЬ) и (descriptor.sManufacturer # НУЛЬ) или (descriptor.sProduct # НУЛЬ) то
		если descriptor.sManufacturer # НУЛЬ то out.пСтроку8(descriptor.sManufacturer^); out.пСимв8(" "); всё;
		если descriptor.sProduct # НУЛЬ то out.пСтроку8(descriptor.sProduct^); всё;
	иначе
		out.пСтроку8("unknown device");
	всё;
кон ShowDeviceName;

(* Shows device descriptor / qualifier information and all configurations including its interfaces and endpoints. *)
проц ShowDevice(dev : Usb.UsbDevice; indent : цел32; details : булево; out : Потоки.Писарь);
перем a, c, e, i : цел32;
нач
	если dev.hubFlag то
		если dev.parent = dev то out.пСтроку8("Root "); всё;
		out.пСтроку8("Hub with "); out.пЦел64(dev.nbrOfPorts, 0); out.пСтроку8(" ports: ");
	всё;
	ShowDeviceName(dev, out);
	если ~details или (dev.hubFlag и (dev.parent = dev)) то возврат всё;
	out.пСтроку8("(S/N: ");
	если dev.descriptor(Usb.DeviceDescriptor).sSerialNumber # НУЛЬ то out.пСтроку8(dev.descriptor(Usb.DeviceDescriptor).sSerialNumber^); out.пСтроку8(")");
	иначе out.пСтроку8("Not available)");
	всё;
	out.пВК_ПС;
	Indent(indent+4, out);
	out.пСтроку8("Address: "); out.пЦел64(dev.address, 0); out.пСтроку8(" ");
	если dev.speed = UsbHcdi.LowSpeed то out.пСтроку8(" [LowSpeed]");
	аесли dev.speed = UsbHcdi.FullSpeed то out.пСтроку8(" [FullSpeed]");
	аесли dev.speed = UsbHcdi.HighSpeed то out.пСтроку8(" [HighSpeed]");
	иначе out.пСтроку8(" [UnknownSpeed!!!]");
	всё;
	out.пВК_ПС;
	Indent(indent+4, out); out.пСтроку8("Device descriptor information: ");
	out.пВК_ПС;
	ShowDescriptor(dev.descriptor (Usb.DeviceDescriptor), indent+8, out);
	(* List all configurations *)
	нцДля c := 0 до dev.descriptor.bNumConfigurations-1 делай
		Indent(indent+12, out);
		out.пСтроку8("Configuration "); out.пЦел64(c, 0); out.пСтроку8(":");
		если dev.configurations[c](Usb.ConfigurationDescriptor).sConfiguration # НУЛЬ то  out.пСтроку8(dev.configurations[c](Usb.ConfigurationDescriptor).sConfiguration^); всё;
		если dev.actConfiguration = dev.configurations[c] то out.пСтроку8(" [active]"); всё; out.пВК_ПС;
		ShowConfiguration(dev.configurations[c](Usb.ConfigurationDescriptor), indent + 16, out);
		out.пВК_ПС;
		(* List all interfaces *)
		нцДля i := 0 до dev.configurations[c].bNumInterfaces - 1 делай
			Indent(indent+16, out);
			out.пСтроку8("Interface "); out.пЦел64(i, 0); out.пСтроку8(": "); out.пВК_ПС;
			ShowInterface(dev.configurations[c].interfaces[i] (Usb.InterfaceDescriptor), indent+20, out); out.пВК_ПС;
			(* List all endpoints *)
			нцДля e := 0 до dev.configurations[c].interfaces[i].bNumEndpoints-1 делай
				ShowEndpoint(dev.configurations[c].interfaces[i].endpoints[e] (Usb.EndpointDescriptor), indent+24, out);
			кц;
			(* List alternate interface if available *)
			Indent(indent+20, out);
			out.пСтроку8("Alternate interface: ");
			если dev.configurations[c].interfaces[i].numAlternateInterfaces = 0 то out.пСтроку8("n/a");  out.пВК_ПС;
			иначе
				нцДля a := 0 до dev.configurations[c].interfaces[i].numAlternateInterfaces-1 делай
					Indent(indent+20, out); out.пСтроку8("Alternate Interface "); out.пЦел64(a, 0); out.пСтроку8(": ");
					ShowInterface(dev.configurations[c].interfaces[i].alternateInterfaces[a] (Usb.InterfaceDescriptor), indent+20, out); out.пВК_ПС;
					(* List all endpoints *)
					нцДля e := 0 до dev.configurations[c].interfaces[i].bNumEndpoints-1 делай
						ShowEndpoint(dev.configurations[c].interfaces[i].alternateInterfaces[a].endpoints[e] (Usb.EndpointDescriptor), indent+24, out);
					кц;
				кц;
				out.пВК_ПС;
			всё;
		кц;
	кц;
	Indent(indent+4, out);
	out.пСтроку8("Device qualifier information: ");
	если dev.qualifier = НУЛЬ то out.пСтроку8("n/a"); out.пВК_ПС;
	иначе
		out.пВК_ПС;
		ShowDescriptor(dev.qualifier (Usb.DeviceDescriptor), indent + 8, out);
		нцДля c := 0 до dev.qualifier.bNumConfigurations-1 делай
			Indent(indent+12, out);
			out.пСтроку8("Other-Speed Configuration "); out.пЦел64(c, 0); out.пСтроку8(":");
			если dev.otherconfigurations[c](Usb.ConfigurationDescriptor).sConfiguration#НУЛЬ то  out.пСтроку8(dev.configurations[c](Usb.ConfigurationDescriptor).sConfiguration^); всё;
			(* List all interfaces *)
			нцДля i := 0 до dev.otherconfigurations[c].bNumInterfaces - 1 делай
				out.пВК_ПС; Indent(indent+16, out);
				out.пСтроку8("Interface "); out.пЦел64(i, 0); out.пСтроку8(": "); out.пВК_ПС;
				ShowInterface(dev.otherconfigurations[c].interfaces[i] (Usb.InterfaceDescriptor), indent+20, out); out.пВК_ПС;
				(* List all endpoints *)
				нцДля e := 0 до dev.otherconfigurations[c].interfaces[i].bNumEndpoints-1 делай
					ShowEndpoint(dev.otherconfigurations[c].interfaces[i].endpoints[e] (Usb.EndpointDescriptor), indent+24, out);
				кц;
				(* List alternate interface if available *)
				Indent(indent+16, out);
				out.пСтроку8("Alternate interface: ");
				если dev.otherconfigurations[c].interfaces[i].numAlternateInterfaces = 0 то out.пСтроку8("n/a");  out.пВК_ПС;
				иначе
					нцДля a := 0 до dev.otherconfigurations[c].interfaces[i].numAlternateInterfaces-1 делай
						out.пСтроку8("Alternate Interface "); out.пЦел64(a, 0); out.пСтроку8(": "); out.пВК_ПС;
						ShowInterface(dev.otherconfigurations[c].interfaces[i].alternateInterfaces[a] (Usb.InterfaceDescriptor), indent+20, out); out.пВК_ПС;
						(* List all endpoints *)
						нцДля e := 0 до dev.otherconfigurations[c].interfaces[i].bNumEndpoints-1 делай
							ShowEndpoint(dev.otherconfigurations[c].interfaces[i].alternateInterfaces[a].endpoints[e] (Usb.EndpointDescriptor), indent+24, out);
						кц;
					кц;
				всё;
			кц;
		кц;
	всё;
кон ShowDevice;

(* Display textual respresenation of device descriptor or device qualifier *)
проц ShowDescriptor(d : Usb.DeviceDescriptor;  indent : цел32; out : Потоки.Писарь);
нач
	Indent(indent, out);
	out.пСтроку8("USB Version: "); PrintHex(логСдвиг(d.bcdUSB, -8), out); out.пСимв8("."); PrintHex(d.bcdUSB остОтДеленияНа 100H, out);
	out.пСтроку8(", Device Class: "); PrintHex(d.bDeviceClass, out);
	out.пСтроку8("H, Subclass: "); PrintHex(d.bDeviceSubClass, out);
	out.пСтроку8("H, Protocol: "); PrintHex(d.bDeviceProtocol, out); out.пСтроку8("H");
	out.пВК_ПС;
	Indent(indent, out);
	out.пСтроку8("MaxPacketSize0: "); out.пЦел64(d.bMaxPacketSize0, 0); out.пСтроку8(" Bytes"); out.пВК_ПС;
	Indent(indent, out);
	out.пСтроку8("idVendor: "); PrintHex(d.idVendor, out);
	out.пСтроку8("H,  idProduct: "); PrintHex(d.idProduct, out);
	out.пСтроку8("H,  Device Version: "); PrintHex(логСдвиг(d.bcdDevice, -8), out); out.пСимв8("."); PrintHex(d.bcdDevice остОтДеленияНа 100H, out);
	out.пВК_ПС;
кон ShowDescriptor;

(* Display textual respresentation of a USB device configuration *)
проц ShowConfiguration(c : Usb.ConfigurationDescriptor; indent : цел32; out : Потоки.Писарь);
нач
	Indent(indent, out); out.пСтроку8("ConfigurationValue: "); out.пЦел64(c.bConfigurationValue, 0); out.пВК_ПС;
	Indent(indent, out); out.пСтроку8 ("MaxPower: "); out.пЦел64(c.bMaxPower, 0); out.пСтроку8(" mA  "); out.пВК_ПС;
	Indent(indent, out); out.пСтроку8("Power support: ");
	если c.bmAttributes * {6} # {} то out.пСтроку8("Self-Powered"); иначе out.пСтроку8("Bus-Powered"); всё;
	out.пВК_ПС;
	Indent(indent, out); out.пСтроку8("Remote Wake-up support: ");
	если c.bmAttributes * {5} # {} то out.пСтроку8("Yes"); иначе out.пСтроку8("No"); всё;
кон ShowConfiguration;

(* Display textual representation of a USB device interface *)
проц ShowInterface(i : Usb.InterfaceDescriptor; indent : цел32; out : Потоки.Писарь);
перем drv : Usbdi.Driver;
нач
	Indent(indent, out);
	если i.sInterface # НУЛЬ то out.пСтроку8(i.sInterface^); out.пСтроку8(": "); всё;
	out.пСтроку8("[Class: "); PrintHex(i.bInterfaceClass, out);
	out.пСтроку8("H Subclass: "); PrintHex(i.bInterfaceSubClass, out);
	out.пСтроку8("H Protocol: "); PrintHex(i.bInterfaceProtocol, out);
	out.пСтроку8("H #Endpoints: "); out.пЦел64(i.bNumEndpoints, 0);
	out.пСтроку8("]"); out.пВК_ПС;
	Indent(indent, out);
	drv := i.driver;
	out.пСтроку8("Driver: ");
	если drv # НУЛЬ то
		out.пСтроку8("["); out.пСтроку8(drv.name);
		out.пСтроку8("("); out.пСтроку8(drv.desc); out.пСтроку8(")]");
	иначе out.пСтроку8("[No driver installed for this interface]");
	всё;
кон ShowInterface;

(* Display textual representation of a USB device endpoint *)
проц ShowEndpoint(e : Usb.EndpointDescriptor; indent : цел32; out : Потоки.Писарь);
перем attr : цел32;
нач
	Indent(indent, out);
	out.пСтроку8("Endpoint "); 	out.пЦел64(e.bEndpointAddress остОтДеленияНа 16, 0);
	out.пСтроку8(":"); out.пСтроку8(" [Type: ");
	attr := НИЗКОУР.подмениТипЗначения(цел32, e.bmAttributes);
	просей attr из
		0 : out.пСтроку8("Control");
		|1 : out.пСтроку8("Isochronous");
		|2 : out.пСтроку8("Bulk");
		|3 : out.пСтроку8("Interrupt");
	иначе
		out.пСтроку8("unknown");
	всё;
	если (attr#0) и (attr<4) то
		out.пСтроку8("(");
		если (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, e.bEndpointAddress) * {7}) # {} то	 out.пСтроку8("IN"); out.пСтроку8(")");
		иначе out.пСтроку8("OUT)");
		всё;
	всё;
	если attr = 1 то
		out.пСтроку8(", Synchronization: ");
		просей НИЗКОУР.подмениТипЗначения(цел32, логСдвиг(НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, attr) * {2..3}, -2)) из
			0 : out.пСтроку8("None");
			|1: out.пСтроку8("Asynchronous");
			|2: out.пСтроку8("Adaptive");
			|3: out.пСтроку8("Synchronous");
		всё;
		out.пСтроку8(", Usage: ");
		просей НИЗКОУР.подмениТипЗначения(цел32, логСдвиг(НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, attr) * {2..3}, -2)) из
			0 : out.пСтроку8("Data");
			|1: out.пСтроку8("Feedback");
			|2: out.пСтроку8("Implicit Feedback");
			|3: out.пСтроку8("Reserved");
		всё;
	всё;
	out.пСтроку8(" MaxPacketSize: "); out.пЦел64(e.wMaxPacketSize, 0);
	out.пСтроку8(" Bytes IRQinterval: "); out.пЦел64(e.bInterval, 0); out.пСтроку8(" ms]");
	out.пВК_ПС;
кон ShowEndpoint;

(* Display textual representation of the specified device and its descendants *)
проц ShowDeviceChain(dev : Usb.UsbDevice; indent : цел32; details : булево; out : Потоки.Писарь);
перем i, j : цел32;
нач
	если dev = НУЛЬ то out.пСтроку8("No device attached");
	аесли dev.hubFlag то
		ShowDevice(dev, indent, details, out); out.пВК_ПС;
		нцДля i := 0 до dev.nbrOfPorts - 1 делай
			нцДля j := 0 до indent - 1 делай out.пСимв8(" "); кц;
			out.пСтроку8("    Port "); out.пЦел64(i+1, 0); out.пСтроку8(": ");
			если dev.deviceAtPort[i] = НУЛЬ то out.пСтроку8("No device attached."); out.пВК_ПС;
			аесли dev.portPermanentDisabled[i] то out.пСтроку8("Permanent disable (error)"); out.пВК_ПС;
			иначе ShowDeviceChain(dev.deviceAtPort[i], indent+8, details, out);
			всё;
		кц;
	иначе ShowDevice(dev, indent, details, out); out.пВК_ПС;
	всё;
кон ShowDeviceChain;

проц DoShow*(out : Потоки.Писарь; details: булево);
перем
	i : размерМЗ;
	rootHubs : Usb.RootHubArray;
нач
	out.пСтроку8("Usb: Topology and device information: "); out.пВК_ПС;
	Usb.GetRootHubs(rootHubs);
	нач {единолично}
		если rootHubs # НУЛЬ то
			нцДля i := 0 до длинаМассива(rootHubs)-1 делай
				ShowDeviceChain(rootHubs[i], 0, details, out);
				rootHubs[i] := НУЛЬ;
			кц;
		иначе out.пСтроку8("No USB host controllers found."); out.пВК_ПС;
		всё;
	кон;
кон DoShow;


(** Prints information about current usb tree *)
проц Show*(context : Commands.Context);
перем
	details : булево;
	pstr : массив 10 из симв8;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(pstr);
	details :=  Strings.Match("details", pstr);
	DoShow(context.out, details);
кон Show;

(** Shows all registered drivers and their instances *)
проц ShowDrivers*(context : Commands.Context);
перем instances : Plugins.Table; i : размерМЗ;
нач
	Usb.drivers.Show;
	context.out.пВК_ПС; context.out.пСтроку8("Usb: Instances of registered device drivers: "); context.out.пВК_ПС;
	Usb.usbDrivers.GetAll(instances);
	если instances=НУЛЬ то
		context.out.пСтроку8("no device drivers instances installed"); context.out.пВК_ПС;
	иначе
		нцДля i:=0 до длинаМассива(instances)-1 делай
			context.out.пСтроку8("   ");
			context.out.пСтроку8(instances[i].name); context.out.пСтроку8(" (");
			context.out.пСтроку8(instances[i].desc); context.out.пСтроку8(")");
			context.out.пВК_ПС;
		кц;
	всё;
кон ShowDrivers;

(** Shows all registered USB host controllers. *)
проц ShowHc*(context : Commands.Context); (* ["details"|"schedule"|"all"] ~ *)
перем
	table : Plugins.Table; hcd : UsbHcdi.Hcd;
	pstr : массив 10 из симв8;
	i : размерМЗ;

	проц ShowPipes(hcd : UsbHcdi.Hcd; details : булево; tds: булево);
	перем i, j, k : размерМЗ; pipe : UsbHcdi.Pipe;
	нач
		нцДля i := 0 до 127 делай (* search all addresses ... *)
			нцДля j := 0 до 15 делай (* ... and all endpoints for presence of a pipe *)
				нцДля k := 0 до 1 делай
					pipe := hcd.pipes[i][k][j];
					если pipe # НУЛЬ то
						context.out.пСтроку8("ADR: "); context.out.пЦел64(i, 0); context.out.пСтроку8(": "); pipe.Show(details);
						если tds то hcd.ShowPipe(pipe) всё;
					всё;
				кц;
			кц;
		кц;
	кон ShowPipes;

нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(pstr);
	UsbHcdi.controllers.GetAll(table);
	если table # НУЛЬ то
		нцДля i := 0 до длинаМассива(table)-1 делай
			hcd := table[i] (UsbHcdi.Hcd);
			context.out.пСтроку8("**** "); context.out.пСтроку8(hcd.name); context.out.пСтроку8(" ("); context.out.пСтроку8(hcd.desc); context.out.пСтроку8(")"); context.out.пВК_ПС;
			если Strings.Match("schedule", pstr) то
				если UsbDebug.Trace то hcd.ShowSchedule; иначе context.out.пСтроку8("UsbInfo: UsbDebug.Trace is FALSE. Cannot show schedule."); context.out.пВК_ПС; всё;
			аесли Strings.Match("details", pstr) то
				если UsbDebug.Trace то hcd.Diag; иначе context.out.пСтроку8("UsbInfo: UsbDebug.Trace is FALSE. Cannot show diagnostics."); context.out.пВК_ПС; всё;
			аесли Strings.Match("pipes", pstr) то
				ShowPipes(hcd, ложь, ложь);
			аесли Strings.Match("pipemore", pstr) то
				ShowPipes(hcd, истина, ложь);
			аесли Strings.Match("tds", pstr) то
				ShowPipes(hcd, истина, истина);
			аесли Strings.Match("all", pstr) то
				если UsbDebug.Trace то hcd.Diag; hcd.ShowSchedule;
				иначе
					context.out.пСтроку8("UsbInfo: UsbDebug.Trace is FALSE. Cannot show schedule/diagnostics."); context.out.пВК_ПС;
				всё;
				ShowPipes(hcd, истина, ложь);
			всё;
			context.out.пВК_ПС;
		кц;
	иначе
		context.out.пСтроку8("UsbInfo: No USB host controllers found."); context.out.пВК_ПС;
	всё;
кон ShowHc;

(* Helper: Displays the number <was> in hex to the kernel log *)
проц PrintHex(was: цел32; out : Потоки.Писарь);
перем z,d,h,i:цел32;
нач
	z := 0;
	d := 16*16*16*16*16*16*16; (* what a quick hack *)
	нцДля i:=0 до 7 делай
		h := (was DIV d) остОтДеленияНа 16;
		если (z = 1) или (h # 0) или (i = 7) то
			z := 1;
			если h < 10 то out.пЦел64(h,0); иначе out.пСимв8(симв8ИзКода(кодСимв8("A")+h-10)); всё;
		всё;
		d:=d DIV 16;
	кц;
кон PrintHex;

(* Helper *)
проц Indent(indent : цел32; out : Потоки.Писарь);
перем i : цел32;
нач
	нцДля i := 0 до indent-1 делай out.пСимв8(" "); кц;
кон Indent;

(** Trace options interface *)

проц TraceAll*(context : Commands.Context);
нач
	если UsbDebug.Trace то
		context.out.пСтроку8("UsbInfo: All trace options enabled."); context.out.пВК_ПС;
		UsbDebug.traceDm := истина;
		UsbDebug.traceParsing := истина;
		UsbDebug.traceDeviceStates := истина;
		UsbDebug.traceControl := истина;
		UsbDebug.traceControlData := истина;
		UsbDebug.traceTransfers := истина;
		UsbDebug.traceFailed := истина;
		UsbDebug.traceShortPackets := истина;
		UsbDebug.tracePipes := истина;
		UsbDebug.traceCopying := истина;
		UsbDebug.traceIoc := истина;
		UsbDebug.traceInit := истина;
		UsbDebug.traceInterrupts := истина;
		UsbDebug.traceQueuing := истина;
		UsbDebug.traceHubRequests := истина;
		UsbDebug.traceSuspend := истина;
		UsbDebug.traceConnects := истина;
		UsbDebug.traceInfo := истина;
		UsbDebug.traceSensing := истина;
		UsbDebug.traceScRequests := истина;
		UsbDebug.traceScTransfers := истина;
		UsbDebug.traceCSWs := истина;
		UsbDebug.traceCBWs := истина;
		UsbDebug.traceScInit := истина;
		UsbDebug.traceCustom := истина;
	иначе
		context.out.пСтроку8("UsbInfo: UsbDebug.Trace is FALSE... cannot enable tracing."); context.out.пВК_ПС;
	всё;
кон TraceAll;

проц TraceNone*(context : Commands.Context);
нач
	UsbDebug.traceDm := ложь;
	UsbDebug.traceParsing := ложь;
	UsbDebug.traceDeviceStates := ложь;
	UsbDebug.traceControl := ложь;
	UsbDebug.traceControlData := ложь;
	UsbDebug.traceTransfers := ложь;
	UsbDebug.traceFailed := ложь;
	UsbDebug.traceShortPackets := ложь;
	UsbDebug.tracePipes := ложь;
	UsbDebug.traceCopying := ложь;
	UsbDebug.traceIoc := ложь;
	UsbDebug.traceInit := ложь;
	UsbDebug.traceInterrupts := ложь;
	UsbDebug.traceQueuing := ложь;
	UsbDebug.traceHubRequests := ложь;
	UsbDebug.traceSuspend := ложь;
	UsbDebug.traceConnects := ложь;
	UsbDebug.traceInfo := ложь;
	UsbDebug.traceSensing := ложь;
	UsbDebug.traceScRequests := ложь;
	UsbDebug.traceScTransfers := ложь;
	UsbDebug.traceCSWs := ложь;
	UsbDebug.traceCBWs := ложь;
	UsbDebug.traceScInit := ложь;
	UsbDebug.traceCustom := ложь;
	context.out.пСтроку8("UsbInfo: All trace options disabled."); context.out.пВК_ПС;
кон TraceNone;

проц TraceShow*(context : Commands.Context);

	проц ShowOn(on : булево);
	нач
		если on то context.out.пСтроку8("On"); иначе context.out.пСтроку8("Off"); всё;
	кон ShowOn;

нач
	context.out.пСтроку8("UsbInfo: Trace options state: "); context.out.пВК_ПС;
	context.out.пСтроку8("UsbDebug.Trace: "); ShowOn(UsbDebug.Trace); context.out.пВК_ПС;
	context.out.пСтроку8("traceDm: "); ShowOn(UsbDebug.traceDm); context.out.пВК_ПС;
	context.out.пСтроку8("traceParsing: "); ShowOn(UsbDebug.traceParsing); context.out.пВК_ПС;
	context.out.пСтроку8("traceDeviceStates: "); ShowOn(UsbDebug.traceDeviceStates); context.out.пВК_ПС;
	context.out.пСтроку8("traceControl: "); ShowOn(UsbDebug.traceControl); context.out.пВК_ПС;
	context.out.пСтроку8("traceControlData: "); ShowOn(UsbDebug.traceControlData); context.out.пВК_ПС;
	context.out.пСтроку8("traceTransfers: "); ShowOn(UsbDebug.traceTransfers); context.out.пВК_ПС;
	context.out.пСтроку8("traceFailed: "); ShowOn(UsbDebug.traceFailed); context.out.пВК_ПС;
	context.out.пСтроку8("traceShortPackets: "); ShowOn(UsbDebug.traceShortPackets); context.out.пВК_ПС;
	context.out.пСтроку8("tracePipes: "); ShowOn(UsbDebug.tracePipes); context.out.пВК_ПС;
	context.out.пСтроку8("traceCopying: "); ShowOn(UsbDebug.traceCopying); context.out.пВК_ПС;
	context.out.пСтроку8("traceIoc: "); ShowOn(UsbDebug.traceIoc); context.out.пВК_ПС;
	context.out.пСтроку8("traceInit: "); ShowOn(UsbDebug.traceInit); context.out.пВК_ПС;
	context.out.пСтроку8("traceInterrupts: "); ShowOn(UsbDebug.traceInterrupts); context.out.пВК_ПС;
	context.out.пСтроку8("traceQueuing: "); ShowOn(UsbDebug.traceQueuing); context.out.пВК_ПС;
	context.out.пСтроку8("traceHubRequests: "); ShowOn(UsbDebug.traceHubRequests); context.out.пВК_ПС;
	context.out.пСтроку8("traceSuspend: "); ShowOn(UsbDebug.traceSuspend); context.out.пВК_ПС;
	context.out.пСтроку8("traceConnects: "); ShowOn(UsbDebug.traceConnects); context.out.пВК_ПС;
	context.out.пСтроку8("traceInfo: "); ShowOn(UsbDebug.traceInfo); context.out.пВК_ПС;
	context.out.пСтроку8("traceSensing: "); ShowOn(UsbDebug.traceSensing); context.out.пВК_ПС;
	context.out.пСтроку8("traceScRequests: "); ShowOn(UsbDebug.traceScRequests); context.out.пВК_ПС;
	context.out.пСтроку8("traceScTransfers: "); ShowOn(UsbDebug.traceScTransfers); context.out.пВК_ПС;
	context.out.пСтроку8("traceCSWs: "); ShowOn(UsbDebug.traceCSWs); context.out.пВК_ПС;
	context.out.пСтроку8("traceCBWs: "); ShowOn(UsbDebug.traceCBWs); context.out.пВК_ПС;
	context.out.пСтроку8("traceScInit: "); ShowOn(UsbDebug.traceScInit); context.out.пВК_ПС;
	context.out.пСтроку8("traceCustom: "); ShowOn(UsbDebug.traceCustom); context.out.пВК_ПС;
кон TraceShow;

проц TraceOn*(context : Commands.Context);
нач
	если UsbDebug.Trace то
		TraceOnOff(context, истина);
	иначе
		context.out.пСтроку8("UsbInfo: UsbDebug.Trace is FALSE... cannot use tracing."); context.out.пВК_ПС;
	всё;
кон TraceOn;

проц TraceOff*(context : Commands.Context);
нач
	TraceOnOff(context, ложь);
кон TraceOff;

проц TraceOnOff(context : Commands.Context; on : булево);
перем pstr : массив 32 из симв8; invalid : булево;
нач
	нцПока 	context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(pstr) делай
		 invalid := ложь;
		если Strings.Match("Dm", pstr) то UsbDebug.traceDm := on;
		аесли Strings.Match("Parsing", pstr) то UsbDebug.traceParsing := on;
		аесли Strings.Match("DeviceStates", pstr) то UsbDebug.traceDeviceStates := on;
		аесли Strings.Match("Control", pstr) то UsbDebug.traceControl := on;
		аесли Strings.Match("ControlData", pstr) то UsbDebug.traceControlData := on;
		аесли Strings.Match("Transfers", pstr) то UsbDebug.traceTransfers := on;
		аесли Strings.Match("Failed", pstr) то UsbDebug.traceFailed := on;
		аесли Strings.Match("ShortPackets", pstr) то UsbDebug.traceShortPackets := on;
		аесли Strings.Match("Pipes", pstr) то UsbDebug.tracePipes := on;
		аесли Strings.Match("Copying", pstr) то UsbDebug.traceCopying := on;
		аесли Strings.Match("Ioc", pstr) то UsbDebug.traceIoc := on;
		аесли Strings.Match("Init", pstr) то UsbDebug.traceInit := on;
		аесли Strings.Match("Interrupts", pstr) то UsbDebug.traceInterrupts := on;
		аесли Strings.Match("Queuing", pstr) то UsbDebug.traceQueuing := on;
		аесли Strings.Match("HubRequests", pstr) то UsbDebug.traceHubRequests := on;
		аесли Strings.Match("Suspend", pstr) то UsbDebug.traceSuspend := on;
		аесли Strings.Match("Connects", pstr) то UsbDebug.traceConnects := on;
		аесли Strings.Match("Info", pstr) то UsbDebug.traceInfo := on;
		аесли Strings.Match("Sensing", pstr) то UsbDebug.traceSensing := on;
		аесли Strings.Match("ScRequests", pstr) то UsbDebug.traceScRequests := on;
		аесли Strings.Match("ScTransfers", pstr) то UsbDebug.traceScTransfers := on;
		аесли Strings.Match("CSWs", pstr) то UsbDebug.traceCSWs := on;
		аесли Strings.Match("CBWs", pstr) то UsbDebug.traceCBWs := on;
		аесли Strings.Match("ScInit", pstr) то UsbDebug.traceScInit := on;
		аесли Strings.Match("Custom", pstr) то UsbDebug.traceCustom := on;
		иначе
			context.error.пСтроку8("Trace option '"); context.error.пСтроку8(pstr); context.error.пСтроку8("' not known."); context.error.пВК_ПС;
			invalid := истина;
		всё;
		если ~invalid то
			context.out.пСтроку8("Trace option '"); context.out.пСтроку8(pstr); context.out.пСтроку8("' turned ");
			если on то context.out.пСтроку8("on."); иначе context.out.пСтроку8("off."); всё;
		всё;
		context.out.пВК_ПС;
	кц;
кон TraceOnOff;

кон UsbInfo.

UsbInfo.Open ~
UsbInfo.Show ~
UsbInfo.Show details ~
UsbInfo.ShowDrivers ~
UsbInfo.ShowHc ~
UsbInfo.ShowHc details ~

System.Free UsbInfo ~
