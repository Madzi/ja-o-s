(* Runtime support for activities *)
(* Copyright (C) Florian Negele *)

(** The module provides the runtime support for activities associated with active objects. *)
(** It implements a basic task scheduler that distributes the work of concurrent activities to logical processors. *)
(** In addition, it also provides a framework for implementing synchronisation primitives. *)
модуль Activities;

использует НИЗКОУР, BaseTypes, Counters, CPU, Processors, Queues, Timer;

(** Represents one of four different priorities of an activity. *)
тип Priority* = размерМЗ;

конст SafeStackSize = 512 * размерОт адресВПамяти;
конст InitialStackSize = CPU.StackSize;
конст MaximumStackSize* = 1024 * InitialStackSize;

конст (** Indicates the lowest priority used for idle processors. *) IdlePriority* = 0;
конст (** Indicates the default priority of new activities. *) DefaultPriority* = 1;
конст (** Indicates a higher priority than the default. *) HighPriority* = 2;
конст (** Indicates the highest of all priorities. *) RealtimePriority* = 3;

конст LowestPriority = IdlePriority; HighestPriority = RealtimePriority;
конст Priorities = HighestPriority - LowestPriority + 1;

(** Represents a procedure that is called after the execution of an activity has been suspended by the {{{[[Activities.SwitchTo]]}}} procedure. *)
тип SwitchFinalizer* = проц (previous {неОтслСборщиком}: Activity; value: адресВПамяти);

(* Represents the stack of an activity. *)
тип Stack = укль {удалятьИзПамятиЯвно} на массив из симв8;
тип StackRecord = укль {опасныйДоступКПамяти} на запись
	previous {неОтслСборщиком}, next {неОтслСборщиком}: Stack;
кон;

(** Represents the handler identifying activities that are currently either running or suspended. *)
тип Activity* = окласс {удалятьИзПамятиЯвно} (Queues.Item)

	перем processor {неОтслСборщиком}: укль {опасныйДоступКПамяти} на Processor;
	перем firstStack {неОтслСборщиком}: Stack;
	перем stackLimit: адресВПамяти;
	перем quantum := CPU.Quantum: длинноеЦелМЗ;
	перем priority: Priority;
	перем finalizer := НУЛЬ: SwitchFinalizer;
	перем previous: Activity; argument: адресВПамяти;
	перем framePointer: адресВПамяти;
	перем procedure: проц;
	перем object-: BaseTypes.Object;
	перем bound := ложь: булево;
	перем startTime-: Timer.Counter;
	перем time- := 0: цел64;
	перем stack {неОтслСборщиком}: Stack;
	перем context*: окласс;

	проц &InitializeActivity (procedure: проц; priority: Priority);
	перем stackRecord {неОтслСборщиком}: StackRecord; stackFrame {неОтслСборщиком}: BaseTypes.StackFrame;
	перем StackFrameDescriptor {неОтслСборщиком} отКомпоновщика "BaseTypes.StackFrame": BaseTypes.Descriptor;
	нач {безКооперации, безОбычныхДинПроверок}
		утв (priority < Priorities);
		утв (InitialStackSize > SafeStackSize);
		нов (stack, InitialStackSize);
		утв (stack # НУЛЬ);
		firstStack := stack;
		stackRecord := операцияАдресОт stack[0];
		stackRecord.next := НУЛЬ;
		stackRecord.previous := НУЛЬ;
		stackLimit := операцияАдресОт stack[SafeStackSize+3* размерОт адресВПамяти]; сам.priority := priority;
		framePointer := операцияАдресОт stack[InitialStackSize - 4 * размерОт адресВПамяти] - CPU.StackDisplacement;
		stackFrame := framePointer + CPU.StackDisplacement;
		stackFrame.caller := Start;
		stackFrame.previous := НУЛЬ;
		stackFrame.descriptor := операцияАдресОт StackFrameDescriptor;
		сам.procedure := procedure;
	кон InitializeActivity;

	проц ~{перекрыта}Finalize;
	перем address: адресВПамяти; stackFrame {неОтслСборщиком}: BaseTypes.StackFrame; currentActivity {неОтслСборщиком}: Activity; stack{неОтслСборщиком}, next{неОтслСборщиком}: Stack; stackRecord{неОтслСборщиком}: StackRecord;
	нач {безКооперации, безОбычныхДинПроверок}
		address := framePointer;
		currentActivity := НИЗКОУР.GetActivity ()(Activity); НИЗКОУР.SetActivity (сам);
		нцПока address # НУЛЬ делай
			stackFrame := address + CPU.StackDisplacement;
			если нечётноеЛи¿ (stackFrame.descriptor) то
				умень (stackFrame.descriptor);
				stackFrame.Reset;
				address := stackFrame.previous;
			иначе
				address := stackFrame.descriptor;
			всё;
		кц;
		НИЗКОУР.SetActivity (currentActivity);
		stack := firstStack;
		нцДо
			stackRecord := операцияАдресОт stack[0];
			next := stackRecord.next;
			удалиИзПамяти (stack);
			stack := next;
		кцПри stack = НУЛЬ;
		Finalize^;
	кон Finalize;

кон Activity;

(* Represents a handler for an activity that is associated with an active object. *)
тип Process = окласс {удалятьИзПамятиЯвно} (Activity)

	проц &InitializeProcess (procedure: проц; priority: Priority; object: BaseTypes.Object);
	нач {безКооперации, безОбычныхДинПроверок}
		InitializeActivity (procedure, priority);
		утв (object # НУЛЬ);
		утв (object.action # НУЛЬ);
		сам.object := object;
		object.action.activity := сам;
	кон InitializeProcess;

	проц Unlink;
	нач {безКооперации, безОбычныхДинПроверок} object := НУЛЬ;
	кон Unlink;

	проц ~{перекрыта}Finalize;
	перем currentActivity {неОтслСборщиком}: Activity; item: Queues.Item;
	нач {безКооперации, безОбычныхДинПроверок}
		если object # НУЛЬ то
			currentActivity := НИЗКОУР.GetActivity ()(Activity); НИЗКОУР.SetActivity (сам);
			object.action.activity := НУЛЬ;
			если Queues.Dequeue (item, object.action.waitingQueue) то Resume (item(Activity)) всё;
			Unlink;
			НИЗКОУР.SetActivity (currentActivity);
		всё;
		Finalize^;
	кон Finalize;

кон Process;

(* Stores information per processor. *)
тип Processor = запись {выровнять (CPU.CacheLineSize)}
	assigning := ложь: булево;
	originalFramePointer: адресВПамяти;
	readyQueue: массив Priorities из Queues.AlignedQueue;
	runningActivity {неОтслСборщиком}: Activity;
	index: размерМЗ;
кон;

перем processors: массив Processors.Maximum из Processor;
перем readyQueue: массив Priorities из Queues.AlignedQueue;
перем working, physicalProcessors, virtualProcessors: Counters.AlignedCounter;

(** Stores an atomic counter indicating the number of activities that are awaiting interrupts to occur. *)
(** The scheduler stops its execution if all processors are idle, unless there are activities waiting for interrupts. *)
перем awaiting*: Counters.AlignedCounter;

проц StoreActivity отКомпоновщика "Environment.StoreActivity";

проц GetProcessTime-(): цел64;
перем activity: Activity; diff: Timer.Counter;
нач{безКооперации, безОбычныхДинПроверок}
	activity := НИЗКОУР.GetActivity ()(Activity);
	diff := Timer.GetCounter()-activity.startTime;
	возврат activity.time + diff;
кон GetProcessTime;

(** Returns the handler of the current activity executing this procedure call. *)
проц GetCurrentActivity- (): {неОтслСборщиком} Activity;
нач {безКооперации, безОбычныхДинПроверок} возврат НИЗКОУР.GetActivity ()(Activity);
кон GetCurrentActivity;

(** Returns the unique index of the processor executing this procedure call. *)
проц GetCurrentProcessorIndex- (): размерМЗ;
нач {безКооперации, безОбычныхДинПроверок} если НИЗКОУР.GetActivity () # НУЛЬ то возврат НИЗКОУР.GetActivity ()(Activity).processor.index иначе возврат 0 всё;
кон GetCurrentProcessorIndex;

(** Sets the priority of the current activity calling this procedure and returns the previous value. *)
проц SetCurrentPriority- (priority: Priority): Priority;
перем currentActivity {неОтслСборщиком}: Activity; previousPriority: Priority;
нач {безКооперации, безОбычныхДинПроверок}
	утв (priority < Priorities);
	currentActivity := НИЗКОУР.GetActivity ()(Activity);
	previousPriority := currentActivity.priority;
	currentActivity.priority := priority;
	возврат previousPriority;
кон SetCurrentPriority;

(** Binds the calling activity to the currently executing processor. *)
проц BindToCurrentProcessor-;
нач {безКооперации, безОбычныхДинПроверок}
	НИЗКОУР.GetActivity ()(Activity).bound := истина;
кон BindToCurrentProcessor;

(** Returns whether there is an activity that is ready to run and has at least the specified priority. *)
проц Select- (перем activity: Activity; minimum: Priority): булево;
перем processor {неОтслСборщиком}: укль {опасныйДоступКПамяти} на Processor;
перем priority := HighestPriority + 1: Priority; item: Queues.Item;
нач {безКооперации, безОбычныхДинПроверок}
	processor := НИЗКОУР.GetActivity ()(Activity).processor;
	нцДо
		умень (priority);
		если Queues.Dequeue (item, processor.readyQueue[priority]) то
			activity := item(Activity); возврат истина;
		аесли Queues.Dequeue (item, readyQueue[priority]) то
			activity := item(Activity);
			если activity.bound и (activity.processor # processor) то
				Enqueue (activity, операцияАдресОт activity.processor.readyQueue[activity.priority]);
			иначе
				возврат истина;
			всё;
		всё;
	кцПри priority = minimum;
	возврат ложь;
кон Select;

(** Performs a cooperative task switch by suspending the execution of the current activity and resuming the execution of any other activity that is ready to continue. *)
(** This procedure is called by the compiler whenever it detects that the time quantum of the current activity has expired. *)
проц Switch-;
перем currentActivity {неОтслСборщиком}, nextActivity: Activity;
нач {безКооперации, безОбычныхДинПроверок}
	currentActivity := НИЗКОУР.GetActivity ()(Activity);
	если Select (nextActivity, currentActivity.priority) то
		SwitchTo (nextActivity, Enqueue, операцияАдресОт readyQueue[currentActivity.priority]);
		FinalizeSwitch;
	иначе
		currentActivity.quantum := CPU.Quantum;
	всё;
кон Switch;

(* Switch finalizer that enqueues the previous activity to the specified ready queue. *)
проц Enqueue (previous {неОтслСборщиком}: Activity; queue {неОтслСборщиком}: укль {опасныйДоступКПамяти} на Queues.Queue);
нач {безКооперации, безОбычныхДинПроверок}
	Queues.Enqueue (previous, queue^);
	если операцияАдресОт queue^ = операцияАдресОт readyQueue[IdlePriority] то возврат всё;
	если Counters.Read (working) < Processors.count то Processors.ResumeAllProcessors всё;
кон Enqueue;

(** Resumes the execution of an activity that was suspended by a call to the {{{[[Activities.SwitchTo]]}}} procedure beforehand. *)
проц Resume- (activity: Activity);
нач {безКооперации, безОбычныхДинПроверок}
	утв (activity # НУЛЬ);
	Enqueue (activity, операцияАдресОт readyQueue[activity.priority])
кон Resume;

(** Performs a synchronous task switch. *)
(** The resumed activity continues its execution by first calling the specified finalizer procedure with the given argument. *)
(** Each invocation of this procedure must be directly followed by a call to the {{{[[Activities.FinalizeSwitch]]}}} procedure. *)
проц SwitchTo- (перем activity: Activity; finalizer: SwitchFinalizer; argument: адресВПамяти);
перем currentActivity {неОтслСборщиком}, nextActivity {неОтслСборщиком}: Activity; diff: Timer.Counter;
нач {безКооперации, безОбычныхДинПроверок}
	если activity.bound и (activity.processor # НИЗКОУР.GetActivity ()(Activity).processor) то
		нцДо кцПри Select (nextActivity, IdlePriority);
		Resume (activity); activity := nextActivity;
	всё;
	currentActivity := НИЗКОУР.GetActivity ()(Activity);
	currentActivity.framePointer := НИЗКОУР.GetFramePointer ();
	currentActivity.quantum := CPU.Quantum;
	diff := Timer.GetCounter() - currentActivity.startTime;
	currentActivity.time := currentActivity.time + diff;
	nextActivity := activity;
	nextActivity.processor := currentActivity.processor;
	nextActivity.finalizer := finalizer;
	nextActivity.argument := argument;
	nextActivity.previous := currentActivity;
	nextActivity.processor.runningActivity := nextActivity;
	nextActivity.startTime := Timer.GetCounter();
	activity := НУЛЬ;
	НИЗКОУР.SetActivity (nextActivity); StoreActivity;
	НИЗКОУР.SetFramePointer (nextActivity.framePointer);
кон SwitchTo;

(** Finalizes a task switch performed by calling the switch finalizer of the previously suspended activity. *)
(** This procedure must be called after each invocation of the {{{[[Activities.SwitchTo]]}}} procedure. *)
проц FinalizeSwitch-;
перем currentActivity {неОтслСборщиком}: Activity;
нач {безКооперации, безОбычныхДинПроверок}
	currentActivity := НИЗКОУР.GetActivity ()(Activity);
	если currentActivity.finalizer # НУЛЬ то currentActivity.finalizer (currentActivity.previous, currentActivity.argument) всё;
	currentActivity.finalizer := НУЛЬ; currentActivity.previous := НУЛЬ;
кон FinalizeSwitch;

(* Entry point for new activities. *)
проц Start;
перем currentActivity {неОтслСборщиком}: Activity;
перем procedure {неОтслСборщиком}: укль {опасныйДоступКПамяти} на запись body: проц (object {неОтслСборщиком}: окласс) кон;
нач {безКооперации, безОбычныхДинПроверок}
	FinalizeSwitch;
	currentActivity := НИЗКОУР.GetActivity ()(Activity);
	procedure := операцияАдресОт currentActivity.procedure;
	procedure.body (currentActivity.object);
	TerminateCurrentActivity;
кон Start;

(** This procedure is called by the compiler for each {{{NEW}}} statement that creates an active object. *)
(** It associates an active object with a new activity that begins its execution with the specified body procedure. *)
проц Create- (body: проц; priority: Priority; object {неОтслСборщиком}: BaseTypes.Object);
перем activity: Process;
нач {безКооперации, безОбычныхДинПроверок}
	если priority = IdlePriority то priority := НИЗКОУР.GetActivity ()(Activity).priority всё;
	нов (activity, body, priority, object);
	утв (activity # НУЛЬ);
	activity.context := GetCurrentActivity ().context;
	Resume (activity);
кон Create;

(** Creates an activity that pretends to be executed on a distinct processor. *)
проц CreateVirtualProcessor- (): {неОтслСборщиком} Activity;
перем activity {неОтслСборщиком}: Activity; index: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	нов (activity, НУЛЬ, DefaultPriority);
	утв (activity # НУЛЬ);
	index := Counters.Increment (virtualProcessors, 1) + Processors.count;
	утв (index < Processors.Maximum);
	activity.processor := операцияАдресОт processors[index];
	activity.processor.index := index;
	activity.bound := истина;
	возврат activity;
кон CreateVirtualProcessor;

(** Temporarily exchanges the currently running activity with a virtual processor in order to call the specified procedure in a different context. *)
проц CallVirtual- (procedure: проц (value: адресВПамяти); value: адресВПамяти; processor {неОтслСборщиком}: Activity);
перем currentActivity {неОтслСборщиком}: Activity; stackPointer: адресВПамяти;
нач {безКооперации, безОбычныхДинПроверок}
	утв (processor # НУЛЬ);
	currentActivity := НИЗКОУР.GetActivity ()(Activity); stackPointer := НИЗКОУР.GetStackPointer (); НИЗКОУР.SetActivity (processor); StoreActivity;
	НИЗКОУР.SetStackPointer (операцияАдресОт processor.stack[длинаМассива (processor.stack) - CPU.StackDisplacement]);
	procedure (value); НИЗКОУР.SetActivity (currentActivity); StoreActivity; НИЗКОУР.SetStackPointer (stackPointer);
кон CallVirtual;

(** Creates a new activity that calls the specified procedure. *)
проц Call- (procedure: проц);
перем activity: Activity;
нач {безКооперации, безОбычныхДинПроверок}
	нов (activity, procedure, DefaultPriority);
	утв (activity # НУЛЬ);
	Resume (activity);
кон Call;

(** Starts the scheduler on the current processor by creating a new activity that calls the specified procedure. *)
(** This procedure is called by the runtime system once during the initialization of each processor. *)
проц Execute- (procedure: проц);
перем previousActivity {неОтслСборщиком}: Activity;
нач {безКооперации, безОбычныхДинПроверок}
	НИЗКОУР.SetActivity (НУЛЬ);
	BeginExecution (procedure);
	previousActivity := НИЗКОУР.GetActivity ()(Activity);
	previousActivity.processor.runningActivity := НУЛЬ;
	НИЗКОУР.SetActivity (НУЛЬ);
	Dispose (previousActivity, НУЛЬ);
кон Execute;

(* Turns the calling procedure temporarily into an activity that begins its execution with the specified procedure. *)
проц BeginExecution (procedure: проц);
перем activity {неОтслСборщиком}: Activity; index: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	нов (activity, procedure, DefaultPriority);
	утв (activity # НУЛЬ);
	index := Counters.Increment (physicalProcessors, 1);
	утв (index < Processors.count);
	activity.processor := операцияАдресОт processors[index];
	activity.processor.originalFramePointer := НИЗКОУР.GetFramePointer ();
	activity.processor.runningActivity := activity; activity.processor.index := index;
	утв (Counters.Increment (working, 1) < Processors.count);
	если (index = 0) и (Processors.count > 1) то Processors.StartAll всё;
	НИЗКОУР.SetActivity (activity); НИЗКОУР.SetFramePointer (activity.framePointer);
кон BeginExecution;

(* Yields the execution of the current activity to any activity with the given minimal priority. *)
проц YieldExecution (minimum: Priority; finalizer: SwitchFinalizer; value: адресВПамяти);
перем nextActivity: Activity;
нач {безКооперации, безОбычныхДинПроверок}
	нц
		если Select (nextActivity, minimum) то
			SwitchTo (nextActivity, finalizer, value);
			FinalizeSwitch;
		иначе
			если Counters.Decrement (working, 1) + Counters.Read (awaiting) > 1 то Processors.SuspendCurrentProcessor всё;
			если Counters.Increment (working, 1) + Counters.Read (awaiting) = 0 то прервиЦикл всё;
		всё;
	кц;
кон YieldExecution;

(* This procedure returns to the procedure that called BeginExecution. *)
проц EndExecution;
перем currentActivity {неОтслСборщиком}: Activity;
нач {безКооперации, безОбычныхДинПроверок}
	currentActivity := НИЗКОУР.GetActivity ()(Activity);
	currentActivity.framePointer := НИЗКОУР.GetFramePointer ();
	если Counters.Decrement (working, 1) < Processors.count то Processors.ResumeAllProcessors всё;
	НИЗКОУР.SetFramePointer (currentActivity.processor.originalFramePointer);
кон EndExecution;

(** This is the default procedure for initially idle processors starting the scheduler using the {{{[[Activities.Execute]]}}} procedure. *)
проц Idle-;
нач {безКооперации, безОбычныхДинПроверок}
	утв (SetCurrentPriority (IdlePriority) = DefaultPriority);
	YieldExecution (IdlePriority + 1, Enqueue, операцияАдресОт readyQueue[IdlePriority]); EndExecution;
кон Idle;

(** Terminates the execution of the current activity calling this procedure. *)
(** This procedure is also invoked at the end of the body of an active object. *)
проц {NORETURN} TerminateCurrentActivity-;
нач {безКооперации, безОбычныхДинПроверок}
	YieldExecution (IdlePriority, Dispose, НУЛЬ);
	EndExecution; СТОП (1234);
кон TerminateCurrentActivity;

(* Switch finalizer that disposes the resources of the terminated activity. *)
проц Dispose (previous {неОтслСборщиком}: Activity; value: адресВПамяти);
нач {безКооперации, безОбычныхДинПроверок}
	удалиИзПамяти (previous);
кон Dispose;

(** This procedure is called by the compiler while executing a {{{WAIT}}} statement. *)
(** It awaits the termination of all activities associated with an active object. *)
проц Wait- (object {неОтслСборщиком}: BaseTypes.Object);
перем nextActivity: Activity; item: Queues.Item;
нач {безКооперации, безОбычныхДинПроверок}
	утв (object # НУЛЬ);
	если object.action = НУЛЬ то возврат всё;
	если object.action.activity = НУЛЬ то возврат всё;
	нцДо кцПри Select (nextActivity, IdlePriority);
	SwitchTo (nextActivity, EnqueueWaiting, object.action); FinalizeSwitch;
	нцПока Queues.Dequeue (item, object.action.waitingQueue) делай Resume (item (Activity)) кц;
кон Wait;

(* Switch finalizer that enqueues acitivities waiting on an active object. *)
проц EnqueueWaiting (previous {неОтслСборщиком}: Activity; action {неОтслСборщиком}: укль {опасныйДоступКПамяти} на BaseTypes.Action);
перем item: Queues.Item;
нач {безКооперации, безОбычныхДинПроверок}
	Queues.Enqueue (previous, action.waitingQueue);
	если action.activity # НУЛЬ то возврат всё;
	если Queues.Dequeue (item, action.waitingQueue) то Resume (item (Activity)) всё;
кон EnqueueWaiting;

проц  ReturnToStackSegment*;
перем
	stackFrame {неОтслСборщиком}: BaseTypes.StackFrame;
	currentActivity {неОтслСборщиком}: Activity;
	newStack {неОтслСборщиком}: Stack;
	stackRecord {неОтслСборщиком}: StackRecord;
нач{безКооперации, безОбычныхДинПроверок}
	(* old stack pointer and base pointer have been pushed again, we have to revert this *)
	stackFrame := НИЗКОУР.GetFramePointer();
	(*
	TRACE(stackFrame.caller);
	TRACE(stackFrame.previous);
	previousFrame := stackFrame.previous;
	TRACE(ADDRESS OF previousFrame.caller + SIZE OF ADDRESS);
	TRACE(previousFrame.caller);
	TRACE(previousFrame.previous);
	*)
	currentActivity := НИЗКОУР.GetActivity ()(Activity);
	stackRecord := операцияАдресОт currentActivity.stack[0];
	newStack := stackRecord.previous;
	currentActivity.stack := newStack;
	currentActivity.stackLimit := операцияАдресОт newStack[SafeStackSize + 3 * размерОт адресВПамяти];
кон ReturnToStackSegment;

проц {NOPAF} ReturnToStackSegment0;
нач{безКооперации, безОбычныхДинПроверок}
	CPU.SaveResult;
	ReturnToStackSegment;
	CPU.RestoreResultAndReturn;
кон ReturnToStackSegment0;

(** Expands the stack memory of the current activity to include the specified stack address and returns the new stack pointer to be set after the call. *)
проц ExpandStack- (address: адресВПамяти; parSize: размерМЗ): адресВПамяти;
перем
	currentActivity {неОтслСборщиком}: Activity;
	varSize, minSize, newSize: размерМЗ;  sp: адресВПамяти;
	newStack {неОтслСборщиком}: укль {удалятьИзПамятиЯвно} на массив из симв8;
	stackFrame {неОтслСборщиком}, previousFrame {неОтслСборщиком}, newFrame {неОтслСборщиком}: BaseTypes.StackFrame;
	stackRecord{неОтслСборщиком}, newStackRecord{неОтслСборщиком}: StackRecord;
нач {безКооперации, безОбычныхДинПроверок}
	(* check for valid argument *)
	currentActivity := НИЗКОУР.GetActivity ()(Activity);

	stackFrame := НИЗКОУР.GetFramePointer ();
	previousFrame := stackFrame.previous;
	varSize := stackFrame.previous - address;

	(*
	TRACE(SYSTEM.GetFramePointer(), address, varSize, parSize, size, stackFrame.caller);
	*)
	утв(varSize >= 0);
	утв(parSize >= 0);

	newSize := длинаМассива (currentActivity.stack); (* current stack size *)
	minSize := SafeStackSize + parSize + varSize + 3 * размер16_от(адресВПамяти) (* stack frame *) + 3 * размер16_от(адресВПамяти) (* previous, next *);
	нцДо увел (newSize, newSize) кцПри newSize >= minSize;
	утв (newSize <= MaximumStackSize);
	stackRecord := операцияАдресОт currentActivity.stack[0];
	newStack := stackRecord.next;
	если (newStack = НУЛЬ) или (длинаМассива(newStack) < newSize) то
		нов (newStack, newSize);
		утв (newStack # НУЛЬ);
		newStackRecord := операцияАдресОт newStack[0];
		newStackRecord.previous := currentActivity.stack;
		newStackRecord.next := НУЛЬ;
		stackRecord.next := newStack;
	иначе
		newStackRecord := операцияАдресОт newStack[0];
		утв(newStackRecord.previous = currentActivity.stack);
		утв(stackRecord.next = newStack);
	всё;
	newSize := длинаМассива(newStack);
	newFrame := операцияАдресОт newStack[0] + newSize- parSize -  3*размерОт адресВПамяти;
	newFrame.previous := stackFrame.previous;
	newFrame.descriptor := previousFrame.descriptor;
	newFrame.caller := ReturnToStackSegment0;
	previousFrame.descriptor := stackFrame.descriptor; (* trick to get a base stack frame descriptor *)

	stackFrame.previous := newFrame;
	НИЗКОУР.копируйПамять(операцияАдресОт previousFrame.caller + размерОт адресВПамяти, операцияАдресОт newFrame.caller + размерОт адресВПамяти, parSize); (* copy parameters *)

	sp := адресОт(newFrame.descriptor) - varSize;
	удалиИзПамяти (currentActivity.stack); currentActivity.stack := newStack;
	currentActivity.stackLimit := операцияАдресОт newStack[SafeStackSize + 3 * размерОт адресВПамяти];

	возврат sp;
кон ExpandStack;

(** Returns whether the specified address corresponds to a local variable that resides on the stack of the current activity calling this procedure. *)
проц IsLocalVariable- (address: адресВПамяти): булево;
перем currentActivity {неОтслСборщиком}: Activity; begin, end: адресВПамяти; stack {неОтслСборщиком}: Stack; stackRecord {неОтслСборщиком}: StackRecord;
нач {безКооперации, безОбычныхДинПроверок}
	currentActivity := НИЗКОУР.GetActivity ()(Activity);
	если currentActivity = НУЛЬ то возврат ложь всё;
	stack := currentActivity.firstStack;
	нцДо
		begin := операцияАдресОт stack[0];
		end := begin + длинаМассива (stack);
		если (address >= begin) и (address < end) то возврат истина всё;
		stackRecord := begin;
		stack := stackRecord.next;
	кцПри stack = НУЛЬ;
	возврат ложь;
кон IsLocalVariable;

(** Returns whether any activity is currently executing an assignment statement. *)
проц AssignmentsInProgress- (): булево;
перем i: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	нцДля i := 0 до Processors.Maximum - 1 делай если processors[i].assigning то возврат истина всё кц; возврат ложь;
кон AssignmentsInProgress;

(** Terminates the module and disposes all of its resources. *)
проц Terminate-;
перем priority: Priority;
нач {безКооперации, безОбычныхДинПроверок}
	нцДля priority := LowestPriority до HighestPriority делай
		Queues.Dispose (readyQueue[priority]);
	кц;
кон Terminate;

кон Activities.
