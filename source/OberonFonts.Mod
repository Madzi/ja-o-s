модуль OberonFonts; (** AUTHOR "GF"; PURPOSE "Create Oberon fonts out of TrueType fonts" *)

использует Strings, Files, S := НИЗКОУР, Log := ЛогЯдра, Modules, TextUtilities,
		OTInt := OpenTypeInt, OType := OpenType, OpenTypeScan;

конст
	ScreenDPI = 71;
	FontId = 0DBX;
	FontFont = 0X;

тип
	Name = массив 32 из симв8;

	RasterData* = запись (OType.RasterData)
		adr*: адресВПамяти;							(* base address of pattern *)
		bpr*: цел32;							(* number of bytes per row *)
		len*: цел32;							(* pattern length *)
	кон;


	(* fill rectangle in pattern *)
	проц FillRect( llx, lly, urx, ury, opacity: цел16; перем data: OType.RasterData0 );
		перем x0, x1, h, n: цел16;  adr, a: адресВПамяти;  mask: мнвоНаБитахМЗ;  byte: симв8;
	нач
		просейТип data: RasterData делай
			x0 := llx DIV 8;  x1 := urx DIV 8;
			adr := data.adr + data.bpr * lly + x0;
			h := ury - lly;
			если x0 = x1 то
				mask := мнвоНаБитахМЗ({(llx остОтДеленияНа 8) .. ((urx-1) остОтДеленияНа 8)})
			иначе
				mask := мнвоНаБитахМЗ({(llx остОтДеленияНа 8) .. 7})
			всё;
			n := h; a := adr;
			нцПока n > 0 делай
				утв( (data.adr <= a) и (a < data.adr + data.len), 110 );
				S.прочтиОбъектПоАдресу( a, byte );
				S.запишиОбъектПоАдресу( a, симв8ИзКода( цел32(мнвоНаБитахМЗ(кодСимв8( byte )) + mask)) );
				умень( n ); увел( a, data.bpr )
			кц;
			если x0 < x1 то
				увел( x0 ); увел( adr );
				нцПока x0 < x1 делай
					n := h;  a := adr;
					нцПока n > 0 делай
						утв( (data.adr <= a) и (a < data.adr + data.len), 111 );
						S.запишиОбъектПоАдресу( a, 0FFX );
						умень( n ); увел( a, data.bpr )
					кц;
					увел( x0 ); увел( adr )
				кц;
				если 8*x1 # urx то
					mask := мнвоНаБитахМЗ({0 .. (urx-1) остОтДеленияНа 8});
					n := h;  a := adr;
					нцПока n > 0 делай
						утв( (data.adr <= a) и (a < data.adr + data.len), 112 );
						S.прочтиОбъектПоАдресу( a, byte );
						S.запишиОбъектПоАдресу( a, симв8ИзКода( цел32(мнвоНаБитахМЗ(кодСимв8( byte )) + mask)));
						умень( n ); увел( a, data.bpr )
					кц
				всё
			всё
		всё
	кон FillRect;

	проц MakeFont( inst: OType.Instance;  glyph: OType.Glyph;  name: массив из симв8 ): Files.File;
		конст
			mode = {OType.Hinted, OType.Width, OType.Raster};
		перем
			file: Files.File;  font: OType.Font;  i, chars, ranges, xmin, ymin, xmax, ymax, j: цел16;
			beg, end: массив 64 из цел16;  data: RasterData;  no, bytes, k: цел32;
			ras: OpenTypeScan.Rasterizer;
			w: Files.Writer;  ipos, apos: Files.Position;
			pattern: массив 360*360 DIV 8 из симв8;				(* enough for 36 point at 720 dpi *)
	нач
		file := Files.New( name );
		утв(file # НУЛЬ);
		Files.OpenWriter( w, file, 0 );
		w.пСимв8( FontId );																	(* Id *)
		w.пСимв8( FontFont );																(* type (metric/font) *)
		w.пСимв8( 0X );																		(* family *)
		w.пСимв8( 0X );																		(* variant *)
		i := inst.font.hhea.ascender + inst.font.hhea.descender + inst.font.hhea.lineGap;
		w.пЦел16_мз( устарПреобразуйКБолееУзкомуЦел(OTInt.MulDiv( i, inst.yppm, 40H*устарПреобразуйКБолееШирокомуЦел(inst.font.head.unitsPerEm) )) );	(* height *)

		w.пЦел16_мз( 0 ); w.пЦел16_мз( 0 ); w.пЦел16_мз( 0 ); w.пЦел16_мз( 0 );	(* fix later *) (* min/max X/Y *)

		font := inst.font;
		i := 0;  chars := 0;  ranges := 0;
		если OType.UnicodeToGlyph( font, OType.CharToUnicode[1] ) = 0 то
			i := 2;  chars := 1;  beg[0] := 0;  end[0] := 1;  ranges := 1				(* make range for 0X *)
		всё;
		нцДо
			нцПока (i < 256) и (i # 9) и (OType.UnicodeToGlyph( font, OType.CharToUnicode[i] ) = 0) делай  увел( i )  кц;
			если i < 256 то
				beg[ranges] := i;  увел( i );  увел( chars );
				нцПока (i < 256) и (OType.UnicodeToGlyph( font, OType.CharToUnicode[i] ) # 0) делай  увел( i ); увел( chars )  кц;
				end[ranges] := i;  увел( ranges )
			всё
		кцПри i = 256;
		w.пЦел16_мз( ranges);																(* number of runs *)
		i := 0;  нцПока i < ranges делай
			w.пЦел16_мз( beg[i] ); w.пЦел16_мз( end[i] );										(* start/end of run *)
			увел( i )
		кц;

		ipos := w.МестоВПотоке( );										(* open rider for later writing metrics *)
		i := 0;  нцПока i < chars делай
			w.пЦел16_мз( 0 ); w.пЦел16_мз( 0 ); w.пЦел16_мз( 0 ); w.пЦел16_мз( 0 ); w.пЦел16_мз( 0 );
			увел( i )
		кц;

		xmin := матМаксимум(цел16); ymin := матМаксимум(цел16); xmax := матМинимум(цел16); ymax := матМинимум(цел16);
		i := 0;
		нцПока i < ranges делай
			j := beg[i];
			нцПока j < end[i] делай
				no := OType.UnicodeToGlyph( font, OType.CharToUnicode[j] );
				если (j = 9) и (no = 0) то
					no := OType.UnicodeToGlyph( font, OType.CharToUnicode[кодСимв8("I")] );
					OType.LoadGlyph( inst, glyph, ras, устарПреобразуйКБолееУзкомуЦел(no), {OType.Hinted, OType.Width} );
					glyph.awx := 8*glyph.awx;
					glyph.hbx := 0;  glyph.hby := 0;  glyph.rw := 0;  glyph.rh := 0
				иначе
					OType.LoadGlyph( inst, glyph, ras, устарПреобразуйКБолееУзкомуЦел(no), mode )
				всё;
				apos := w.МестоВПотоке( );  w.ПерейдиКМестуВПотоке( ipos );
				w.пЦел16_мз( glyph.awx );											(* advance *)
				w.пЦел16_мз( glyph.hbx );											(* horizontal bearing x *)
				w.пЦел16_мз( glyph.hby );											(* horizontal bearing y *)
				w.пЦел16_мз( glyph.rw );											(* image width *)
				w.пЦел16_мз( glyph.rh );												(* image height *)
				ipos := w.МестоВПотоке( );  w.ПерейдиКМестуВПотоке( apos );
				если glyph.rw * glyph.rh # 0 то
					если glyph.hbx < xmin то  xmin := glyph.hbx  всё;
					если glyph.hby < ymin то  ymin := glyph.hby  всё;
					если glyph.hbx + glyph.rw > xmax то  xmax := glyph.hbx + glyph.rw  всё;
					если glyph.hby + glyph.rh > ymax то  ymax := glyph.hby + glyph.rh  всё;
					data.rect := FillRect;  data.adr := адресОт(pattern);
					data.bpr := (glyph.rw+7) DIV 8;  data.len := длинаМассива(pattern);
					bytes := glyph.rh * data.bpr;
					утв( bytes < длинаМассива(pattern) );
					k := 0;  нцДо  pattern[k] := 0X;  увел( k )  кцПри k = bytes;
					OType.EnumRaster( ras, data );
					k := 0;  нцДо  w.пСимв8( pattern[k] ); увел( k )  кцПри k = bytes			(* pattern *)
				всё;
				увел( j )
			кц;
			увел( i )
		кц;

		w.ПерейдиКМестуВПотоке( 6 );
		w.пЦел16_мз( xmin); w.пЦел16_мз( xmax);										(* minX/maxX *)
		w.пЦел16_мз( ymin); w.пЦел16_мз( ymax);										(* minY/maxY *)
		w.ПротолкниБуферВПоток;
		возврат file
	кон MakeFont;



	проц IsDigit( c: симв8 ): булево;
	нач
		возврат (c >= '0') и (c <= '9')
	кон IsDigit;

	проц MakeTtfName( конст oname: массив из симв8; перем ttfname: Name; перем size, res: цел16 );
	перем i: цел32; c: симв8;
	нач
		если Strings.EndsWith( ".Scn.Fnt", oname ) то  res := ScreenDPI
		аесли Strings.EndsWith( ".Pr6.Fnt", oname ) то  res := 600
		аесли Strings.EndsWith( ".Pr3.Fnt", oname ) то  res := 300
		аесли Strings.EndsWith( ".Pr2.Fnt", oname ) то  res := 200
		иначе  size := 0;  res := 0;  ttfname := "";  возврат
		всё;
		i := 0;
		нцДо
			c := oname[i];  ttfname[i] := c;  увел( i )
		кцПри IsDigit( c );
		ttfname[i-1] := 0X;
		size := 0;
		нцПока IsDigit( c ) делай
			size := 10*size + кодСимв8( c ) - кодСимв8( '0' );  c := oname[i];  увел( i )
		кц;
		если c = 'b' то  Strings.Append( ttfname, "_bd" )
		аесли c = 'i' то  Strings.Append( ttfname, "_i" )
		всё;
		Strings.Append( ttfname, ".ttf" );
	кон MakeTtfName;


	проц ProvideFont*( конст oname: массив из симв8 );
	перем
		ttfname: Name;  size, res: цел16;  f: Files.File;
		font: OType.Font;  inst: OType.Instance;  glyph: OType.Glyph;
	нач
		f := Files.Old( oname );
		если f # НУЛЬ то
			(* font already exists *) f.Close
		иначе
			MakeTtfName( oname, ttfname, size, res );
			если size > 0 то
				font := OType.Open( ttfname );
				если font # НУЛЬ то
					нов( glyph );
					OType.InitGlyph( glyph, font );
					OType.GetInstance( font, 40H*size, res, res, OType.Identity, inst );
					f := MakeFont( inst, glyph, oname );
					Files.Register( f );
					Log.пСтроку8( "Created Oberon font " );  Log.пСтроку8( oname );  Log.пВК_ПС
				всё
			всё
		всё
	кон ProvideFont;


	проц Allocatable*( конст oname: массив из симв8 ): булево;
	перем f: Files.File;  ttfname: Name;  size, res: цел16;
	нач
		f := Files.Old( oname );
		если f # НУЛЬ то  возврат истина
		иначе
			MakeTtfName( oname, ttfname, size, res );
			f := Files.Old( ttfname );
			если f # НУЛЬ то  возврат истина  всё;
			возврат ложь
		всё
	кон Allocatable;

	проц Install*;
	кон Install;

	проц Finalize;
	нач
		TextUtilities.oberonFontAllocatable := НУЛЬ
	кон Finalize;


нач
	TextUtilities.oberonFontAllocatable := Allocatable;
	Modules.InstallTermHandler( Finalize )
кон OberonFonts.

