модуль BenchInterrupts; (** AUTHOR "staubesv"; PURPOSE "Interrupt latency benchmarks"; *)
(**

	Non-comrehensive list of aspects to be considered:
	-	The garbage collector prohibits interrupts while running. Depending on the current state of the heap, this can
		introduce delays in the order of seconds
	- 	There can be multipe handlers per interrupt vector
	-	The Machine.SoftInt is a temporary interrupt vector that is potentially used by other applications
	-	Result is dependent on other interrupts that have a higher priority and can interrupt our handler

*)

использует
	НИЗКОУР,
	Machine, Heaps, Потоки, Commands, MathL;

конст
	(*	Interrupt vector number for 1st level interrupt handler benchmark
		This is a temporary interrupt vector number that is potentially used for different purposes *)
	InterruptVectorNumber = Machine.SoftInt;

	MinNofSamples = 1000;
	MaxNofSamples = 1000000;

перем
	mhz : цел32;

	start, stop : цел64;
	ngc : размерМЗ;
	data : укль на массив из цел32;

проц InterruptHandler(перем state: Machine.State);
нач
	stop := Machine.GetTimer();
кон InterruptHandler;

(* Software interrupt call *)
проц -SoftwareInterrupt;
машКод
#если I386 то
	INT InterruptVectorNumber
#аесли AMD64 то
	INT InterruptVectorNumber
#иначе
	unimplemented
#кон
кон SoftwareInterrupt;

(** Start the 1st Level Interrupt Handler latency benchmark *)
проц Bench*(context : Commands.Context); (** [nofSamples] ~ *)
перем nofSamples, ignore : цел32; oldNgc, index : размерМЗ;
нач {единолично}
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(nofSamples, ложь);
	если (nofSamples < MinNofSamples) то nofSamples := MinNofSamples;
	аесли (nofSamples > MaxNofSamples) то nofSamples := MaxNofSamples;
	всё;
	context.out.пСтроку8("Starting 1st level interrupt handler latency benchmark (");
	context.out.пЦел64(nofSamples, 0); context.out.пСтроку8(" samples) ... ");
	context.out.ПротолкниБуферВПоток;
	нов(data, nofSamples);
	Machine.InstallHandler(InterruptHandler, InterruptVectorNumber);
	ignore := Machine.AcquirePreemption();
	oldNgc := Heaps.Ngc;
	нцДля index := 0 до длинаМассива(data)-1 делай
		start := Machine.GetTimer();
		SoftwareInterrupt;
		data[index] := устарПреобразуйКБолееУзкомуЦел(stop - start);
	кц;
	ngc := Heaps.Ngc - oldNgc;
	Machine.ReleasePreemption;
	Machine.RemoveHandler(InterruptHandler, InterruptVectorNumber);
	context.out.пСтроку8("done."); context.out.пВК_ПС;
кон Bench;

проц CyclesToMs(cycles : цел64; mhz : цел32) : вещ64;
нач
	возврат вещ64(cycles) / (1000*mhz);
кон CyclesToMs;

проц ShowMs(cycles : цел64; out : Потоки.Писарь);
нач
	если (mhz # 0) то
		out.пСтроку8(" ("); out.пВещ64_ФФТ(CyclesToMs(cycles, mhz), 0, 6, 0); out.пСтроку8(" ms)");
	всё;
кон ShowMs;

(** Show the results of the last benchmark run *)
проц Show*(context : Commands.Context); (** mhz ~ *)
перем
	nofSamples, min, avg, max : цел32; i : размерМЗ; sum : цел64;
	diff, diffSum, standardDeviation : вещ64;
нач {единолично}
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(mhz, ложь);
	если (data # НУЛЬ) то
		nofSamples := длинаМассива(data)(цел32);
		min := матМаксимум(цел32); max := матМинимум(цел32); sum := 0;
		(* calculate min, max and sum *)
		нцДля i := 0 до длинаМассива(data)-1 делай
			если (data[i] < min) то min := data[i];
			аесли (data[i] > max) то max := data[i];
			всё;
			sum := sum + data[i];
		кц;
		avg := устарПреобразуйКБолееУзкомуЦел(sum DIV nofSamples);
		(* calculate standard deviation *)
		diffSum := 0;
		нцДля i := 0 до длинаМассива(data)-1 делай
			diff := avg - data[i];
			diffSum := diffSum + (diff * diff);
		кц;
		standardDeviation := MathL.sqrt(diffSum / nofSamples);
		context.out.пСтроку8("NofSamples: "); context.out.пЦел64(nofSamples, 0); context.out.пВК_ПС;
		context.out.пСтроку8("Nof GC runs while benchmarking: "); context.out.пЦел64(ngc, 0); context.out.пВК_ПС;
		context.out.пСтроку8("CPU clock rate: ");
		если (mhz # 0) то context.out.пЦел64(mhz, 0); context.out.пСтроку8(" MHz"); иначе context.out.пСтроку8("Unknown"); всё;
		context.out.пВК_ПС;
		context.out.пСтроку8("Interrupt Latency in CPU cycles: "); context.out.пВК_ПС;
		context.out.пСтроку8("Min: "); context.out.пЦел64(min, 0); ShowMs(min, context.out); context.out.пВК_ПС;
		context.out.пСтроку8("Max: "); context.out.пЦел64(max, 0); ShowMs(max, context.out); context.out.пВК_ПС;
		context.out.пСтроку8("Avg: "); context.out.пЦел64(avg, 0); ShowMs(avg, context.out); context.out.пВК_ПС;
		context.out.пСтроку8("Standard Deviation: "); context.out.пВещ64_ФФТ(standardDeviation, 0, 0, 0); context.out.пВК_ПС;
	иначе
		context.out.пСтроку8("No data available."); context.out.пВК_ПС;
	всё;
кон Show;

кон BenchInterrupts.

System.Free BenchInterrupts ~

BenchInterrupts.Bench 1000000 ~

BenchInterrupts.Show 2000 ~
