(**
	AUTHOR: Alexey Morozov
	PURPOSE: Dynamic library tools for Unix platforms
*)
модуль HostLibs;

использует
	НИЗКОУР, Unix;

тип
	LibHandle* = адресВПамяти; (** dynamic library handle type *)

конст
	NilLibHandle* = НУЛЬ; (** invalid library handle *)

(*
	dlopen flags

	#define RTLD_LAZY     0x0001
	#define RTLD_NOW      0x0002
	#define RTLD_GLOBAL   0x0100
	#define RTLD_LOCAL    0x0000
	#define RTLD_NOSHARE  0x1000
	#define RTLD_EXE      0x2000
	#define RTLD_SCRIPT   0x4000
*)

	(**
		Load a dynamic library

		fileName: library file name
		lib: returned loaded library handle; NilLibHandle in case of an error

		Return: TRUE in case of success
	*)
	проц LoadLibrary*(конст fileName: массив из симв8; перем lib: LibHandle): булево;
	нач
		lib := Unix.Dlopen(fileName, 0x0001); (* RTLD_LAZY: use lazy binding - resolve symbols only at the user request *)
		возврат (lib # NilLibHandle);
	выходя
		возврат ложь;
	кон LoadLibrary;

	(**
		Free a previously loaded dynamic library

		lib: library handle

		Return: TRUE in case of success
	*)
	проц FreeLibrary*(конст lib: LibHandle): булево;
	нач
		если lib # НУЛЬ то
			Unix.Dlclose(lib);
			возврат истина;
		всё;
	выходя
		возврат ложь;
	кон FreeLibrary;

	(**
		Get a procedure from a loaded dynamic library

		lib: library handle
		name: name of the procedure
		procAddr: address of the destination procedure pointer (e.g. ADDRESSOF(procedureVariable))

		Return: TRUE in case of success
	*)
	проц GetProcedure*(конст lib: LibHandle; конст name: массив из симв8; конст procAddr: адресВПамяти): булево;
	перем addr: адресВПамяти;
	нач
		утв(procAddr # НУЛЬ);
		Unix.Dlsym(lib,name,procAddr);
		НИЗКОУР.прочтиОбъектПоАдресу(procAddr,addr);
		возврат addr # НУЛЬ;
	выходя
		возврат ложь;
	кон GetProcedure;

кон HostLibs.
