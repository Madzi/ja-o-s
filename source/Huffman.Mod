модуль Huffman;  (** AUTHOR GF;  PURPOSE "Huffman compression";  *)

использует Потоки;

тип
	BitReader = окласс
		перем
			in: Потоки.Чтец;
			curByte, curBit: цел32;

			проц &New( r: Потоки.Чтец );
			нач
				in := r;  curBit := -1;  curByte := 0
			кон New;

			проц Initialize;
			нач
				curBit := -1;  curByte := 0
			кон Initialize;

			проц Bit( ): цел32;
			перем
				bit: цел32;  ch: симв8;
			нач
				если curBit < 0 то
					in.чСимв8( ch );  curByte := кодСимв8( ch );  curBit := 7
				всё;
				bit := арифмСдвиг( curByte, -curBit ) остОтДеленияНа 2;
				умень( curBit );
				возврат bit
			кон Bit;

	кон BitReader;


тип
	BitWriter = окласс
		перем
			out: Потоки.Писарь;
			curByte, curBit: цел32;

			проц &New( w: Потоки.Писарь );
			нач
				out := w;  curBit := 0;  curByte := 0
			кон New;

			проц Bit( bit: цел32 );
			нач
				curByte := 2*curByte + bit;
				увел( curBit );
				если curBit > 7 то
					out.пСимв8( симв8ИзКода( curByte ) );  curByte := 0;  curBit := 0
				всё
			кон Bit;

			проц Finish;	(* flush last few bits *)
			нач
				нцПока curBit # 0 делай  Bit( 0 )  кц;
				out.ПротолкниБуферВПоток
			кон Finish;

	кон BitWriter;


тип
	PatternCounts = массив 256 из цел32;		(* scaled to 0 .. 101H *)

	Pattern = запись  patt, weight: цел32  кон;
	PatternWeights = укль на массив из Pattern;		(* weights of patterns contained in block *)

	HuffCode = запись  bits, val: цел32  кон;

тип
	Node = окласс
		перем
			weight: цел32;
			pattern: цел32;		(* node pattern if node is leaf *)
			left, right: Node;		(* both NIL in case of leaf *)

			проц &Init( patt, w: цел32 );
			нач
				pattern := patt;  weight := w;  left := НУЛЬ;  right := НУЛЬ
			кон Init;

			проц AddChildren( l, r: Node );
			нач
				left := l;  right := r;  weight := l.weight + r.weight
			кон AddChildren;

	кон Node;


тип
	Encoder* = окласс
		перем
			w: Потоки.Писарь;
			out: BitWriter;
			codeTable: массив 256 из HuffCode;



			проц &New*( output: Потоки.Писарь );
			нач
				w := output;
				нов( out, w )
			кон New;


			проц WriteFrequencies( pw: PatternWeights );
			перем i, n: размерМЗ;
				pc: PatternCounts;
			нач
				n := длинаМассива( pw^ );
				если n < 128 то
					w.пСимв8( симв8ИзКода( n ) );
					нцДля i := 0 до n - 1 делай  w.пЦел64_7б( pw[i].weight );  w.пСимв8( симв8ИзКода( pw[i].patt ) )  кц
				иначе
					w.пСимв8( 0X );
					нцДля i := 0 до 255 делай  pc[i] := 0  кц;
					нцДля i := 0 до n -1 делай  pc[pw[i].patt] := pw[i].weight  кц;
					нцДля i := 0 до 255 делай  w.пЦел64_7б( pc[i] )  кц
				всё;
			кон WriteFrequencies;


			проц CountPatterns( конст source: массив из симв8; len: размерМЗ ): PatternWeights;
			перем
				i: размерМЗ;  pc: PatternCounts;
			нач
				нцДля i := 0 до 255 делай  pc[i] := 0  кц;
				нцДля i := 0 до len - 1 делай  увел( pc[кодСимв8( source[i] )] )  кц;
				нцДля i := 0 до 255 делай
					если pc[i] > 0 то (* scale => [1..101H] *)
						pc[i] := 100H * pc[i] DIV len(цел32) + 1;
					всё;
				кц;
				возврат ContainedPatterns( pc )
			кон CountPatterns;


			проц BuildCodeTable( pw: PatternWeights );
			перем
				initval: HuffCode;
				tree: Node;

				проц Traverse( node: Node;  code: HuffCode );
				нач
					если node.left = НУЛЬ то  (* leaf *)
						codeTable[node.pattern] := code;
					иначе
						увел( code.bits );
						code.val := 2*code.val;  Traverse( node.right, code );	(* ..xx0 *)
						code.val := code.val + 1;  Traverse( node.left, code );	(* ..xx1 *)
					всё;
				кон Traverse;

			нач
				tree := BuildTree( pw );
				initval.bits := 0;  initval.val := 0;
				Traverse( tree, initval );
			кон BuildCodeTable;


			проц PutCode( code: HuffCode );
			перем len, val: цел32;
			нач
				len := code.bits;  val := code.val;
				нцПока len > 0 делай
					умень( len );  out.Bit( арифмСдвиг( val, -len ) остОтДеленияНа 2 )
				кц
			кон PutCode;


			проц CompressBlock*( конст source: массив из симв8; len: размерМЗ );
			перем
				i: размерМЗ;
				pw: PatternWeights;
			нач
				pw := CountPatterns( source, len );
				WriteFrequencies( pw );
				BuildCodeTable( pw );
				нцДля i := 0 до len - 1 делай  PutCode( codeTable[кодСимв8( source[i] )] )  кц;
				out.Finish;
			кон CompressBlock;

	кон Encoder;


тип
	Decoder* = окласс
		перем
			r: Потоки.Чтец;
			in: BitReader;
			tree: Node;

			проц &New*( input: Потоки.Чтец );
			нач
				r := input;
				нов( in, r )
			кон New;


			проц ReadFrequencies( r: Потоки.Чтец ): PatternWeights;
			перем
				i, n: цел32;  c: симв8;
				pw: PatternWeights;
				pc: PatternCounts;
			нач
				r.чСимв8( c );  n := кодСимв8( c );
				если n > 0 то
					нов( pw, n );
					нцДля i := 0 до n - 1 делай  r.чЦел32_7б( pw[i].weight );  r.чСимв8( c );  pw[i].patt := кодСимв8( c )  кц
				иначе
					нцДля i := 0 до 255 делай  r.чЦел32_7б( pc[i] )  кц;
					pw := ContainedPatterns( pc )
				всё;
				возврат pw
			кон ReadFrequencies;


			проц ExtractBlock*( перем buf: массив из симв8; len: цел32 );
			перем
				i: цел32;  n: Node;
			нач
				tree := BuildTree( ReadFrequencies( r ) );
				in.Initialize;
				i := 0;
				нцДо
					n := tree;
					нцДо
						если in.Bit() = 1 то  n := n.left  иначе  n := n.right  всё;
					кцПри n.left = НУЛЬ;	(* leaf *)
					buf[i] := симв8ИзКода( n.pattern );  увел( i )
				кцПри i >= len;
			кон ExtractBlock;

	кон Decoder;





	(* sort patterns by weight, omit unused patterns *)
	проц ContainedPatterns( перем pc: PatternCounts ): PatternWeights;
	перем
		i, n, start: цел32;
		pw: PatternWeights;
		a: массив 256 из Pattern;

		проц Sort( low, high: цел32 );
		перем
			i, j, m: цел32;  tmp: Pattern;
		нач
			если low < high то
				i := low;  j := high;  m := (i + j) DIV 2;
				нцДо
					нцПока a[i].weight < a[m].weight делай  увел( i )  кц;
					нцПока a[j].weight > a[m].weight делай  умень( j )  кц;
					если i <= j то
						если i = m то  m := j  аесли j = m то  m := i  всё;
						tmp := a[i];  a[i] := a[j];  a[j] := tmp;
						увел( i );  умень( j )
					всё;
				кцПри i > j;
				Sort( low, j );  Sort( i, high )
			всё
		кон Sort;

	нач
		нцДля i := 0 до 255 делай  a[i].patt := i;  a[i].weight := pc[i]  кц;
		Sort( 0, 255 );	(* sort patterns by weight *)
		i := 0;
		нцПока a[i].weight = 0 делай  увел( i )  кц; 	(* skip unused patterns *)
		n := 256 - i;  start := i;
		нов( pw, n );
		нцДля i := 0 до n - 1 делай  pw[i] := a[start + i]  кц;
		возврат pw
	кон ContainedPatterns;


	проц BuildTree( pw: PatternWeights ): Node;
	перем
		i, start, top: размерМЗ;  node, n2: Node;
		a: укль на массив из Node;
		patt: цел32;
	нач
		нов( a, длинаМассива( pw^ ) );  top := длинаМассива( pw^ ) - 1;
		нцДля i := 0 до top делай  нов( a[i], pw[i].patt, pw[i].weight )  кц;
		если top = 0 то
			(* the whole, probably last small block contains only a single pattern *)
			patt := (a[0].pattern + 1) остОтДеленияНа 256;	(* some different pattern *)
			нов( node, 0, 0 );  нов( n2, patt, 0 );  node.AddChildren( n2, a[0] );
		иначе
			start := 0;
			нцПока start < top делай
				нов( node, 0, 0 );  node.AddChildren( a[start], a[start+1] );
				i := start + 1;
				нцПока (i < top) и (a[i+1].weight < node.weight) делай  a[i] := a[i+1];  увел( i )  кц;
				a[i] := node;
				увел( start );
			кц
		всё;
		возврат node
	кон BuildTree;


кон Huffman.

