модуль Models; (** AUTHOR "staubesv"; PURPOSE "Models"; *)

использует
	Потоки, Locks, Types, Strings, XML, Texts, TextUtilities, Repositories, XMLObjects;

конст
	Ok* = Types.Ok;

	(** Notification mode *)
	NoNotifications* = 0; (** No notification of listeners upon changes *)
	OnChanged* = 1; (** Notify listeners when model value has changed after releasing the write lock *)


	InitialStringSize = 128;

	AttributeName = "name";

тип

	(**
		Base class of models.
		Services:
			- Abstract interface for generic read/write access
			- Recursive reader/writer lock
			- Notification of listeners
			- Internalization/externalization
	*)
	Model* = окласс(Repositories.Component)
	перем
		changed : булево;
		notificationMode : цел8;
		lock : Locks.RWLock;

		проц &{перекрыта}Init*; (** protected *)
		нач
			Init^;
			notificationMode := OnChanged;
			changed := ложь;
			нов(lock);
		кон Init;

	(** Generic access to data of the model using type conversion *)

		(** Generically set data of model. Implicit type conversion if necessary and possible *)
		проц SetGeneric*(конст value : Types.Any; перем res : целМЗ); (** abstract *)
		кон SetGeneric;

		(** Generically get data of model. Implicit type conversion if necessary and possible *)
		проц GetGeneric*(перем value : Types.Any; перем res : целМЗ); (** abstract *)
		кон GetGeneric;

	(** Locking (Recursive reader/writer lock) *)

		(** Acquire read lock. *)
		проц AcquireRead*;
		нач
			lock.AcquireRead;
		кон AcquireRead;

		(** Release read lock *)
		проц ReleaseRead*;
		нач
			lock.ReleaseRead;
		кон ReleaseRead;

		(** Returns TRUE if the caller holds a read lock, FALSE otherwise *)
		проц HasReadLock*() : булево;
		нач
			возврат lock.HasReadLock();
		кон HasReadLock;

		(** Acquire write lock *)
		проц AcquireWrite*;
		нач
			lock.AcquireWrite;
		кон AcquireWrite;

		(** Release write lock. If the data has changed, all listeners will be notified when the last
			writer releases its lock *)
		проц ReleaseWrite*;
		перем notifyListeners : булево;
		нач
			(* If the last writer releases the lock and the model data has changed, we have to notify interested listeners *)
			если (lock.GetWLockLevel() = 1) то
				если (notificationMode = OnChanged) то
					notifyListeners := changed;
					changed := ложь;
				иначе
					notifyListeners := ложь;
				всё;
			иначе
				notifyListeners := ложь;
			всё;
			lock.ReleaseWrite;
			если notifyListeners то
				onChanged.Call(сам);
			всё;
		кон ReleaseWrite;

		(** Returns TRUE if the caller holds the writer lock, FALSE otherwise *)
		проц HasWriteLock*() : булево;
		нач
			возврат lock.HasWriteLock();
		кон HasWriteLock;

	(** Change notification *)

		(** Set how the model notifies listeners upon value changes *)
		проц SetNotificationMode*(mode : цел8);
		нач
			утв((mode = NoNotifications) или (mode = OnChanged));
			lock.AcquireWrite;
			если (notificationMode # mode) то
				notificationMode := mode;
			всё;
			(*	the release of the write lock will cause notification if model value has changed and
				notification was disabled before *)
			lock.ReleaseWrite;
		кон SetNotificationMode;

		(** 	Indicate that the value of the model has changed. Listeners will be notified when the writer lock
			is released. Caller must hold write lock! *)
		проц Changed*; (** protected *)
		нач
			утв(HasWriteLock());
			changed := истина;
		кон Changed;

	(** Internalization and externalization *)

		проц {перекрыта}AddContent*(content : XML.Content); (** overwrite, protected *)
		перем string : Types.String; res : целМЗ;
		нач
			если (content # НУЛЬ) и (content суть XML.Element) и (content(XML.Element).GetName()^="VALUE") то
				content := content(XML.Element).GetFirst();
			всё;

			если (сам суть Container) то
				AddContent^(content);
			аесли (content # НУЛЬ) и (content суть XML.ArrayChars) то
				(* This violates the XML document structure. Could be fixed by allowing XML.ArrayChars SET and GET
					procedures that dynamically set/get the model data as string *)
				string.value := content(XML.ArrayChars).GetStr();
				если (string.value # НУЛЬ) то
					SetGeneric(string, res); (* ignore res *)
				всё;
			аесли (content # НУЛЬ) то AddContent^(content);
			иначе (* empty content, does not have to add content *)
			всё;
		кон AddContent;

		(** Write current data value of model to stream <w> at indention level <level>. Caller must hold read lock *)
		проц WriteValue*(w : Потоки.Писарь; level : цел32);
		нач
			утв(w # НУЛЬ);
			утв(HasReadLock());
		кон WriteValue;

		(** Externalize model to stream <w> at indention level <level> *)
		проц {перекрыта}Write*(w: Потоки.Писарь; context: динамическиТипизированныйУкль; level : цел32); (** overwrite *)
		перем name : Strings.String; enum: XMLObjects.Enumerator; c: динамическиТипизированныйУкль;
		нач
			если (сам суть Container) то
				Write^(w, context, level);
			иначе
				(* Hmm... this violates the idea of XML.Element as container *)
				AcquireRead;
				name := GetName();
				w.пСимв8('<'); w.пСтроку8(name^); WriteAttributes(w, context, level); w.пСимв8('>');
				NewLine(w,level+1);
				w.пСтроку8("<VALUE>");
				WriteValue(w, level + 1);
				w.пСтроку8("</VALUE>");

				enum := GetContents();
				нцПока enum.HasMoreElements() делай
					c := enum.GetNext();
					c(XML.Content).Write(w, context, level+1);
				кц;
				NewLine(w,level);
				w.пСтроку8("</"); w.пСтроку8(name^); w.пСимв8('>');
				ReleaseRead;
			всё;
		кон Write;

	кон Model;

тип

	Boolean* = окласс(Model)
	перем
		value : булево;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrBoolean);
			value := ложь;
			SetGenerator("Models.GenBoolean");
		кон Init;

		проц Set*(value : булево);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : булево;
		перем value : булево;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : булево;
		нач
			Types.GetBoolean(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : булево;
		нач
			currentValue := Get();
			Types.SetBoolean(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			если value то w.пСтроку8("TRUE"); иначе w.пСтроку8("FALSE"); всё;
		кон WriteValue;

	кон Boolean;

тип

	Integer* = окласс(Model)
	перем
		value : цел32;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrInteger);
			value := 0;
			SetGenerator("Models.GenInteger");
		кон Init;

		проц Set*(value : цел32);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : цел32;
		перем value : цел32;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц Add*(value : цел32);
		нач
			если (value # 0) то
				AcquireWrite;
				сам.value := сам.value + value;
				Changed;
				ReleaseWrite;
			всё;
		кон Add;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : цел32;
		нач
			Types.GetInteger(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : цел32;
		нач
			currentValue := Get();
			Types.SetInteger(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			w.пЦел64(value, 0);
		кон WriteValue;

	кон Integer;

	Hugeint* = окласс(Model)
	перем
		value : цел64;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrHugeint);
			value := 0;
			SetGenerator("Models.GenHugeint");
		кон Init;

		проц Set*(value : цел64);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : цел64;
		перем value : цел64;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц Add*(value : цел64);
		нач
			если (value # 0) то
				AcquireWrite;
				сам.value := сам.value + value;
				Changed;
				ReleaseWrite;
			всё;
		кон Add;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : цел64;
		нач
			Types.GetHugeint(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : цел64;
		нач
			currentValue := Get();
			Types.SetHugeint(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			w.пЦел64(value, 0);
		кон WriteValue;

	кон Hugeint;

	Size* = окласс(Model)
	перем
		value : размерМЗ;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrSize);
			value := 0;
			SetGenerator("Models.GenSize");
		кон Init;

		проц Set*(value : размерМЗ);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : размерМЗ;
		перем value : размерМЗ;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц Add*(value : размерМЗ);
		нач
			если (value # 0) то
				AcquireWrite;
				сам.value := сам.value + value;
				Changed;
				ReleaseWrite;
			всё;
		кон Add;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : размерМЗ;
		нач
			Types.GetSize(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : размерМЗ;
		нач
			currentValue := Get();
			Types.SetSize(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			w.пЦел64(value, 0);
		кон WriteValue;

	кон Size;

тип

	Real* = окласс(Model)
	перем
		value : вещ32;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrReal);
			value := 0.0;
			SetGenerator("Models.GenReal");
		кон Init;

		проц Set*(value : вещ32);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : вещ32;
		перем value : вещ32;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : вещ32;
		нач
			Types.GetReal(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : вещ32;
		нач
			currentValue := Get();
			Types.SetReal(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			w.пВещ64(value, 15); (*8 decimal, 'E-', 2 expo, decimal point, leading space*)
		кон WriteValue;

	кон Real;

тип

	Longreal* = окласс(Model)
	перем
		value : вещ64;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrLongreal);
			value := 0.0;
			SetGenerator("Models.GenLongreal");
		кон Init;

		проц Set*(value : вещ64);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : вещ64;
		перем value : вещ64;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : вещ64;
		нач
			Types.GetLongreal(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : вещ64;
		нач
			currentValue := Get();
			Types.SetLongreal(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			w.пВещ64(value, 24); (* leading space, decimal point, 16 digits, 5 expo *)
		кон WriteValue;

	кон Longreal;

тип

	Char* = окласс(Model)
	перем
		value : симв8;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrChar);
			value := 0X;
			SetGenerator("Models.GenChar");
		кон Init;

		проц Set*(value : симв8);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : симв8;
		перем value : симв8;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : симв8;
		нач
			Types.GetChar(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : симв8;
		нач
			currentValue := Get();
			Types.SetChar(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			если IsPrintableCharacter(value) то
				w.пСимв8(value);
			иначе
				w.пСтроку8("0x"); w.пЦел64(кодСимв8(value), 0); (*? TBD Support in Types.Mod *)
			всё;
		кон WriteValue;

	кон Char;

тип

	(** 0X-terminated string (no Unicode support here! *)
	String* = окласс(Model)
	перем
		value : Strings.String; (* {value # NIL} *)

		проц &{перекрыта}Init*;
		нач
			Init^;
			нов(value, InitialStringSize);
			SetNameAsString(StrString);
			SetGenerator("Models.GenString");
		кон Init;

		проц Set*(value : Strings.String);
		нач
			утв(value # НУЛЬ); (*? CHECK *)
			AcquireWrite;
			если (value # сам.value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : Strings.String;
		перем value : Strings.String;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			утв(value # НУЛЬ);
			возврат value;
		кон Get;

		проц SetAOC*(конст value : массив из симв8);
		перем length : цел32;
		нач
			length := 0;
			нцПока (length < длинаМассива(value)) и (value[length] # 0X) делай увел(length); кц;
			AcquireWrite;
			если (length+1 > длинаМассива(сам.value^)) то
				сам.value := Strings.NewString(value);
				Changed;
			аесли (сам.value^ # value) то
				копируйСтрокуДо0(value, сам.value^);
				Changed;
			всё;
			утв(сам.value # НУЛЬ);
			ReleaseWrite;
		кон SetAOC;

		проц GetAOC*(перем value : массив из симв8);
		нач
			AcquireRead;
			копируйСтрокуДо0(сам.value^, value);
			ReleaseRead;
		кон GetAOC;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : Strings.String;
		нач
			Types.GetString(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : Strings.String;
		нач
			currentValue := Get();
			Types.SetString(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		перем res : целМЗ;
		нач
			WriteValue^(w, level);
			XML.UTF8ToStream(value^, w, res); (* ignore res *)
		кон WriteValue;

	кон String;

тип

	Set* = окласс(Model)
	перем
		value : мнвоНаБитахМЗ;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrSet);
			value := {};
			SetGenerator("Models.GenSet");
		кон Init;

		проц Set*(value : мнвоНаБитахМЗ);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value := value;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц Get*() : мнвоНаБитахМЗ;
		перем value : мнвоНаБитахМЗ;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц Include*(element : цел32);
		нач
			AcquireWrite;
			если ~(element в сам.value) то
				включиВоМнвоНаБитах(сам.value, element);
				Changed;
			всё;
			ReleaseWrite;
		кон Include;

		проц Exclude*(element : цел32);
		нач
			AcquireWrite;
			если (element в сам.value) то
				исключиИзМнваНаБитах(сам.value, element);
				Changed;
			всё;
			ReleaseWrite;
		кон Exclude;

		проц Contains*(element : цел32) : булево;
		перем result : булево;
		нач
			AcquireRead;
			result := element в сам.value;
			ReleaseRead;
			возврат result;
		кон Contains;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : мнвоНаБитахМЗ;
		нач
			Types.GetSet(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : мнвоНаБитахМЗ;
		нач
			currentValue := Get();
			Types.SetSet(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			w.пМнвоНаБитахМЗ(value);
		кон WriteValue;

	кон Set;

тип

	(*? would make more sense to Texts.UnicodeText to be the model itself *)
	Text* = окласс(Model)
	перем
		value : Texts.Text; (* {value # NIL} *)

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrText);
			нов(value); value.onTextChanged.Add(OnTextChanged);
			SetGenerator("Models.GenText");
		кон Init;

		проц {перекрыта}AcquireRead*;
		нач
			value.AcquireRead;
		кон AcquireRead;

		проц {перекрыта}ReleaseRead*;
		нач
			value.ReleaseRead;
		кон ReleaseRead;

		проц {перекрыта}HasReadLock*() : булево;
		нач
			возврат value.HasReadLock();
		кон HasReadLock;

		проц {перекрыта}AcquireWrite*;
		нач
			value.AcquireWrite;
		кон AcquireWrite;

		проц {перекрыта}ReleaseWrite*;
		нач
			value.ReleaseWrite;
			onChanged.Call(сам); (*? TBD only call when text has changed *)
		кон ReleaseWrite;

		проц {перекрыта}HasWriteLock*() : булево;
		нач
			возврат value.HasWriteLock();
		кон HasWriteLock;

		(* will copy text! *)
		проц Set*(value : Texts.Text);
		нач
			AcquireWrite;
			если (сам.value # value) то
				сам.value.Delete(0, сам.value.GetLength());
				value.AcquireRead;
				сам.value.CopyFromText(value, 0, value.GetLength(), 0);
				value.ReleaseRead;
				Changed;
			всё;
			ReleaseWrite;
		кон Set;

		проц SetReference*(value: Texts.Text);
		нач
			сам.value := value;
			AcquireWrite;
			Changed;
			ReleaseWrite;
		кон SetReference;

		проц Get*() : Texts.Text;
		перем value : Texts.Text;
		нач
			AcquireRead;
			value := сам.value;
			ReleaseRead;
			возврат value;
		кон Get;

		проц OnTextChanged(sender, data : динамическиТипизированныйУкль);
		нач
			Changed;
		кон OnTextChanged;

		проц SetAsString*(конст string : массив из симв8);
		нач
			value.AcquireWrite;
			value.Delete(0, value.GetLength());
			TextUtilities.StrToText(value, 0, string);
			ReleaseWrite;
		кон SetAsString;

		проц GetAsString*(перем string : массив из симв8);
		нач
			AcquireRead;
			TextUtilities.TextToStr(value, string);
			ReleaseRead;
		кон GetAsString;

		проц {перекрыта}SetGeneric*(конст value : Types.Any; перем res : целМЗ);
		перем newValue : Texts.Text;
		нач
			Types.GetText(value, newValue, res);
			если (res = Types.Ok) то Set(newValue); всё;
		кон SetGeneric;

		проц {перекрыта}GetGeneric*(перем value : Types.Any; перем res : целМЗ);
		перем currentValue : Texts.Text;
		нач
			currentValue := Get();
			Types.SetText(value, currentValue, res);
		кон GetGeneric;

		проц {перекрыта}WriteValue*(w : Потоки.Писарь; level : цел32); (** protected *)
		нач
			WriteValue^(w, level);
			(* TBD *)
		кон WriteValue;

	кон Text;

тип

	Container* = окласс(Model)

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrContainer);
			SetGenerator("Models.GenContainer");
		кон Init;

		проц FindModel(конст name : массив из симв8) : Model;
		перем result : Model; string : Strings.String; content : XML.Content;
		нач
			result := НУЛЬ;
			(*? locking!!! *)
			content := GetFirst();
			нцПока (result = НУЛЬ) и (content # НУЛЬ) делай
				если (content суть Model) то
					string := content(Model).GetAttributeValue(AttributeName);
					если (string # НУЛЬ) и (string^ = name) то result := content(Model); всё;
				всё;
				content := GetNext(content);
			кц;
			возврат result;
		кон FindModel;

		проц FindModelByName(конст fullname : массив из симв8) : Model;
		перем curModel : Model; name : массив 32 из симв8; i, j : цел32; done : булево;
		нач
			curModel := сам;
			done := ложь;
			i := 0; j := 0;
			нцПока ~done и (curModel # НУЛЬ)  и (i < длинаМассива(fullname)) и (j < длинаМассива(name)) делай
				если (fullname[i] = ".") или (fullname[i] = 0X) то
					name[j] := 0X;
					если (curModel суть Container) то
						curModel := curModel(Container).FindModel(name);
					иначе
						curModel := НУЛЬ;
					всё;
					done := (fullname[i] = 0X);
					j := 0;
				иначе
					name[j] := fullname[i];
					увел(j);
				всё;
				увел(i);
			кц;
			возврат curModel;
		кон FindModelByName;

		проц SetField*(конст name : массив из симв8; конст value : Types.Any; перем res : целМЗ);
		перем model : Model;
		нач
			model := FindModelByName(name);
			если (model # НУЛЬ) и ~(model суть Container) то
				model.SetGeneric(value, res);
			иначе
				res := 192;
			всё;
		кон SetField;

		проц GetField*(конст name : массив из симв8; перем value : Types.Any; перем res : целМЗ);
		перем model : Model;
		нач
			model := FindModelByName(name);
			если (model # НУЛЬ) и ~(model суть Container) то
				model.GetGeneric(value, res);
			иначе
				res := 192;
			всё;
		кон GetField;

	кон Container;


перем
	StrBoolean, StrInteger, StrHugeint, StrSize, StrReal, StrLongreal, StrChar, StrString, StrSet, StrText, StrContainer : Strings.String;

проц NewLine*(w : Потоки.Писарь; level : цел32);
нач
	утв(w # НУЛЬ);
	w.пВК_ПС; нцПока level > 0 делай w.пСимв8(09X); умень(level) кц
кон NewLine;

(* Helper procedures *)

проц IsPrintableCharacter(ch : симв8) : булево;
нач
	возврат (" " < ch) и (кодСимв8(ch) < 128);
кон IsPrintableCharacter;

(* global helper procedures *)



проц GetReal*(m: Model; перем r: вещ64): булево;
перем real: Types.Longreal; res: целМЗ;
нач
	если m = НУЛЬ то возврат ложь всё;
	m.GetGeneric(real, res);
	если (res = Ok) то
		r := real.value; возврат истина
	иначе возврат ложь
	всё;
кон GetReal;

проц GetInteger*(m: Model; перем i: цел32): булево;
перем int: Types.Integer; res: целМЗ;
нач
	если m = НУЛЬ то возврат ложь всё;
	m.GetGeneric(int, res);
	если (res = Ok) то
		i := int.value; возврат истина
	иначе возврат ложь
	всё;
кон GetInteger;

проц SetReal*(m: Model; r: вещ64);
перем real: Types.Longreal; res: целМЗ;
нач
	если m = НУЛЬ то возврат всё;
	real.value := r;
	m.SetGeneric(real, res);
кон SetReal;

(** Generator procedures *)

проц GenBoolean*() : XML.Element;
перем boolean : Boolean;
нач
	нов(boolean); возврат boolean;
кон GenBoolean;

проц GenInteger*() : XML.Element;
перем integer : Integer;
нач
	нов(integer); возврат integer;
кон GenInteger;

проц GenHugeint*() : XML.Element;
перем hugeint : Hugeint;
нач
	нов(hugeint); возврат hugeint;
кон GenHugeint;

проц GenSize*() : XML.Element;
перем size : Size;
нач
	нов(size); возврат size;
кон GenSize;

проц GenReal*() : XML.Element;
перем real : Real;
нач
	нов(real); возврат real;
кон GenReal;

проц GenLongreal*() : XML.Element;
перем longReal : Longreal;
нач
	нов(longReal); возврат longReal;
кон GenLongreal;

проц GenChar*() : XML.Element;
перем char : Char;
нач
	нов(char); возврат char;
кон GenChar;

проц GenString*() : XML.Element;
перем string : String;
нач
	нов(string); возврат string;
кон GenString;

проц GenSet*() : XML.Element;
перем set : Set;
нач
	нов(set); возврат set;
кон GenSet;

проц GenText*() : XML.Element;
перем text : Text;
нач
	нов(text); возврат text;
кон GenText;

проц GenContainer*() : XML.Element;
перем container : Container;
нач
	нов(container); возврат container;
кон GenContainer;

проц InitStrings;
нач
	StrBoolean := Strings.NewString("Boolean");
	StrInteger := Strings.NewString("Integer");
	StrHugeint := Strings.NewString("Hugeint");
	StrSize := Strings.NewString("Size");
	StrReal := Strings.NewString("Real");
	StrLongreal := Strings.NewString("Longreal");
	StrChar := Strings.NewString("Char");
	StrString := Strings.NewString("String");
	StrSet := Strings.NewString("Set");
	StrText := Strings.NewString("Text");
	StrContainer := Strings.NewString("Container");
кон InitStrings;

нач
	InitStrings;
кон Models.
