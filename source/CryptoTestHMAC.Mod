модуль CryptoTestHMAC;	(** AUTHOR "F.N."; PURPOSE "HMAC Test"; *)

использует
		CryptoHMAC, Utils := CryptoUtils, Out := ЛогЯдра;

	проц DoTest( modname, data, key, expDigest: массив из симв8; dataLen, keyLen: цел32 );
		перем
			hmac: CryptoHMAC.HMac;
			output: массив 20 из симв8;
	нач
		нов( hmac, modname );
		Out.пВК_ПС; Out.пСтроку8( "*************************" ); Out.пВК_ПС;
		Out.пСтроку8( "HMAC-Test. Digest: " ); Out.пСтроку8( modname ); Out.пВК_ПС; Out.пВК_ПС;
		Out.пСтроку8( "key: " ); Utils.PrintHex( key, 0, keyLen ); Out.пВК_ПС;
		Out.пСтроку8( "data: " ); Out.пСтроку8( data ); Out.пВК_ПС;
		Out.пСтроку8( "expected digest: " ); Utils.PrintHex( expDigest, 0, hmac.size ); Out.пВК_ПС;
		hmac.Initialize( key, keyLen );
		hmac.Update( data, 0, dataLen );
		hmac.GetMac( output, 0 );
		Out.пСтроку8( "computed digest: " ); Utils.PrintHex( output, 0, hmac.size ); Out.пВК_ПС;
	кон DoTest;

	(* produces two macs from the same data: in one and in two iterations respective *)
	проц ConcatenateTest( modname: массив из симв8 );
		перем
			hmac: CryptoHMAC.HMac;
			binData, output, key: массив 20 из симв8;
	нач
		нов( hmac, modname );
		Out.пВК_ПС; Out.пСтроку8( "*************************" ); Out.пВК_ПС;
		Out.пСтроку8( "HMAC Concatenation-Test. Digest: " ); Out.пСтроку8( modname ); Out.пВК_ПС; Out.пВК_ПС;
		key := "abcdefghijklmnop";
		binData := "hey mister music";
		hmac.Initialize( key, 16 );
		hmac.Update( binData, 0, 16 );
		hmac.GetMac( output, 0 );
		Out.пСтроку8( "digest when Update is invoked once:" ); Utils.PrintHex( output, 0, hmac.size ); Out.пВК_ПС;
		hmac.Initialize( key, 16 );
		hmac.Update( binData, 0, 4 );
		hmac.Update( binData, 4, 6 );
		hmac.Update( binData, 10, 6 );
		hmac.GetMac( output, 0 );
		Out.пСтроку8( "digest when Update is invoked three times:" ); Utils.PrintHex( output, 0, hmac.size ); Out.пВК_ПС
	кон ConcatenateTest;

	(* test vectors from rfc 2104 *)
	проц Test1MD5*;
		перем binKey, hexKey, hexDigest, binDigest: массив 64 из симв8;
	нач
		hexKey := "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b";
		hexDigest := "9294727a3638bb1c13f48ef8158bfc9d";
		Utils.Hex2Bin( hexKey, 0, binKey, 0, 16 );
		Utils.Hex2Bin( hexDigest, 0, binDigest, 0, 16 );
		DoTest( "CryptoMD5", "Hi There", binKey, binDigest, 8, 16 );
	кон Test1MD5;

	проц Test1SHA1*;
		перем binKey, hexKey, hexDigest, binDigest: массив 64 из симв8;
	нач
		hexKey := "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b";
		hexDigest := "b617318655057264e28bc0b6fb378c8ef146be00";
		Utils.Hex2Bin( hexKey, 0, binKey, 0, 20 );
		Utils.Hex2Bin( hexDigest, 0, binDigest, 0, 20 );
		DoTest( "CryptoSHA1", "Hi There", binKey, binDigest, 8, 20 );
	кон Test1SHA1;

	проц Test2*;
		перем
			binKey, hexKey, hexDigest, binDigest: массив 64 из симв8;
			hexData, binData: массив 104 из симв8;
	нач
		hexKey := "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		hexDigest := "125d7342b2ac11cd91a39af48aa17b4f63f175d3";
		hexData := "dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
		Utils.Hex2Bin( hexKey, 0, binKey, 0, 20 );
		Utils.Hex2Bin( hexDigest, 0, binDigest, 0, 20 );
		Utils.Hex2Bin( hexData, 0, binData, 0, 50 );
		DoTest( "CryptoSHA1", binData, binKey, binDigest, 50, 20 );
	кон Test2;

	проц Test3*;
		перем
			hexDigest, binDigest: массив 64 из симв8;
	нач
		hexDigest := "750c783e6ab0b503eaa86e310a5db738";
		Utils.Hex2Bin( hexDigest, 0, binDigest, 0, 16 );
		DoTest( "CryptoMD5", "what do ya want for nothing?", "Jefe", binDigest, 28, 4 );
	кон Test3;

	проц MD5ConcatenateTest*;
	нач
		ConcatenateTest( "CryptoMD5" );
	кон MD5ConcatenateTest;

кон CryptoTestHMAC.


System.Free CryptoTestHMAC CryptoHMAC CryptoMD5 AosCryptoSHA~

Aos.Call CryptoTestHMAC.Test1MD5~
Aos.Call CryptoTestHMAC.Test1SHA1~
Aos.Call CryptoTestHMAC.Test2~
Aos.Call CryptoTestHMAC.Test3~
Aos.Call CryptoTestHMAC.MD5ConcatenateTest~

