модуль LisBinaryCode; (** AUTHOR ""; PURPOSE ""; *)

использует Basic := FoxBasic, Sections := LisSections, Потоки, ObjectFile, BitSets;

конст
	Absolute*=ObjectFile.Absolute;
	Relative*=ObjectFile.Relative;
	Byte=8;
тип
	Code* = BitSets.BitSet;
	Unit*= ObjectFile.Unit;
	Bits*=ObjectFile.Bits;

	FixupPatterns*=ObjectFile.FixupPatterns;

	Alias*=окласс
	перем
		nextAlias-: Alias;
		identifier-: ObjectFile.Identifier;
		offset-: Unit;

		проц & InitAlias*(l0identifier: ObjectFile.Identifier; l1offset: Unit);
		нач
			nextAlias := НУЛЬ;
			сам.identifier := l0identifier;
			сам.offset := l1offset;
		кон InitAlias;

		проц Dump*(w: Потоки.Писарь);
		нач
			Basic.WriteSegmentedName(w, identifier.name);
			если identifier.fingerprint # 0 то w.пСтроку8("["); w.п16ричное(identifier.fingerprint,0); w.пСтроку8("]") всё;
			w.пСтроку8(" "); w.пЦел64(offset,1);
		кон Dump;

	кон Alias;

	AliasList*=окласс
	перем
		firstAlias-, lastAlias-: Alias; aliases-: размерМЗ;

		проц &InitAliasList*;
		нач
			firstAlias := НУЛЬ; lastAlias := НУЛЬ;
			aliases := 0;
		кон InitAliasList;

		проц AddAlias*(alias: Alias);
		нач
			если firstAlias = НУЛЬ то
				firstAlias := alias;
			иначе
				lastAlias.nextAlias := alias;
			всё;
			lastAlias := alias; alias.nextAlias := НУЛЬ;
			увел(aliases);
		кон AddAlias;

		проц Dump*(w: Потоки.Писарь);
		перем alias: Alias;
		нач
			alias := firstAlias;
			нцПока alias # НУЛЬ делай
				w.пСтроку8("alias "); w.пЦел64(alias.offset,1); w.пСтроку8(" <-- ");
				alias.Dump(w);
				w.пВК_ПС;
				alias := alias.nextAlias;
			кц;
		кон Dump;

	кон AliasList;

	Fixup*=окласс
	перем
		nextFixup-: Fixup;
		mode-: целМЗ; (* fixup mode: relative or absolute *)
		displacement-: Unit; (* displacement of the fixup ('source') *)
		scale-: ObjectFile.Bits; (* exponent of scale factor (factor=2^scale) *)
		patterns-: размерМЗ;
		pattern-: FixupPatterns; (* patterns describing the fixup format, cf. above *)
		offset-: Unit;

		symbol-: ObjectFile.Identifier; (* reference to the fixup's destination section *)
		symbolOffset-: размерМЗ; (* offset in intermediate section, must be patched (resolved and added to displacement) to destination section displacement *)
		resolved*: Sections.Section; (* cache ! *)

		проц & InitFixup*(l2mode: целМЗ; fixupOffset: Unit; l3symbol: ObjectFile.Identifier; l4symbolOffset: размерМЗ; l5displacement: Unit; l6scale: Bits; fixupPattern: ObjectFile.FixupPatterns);
		нач
			утв((l2mode = Relative) или (l2mode = Absolute));
			утв(l3symbol.name # "");
			утв(l3symbol.name[0] # 0);
			nextFixup := НУЛЬ;
			сам.mode := l2mode;
			сам.displacement := l5displacement;
			сам.scale := l6scale;
			сам.offset := fixupOffset;
			сам.pattern := fixupPattern;
			если fixupPattern # НУЛЬ то
				сам.patterns := длинаМассива(fixupPattern);
			иначе
				сам.patterns := 0
			всё;

			сам.symbol := l3symbol;
			сам.symbolOffset := l4symbolOffset;
		кон InitFixup;

		проц SetFixupOffset*(l7offset: Unit);
		нач
			сам.offset := l7offset;
		кон SetFixupOffset;

		проц SetSymbol*(l8symbol: Sections.SectionName; fp: ObjectFile.Fingerprint; l9symbolOffset: размерМЗ; l10displacement: Unit);
		нач
			сам.symbol.name := l8symbol;
			сам.symbol.fingerprint := fp;
			сам.symbolOffset := l9symbolOffset;
			сам.displacement := l10displacement;
		кон SetSymbol;

		проц Dump*(w: Потоки.Писарь);
		перем i: размерМЗ;
		нач
			Basic.WriteSegmentedName(w, symbol.name);
			если symbol.fingerprint # 0 то w.пСтроку8("["); w.п16ричное(symbol.fingerprint,0); w.пСтроку8("]") всё;
			если symbolOffset # 0 то w.пСтроку8(":"); w.пЦел64(symbolOffset, 0) всё;

			w.пСтроку8(" (displ="); w.пЦел64(displacement, 0); w.пСтроку8(")");

			если scale # 0 то w.пСтроку8(" *"); w.пЦел64(scale,1); всё;
			w.пСтроку8(" [");

			если pattern # НУЛЬ то
				нцДля i := 0 до длинаМассива(pattern)-1 делай
					w.пЦел64(pattern[i].offset,1);
					если pattern[i].bits >=0 то w.пСтроку8("+"); w.пЦел64(pattern[i].bits,1);
					иначе w.пСтроку8("-"); w.пЦел64(-pattern[i].bits,1);
					всё;
					если i < длинаМассива(pattern)-1 то w.пСтроку8(", ") иначе w.пСтроку8(" ") всё;
				кц;
			всё;
			если mode = Absolute то w.пСтроку8("abs") аесли mode = Relative то w.пСтроку8("rel") иначе w.пСтроку8("?"); всё;
			w.пСтроку8("]");

		кон Dump;

	кон Fixup;

	FixupList*=окласс
	перем
		firstFixup-, lastFixup-: Fixup; fixups-: размерМЗ;

		проц &InitFixupList*;
		нач
			firstFixup := НУЛЬ; lastFixup := НУЛЬ;
			fixups := 0;
		кон InitFixupList;

		проц AddFixup*(fixup: Fixup);
		нач
			если firstFixup = НУЛЬ то
				firstFixup := fixup;
			иначе
				lastFixup.nextFixup := fixup;
			всё;
			lastFixup := fixup; fixup.nextFixup := НУЛЬ;
			увел(fixups);
		кон AddFixup;

		проц Dump*(w: Потоки.Писарь);
		перем fixup: Fixup;
		нач
			fixup := firstFixup;
			нцПока fixup # НУЛЬ делай
				w.пСтроку8("fixup "); w.пЦел64(fixup.offset,1); w.пСтроку8(" <-- ");
				fixup.Dump(w);
				w.пВК_ПС;
				fixup := fixup.nextFixup;
			кц;
		кон Dump;

	кон FixupList;

	LabelList*= укль на запись
		offset-: Unit; position-: Basic.Position;
		prev-: LabelList;
	кон;

	Section* = окласс
	перем
		os*: ObjectFile.Section;
		labels-: LabelList; (* labels for tracking the PC / debugging *)
		fixupList-: FixupList;
		aliasList-: AliasList;
		finally-: Unit; (* position of finally section in bitstream -1 if none *)
		comments-: Sections.CommentWriter; (* writer to write comment text between instructions *)
		bigEndian-: булево; (* endianess of bits (not bytes) *)
		pc-: Unit; (* current position, in units *)

		проц GetPC(): Unit;
		нач
			возврат pc;
		кон GetPC;

		проц & InitBinarySection*(type: целМЗ; unit: Bits; конст name:Basic.SegmentedName; dump: булево; l11bigEndian: булево);
		нач
			утв(unit > 0);
			утв(unit <= 32); (* implementation restriction *)
			сам.os.type := type;
			os.identifier.name := name;
			(*
			ObjectFile.ToSegmentedName(name, SELF.identifier.name);
			*)
			(*COPY(name,SELF.identifier.name);*)
			нов(os.bits,0);
			сам.os.unit := unit;
			если dump то
				comments := Sections.NewCommentWriter(GetPC);
			иначе
				comments := НУЛЬ
			всё;
			os.alignment := 0;
			finally := -1;
			labels := НУЛЬ;
			сам.bigEndian := l11bigEndian;
			нов(fixupList);
			нов(aliasList);
			pc := 0;
			os.fixed := ложь;
		кон InitBinarySection;

		проц Reset*;
		нач
			нов(os.bits,0);
			нов(fixupList);
			если comments # НУЛЬ то comments.Сбрось всё;
			pc := 0;
		кон Reset;

		проц AddLabel*(position: Basic.Position);
		перем new: LabelList;
		нач
			нов(new);
			если labels = НУЛЬ то
				labels := new
			иначе
				new.prev := labels; labels := new;
			всё;
			new.position := position;
			new.offset := pc;
		кон AddLabel;

		проц SetPC*(l12pc: Unit);
		нач
			сам.pc := l12pc;
			CheckSize(0); (* adjust size *)
		кон SetPC;

		проц Align*(alignment: Unit);
		нач
			SetPC(pc + (-pc) остОтДеленияНа alignment)
		кон Align;

		проц SetFinally*(atPC: Unit);
		нач finally := atPC
		кон SetFinally;

		проц SetAlignment*(fixed: булево; alignat: Unit);
		нач os.alignment := alignat; сам.os.fixed := fixed;
		кон SetAlignment;

		проц CheckSize(size: размерМЗ);
		нач
			если os.bits.GetSize()  < size + pc*os.unit то os.bits.Resize(size + pc*os.unit) всё;
			утв(os.bits.GetSize() остОтДеленияНа os.unit = 0);
		кон CheckSize;

		проц CopyBits*(src: BitSets.BitSet; srcPos, len: Bits);
		нач
			утв(len остОтДеленияНа os.unit = 0);
			CheckSize(src.GetSize());
			BitSets.CopyBits(src,srcPos,os.bits,pc*os.unit,len);
			увел(pc,len DIV os.unit);
		кон CopyBits;

		проц PutBits*(d: цел64; size: Bits);
		нач
			(*ASSERT(size MOD unit = 0);*)
			CheckSize(size);
			os.bits.SetBits(pc*os.unit,size,d);
			увел(pc,size DIV os.unit);
		кон PutBits;

		проц PutBitsAt*(at: Unit; d: цел64; size: Bits);
		перем oldpc: Unit;
		нач
			oldpc := pc;
			pc := at;
			PutBits(d,size);
			pc := oldpc;
		кон PutBitsAt;

		проц PutByte* (b: целМЗ);
		нач
			PutBits(b,Byte);
		кон PutByte;

		проц PutWord*(w: целМЗ);
		перем c1,c2: целМЗ;
		нач
			утв((2*Byte) остОтДеленияНа os.unit = 0);
			CheckSize(2*Byte);
			c1 := w;
			c2 := w DIV 100H;
			если bigEndian то
				os.bits.SetBits(pc*os.unit,Byte,c2);
				os.bits.SetBits(pc*os.unit+Byte,Byte,c1);
			иначе
				os.bits.SetBits(pc*os.unit,Byte,c1);
				os.bits.SetBits(pc*os.unit+Byte,Byte,c2);
			всё;
			увел(pc,(2*Byte) DIV os.unit);
		кон PutWord;

		проц PutDWord*(d: целМЗ);
		перем c1,c2,c3,c4: целМЗ;
		нач
			утв((4*Byte) остОтДеленияНа os.unit = 0);
			CheckSize(4*Byte);
			c1 := d;
			c2 := d DIV 100H;
			c3 := d DIV 10000H;
			c4 := d DIV 1000000H;
			если bigEndian то
				os.bits.SetBits(pc*os.unit+0*Byte,Byte,c4);
				os.bits.SetBits(pc*os.unit+1*Byte,Byte,c3);
				os.bits.SetBits(pc*os.unit+2*Byte,Byte,c2);
				os.bits.SetBits(pc*os.unit+3*Byte,Byte,c1);
			иначе
				os.bits.SetBits(pc*os.unit+0*Byte,Byte,c1);
				os.bits.SetBits(pc*os.unit+1*Byte,Byte,c2);
				os.bits.SetBits(pc*os.unit+2*Byte,Byte,c3);
				os.bits.SetBits(pc*os.unit+3*Byte,Byte,c4);
			всё;
			увел(pc,(4*Byte) DIV os.unit);
		кон PutDWord;

		проц PutQWord* (q: цел64);
		перем c: массив 8 из целМЗ; i: целМЗ;
		нач
			утв((8*Byte) остОтДеленияНа os.unit = 0);
			CheckSize(8*Byte);
			нцДля i := 0 до 7 делай
				c[i] := устарПреобразуйКБолееУзкомуЦел(q остОтДеленияНа 100H);
				q := q DIV 100H;
			кц;
			если bigEndian то
				нцДля i := 0 до 7 делай
					os.bits.SetBits(pc*os.unit+i*Byte,Byte,c[7-i]);
				кц;
			иначе
				нцДля i := 0 до 7 делай
					os.bits.SetBits(pc*os.unit+i*Byte,Byte,c[i]);
				кц;
			всё;
			увел(pc,(8*Byte) DIV os.unit);
		кон PutQWord;

		проц PutReal*(f: вещ32);
		нач
			PutDWord(ConvertReal(f))
		кон PutReal;

		проц PutLongreal*(f: вещ64);
		нач
			PutQWord(ConvertLongreal(f))
		кон PutLongreal;

		проц PutByteAt*(at: Unit; d: целМЗ);
		перем oldpc: Unit;
		нач
			oldpc := pc;
			pc := at;
			PutByte(d);
			pc := oldpc;
		кон PutByteAt;

		проц PutWordAt*(at: Unit; d: целМЗ);
		перем oldpc: Unit;
		нач
			oldpc := pc;
			pc := at;
			PutWord(d);
			pc := oldpc;
		кон PutWordAt;

		проц PutDWordAt*(at: Unit; d: целМЗ);
		перем oldpc: Unit;
		нач
			oldpc := pc;
			pc := at;
			PutDWord(d);
			pc := oldpc;
		кон PutDWordAt;

		проц PutQWordAt*(at: Unit; d: цел64);
		перем oldpc: Unit;
		нач
			oldpc := pc;
			pc := at;
			PutQWord(d);
			pc := oldpc;
		кон PutQWordAt;

		проц PutBytes* (data: цел64; bytes: целМЗ);
		нач
			просей bytes из
			1: PutByte (устарПреобразуйКБолееУзкомуЦел(data));
			| 2: PutWord (устарПреобразуйКБолееУзкомуЦел(data));
			| 4: PutDWord (устарПреобразуйКБолееУзкомуЦел(data));
			| 8: PutQWord(data);
			всё
		кон PutBytes;

		проц GetByte* (l13pc: Unit): симв8;
		нач
			возврат симв8ИзКода(os.bits.GetBits(l13pc*os.unit,8));
		кон GetByte;

		проц GetWord*(l14pc: Unit): целМЗ;
		перем c1,c2: целМЗ;
		нач
			c1 := os.bits.GetBits(l14pc*os.unit,8);
			c2 := os.bits.GetBits(l14pc*os.unit+8,8);
			если bigEndian то
				возврат c1*100H + c2;
			иначе
				возврат c1 + c2*100H;
			всё
		кон GetWord;

		проц GetDWord*(l15pc: Unit): целМЗ;
		перем c1,c2,c3,c4: целМЗ;
		нач
			c1 := os.bits.GetBits(l15pc*os.unit+0*Byte,Byte);
			c2 := os.bits.GetBits(l15pc*os.unit+1*Byte,Byte);
			c3 := os.bits.GetBits(l15pc*os.unit+2*Byte,Byte);
			c4 := os.bits.GetBits(l15pc*os.unit+3*Byte,Byte);
			если bigEndian то
				возврат c4 + 100H * (c3 + 100H * (c2 + c1*100H));
			иначе
				возврат c1 + 100H * (c2 + 100H * (c3 + c4*100H));
			всё
		кон GetDWord;

		проц GetQWord*(l16pc: Unit): цел64;
		перем i: целМЗ; h: цел64;
		нач
			h := 0;
			если bigEndian то
				нцДля i := 0 до 7 делай
					h := 100H*h;
					h := h + os.bits.GetBits(l16pc*os.unit+i*Byte,Byte);
				кц;
			иначе
				нцДля i := 7 до 0 шаг -1 делай
					h := 100H*h;
					h := h + os.bits.GetBits(l16pc*os.unit+i*Byte,Byte);
				кц;
			всё;
			возврат h
		кон GetQWord;

		проц GetReal*(l17pc: Unit): вещ32;
		нач
			возврат ConvertToReal(GetDWord(l17pc))
		кон GetReal;

		проц GetLongreal*(l18pc: Unit): вещ64;
		нач
			возврат ConvertToLongreal(GetDWord(l18pc))
		кон GetLongreal;

		проц GetBits*(l19pc: Unit; size: Bits): целМЗ;
		нач
			возврат os.bits.GetBits(l19pc*os.unit,size)
		кон GetBits;

		проц ApplyFixup*(fixup: Fixup): булево;
		перем address: Unit; i: размерМЗ;

			проц PatchPattern (конст pattern: ObjectFile.FixupPattern);
			нач
				os.bits.SetBits(fixup.offset*os.unit+pattern.offset,pattern.bits,address);
				address := арифмСдвиг (address, -pattern.bits);
			кон PatchPattern;

			проц CheckBits(): булево;
			перем nobits, remainder: Bits; l20i: размерМЗ;
			нач
				nobits := 0;
				нцДля l20i := 0 до fixup.patterns-1 делай
					увел(nobits,fixup.pattern[l20i].bits);
				кц;
				если fixup.mode = Relative то умень(nobits) всё;
				remainder := арифмСдвиг(address,-nobits);
				возврат (nobits >31) или (remainder = 0) или (remainder = -1)
			кон CheckBits;

		нач
			address := fixup.displacement;
			если fixup.mode = Relative то
				address := address - fixup.offset
			иначе
				утв(fixup.mode = Absolute)
			всё;
			address := арифмСдвиг(address,fixup.scale);

			если CheckBits() то
				нцДля i := 0 до fixup.patterns-1 делай
					PatchPattern(fixup.pattern[i]);
				кц;
				возврат истина
			иначе
				возврат ложь
			всё;
		кон ApplyFixup;

		проц DumpCode*(w: Потоки.Писарь; from,to: Unit);
		перем i: Unit; c: Sections.Comment; nextpos: Unit;

			проц Hex(l21i: целМЗ): симв8;
			нач
				утв(l21i>=0);
				утв(l21i<16);
				если l21i<10 то
					возврат симв8ИзКода(кодСимв8("0")+l21i)
				иначе
					возврат симв8ИзКода(кодСимв8("A")+l21i-10);
				всё;
			кон Hex;

			проц DumpUnit(at: Unit);
			перем val: целМЗ; a: массив 9 из симв8; bits: Bits;
			нач
				val := GetBits(at,os.unit);
				bits := os.unit;
				a[(bits-1) DIV 4 +1] := 0X;
				нцПока (bits > 0) делай
					a[(bits-1) DIV 4] := Hex(val остОтДеленияНа 16);
					val := логСдвиг(val,-4);
					умень(bits,4);
				кц;
				w.пСтроку8(a);
			кон DumpUnit;

			проц DumpBlock(l22from,l23to: Unit);
			перем l24i: Unit; nr: целМЗ;
			нач
				l24i := l22from; nr := 0;
				если l23to >= pc то l23to := pc-1 всё;
				нцПока l24i <= l23to делай
					w.пСтроку8("[");
					если comments = НУЛЬ то w.п16ричное(l24i,3) иначе w.пЦел64(l24i,4) всё;
					w.пСтроку8("] ");
					nr := 0;
					нцПока (l24i<=l23to) и (nr<16) делай
						если l24i = 8 то w.пСтроку8(" ") всё;
						DumpUnit(l24i);
						w.пСтроку8(" ");
						увел(l24i); увел(nr);
					кц;
					если l24i <= l23to то
						w.пВК_ПС;
					всё;
				кц;
			кон DumpBlock;

		нач
			если comments # НУЛЬ то
				c := comments.firstComment;
				нцПока(c # НУЛЬ) и (c.pos <from) делай
					c := c.nextComment;
				кц;
				i := from;
				нцПока(i<=to) делай
					нцПока (c # НУЛЬ) и (c.pos = i) делай
						c.Dump(w); w.пВК_ПС;
						c := c.nextComment;
					кц;
					если (c # НУЛЬ) и (c.pos <= to) то nextpos := c.pos-1 иначе nextpos := to всё;
					DumpBlock(i,nextpos);w.пВК_ПС;
					i := nextpos+1;
				кц;
				нцПока (c#НУЛЬ) и (c.pos = to+1) делай
					c.Dump(w); w.пВК_ПС; c := c.nextComment;
				кц;
			иначе
				DumpBlock(0,сам.pc-1)
			всё
		кон DumpCode;

		проц Dump*(w: Потоки.Писарь);
		перем ww: Basic.Writer;
		нач
			если comments # НУЛЬ то comments.ПротолкниБуферВПоток всё;
			ww := Basic.GetWriter(w);
			ww.пСтроку8(" unit="); ww.пЦел64(os.unit,1);
			если os.fixed то w.пСтроку8(" fixed") иначе w.пСтроку8(" relocatable") всё;
			w.пСтроку8(" align="); w.пЦел64(os.alignment,1);
			ww.пСтроку8(" size="); ww.пЦел64(сам.pc,1);
			ww.пСтроку8(" fixups="); ww.пЦел64(сам.os.fixups,1);
			ww.пВК_ПС;
			ww.IncIndent;
			fixupList.Dump(ww);
			DumpCode(ww,0,сам.pc-1);
			ww.DecIndent;
		кон Dump;

	кон Section;

	проц ConvertReal* (value: вещ32): цел32;
	конст Exponent = 8; Significant = 23;
	перем result: цел32; перем exponent, i: целМЗ;
	нач
		(*! NaN code missing, we need an extra - machine specific - functionality for that  / cf. Streams.NaNCode *)
		если value = 0 то возврат 0 всё;
		result := 0; exponent := 0;
		если value < 0 то value := -value; result := арифмСдвиг (1, Exponent) всё;
		нцПока value < 1 делай value := value * 2; умень (exponent) кц;
		нцПока value >= 2 делай value := value / 2; увел (exponent) кц;
		value := value - 1; увел (result, арифмСдвиг (1, Exponent - 1) - 1 + exponent);
		нцДля i := 0 до Significant - 1 делай
			value := value * 2; увел (result, result);
			если value >= 1 то value := value - 1; увел (result) всё;
		кц;
		возврат result;
	кон ConvertReal;

	проц ConvertLongreal*(value: вещ64): цел64;
	конст Exponent = 11; Significant = 52;
	перем result: цел64; перем exponent, i: целМЗ;
	нач
		(*! NaN code missing, we need an extra - machine specific - functionality for that  / cf. Streams.NaNCode *)
		если value = 0 то возврат 0 всё;
		result := 0; exponent := 0;
		если value < 0 то value := -value; result := арифмСдвиг (1, Exponent) всё;
		нцПока value < 1 делай value := value * 2; умень (exponent) кц;
		нцПока value >= 2 делай value := value / 2; увел (exponent) кц;
		value := value - 1; увел (result, арифмСдвиг (1, Exponent - 1) - 1 + exponent);
		нцДля i := 0 до Significant - 1 делай
			value := value * 2; увел (result, result);
			если value >= 1 то value := value - 1; увел (result) всё;
		кц;
		возврат result;
	кон ConvertLongreal;

	проц ConvertToReal*(x: цел32): вещ32;
	перем result: вещ32; e,i: цел32;

		проц Bit(bit: цел32): булево;
		нач
			возврат нечётноеЛи¿(арифмСдвиг(x,-bit))
		кон Bit;
	нач
		result := 0; e := 0;
		нцДля i := 0 до 22 делай
			если Bit(i) то result := result + 1 всё; result := result / 2;
		кц;
		нцДля i :=  30 до 23 шаг -1 делай
			e := e*2; если Bit(i) то e := e+1 всё;
		кц;
		если e = 0FFH то (* NaN or Inf *)
			СТОП(200);
			(*! NaN code missing, we need an extra - machine specific - functionality for that  / cf. Streams.NaNCode *)
		аесли (result # 0) или (e#0) то
			result := result + 1;
			умень(e,127);
			нцПока e > 0 делай result := result *2; умень(e) кц;
			нцПока e < 0 делай result := result / 2; увел(e) кц;
			если Bit(31) то result := -result всё;
		всё;
		возврат result
	кон ConvertToReal;

	проц ConvertToLongreal*(x: цел64): вещ64;
	перем result: вещ64; e,i: цел32;

		проц Bit(bit: цел32): булево;
		нач
			возврат нечётноеЛи¿(арифмСдвиг(x,-bit))
		кон Bit;
	нач
		result := 0; e:= 0;
		нцДля i := 0 до 51 делай
			если Bit(i) то result := result + 1 всё; result := result / 2;
		кц;
		result := result + 1;
		нцДля i :=  62 до 52 шаг -1 делай
			e := e*2; если Bit(i) то e := e+1 всё;
		кц;
		если e = 7FFH то (* NaN or Inf *)
			СТОП(200)
			(*! NaN code missing, we need an extra - machine specific - functionality for that  / cf. Streams.NaNCode *)
		аесли (result # 0) или (e#0) то
			умень(e,1023);
			нцПока e > 0 делай result := result *2; умень(e) кц;
			нцПока e < 0 делай result := result / 2; увел(e) кц;
			если Bit(63) то result := -result всё;
		всё;
		возврат result
	кон ConvertToLongreal;

	проц NewFixup*(mode: целМЗ; fixupOffset: Unit; symbol: ObjectFile.Identifier; symbolOffset: размерМЗ; displacement: Unit; scale: Bits; fixupPattern: ObjectFile.FixupPatterns): Fixup;
	перем fixup: Fixup;
	нач
		нов(fixup,mode,fixupOffset,symbol,symbolOffset,displacement,scale,fixupPattern); возврат fixup
	кон NewFixup;

	проц NewBinarySection*(type: целМЗ; unit: Bits; конст name: Basic.SegmentedName; dump: булево; bigEndian: булево): Section;
	перем binarySection: Section;
	нач
		нов(binarySection,type,unit,name,dump,bigEndian); возврат binarySection
	кон NewBinarySection;

кон LisBinaryCode.

FoxBinaryCode.TestFixup
