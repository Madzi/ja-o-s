модуль AdaptiveHuffman;	(** AUTHOR GF; PUROSE "adaptive Huffman coding" *)

(* adaptive Huffman compression, Vitter's FGK algorithm *)

использует Потоки;

конст
	AlphabetSize = 256;		(* byte *)
	BlockSize* = 8*1024;
	ScaleLimit = 4*1024;

	Encode = 0;  Decode = 1;

тип
	BitReader = окласс
		перем
			in: Потоки.Чтец;
			curByte, curBit: цел32;

			проц &New( r: Потоки.Чтец );
			нач
				in := r;  curBit := -1;  curByte := 0
			кон New;

			проц Initialize;
			нач
				curBit := -1;  curByte := 0
			кон Initialize;

			проц Bit( ): цел32;
			перем
				bit: цел32;  ch: симв8;
			нач
				если curBit < 0 то
					in.чСимв8( ch );  curByte := кодСимв8( ch );  curBit := 7
				всё;
				bit := арифмСдвиг( curByte, -curBit ) остОтДеленияНа 2;
				умень( curBit );
				возврат bit
			кон Bit;

	кон BitReader;


тип
	BitWriter = окласс
		перем
			out: Потоки.Писарь;
			curByte, curBit: цел32;

			проц &New( w: Потоки.Писарь );
			нач
				out := w;  curBit := 0;  curByte := 0
			кон New;

			проц Bit( bit: цел32 );
			нач
				curByte := 2*curByte + bit;
				увел( curBit );
				если curBit > 7 то
					out.пСимв8( симв8ИзКода( curByte ) );  curByte := 0;  curBit := 0
				всё
			кон Bit;

			проц Finish;	(* flush last few bits *)
			нач
				нцПока curBit # 0 делай  Bit( 0 )  кц;
				out.ПротолкниБуферВПоток
			кон Finish;

	кон BitWriter;




тип
	HuffmanCoder = окласс
		тип
			Index = цел16;		(* 16-bit integer to keep the table small *)
			Pattern = цел16;
			Node = запись
				weight: цел16;		(* node weight	*)
				pattern: Pattern;		(* node pattern (if weight is even, leaf node)	*)
				up: Index;				(* next node up the tree	*)
				down: Index			(* pair of down nodes (if weight is odd, internal node)	*)
			кон;
		перем
			mode: цел8;	(* Encode, Decode *)
			in: BitReader;		(* input from archive *)
			out: BitWriter;	(* output to archive *)

			esc: Index;		(* the current escape node	*)
			root: Index;         (* the root of the tree	*)
			map: массив AlphabetSize из Index;			(* mapping of patterns to nodes	*)
			table: массив 2*AlphabetSize + 2 из Node;	(* the Huffman tree	*)



		проц &New( m: цел8;  input: Потоки.Чтец;  output: Потоки.Писарь );
		нач
			утв( m в {Encode, Decode} );
			mode := m;
			если mode = Encode то  нов( out, output )  иначе  нов( in, input )  всё;
		кон New;

		проц Initialize;
		перем  i: Index;
		нач
			root := 2*AlphabetSize + 2 - 1;
			нцДля i := 0 до root делай
				table[i].up := 0;  table[i].down := 0;  table[i].pattern := 0;  table[i].weight := 0
			кц;
			нцДля i := 0 до AlphabetSize - 1 делай  map[i]  := 0  кц;
			esc := root;
			если mode = Decode то  in.Initialize  всё
		кон Initialize;

		проц Finish;
		нач
			утв( mode = Encode );
			out.Finish  (* flush last few bits *)
		кон Finish;


		проц GetPattern(  ): Pattern;
		перем  i: цел16;  patt: Pattern;
		нач
			patt := 0;
			нцДля i := 0 до 7 делай
				если in.Bit() = 1 то  patt := patt + цел16(арифмСдвиг( 1, i ))  всё;
			кц;
			возврат patt
		кон GetPattern;

		проц PutPattern( patt: Pattern );
		перем i: цел32;
		нач
			нцДля i := 0 до 7 делай
				out.Bit( patt остОтДеленияНа 2 );  patt := patt DIV 2;
			кц
		кон PutPattern;


		(* split escape node to incorporate a new pattern *)
		проц AddPattern( patt: Pattern ): Index;
		перем  pair, node: Index;
		нач
			утв( esc > 1 );
			pair := esc;  node := esc - 1;  esc := esc - 2 ;

			table[pair].down := node;  table[pair].weight := 1;

			table[node].up := pair;
			table[node].down := 0;  table[node].weight := 0;  table[node].pattern := patt;

			table[esc].up := pair;
			table[esc].down := 0;  table[esc].weight := 0;

			map[patt] := node;

			возврат node;
		кон AddPattern;


		(* swap leaf to group leader position, return pattern's new node *)
		проц GroupLeader( node: Index ): Index;
		перем
			leader: Index;
			weight: цел32;
			patt, prev:  Pattern;
		нач
			weight := table[node].weight;  leader := node;
			нцПока (leader < root) и (weight = table[leader + 1].weight) делай  увел( leader )  кц;
			если leader # node то
				(* swap the leaf nodes *)
				patt := table[node].pattern;
				prev := table[leader].pattern;
				table[leader].pattern := patt;
				table[node].pattern := prev;
				map[patt] := leader;
				map[prev] := node;
			всё;
			возврат leader
		кон GroupLeader;


		(*	slide internal node up over all leaves of equal weight
			or exchange leaf with next smaller weight internal node.
			return node's new position	*)
		проц SlideNode( node: Index ): Index;
		перем  next: Index;  swap: Node;
		нач
			swap := table[node];  next := node + 1;
			(* if we're sliding an internal node, find the highest possible leaf to exchange with *)
			если нечётноеЛи¿( swap.weight ) то
				нцПока swap.weight > table[next+1].weight делай  увел( next )  кц
			всё;
			(* swap the two nodes *)
			table[node] := table[next];  table[next] := swap;
			table[next].up := table[node].up;  table[node].up := swap.up;
			(* repair the pattern map and tree structure *)
			если нечётноеЛи¿( swap.weight ) то
				table[swap.down].up := next;
				table[swap.down - 1].up := next;
				map[table[node].pattern] := node;
			иначе
				table[table[node].down - 1].up := node;
				table[table[node].down].up := node;
				map[swap.pattern] := next;
			всё;
			возврат next;
		кон SlideNode;


		(* increment pattern weight and re balance the tree. *)
		проц IncrementWeight( node: Index );
		перем  up: Index;
		нач
			(*	obviate swapping a parent with its child: increment the leaf and proceed directly to its parent.
				otherwise, promote leaf to group leader position in the tree	*)
			если table[node].up = node + 1 то
				увел( table[node].weight, 2 );  увел( node );
			иначе
				node := GroupLeader( node )
			всё;
			(*	increase the weight of each node and slide over any smaller weights ahead of it until reaching the root.
				internal nodes work upwards from their initial positions; while pattern nodes slide over first,
				then work up from their final positions.	*)
			увел( table[node].weight, 2 );  up := table[node].up;
			нцПока up # 0 делай
				нцПока table[node].weight > table[node + 1].weight делай  node := SlideNode( node)  кц;
				если нечётноеЛи¿( table[node].weight) то
					node := up
				иначе
					node := table[node].up
				всё;
				увел( table[node].weight, 2 );  up := table[node].up
			кц;
		кон IncrementWeight;


		(*  scale all weights and rebalance the tree,
		    zero weight nodes are removed from the tree	*)
		проц Scale;
		перем  node, prev: Index;  weight: цел16;

			проц Weight( idx: Index ): цел16;
			перем w: цел16;
			нач
				w := table[idx].weight;
				если нечётноеЛи¿( w ) то  возврат w - 1  иначе  возврат w  всё
			кон Weight;

		нач
			node := esc + 1;
			(* work up the tree from the escape node scaling weights *)
			нцПока node <= root делай
				weight := table[node].weight;
				если нечётноеЛи¿( weight ) то
					(* recompute the weight of internal nodes *)
					weight := Weight( table[node].down ) + Weight( table[node].down-1 ) + 1
				иначе
					(* remove zero weight leaves and remove them from the pattern map *)
					weight := weight DIV 2;
					если нечётноеЛи¿( weight ) то  умень( weight )  всё;
					если weight = 0 то
						map[table[node].pattern] := 0;  увел( esc, 2 );
					всё
				всё;
				(* slide the scaled node back down over any previous nodes with larger weights *)
				table[node].weight := weight;
				prev := node - 1;
				нцПока weight < table[prev].weight делай  неважно SlideNode( prev );  умень( prev )  кц;
				увел( node )
			кц;
			(* prepare a new escape node *)
			table[esc].down := 0;  table[esc].weight := 0;
		кон Scale;




		проц EncodeByte( ch: симв8 );
		перем
			code, bits: цел32;
			cur, node: Index;  patt: Pattern;
		нач
			patt := кодСимв8( ch );  node := map[patt];

			(* accumulate the code bits by working up from the node to the root	*)
			cur := node;  code := 0;  bits := 0;
			если cur = 0 то  cur := esc  всё;
			нцПока table[cur].up # 0 делай
				если нечётноеЛи¿( cur ) то  code := code*2 + 1  иначе  code := code*2  всё;
				увел( bits );  cur := table[cur].up
			кц;
			(* send the code *)
			нцПока bits > 0 делай
				out.Bit( code остОтДеленияНа 2 );  code := code DIV 2;
				умень( bits )
			кц;
			если node = 0 то
				(* send the new pattern and incorporate it into the tree *)
				PutPattern( patt );  node := AddPattern( patt );
			всё;
			IncrementWeight( node );
		кон EncodeByte;


		проц ExtractByte( ): симв8;
		перем
			node, down: Index;
			patt: Pattern;
		нач
			(* work down the tree from the root until reaching either a leaf or the escape node *)
			node := root;  down := table[node].down;
			нцПока down # 0 делай
				если in.Bit( ) = 1 то  node := down - 1  иначе  node := down  всё;
				down := table[node].down;
			кц;
			если node = esc то
				(* add the new pattern to the tree *)
				patt := GetPattern( );  node := AddPattern( patt )
			иначе
				patt := table[node].pattern
			всё;
			IncrementWeight( node );
			возврат симв8ИзКода( patt );
		кон ExtractByte;

	кон HuffmanCoder;



тип
	Encoder* =окласс
		перем
			huff: HuffmanCoder;

			проц & New*( archive: Потоки.Писарь );
			нач
				нов( huff, Encode, НУЛЬ, archive );
			кон New;

			проц CompressBlock*( конст source: массив из симв8; len: размерМЗ );
			перем  i: размерМЗ;
			нач
				huff.Initialize;
				i := 0;
				нцПока i < len делай
					huff.EncodeByte( source[i] );  увел( i );
					если (i остОтДеленияНа ScaleLimit = 0) и ((len-i) >= ScaleLimit) то  huff.Scale  всё
				кц;
				huff.Finish
			кон CompressBlock;

	кон Encoder;



тип
	Decoder* =окласс
		перем
			huff: HuffmanCoder;

			проц & New*( archive: Потоки.Чтец );
			нач
				нов( huff, Decode, archive, НУЛЬ );
			кон New;

			проц ExtractBlock*( перем buf: массив из симв8; len: цел32 );
			перем  i: цел32;
			нач
				huff.Initialize;
				i := 0;
				нцПока i < len делай
					buf[i] := huff.ExtractByte( );  увел( i );
					если (i остОтДеленияНа ScaleLimit = 0) и ((len - i) >= ScaleLimit) то  huff.Scale  всё
				кц;
			кон ExtractBlock;

	кон Decoder;


кон AdaptiveHuffman.


