модуль MenuPages; (** AUTHOR "staubesv"; PURPOSE "Generate Menupages"; *)

использует
	Потоки, Commands, Files, WMComponents, WMStandardComponents;

конст
	MinNofColumns = 4;
	ColumnWidth = 120;

проц GetColumn() : WMStandardComponents.Panel;
перем column : WMStandardComponents.Panel;
нач
	нов(column);
	column.SetName("Standard:Panel");
	column.alignment.Set(WMComponents.AlignLeft);
	column.bounds.SetWidth(ColumnWidth);
	возврат column;
кон GetColumn;

проц GetButton(конст caption, command : массив из симв8; color, colorHover : цел32) : WMStandardComponents.Button;
перем button : WMStandardComponents.Button; syscmd : WMStandardComponents.SystemCommand;
нач
	нов(button);
	button.SetName("Standard:Button");
	button.caption.SetAOC(caption);
	button.alignment.Set(WMComponents.AlignTop);
	button.onClickHandler.SetAOC("X Run");
	если (caption = "") то button.enabled.Set(ложь); всё;
	если (color # 0) то button.clDefault.Set(color); всё;
	если (colorHover # 0) то button.clHover.Set(colorHover); всё;
	нов(syscmd);
	syscmd.SetName("Standard:SystemCommand");
	syscmd.id.SetAOC("X");
	syscmd.commandString.SetAOC(command);
	button.AddContent(syscmd);
	возврат button;
кон GetButton;

проц GetColor(in : Потоки.Чтец) : цел32;
перем ch : симв8; color : цел32;
нач
	in.ПропустиБелоеПоле;
	ch := in.ПодглядиСимв8();
	если ("0" <= ch) и (ch <= "9") то
		in.чЦел32(color, истина);
	иначе
		color := 0;
	всё;
	возврат color;
кон GetColor;

проц GetEntry(in : Потоки.Чтец; перем name, command : массив из симв8; перем color, colorHover : цел32);
нач
	name := ""; command := "";
	color := 0; colorHover := 0;
	in.ПропустиБелоеПоле; in.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);
	in.ПропустиБелоеПоле; in.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(command);
	color := GetColor(in);
	colorHover := GetColor(in);
кон GetEntry;

проц AddEntries(menuPage : WMStandardComponents.Panel; in : Потоки.Чтец);
перем
	column : WMStandardComponents.Panel; nofColumns : цел32;
	entryName : массив 64 из симв8;
	commandStr : массив 512 из симв8;
	color, colorHover : цел32;
нач
	nofColumns := 0;
	нцПока ((in.кодВозвратаПоследнейОперации = Потоки.Успех) и (in.ПодглядиСимв8() # 0X)) или (nofColumns < MinNofColumns) делай
		column := GetColumn();
		menuPage.AddContent(column);
		GetEntry(in, entryName, commandStr, color, colorHover);
		column.AddContent(GetButton(entryName, commandStr, color, colorHover));
		GetEntry(in, entryName, commandStr, color, colorHover);
		column.AddContent(GetButton(entryName, commandStr, color, colorHover));
		in.ПропустиБелоеПоле;
		увел(nofColumns);
	кц;
кон AddEntries;

проц Generate*(context : Commands.Context); (** filename menuName {Entries} ~ *)
перем
	filename : Files.FileName; file : Files.File; writer : Files.Writer;
	menuName : массив 64 из симв8;
	menuPage : WMStandardComponents.Panel;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(menuName);
	если (menuName # "") то
		нов(menuPage);
		menuPage.SetName("Standard:Panel");
		menuPage.fillColor.Set(0);
		menuPage.SetAttributeValue("caption", menuName);
		AddEntries(menuPage, context.arg);
		file := Files.New(filename);
		если (file # НУЛЬ) то
			Files.OpenWriter(writer, file, 0);
			menuPage.Write(writer, НУЛЬ, 0);
			writer.ПротолкниБуферВПоток;
			Files.Register(file);
			context.out.пСтроку8("Generated menu page "); context.out.пСтроку8(filename); context.out.пВК_ПС;
		иначе
			context.out.пСтроку8("Could not generate file '"); context.out.пСтроку8(filename); context.out.пВК_ПС;
		всё;
	иначе
		context.out.пСтроку8("Expected menu name argument"); context.out.пВК_ПС;
	всё;
кон Generate;

кон MenuPages.

System.Free MenuPages ~

MenuPages.Generate MenuPage15.xml TestMenu
	Hello "System.Show Hello"
	World "System.Show World"
	"" ""
	News "System.Show News"
	"" ""
	"" ""
	"" ""
	Reboot "System.Reboot" 0FF000060H 0FF0000FFH
~

