(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль CalcGrunwald;   (** AUTHOR "adf"; PURPOSE "Grünwald-Letnikov algorithms for the fractional calculus"; *)

использует NbrInt, NbrRe, MathRe, CalcFn;

конст
	(** Status of integration, i.e., admissible values for the returned parameter 'res'. *)
	OKay* = 0;  MaxSubDivReached* = 1;  Oscillation* = 2;

	(**  Arguments of differ-integration are:
	f	the function being differintegrated,
	x	the argument of f, also the upper limit of differintegration,
	order	(> 0)  the fractional order of differintegration, i.e., a,
	error	the requested and achieved error tolerances,
	res	status of the differintegration.
	 *)

перем
	(** Upper bound for the number of subintervals for integration/differentiation. *)
	MaxIntervals-: NbrInt.Integer;

	проц Grunwald( f: CalcFn.ReArg;  a, b, order: NbrRe.Real;  перем error, result: NbrRe.Real;  перем res: NbrInt.Integer );
	(* Use Grünwald-Letnikov definition for fractional-order differintegration. Requires  a < b.
	f	the argument of differintegration,
	a	the lower limit of differintegration,
	b	the upper limit of differintegration,
	order	the fractional order of differintegration, i.e., a,
			if  order > 0  then it is a fractional differentiation
			if  order < 0  then it is a fractional integration
	error	the requested and achieved error tolerances.
		 *)
	перем i, im1, ip1, n: NbrInt.Integer;  absError, absX, dx, errorLast, fn, fnm1, last, tol: NbrRe.Real;
		fns, save: укль на массив из NbrRe.Real;
	нач
		(* Implements the G2 algorithm of K. B. Oldham and J. Spanier, "The Fractional Calculus", 1974, pg. 138. *)
		result := 0;  n := 4;  tol := NbrRe.Max( error, 100 * NbrRe.Epsilon );  нов( fns, 3 );  dx := (b - a) / 2;
		нцДля i := 0 до 2 делай fns[i] := f( b - i * dx ) кц;
		error := NbrRe.MaxNbr;
		нц
			save := fns;  fns := НУЛЬ;  нов( fns, n + 1 );  dx := (b - a) / n;
			нцДля i := 0 до n делай
				если NbrInt.Odd( i ) то fns[i] := f( b - i * dx ) иначе fns[i] := save[i DIV 2] всё
			кц;
			last := result;  result := 0;
			нцДля i := n - 1 до 1 шаг -1 делай
				im1 := i - 1;  ip1 := i + 1;
				(* G2 algorithm, pg. 138 in Oldham & Spanier. *)
				fn := order * (fns[ip1] - 2 * fns[i] + fns[im1]) / 2;  fn := order * ((fns[ip1] - fns[im1]) - fn) / 4;
				fn := fns[i] - fn;  result := (result + fn) * (im1 - order) / i
			кц;
			fnm1 := f( b + dx );  fn := order * (fns[1] - 2 * fns[0] + fnm1) / 2;  fn := order * ((fns[1] - fnm1) - fn) / 4;
			result := (result + fns[0] - fn) * MathRe.Power( n / b, order );  n := 2 * n;  errorLast := error;
			absError := NbrRe.Abs( result - last );  absX := NbrRe.Abs( result );
			если absX > 1 то error := absError / absX иначе error := NbrRe.Max( absError, 50 * NbrRe.Epsilon * absX ) всё;
			если error < tol то res := OKay;  прервиЦикл всё;
			если n = MaxIntervals то res := MaxSubDivReached;  прервиЦикл всё;
			если error > errorLast то result := last;  res := Oscillation;  прервиЦикл всё
		кц
	кон Grunwald;

	(** Computes a Riemann-Liouville fractional-order derivative.
			Daf(x) = DnIaf(x) = (1/G(n-a)) dn/dxn x0x (x-y)n-1-a f(y) dy,   n-1 < a # n,  n N @,  a N !+  *)
	проц SolveD*( f: CalcFn.ReArg;  x, order: NbrRe.Real;  перем error: NbrRe.Real;  перем res: NbrInt.Integer ): NbrRe.Real;
	перем result, zero: NbrRe.Real;
	нач
		error := 0;  result := 0;  res := OKay;  zero := 0;
		если f # НУЛЬ то
			если zero # x то
				если order < zero то order := -order всё;
				если zero < x то Grunwald( f, zero, x, order, error, result, res )
				иначе Grunwald( f, x, zero, order, error, result, res );  result := -result
				всё
			всё
		иначе result := 0
		всё;
		возврат result
	кон SolveD;

	(** Computes a Riemann-Liouville fractional-order integral.
			 Iaf(x) = (1/G(a)) x0x (x-y)a-1 f(y) dy,  a N !+  *)
	проц SolveI*( f: CalcFn.ReArg;  x, order: NbrRe.Real;  перем error: NbrRe.Real;  перем res: NbrInt.Integer ): NbrRe.Real;
	перем result, zero: NbrRe.Real;
	нач
		error := 0;  result := 0;  res := OKay;  zero := 0;
		если f # НУЛЬ то
			если zero # x то
				если order > zero то order := -order всё;
				если zero < x то Grunwald( f, zero, x, order, error, result, res )
				иначе Grunwald( f, x, zero, order, error, result, res );  result := -result
				всё
			всё
		всё;
		возврат result
	кон SolveI;

нач
	MaxIntervals := NbrInt.MaxNbr
кон CalcGrunwald.
