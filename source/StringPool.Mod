модуль StringPool;	(** prk  **) (** AUTHOR "prk"; PURPOSE "StringPool"; *)

использует
		НИЗКОУР,
		ЛогЯдра;	(*debug only*)

(**
	StringPool stores strings of any length. Equal strings have the same index.
	String with index 0 is guaranteed to be the empty string.
*)

конст
	(* Module Configuration *)
	StringPoolSize0 = 1024*256;	(* initial string pool size *)
	HashTableSize0 = 1024;	(* initial hash table size *)
	ТакойСтрокиНет* = -1; (* значение, показывающее отсутствие ключа. Ни у какой реальной строки такого ключа получиться не может *)

тип
	(* Helper Structures *)
	Index* = размерМЗ;
	StringPool = укль на массив из симв8;

перем
	pool: StringPool;
	poolLen: размерМЗ;
	poolIndex: укль на массив из Index;
	poolIndexSize: размерМЗ;	(* LEN(poolIndex)-1 *)

	ALastGet,
	AStrings, AGetString, ACompareString, ACompareString0, AStringCmpHit, ASearchHits, ASearchMisses: размерМЗ;
	AInsertHashRetries: массив 10 из размерМЗ;
	ASearchHashRetries: массив 10 из размерМЗ;

	(** ----------------- String Pool functions ------------------ *)

	(* Hash - Return an Hash value in [0, poolIndexSize[ *)

	проц Hash(конст str: массив из симв8): размерМЗ;
		перем i, h: размерМЗ;  ch: симв8;
	нач
		i := 0; ch := str[0]; h := 0;
		нцПока ch # 0X делай
			h := размерМЗ(мнвоНаБитахМЗ(вращБит(h, 7)) / мнвоНаБитахМЗ(кодСимв8(ch)));
			увел(i); ch := str[i]
		кц;
		h := h остОтДеленияНа poolIndexSize;
		возврат h
	кон Hash;

	(* GrowPool - increase string pool size *)

	проц GrowPool;
		перем new: StringPool;
	нач
		нов(new, 2*длинаМассива(pool));
		НИЗКОУР.копируйПамять(адресОт(pool[0]), адресОт(new[0]), длинаМассива(pool));
		pool := new
	кон GrowPool;

	(* GrowHashTable - Increase Hash table size and recompute all entries *)

	проц GrowHashTable;
		перем i, t, h: размерМЗ; idx, idx0: Index; ch: симв8;
	нач
		t := (poolIndexSize+1)*2;
		нов(poolIndex, t);
		нцДля i := 0 до t-1 делай  poolIndex[i] := ТакойСтрокиНет кц;
		нцДля i := 0 до длинаМассива(AInsertHashRetries)-1 делай AInsertHashRetries[i] := 0 кц;
		poolIndexSize := t-1;

		(* re-fill the hash-table *)
		idx := 0;
		нцПока idx < poolLen делай
			(*hash*)
			idx0 := idx; h := 0;
			ch := pool[idx];
			нцПока ch # 0X делай
				h :=размерМЗ(мнвоНаБитахМЗ(вращБит(h, 7)) / мнвоНаБитахМЗ(устарПреобразуйКБолееШирокомуЦел(кодСимв8(ch))));
				увел(idx); ch := pool[idx]
			кц;
			h := h остОтДеленияНа poolIndexSize;
			увел(idx);	(*skip 0X*)

			i := 0;
			нцПока poolIndex[h] # ТакойСтрокиНет делай
				увел(i);
				увел(h);
				если h >= poolIndexSize то  умень(h, poolIndexSize)  всё
			кц;
			если i >= длинаМассива(AInsertHashRetries) то i := длинаМассива(AInsertHashRetries)-1 всё;
			увел(AInsertHashRetries[i]);

			poolIndex[h] := idx0
		кц
	кон GrowHashTable;

	(** GetString - Get a string from the string pool *)

	проц GetString*(index: Index; перем str: массив из симв8);
		перем i: размерМЗ; ch: симв8;
	нач
		ALastGet := index;
		увел(AGetString);
		i := 0;
		нцДо
			ch := pool[index+i]; str[i] := ch; увел(i)
		кцПри ch = 0X
	кон GetString;

	(* AddToPool - Add a string to the pool *)

	проц AddToPool(перем index: Index; конст str: массив из симв8);
	перем i: размерМЗ; ch: симв8;
	нач
		увел(AStrings);
		если длинаМассива(str) > длинаМассива(pool) - poolLen то GrowPool всё;
		i := 0; index := poolLen;
		нцДо
			ch := str[i]; pool[poolLen+i] := ch; увел(i)
		кцПри ch = 0X;
		увел(poolLen, i);
	кон AddToPool;

	(** GetIndex - Retrieve a string from the pool, add if not present *)

	проц GetIndex*(конст str: массив из симв8;  перем index: Index);
		перем i, h: размерМЗ; idx: Index;
	нач {единолично}
		если AStrings > poolIndexSize DIV 4 то GrowHashTable всё;
		h := Hash(str);
		idx := poolIndex[h];
		i := 0;
		нц
			если (idx = ТакойСтрокиНет) то	(* miss *)
				увел(ASearchMisses);
				если i >= 10 то i := 9 всё;
				увел(AInsertHashRetries[i]);
				AddToPool(index, str);
				poolIndex[h] := index;
				прервиЦикл
			аесли (CompareString0(idx, str) = 0) то
				увел(ASearchHits);
				если i >= длинаМассива(ASearchHashRetries) то i := длинаМассива(ASearchHashRetries)-1 всё;
				увел(ASearchHashRetries[i]);
				index := idx;
				прервиЦикл
			всё;
			увел(i);
			утв(i < poolIndexSize);
			увел(h);
			если h >= poolIndexSize то умень(h, poolIndexSize) всё;
			idx := poolIndex[h]
		кц;
	кон GetIndex;

	проц GetIndex1*(конст str: массив из симв8): Index;
		перем idx: Index;
	нач
		GetIndex(str, idx); возврат idx
	кон GetIndex1;

	(** Compare two strings
		CompareString = 0 <==> Str(index1) = Str(index2)
		CompareString < 0 <==> Str(index1) < Str(index2)
		CompareString > 0 <==> Str(index1) > Str(index2)
	*)

	проц CompareString*(index1, index2: Index): целМЗ;
		перем  ch: симв8;
	нач
		увел(ACompareString);
		если index1 = index2 то
			увел(AStringCmpHit); возврат 0
		всё;
		ch := pool[index1];
		нцПока (ch # 0X) и (ch = pool[index2]) делай
			увел(index1); увел(index2);
			ch := pool[index1]
		кц;
		возврат кодСимв8(ch) - кодСимв8(pool[index2])
	кон CompareString;

	проц CompareString0*(index: Index;  конст str: массив из симв8): целМЗ;
		перем  ch1, ch2: симв8; i: размерМЗ;
	нач
		увел(ACompareString0); i := 0;
		нцДо
			ch1 := pool[index+i];
			ch2 := str[i];
			увел(i)
		кцПри (ch1 = 0X) или (ch1 # ch2);
		возврат кодСимв8(ch1) - кодСимв8(ch2)
	кон CompareString0;

(*
	optimized version (no index checks)

	PROCEDURE CompareString0*(index: Index;  CONST str: ARRAY OF CHAR): INTEGER;
		VAR  ch1, ch2: CHAR; adr1, adr2: ADDRESS; i: SIZE;
	BEGIN
		INC(ACompareString0);
		adr1 := ADDRESSOF(pool[index]);
		adr2 := ADDRESSOF(str[0]);
		REPEAT
			SYSTEM.GET(adr1+i, ch1);
			SYSTEM.GET(adr2+i, ch2);
			INC(i)
		UNTIL (ch1 = 0X) OR (ch1 # ch2);

		RETURN ORD(ch1) - ORD(ch2)
	END CompareString0;
*)

	проц DumpPool*;
		перем i: размерМЗ; ch: симв8;
	нач
		ЛогЯдра.пСтроку8("StringPool.Dump:");
		ЛогЯдра.пСтроку8("size = "); ЛогЯдра.пЦел64(poolLen,1);
		ЛогЯдра.пВК_ПС;
		ЛогЯдра.пЦел64(0, 4); ЛогЯдра.пСтроку8(": ");
		i := 0;
		нцПока i < poolLen делай
			ch := pool[i]; увел(i);
			если ch = 0X то
				ЛогЯдра.пВК_ПС; ЛогЯдра.пЦел64(i, 4); ЛогЯдра.пСтроку8(": ");
			иначе
				ЛогЯдра.пСимв8(ch)
			всё
		кц;
	кон DumpPool;

	проц Init;
		перем i: размерМЗ; str: массив 2 из симв8;
	нач
		нов(pool, StringPoolSize0);
		нов(poolIndex, HashTableSize0);
		poolIndexSize := HashTableSize0-1;
		нцДля i := 0 до poolIndexSize делай poolIndex[i] := ТакойСтрокиНет кц;
		str := "";
		AddToPool(i, str);
	кон Init;

нач
	Init;
кон StringPool.

(*
ToDo:
* store string len in the pool, use it when retrieving (SYS.MOVE). In this case entries should be aligned

Log:
	15.03.02	prk	ALastGet added; DumpPool improved
	08.02.02	prk	use Aos instead of Oberon modules
	27.06.01	prk	first version
*)
