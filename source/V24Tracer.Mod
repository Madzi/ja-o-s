модуль V24Tracer; (** AUTHOR "TF/AFI"; PURPOSE "Man in the middle attack for Serial Ports" *)

использует
	Commands, Потоки, Modules, ЛогЯдра, Serials;

тип
	SerialPortTracer = окласс
	перем
		port : Serials.Port;
		seq : цел32;	(* Sequence number of this serial port tracer *)
		ch : симв8;
		V24writer : Потоки.Писарь;	(* Writer used by this serial port tracer *)
		alive : булево;
		res : целМЗ;

	проц &Init*(seqNo, portNo, bps : цел32);
	нач
		port := Serials.GetPort(portNo);
		если res = 0 то
			port.Open(bps, 8, Serials.ParNo, Serials.Stop1, res);
			Потоки.НастройПисаря(V24writer, port.ЗапишиВПоток);
			seq := seqNo
		всё;
	кон Init;

	проц Close;
	нач
		alive := ложь;
		port.Закрой()
	кон Close;

	нач {активное}
		alive := истина;
		нцПока alive делай
			port.ReceiveChar(ch, res);
			если res = Serials.Ok то
				если fine то	(* Display the origin of each character, its hex value and its printable value *)
					ЛогЯдра.пВК_ПС; ЛогЯдра.пЦел64(seq, 0); ЛогЯдра.пСтроку8(" --> : ");
					ЛогЯдра.п16ричное(кодСимв8(ch), -2); ЛогЯдра.пСимв8("X");
					если кодСимв8(ch) > 32 то ЛогЯдра.пСтроку8("   "); ЛогЯдра.пСимв8(ch) всё
				иначе	(* Display a stream of characters with the same origin. In order to obtain this result,
								it is preferable, when a modem is tested, to operate it without echo - use an ATE0 command. *)
					если seq # activeseq то
						activeseq := seq;
						ЛогЯдра.пВК_ПС; ЛогЯдра.пЦел64(seq, 0); ЛогЯдра.пСтроку8(" --> : ")
					всё;
					если кодСимв8(ch) > 32 то ЛогЯдра.пСимв8(ch)
					аесли (ch = " ") или (ch = 0DX)или (ch = 0AX) то ЛогЯдра.пСимв8(" ")
					иначе если ch # 0AX то ЛогЯдра.п16ричное(кодСимв8(ch), -2); ЛогЯдра.пСимв8("X") всё
					всё;
				всё;
				(* Send the character just received to the other port *)
				tracingport[(seq + 1) остОтДеленияНа 2].V24writer.пСимв8(ch);
				tracingport[(seq + 1) остОтДеленияНа 2].V24writer.ПротолкниБуферВПоток();
			иначе
				alive := ложь;
				ЛогЯдра.пСтроку8("Character in error "); ЛогЯдра.пСимв8(ch); ЛогЯдра.пЦел64(res, 4); ЛогЯдра.пВК_ПС;
			всё;
		кц;
		ЛогЯдра.пСтроку8("Tracer "); ЛогЯдра.пЦел64(seq, 0); ЛогЯдра.пСтроку8(" terminated."); ЛогЯдра.пВК_ПС
	кон SerialPortTracer;

перем running, fine : булево;
	tracingport : массив 2 из SerialPortTracer;
	activeseq : цел32;

проц SetMode*(context : Commands.Context);
перем name : массив 100 из симв8;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) и (name = "fine") то fine := истина иначе fine := ложь всё;
	context.out.пСтроку8("Tracing mode "); context.out.пСтроку8(name); context.out.пВК_ПС;
кон SetMode;

проц Enable*(context : Commands.Context);
перем inPort, outPort, baud : цел32;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(inPort, ложь);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(outPort, ложь);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(baud, ложь);
	нов(tracingport[0], 0, inPort, baud);	(* Instantiate 2 active tracers, which will be used in alternance *)
	нов(tracingport[1], 1, outPort, baud);
	running := истина;
	context.out.пСтроку8("Tracing active ... "); context.out.пВК_ПС;
	activeseq := -1;
кон Enable;

проц Finalize;
кон Finalize;

проц Disable*;
нач
	tracingport[0].Close();
	tracingport[1].Close();
кон Disable;

нач
	fine := истина;	(* Default *)
	Modules.InstallTermHandler(Finalize)
кон V24Tracer.

Use:
	1. Add the device to be traced to serialport 0
	2. Add the machine that knows the device to serialport 1 (eg. Windows/Linux/Unix/... with driver)
	3. Start the tracer, guessing the connection settings eg. baud rate (coarse mode is better in many cases)
	4. Start using the device
	5. Look at the data in the kernel log
	6. If output looks strange, disable the tracer, reguess the connection settings and goto 3

V24Tracer.Enable 0 1 9600 ~ InPort OutPort bps
V24Tracer.SetMode coarse ~
V24Tracer.SetMode fine ~
V24Tracer.Disable ~
System.Free V24Tracer ~

