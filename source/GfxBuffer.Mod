(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль GfxBuffer; (** portable *)	(* eos  *)
(** AUTHOR "eos"; PURPOSE "Raster contexts rendering into background buffers"; *)

	(*
		10.12.98 - first version; derived from GfxDev
		25.8.99 - replaced GfxMaps with Images/GfxImages
		10.8.99 - scratched SetPoint, added Close method
		13.02.2000 - new get/set clip methods
		10.05.2000 - Rect now ignores empty rectangles
	*)


	использует
		Images := Raster, GfxMatrix, GfxImages, GfxRegions, Gfx, GfxRaster;


	тип
		Context* = укль на ContextDesc;
		ContextDesc* = запись (GfxRaster.ContextDesc)
			orgX*, orgY*: вещ32;	(** origin of default coordinate system **)
			scale*: вещ32;	(** default scale factor **)
			bgCol*: Gfx.Color;	(** background color for erasing **)
			img*: Images.Image;	(** target buffer **)
			pix: Images.Pixel;
			compOp:цел8; (* composition operation for raster device *)
		кон;

		RegData = запись (GfxRegions.EnumData)
			dx, dy: цел16;
			bc: Context;
			mode: Images.Mode;
		кон;


	перем
		Methods: Gfx.Methods;


	(*--- Rendering ---*)

	проц Color (llx, lly, urx, ury: цел16; перем data: GfxRegions.EnumData);
		перем bc: Context; mode: Images.Mode;
	нач
		bc := data(RegData).bc;
		Images.InitMode(mode, bc.compOp);
		Images.Fill(bc.img, llx, lly, urx, ury, bc.pix, mode)
	кон Color;

	проц Tile (llx, lly, urx, ury: цел16; перем data: GfxRegions.EnumData);
		перем bc: Context;
	нач
		просейТип data: RegData делай
			bc := data.bc;
			Images.FillPattern(bc.pat.img, bc.img, llx, lly, urx, ury, data.dx, data.dy, data.mode)
		всё
	кон Tile;

	проц Dot (rc: GfxRaster.Context; x, y: цел32);
		перем bc: Context; px, py: размерМЗ; mode: Images.Mode;
	нач
		если (rc.clipState = GfxRaster.In) или
			(rc.clipState = GfxRaster.InOut) и GfxRegions.RectInside(цел16(x), цел16(y), цел16(x+1), цел16(y+1), rc.clipReg)
		то
			bc := rc(Context);
			если bc.pat = НУЛЬ то
				Images.InitMode(mode, bc.compOp);
				Images.Put(bc.img, цел16(x), цел16(y), bc.pix, mode)
			иначе
				px := (x - округлиВниз(bc.orgX + bc.pat.px + 0.5)) остОтДеленияНа bc.pat.img.width;
				py := (y - округлиВниз(bc.orgY + bc.pat.py + 0.5)) остОтДеленияНа bc.pat.img.height;
				Images.InitModeColor(mode, Images.srcOverDst, bc.col.r, bc.col.g, bc.col.b);
				Images.Copy(bc.pat.img, bc.img, px, py, px+1, py+1, цел16(x), цел16(y), mode)
			всё
		всё
	кон Dot;

	проц Rect (rc: GfxRaster.Context; llx, lly, urx, ury: цел32);
		перем bc: Context; data: RegData; mode: Images.Mode;
	нач
		если (rc.clipState # GfxRaster.Out) и (llx < urx) и (lly < ury) то
			bc := rc(Context);
			если bc.pat = НУЛЬ то
				если rc.clipState = GfxRaster.In то
					Images.InitMode(mode, bc.compOp);
					Images.Fill(bc.img, устарПреобразуйКБолееУзкомуЦел(llx), устарПреобразуйКБолееУзкомуЦел(lly), устарПреобразуйКБолееУзкомуЦел(urx), устарПреобразуйКБолееУзкомуЦел(ury), bc.pix, mode)
				иначе
					data.bc := bc;
					GfxRegions.Enumerate(bc.clipReg, устарПреобразуйКБолееУзкомуЦел(llx), устарПреобразуйКБолееУзкомуЦел(lly), устарПреобразуйКБолееУзкомуЦел(urx), устарПреобразуйКБолееУзкомуЦел(ury), Color, data)
				всё
			иначе
				data.bc := bc;
				data.dx := устарПреобразуйКБолееУзкомуЦел(округлиВниз(bc.orgX + bc.pat.px + 0.5));
				data.dy := устарПреобразуйКБолееУзкомуЦел(округлиВниз(bc.orgY + bc.pat.py + 0.5));
				Images.InitModeColor(data.mode, Images.srcOverDst, bc.col.r, bc.col.g, bc.col.b);
				GfxRegions.Enumerate(bc.clipReg, устарПреобразуйКБолееУзкомуЦел(llx), устарПреобразуйКБолееУзкомуЦел(lly), устарПреобразуйКБолееУзкомуЦел(urx), устарПреобразуйКБолееУзкомуЦел(ury), Tile, data)
			всё
		всё
	кон Rect;

	проц SetColPat (rc: GfxRaster.Context; col: Gfx.Color; pat: Gfx.Pattern);
		перем bc: Context;
	нач
		bc := rc(Context);
		bc.col := col; bc.pat := pat;
		Images.SetRGBA(bc.pix, col.r, col.g, col.b, col.a)
	кон SetColPat;


	(*--- Methods ---*)

	проц ResetCTM (ctxt: Gfx.Context);
		перем bc: Context;
	нач
		bc := ctxt(Context);
		GfxMatrix.Translate(GfxMatrix.Identity, bc.orgX, bc.orgY, bc.ctm);
		GfxMatrix.Scale(bc.ctm, bc.scale, bc.scale, bc.ctm)
	кон ResetCTM;

	проц ResetClip (ctxt: Gfx.Context);
		перем bc: Context;
	нач
		bc := ctxt(Context);
		GfxRaster.ResetClip(bc);
		GfxRegions.SetToRect(bc.clipReg, 0, 0, цел16(bc.img.width), цел16(bc.img.height))
	кон ResetClip;

	проц Image (ctxt: Gfx.Context; x, y: вещ32; img: Images.Image; перем filter: GfxImages.Filter);
		перем bc: Context; m: GfxMatrix.Matrix; dx, dy, llx, lly, urx, ury: цел16; col: Images.Pixel;
	нач
		bc := ctxt(Context);
		GfxMatrix.Translate(bc.ctm, x, y, m);
		dx := устарПреобразуйКБолееУзкомуЦел(округлиВниз(m[2, 0] + 0.5));
		dy := устарПреобразуйКБолееУзкомуЦел(округлиВниз(m[2, 1] + 0.5));
		col := filter.col;
		Images.SetModeColor(filter, bc.fillCol.r, bc.fillCol.g, bc.fillCol.b);
		если (filter.hshift # GfxImages.hshift) и (dx + 0.1 < m[2, 0]) и (m[2, 0] < dx + 0.9) или
			(filter.vshift # GfxImages.vshift) и (dy + 0.1 < m[2, 1]) и (m[2, 1] < dy + 0.9) или
			GfxMatrix.Scaled(m) или
			GfxMatrix.Rotated(m)
		то
			GfxImages.Transform(img, bc.img, m, filter)
		иначе
			llx := 0; lly := 0; urx := цел16(img.width); ury := цел16(img.height);
			GfxRegions.ClipRect(llx, lly, urx, ury, bc.clipReg.llx - dx, bc.clipReg.lly - dy, bc.clipReg.urx - dx, bc.clipReg.ury - dy);
			если bc.clipReg.llx >  dx то dx := bc.clipReg.llx всё;
			если bc.clipReg.lly > dy то dy := bc.clipReg.lly всё;
			если dx + urx > bc.img.width то urx := цел16(bc.img.width - dx) всё;
			если dy + ury > bc.img.height то ury := цел16(bc.img.height - dy) всё;
			если dx < 0 то llx := -dx; dx := 0 всё;
			если dy < 0 то lly := -dy; dy := 0 всё;
			если (llx < urx) и (lly < ury) то
				если ~GfxRegions.RectEmpty(llx, lly, urx, ury) то
					Images.Copy(img, bc.img, llx, lly, urx, ury, dx, dy, filter)
				всё
			всё
		всё;
		Images.SetModeColor(filter, кодСимв8(col[Images.r]), кодСимв8(col[Images.g]), кодСимв8(col[Images.b]))
	кон Image;

	проц InitMethods;
		перем do: Gfx.Methods;
	нач
		нов(do); Methods := do;
		do.reset := Gfx.DefResetContext;
		do.resetCTM := ResetCTM; do.setCTM := Gfx.DefSetCTM; do.translate := Gfx.DefTranslate;
		do.scale := Gfx.DefScale; do.rotate := Gfx.DefRotate; do.concat := Gfx.DefConcat;
		do.resetClip := ResetClip; do.getClipRect := GfxRaster.GetClipRect;
		do.getClip := GfxRaster.GetClip; do.setClip := GfxRaster.SetClip;
		do.setStrokeColor := Gfx.DefSetStrokeColor; do.setStrokePattern := Gfx.DefSetStrokePattern;
		do.setFillColor := Gfx.DefSetFillColor; do.setFillPattern := Gfx.DefSetFillPattern;
		do.setLineWidth := Gfx.DefSetLineWidth; do.setDashPattern := Gfx.DefSetDashPattern;
		do.setCapStyle := Gfx.DefSetCapStyle; do.setJoinStyle := Gfx.DefSetJoinStyle;
		do.setStyleLimit := Gfx.DefSetStyleLimit; do.setFlatness := Gfx.DefSetFlatness;
		do.setFont := Gfx.DefSetFont; do.getWidth := Gfx.DefGetStringWidth;
		do.begin := GfxRaster.Begin; do.end := GfxRaster.End;
		do.enter := GfxRaster.Enter; do.exit := GfxRaster.Exit; do.close := GfxRaster.Close;
		do.line := GfxRaster.Line; do.arc := GfxRaster.Arc; do.bezier := GfxRaster.Bezier;
		do.show := GfxRaster.Show;
		do.flatten := Gfx.DefFlatten; do.outline := Gfx.DefOutline;
		do.render := GfxRaster.Render;
		do.rect := GfxRaster.Rect; do.ellipse := GfxRaster.Ellipse;
		do.image := Image; do.newPattern := Gfx.DefNewPattern;
	кон InitMethods;


	(*--- Exported Interface ---*)

	(** set default coordinate origin and scale factor **)
	проц SetCoordinates* (bc: Context; x, y, scale: вещ32);
	нач
		bc.orgX := x; bc.orgY := y; bc.scale := scale
	кон SetCoordinates;

	(** set background color **)
	проц SetBGColor* (bc: Context; col: Gfx.Color);
	нач
		bc.bgCol := col
	кон SetBGColor;

	(** set composition operation as defined in Raster **)
	проц SetCompOp* (bc: Context; op:цел8);
	нач
		bc.compOp := op
	кон SetCompOp;

	(** initialize buffered context **)
	проц Init* (bc: Context; img: Images.Image);
	нач
		GfxRaster.InitContext(bc);
		bc.compOp := 1; (* Copy Op is default *)
		bc.img := img; bc.do := Methods; bc.dot := Dot; bc.rect := Rect; bc.setColPat := SetColPat;
		SetCoordinates(bc, 0, 0, 1);
		SetBGColor(bc, Gfx.White);
		Gfx.DefResetContext(bc)
	кон Init;


нач
	InitMethods
кон GfxBuffer.
