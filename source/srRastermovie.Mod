модуль srRastermovie;

использует Strings, Raster, WMGraphics, Out:=ЛогЯдра, WMWindowManager, Rectangles:=WMRectangles, Kernel,Modules(*srRenderBase*);

тип Screen = окласс(WMWindowManager.BufferWindow)
перем
	alive: булево;
	timer: Kernel.Timer;
	frame: цел32;
	playing: булево;

проц & New*;
нач
(*	Init(srRenderBase.W*16 + 2, srRenderBase.H*16 + 2, FALSE); *)
	manager := WMWindowManager.GetDefaultManager();
	manager.Add(300, 0, сам, { WMWindowManager.FlagFrame, WMWindowManager.FlagStayOnTop});
кон New;

проц {перекрыта}Close*;
нач
	alive := ложь;
	Close^;
кон Close;

проц playframe;
перем
	t: массив 24 из симв8;
	n: массив 8 из симв8;
нач
	t:="";
	увел(frame);
	Strings.Append(t,title);
	Strings.IntToStr(frame, n);
	Strings.Append(t,n);
	Strings.Append(t,".bmp");
(*	WMGraphics.LoadImage(t,FALSE); *)
(*	IF ~done THEN playing := FALSE END; *)
	screen.Invalidate(Rectangles.MakeRect(0, 0, screen.GetWidth(), screen.GetHeight()));
кон playframe;

(*BEGIN {ACTIVE}
	alive := TRUE;
	NEW(timer);
	Objects.SetPriority(Objects.Normal);
	REPEAT
	timer.Sleep(0);
	IF playing THEN playframe END;
	UNTIL ~playing;
	srBase.worldalive :=FALSE;
	Close; *)
кон Screen;
перем
	title: массив 24 из симв8;
	frame: цел32;
	screen: Screen;

проц snap*(img: Raster.Image);
перем
	t: массив 24 из симв8;
	n: массив 8 из симв8;
	res: целМЗ;
нач
	увел(frame);
	Strings.Append(t,title);
	Strings.IntToStr(frame, n);
	Strings.Append(t,n);
	Strings.Append(t,".bmp");
	WMGraphics.StoreImage(img,t, res);
	Out.пСтроку8(t); Out.пВК_ПС;
кон snap;

проц snapshot*(img: Raster.Image);
перем
	res: целМЗ;
нач
	WMGraphics.StoreImage(img,"SYS:snapshot.bmp", res);
	Out.пСтроку8("snap snap"); Out.пВК_ПС;
кон snapshot;

проц play*;
нач
	нов(screen);
	screen.frame := 1000;
	screen.playing := истина;
кон play;

проц Close*;
нач
	если screen#НУЛЬ то screen.Close всё;
кон Close;

нач
	title := "FAT:x";
	frame := 10000;
	Modules.InstallTermHandler(Close);
кон srRastermovie.play

System.Free srRastermovie
